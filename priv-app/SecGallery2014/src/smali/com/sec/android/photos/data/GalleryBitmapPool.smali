.class public Lcom/sec/android/photos/data/GalleryBitmapPool;
.super Ljava/lang/Object;
.source "GalleryBitmapPool.java"


# static fields
.field private static final CAPACITY_BYTES:I = 0x1400000

.field private static final COMMON_PHOTO_ASPECT_RATIOS:[Landroid/graphics/Point;

.field private static final POOL_INDEX_MISC:I = 0x2

.field private static final POOL_INDEX_NONE:I = -0x1

.field private static final POOL_INDEX_PHOTO:I = 0x1

.field private static final POOL_INDEX_SQUARE:I

.field private static sInstance:Lcom/sec/android/photos/data/GalleryBitmapPool;


# instance fields
.field private mCapacityBytes:I

.field private mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

.field private mSharedNodePool:Landroid/util/Pools$Pool;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pools$Pool",
            "<",
            "Lcom/sec/android/photos/data/SparseArrayBitmapPool$Node;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x3

    .line 53
    new-array v0, v4, [Landroid/graphics/Point;

    const/4 v1, 0x0

    new-instance v2, Landroid/graphics/Point;

    const/4 v3, 0x4

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v4, v5}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/graphics/Point;

    const/16 v2, 0x10

    const/16 v3, 0x9

    invoke-direct {v1, v2, v3}, Landroid/graphics/Point;-><init>(II)V

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/android/photos/data/GalleryBitmapPool;->COMMON_PHOTO_ASPECT_RATIOS:[Landroid/graphics/Point;

    .line 72
    new-instance v0, Lcom/sec/android/photos/data/GalleryBitmapPool;

    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getSquareSize()I

    move-result v1

    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getPhotoSize()I

    move-result v2

    invoke-static {}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getMiscSize()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/photos/data/GalleryBitmapPool;-><init>(III)V

    sput-object v0, Lcom/sec/android/photos/data/GalleryBitmapPool;->sInstance:Lcom/sec/android/photos/data/GalleryBitmapPool;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 3
    .param p1, "capacityBytes"    # I

    .prologue
    .line 61
    div-int/lit8 v0, p1, 0x3

    div-int/lit8 v1, p1, 0x3

    div-int/lit8 v2, p1, 0x3

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/android/photos/data/GalleryBitmapPool;-><init>(III)V

    .line 62
    return-void
.end method

.method private constructor <init>(III)V
    .locals 4
    .param p1, "squreBytes"    # I
    .param p2, "photoBytes"    # I
    .param p3, "miscBytes"    # I

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Landroid/util/Pools$SynchronizedPool;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/Pools$SynchronizedPool;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mSharedNodePool:Landroid/util/Pools$Pool;

    .line 65
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    iput-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    .line 66
    iget-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    iget-object v3, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mSharedNodePool:Landroid/util/Pools$Pool;

    invoke-direct {v2, p1, v3}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;-><init>(ILandroid/util/Pools$Pool;)V

    aput-object v2, v0, v1

    .line 67
    iget-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    const/4 v1, 0x1

    new-instance v2, Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    iget-object v3, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mSharedNodePool:Landroid/util/Pools$Pool;

    invoke-direct {v2, p2, v3}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;-><init>(ILandroid/util/Pools$Pool;)V

    aput-object v2, v0, v1

    .line 68
    iget-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    const/4 v1, 0x2

    new-instance v2, Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    iget-object v3, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mSharedNodePool:Landroid/util/Pools$Pool;

    invoke-direct {v2, p3, v3}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;-><init>(ILandroid/util/Pools$Pool;)V

    aput-object v2, v0, v1

    .line 69
    add-int v0, p1, p2

    add-int/2addr v0, p3

    iput v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mCapacityBytes:I

    .line 70
    return-void
.end method

.method public static getInstance()Lcom/sec/android/photos/data/GalleryBitmapPool;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/photos/data/GalleryBitmapPool;->sInstance:Lcom/sec/android/photos/data/GalleryBitmapPool;

    return-object v0
.end method

.method private static getMiscSize()I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 89
    invoke-static {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v0

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getTargetSize(I)I

    move-result v1

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    mul-int/lit8 v0, v0, 0x6

    return v0
.end method

.method private static getPhotoSize()I
    .locals 1

    .prologue
    .line 85
    const v0, 0x6aaaaa

    return v0
.end method

.method private getPoolForDimensions(II)Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getPoolIndexForDimensions(II)I

    move-result v0

    .line 96
    .local v0, "index":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 97
    const/4 v1, 0x0

    .line 99
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    aget-object v1, v1, v0

    goto :goto_0
.end method

.method private getPoolIndexForDimensions(II)I
    .locals 8
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 104
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 105
    :cond_0
    const/4 v6, -0x1

    .line 123
    :goto_0
    return v6

    .line 107
    :cond_1
    if-ne p1, p2, :cond_2

    .line 108
    const/4 v6, 0x0

    goto :goto_0

    .line 111
    :cond_2
    if-le p1, p2, :cond_3

    .line 112
    move v5, p2

    .line 113
    .local v5, "min":I
    move v4, p1

    .line 118
    .local v4, "max":I
    :goto_1
    sget-object v1, Lcom/sec/android/photos/data/GalleryBitmapPool;->COMMON_PHOTO_ASPECT_RATIOS:[Landroid/graphics/Point;

    .local v1, "arr$":[Landroid/graphics/Point;
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_5

    aget-object v0, v1, v2

    .line 119
    .local v0, "ar":Landroid/graphics/Point;
    iget v6, v0, Landroid/graphics/Point;->x:I

    mul-int/2addr v6, v5

    iget v7, v0, Landroid/graphics/Point;->y:I

    mul-int/2addr v7, v4

    if-ne v6, v7, :cond_4

    .line 120
    const/4 v6, 0x1

    goto :goto_0

    .line 115
    .end local v0    # "ar":Landroid/graphics/Point;
    .end local v1    # "arr$":[Landroid/graphics/Point;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "max":I
    .end local v5    # "min":I
    :cond_3
    move v5, p1

    .line 116
    .restart local v5    # "min":I
    move v4, p2

    .restart local v4    # "max":I
    goto :goto_1

    .line 118
    .restart local v0    # "ar":Landroid/graphics/Point;
    .restart local v1    # "arr$":[Landroid/graphics/Point;
    .restart local v2    # "i$":I
    .restart local v3    # "len$":I
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 123
    .end local v0    # "ar":Landroid/graphics/Point;
    :cond_5
    const/4 v6, 0x2

    goto :goto_0
.end method

.method private static getSquareSize()I
    .locals 2

    .prologue
    .line 79
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    mul-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x4

    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileNumPerScreen()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    mul-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public clear()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    .local v0, "arr$":[Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 181
    .local v3, "p":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    invoke-virtual {v3}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;->clear()V

    .line 180
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 183
    .end local v3    # "p":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    :cond_0
    return-void
.end method

.method public get(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 151
    invoke-direct {p0, p1, p2}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getPoolForDimensions(II)Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    move-result-object v0

    .line 152
    .local v0, "pool":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    if-nez v0, :cond_0

    .line 153
    const/4 v1, 0x0

    .line 155
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;->get(II)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public declared-synchronized getCapacity()I
    .locals 1

    .prologue
    .line 130
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mCapacityBytes:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getSize()I
    .locals 6

    .prologue
    .line 140
    const/4 v4, 0x0

    .line 141
    .local v4, "total":I
    iget-object v0, p0, Lcom/sec/android/photos/data/GalleryBitmapPool;->mPools:[Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    .local v0, "arr$":[Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 142
    .local v3, "p":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    invoke-virtual {v3}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;->getSize()I

    move-result v5

    add-int/2addr v4, v5

    .line 141
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 144
    .end local v3    # "p":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    :cond_0
    return v4
.end method

.method public put(Landroid/graphics/Bitmap;)Z
    .locals 4
    .param p1, "b"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v1, 0x0

    .line 164
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v2, v3, :cond_1

    .line 172
    :cond_0
    :goto_0
    return v1

    .line 167
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/sec/android/photos/data/GalleryBitmapPool;->getPoolForDimensions(II)Lcom/sec/android/photos/data/SparseArrayBitmapPool;

    move-result-object v0

    .line 168
    .local v0, "pool":Lcom/sec/android/photos/data/SparseArrayBitmapPool;
    if-nez v0, :cond_2

    .line 169
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 172
    :cond_2
    invoke-virtual {v0, p1}, Lcom/sec/android/photos/data/SparseArrayBitmapPool;->put(Landroid/graphics/Bitmap;)Z

    move-result v1

    goto :goto_0
.end method
