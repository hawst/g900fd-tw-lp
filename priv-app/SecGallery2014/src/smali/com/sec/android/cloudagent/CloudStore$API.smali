.class public final Lcom/sec/android/cloudagent/CloudStore$API;
.super Ljava/lang/Object;
.source "CloudStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/cloudagent/CloudStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "API"
.end annotation


# static fields
.field public static final KEY_CLOUD_AVAILABLE:Ljava/lang/String; = "cloud_available"

.field public static final KEY_CLOUD_VENDOR_AVAILABLE:Ljava/lang/String; = "cloud_vendor_available"

.field public static final KEY_DOWNLOAD:Ljava/lang/String; = "download"

.field public static final KEY_EXCEPTION:Ljava/lang/String; = "exception"

.field public static final KEY_GETDOWNLOAD_URL:Ljava/lang/String; = "get_download_URL"

.field public static final KEY_GETSTREAMING_URL:Ljava/lang/String; = "get_streaming_URL"

.field public static final KEY_GETTHUMBNAIL:Ljava/lang/String; = "get_thumbnail"

.field public static final KEY_MAKE_AVAILABLE_OFFLINE:Ljava/lang/String; = "make_available_offline"

.field public static final KEY_PREFETCH:Ljava/lang/String; = "prefetch"

.field public static final KEY_PREFETCH_WITH_BLOCKING:Ljava/lang/String; = "prefetch_with_blocking"

.field public static final KEY_REVERT_AVAILABLE_OFFLINE:Ljava/lang/String; = "revert_available_offline"

.field public static final KEY_SHARE_URL:Ljava/lang/String; = "get_share_URL"

.field public static final KEY_SYNC:Ljava/lang/String; = "sync"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static download(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    const-string v4, "download"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 324
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 325
    const-string v2, "exception"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 326
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_0

    .line 327
    iget-object v2, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v2, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v2

    .line 329
    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_0
    return-void
.end method

.method public static getDownloadURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 299
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_download_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 300
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 302
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 303
    const-string v4, "DOWNLOAD_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 304
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 313
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 307
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 308
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 309
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 313
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getSharedURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 248
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_share_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 249
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 251
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 252
    const-string v4, "SHARE_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 253
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 262
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 256
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 257
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 258
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 262
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getStreamingURL(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 273
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "get_streaming_URL"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 274
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 276
    .local v2, "url":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 277
    const-string v4, "STREAMING_URL"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 278
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 287
    .end local v2    # "url":Ljava/lang/String;
    .local v3, "url":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 281
    .end local v3    # "url":Ljava/lang/String;
    .restart local v2    # "url":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 282
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 283
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 287
    .end local v2    # "url":Ljava/lang/String;
    .restart local v3    # "url":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getThumbnail(Landroid/content/Context;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 228
    const/4 v0, 0x0

    .line 229
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Images;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    const-string v5, "get_thumbnail"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 231
    .local v1, "bundle":Landroid/os/Bundle;
    if-eqz v1, :cond_0

    .line 232
    const-string v3, "ThumbnailBitmap"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 234
    .local v2, "thumbPath":Ljava/lang/String;
    invoke-static {v2}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 237
    .end local v2    # "thumbPath":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method public static isCloudAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 370
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 371
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 372
    const-string v1, "cloudAvailable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 374
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isCloudVendorAvailable(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 406
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    const-string v3, "cloud_vendor_available"

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 407
    .local v0, "result":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 408
    const-string v1, "cloud_vendor_available"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 410
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static makeAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 349
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "make_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 351
    return-void
.end method

.method public static prefetch(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 338
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "prefetch"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 340
    return-void
.end method

.method public static prefetchWithBlocking(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/android/cloudagent/exception/CloudException;
        }
    .end annotation

    .prologue
    .line 384
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    const-string v6, "prefetch_with_blocking"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v6, v7, v8}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    .line 385
    .local v0, "bundle":Landroid/os/Bundle;
    const/4 v2, 0x0

    .line 386
    .local v2, "path":Ljava/lang/String;
    if-eqz v0, :cond_1

    .line 387
    const-string v4, "CACHED_PATH"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 389
    if-eqz v2, :cond_0

    move-object v3, v2

    .line 398
    .end local v2    # "path":Ljava/lang/String;
    .local v3, "path":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 392
    .end local v3    # "path":Ljava/lang/String;
    .restart local v2    # "path":Ljava/lang/String;
    :cond_0
    const-string v4, "exception"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;

    .line 393
    .local v1, "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    if-eqz v1, :cond_1

    .line 394
    iget-object v4, v1, Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;->o:Ljava/lang/Object;

    check-cast v4, Lcom/sec/android/cloudagent/exception/CloudException;

    throw v4

    .end local v1    # "e":Lcom/sec/android/cloudagent/CloudStore$CloudSerializable;
    :cond_1
    move-object v3, v2

    .line 398
    .end local v2    # "path":Ljava/lang/String;
    .restart local v3    # "path":Ljava/lang/String;
    goto :goto_0
.end method

.method public static revertAvailableOffline(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 360
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "revert_available_offline"

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 362
    return-void
.end method

.method public static sync(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 217
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/cloudagent/CloudStore$Files;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "sync"

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->call(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 218
    return-void
.end method
