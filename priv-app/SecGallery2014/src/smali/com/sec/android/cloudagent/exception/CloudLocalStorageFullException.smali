.class public Lcom/sec/android/cloudagent/exception/CloudLocalStorageFullException;
.super Lcom/sec/android/cloudagent/exception/CloudException;
.source "CloudLocalStorageFullException.java"


# static fields
.field public static final name:Ljava/lang/String; = "CloudLocalStorageFullException"

.field private static final serialVersionUID:J = 0x1L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>()V

    .line 11
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "detailMessage"    # Ljava/lang/String;

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/cloudagent/exception/CloudException;-><init>(Ljava/lang/String;)V

    .line 15
    return-void
.end method
