.class Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;
.super Ljava/lang/Thread;
.source "VideoWallMgr.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateThread"
.end annotation


# static fields
.field private static final STATE_ACTIVE:I = 0x1

.field private static final STATE_IDLE:I = 0x0

.field private static final STATE_PAUSED:I = 0x2


# instance fields
.field private volatile mReqPause:Z

.field private volatile mReqStop:Z

.field private volatile mThreadState:I

.field final synthetic this$0:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;


# direct methods
.method private constructor <init>(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;
    .param p2, "x1"    # Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$1;

    .prologue
    .line 183
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;-><init>(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;)V

    return-void
.end method


# virtual methods
.method public isPaused()Z
    .locals 2

    .prologue
    .line 252
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isStopped()Z
    .locals 1

    .prologue
    .line 257
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pauseThread()V
    .locals 1

    .prologue
    .line 229
    monitor-enter p0

    .line 230
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    .line 231
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 232
    monitor-exit p0

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public resumeThread()V
    .locals 1

    .prologue
    .line 222
    monitor-enter p0

    .line 223
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    .line 224
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 225
    monitor-exit p0

    .line 226
    return-void

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 196
    iput v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    .line 197
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    .line 198
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqStop:Z

    .line 200
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqStop:Z

    if-nez v1, :cond_4

    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    # getter for: Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I
    invoke-static {v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->access$100(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;)I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 202
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->this$0:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    # invokes: Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->updateItem(I)V
    invoke-static {v1, v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->access$200(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;I)V

    .line 203
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqStop:Z

    if-eqz v1, :cond_3

    .line 207
    :cond_1
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    if-eqz v1, :cond_2

    .line 208
    monitor-enter p0

    .line 209
    const/4 v1, 0x2

    :try_start_0
    iput v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    .line 210
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 211
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    .line 212
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    :cond_2
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqStop:Z

    if-nez v1, :cond_0

    .line 215
    const-wide/16 v2, 0x32

    invoke-static {v2, v3}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 201
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 212
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 218
    .end local v0    # "i":I
    :cond_4
    iput v4, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    .line 219
    return-void
.end method

.method public stopThread()V
    .locals 1

    .prologue
    .line 236
    monitor-enter p0

    .line 237
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqStop:Z

    .line 238
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 239
    monitor-exit p0

    .line 240
    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public waitTillPaused()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 243
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    if-ne v0, v2, :cond_0

    .line 244
    iput-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mReqPause:Z

    .line 245
    :goto_0
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->mThreadState:I

    if-ne v0, v2, :cond_0

    .line 246
    const-wide/16 v0, 0xa

    invoke-static {v0, v1}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    .line 249
    :cond_0
    return-void
.end method
