.class public Lcom/sec/android/app/videoplayer/videowall/MscEngine;
.super Ljava/lang/Object;
.source "MscEngine.java"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 7
    const-string v0, "savsff"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 8
    const-string v0, "savsvc"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 9
    const-string v0, "savscmn"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 10
    const-string/jumbo v0, "sthmb"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 11
    const-string/jumbo v0, "vwengine"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static native close()I
.end method

.method public static native getDurationTime(Ljava/lang/String;Z)I
.end method

.method public static native initView(Landroid/graphics/Bitmap;)I
.end method

.method public static native nativeGetProgressPercent()I
.end method

.method public static native open(Z)I
.end method

.method public static native render(Landroid/graphics/Bitmap;I)I
.end method

.method public static native setThumbnail(ILjava/lang/String;)I
.end method

.method public static native stopTranscoding(I)I
.end method

.method public static native texture(I)I
.end method

.method public static native transcode(Ljava/lang/String;Ljava/lang/String;IIIIIIIZI)I
.end method
