.class public Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;
.super Ljava/lang/Object;
.source "VideoWallHWUtils.java"


# static fields
.field public static Clock:I = 0x0

.field public static Cores:I = 0x0

.field public static Device:I = 0x0

.field public static final NATIVE_DBG:Z = false

.field public static final Samsung_Device_1024x600_sw:I = 0xa09

.field public static final Samsung_Device_1280x720_hw:I = 0xa02

.field public static final Samsung_Device_1280x720_sw:I = 0xa01

.field public static final Samsung_Device_1280x800_dp10_sw:I = 0xa08

.field public static final Samsung_Device_1280x800_sw:I = 0xa07

.field public static final Samsung_Device_1920x1080_sw:I = 0xa0a

.field public static final Samsung_Device_2560x1600_sw:I = 0xa0b

.field public static final Samsung_Device_800x480_hw:I = 0xa06

.field public static final Samsung_Device_800x480_sw:I = 0xa05

.field public static final Samsung_Device_960x540_hw:I = 0xa04

.field public static final Samsung_Device_960x540_sw:I = 0xa03

.field public static final Samsung_Device_dual_under15:I = 0xa0d

.field public static final Samsung_Device_no_library:I = 0xa0e

.field public static final Samsung_Device_single:I = 0xa0c

.field public static final Support_AP_Frequency:I = 0x162010

.field public static final TAG:Ljava/lang/String; = "videowall-Global"

.field public static isswkey:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    sput v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    .line 39
    sput v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Cores:I

    .line 40
    sput v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Clock:I

    .line 42
    sput-boolean v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getListViewThumbnailHeight()I
    .locals 3

    .prologue
    const/16 v1, 0xd0

    const/16 v0, 0xa0

    .line 187
    sget v2, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 208
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 195
    goto :goto_0

    .line 200
    :pswitch_2
    const/16 v0, 0xf0

    goto :goto_0

    :pswitch_3
    move v0, v1

    .line 204
    goto :goto_0

    .line 206
    :pswitch_4
    const/16 v0, 0x130

    goto :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static getListViewThumbnailWidth()I
    .locals 2

    .prologue
    const/16 v0, 0x100

    .line 160
    sget v1, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    packed-switch v1, :pswitch_data_0

    .line 182
    :goto_0
    :pswitch_0
    return v0

    .line 163
    :pswitch_1
    const/16 v0, 0xe0

    goto :goto_0

    .line 169
    :pswitch_2
    const/16 v0, 0x150

    goto :goto_0

    .line 172
    :pswitch_3
    const/16 v0, 0x120

    goto :goto_0

    .line 174
    :pswitch_4
    const/16 v0, 0x190

    goto :goto_0

    .line 178
    :pswitch_5
    const/16 v0, 0x160

    goto :goto_0

    .line 180
    :pswitch_6
    const/16 v0, 0x1f0

    goto :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public static getSupportVW()Z
    .locals 1

    .prologue
    .line 145
    sget v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    packed-switch v0, :pswitch_data_0

    .line 151
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 149
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 145
    nop

    :pswitch_data_0
    .packed-switch 0xa0c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static getThumbnailFps()I
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0xa

    return v0
.end method

.method public static getVwengineLib()Ljava/lang/String;
    .locals 1

    .prologue
    .line 156
    const-string/jumbo v0, "vwengine"

    return-object v0
.end method

.method public static printDeviceInfo()Ljava/lang/String;
    .locals 1

    .prologue
    .line 213
    sget v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    packed-switch v0, :pswitch_data_0

    .line 241
    const-string v0, "else"

    :goto_0
    return-object v0

    .line 215
    :pswitch_0
    const-string v0, "Samsung_Device_1280x720_sw"

    goto :goto_0

    .line 217
    :pswitch_1
    const-string v0, "Samsung_Device_1280x720_hw"

    goto :goto_0

    .line 219
    :pswitch_2
    const-string v0, "Samsung_Device_960x540_sw"

    goto :goto_0

    .line 221
    :pswitch_3
    const-string v0, "Samsung_Device_960x540_hw"

    goto :goto_0

    .line 223
    :pswitch_4
    const-string v0, "Samsung_Device_800x480_sw"

    goto :goto_0

    .line 225
    :pswitch_5
    const-string v0, "Samsung_Device_800x480_hw"

    goto :goto_0

    .line 227
    :pswitch_6
    const-string v0, "Samsung_Device_1024x600_sw"

    goto :goto_0

    .line 229
    :pswitch_7
    const-string v0, "Samsung_Device_1280x800_sw"

    goto :goto_0

    .line 231
    :pswitch_8
    const-string v0, "Samsung_Device_1280x800_dp10_sw"

    goto :goto_0

    .line 233
    :pswitch_9
    const-string v0, "Samsung_Device_single"

    goto :goto_0

    .line 235
    :pswitch_a
    const-string v0, "Samsung_Device_dual_under15"

    goto :goto_0

    .line 237
    :pswitch_b
    const-string v0, "Samsung_Device_1920x1080_sw"

    goto :goto_0

    .line 239
    :pswitch_c
    const-string v0, "Samsung_Device_2560x1600_sw"

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0xa01
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_b
        :pswitch_c
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static readCoreNum()I
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v8, 0x0

    .line 247
    const/4 v0, 0x0

    .line 248
    .local v0, "core_num":I
    const-string v6, ""

    .line 249
    .local v6, "line":Ljava/lang/String;
    const/4 v4, 0x0

    .line 251
    .local v4, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v5, Ljava/io/BufferedReader;

    new-instance v9, Ljava/io/FileReader;

    const-string v10, "/sys/devices/system/cpu/possible"

    invoke-direct {v9, v10}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v5, v9}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    .end local v4    # "in":Ljava/io/BufferedReader;
    .local v5, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v5}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v6

    .line 254
    invoke-virtual {v5}, Ljava/io/BufferedReader;->close()V

    .line 255
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "read line = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 269
    if-eqz v6, :cond_0

    const-string v9, "null"

    invoke-virtual {v6, v9}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 270
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    if-ne v9, v12, :cond_3

    .line 271
    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 284
    :cond_0
    :goto_0
    if-ge v0, v12, :cond_1

    .line 285
    const/4 v0, 0x1

    .line 286
    :cond_1
    const-string/jumbo v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "core_num = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v4, v5

    .line 288
    .end local v0    # "core_num":I
    .end local v5    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    :goto_1
    return v0

    .line 257
    .restart local v0    # "core_num":I
    :catch_0
    move-exception v1

    .line 258
    .local v1, "e":Ljava/io/IOException;
    :goto_2
    const-string/jumbo v9, "videowall-Global"

    const-string v10, "IOException!"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    if-eqz v4, :cond_2

    .line 261
    :try_start_2
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_2
    :goto_3
    move v0, v8

    .line 266
    goto :goto_1

    .line 262
    :catch_1
    move-exception v2

    .line 264
    .local v2, "e1":Ljava/io/IOException;
    const-string/jumbo v9, "videowall-Global"

    const-string v10, "IOException! e1"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 272
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v5    # "in":Ljava/io/BufferedReader;
    :cond_3
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v9

    const/4 v10, 0x3

    if-ne v9, v10, :cond_0

    .line 273
    const/4 v7, 0x0

    .line 274
    .local v7, "start_num":C
    const/4 v3, 0x0

    .line 275
    .local v3, "end_num":C
    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v7

    .line 276
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v6, v8}, Ljava/lang/String;->charAt(I)C

    move-result v3

    .line 278
    const-string/jumbo v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "char 1 = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    const-string/jumbo v8, "videowall-Global"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "char 2 = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    sub-int v8, v3, v7

    add-int/lit8 v0, v8, 0x1

    goto/16 :goto_0

    .line 257
    .end local v3    # "end_num":C
    .end local v7    # "start_num":C
    :catch_2
    move-exception v1

    move-object v4, v5

    .end local v5    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    goto :goto_2
.end method

.method public static readMaxClock()I
    .locals 9

    .prologue
    .line 293
    const/4 v0, 0x0

    .line 294
    .local v0, "core_clock":I
    const-string v5, ""

    .line 295
    .local v5, "line":Ljava/lang/String;
    const/4 v3, 0x0

    .line 297
    .local v3, "in":Ljava/io/BufferedReader;
    :try_start_0
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v6, Ljava/io/FileReader;

    const-string v7, "/sys/devices/system/cpu/cpu0/cpufreq/cpuinfo_max_freq"

    invoke-direct {v6, v7}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v4, v6}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    .end local v3    # "in":Ljava/io/BufferedReader;
    .local v4, "in":Ljava/io/BufferedReader;
    :try_start_1
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v5

    .line 300
    invoke-virtual {v4}, Ljava/io/BufferedReader;->close()V

    .line 301
    const-string/jumbo v6, "videowall-Global"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "read line = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 313
    if-eqz v5, :cond_0

    .line 314
    const-string v6, "null"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 315
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 316
    const-string/jumbo v6, "videowall-Global"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "core_clock = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v3, v4

    .line 319
    .end local v0    # "core_clock":I
    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    :goto_0
    return v0

    .line 302
    .restart local v0    # "core_clock":I
    :catch_0
    move-exception v1

    .line 303
    .local v1, "e":Ljava/io/IOException;
    :goto_1
    const-string/jumbo v6, "videowall-Global"

    const-string v7, "IOException!"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    if-eqz v3, :cond_1

    .line 306
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 310
    :cond_1
    :goto_2
    const/4 v0, 0x0

    goto :goto_0

    .line 307
    :catch_1
    move-exception v2

    .line 308
    .local v2, "e1":Ljava/io/IOException;
    const-string/jumbo v6, "videowall-Global"

    const-string v7, "IOException! e1"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 302
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "e1":Ljava/io/IOException;
    .end local v3    # "in":Ljava/io/BufferedReader;
    .restart local v4    # "in":Ljava/io/BufferedReader;
    :catch_2
    move-exception v1

    move-object v3, v4

    .end local v4    # "in":Ljava/io/BufferedReader;
    .restart local v3    # "in":Ljava/io/BufferedReader;
    goto :goto_1
.end method

.method public static setDevice(Landroid/content/Context;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v12, 0x1

    const/high16 v11, 0x41200000    # 10.0f

    .line 45
    const-string/jumbo v9, "videowall-Global"

    const-string v10, "-> setDevice"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    sget v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    if-eqz v9, :cond_0

    .line 47
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->printDeviceInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    :goto_0
    return-void

    .line 51
    :cond_0
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->readCoreNum()I

    move-result v9

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Cores:I

    .line 52
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->readMaxClock()I

    move-result v9

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Clock:I

    .line 54
    sget v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Cores:I

    if-ne v9, v12, :cond_1

    .line 55
    const/16 v9, 0xa0c

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    .line 56
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->printDeviceInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 58
    :cond_1
    sget v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Cores:I

    const/4 v10, 0x2

    if-ne v9, v10, :cond_3

    .line 59
    sget v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Clock:I

    const v10, 0x162010

    if-lt v9, v10, :cond_2

    const-string v9, "msm8660"

    const-string v10, "ro.board.platform"

    invoke-static {v10}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 62
    :cond_2
    const/16 v9, 0xa0d

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    .line 63
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->printDeviceInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Clock : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Clock:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 70
    :cond_3
    :try_start_0
    const-string v9, "savsff"

    invoke-static {v9}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 71
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->getVwengineLib()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 78
    const-string/jumbo v9, "window"

    invoke-virtual {p0, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    .line 80
    .local v8, "wm":Landroid/view/WindowManager;
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 81
    .local v0, "display":Landroid/view/Display;
    new-instance v4, Landroid/util/DisplayMetrics;

    invoke-direct {v4}, Landroid/util/DisplayMetrics;-><init>()V

    .line 82
    .local v4, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {v0, v4}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 83
    iget v9, v4, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v9, v11

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    int-to-float v9, v9

    div-float v1, v9, v11

    .line 89
    .local v1, "dp":F
    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    .line 90
    .local v5, "point":Landroid/graphics/Point;
    invoke-virtual {v0, v5}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 91
    iget v7, v5, Landroid/graphics/Point;->x:I

    .line 92
    .local v7, "rawwd":I
    iget v6, v5, Landroid/graphics/Point;->y:I

    .line 93
    .local v6, "rawht":I
    if-le v7, v6, :cond_4

    move v2, v6

    .line 95
    .local v2, "dsp":I
    :goto_1
    sput-boolean v12, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    .line 97
    sparse-switch v2, :sswitch_data_0

    .line 133
    :goto_2
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "density : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Raw width : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " Raw height : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "dsp : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", swkey : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget-boolean v11, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Cores : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Cores:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", Clock : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sget v11, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Clock:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const-string/jumbo v9, "videowall-Global"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Device : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->printDeviceInfo()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 72
    .end local v0    # "display":Landroid/view/Display;
    .end local v1    # "dp":F
    .end local v2    # "dsp":I
    .end local v4    # "metrics":Landroid/util/DisplayMetrics;
    .end local v5    # "point":Landroid/graphics/Point;
    .end local v6    # "rawht":I
    .end local v7    # "rawwd":I
    .end local v8    # "wm":Landroid/view/WindowManager;
    :catch_0
    move-exception v3

    .line 73
    .local v3, "e":Ljava/lang/UnsatisfiedLinkError;
    const/16 v9, 0xa0e

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    .line 74
    sget-object v9, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v10, "Fail to load library. Disable videowall."

    invoke-virtual {v9, v10}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto/16 :goto_0

    .end local v3    # "e":Ljava/lang/UnsatisfiedLinkError;
    .restart local v0    # "display":Landroid/view/Display;
    .restart local v1    # "dp":F
    .restart local v4    # "metrics":Landroid/util/DisplayMetrics;
    .restart local v5    # "point":Landroid/graphics/Point;
    .restart local v6    # "rawht":I
    .restart local v7    # "rawwd":I
    .restart local v8    # "wm":Landroid/view/WindowManager;
    :cond_4
    move v2, v7

    .line 93
    goto/16 :goto_1

    .line 99
    .restart local v2    # "dsp":I
    :sswitch_0
    sget-boolean v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    if-eqz v9, :cond_5

    .line 100
    const/16 v9, 0xa01

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 102
    :cond_5
    const/16 v9, 0xa02

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 105
    :sswitch_1
    sget-boolean v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    if-eqz v9, :cond_6

    .line 106
    const/16 v9, 0xa03

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 108
    :cond_6
    const/16 v9, 0xa04

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 111
    :sswitch_2
    sget-boolean v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->isswkey:Z

    if-eqz v9, :cond_7

    .line 112
    const/16 v9, 0xa05

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 114
    :cond_7
    const/16 v9, 0xa06

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 117
    :sswitch_3
    const/16 v9, 0xa09

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 120
    :sswitch_4
    float-to-double v10, v1

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpl-double v9, v10, v12

    if-nez v9, :cond_8

    .line 121
    const/16 v9, 0xa08

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 123
    :cond_8
    const/16 v9, 0xa07

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 126
    :sswitch_5
    const/16 v9, 0xa0a

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 129
    :sswitch_6
    const/16 v9, 0xa0b

    sput v9, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->Device:I

    goto/16 :goto_2

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x1e0 -> :sswitch_2
        0x21c -> :sswitch_1
        0x258 -> :sswitch_3
        0x2d0 -> :sswitch_0
        0x320 -> :sswitch_4
        0x438 -> :sswitch_5
        0x640 -> :sswitch_6
    .end sparse-switch
.end method
