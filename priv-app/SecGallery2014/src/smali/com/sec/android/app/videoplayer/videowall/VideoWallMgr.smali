.class public Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;
.super Ljava/lang/Object;
.source "VideoWallMgr.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$1;,
        Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;
    }
.end annotation


# static fields
.field private static final ITEM_STATE_ACTIVE:I = 0x1

.field private static final ITEM_STATE_IDLE:I = 0x0

.field private static final ITEM_STATE_INVALID:I = 0x2

.field private static final MAX_UPDATE_ITEM:I = 0x18

.field private static final TAG:Ljava/lang/String; = "VideoWallMgr"


# instance fields
.field private mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mContext:Landroid/content/Context;

.field private mItemArray:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

.field private mItemCount:I

.field private mItemState:[I

.field private mStarted:Z

.field private mSupportVW:Z

.field private mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

.field private mWallFps:I

.field private mWallHeight:I

.field private mWallWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mContext:Landroid/content/Context;

    .line 34
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->setDevice(Landroid/content/Context;)V

    .line 35
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/VideoWallHWUtils;->getSupportVW()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    .line 36
    const-string v0, "VideoWallMgr"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSupportVW = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    return-void
.end method

.method static synthetic access$100(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    .prologue
    .line 14
    iget v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;
    .param p1, "x1"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->updateItem(I)V

    return-void
.end method

.method private updateItem(I)V
    .locals 11
    .param p1, "index"    # I

    .prologue
    const/4 v10, 0x1

    .line 155
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    aget v4, v6, p1

    .line 156
    .local v4, "state":I
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemArray:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    aget-object v1, v6, p1

    .line 157
    .local v1, "displayItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-nez v4, :cond_4

    .line 158
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 159
    .local v2, "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-object v6, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    iget-object v7, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    iget v8, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;->id:I

    int-to-long v8, v8

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/app/videoplayer/videowall/VideoWallFileUtils;->getThumbnailFileName(Ljava/lang/String;Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v5

    .line 160
    .local v5, "thumbName":Ljava/lang/String;
    invoke-static {p1, v5}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->setThumbnail(ILjava/lang/String;)I

    move-result v3

    .line 161
    .local v3, "res":I
    if-ne v3, v10, :cond_3

    .line 162
    iget v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mWallWidth:I

    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mWallHeight:I

    sget-object v8, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 163
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->initView(Landroid/graphics/Bitmap;)I

    .line 165
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->render(Landroid/graphics/Bitmap;I)I

    move-result v3

    .line 166
    if-nez v3, :cond_0

    .line 167
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->render(Landroid/graphics/Bitmap;I)I

    .line 169
    :cond_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 170
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    aput v10, v6, p1

    .line 178
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v2    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v3    # "res":I
    .end local v5    # "thumbName":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    aget v6, v6, p1

    if-ne v6, v10, :cond_2

    .line 179
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v7, v1, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->notifyDataSetChanged(II)V

    .line 181
    :cond_2
    return-void

    .line 172
    .restart local v2    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .restart local v3    # "res":I
    .restart local v5    # "thumbName":Ljava/lang/String;
    :cond_3
    iget-object v6, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    const/4 v7, 0x2

    aput v7, v6, p1

    goto :goto_0

    .line 174
    .end local v2    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v3    # "res":I
    .end local v5    # "thumbName":Ljava/lang/String;
    :cond_4
    if-ne v4, v10, :cond_1

    .line 175
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 176
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->render(Landroid/graphics/Bitmap;I)I

    goto :goto_0
.end method


# virtual methods
.method public pause()V
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-eqz v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->pauseThread()V

    .line 75
    :cond_0
    return-void
.end method

.method public resetItemArray()V
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-nez v0, :cond_1

    .line 105
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->waitTillPaused()V

    .line 104
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    goto :goto_0
.end method

.method public resume()V
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->resumeThread()V

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->start()V

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 41
    return-void
.end method

.method public setItemArray([Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;II)V
    .locals 11
    .param p1, "itemArray"    # [Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .param p2, "start"    # I
    .param p3, "end"    # I

    .prologue
    const/4 v10, 0x0

    .line 115
    iget-boolean v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    if-eqz v7, :cond_0

    iget-boolean v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-nez v7, :cond_1

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 118
    :cond_1
    if-eqz p1, :cond_0

    array-length v7, p1

    if-eqz v7, :cond_0

    .line 121
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->waitTillPaused()V

    .line 122
    iput v10, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    .line 123
    move v2, p2

    .local v2, "i":I
    :goto_1
    if-gt v2, p3, :cond_4

    .line 124
    array-length v7, p1

    rem-int v7, v2, v7

    aget-object v0, p1, v7

    .line 125
    .local v0, "displayItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-nez v0, :cond_3

    .line 123
    :cond_2
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 127
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 128
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->refreshAvailable()Z

    move-result v7

    if-eqz v7, :cond_2

    move-object v3, v4

    .line 131
    check-cast v3, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 132
    .local v3, "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-object v1, v3, Lcom/sec/android/gallery3d/data/LocalMediaItem;->filePath:Ljava/lang/String;

    .line 133
    .local v1, "filePath":Ljava/lang/String;
    iget-object v6, v3, Lcom/sec/android/gallery3d/data/LocalMediaItem;->caption:Ljava/lang/String;

    .line 134
    .local v6, "videoTitle":Ljava/lang/String;
    iget v5, v3, Lcom/sec/android/gallery3d/data/LocalMediaItem;->id:I

    .line 135
    .local v5, "videoId":I
    int-to-long v8, v5

    invoke-static {v1, v6, v8, v9}, Lcom/sec/android/app/videoplayer/videowall/VideoWallFileUtils;->isThumbnailFile(Ljava/lang/String;Ljava/lang/String;J)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 138
    const-string v7, "VideoWallMgr"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "setItemArray pathName : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemArray:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget v8, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    aput-object v0, v7, v8

    .line 140
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    iget v8, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    aput v10, v7, v8

    .line 141
    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    goto :goto_2

    .line 143
    .end local v0    # "displayItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .end local v1    # "filePath":Ljava/lang/String;
    .end local v3    # "localItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    .end local v4    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v5    # "videoId":I
    .end local v6    # "videoTitle":Ljava/lang/String;
    :cond_4
    iget v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    if-lez v7, :cond_0

    .line 144
    iget-object v7, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v7}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->resumeThread()V

    goto :goto_0
.end method

.method public start()V
    .locals 4

    .prologue
    const/16 v3, 0x18

    const/4 v2, 0x0

    .line 44
    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-eqz v0, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 47
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    .line 48
    invoke-static {v2}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->open(Z)I

    .line 49
    const/16 v0, 0x220

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mWallWidth:I

    .line 50
    const/16 v0, 0x140

    iput v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mWallHeight:I

    .line 55
    new-instance v0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;-><init>(Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$1;)V

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    .line 56
    iget-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->start()V

    .line 58
    new-array v0, v3, [Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemArray:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 59
    new-array v0, v3, [I

    iput-object v0, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    .line 60
    iput v2, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    goto :goto_0
.end method

.method public stop()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 78
    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mSupportVW:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    if-nez v1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iput-boolean v4, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mStarted:Z

    .line 82
    invoke-static {}, Lcom/sec/android/app/videoplayer/videowall/MscEngine;->close()I

    .line 83
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->stopThread()V

    .line 85
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->join(J)V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->isStopped()Z

    move-result v1

    if-nez v1, :cond_2

    .line 87
    const-string v1, "VideoWallMgr"

    const-string v2, "Request Interrupt to thumbnailThread !!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->interrupt()V

    .line 89
    iget-object v1, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mUpdateThread:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;

    invoke-virtual {v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr$UpdateThread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_2
    :goto_1
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemArray:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 95
    iput-object v5, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemState:[I

    .line 96
    iput v4, p0, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->mItemCount:I

    goto :goto_0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Ljava/lang/InterruptedException;
    const-string v1, "VideoWallMgr"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "interruptActiveThread"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
