.class public Lcom/sec/android/exifparser/ExifParser;
.super Ljava/lang/Object;
.source "ExifParser.java"


# static fields
.field private static final APP0_MARKER:B = -0x20t

.field private static final APP1_MARKER:B = -0x1ft

.field private static final APP_MARKER_COUNT:I = 0x40

.field private static final EOI_MARKER:B = -0x27t

.field private static final EXIF_HEADER:I = 0x6

.field private static final FILE_SIZE:I = 0x200000

.field private static final MARKER:I = 0x2

.field private static final MARKER_LEN:I = 0x2

.field private static final MARKER_PREFIX:B = -0x1t

.field private static final SOI_MARKER:B = -0x28t

.field private static final TAG:Ljava/lang/String; = "ExifParser"

.field private static final TIFF_HEADER:I = 0x8

.field private static final TIFF_HEADER_OFFSET:I = 0xc


# instance fields
.field private mApp1DataOffset:I

.field private mApp1Length:I

.field private mApp5DataOffset:I

.field private mApp5Length:I

.field private mByteBuffer:Ljava/nio/ByteBuffer;

.field private mEndian:Ljava/nio/ByteOrder;

.field private mFile:Ljava/io/RandomAccessFile;

.field private mFilePath:Ljava/lang/String;

.field private mLittleEndian:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Lcom/sec/android/exifparser/ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 67
    return-void
.end method

.method private IsExifFormat()Z
    .locals 12

    .prologue
    const/16 v11, 0x49

    const/16 v10, 0x2a

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 170
    iget-object v6, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v6, :cond_1

    .line 243
    :cond_0
    :goto_0
    return v4

    .line 173
    :cond_1
    const/16 v6, 0x14

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 174
    .local v1, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 175
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 176
    .local v0, "buffer":[B
    const/16 v6, 0x8

    new-array v3, v6, [B

    .line 179
    .local v3, "temp":[B
    :try_start_0
    iget-object v6, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const-wide/16 v8, 0x2

    invoke-virtual {v6, v8, v9}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 180
    iget-object v6, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    array-length v8, v0

    invoke-virtual {v6, v0, v7, v8}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_0

    .line 183
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 186
    const/16 v6, -0x1f

    invoke-direct {p0, v3, v6}, Lcom/sec/android/exifparser/ExifParser;->checkMarker([BB)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 190
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 191
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    mul-int/lit16 v6, v6, 0x100

    const/4 v7, 0x1

    aget-byte v7, v3, v7

    and-int/lit16 v7, v7, 0xff

    add-int/2addr v6, v7

    iput v6, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    .line 193
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v6

    add-int/lit8 v6, v6, 0x2

    iput v6, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    .line 196
    const/4 v6, 0x0

    const/4 v7, 0x6

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 198
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x45

    if-ne v6, v7, :cond_3

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x78

    if-ne v6, v7, :cond_3

    const/4 v6, 0x2

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x69

    if-ne v6, v7, :cond_3

    const/4 v6, 0x3

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x66

    if-ne v6, v7, :cond_3

    move v6, v5

    :goto_1
    if-eqz v6, :cond_0

    .line 205
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 208
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v11, :cond_4

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v11, :cond_4

    .line 210
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/android/exifparser/ExifParser;->mLittleEndian:Z

    .line 211
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v6, p0, Lcom/sec/android/exifparser/ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 212
    const-string v6, "ExifParser"

    const-string v7, "Little endian"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    :goto_2
    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-virtual {v1, v3, v6, v7}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 225
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-ne v6, v10, :cond_6

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-nez v6, :cond_6

    :cond_2
    :goto_3
    move v4, v5

    .line 243
    goto/16 :goto_0

    :cond_3
    move v6, v4

    .line 198
    goto :goto_1

    .line 213
    :cond_4
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x4d

    if-ne v6, v7, :cond_5

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    const/16 v7, 0x4d

    if-ne v6, v7, :cond_5

    .line 215
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/sec/android/exifparser/ExifParser;->mLittleEndian:Z

    .line 216
    sget-object v6, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v6, p0, Lcom/sec/android/exifparser/ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 217
    const-string v6, "ExifParser"

    const-string v7, "Big endian"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 238
    :catch_0
    move-exception v2

    .line 240
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 219
    .end local v2    # "e":Ljava/io/IOException;
    :cond_5
    :try_start_1
    const-string v6, "ExifParser"

    const-string v7, "Endian error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 228
    :cond_6
    const/4 v6, 0x0

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-nez v6, :cond_7

    const/4 v6, 0x1

    aget-byte v6, v3, v6

    and-int/lit16 v6, v6, 0xff

    if-eq v6, v10, :cond_2

    .line 232
    :cond_7
    const-string v6, "ExifParser"

    const-string v7, "TIFF mark - error"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private IsJpegFormat()Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 128
    iget-object v3, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v3, :cond_1

    .line 156
    :cond_0
    :goto_0
    return v2

    .line 131
    :cond_1
    const/4 v2, 0x0

    .line 132
    .local v2, "isJpeg":Z
    new-array v0, v4, [B

    .line 135
    .local v0, "buffer":[B
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 137
    const/16 v3, -0x28

    invoke-direct {p0, v0, v3}, Lcom/sec/android/exifparser/ExifParser;->checkMarker([BB)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 148
    const/4 v2, 0x1

    goto :goto_0

    .line 151
    :catch_0
    move-exception v1

    .line 153
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMarker([BB)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "marker"    # B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 160
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 166
    :cond_1
    :goto_0
    return v0

    .line 163
    :cond_2
    aget-byte v2, p1, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    aget-byte v2, p1, v0

    if-eq v2, p2, :cond_1

    :cond_3
    move v0, v1

    .line 166
    goto :goto_0
.end method

.method public static getOrientationOffset(Ljava/io/InputStream;)I
    .locals 14
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 563
    if-nez p0, :cond_0

    .line 564
    const/4 v11, 0x0

    .line 667
    :goto_0
    return v11

    .line 567
    :cond_0
    const/16 v11, 0x8

    new-array v0, v11, [B

    .line 568
    .local v0, "buf":[B
    const/4 v5, 0x0

    .line 571
    .local v5, "length":I
    :cond_1
    :goto_1
    const/4 v11, 0x2

    invoke-static {p0, v0, v11}, Lcom/sec/android/gallery3d/data/Exif;->read(Ljava/io/InputStream;[BI)Z

    move-result v11

    if-eqz v11, :cond_7

    const/4 v11, 0x0

    aget-byte v11, v0, v11

    and-int/lit16 v11, v11, 0xff

    const/16 v12, 0xff

    if-ne v11, v12, :cond_7

    .line 572
    const/4 v11, 0x1

    aget-byte v11, v0, v11

    and-int/lit16 v7, v11, 0xff

    .line 575
    .local v7, "marker":I
    const/16 v11, 0xff

    if-eq v7, v11, :cond_1

    .line 580
    const/16 v11, 0xd8

    if-eq v7, v11, :cond_1

    const/4 v11, 0x1

    if-eq v7, v11, :cond_1

    .line 584
    const/16 v11, 0xd9

    if-eq v7, v11, :cond_2

    const/16 v11, 0xda

    if-ne v7, v11, :cond_3

    .line 585
    :cond_2
    const/4 v11, 0x0

    goto :goto_0

    .line 589
    :cond_3
    const/4 v11, 0x2

    invoke-static {p0, v0, v11}, Lcom/sec/android/gallery3d/data/Exif;->read(Ljava/io/InputStream;[BI)Z

    move-result v11

    if-nez v11, :cond_4

    .line 590
    const/4 v11, 0x0

    goto :goto_0

    .line 592
    :cond_4
    const/4 v11, 0x0

    const/4 v12, 0x2

    const/4 v13, 0x0

    invoke-static {v0, v11, v12, v13}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v5

    .line 593
    const/4 v11, 0x2

    if-ge v5, v11, :cond_5

    .line 594
    const-string v11, "ExifParser"

    const-string v12, "Invalid length"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    const/4 v11, 0x0

    goto :goto_0

    .line 597
    :cond_5
    add-int/lit8 v5, v5, -0x2

    .line 600
    const/16 v11, 0xe1

    if-ne v7, v11, :cond_8

    const/4 v11, 0x6

    if-lt v5, v11, :cond_8

    .line 601
    const/4 v11, 0x6

    invoke-static {p0, v0, v11}, Lcom/sec/android/gallery3d/data/Exif;->read(Ljava/io/InputStream;[BI)Z

    move-result v11

    if-nez v11, :cond_6

    const/4 v11, 0x0

    goto :goto_0

    .line 602
    :cond_6
    add-int/lit8 v5, v5, -0x6

    .line 603
    const/4 v11, 0x0

    const/4 v12, 0x4

    const/4 v13, 0x0

    invoke-static {v0, v11, v12, v13}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v11

    const v12, 0x45786966

    if-ne v11, v12, :cond_8

    const/4 v11, 0x4

    const/4 v12, 0x2

    const/4 v13, 0x0

    invoke-static {v0, v11, v12, v13}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v11

    if-nez v11, :cond_8

    .line 619
    .end local v7    # "marker":I
    :cond_7
    const/16 v11, 0x8

    if-le v5, v11, :cond_f

    .line 620
    const/4 v8, 0x0

    .line 621
    .local v8, "offset":I
    new-array v4, v5, [B

    .line 622
    .local v4, "jpeg":[B
    invoke-static {p0, v4, v5}, Lcom/sec/android/gallery3d/data/Exif;->read(Ljava/io/InputStream;[BI)Z

    move-result v11

    if-nez v11, :cond_9

    .line 623
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 611
    .end local v4    # "jpeg":[B
    .end local v8    # "offset":I
    .restart local v7    # "marker":I
    :cond_8
    int-to-long v12, v5

    :try_start_0
    invoke-virtual {p0, v12, v13}, Ljava/io/InputStream;->skip(J)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 615
    const/4 v5, 0x0

    .line 616
    goto/16 :goto_1

    .line 612
    :catch_0
    move-exception v3

    .line 613
    .local v3, "ex":Ljava/io/IOException;
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 627
    .end local v3    # "ex":Ljava/io/IOException;
    .end local v7    # "marker":I
    .restart local v4    # "jpeg":[B
    .restart local v8    # "offset":I
    :cond_9
    const/4 v11, 0x4

    const/4 v12, 0x0

    invoke-static {v4, v8, v11, v12}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v10

    .line 628
    .local v10, "tag":I
    const v11, 0x49492a00    # 823968.0f

    if-eq v10, v11, :cond_a

    const v11, 0x4d4d002a    # 2.14958752E8f

    if-eq v10, v11, :cond_a

    .line 629
    const-string v11, "ExifParser"

    const-string v12, "Invalid byte order"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 632
    :cond_a
    const v11, 0x49492a00    # 823968.0f

    if-ne v10, v11, :cond_c

    const/4 v6, 0x1

    .line 635
    .local v6, "littleEndian":Z
    :goto_2
    const/4 v11, 0x4

    const/4 v12, 0x4

    invoke-static {v4, v11, v12, v6}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v11

    add-int/lit8 v1, v11, 0x2

    .line 636
    .local v1, "count":I
    const/16 v11, 0xa

    if-lt v1, v11, :cond_b

    if-le v1, v5, :cond_d

    .line 637
    :cond_b
    const-string v11, "ExifParser"

    const-string v12, "Invalid offset"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 632
    .end local v1    # "count":I
    .end local v6    # "littleEndian":Z
    :cond_c
    const/4 v6, 0x0

    goto :goto_2

    .line 640
    .restart local v1    # "count":I
    .restart local v6    # "littleEndian":Z
    :cond_d
    add-int/2addr v8, v1

    .line 641
    sub-int/2addr v5, v1

    .line 644
    add-int/lit8 v11, v8, -0x2

    const/4 v12, 0x2

    invoke-static {v4, v11, v12, v6}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v1

    move v2, v1

    .line 645
    .end local v1    # "count":I
    .local v2, "count":I
    :goto_3
    add-int/lit8 v1, v2, -0x1

    .end local v2    # "count":I
    .restart local v1    # "count":I
    if-lez v2, :cond_f

    const/16 v11, 0xc

    if-lt v5, v11, :cond_f

    .line 647
    const/4 v11, 0x2

    invoke-static {v4, v8, v11, v6}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v10

    .line 648
    const/16 v11, 0x112

    if-ne v10, v11, :cond_e

    .line 650
    add-int/lit8 v11, v8, 0x8

    const/4 v12, 0x2

    invoke-static {v4, v11, v12, v6}, Lcom/sec/android/gallery3d/data/Exif;->pack([BIIZ)I

    move-result v9

    .line 651
    .local v9, "orientation":I
    packed-switch v9, :pswitch_data_0

    .line 658
    :pswitch_0
    const-string v11, "ExifParser"

    const-string v12, "Unsupported orientation"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 659
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 656
    :pswitch_1
    add-int/lit8 v11, v8, 0x14

    goto/16 :goto_0

    .line 661
    .end local v9    # "orientation":I
    :cond_e
    add-int/lit8 v8, v8, 0xc

    .line 662
    add-int/lit8 v5, v5, -0xc

    move v2, v1

    .end local v1    # "count":I
    .restart local v2    # "count":I
    goto :goto_3

    .line 666
    .end local v2    # "count":I
    .end local v4    # "jpeg":[B
    .end local v6    # "littleEndian":Z
    .end local v8    # "offset":I
    .end local v10    # "tag":I
    :cond_f
    const-string v11, "ExifParser"

    const-string v12, "Orientation not found"

    invoke-static {v11, v12}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 651
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 70
    iput-object v1, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    .line 71
    iput-object v1, p0, Lcom/sec/android/exifparser/ExifParser;->mFilePath:Ljava/lang/String;

    .line 73
    iput v0, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    .line 74
    iput v0, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    .line 76
    iput v0, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    .line 77
    iput v0, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    .line 79
    iput-boolean v0, p0, Lcom/sec/android/exifparser/ExifParser;->mLittleEndian:Z

    .line 80
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    iput-object v0, p0, Lcom/sec/android/exifparser/ExifParser;->mEndian:Ljava/nio/ByteOrder;

    .line 81
    return-void
.end method

.method public static setOrientation(Ljava/lang/String;II)V
    .locals 8
    .param p0, "filePath"    # Ljava/lang/String;
    .param p1, "offset"    # I
    .param p2, "rotation"    # I

    .prologue
    .line 528
    const/4 v5, 0x0

    .line 529
    .local v5, "orientation":I
    sparse-switch p2, :sswitch_data_0

    .line 544
    :goto_0
    const/4 v3, 0x0

    .line 546
    .local v3, "file":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v6, "rw"

    invoke-direct {v4, p0, v6}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 548
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .local v4, "file":Ljava/io/RandomAccessFile;
    const/4 v6, 0x4

    :try_start_1
    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 549
    .local v0, "bb":Ljava/nio/ByteBuffer;
    sget-object v6, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 550
    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 551
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 552
    .local v1, "buffer":[B
    int-to-long v6, p1

    invoke-virtual {v4, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 553
    const/4 v6, 0x0

    const/4 v7, 0x4

    invoke-virtual {v4, v1, v6, v7}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 558
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v3, v4

    .line 560
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "buffer":[B
    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    :goto_1
    return-void

    .line 531
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    :sswitch_0
    const/4 v5, 0x1

    .line 532
    goto :goto_0

    .line 534
    :sswitch_1
    const/4 v5, 0x6

    .line 535
    goto :goto_0

    .line 537
    :sswitch_2
    const/4 v5, 0x3

    .line 538
    goto :goto_0

    .line 540
    :sswitch_3
    const/16 v5, 0x8

    goto :goto_0

    .line 554
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    :catch_0
    move-exception v2

    .line 556
    .local v2, "e":Ljava/io/IOException;
    :goto_2
    :try_start_2
    const-string v6, "ExifParser"

    const-string v7, "setOrientation exception"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 558
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v2    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .restart local v4    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 554
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .restart local v4    # "file":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2

    .line 529
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 117
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_0

    .line 118
    iget-object v1, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 124
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/exifparser/ExifParser;->reset()V

    .line 125
    return-void

    .line 119
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getApp5Segment()Ljava/lang/String;
    .locals 14

    .prologue
    .line 411
    const-string v8, ""

    .line 413
    .local v8, "text":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v10, :cond_0

    move-object v9, v8

    .line 524
    .end local v8    # "text":Ljava/lang/String;
    .local v9, "text":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 420
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_0
    const/4 v3, 0x0

    .line 422
    .local v3, "hasApp5":Z
    :try_start_0
    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    iget v11, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    add-int/2addr v10, v11

    add-int/lit8 v6, v10, -0x2

    .line 425
    .local v6, "markerOffset":I
    const/4 v10, 0x2

    new-array v1, v10, [B

    .line 426
    .local v1, "buffer":[B
    const/4 v7, 0x0

    .line 428
    .local v7, "seekPos":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    const/16 v10, 0x40

    if-ge v4, v10, :cond_1

    .line 429
    const/4 v10, 0x1

    if-lt v4, v10, :cond_2

    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    iget v11, p0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    add-int/2addr v10, v11

    add-int/lit8 v10, v10, -0x2

    if-ne v6, v10, :cond_2

    .line 469
    :cond_1
    :goto_2
    if-nez v3, :cond_5

    move-object v9, v8

    .line 470
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto :goto_0

    .line 432
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_2
    if-ltz v6, :cond_1

    int-to-long v10, v6

    iget-object v12, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    .line 435
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v6

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 436
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 439
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_4

    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x1f

    if-lt v10, v11, :cond_4

    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x11

    if-gt v10, v11, :cond_4

    .line 444
    const/4 v10, 0x1

    aget-byte v10, v1, v10

    const/16 v11, -0x1b

    if-ne v10, v11, :cond_3

    .line 445
    const/4 v3, 0x1

    .line 446
    goto :goto_2

    .line 449
    :cond_3
    add-int/lit8 v7, v6, 0x2

    .line 451
    if-ltz v7, :cond_1

    int-to-long v10, v7

    iget-object v12, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-gez v10, :cond_1

    .line 454
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v7

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 458
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_1

    .line 461
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    mul-int/lit16 v10, v10, 0x100

    const/4 v11, 0x1

    aget-byte v11, v1, v11

    and-int/lit16 v11, v11, 0xff

    add-int v5, v10, v11

    .line 464
    .local v5, "markerLength":I
    add-int/lit8 v10, v5, 0x2

    add-int/2addr v6, v10

    .line 428
    .end local v5    # "markerLength":I
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 472
    :cond_5
    add-int/lit8 v10, v6, 0x2

    add-int/lit8 v10, v10, 0x2

    iput v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    .line 474
    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    if-lez v10, :cond_6

    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    int-to-long v10, v10

    iget-object v12, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_7

    :cond_6
    move-object v9, v8

    .line 475
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 477
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_7
    add-int/lit8 v7, v6, 0x2

    .line 479
    if-ltz v7, :cond_8

    int-to-long v10, v7

    iget-object v12, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_9

    :cond_8
    move-object v9, v8

    .line 480
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 482
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_9
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v12, v7

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 485
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    const/4 v12, 0x2

    invoke-virtual {v10, v1, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-ne v10, v11, :cond_a

    move-object v9, v8

    .line 486
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 488
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_a
    const/4 v10, 0x0

    aget-byte v10, v1, v10

    and-int/lit16 v10, v10, 0xff

    mul-int/lit16 v10, v10, 0x100

    const/4 v11, 0x1

    aget-byte v11, v1, v11

    and-int/lit16 v11, v11, 0xff

    add-int v5, v10, v11

    .line 493
    .restart local v5    # "markerLength":I
    iput v5, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    .line 495
    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    if-nez v10, :cond_b

    move-object v9, v8

    .line 496
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 499
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_b
    const/4 v0, 0x0

    .line 501
    .local v0, "attribute":[B
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-nez v10, :cond_c

    .line 502
    const/high16 v10, 0x200000

    invoke-static {v10}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 505
    :cond_c
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-eqz v10, :cond_d

    .line 506
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v10}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 508
    :cond_d
    if-eqz v0, :cond_10

    .line 509
    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    if-ltz v10, :cond_e

    iget v10, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    int-to-long v10, v10

    iget-object v12, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v12}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v10, v10, v12

    if-ltz v10, :cond_f

    :cond_e
    move-object v9, v8

    .line 510
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 512
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_f
    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    iget v11, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    int-to-long v12, v11

    invoke-virtual {v10, v12, v13}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 514
    array-length v10, v0

    iget v11, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    add-int/lit8 v11, v11, -0x2

    if-lt v10, v11, :cond_10

    iget-object v10, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v11, 0x0

    iget v12, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    add-int/lit8 v12, v12, -0x2

    invoke-virtual {v10, v0, v11, v12}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v10

    const/4 v11, -0x1

    if-eq v10, v11, :cond_10

    .line 516
    new-instance v9, Ljava/lang/String;

    const/4 v10, 0x0

    iget v11, p0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    add-int/lit8 v11, v11, -0x2

    invoke-direct {v9, v0, v10, v11}, Ljava/lang/String;-><init>([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    move-object v8, v9

    .end local v0    # "attribute":[B
    .end local v1    # "buffer":[B
    .end local v4    # "i":I
    .end local v5    # "markerLength":I
    .end local v6    # "markerOffset":I
    .end local v7    # "seekPos":I
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :cond_10
    :goto_3
    move-object v9, v8

    .line 524
    .end local v8    # "text":Ljava/lang/String;
    .restart local v9    # "text":Ljava/lang/String;
    goto/16 :goto_0

    .line 519
    .end local v9    # "text":Ljava/lang/String;
    .restart local v8    # "text":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 521
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3
.end method

.method public open(Ljava/lang/String;)Z
    .locals 5
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    .line 84
    const/4 v2, 0x0

    .line 87
    .local v2, "isOpened":Z
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 89
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 90
    const/4 v3, 0x0

    .line 112
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v3

    .line 92
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/exifparser/ExifParser;->mFilePath:Ljava/lang/String;

    .line 94
    new-instance v3, Ljava/io/RandomAccessFile;

    const-string v4, "rw"

    invoke-direct {v3, p1, v4}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    .line 96
    iget-object v3, p0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v3, :cond_1

    .line 97
    invoke-direct {p0}, Lcom/sec/android/exifparser/ExifParser;->IsJpegFormat()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 98
    invoke-direct {p0}, Lcom/sec/android/exifparser/ExifParser;->IsExifFormat()Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_1

    .line 99
    const/4 v2, 0x1

    .line 108
    .end local v1    # "file":Ljava/io/File;
    :cond_1
    :goto_1
    if-nez v2, :cond_2

    .line 109
    invoke-virtual {p0}, Lcom/sec/android/exifparser/ExifParser;->close()V

    :cond_2
    move v3, v2

    .line 112
    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    .line 105
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v3, "ExifParser"

    const-string v4, "File is not founded."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setApp5Segment(Ljava/lang/String;)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 247
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/exifparser/ExifParser;->setApp5Segment(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setApp5Segment(Ljava/lang/String;Z)Z
    .locals 18
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "replaced"    # Z

    .prologue
    .line 251
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v14, :cond_1

    .line 252
    const/4 v6, 0x0

    .line 407
    :cond_0
    :goto_0
    return v6

    .line 257
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v14

    if-nez v14, :cond_3

    .line 258
    :cond_2
    const/4 v6, 0x0

    goto :goto_0

    .line 260
    :cond_3
    const/4 v6, 0x0

    .line 263
    .local v6, "hasApp5":Z
    :try_start_0
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    add-int/2addr v14, v15

    add-int/lit8 v9, v14, -0x2

    .line 266
    .local v9, "markerOffset":I
    const/4 v14, 0x2

    new-array v4, v14, [B

    .line 267
    .local v4, "buffer":[B
    const/4 v11, 0x0

    .line 269
    .local v11, "seekPos":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    const/16 v14, 0x40

    if-ge v7, v14, :cond_4

    .line 270
    const/4 v14, 0x1

    if-lt v7, v14, :cond_7

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp1DataOffset:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/exifparser/ExifParser;->mApp1Length:I

    add-int/2addr v14, v15

    add-int/lit8 v14, v14, -0x2

    if-ne v9, v14, :cond_7

    .line 311
    :cond_4
    :goto_2
    const/4 v3, 0x0

    .line 313
    .local v3, "attribute":[B
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-nez v14, :cond_5

    .line 314
    const/high16 v14, 0x200000

    invoke-static {v14}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v14

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    .line 317
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    if-eqz v14, :cond_6

    .line 318
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mByteBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v14}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    .line 320
    :cond_6
    if-nez v3, :cond_d

    .line 321
    const/4 v6, 0x0

    goto :goto_0

    .line 273
    .end local v3    # "attribute":[B
    :cond_7
    if-ltz v9, :cond_4

    int-to-long v14, v9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    .line 276
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v9

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 277
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_8

    .line 278
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 280
    :cond_8
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_c

    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1f

    if-lt v14, v15, :cond_c

    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x11

    if-gt v14, v15, :cond_c

    .line 285
    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1b

    if-ne v14, v15, :cond_9

    if-nez p2, :cond_9

    .line 286
    const/4 v6, 0x1

    goto/16 :goto_0

    .line 287
    :cond_9
    const/4 v14, 0x1

    aget-byte v14, v4, v14

    const/16 v15, -0x1b

    if-ne v14, v15, :cond_a

    const/4 v14, 0x1

    move/from16 v0, p2

    if-ne v0, v14, :cond_a

    .line 288
    const/4 v6, 0x1

    .line 289
    goto :goto_2

    .line 291
    :cond_a
    add-int/lit8 v11, v9, 0x2

    .line 293
    if-ltz v11, :cond_4

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    .line 296
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 300
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_b

    .line 301
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 303
    :cond_b
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    and-int/lit16 v14, v14, 0xff

    mul-int/lit16 v14, v14, 0x100

    const/4 v15, 0x1

    aget-byte v15, v4, v15

    and-int/lit16 v15, v15, 0xff

    add-int v8, v14, v15

    .line 306
    .local v8, "markerLength":I
    add-int/lit8 v14, v8, 0x2

    add-int/2addr v9, v14

    .line 269
    .end local v8    # "markerLength":I
    :cond_c
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_1

    .line 323
    .restart local v3    # "attribute":[B
    :cond_d
    move v2, v9

    .line 325
    .local v2, "app5MarkerOffset":I
    add-int/lit8 v14, v2, 0x2

    add-int/lit8 v14, v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    .line 327
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    if-lez v14, :cond_e

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5DataOffset:I

    int-to-long v14, v14

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_f

    .line 328
    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 330
    :cond_f
    const/4 v10, 0x0

    .line 332
    .local v10, "nextMarkerOffset":I
    if-eqz v6, :cond_14

    .line 333
    add-int/lit8 v11, v2, 0x2

    .line 335
    if-ltz v11, :cond_10

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_11

    .line 336
    :cond_10
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 338
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 341
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_12

    .line 342
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 344
    :cond_12
    const/4 v14, 0x0

    aget-byte v14, v4, v14

    and-int/lit16 v14, v14, 0xff

    mul-int/lit16 v14, v14, 0x100

    const/4 v15, 0x1

    aget-byte v15, v4, v15

    and-int/lit16 v15, v15, 0xff

    add-int v8, v14, v15

    .line 347
    .restart local v8    # "markerLength":I
    add-int v14, v2, v8

    add-int/lit8 v10, v14, 0x2

    .line 353
    .end local v8    # "markerLength":I
    :goto_3
    if-ltz v10, :cond_13

    int-to-long v14, v10

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_15

    .line 354
    :cond_13
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 350
    :cond_14
    move v10, v2

    goto :goto_3

    .line 356
    :cond_15
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v14}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    .line 358
    .local v12, "oldFileSize":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v10

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 359
    array-length v14, v3

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v15, v0

    if-ge v14, v15, :cond_16

    .line 360
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 362
    :cond_16
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v14, v3, v15, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v14

    const/4 v15, -0x1

    if-ne v14, v15, :cond_17

    .line 364
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 367
    :cond_17
    if-ltz v2, :cond_18

    int-to-long v14, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_19

    .line 368
    :cond_18
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 371
    :cond_19
    const/4 v14, 0x0

    const/4 v15, -0x1

    aput-byte v15, v4, v14

    .line 372
    const/4 v14, 0x1

    const/16 v15, -0x1b

    aput-byte v15, v4, v14

    .line 374
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v2

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 375
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 378
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v14

    array-length v14, v14

    add-int/lit8 v14, v14, 0x2

    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    .line 379
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    and-int/lit16 v15, v15, 0xff

    int-to-byte v15, v15

    aput-byte v15, v4, v14

    .line 380
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    const v16, 0xff00

    and-int v15, v15, v16

    div-int/lit16 v15, v15, 0x100

    int-to-byte v15, v15

    aput-byte v15, v4, v14

    .line 381
    add-int/lit8 v11, v2, 0x2

    .line 382
    if-ltz v11, :cond_1a

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1b

    .line 383
    :cond_1a
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 384
    :cond_1b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 385
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v14, v4, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 387
    add-int/lit8 v14, v2, 0x2

    add-int/lit8 v11, v14, 0x2

    .line 388
    if-ltz v11, :cond_1c

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1d

    .line 389
    :cond_1c
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 390
    :cond_1d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 391
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v15

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v17

    move-object/from16 v0, v17

    array-length v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v14 .. v17}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 393
    add-int/lit8 v14, v2, 0x2

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/exifparser/ExifParser;->mApp5Length:I

    add-int v11, v14, v15

    .line 394
    if-ltz v11, :cond_1e

    int-to-long v14, v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-ltz v14, :cond_1f

    .line 395
    :cond_1e
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 396
    :cond_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 397
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/android/exifparser/ExifParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v15, 0x0

    int-to-long v0, v10

    move-wide/from16 v16, v0

    sub-long v16, v12, v16

    move-wide/from16 v0, v16

    long-to-int v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v14, v3, v15, v0}, Ljava/io/RandomAccessFile;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    if-ge v11, v10, :cond_0

    goto/16 :goto_0

    .line 402
    .end local v2    # "app5MarkerOffset":I
    .end local v3    # "attribute":[B
    .end local v4    # "buffer":[B
    .end local v7    # "i":I
    .end local v9    # "markerOffset":I
    .end local v10    # "nextMarkerOffset":I
    .end local v11    # "seekPos":I
    .end local v12    # "oldFileSize":J
    :catch_0
    move-exception v5

    .line 404
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_0
.end method
