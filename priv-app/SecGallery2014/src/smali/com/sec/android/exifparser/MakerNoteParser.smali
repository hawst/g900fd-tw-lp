.class public Lcom/sec/android/exifparser/MakerNoteParser;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/exifparser/MakerNoteParser$Tag;,
        Lcom/sec/android/exifparser/MakerNoteParser$IFD;
    }
.end annotation


# static fields
.field private static final APP0_MARKER:B = -0x20t

.field private static final APP1_MARKER:B = -0x1ft

.field private static final EOI_MARKER:B = -0x27t

.field private static final EXIF_HEADER:I = 0x6

.field private static final EXIF_PRIVATE_TAGCOUNT:I = 0x3b

.field private static final MARKER:I = 0x2

.field private static final MARKER_LEN:I = 0x2

.field private static final MARKER_PREFIX:B = -0x1t

.field private static final SEC_MAKERNOTE_TAGCOUNT:I = 0x32

.field private static final SOI_MARKER:B = -0x28t

.field private static final TAG:Ljava/lang/String; = "MakerNoteParser"

.field public static final TAG_AE:I = 0x60

.field public static final TAG_AF:I = 0x80

.field public static final TAG_AWB01:I = 0xa0

.field public static final TAG_AWB02:I = 0xa1

.field public static final TAG_CITYID:I = 0x10

.field public static final TAG_COLORINFO:I = 0x20

.field public static final TAG_COLORSPACE:I = 0x50

.field public static final TAG_CONTINUOUSSHOTINTO:I = 0xb

.field public static final TAG_CTWEATHER:I = 0xc

.field private static final TAG_EXIF_EXIFIFD:I = 0x8769

.field private static final TAG_EXIF_MAKERNOTEIFD:I = 0x927c

.field public static final TAG_FACEDETECTION:I = 0x100

.field public static final TAG_FACEFEAT01:I = 0x101

.field public static final TAG_FACEFEAT02:I = 0x102

.field public static final TAG_FACENAMEINFO:I = 0x123

.field public static final TAG_FACERECOGNITION:I = 0x120

.field public static final TAG_FAVORITETAGGING:I = 0x40

.field public static final TAG_GPSINFO01:I = 0x30

.field public static final TAG_GPSINFO02:I = 0x31

.field public static final TAG_IMAGECOUNT:I = 0x25

.field public static final TAG_IPC:I = 0xc0

.field public static final TAG_LENSINFO:I = 0x140

.field public static final TAG_MAKERNOTEVERSION:I = 0x1

.field public static final TAG_MODELSERIALNUMBER:I = 0x23

.field public static final TAG_PREVIEWIMAGEINFO:I = 0x35

.field public static final TAG_SAMSUNGCONTENTID:I = 0x4

.field public static final TAG_SAMSUNGDEVICEID:I = 0x2

.field public static final TAG_SAMSUNGMODELID:I = 0x3

.field public static final TAG_SHOTMODE:I = 0xa

.field public static final TAG_SMARTAUTO:I = 0xe0

.field public static final TAG_SRMCOMPRESSIONMODE:I = 0x45

.field public static final TAG_THIRDPARTY:I = 0xa000

.field private static final TAG_TYPE_ASCII:I = 0x2

.field private static final TAG_TYPE_BYTE:I = 0x1

.field private static final TAG_TYPE_LONG:I = 0x4

.field private static final TAG_TYPE_RATIONAL:I = 0x5

.field private static final TAG_TYPE_SHORT:I = 0x3

.field private static final TAG_TYPE_SLONG:I = 0x9

.field private static final TAG_TYPE_SRATIONAL:I = 0xa

.field private static final TAG_TYPE_UNDEFINED:I = 0x7

.field private static final TIFF_HEADER:I = 0x8

.field private static final TIFF_HEADER_OFFSET:I = 0xc

.field private static final TIFF_TAGCOUNT:I = 0x23


# instance fields
.field private mApp1DataOffset:I

.field private mApp1Length:I

.field private mFile:Ljava/io/RandomAccessFile;

.field private mFilePath:Ljava/lang/String;

.field private mLittleEndian:Z

.field private mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

.field private mSupported:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z

    .line 111
    return-void
.end method

.method private IsExifFormat()Z
    .locals 24

    .prologue
    .line 425
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    if-nez v19, :cond_0

    .line 426
    const/16 v19, 0x0

    .line 701
    :goto_0
    return v19

    .line 428
    :cond_0
    const/16 v19, 0x14

    invoke-static/range {v19 .. v19}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 429
    .local v7, "byteBuffer":Ljava/nio/ByteBuffer;
    sget-object v8, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    .line 430
    .local v8, "byteOrder":Ljava/nio/ByteOrder;
    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 431
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 432
    .local v6, "buffer":[B
    const/16 v19, 0x8

    move/from16 v0, v19

    new-array v0, v0, [B

    move-object/from16 v18, v0

    .line 433
    .local v18, "temp":[B
    const/4 v10, 0x0

    .line 436
    .local v10, "endianOffset":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const-wide/16 v20, 0x2

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 437
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    array-length v0, v6

    move/from16 v21, v0

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_1

    .line 438
    const/16 v19, 0x0

    goto :goto_0

    .line 440
    :cond_1
    const/16 v19, 0x0

    const/16 v20, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v7, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 443
    const/16 v19, -0x1f

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, v19

    invoke-direct {v0, v1, v2}, Lcom/sec/android/exifparser/MakerNoteParser;->checkMarker([BB)Z

    move-result v19

    if-nez v19, :cond_2

    .line 444
    const/16 v19, 0x0

    goto :goto_0

    .line 447
    :cond_2
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 448
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    move/from16 v0, v19

    mul-int/lit16 v0, v0, 0x100

    move/from16 v19, v0

    const/16 v20, 0x1

    aget-byte v20, v18, v20

    move/from16 v0, v20

    and-int/lit16 v0, v0, 0xff

    move/from16 v20, v0

    add-int v19, v19, v20

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/exifparser/MakerNoteParser;->mApp1Length:I

    .line 449
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v19

    add-int/lit8 v19, v19, 0x2

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/exifparser/MakerNoteParser;->mApp1DataOffset:I

    .line 452
    const/16 v19, 0x0

    const/16 v20, 0x6

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v7, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 454
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x45

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    const/16 v19, 0x1

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x78

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    const/16 v19, 0x2

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x69

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    const/16 v19, 0x3

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x66

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    const/16 v19, 0x1

    :goto_1
    if-nez v19, :cond_4

    .line 457
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 454
    :cond_3
    const/16 v19, 0x0

    goto :goto_1

    .line 461
    :cond_4
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 464
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x49

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    const/16 v19, 0x1

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x49

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_8

    .line 466
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 467
    const-string v19, "MakerNoteParser"

    const-string v20, "Little endian"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 478
    :goto_2
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 479
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x2a

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_b

    const/16 v19, 0x1

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    if-nez v19, :cond_b

    .line 491
    :cond_5
    const/16 v19, 0x0

    const/16 v20, 0x4

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-virtual {v7, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 493
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mLittleEndian:Z

    move/from16 v19, v0

    if-nez v19, :cond_6

    .line 494
    sget-object v8, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    .line 495
    invoke-virtual {v7, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 496
    const/4 v10, 0x2

    .line 499
    :cond_6
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->position()I

    move-result v19

    add-int/lit8 v13, v19, 0x2

    .line 501
    .local v13, "firstIfdOffset":I
    if-ltz v13, :cond_7

    int-to-long v0, v13

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_d

    .line 502
    :cond_7
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 468
    .end local v13    # "firstIfdOffset":I
    :cond_8
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x4d

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    const/16 v19, 0x1

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x4d

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_a

    .line 470
    const/16 v19, 0x0

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 471
    const-string v19, "MakerNoteParser"

    const-string v20, "Big endian"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 696
    :catch_0
    move-exception v9

    .line 698
    .local v9, "e":Ljava/io/IOException;
    invoke-virtual {v9}, Ljava/io/IOException;->printStackTrace()V

    .line 701
    .end local v9    # "e":Ljava/io/IOException;
    :cond_9
    :goto_3
    const/16 v19, 0x1

    goto/16 :goto_0

    .line 473
    :cond_a
    :try_start_1
    const-string v19, "MakerNoteParser"

    const-string v20, "Endian error"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 482
    :cond_b
    const/16 v19, 0x0

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    if-nez v19, :cond_c

    const/16 v19, 0x1

    aget-byte v19, v18, v19

    move/from16 v0, v19

    and-int/lit16 v0, v0, 0xff

    move/from16 v19, v0

    const/16 v20, 0x2a

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_5

    .line 486
    :cond_c
    const-string v19, "MakerNoteParser"

    const-string v20, "TIFF mark - error"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 487
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 504
    .restart local v13    # "firstIfdOffset":I
    :cond_d
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 506
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 507
    .local v4, "b":Ljava/nio/ByteBuffer;
    const/4 v11, 0x0

    .line 508
    .local v11, "entryCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    int-to-long v0, v13

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 509
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_e

    .line 510
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 511
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 512
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 513
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 514
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 515
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 516
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 517
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 521
    :cond_e
    const/16 v19, 0x23

    move/from16 v0, v19

    if-le v11, v0, :cond_f

    .line 522
    const-string v19, "MakerNoteParser"

    const-string v20, "Tiff tag count is big."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 526
    :cond_f
    const/4 v12, 0x0

    .line 527
    .local v12, "exifIfdOffset":I
    const/16 v19, 0xc

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 528
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 529
    const/16 v17, 0x0

    .line 530
    .local v17, "tagCode":I
    const/16 v16, 0x0

    .line 532
    .local v16, "seekPos":I
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_4
    const/16 v19, 0x23

    move/from16 v0, v19

    if-ge v14, v0, :cond_10

    .line 533
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 534
    add-int/lit8 v19, v13, 0x2

    mul-int/lit8 v20, v14, 0xc

    add-int v16, v19, v20

    .line 536
    if-ltz v16, :cond_10

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_11

    .line 573
    :cond_10
    :goto_5
    if-nez v12, :cond_13

    .line 574
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 539
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0xc

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_12

    .line 542
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 543
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 544
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 545
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 546
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 547
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 548
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 549
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 552
    const v19, 0x8769

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_12

    .line 555
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 556
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 557
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 558
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 559
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 560
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 561
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 562
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 564
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 565
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v12

    .line 568
    goto/16 :goto_5

    .line 532
    :cond_12
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_4

    .line 578
    :cond_13
    add-int/lit8 v16, v12, 0xc

    .line 579
    if-ltz v16, :cond_14

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_15

    .line 580
    :cond_14
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 582
    :cond_15
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 584
    const/4 v11, 0x0

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 586
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_16

    .line 587
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 588
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 589
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 590
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 591
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 592
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 593
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 594
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 598
    :cond_16
    const/16 v19, 0x3b

    move/from16 v0, v19

    if-le v11, v0, :cond_17

    .line 599
    const-string v19, "MakerNoteParser"

    const-string v20, "Exif tag count is big."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 603
    :cond_17
    const/16 v19, 0xc

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 604
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 605
    const/16 v17, 0x0

    .line 606
    const/4 v14, 0x0

    :goto_6
    const/16 v19, 0x3b

    move/from16 v0, v19

    if-ge v14, v0, :cond_18

    .line 607
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 608
    add-int/lit8 v19, v12, 0xc

    add-int/lit8 v19, v19, 0x2

    mul-int/lit8 v20, v14, 0xc

    add-int v16, v19, v20

    .line 610
    if-ltz v16, :cond_18

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_1a

    .line 632
    :cond_18
    const/4 v15, 0x0

    .line 634
    .local v15, "makerNoteIfdOffset":I
    const v19, 0x927c

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_19

    .line 635
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getShort()S

    .line 636
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    .line 637
    invoke-virtual {v7}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v15

    .line 642
    :cond_19
    if-nez v15, :cond_1c

    .line 643
    const-string v19, "MakerNoteParser"

    const-string v20, "MakerNoteIfd is not found"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 613
    .end local v15    # "makerNoteIfdOffset":I
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 615
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0xc

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1b

    .line 616
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 617
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 618
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 619
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 620
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 621
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 622
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 623
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v17

    .line 625
    const v19, 0x927c

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_18

    .line 606
    :cond_1b
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_6

    .line 647
    .restart local v15    # "makerNoteIfdOffset":I
    :cond_1c
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v7, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 649
    const/4 v11, 0x0

    .line 650
    add-int/lit8 v16, v15, 0xc

    .line 652
    if-ltz v16, :cond_1d

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_1e

    .line 653
    :cond_1d
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 655
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 656
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    const/16 v21, 0x2

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1f

    .line 657
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 658
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 659
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 660
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 661
    const/16 v19, 0x2

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v7, v0, v10, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 662
    invoke-static/range {v18 .. v18}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 663
    invoke-virtual {v4, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 664
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v11

    .line 670
    :cond_1f
    const/16 v19, 0x32

    move/from16 v0, v19

    if-le v11, v0, :cond_20

    .line 671
    const/16 v11, 0x32

    .line 672
    const-string v19, "MakerNoteParser"

    const-string v20, "MakerNote tag count is big."

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    move-object/from16 v19, v0

    if-nez v19, :cond_22

    .line 676
    new-instance v19, Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    add-int/lit8 v20, v15, 0xc

    add-int/lit8 v20, v20, 0x2

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    move/from16 v2, v20

    invoke-direct {v0, v1, v2, v8}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;-><init>(Lcom/sec/android/exifparser/MakerNoteParser;ILjava/nio/ByteOrder;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    .line 681
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    move-object/from16 v19, v0

    if-eqz v19, :cond_9

    .line 682
    add-int/lit8 v19, v15, 0xc

    add-int/lit8 v16, v19, 0x2

    .line 684
    if-ltz v16, :cond_21

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v22

    cmp-long v19, v20, v22

    if-ltz v19, :cond_23

    .line 685
    :cond_21
    const/16 v19, 0x0

    goto/16 :goto_0

    .line 679
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->reset()V

    goto :goto_7

    .line 687
    :cond_23
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    invoke-virtual/range {v19 .. v21}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 688
    mul-int/lit8 v19, v11, 0xc

    invoke-static/range {v19 .. v19}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 689
    .local v5, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 690
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    .line 691
    array-length v0, v6

    move/from16 v19, v0

    mul-int/lit8 v20, v11, 0xc

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    mul-int/lit8 v21, v11, 0xc

    move-object/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v6, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v19

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_9

    .line 693
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->unflatten(Ljava/nio/ByteBuffer;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_3
.end method

.method private IsJpegFormat()Z
    .locals 6

    .prologue
    const/4 v4, 0x2

    const/4 v2, 0x0

    .line 209
    iget-object v3, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-nez v3, :cond_1

    .line 237
    :cond_0
    :goto_0
    return v2

    .line 212
    :cond_1
    const/4 v2, 0x0

    .line 213
    .local v2, "isJpeg":Z
    new-array v0, v4, [B

    .line 216
    .local v0, "buffer":[B
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v4, 0x0

    const/4 v5, 0x2

    invoke-virtual {v3, v0, v4, v5}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 218
    const/16 v3, -0x28

    invoke-direct {p0, v0, v3}, Lcom/sec/android/exifparser/MakerNoteParser;->checkMarker([BB)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    const/4 v2, 0x1

    goto :goto_0

    .line 232
    :catch_0
    move-exception v1

    .line 234
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private checkMarker([BB)Z
    .locals 4
    .param p1, "buffer"    # [B
    .param p2, "marker"    # B

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 241
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 247
    :cond_1
    :goto_0
    return v0

    .line 244
    :cond_2
    aget-byte v2, p1, v1

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    aget-byte v2, p1, v0

    if-eq v2, p2, :cond_1

    :cond_3
    move v0, v1

    .line 247
    goto :goto_0
.end method

.method private reset()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 114
    iput-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    .line 115
    iput-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFilePath:Ljava/lang/String;

    .line 117
    iput v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mApp1Length:I

    .line 118
    iput v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mApp1DataOffset:I

    .line 120
    iput-boolean v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mLittleEndian:Z

    .line 122
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v0}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->reset()V

    .line 125
    :cond_0
    iput-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    .line 126
    iput-boolean v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z

    .line 127
    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 198
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v1}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 205
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/sec/android/exifparser/MakerNoteParser;->reset()V

    .line 206
    return-void

    .line 200
    :catch_0
    move-exception v0

    .line 202
    .local v0, "e":Ljava/io/IOException;
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public getMakerNote(I)I
    .locals 4
    .param p1, "code"    # I

    .prologue
    const/4 v2, 0x0

    .line 251
    const/4 v3, 0x4

    new-array v1, v3, [B

    .line 253
    .local v1, "buffer":[B
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/exifparser/MakerNoteParser;->getMakerNote(I[B)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 254
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 255
    .local v0, "bb":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-nez v3, :cond_1

    .line 261
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    :cond_0
    :goto_0
    return v2

    .line 257
    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    :cond_1
    iget-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v2}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 258
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    goto :goto_0
.end method

.method public getMakerNote(IJ)J
    .locals 4
    .param p1, "code"    # I
    .param p2, "defaultVal"    # J

    .prologue
    .line 265
    const/16 v2, 0x8

    new-array v1, v2, [B

    .line 267
    .local v1, "buffer":[B
    invoke-virtual {p0, p1, v1}, Lcom/sec/android/exifparser/MakerNoteParser;->getMakerNote(I[B)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 268
    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 269
    .local v0, "bb":Ljava/nio/ByteBuffer;
    iget-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-nez v2, :cond_1

    .line 270
    const-wide/16 p2, 0x0

    .line 275
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local p2    # "defaultVal":J
    :cond_0
    :goto_0
    return-wide p2

    .line 271
    .restart local v0    # "bb":Ljava/nio/ByteBuffer;
    .restart local p2    # "defaultVal":J
    :cond_1
    iget-object v2, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v2}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 272
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide p2

    goto :goto_0
.end method

.method public getMakerNote(I[B)Z
    .locals 16
    .param p1, "code"    # I
    .param p2, "outBuf"    # [B

    .prologue
    .line 279
    if-nez p2, :cond_0

    .line 280
    const/4 v11, 0x0

    .line 366
    :goto_0
    return v11

    .line 282
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z

    if-nez v11, :cond_1

    .line 283
    const-string v11, "MakerNoteParser"

    const-string v12, "Not supported code"

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    const/4 v11, 0x0

    goto :goto_0

    .line 289
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-nez v11, :cond_2

    .line 290
    const/4 v11, 0x0

    goto :goto_0

    .line 292
    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    move-result-object v5

    .line 294
    .local v5, "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    if-eqz v5, :cond_8

    .line 295
    invoke-virtual {v5}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getCount()I

    move-result v8

    .line 296
    .local v8, "valueCount":I
    move v10, v8

    .line 298
    .local v10, "valueSizeInBytes":I
    invoke-virtual {v5}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getValue()[B

    move-result-object v3

    .line 299
    .local v3, "buf":[B
    invoke-static {v3}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 300
    .local v2, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v5}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 302
    invoke-static/range {p2 .. p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 303
    .local v4, "outBb":Ljava/nio/ByteBuffer;
    invoke-virtual {v5}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v11

    invoke-virtual {v4, v11}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 305
    array-length v11, v3

    move-object/from16 v0, p2

    array-length v12, v0

    if-le v11, v12, :cond_3

    .line 306
    const/4 v11, 0x0

    goto :goto_0

    .line 308
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getType()I

    move-result v11

    packed-switch v11, :pswitch_data_0

    .line 331
    :pswitch_0
    const/4 v11, 0x0

    goto :goto_0

    .line 314
    :pswitch_1
    mul-int/lit8 v10, v8, 0x2

    .line 334
    :goto_1
    :pswitch_2
    const/4 v11, 0x4

    if-gt v10, v11, :cond_5

    .line 335
    array-length v11, v3

    move-object/from16 v0, p2

    array-length v12, v0

    if-ge v11, v12, :cond_4

    .line 336
    const-string v11, "MakerNoteParser"

    const-string v12, "Output buffer is larger than input buffer."

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    :cond_4
    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 340
    const/4 v11, 0x1

    goto :goto_0

    .line 317
    :pswitch_3
    mul-int/lit8 v10, v8, 0x4

    .line 318
    goto :goto_1

    .line 320
    :pswitch_4
    mul-int/lit8 v10, v8, 0x8

    .line 321
    goto :goto_1

    .line 325
    :pswitch_5
    mul-int/lit8 v10, v8, 0x4

    .line 326
    goto :goto_1

    .line 328
    :pswitch_6
    mul-int/lit8 v10, v8, 0x8

    .line 329
    goto :goto_1

    .line 342
    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v11, :cond_8

    .line 343
    move-object/from16 v0, p2

    array-length v11, v0

    if-ge v11, v10, :cond_6

    .line 344
    const-string v11, "MakerNoteParser"

    const-string v12, "Output buffer is short."

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 346
    :cond_6
    move-object/from16 v0, p2

    array-length v11, v0

    if-le v11, v10, :cond_7

    .line 347
    const-string v11, "MakerNoteParser"

    const-string v12, "Output buffer is larger than input buffer."

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_7
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v9

    .line 352
    .local v9, "valueOffset":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v11}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->getOffset()J

    move-result-wide v12

    const-wide/16 v14, 0x2

    sub-long/2addr v12, v14

    int-to-long v14, v9

    add-long v6, v12, v14

    .line 354
    .local v6, "seekPos":J
    const-wide/16 v12, 0x0

    cmp-long v11, v6, v12

    if-ltz v11, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v11}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v12

    cmp-long v11, v6, v12

    if-gez v11, :cond_8

    .line 355
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v11, v6, v7}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 356
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v12, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v11, v0, v12, v10}, Ljava/io/RandomAccessFile;->read([BII)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v11

    const/4 v12, -0x1

    if-eq v11, v12, :cond_8

    .line 357
    const/4 v11, 0x1

    goto/16 :goto_0

    .line 360
    .end local v6    # "seekPos":J
    :catch_0
    move-exception v11

    .line 366
    .end local v2    # "bb":Ljava/nio/ByteBuffer;
    .end local v3    # "buf":[B
    .end local v4    # "outBb":Ljava/nio/ByteBuffer;
    .end local v8    # "valueCount":I
    .end local v9    # "valueOffset":I
    .end local v10    # "valueSizeInBytes":I
    :cond_8
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 308
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public getMakerNoteTag(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    .locals 3
    .param p1, "code"    # I

    .prologue
    const/4 v0, 0x0

    .line 370
    iget-boolean v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z

    if-nez v1, :cond_1

    .line 371
    const-string v1, "MakerNoteParser"

    const-string v2, "Not supported code"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_0
    :goto_0
    return-object v0

    .line 377
    :cond_1
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-eqz v1, :cond_0

    .line 380
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v0, p1}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    move-result-object v0

    goto :goto_0
.end method

.method public isSupportedVersion([B)Z
    .locals 7
    .param p1, "version"    # [B

    .prologue
    const/16 v4, 0x30

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 130
    if-eqz p1, :cond_0

    array-length v3, p1

    if-nez v3, :cond_1

    .line 149
    :cond_0
    :goto_0
    return v1

    .line 133
    :cond_1
    const/4 v3, 0x4

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 134
    .local v0, "bb":Ljava/nio/ByteBuffer;
    iget-object v3, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-eqz v3, :cond_0

    .line 136
    iget-object v3, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v3}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 138
    invoke-virtual {v0, v1, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 139
    const/16 v3, 0x31

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 140
    invoke-virtual {v0, v5, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 141
    invoke-virtual {v0, v6, v4}, Ljava/nio/ByteBuffer;->put(IB)Ljava/nio/ByteBuffer;

    .line 143
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 145
    aget-byte v3, p1, v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    aget-byte v3, p1, v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    aget-byte v3, p1, v5

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    aget-byte v3, p1, v6

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-ne v3, v4, :cond_0

    move v1, v2

    .line 147
    goto :goto_0
.end method

.method public open(Ljava/lang/String;)Z
    .locals 6
    .param p1, "filename"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 153
    const/4 v2, 0x0

    .line 156
    .local v2, "isOpened":Z
    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 158
    .local v1, "file":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v5

    if-nez v5, :cond_0

    .line 193
    .end local v1    # "file":Ljava/io/File;
    :goto_0
    return v4

    .line 161
    .restart local v1    # "file":Ljava/io/File;
    :cond_0
    iput-object p1, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFilePath:Ljava/lang/String;

    .line 163
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v5, "rw"

    invoke-direct {v4, p1, v5}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    .line 165
    iget-object v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v4, :cond_2

    .line 166
    invoke-direct {p0}, Lcom/sec/android/exifparser/MakerNoteParser;->IsJpegFormat()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 167
    invoke-direct {p0}, Lcom/sec/android/exifparser/MakerNoteParser;->IsExifFormat()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 168
    const/4 v3, 0x0

    .line 170
    .local v3, "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    iget-object v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-eqz v4, :cond_1

    .line 171
    iget-object v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    move-result-object v3

    .line 174
    :cond_1
    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getValue()[B

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/android/exifparser/MakerNoteParser;->isSupportedVersion([B)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 175
    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    const/4 v2, 0x1

    .line 189
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    :cond_2
    :goto_1
    if-nez v2, :cond_3

    .line 190
    invoke-virtual {p0}, Lcom/sec/android/exifparser/MakerNoteParser;->close()V

    :cond_3
    move v4, v2

    .line 193
    goto :goto_0

    .line 178
    .restart local v1    # "file":Ljava/io/File;
    .restart local v3    # "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    :cond_4
    const/4 v4, 0x0

    :try_start_1
    iput-boolean v4, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    .line 179
    const/4 v2, 0x0

    goto :goto_1

    .line 184
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    :catch_0
    move-exception v0

    .line 186
    .local v0, "e":Ljava/io/FileNotFoundException;
    const-string v4, "MakerNoteParser"

    const-string v5, "File is not founded."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setMakerNote(II)V
    .locals 9
    .param p1, "code"    # I
    .param p2, "value"    # I

    .prologue
    const/4 v7, 0x4

    .line 384
    iget-boolean v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mSupported:Z

    if-nez v6, :cond_1

    .line 385
    const-string v6, "MakerNoteParser"

    const-string v7, "Not supported code"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    if-eqz v6, :cond_0

    .line 394
    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mMakerNoteTags:Lcom/sec/android/exifparser/MakerNoteParser$IFD;

    invoke-virtual {v6, p1}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->get(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    move-result-object v3

    .line 396
    .local v3, "tag":Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    if-eqz v3, :cond_0

    .line 397
    invoke-virtual {v3}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getType()I

    move-result v6

    if-eq v6, v7, :cond_2

    .line 398
    const-string v6, "MakerNoteParser"

    const-string v7, "Not supported types."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 402
    :cond_2
    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    if-eqz v6, :cond_0

    .line 404
    :try_start_0
    invoke-virtual {v3}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getOffset()J

    move-result-wide v4

    .line 405
    .local v4, "seekPos":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->length()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    .line 406
    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v6, v4, v5}, Ljava/io/RandomAccessFile;->seek(J)V

    .line 408
    const/4 v6, 0x4

    invoke-static {v6}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 409
    .local v0, "bb":Ljava/nio/ByteBuffer;
    invoke-virtual {v3}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getByteOrder()Ljava/nio/ByteOrder;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 410
    invoke-virtual {v0, p2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 411
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    .line 412
    .local v1, "buffer":[B
    iget-object v6, p0, Lcom/sec/android/exifparser/MakerNoteParser;->mFile:Ljava/io/RandomAccessFile;

    const/4 v7, 0x0

    const/4 v8, 0x4

    invoke-virtual {v6, v1, v7, v8}, Ljava/io/RandomAccessFile;->write([BII)V

    .line 414
    invoke-virtual {v3, v1}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->putValue([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 416
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    .end local v1    # "buffer":[B
    .end local v4    # "seekPos":J
    :catch_0
    move-exception v2

    .line 418
    .local v2, "e":Ljava/io/IOException;
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method
