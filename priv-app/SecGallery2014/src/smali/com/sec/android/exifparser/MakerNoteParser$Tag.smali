.class public Lcom/sec/android/exifparser/MakerNoteParser$Tag;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/exifparser/MakerNoteParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "Tag"
.end annotation


# instance fields
.field private mByteOrder:Ljava/nio/ByteOrder;

.field private mCode:I

.field private mCount:I

.field private mOffset:J

.field private mType:I

.field private mValue:[B

.field final synthetic this$0:Lcom/sec/android/exifparser/MakerNoteParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/exifparser/MakerNoteParser;III[BJLjava/nio/ByteOrder;)V
    .locals 2
    .param p2, "code"    # I
    .param p3, "type"    # I
    .param p4, "count"    # I
    .param p5, "value"    # [B
    .param p6, "offset"    # J
    .param p8, "byteOrder"    # Ljava/nio/ByteOrder;

    .prologue
    .line 815
    iput-object p1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->this$0:Lcom/sec/android/exifparser/MakerNoteParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 811
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    .line 816
    iput p2, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mCode:I

    .line 817
    iput p3, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mType:I

    .line 818
    iput p4, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mCount:I

    .line 819
    iput-wide p6, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mOffset:J

    .line 820
    iput-object p8, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mByteOrder:Ljava/nio/ByteOrder;

    .line 822
    if-eqz p5, :cond_0

    array-length v1, p5

    if-lez v1, :cond_0

    .line 823
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 825
    .local v0, "bb":Ljava/nio/ByteBuffer;
    if-eqz v0, :cond_0

    .line 826
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 828
    invoke-virtual {v0, p5}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 829
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    .line 832
    .end local v0    # "bb":Ljava/nio/ByteBuffer;
    :cond_0
    return-void
.end method


# virtual methods
.method public getByteOrder()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mByteOrder:Ljava/nio/ByteOrder;

    return-object v0
.end method

.method public getCode()I
    .locals 1

    .prologue
    .line 835
    iget v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mCode:I

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 843
    iget v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mCount:I

    return v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 851
    iget-wide v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mOffset:J

    return-wide v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 839
    iget v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mType:I

    return v0
.end method

.method public getValue()[B
    .locals 1

    .prologue
    .line 847
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    return-object v0
.end method

.method public putValue([B)V
    .locals 3
    .param p1, "value"    # [B

    .prologue
    .line 859
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    array-length v1, v1

    array-length v2, p1

    if-lt v1, v2, :cond_0

    .line 860
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mValue:[B

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 861
    .local v0, "destB":Ljava/nio/ByteBuffer;
    iget-object v1, p0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 862
    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 864
    .end local v0    # "destB":Ljava/nio/ByteBuffer;
    :cond_0
    return-void
.end method
