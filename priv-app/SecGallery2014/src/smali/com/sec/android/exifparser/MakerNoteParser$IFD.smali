.class Lcom/sec/android/exifparser/MakerNoteParser$IFD;
.super Ljava/lang/Object;
.source "MakerNoteParser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/exifparser/MakerNoteParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IFD"
.end annotation


# instance fields
.field private mAttributes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/sec/android/exifparser/MakerNoteParser$Tag;",
            ">;"
        }
    .end annotation
.end field

.field private mByteOrder:Ljava/nio/ByteOrder;

.field private mEntryCount:I

.field private mIfdOffset:J

.field final synthetic this$0:Lcom/sec/android/exifparser/MakerNoteParser;


# direct methods
.method public constructor <init>(Lcom/sec/android/exifparser/MakerNoteParser;ILjava/nio/ByteOrder;)V
    .locals 2
    .param p2, "ifdOffset"    # I
    .param p3, "byteOrder"    # Ljava/nio/ByteOrder;

    .prologue
    .line 718
    iput-object p1, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->this$0:Lcom/sec/android/exifparser/MakerNoteParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 719
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    .line 720
    int-to-long v0, p2

    iput-wide v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mIfdOffset:J

    .line 721
    iput-object p3, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    .line 722
    return-void
.end method


# virtual methods
.method public get(I)Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 742
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    new-instance v1, Ljava/lang/Integer;

    invoke-direct {v1, p1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    .line 745
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getByteOrder()Ljava/nio/ByteOrder;
    .locals 1

    .prologue
    .line 725
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    return-object v0
.end method

.method public getEntryCount()I
    .locals 1

    .prologue
    .line 729
    iget v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mEntryCount:I

    return v0
.end method

.method public getOffset()J
    .locals 2

    .prologue
    .line 738
    iget-wide v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mIfdOffset:J

    return-wide v0
.end method

.method public put(Lcom/sec/android/exifparser/MakerNoteParser$Tag;)Lcom/sec/android/exifparser/MakerNoteParser$Tag;
    .locals 2
    .param p1, "tag"    # Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    .prologue
    .line 749
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 750
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;->getCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    .line 752
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 733
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mAttributes:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 735
    :cond_0
    return-void
.end method

.method public unflatten(Ljava/nio/ByteBuffer;)V
    .locals 18
    .param p1, "bb"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 756
    if-eqz p1, :cond_1

    .line 757
    const/4 v13, 0x0

    .line 758
    .local v13, "offset":I
    const/4 v2, 0x4

    new-array v7, v2, [B

    .line 761
    .local v7, "value":[B
    const/4 v14, 0x0

    .line 762
    .local v14, "tagCount":I
    const/4 v4, 0x0

    .line 763
    .local v4, "code":I
    const/4 v5, 0x0

    .line 764
    .local v5, "type":I
    const/4 v6, 0x0

    .line 766
    .local v6, "count":I
    const/4 v12, 0x0

    .line 768
    .local v12, "endianOffset":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    sget-object v3, Ljava/nio/ByteOrder;->BIG_ENDIAN:Ljava/nio/ByteOrder;

    if-ne v2, v3, :cond_0

    .line 769
    const/4 v12, 0x2

    .line 772
    :cond_0
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    const/16 v3, 0xc

    if-lt v2, v3, :cond_1

    const/16 v2, 0x32

    if-ge v14, v2, :cond_1

    .line 773
    const/4 v13, 0x0

    .line 775
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 776
    .local v11, "b":Ljava/nio/ByteBuffer;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 777
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 778
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v12, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 779
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 780
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 781
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 783
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 784
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 785
    const/4 v2, 0x0

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 786
    const/4 v2, 0x2

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v12, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 787
    invoke-static {v7}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 789
    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    .line 791
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    .line 792
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v13

    .line 794
    const/4 v2, 0x0

    const/4 v3, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v2, v3}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 797
    new-instance v2, Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->this$0:Lcom/sec/android/exifparser/MakerNoteParser;

    int-to-long v8, v13

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mIfdOffset:J

    move-wide/from16 v16, v0

    add-long v8, v8, v16

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->mByteOrder:Ljava/nio/ByteOrder;

    invoke-direct/range {v2 .. v10}, Lcom/sec/android/exifparser/MakerNoteParser$Tag;-><init>(Lcom/sec/android/exifparser/MakerNoteParser;III[BJLjava/nio/ByteOrder;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/android/exifparser/MakerNoteParser$IFD;->put(Lcom/sec/android/exifparser/MakerNoteParser$Tag;)Lcom/sec/android/exifparser/MakerNoteParser$Tag;

    .line 798
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 801
    .end local v4    # "code":I
    .end local v5    # "type":I
    .end local v6    # "count":I
    .end local v7    # "value":[B
    .end local v11    # "b":Ljava/nio/ByteBuffer;
    .end local v12    # "endianOffset":I
    .end local v13    # "offset":I
    .end local v14    # "tagCount":I
    :cond_1
    return-void
.end method
