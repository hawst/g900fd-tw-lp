.class public final Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;
.super Ljava/lang/Object;
.source "CloudIntegratorApi.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/cloud/integrator/CloudIntegratorApi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "API"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static canShowDropboxLanding(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 38
    # invokes: Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->access$000(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 39
    const/4 v8, -0x1

    .line 56
    :cond_0
    :goto_0
    return v8

    .line 42
    :cond_1
    const/4 v8, -0x1

    .line 43
    .local v8, "enabled":I
    const-string v6, "can_show"

    .line 44
    .local v6, "CAN_SHOW":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "can_show"

    aput-object v4, v2, v1

    .line 45
    .local v2, "proj":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 46
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "content://com.dropbox.android.provider.SDK/can_show_gallery_popup"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 49
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 50
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 51
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 52
    const-string v1, "can_show"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 54
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static isDealDevice(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 133
    # invokes: Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->access$000(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 134
    const/4 v8, -0x1

    .line 150
    :cond_0
    :goto_0
    return v8

    .line 137
    :cond_1
    const/4 v8, -0x1

    .line 138
    .local v8, "result":I
    const-string v6, "is_deal_device"

    .line 139
    .local v6, "COL_NAME":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "is_deal_device"

    aput-object v4, v2, v1

    .line 140
    .local v2, "proj":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 141
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "content://com.dropbox.android.provider.SDK/is_samsung_docs_and_gallery_deal_device"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 143
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 144
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 145
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 146
    const-string v1, "is_deal_device"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 148
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static isUserAddedtoDropbox(Landroid/content/Context;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 68
    # invokes: Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->access$000(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 69
    const/4 v8, -0x1

    .line 86
    :cond_0
    :goto_0
    return v8

    .line 72
    :cond_1
    const/4 v8, -0x1

    .line 73
    .local v8, "enabled":I
    const-string v6, "logged_in"

    .line 74
    .local v6, "LOGGED_IN":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "logged_in"

    aput-object v4, v2, v1

    .line 75
    .local v2, "proj":[Ljava/lang/String;
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 76
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v1, "content://com.dropbox.android.provider.SDK/is_any_user_logged_in"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 79
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 80
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    .line 81
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 82
    const-string v1, "logged_in"

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    .line 84
    :cond_2
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static launchDocViewerLandingPageActivity(Landroid/content/Context;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "documentText"    # Ljava/lang/String;
    .param p2, "documentUri"    # Landroid/net/Uri;

    .prologue
    .line 113
    # invokes: Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->access$000(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 115
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 116
    const-string v1, "launchIntent"

    const-string v2, "com.dropbox.android.intent.action.DOCS_LANDING_PAGE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    const-string v1, "docUri"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 118
    const-string v1, "docText"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 121
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method public static launchGalleryLandingPageActivity(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 96
    # invokes: Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->access$000(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 97
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 98
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 99
    const-string v1, "launchIntent"

    const-string v2, "com.dropbox.android.intent.action.GALLERY_LANDING_PAGE"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 102
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
