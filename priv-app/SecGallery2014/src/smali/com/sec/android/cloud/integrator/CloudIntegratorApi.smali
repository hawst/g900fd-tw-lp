.class public Lcom/sec/android/cloud/integrator/CloudIntegratorApi;
.super Ljava/lang/Object;
.source "CloudIntegratorApi.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/cloud/integrator/CloudIntegratorApi$API;
    }
.end annotation


# static fields
.field public static final DOC_VIEWER_LAUNCH_PAGE_INTENT:Ljava/lang/String; = "com.dropbox.android.intent.action.DOCS_LANDING_PAGE"

.field static final EXTRA_DOC_TEXT:Ljava/lang/String; = "docText"

.field static final EXTRA_DOC_URI:Ljava/lang/String; = "docUri"

.field public static final GALLERY_LAUNCH_PAGE_INTENT:Ljava/lang/String; = "com.dropbox.android.intent.action.GALLERY_LANDING_PAGE"

.field static final LAUNCH_INTENT:Ljava/lang/String; = "launchIntent"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Landroid/content/Context;

    .prologue
    .line 14
    invoke-static {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorApi;->checkDropbox(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private static checkDropbox(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 155
    const/4 v1, 0x0

    .line 157
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.dropbox.android"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 161
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 158
    :catch_0
    move-exception v0

    .line 159
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 161
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method
