.class public Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;
.super Landroid/app/Activity;
.source "CloudIntegratorActivity.java"


# static fields
.field public static final ACTION_LAUNCH:I = 0x1

.field public static final TAG:Ljava/lang/String; = "CloudIntegrator"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private checkCloudAgent()Z
    .locals 5

    .prologue
    .line 66
    const/4 v1, 0x0

    .line 68
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.android.cloudagent"

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 72
    :goto_0
    if-eqz v1, :cond_0

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 69
    :catch_0
    move-exception v0

    .line 70
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 72
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 52
    const-string v1, "CloudIntegrator"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onActivityResult : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    const/4 v1, 0x1

    if-ne p1, v1, :cond_0

    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 54
    invoke-direct {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->checkCloudAgent()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "CloudIntegrator"

    const-string v2, "onActivityResult:Sending Broadcast to CA!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.cloudagent.ACTION_REQUEST_SIGNIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 57
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "packageName"

    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    invoke-virtual {p0, v0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 61
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->finish()V

    .line 62
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 20
    const-string v4, "CloudIntegrator"

    const-string v5, "CloudIntegratorActivity::onCreate"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 22
    const/4 v0, 0x0

    .line 23
    .local v0, "action":Ljava/lang/String;
    const/4 v2, 0x0

    .line 24
    .local v2, "documentUri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 25
    .local v1, "documentText":Ljava/lang/String;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 27
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 28
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "launchIntent"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 29
    const-string v4, "CloudIntegrator"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "CloudIntegratorActivity: Launch intent is:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    if-eqz v0, :cond_0

    const-string v4, "com.dropbox.android.intent.action.GALLERY_LANDING_PAGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "com.dropbox.android.intent.action.DOCS_LANDING_PAGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 33
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->finish()V

    .line 36
    :cond_1
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 37
    const-string v4, "com.dropbox.android.intent.action.DOCS_LANDING_PAGE"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 38
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "docUri"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    .end local v2    # "documentUri":Landroid/net/Uri;
    check-cast v2, Landroid/net/Uri;

    .line 39
    .restart local v2    # "documentUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "docText"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 41
    const-string v4, "android.intent.extra.TEXT"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 42
    const-string v4, "android.intent.extra.STREAM"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 45
    :cond_2
    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/cloud/integrator/CloudIntegratorActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 47
    :cond_3
    return-void
.end method
