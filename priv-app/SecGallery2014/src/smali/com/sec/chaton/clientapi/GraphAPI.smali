.class public Lcom/sec/chaton/clientapi/GraphAPI;
.super Ljava/lang/Object;
.source "GraphAPI.java"


# static fields
.field protected static final ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

.field public static final BUDDY_NAME:Ljava/lang/String; = "buddy_name"

.field public static final BUDDY_NO:Ljava/lang/String; = "buddy_no"

.field public static final BUDDY_STATUS_MESSAGE:Ljava/lang/String; = "buddy_status_message"

.field protected static final BUDDY_URI:Landroid/net/Uri;

.field protected static final CHATON_ACTION_ADD_BUDDY:Ljava/lang/String; = "com.sec.chaton.action.ADD_BUDDY"

.field protected static final DATA_PREFIX:Ljava/lang/String; = "chaton://"

.field protected static final EXTRA_KEY_EXCEPT:Ljava/lang/String; = "except"

.field protected static final EXTRA_KEY_ID:Ljava/lang/String; = "id"

.field protected static final EXTRA_KEY_MAX_COUNT:Ljava/lang/String; = "max"

.field protected static final EXTRA_KEY_REQUIRE:Ljava/lang/String; = "require"

.field protected static final EXTRA_KEY_SINGLE:Ljava/lang/String; = "single"

.field public static final ME_COUNTRY_ISO_CODE:Ljava/lang/String; = "country_iso_code"

.field public static final ME_COUNTRY_NUM:Ljava/lang/String; = "country_num"

.field public static final ME_ID:Ljava/lang/String; = "id"

.field public static final ME_NAME:Ljava/lang/String; = "name"

.field protected static final ME_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "content://com.sec.chaton.access_token.provider"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/clientapi/GraphAPI;->ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

    .line 36
    const-string v0, "content://com.sec.chaton.provider/buddy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/clientapi/GraphAPI;->BUDDY_URI:Landroid/net/Uri;

    .line 87
    const-string v0, "content://com.sec.chaton.provider/me"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/chaton/clientapi/GraphAPI;->ME_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addBuddy(Landroid/content/Context;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.7.7"
        versionCode = 0x1a0c05
    .end annotation

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 136
    const-class v4, Lcom/sec/chaton/clientapi/GraphAPI;

    const-string v5, "addBuddy"

    new-array v6, v2, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    const-class v7, Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-static {p0, v4, v5, v6}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 161
    :goto_0
    return v2

    .line 139
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v4, 0x14

    if-le v2, v4, :cond_2

    .line 140
    :cond_1
    const/4 v2, 0x7

    goto :goto_0

    .line 144
    :cond_2
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.chaton.action.ADD_BUDDY"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 146
    .local v1, "i":Landroid/content/Intent;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 147
    const-string v2, "id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 148
    invoke-static {p0, v1}, Lcom/sec/chaton/clientapi/GraphAPI;->addPassword(Landroid/content/Context;Landroid/content/Intent;)V

    .line 151
    :cond_3
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move v2, v3

    .line 153
    goto :goto_0

    .line 155
    .end local v1    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 157
    const/4 v2, 0x4

    goto :goto_0

    .line 159
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :catch_1
    move-exception v0

    .line 160
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 161
    const/4 v2, 0x3

    goto :goto_0
.end method

.method protected static addPassword(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 32
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/sec/chaton/clientapi/GraphAPI;->ACCESS_TOKEN_PROVIDER_URI:Landroid/net/Uri;

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 33
    .local v0, "returnUri":Landroid/net/Uri;
    const-string v1, "password"

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 34
    return-void
.end method

.method public static getBuddyList(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "selectionArgs"    # [Ljava/lang/String;
    .param p3, "sortOrder"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.6.3"
        versionCode = 0x17f
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 44
    const-class v0, Lcom/sec/chaton/clientapi/GraphAPI;

    const-string v1, "getBuddyList"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Class;

    const-class v3, Landroid/content/Context;

    aput-object v3, v2, v4

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v5

    const-class v3, [Ljava/lang/String;

    aput-object v3, v2, v6

    const-class v3, Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-static {p0, v0, v1, v2}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;

    const-string v1, "API isn\'t availble. please check your ChatON version."

    invoke-direct {v0, v1}, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/clientapi/GraphAPI;->BUDDY_URI:Landroid/net/Uri;

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "buddy_no"

    aput-object v3, v2, v4

    const-string v3, "buddy_name"

    aput-object v3, v2, v5

    const-string v3, "buddy_status_message"

    aput-object v3, v2, v6

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static getBuddyProfileImage(Landroid/content/Context;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "buddyNo"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.6.3"
        versionCode = 0x17f
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/chaton/clientapi/exception/IllegalArgumentClientAPIException;,
            Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;
        }
    .end annotation

    .prologue
    .line 55
    const-class v3, Lcom/sec/chaton/clientapi/GraphAPI;

    const-string v4, "getBuddyProfileImage"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {p0, v3, v4, v5}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 56
    new-instance v3, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;

    const-string v4, "API isn\'t availble. please check your ChatON version."

    invoke-direct {v3, v4}, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 58
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x14

    if-le v3, v4, :cond_2

    .line 59
    :cond_1
    new-instance v3, Lcom/sec/chaton/clientapi/exception/IllegalArgumentClientAPIException;

    const-string v4, "buddyNo\'s length must be greater than or equals to 1 and be less than or equals to 20"

    invoke-direct {v3, v4}, Lcom/sec/chaton/clientapi/exception/IllegalArgumentClientAPIException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 62
    :cond_2
    const/4 v0, 0x0

    .line 63
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    const/4 v2, 0x0

    .line 65
    .local v2, "pfdInput":Landroid/os/ParcelFileDescriptor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/clientapi/GraphAPI;->BUDDY_URI:Landroid/net/Uri;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "buddy_no/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "r"

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 66
    if-eqz v2, :cond_3

    .line 67
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 75
    :cond_3
    if-eqz v2, :cond_4

    .line 77
    :try_start_1
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 84
    :cond_4
    :goto_0
    return-object v0

    .line 78
    :catch_0
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 70
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 72
    .local v1, "e":Ljava/io/FileNotFoundException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    if-eqz v2, :cond_4

    .line 77
    :try_start_3
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 78
    :catch_2
    move-exception v1

    .line 80
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 75
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_5

    .line 77
    :try_start_4
    invoke-virtual {v2}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 81
    :cond_5
    :goto_1
    throw v3

    .line 78
    :catch_3
    move-exception v1

    .line 80
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static getMyProfile(Landroid/content/Context;)Landroid/database/Cursor;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.7.7"
        versionCode = 0x1a0c05
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;
        }
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 96
    const-class v0, Lcom/sec/chaton/clientapi/GraphAPI;

    const-string v1, "getMyProfile"

    new-array v2, v6, [Ljava/lang/Class;

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v5

    invoke-static {p0, v0, v1, v2}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;

    const-string v1, "API isn\'t availble. please check your ChatON version."

    invoke-direct {v0, v1}, Lcom/sec/chaton/clientapi/exception/NotAvailableClientAPIException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/chaton/clientapi/GraphAPI;->ME_URI:Landroid/net/Uri;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v4, "id"

    aput-object v4, v2, v5

    const-string v4, "name"

    aput-object v4, v2, v6

    const/4 v4, 0x2

    const-string v5, "country_num"

    aput-object v5, v2, v4

    const/4 v4, 0x3

    const-string v5, "country_iso_code"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method static pickBuddy(Landroid/content/Context;Z[Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "single"    # Z
    .param p2, "exceptBuddies"    # [Ljava/lang/String;
    .param p3, "require"    # Ljava/lang/String;
    .param p4, "maxCount"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    const-string v2, "chaton://"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 114
    .local v0, "i":Landroid/content/Intent;
    if-lez p4, :cond_0

    .line 115
    const-string v1, "max"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 118
    :cond_0
    const-string/jumbo v1, "single"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 120
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 121
    const-string v1, "require"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    :cond_1
    if-eqz p2, :cond_2

    array-length v1, p2

    if-lez v1, :cond_2

    .line 125
    const-string v1, "except"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    :cond_2
    return-object v0
.end method
