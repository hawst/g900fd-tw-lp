.class public Lcom/sec/chaton/clientapi/MessageAPI;
.super Ljava/lang/Object;
.source "MessageAPI.java"


# static fields
.field protected static final MAX_LENGTH_TEXT:I = 0x7d0

.field protected static final MAX_SIZE_IMAGE_VIDEO:J = 0xa00000L


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static CheckMuliMediaValidation(Landroid/content/Context;Landroid/net/Uri;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/16 v11, 0x9

    .line 244
    const/4 v9, 0x0

    .line 245
    .local v9, "fileName":Ljava/lang/String;
    const-string v0, "content"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 246
    const/4 v10, 0x0

    .line 248
    .local v10, "oCursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 249
    const/4 v6, -0x1

    .line 250
    .local v6, "columnIndex":I
    if-eqz v10, :cond_2

    .line 251
    :cond_0
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    const-string v0, "_data"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    .line 253
    const/4 v0, -0x1

    if-eq v6, v0, :cond_0

    .line 254
    invoke-interface {v10, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    goto :goto_0

    .line 257
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 277
    .end local v6    # "columnIndex":I
    .end local v10    # "oCursor":Landroid/database/Cursor;
    :cond_2
    :goto_1
    new-instance v8, Ljava/io/File;

    invoke-direct {v8, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    .local v8, "file":Ljava/io/File;
    if-eqz v8, :cond_3

    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    :cond_3
    move v0, v11

    .line 287
    .end local v8    # "file":Ljava/io/File;
    :goto_2
    return v0

    .line 260
    .restart local v10    # "oCursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 261
    .local v7, "e":Ljava/lang/SecurityException;
    invoke-virtual {v7}, Ljava/lang/SecurityException;->printStackTrace()V

    .line 262
    const/4 v0, 0x3

    goto :goto_2

    .line 265
    .end local v7    # "e":Ljava/lang/SecurityException;
    .end local v10    # "oCursor":Landroid/database/Cursor;
    :cond_4
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 270
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    :cond_5
    move v0, v11

    .line 274
    goto :goto_2

    .line 282
    .restart local v8    # "file":Ljava/io/File;
    :cond_6
    invoke-virtual {v8}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/32 v2, 0xa00000

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    .line 283
    const/16 v0, 0x8

    goto :goto_2

    .line 285
    :cond_7
    const/4 v8, 0x0

    .line 287
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public static openChatRoom(Landroid/content/Context;[Ljava/lang/String;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipients"    # [Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 35
    const-class v5, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v6, "openChatRoom"

    new-array v7, v3, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    const-class v8, [Ljava/lang/String;

    aput-object v8, v7, v4

    invoke-static {p0, v5, v6, v7}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 64
    :goto_0
    return v3

    .line 38
    :cond_0
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 39
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v3, "open"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRecipients([Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 44
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 46
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 48
    goto :goto_0

    .line 50
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 51
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 52
    const/4 v3, 0x5

    goto :goto_0

    .line 54
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 55
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 56
    const/4 v3, 0x4

    goto :goto_0

    .line 58
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 59
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 60
    const/4 v3, 0x7

    goto :goto_0

    .line 62
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 63
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 64
    const/4 v3, 0x3

    goto :goto_0
.end method

.method public static sendAppLinkMessage(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "appVersion"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    .line 478
    .local p4, "appParamInfo":Ljava/util/List;, "Ljava/util/List<Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;>;"
    const-class v3, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v4, "sendAppLinkMessage"

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/content/Context;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const-class v7, Ljava/util/List;

    aput-object v7, v5, v6

    invoke-static {p0, v3, v4, v5}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 479
    const/4 v3, 0x2

    .line 516
    :goto_0
    return v3

    .line 481
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x7d0

    if-le v3, v4, :cond_1

    .line 482
    const/4 v3, 0x6

    goto :goto_0

    .line 485
    :cond_1
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 486
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v3, "send"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAppName(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->app:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v3, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAppVer(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAppParamInfo(Ljava/util/List;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v4, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->text:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    invoke-virtual {v3, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 496
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 498
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 500
    const/4 v3, 0x1

    goto :goto_0

    .line 502
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 503
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 504
    const/4 v3, 0x5

    goto :goto_0

    .line 506
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 507
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 508
    const/4 v3, 0x4

    goto :goto_0

    .line 510
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 511
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 512
    const/4 v3, 0x7

    goto :goto_0

    .line 514
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 515
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 516
    const/4 v3, 0x3

    goto :goto_0
.end method

.method public static sendMultiMediaMessage(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v5, 0x7

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 292
    const-class v7, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v8, "sendMultiMediaMessage"

    new-array v9, v6, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Landroid/content/Context;

    aput-object v11, v9, v10

    const-class v10, Landroid/net/Uri;

    aput-object v10, v9, v4

    const-class v10, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    aput-object v10, v9, v3

    invoke-static {p0, v7, v8, v9}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 332
    :cond_0
    :goto_0
    return v3

    .line 295
    :cond_1
    if-eqz p1, :cond_2

    sget-object v7, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->image:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    if-eq p2, v7, :cond_3

    sget-object v7, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->video:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    if-eq p2, v7, :cond_3

    :cond_2
    move v3, v5

    .line 296
    goto :goto_0

    .line 299
    :cond_3
    invoke-static {p0, p1}, Lcom/sec/chaton/clientapi/MessageAPI;->CheckMuliMediaValidation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 300
    .local v3, "nCheckResult":I
    if-ne v3, v4, :cond_0

    .line 304
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 305
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v7, "send"

    invoke-virtual {v0, v7}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->multimedia:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 312
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 314
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 316
    goto :goto_0

    .line 318
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 319
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 320
    const/4 v3, 0x5

    goto :goto_0

    .line 322
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 323
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 324
    const/4 v3, 0x4

    goto :goto_0

    .line 326
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 327
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    move v3, v5

    .line 328
    goto :goto_0

    .line 330
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 331
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v6

    .line 332
    goto :goto_0
.end method

.method public static sendMultiMediaMessage(Landroid/content/Context;[Ljava/lang/String;Landroid/net/Uri;Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipients"    # [Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mimeType"    # Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 338
    const-class v7, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v8, "sendMultiMediaMessage"

    new-array v9, v5, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Landroid/content/Context;

    aput-object v11, v9, v10

    const-class v10, [Ljava/lang/String;

    aput-object v10, v9, v4

    const-class v10, Landroid/net/Uri;

    aput-object v10, v9, v3

    const-class v10, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    aput-object v10, v9, v6

    invoke-static {p0, v7, v8, v9}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 376
    :cond_0
    :goto_0
    return v3

    .line 341
    :cond_1
    invoke-static {p0, p2}, Lcom/sec/chaton/clientapi/MessageAPI;->CheckMuliMediaValidation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 342
    .local v3, "nCheckResult":I
    if-ne v3, v4, :cond_0

    .line 346
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 347
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v7, "send"

    invoke-virtual {v0, v7}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRecipients([Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->multimedia:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRequiredAccessToken(Z)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 356
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 358
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 360
    goto :goto_0

    .line 362
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 363
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 364
    const/4 v3, 0x5

    goto :goto_0

    .line 366
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 367
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    move v3, v5

    .line 368
    goto :goto_0

    .line 370
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 371
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 372
    const/4 v3, 0x7

    goto :goto_0

    .line 374
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 375
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v6

    .line 376
    goto :goto_0
.end method

.method public static sendMultiMediaMessageWithText(Landroid/content/Context;Landroid/net/Uri;Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;Ljava/lang/String;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mimeType"    # Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;
    .param p3, "text"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 382
    const-class v7, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v8, "sendMultiMediaMessageWithText"

    new-array v9, v5, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Landroid/content/Context;

    aput-object v11, v9, v10

    const-class v10, Landroid/net/Uri;

    aput-object v10, v9, v4

    const-class v10, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    aput-object v10, v9, v3

    const-class v10, Ljava/lang/String;

    aput-object v10, v9, v6

    invoke-static {p0, v7, v8, v9}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 423
    :cond_0
    :goto_0
    return v3

    .line 385
    :cond_1
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x7d0

    if-le v7, v8, :cond_2

    .line 386
    const/4 v3, 0x6

    goto :goto_0

    .line 389
    :cond_2
    invoke-static {p0, p1}, Lcom/sec/chaton/clientapi/MessageAPI;->CheckMuliMediaValidation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 390
    .local v3, "nCheckResult":I
    if-ne v3, v4, :cond_0

    .line 394
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 395
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v7, "send"

    invoke-virtual {v0, v7}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    sget-object v8, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->multimedia_text:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v7

    invoke-virtual {v7, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 403
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 405
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {p0, v7}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 407
    goto :goto_0

    .line 409
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 410
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 411
    const/4 v3, 0x5

    goto :goto_0

    .line 413
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 414
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    move v3, v5

    .line 415
    goto :goto_0

    .line 417
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 418
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 419
    const/4 v3, 0x7

    goto :goto_0

    .line 421
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 422
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v6

    .line 423
    goto :goto_0
.end method

.method public static sendMultiMediaMessageWithText(Landroid/content/Context;[Ljava/lang/String;Landroid/net/Uri;Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipients"    # [Ljava/lang/String;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "mimeType"    # Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;
    .param p4, "text"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    .line 429
    const-class v4, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v5, "sendMultiMediaMessageWithText"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, [Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Landroid/net/Uri;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-class v8, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    aput-object v8, v6, v7

    const/4 v7, 0x4

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {p0, v4, v5, v6}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 430
    const/4 v3, 0x2

    .line 472
    :cond_0
    :goto_0
    return v3

    .line 432
    :cond_1
    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v4

    const/16 v5, 0x7d0

    if-le v4, v5, :cond_2

    .line 433
    const/4 v3, 0x6

    goto :goto_0

    .line 436
    :cond_2
    invoke-static {p0, p2}, Lcom/sec/chaton/clientapi/MessageAPI;->CheckMuliMediaValidation(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v3

    .line 437
    .local v3, "nCheckResult":I
    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    .line 441
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 442
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v4, "send"

    invoke-virtual {v0, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRecipients([Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->multimedia_text:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v4, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRequiredAccessToken(Z)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 452
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 454
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 456
    const/4 v3, 0x1

    goto :goto_0

    .line 458
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 459
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 460
    const/4 v3, 0x5

    goto :goto_0

    .line 462
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 463
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 464
    const/4 v3, 0x4

    goto :goto_0

    .line 466
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 467
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 468
    const/4 v3, 0x7

    goto :goto_0

    .line 470
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 471
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 472
    const/4 v3, 0x3

    goto :goto_0
.end method

.method public static sendTextMessage(Landroid/content/Context;Ljava/lang/String;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 70
    const-class v5, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v6, "sendTextMessage"

    new-array v7, v3, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v7, v8

    const-class v8, Ljava/lang/String;

    aput-object v8, v7, v4

    invoke-static {p0, v5, v6, v7}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 105
    :goto_0
    return v3

    .line 73
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v5, 0x7d0

    if-le v3, v5, :cond_1

    .line 74
    const/4 v3, 0x6

    goto :goto_0

    .line 77
    :cond_1
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 78
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v3, "send"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->text:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v3, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v5, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->text:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    invoke-virtual {v3, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 85
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 87
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 89
    goto :goto_0

    .line 91
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 93
    const/4 v3, 0x5

    goto :goto_0

    .line 95
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 96
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 97
    const/4 v3, 0x4

    goto :goto_0

    .line 99
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 100
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 101
    const/4 v3, 0x7

    goto :goto_0

    .line 103
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 104
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 105
    const/4 v3, 0x3

    goto :goto_0
.end method

.method public static sendTextMessage(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipients"    # [Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v4, 0x1

    .line 111
    const-class v6, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v7, "sendTextMessage"

    new-array v8, v5, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v8, v9

    const-class v9, [Ljava/lang/String;

    aput-object v9, v8, v4

    const-class v9, Ljava/lang/String;

    aput-object v9, v8, v3

    invoke-static {p0, v6, v7, v8}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 148
    :goto_0
    return v3

    .line 114
    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v6, 0x7d0

    if-le v3, v6, :cond_1

    .line 115
    const/4 v3, 0x6

    goto :goto_0

    .line 118
    :cond_1
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 119
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v3, "send"

    invoke-virtual {v0, v3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRecipients([Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->text:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v3, v6}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRequiredAccessToken(Z)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v3

    sget-object v6, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->text:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    invoke-virtual {v3, v6}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 128
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 130
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v3, v4

    .line 132
    goto :goto_0

    .line 134
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 135
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 136
    const/4 v3, 0x5

    goto :goto_0

    .line 138
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 139
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 140
    const/4 v3, 0x4

    goto :goto_0

    .line 142
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 143
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 144
    const/4 v3, 0x7

    goto :goto_0

    .line 146
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 147
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v3, v5

    .line 148
    goto :goto_0
.end method

.method public static sendTextMessageWithURL(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "url"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x2

    const/4 v5, 0x1

    .line 200
    const-class v7, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v8, "sendTextMessageWithURL"

    new-array v9, v6, [Ljava/lang/Class;

    const/4 v10, 0x0

    const-class v11, Landroid/content/Context;

    aput-object v11, v9, v10

    const-class v10, Ljava/lang/String;

    aput-object v10, v9, v5

    const-class v10, Ljava/lang/String;

    aput-object v10, v9, v4

    invoke-static {p0, v7, v8, v9}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 239
    :goto_0
    return v4

    .line 203
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 204
    .local v3, "tempMessage":Ljava/lang/StringBuilder;
    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 206
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const/16 v7, 0x7d0

    if-le v4, v7, :cond_1

    .line 207
    const/4 v4, 0x6

    goto :goto_0

    .line 210
    :cond_1
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 211
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v4, "send"

    invoke-virtual {v0, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    sget-object v7, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->text_url:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v4, v7}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    sget-object v7, Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;->text:Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;

    invoke-virtual {v4, v7}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMimeType(Lcom/sec/chaton/clientapi/ChatONAPI$MimeType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 219
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 221
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move v4, v5

    .line 223
    goto :goto_0

    .line 225
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 226
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 227
    const/4 v4, 0x5

    goto :goto_0

    .line 229
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 230
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 231
    const/4 v4, 0x4

    goto :goto_0

    .line 233
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 234
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 235
    const/4 v4, 0x7

    goto :goto_0

    .line 237
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 238
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    move v4, v6

    .line 239
    goto :goto_0
.end method

.method public static sendTextMessageWithURL(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "recipients"    # [Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "url"    # Ljava/lang/String;
    .annotation runtime Lcom/sec/chaton/clientapi/API;
        description = "1.10.3"
        versionCode = 0xa7dcbd
    .end annotation

    .prologue
    .line 154
    const-class v4, Lcom/sec/chaton/clientapi/MessageAPI;

    const-string v5, "sendTextMessageWithURL"

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v7, 0x0

    const-class v8, Landroid/content/Context;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-class v8, [Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x3

    const-class v8, Ljava/lang/String;

    aput-object v8, v6, v7

    invoke-static {p0, v4, v5, v6}, Lcom/sec/chaton/clientapi/UtilityAPI;->isAvailableAPI(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;[Ljava/lang/Class;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 155
    const/4 v4, 0x2

    .line 194
    :goto_0
    return v4

    .line 157
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, p3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 158
    .local v3, "tempMessage":Ljava/lang/StringBuilder;
    const-string v4, "\n\n"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    const/16 v5, 0x7d0

    if-le v4, v5, :cond_1

    .line 161
    const/4 v4, 0x6

    goto :goto_0

    .line 164
    :cond_1
    new-instance v0, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    invoke-direct {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;-><init>()V

    .line 165
    .local v0, "builder":Lcom/sec/chaton/clientapi/ChatONMessage$Builder;
    const-string v4, "send"

    invoke-virtual {v0, v4}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setAction(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContext(Landroid/content/Context;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRecipients([Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    sget-object v5, Lcom/sec/chaton/clientapi/ChatONMessage$contentType;->text_url:Lcom/sec/chaton/clientapi/ChatONMessage$contentType;

    invoke-virtual {v4, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setContentType(Lcom/sec/chaton/clientapi/ChatONMessage$contentType;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setMsg(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setUri(Ljava/lang/String;)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->setRequiredAccessToken(Z)Lcom/sec/chaton/clientapi/ChatONMessage$Builder;

    .line 174
    :try_start_0
    invoke-virtual {v0}, Lcom/sec/chaton/clientapi/ChatONMessage$Builder;->build()Lcom/sec/chaton/clientapi/ChatONMessage;

    move-result-object v2

    .line 176
    .local v2, "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    invoke-virtual {v2}, Lcom/sec/chaton/clientapi/ChatONMessage;->getIntent()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p0, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    .line 178
    const/4 v4, 0x1

    goto :goto_0

    .line 180
    .end local v2    # "msg":Lcom/sec/chaton/clientapi/ChatONMessage;
    :catch_0
    move-exception v1

    .line 181
    .local v1, "e":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    .line 182
    const/4 v4, 0x5

    goto :goto_0

    .line 184
    .end local v1    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v1

    .line 185
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 186
    const/4 v4, 0x4

    goto :goto_0

    .line 188
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    :catch_2
    move-exception v1

    .line 189
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 190
    const/4 v4, 0x7

    goto :goto_0

    .line 192
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_3
    move-exception v1

    .line 193
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 194
    const/4 v4, 0x3

    goto :goto_0
.end method
