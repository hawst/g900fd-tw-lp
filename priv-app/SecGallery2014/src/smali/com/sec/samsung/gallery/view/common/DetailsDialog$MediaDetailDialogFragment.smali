.class Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;
.super Landroid/app/DialogFragment;
.source "DetailsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/DetailsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MediaDetailDialogFragment"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;)V
    .locals 1
    .param p2, "ctx"    # Landroid/content/Context;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 116
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 117
    .local v0, "args":Landroid/os/Bundle;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 118
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 128
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    new-instance v2, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mContentsList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->access$200(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/samsung/gallery/view/common/DetailsDialog$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailsListAdapter:Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->access$102(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;)Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;

    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->onCreateDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 131
    .local v0, "detailDialog":Landroid/app/Dialog;
    new-instance v1, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment$1;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;)V

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 146
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 147
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 149
    :cond_0
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 151
    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->onDialogDestroy()V

    .line 158
    return-void
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$MediaDetailDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->onDialogDismiss()V

    .line 123
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 124
    return-void
.end method
