.class Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;
.super Landroid/content/BroadcastReceiver;
.source "KnoxModeReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 32
    if-eqz p2, :cond_0

    .line 33
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 34
    .local v1, "data":Landroid/os/Bundle;
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 35
    .local v0, "action":Ljava/lang/String;
    const-string v4, "PACKAGENAME"

    invoke-virtual {p2, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 37
    .local v2, "packageName":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    # getter for: Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mKNOXProgressReceiver: onRecieve : action = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    const-string v4, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 40
    sget-object v4, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    const-string v5, "PROGRESS"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-interface {v4, v5}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->updateProgress(I)V

    .line 60
    .end local v0    # "action":Ljava/lang/String;
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v2    # "packageName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 42
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "data":Landroid/os/Bundle;
    .restart local v2    # "packageName":Ljava/lang/String;
    :cond_1
    const-string v4, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 43
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->access$100(Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 44
    sget-object v4, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    const-string v5, "ERRORCODE"

    invoke-virtual {p2, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    invoke-interface {v4, v5}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->failed(I)V

    goto :goto_0

    .line 45
    :cond_2
    const-string v4, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 46
    sget-object v4, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    invoke-interface {v4}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->updateProgress()V

    goto :goto_0

    .line 48
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->access$100(Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 49
    sget-object v4, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    if-eqz v4, :cond_0

    .line 50
    const-string v4, "SUCCESSCNT"

    invoke-virtual {v1, v4, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 51
    .local v3, "successCnt":I
    if-eqz v3, :cond_4

    .line 52
    # getter for: Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mKNOXProgressReceiver: onRecieve : complete = ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_4
    sget-object v4, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    invoke-interface {v4, v3}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->onCompleted(I)V

    goto :goto_0
.end method
