.class public final enum Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;
.super Ljava/lang/Enum;
.source "CpuMultiCoreCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "CpuBoostCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

.field public static final enum INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

.field public static final enum USE_MULTI_CORE_CPU:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    const-string v1, "INITIALIZE"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    const-string v1, "USE_MULTI_CORE_CPU"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->USE_MULTI_CORE_CPU:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    .line 21
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->INITIALIZE:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->USE_MULTI_CORE_CPU:Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/CpuMultiCoreCmd$CpuBoostCmdType;

    return-object v0
.end method
