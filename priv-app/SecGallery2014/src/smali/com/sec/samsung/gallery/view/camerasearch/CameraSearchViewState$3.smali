.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;
.super Ljava/lang/Object;
.source "CameraSearchViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;II)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I
    .param p4, "extra"    # I

    .prologue
    .line 168
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$900(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 169
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyItem(I)Ljava/lang/String;

    move-result-object v0

    .line 170
    .local v0, "key":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$1000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->onDeleteKeyword(Ljava/lang/String;)V

    .line 171
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->removeKeyword(I)V

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mSearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyWordCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;->access$1100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    new-instance v2, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3$1;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState$3;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 180
    :cond_0
    return-void
.end method
