.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;
.super Ljava/lang/Object;
.source "GlComposeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "LayoutConfiguration"
.end annotation


# instance fields
.field public mLevelLand:I

.field public mLevelMax:I

.field public mLevelPort:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1892
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setLevel(IZ)V
    .locals 7
    .param p1, "level"    # I
    .param p2, "setDst"    # Z

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1900
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCount:I

    if-lt p1, v1, :cond_3

    .line 1901
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCount:I

    add-int/lit8 p1, v1, -0x1

    .line 1904
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getPosCtrl(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$1700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    move-result-object v0

    .line 1906
    .local v0, "posCtrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    if-eqz p2, :cond_4

    .line 1907
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iput-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1912
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->initEnv(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)V

    .line 1913
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWideMode:Z

    if-nez v1, :cond_5

    move v1, v2

    :goto_2
    invoke-virtual {v0, v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetAttributes(IZ)V

    .line 1914
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetCount()V

    .line 1915
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetPosition()V

    .line 1916
    if-nez p2, :cond_2

    .line 1917
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setToCurrentCtrl()V

    .line 1918
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSplitViewExpanded:Z

    if-eqz v6, :cond_1

    move v3, v2

    :cond_1
    invoke-interface {v1, v4, v2, v5, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V

    .line 1920
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setReferCtrl(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V

    .line 1921
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v3, 0x0

    invoke-virtual {v1, v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->setPitchRate(FZ)V

    .line 1922
    return-void

    .line 1902
    .end local v0    # "posCtrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    :cond_3
    if-gez p1, :cond_0

    .line 1903
    const/4 p1, 0x0

    goto :goto_0

    .line 1909
    .restart local v0    # "posCtrl":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iput-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 1910
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iput p1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    goto :goto_1

    :cond_5
    move v1, v3

    .line 1913
    goto :goto_2
.end method
