.class Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;
.super Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PerformClick"
.end annotation


# instance fields
.field mClickMotionPosition:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 1

    .prologue
    .line 2392
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$WindowRunnnable;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$1;

    .prologue
    .line 2392
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 2400
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mDataChanged:Z

    if-eqz v3, :cond_1

    .line 2414
    :cond_0
    :goto_0
    return-void

    .line 2402
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v0, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 2403
    .local v0, "adapter":Landroid/widget/ListAdapter;
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->mClickMotionPosition:I

    .line 2404
    .local v1, "motionPosition":I
    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v3, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mItemCount:I

    if-lez v3, :cond_0

    const/4 v3, -0x1

    if-eq v1, v3, :cond_0

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2407
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget v4, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mFirstPosition:I

    sub-int v4, v1, v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2410
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 2411
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PerformClick;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-virtual {v3, v2, v1, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->performItemClick(Landroid/view/View;IJ)Z

    goto :goto_0
.end method
