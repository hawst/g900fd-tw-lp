.class public abstract Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlSlideshowEffect.java"


# instance fields
.field public isPlayingVideoItem:Z

.field protected mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

.field private mContext:Landroid/content/Context;

.field public mCurrentIndex:I

.field public mEffectDuration:J

.field protected mGlCanvas:[Lcom/sec/android/gallery3d/glcore/GlCanvas;

.field protected mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field protected mImageCache:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public mImageIndex:I

.field protected mIsSlideShowResumed:Z

.field protected mSetImageCount:I

.field public mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

.field protected mStartTime:J

.field protected final mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

.field public mWaitDuration:J


# direct methods
.method protected constructor <init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V
    .locals 3
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 47
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 31
    const-wide/16 v0, 0x3e8

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mEffectDuration:J

    .line 32
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlCanvas:[Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mImageCache:Ljava/util/LinkedList;

    .line 37
    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSetImageCount:I

    .line 40
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mWaitDuration:J

    .line 42
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mIsSlideShowResumed:Z

    .line 43
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isPlayingVideoItem:Z

    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    .line 49
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mContext:Landroid/content/Context;

    .line 50
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 0
    .param p1, "ratio"    # F

    .prologue
    .line 54
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onUpdate(F)V

    .line 55
    return-void
.end method

.method public createView()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 59
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getMaxDisplayObjCount()I

    move-result v1

    .line 60
    .local v1, "objCount":I
    if-eqz v1, :cond_0

    .line 61
    new-array v2, v1, [Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 62
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    new-instance v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v6, v6, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    aput-object v3, v2, v0

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onCreate()V

    .line 67
    return-void
.end method

.method public destoryView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 70
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onDestroy()V

    .line 71
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isPlayingVideoItem:Z

    if-nez v1, :cond_0

    .line 72
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v1, :cond_1

    .line 75
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 76
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 77
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v2, v1, v0

    .line 75
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    .end local v0    # "i":I
    :cond_1
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 81
    return-void
.end method

.method public getCropCenterTexcoord(FFLandroid/graphics/Bitmap;)[F
    .locals 12
    .param p1, "objW"    # F
    .param p2, "objH"    # F
    .param p3, "b"    # Landroid/graphics/Bitmap;

    .prologue
    .line 84
    if-nez p3, :cond_0

    .line 85
    const/4 v3, 0x0

    .line 117
    :goto_0
    return-object v3

    .line 88
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v8, v9

    .line 89
    .local v8, "w":F
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v1, v9

    .line 92
    .local v1, "h":F
    const/high16 v4, 0x3f800000    # 1.0f

    .line 93
    .local v4, "s":F
    const/high16 v5, 0x3f800000    # 1.0f

    .line 94
    .local v5, "t":F
    const/16 v9, 0x8

    new-array v3, v9, [F

    .line 95
    .local v3, "res":[F
    div-float v2, p1, p2

    .line 96
    .local v2, "objRatio":F
    div-float v0, v8, v1

    .line 97
    .local v0, "bitmapRatio":F
    cmpg-float v9, v2, v0

    if-gez v9, :cond_1

    .line 99
    div-float v4, v0, v2

    .line 105
    :goto_1
    const/high16 v9, 0x3f800000    # 1.0f

    div-float v6, v9, v4

    .line 106
    .local v6, "unitS":F
    const/high16 v9, 0x3f800000    # 1.0f

    div-float v7, v9, v5

    .line 107
    .local v7, "unitT":F
    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v6

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    aput v10, v3, v9

    .line 108
    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v7

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    aput v10, v3, v9

    .line 109
    const/4 v9, 0x2

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    aget v11, v3, v11

    sub-float/2addr v10, v11

    aput v10, v3, v9

    .line 110
    const/4 v9, 0x3

    const/4 v10, 0x1

    aget v10, v3, v10

    aput v10, v3, v9

    .line 112
    const/4 v9, 0x4

    const/4 v10, 0x2

    aget v10, v3, v10

    aput v10, v3, v9

    .line 113
    const/4 v9, 0x5

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    aget v11, v3, v11

    sub-float/2addr v10, v11

    aput v10, v3, v9

    .line 114
    const/4 v9, 0x6

    const/4 v10, 0x0

    aget v10, v3, v10

    aput v10, v3, v9

    .line 115
    const/4 v9, 0x7

    const/4 v10, 0x5

    aget v10, v3, v10

    aput v10, v3, v9

    goto :goto_0

    .line 102
    .end local v6    # "unitS":F
    .end local v7    # "unitT":F
    :cond_1
    div-float v5, v2, v0

    goto :goto_1
.end method

.method protected getDelta(FFF)F
    .locals 6
    .param p1, "ratio"    # F
    .param p2, "startValue"    # F
    .param p3, "targetValue"    # F

    .prologue
    .line 122
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, p1, v2

    if-lez v2, :cond_0

    .line 128
    .end local p3    # "targetValue":F
    :goto_0
    return p3

    .line 125
    .restart local p3    # "targetValue":F
    :cond_0
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    float-to-double v4, p1

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    double-to-float v0, v2

    .line 126
    .local v0, "delta":F
    sub-float v2, p3, p2

    neg-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float v1, p2, v2

    .local v1, "value":F
    move p3, v1

    .line 128
    goto :goto_0
.end method

.method protected getDelta(FJJFF)F
    .locals 6
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    .line 134
    sub-long v2, p2, p4

    long-to-float v2, v2

    div-float/2addr v2, p1

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 146
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 140
    .restart local p7    # "targetValue":F
    :cond_0
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    sub-long v4, p2, p4

    long-to-double v4, v4

    mul-double/2addr v2, v4

    float-to-double v4, p1

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->cos(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    sub-double/2addr v2, v4

    double-to-float v0, v2

    .line 143
    .local v0, "delta":F
    sub-float v2, p7, p6

    neg-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float v1, p6, v2

    .local v1, "value":F
    move p7, v1

    .line 146
    goto :goto_0
.end method

.method protected getDelta_CircularOut(FJJFF)F
    .locals 8
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 155
    sub-long v4, p2, p4

    long-to-float v3, v4

    div-float/2addr v3, p1

    cmpl-float v3, v3, v6

    if-lez v3, :cond_0

    .line 172
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 162
    .restart local p7    # "targetValue":F
    :cond_0
    sub-long v4, p2, p4

    long-to-float v3, v4

    div-float v1, v3, p1

    .line 164
    .local v1, "t":F
    sub-float/2addr v1, v6

    .line 166
    mul-float v3, v1, v1

    sub-float v3, v6, v3

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-float v0, v4

    .line 169
    .local v0, "delta":F
    sub-float v3, p7, p6

    mul-float/2addr v3, v0

    add-float v2, p6, v3

    .local v2, "value":F
    move p7, v2

    .line 172
    goto :goto_0
.end method

.method protected getDelta_ExponentialOut(FJJFF)F
    .locals 8
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    .line 181
    sub-long v4, p2, p4

    long-to-float v3, v4

    div-float/2addr v3, p1

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 196
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 188
    .restart local p7    # "targetValue":F
    :cond_0
    sub-long v4, p2, p4

    long-to-float v3, v4

    div-float v1, v3, p1

    .line 190
    .local v1, "t":F
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const/high16 v3, -0x3ee00000    # -10.0f

    mul-float/2addr v3, v1

    float-to-double v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    neg-double v4, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    add-double/2addr v4, v6

    double-to-float v0, v4

    .line 193
    .local v0, "delta":F
    sub-float v3, p7, p6

    mul-float/2addr v3, v0

    add-float v2, p6, v3

    .local v2, "value":F
    move p7, v2

    .line 196
    goto :goto_0
.end method

.method protected getDelta_linear(FFF)F
    .locals 2
    .param p1, "ratio"    # F
    .param p2, "startValue"    # F
    .param p3, "targetValue"    # F

    .prologue
    .line 205
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, p1, v1

    if-lez v1, :cond_0

    .line 213
    .end local p3    # "targetValue":F
    :goto_0
    return p3

    .line 211
    .restart local p3    # "targetValue":F
    :cond_0
    sub-float v1, p3, p2

    mul-float/2addr v1, p1

    add-float v0, p2, v1

    .local v0, "value":F
    move p3, v0

    .line 213
    goto :goto_0
.end method

.method protected getDelta_linear(FJJFF)F
    .locals 4
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    .line 224
    sub-long v2, p2, p4

    long-to-float v2, v2

    div-float v0, v2, p1

    .line 226
    .local v0, "delta":F
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-lez v2, :cond_0

    .line 234
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 232
    .restart local p7    # "targetValue":F
    :cond_0
    sub-float v2, p7, p6

    mul-float/2addr v2, v0

    add-float v1, p6, v2

    .local v1, "value":F
    move p7, v1

    .line 234
    goto :goto_0
.end method

.method protected getDelta_sinusoidalIN(FFF)F
    .locals 8
    .param p1, "ratio"    # F
    .param p2, "startValue"    # F
    .param p3, "targetValue"    # F

    .prologue
    .line 242
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, p1, v3

    if-lez v3, :cond_0

    .line 257
    .end local p3    # "targetValue":F
    :goto_0
    return p3

    .line 249
    .restart local p3    # "targetValue":F
    :cond_0
    sub-float v0, p3, p2

    .line 251
    .local v0, "changeInValue":F
    float-to-double v4, p1

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 254
    .local v1, "delta":F
    neg-float v3, v0

    mul-float/2addr v3, v1

    add-float/2addr v3, v0

    add-float v2, v3, p2

    .local v2, "value":F
    move p3, v2

    .line 257
    goto :goto_0
.end method

.method protected getDelta_sinusoidalIN(FJJFF)F
    .locals 8
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    .line 269
    sub-long v4, p2, p4

    long-to-float v4, v4

    div-float v2, v4, p1

    .line 271
    .local v2, "timeDelta":F
    sub-long v4, p2, p4

    long-to-float v4, v4

    div-float/2addr v4, p1

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 286
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 278
    .restart local p7    # "targetValue":F
    :cond_0
    sub-float v0, p7, p6

    .line 280
    .local v0, "changeInValue":F
    float-to-double v4, v2

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 283
    .local v1, "delta":F
    neg-float v4, v0

    mul-float/2addr v4, v1

    add-float/2addr v4, v0

    add-float v3, v4, p6

    .local v3, "value":F
    move p7, v3

    .line 286
    goto :goto_0
.end method

.method protected getDelta_sinusoidalOut(FFF)F
    .locals 8
    .param p1, "ratio"    # F
    .param p2, "startValue"    # F
    .param p3, "targetValue"    # F

    .prologue
    .line 293
    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, p1, v3

    if-lez v3, :cond_0

    .line 308
    .end local p3    # "targetValue":F
    :goto_0
    return p3

    .line 300
    .restart local p3    # "targetValue":F
    :cond_0
    sub-float v0, p3, p2

    .line 302
    .local v0, "changeInValue":F
    float-to-double v4, p1

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 305
    .local v1, "delta":F
    mul-float v3, v0, v1

    add-float v2, v3, p2

    .local v2, "value":F
    move p3, v2

    .line 308
    goto :goto_0
.end method

.method protected getDelta_sinusoidalOut(FJJFF)F
    .locals 8
    .param p1, "duration"    # F
    .param p2, "now"    # J
    .param p4, "startTime"    # J
    .param p6, "startValue"    # F
    .param p7, "targetValue"    # F

    .prologue
    .line 318
    sub-long v4, p2, p4

    long-to-float v4, v4

    div-float v2, v4, p1

    .line 320
    .local v2, "timeDelta":F
    sub-long v4, p2, p4

    long-to-float v4, v4

    div-float/2addr v4, p1

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 335
    .end local p7    # "targetValue":F
    :goto_0
    return p7

    .line 327
    .restart local p7    # "targetValue":F
    :cond_0
    sub-float v0, p7, p6

    .line 329
    .local v0, "changeInValue":F
    float-to-double v4, v2

    const-wide v6, 0x3ff921fb54442d18L    # 1.5707963267948966

    mul-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v1, v4

    .line 332
    .local v1, "delta":F
    mul-float v4, v0, v1

    add-float v3, v4, p6

    .local v3, "value":F
    move p7, v3

    .line 335
    goto :goto_0
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 342
    sget v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    return v0
.end method

.method public getFocus()I
    .locals 1

    .prologue
    .line 349
    iget v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mCurrentIndex:I

    return v0
.end method

.method public getMaxDisplayObjCount()I
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x0

    return v0
.end method

.method public getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getWaitDuration()J
    .locals 2

    .prologue
    .line 370
    iget-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mWaitDuration:J

    return-wide v0
.end method

.method public isBitmapdecoded()Z
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x1

    return v0
.end method

.method public isInitialLoaded()Z
    .locals 1

    .prologue
    .line 377
    const/4 v0, 0x1

    return v0
.end method

.method public isNextLoaded()Z
    .locals 1

    .prologue
    .line 386
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreate()V
    .locals 0

    .prologue
    .line 393
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 398
    return-void
.end method

.method protected onLayout()V
    .locals 0

    .prologue
    .line 403
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 410
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->onStop()V

    .line 412
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->isPlayingVideoItem:Z

    if-nez v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 415
    :cond_0
    return-void
.end method

.method protected abstract onUpdate(F)V
.end method

.method public process(J)V
    .locals 1
    .param p1, "curTime"    # J

    .prologue
    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getIsShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mIsSlideShowResumed:Z

    .line 544
    :goto_0
    return-void

    .line 543
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->process(J)V

    goto :goto_0
.end method

.method public reLayoutView()V
    .locals 6

    .prologue
    .line 423
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v5, :cond_1

    .line 425
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .local v0, "arr$":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 427
    .local v4, "mGlObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 429
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getContentBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 431
    .local v1, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 433
    invoke-virtual {p0, v4, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setObjectAttrib(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 425
    .end local v1    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 443
    .end local v0    # "arr$":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v4    # "mGlObject":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onLayout()V

    .line 445
    return-void
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    .line 452
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getWaitDuration()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mWaitDuration:J

    .line 458
    :cond_0
    return-void
.end method

.method protected setObjectAttrib(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V
    .locals 18
    .param p1, "objs"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 465
    if-nez p2, :cond_0

    .line 518
    :goto_0
    return-void

    .line 468
    :cond_0
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v0, v2

    move/from16 v17, v0

    .line 470
    .local v17, "w":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v8, v2

    .line 472
    .local v8, "h":F
    div-float v13, v17, v8

    .line 474
    .local v13, "srcRatio":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidth:I

    int-to-float v2, v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeight:I

    int-to-float v3, v3

    div-float v12, v2, v3

    .line 477
    .local v12, "screenRatio":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v11, v2, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    .line 479
    .local v11, "objW":F
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v10, v2, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    .line 481
    .local v10, "objH":F
    cmpl-float v2, v13, v12

    if-lez v2, :cond_1

    .line 485
    div-float v10, v11, v13

    .line 495
    :goto_1
    move v6, v11

    .line 497
    .local v6, "itemW":F
    move v7, v10

    .line 499
    .local v7, "itemH":F
    neg-float v2, v11

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v6, v3

    add-float v14, v2, v3

    .line 501
    .local v14, "startX":F
    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v7, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v7, v3

    sub-float v15, v2, v3

    .line 503
    .local v15, "startY":F
    move-object/from16 v1, p1

    .line 505
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getDistance()F

    move-result v2

    neg-float v2, v2

    invoke-virtual {v1, v14, v15, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 507
    invoke-virtual {v1, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 509
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 512
    const/4 v9, 0x0

    .line 514
    .local v9, "left":F
    const/16 v16, 0x0

    .line 516
    .local v16, "top":F
    const/4 v2, 0x0

    const/4 v3, 0x0

    add-float/2addr v3, v6

    const/4 v4, 0x0

    const/4 v5, 0x0

    add-float/2addr v5, v7

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCustomTexCoord(FFFFFF)V

    goto :goto_0

    .line 491
    .end local v1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v6    # "itemW":F
    .end local v7    # "itemH":F
    .end local v9    # "left":F
    .end local v14    # "startX":F
    .end local v15    # "startY":F
    .end local v16    # "top":F
    :cond_1
    mul-float v11, v10, v13

    goto :goto_1
.end method

.method public setSlideShowInfo(ILcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;)V
    .locals 2
    .param p1, "effectDuration"    # I
    .param p2, "effectType"    # Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowTransitionShow$TransitionType;

    .prologue
    .line 523
    int-to-long v0, p1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mEffectDuration:J

    .line 525
    return-void
.end method

.method public setStartIndex(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 530
    iput p1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mCurrentIndex:I

    .line 532
    return-void
.end method

.method public updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 548
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    .line 550
    .local v0, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-nez v0, :cond_0

    .line 552
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 564
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->setObjectAttrib(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 566
    return-void

    .line 556
    :cond_0
    invoke-virtual {v0, p2}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 558
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->requestFullUpload()V

    .line 560
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V

    goto :goto_0
.end method

.method public updatePlayVideo(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 5
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 570
    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    .line 571
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 572
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPlayUri()Landroid/net/Uri;

    move-result-object v1

    .line 573
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_1

    .line 582
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 575
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v2, "ACTION_PRESENTATION_VIDEO_VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 576
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "VideoUri"

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 577
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 579
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    if-eqz v2, :cond_0

    .line 580
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->mSlideshowVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->initVideoView(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
