.class Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;
.super Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;
.source "AddUserTagListActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddTagTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 561
    .local p3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local p4, "tagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    .line 562
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 563
    return-void
.end method


# virtual methods
.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 567
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/common/AddUserTagUpdateTask;->onPostExecute(Ljava/lang/Boolean;)V

    .line 568
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->setResult(I)V

    .line 569
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;->this$0:Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->finish()V

    .line 570
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 559
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity$AddTagTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
