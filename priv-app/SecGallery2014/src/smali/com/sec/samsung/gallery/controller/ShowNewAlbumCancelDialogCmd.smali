.class public Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowNewAlbumCancelDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private dismissNewAlbumCancelDialog()V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 70
    :cond_0
    return-void
.end method

.method private showNewAlbumCancelDialog(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_CANCEL:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 61
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e01a5

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0236

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;-><init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .line 63
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->addObserver(Ljava/util/Observer;)V

    .line 64
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->showDialog()V

    .line 65
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 28
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 29
    .local v0, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v0, v2

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mContext:Landroid/content/Context;

    .line 30
    const/4 v2, 0x1

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 32
    .local v1, "show":Z
    if-nez v1, :cond_0

    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->dismissNewAlbumCancelDialog()V

    .line 38
    :goto_0
    return-void

    .line 37
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->showNewAlbumCancelDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 5
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 42
    move-object v1, p2

    check-cast v1, Lcom/sec/samsung/gallery/core/Event;

    .line 43
    .local v1, "ev":Lcom/sec/samsung/gallery/core/Event;
    const/4 v2, 0x0

    .line 45
    .local v2, "result":Z
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v3

    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_CANCEL:I

    if-ne v3, v4, :cond_1

    .line 46
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    if-eqz v3, :cond_0

    .line 47
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mConfirmationDialog:Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->dismissDialog()V

    .line 50
    :cond_0
    const/4 v3, 0x1

    new-array v0, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v0, v3

    .line 54
    .local v0, "body":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCancelDialogCmd;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "EXIT_NEW_ALBUM_MODE"

    invoke-virtual {v3, v4, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 57
    .end local v0    # "body":[Ljava/lang/Object;
    :cond_1
    return-void
.end method
