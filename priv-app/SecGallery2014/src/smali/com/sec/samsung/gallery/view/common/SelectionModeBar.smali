.class public Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
.super Ljava/lang/Object;
.source "SelectionModeBar.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMainViewGroup:Landroid/view/ViewGroup;

.field private mSelectAllText:Landroid/widget/TextView;

.field private mSelectionModeLayout:Landroid/widget/RelativeLayout;

.field private mTransParentBackground:Z

.field private mTwCheckBox:Landroid/widget/CheckBox;

.field private mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/ViewObservable;)V
    .locals 8
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "viewObservable"    # Lcom/sec/samsung/gallery/view/ViewObservable;

    .prologue
    const/4 v7, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 41
    new-instance v2, Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 42
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;

    .line 43
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f0175

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    .line 45
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v1

    .line 46
    .local v1, "screenRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 47
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mInflater:Landroid/view/LayoutInflater;

    .line 49
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f020a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    .line 50
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_0

    .line 51
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mInflater:Landroid/view/LayoutInflater;

    const v3, 0x7f0300cb

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    .line 52
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    :goto_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 59
    .local v0, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v2

    invoke-virtual {v0, v7, v2, v7, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 60
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 62
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f0f020c

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTwCheckBox:Landroid/widget/CheckBox;

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f0f020d

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectAllText:Landroid/widget/TextView;

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;-><init>(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    new-instance v3, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$2;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$2;-><init>(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 99
    return-void

    .line 54
    .end local v0    # "lp":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->bringToFront()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)Lcom/sec/samsung/gallery/view/ViewObservable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method


# virtual methods
.method public getSelectionModeBarHeight()I
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    return v0
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 7
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v6, 0x0

    .line 140
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    .line 141
    .local v2, "width":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    .line 142
    .local v0, "height":I
    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    int-to-float v4, v2

    int-to-float v5, v0

    invoke-virtual {v3, v4, v5, v2, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 146
    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v4

    invoke-direct {v1, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 148
    .local v1, "lp":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mDimenUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v3

    invoke-virtual {v1, v6, v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 149
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 150
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->requestLayout()V

    goto :goto_0
.end method

.method public removeView()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mMainViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 157
    :cond_0
    return-void
.end method

.method public setChecked(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 130
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectAllText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 137
    return-void
.end method

.method public setFocused(Z)V
    .locals 1
    .param p1, "focus"    # Z

    .prologue
    .line 164
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    .line 167
    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectAllText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 171
    return-void
.end method

.method public setTransparentBackground(Z)V
    .locals 3
    .param p1, "transparent"    # Z

    .prologue
    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    .line 103
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTransParentBackground:Z

    .line 104
    if-eqz p1, :cond_1

    .line 105
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f02024b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    .line 109
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f0f020b

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 110
    .local v0, "sperator":Landroid/view/View;
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 112
    .end local v0    # "sperator":Landroid/view/View;
    :cond_0
    return-void

    .line 107
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    const v2, 0x7f020013

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundResource(I)V

    goto :goto_0

    .line 110
    .restart local v0    # "sperator":Landroid/view/View;
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(IZ)V

    .line 116
    return-void
.end method

.method public setVisibility(IZ)V
    .locals 3
    .param p1, "visibility"    # I
    .param p2, "enableAnim"    # Z

    .prologue
    .line 120
    if-eqz p2, :cond_0

    .line 121
    if-nez p1, :cond_1

    const v0, 0x7f04001d

    .line 122
    .local v0, "aniEffect":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->startAnimation(Landroid/view/animation/Animation;)V

    .line 124
    .end local v0    # "aniEffect":I
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mTwCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v1, p1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mSelectionModeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 126
    return-void

    .line 121
    :cond_1
    const v0, 0x7f04001e

    goto :goto_0
.end method
