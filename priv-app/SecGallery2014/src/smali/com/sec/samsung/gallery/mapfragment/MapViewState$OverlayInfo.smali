.class public Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
.super Ljava/lang/Object;
.source "MapViewState.java"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "OverlayInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field private final mLatitude:D

.field private final mLongitude:D

.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mPath:Lcom/sec/android/gallery3d/data/Path;


# direct methods
.method public constructor <init>(DDLcom/sec/android/gallery3d/data/Path;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D
    .param p5, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p6, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 581
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 582
    iput-wide p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLatitude:D

    .line 583
    iput-wide p3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLongitude:D

    .line 584
    iput-object p5, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mPath:Lcom/sec/android/gallery3d/data/Path;

    .line 585
    iput-object p6, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 586
    return-void
.end method


# virtual methods
.method public compareTo(Ljava/lang/Object;)I
    .locals 6
    .param p1, "arg0"    # Ljava/lang/Object;

    .prologue
    .line 611
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    .line 612
    .local v0, "refInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    const/4 v1, 0x0

    .line 614
    .local v1, "result":I
    iget-wide v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLatitude:D

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getLatitude()D

    move-result-wide v4

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 615
    const/4 v1, 0x1

    .line 621
    :goto_0
    return v1

    .line 616
    :cond_0
    iget-wide v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLatitude:D

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getLatitude()D

    move-result-wide v4

    cmpl-double v2, v2, v4

    if-lez v2, :cond_1

    .line 617
    const/4 v1, -0x1

    goto :goto_0

    .line 619
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLatitude()D
    .locals 2

    .prologue
    .line 589
    iget-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLatitude:D

    return-wide v0
.end method

.method public getLongitude()D
    .locals 2

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mLongitude:D

    return-wide v0
.end method

.method public getMakerRepresentativeImg()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 605
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->createBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 606
    .local v0, "markerBitmap":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public getMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1

    .prologue
    .line 601
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 597
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->mPath:Lcom/sec/android/gallery3d/data/Path;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
