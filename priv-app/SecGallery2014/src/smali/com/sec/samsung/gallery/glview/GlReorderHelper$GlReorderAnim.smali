.class Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlReorderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlReorderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GlReorderAnim"
.end annotation


# instance fields
.field mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V
    .locals 1

    .prologue
    .line 468
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 469
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;

    .prologue
    .line 468
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 523
    sub-float v4, v7, p1

    .line 524
    .local v4, "invRatio":F
    mul-float v6, v4, v4

    sub-float v4, v7, v6

    .line 525
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectAnimation:Ljava/util/HashSet;
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$800(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 526
    .local v5, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget v6, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msx:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mtx:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msx:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v0, v6, v7

    .line 527
    .local v0, "dx":F
    iget v6, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msy:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mty:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msy:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v1, v6, v7

    .line 528
    .local v1, "dy":F
    iget v6, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msz:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mtz:F

    iget v8, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->msz:F

    sub-float/2addr v7, v8

    mul-float/2addr v7, v4

    add-float v2, v6, v7

    .line 529
    .local v2, "dz":F
    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(FFF)V

    goto :goto_0

    .line 531
    .end local v0    # "dx":F
    .end local v1    # "dy":F
    .end local v2    # "dz":F
    .end local v5    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    return-void
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 511
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->onStop()V

    .line 512
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 516
    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Reorder anim end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectAnimation:Ljava/util/HashSet;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$800(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 518
    return-void
.end method

.method public startReorder(II)V
    .locals 13
    .param p1, "oldFocusIndex"    # I
    .param p2, "newFocusIndex"    # I

    .prologue
    .line 472
    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$300()Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "oldFocusIndex = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " newFocusIndex = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Landroid/util/SparseIntArray;

    move-result-object v10

    invoke-virtual {v10}, Landroid/util/SparseIntArray;->clear()V

    .line 475
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v10

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ge p2, v10, :cond_0

    .line 476
    move v3, p2

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v10

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ge v3, v10, :cond_1

    .line 477
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Landroid/util/SparseIntArray;

    move-result-object v10

    const/4 v11, 0x1

    invoke-virtual {v10, v3, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 476
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 480
    .end local v3    # "i":I
    :cond_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v10

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    add-int/lit8 v3, v10, 0x1

    .restart local v3    # "i":I
    :goto_1
    if-gt v3, p2, :cond_1

    .line 481
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Landroid/util/SparseIntArray;

    move-result-object v10

    const/4 v11, -0x1

    invoke-virtual {v10, v3, v11}, Landroid/util/SparseIntArray;->put(II)V

    .line 480
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 484
    :cond_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$600(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlScroller;

    move-result-object v10

    iget-object v7, v10, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 485
    .local v7, "objSet":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-ge p1, p2, :cond_3

    const/4 v0, -0x1

    .line 486
    .local v0, "addIndex":I
    :goto_2
    invoke-static {p1, p2}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 487
    .local v5, "min":I
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 488
    .local v4, "max":I
    if-ge p1, p2, :cond_4

    add-int/lit8 v8, v5, 0x1

    .line 489
    .local v8, "start":I
    :goto_3
    if-ge p1, p2, :cond_5

    move v2, v4

    .line 490
    .local v2, "end":I
    :goto_4
    const/4 v3, 0x0

    :goto_5
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$600(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlScroller;

    move-result-object v10

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v3, v10, :cond_7

    .line 491
    aget-object v6, v7, v3

    check-cast v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 492
    .local v6, "obj":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-eqz v6, :cond_2

    iget v10, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v11}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v11

    iget v11, v11, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ne v10, v11, :cond_6

    .line 490
    :cond_2
    :goto_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 485
    .end local v0    # "addIndex":I
    .end local v2    # "end":I
    .end local v4    # "max":I
    .end local v5    # "min":I
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .end local v8    # "start":I
    :cond_3
    const/4 v0, 0x1

    goto :goto_2

    .restart local v0    # "addIndex":I
    .restart local v4    # "max":I
    .restart local v5    # "min":I
    :cond_4
    move v8, v5

    .line 488
    goto :goto_3

    .line 489
    .restart local v8    # "start":I
    :cond_5
    add-int/lit8 v2, v4, -0x1

    goto :goto_4

    .line 495
    .restart local v2    # "end":I
    .restart local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    :cond_6
    iget v1, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 496
    .local v1, "displayIndex":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$700(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v10

    if-eq v1, v10, :cond_2

    if-gt v8, v1, :cond_2

    if-gt v1, v2, :cond_2

    .line 498
    add-int v9, v1, v0

    .line 499
    .local v9, "targetDisplayIndex":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-virtual {v10, v9, v11}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getThumbPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 500
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v10, v10, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v11, v11, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v12, v12, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    invoke-virtual {v6, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->setTargetPos(FFF)V

    .line 501
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->setPosAsSource()V

    .line 502
    iput v9, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 503
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectAnimation:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$800(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 504
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectCommit:Ljava/util/HashSet;
    invoke-static {v10}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$900(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 507
    .end local v1    # "displayIndex":I
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .end local v9    # "targetDisplayIndex":I
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->start()V

    .line 508
    return-void
.end method
