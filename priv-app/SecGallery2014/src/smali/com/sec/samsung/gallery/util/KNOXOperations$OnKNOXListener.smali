.class public interface abstract Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;
.super Ljava/lang/Object;
.source "KNOXOperations.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/KNOXOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnKNOXListener"
.end annotation


# virtual methods
.method public abstract cancel()V
.end method

.method public abstract failed(I)V
.end method

.method public abstract handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
.end method

.method public abstract onCompleted(I)V
.end method

.method public abstract onRequestKNOXOperation()V
.end method

.method public abstract startProgress()V
.end method

.method public abstract updateProgress()V
.end method

.method public abstract updateProgress(I)V
.end method
