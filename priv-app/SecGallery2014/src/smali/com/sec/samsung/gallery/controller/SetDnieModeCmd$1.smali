.class Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;
.super Ljava/lang/Object;
.source "SetDnieModeCmd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->execute(Lorg/puremvc/java/interfaces/INotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;

.field final synthetic val$isEnable:Z

.field final synthetic val$mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;ZLcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0

    .prologue
    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;->val$isEnable:Z

    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;->val$mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 29
    :try_start_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;->val$isEnable:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd$1;->val$mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v2, :cond_0

    .line 30
    const/4 v2, 0x6

    invoke-static {v2}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 31
    # getter for: Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "setmDNIeUIMode(6)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    :goto_0
    return-void

    .line 33
    :cond_0
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/sec/android/hardware/SecHardwareInterface;->setmDNIeUIMode(I)Z

    .line 34
    # getter for: Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "setmDNIeUIMode(0)"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalAccessError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 36
    :catch_0
    move-exception v0

    .line 37
    .local v0, "e0":Ljava/lang/IllegalAccessError;
    # getter for: Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/SetDnieModeCmd;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "IllegalAccessError occured!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 38
    .end local v0    # "e0":Ljava/lang/IllegalAccessError;
    :catch_1
    move-exception v1

    .line 39
    .local v1, "e1":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
