.class Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;
.super Ljava/lang/Object;
.source "HelpAirCommandState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

.field final synthetic val$mTapX:I

.field final synthetic val$mTapY:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;II)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iput p2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->val$mTapY:I

    iput p3, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->val$mTapX:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 180
    iget v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->val$mTapY:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int v0, v1, v2

    .line 182
    .local v0, "mHoverY":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHeight:I

    div-int/lit8 v1, v1, 0x8

    if-ge v0, v1, :cond_0

    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHeight:I

    div-int/lit8 v0, v1, 0x8

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setY(F)V

    .line 186
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->val$mTapX:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setX(F)V

    .line 187
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    .line 188
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$5;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v0

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 190
    :cond_1
    return-void
.end method
