.class Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;
.super Ljava/lang/Object;
.source "SecretboxOperations.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/SecretboxOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaObjectComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaObject;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/util/SecretboxOperations$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/util/SecretboxOperations$1;

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I
    .locals 6
    .param p1, "object1"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "object2"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v4, 0x0

    .line 151
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_1

    instance-of v5, p2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v5, :cond_1

    .line 152
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "setPath1":Ljava/lang/String;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 154
    .local v3, "setPath2":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    .line 172
    .end local v2    # "setPath1":Ljava/lang/String;
    .end local v3    # "setPath2":Ljava/lang/String;
    :cond_0
    :goto_0
    return v4

    .line 155
    .restart local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v5, p1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    instance-of v5, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v5, :cond_0

    .line 156
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 157
    .local v0, "itemPath1":Ljava/lang/String;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 159
    .local v1, "itemPath2":Ljava/lang/String;
    if-nez v0, :cond_2

    if-eqz v1, :cond_0

    .line 161
    :cond_2
    if-nez v0, :cond_3

    if-eqz v1, :cond_3

    .line 162
    const/4 v4, -0x1

    goto :goto_0

    .line 163
    :cond_3
    if-eqz v0, :cond_4

    if-nez v1, :cond_4

    .line 164
    const/4 v4, 0x1

    goto :goto_0

    .line 165
    :cond_4
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 166
    const-string v5, "/"

    invoke-virtual {v0, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 167
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v1, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 168
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 149
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;->compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I

    move-result v0

    return v0
.end method
