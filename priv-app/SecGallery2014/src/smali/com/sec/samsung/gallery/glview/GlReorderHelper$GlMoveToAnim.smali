.class Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlReorderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlReorderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GlMoveToAnim"
.end annotation


# instance fields
.field mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V
    .locals 1

    .prologue
    .line 534
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 535
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;

    .prologue
    .line 534
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 566
    sub-float v3, v6, p1

    .line 567
    .local v3, "invRatio":F
    mul-float v5, v3, v3

    sub-float v3, v6, v5

    .line 568
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v4

    .line 570
    .local v4, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v5, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    iget v6, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtx:F

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msx:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v3

    add-float v0, v5, v6

    .line 571
    .local v0, "dx":F
    iget v5, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    iget v6, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mty:F

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msy:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v3

    add-float v1, v5, v6

    .line 572
    .local v1, "dy":F
    iget v5, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    iget v6, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mtz:F

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->msz:F

    sub-float/2addr v6, v7

    mul-float/2addr v6, v3

    add-float v2, v5, v6

    .line 573
    .local v2, "dz":F
    invoke-virtual {v4, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 575
    return-void
.end method

.method protected getParam()I
    .locals 2

    .prologue
    .line 578
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$700(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    or-int/2addr v0, v1

    return v0
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 549
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->onStop()V

    .line 550
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 555
    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Move anim end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 556
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # invokes: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->commitCurrentOrder()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1000(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V

    .line 557
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragListener:Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1100(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragListener:Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1100(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

    move-result-object v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->getParam()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;->onDragFinished(I)V

    .line 560
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # invokes: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->resetDragObject()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$1200(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V

    .line 561
    return-void
.end method

.method public startAnimation()V
    .locals 4

    .prologue
    .line 538
    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$300()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "start move anim. target index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$700(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 539
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    .line 540
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$700(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getThumbPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 541
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v1

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v2

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourcePos(FFF)V

    .line 542
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->mDstPos:Lcom/sec/android/gallery3d/glcore/GlPos;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos;->mZ:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetPos(FFF)V

    .line 543
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->resetPos()V

    .line 544
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->start()V

    .line 545
    return-void
.end method
