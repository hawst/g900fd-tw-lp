.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;
.super Ljava/lang/Object;
.source "GallerySearchUserTagScrollView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExpandClickListener"
.end annotation


# instance fields
.field private mGroup:Landroid/view/ViewGroup;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V
    .locals 1
    .param p2, "group"    # Landroid/view/ViewGroup;

    .prologue
    .line 878
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 876
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->mGroup:Landroid/view/ViewGroup;

    .line 879
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->mGroup:Landroid/view/ViewGroup;

    .line 880
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    .line 884
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v1, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->mGroup:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mExpandStates:Ljava/util/HashMap;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->mGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 885
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->mGroup:Landroid/view/ViewGroup;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->updateExpansionIcon(Landroid/view/ViewGroup;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;Landroid/view/ViewGroup;)V

    .line 886
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView$ExpandClickListener;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->refreshTagList()V

    .line 887
    return-void

    .line 884
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
