.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 654
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMoved()V
    .locals 2

    .prologue
    .line 658
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$702(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z

    .line 659
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitRemoverSettingPosition:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$1100(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getY()F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitRemoverSettingPosition:Landroid/graphics/PointF;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$1100(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Landroid/graphics/PointF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 660
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->getRemoverPosition()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setIndicatorPosition(I)V

    .line 661
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$702(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z

    .line 663
    :cond_0
    return-void
.end method
