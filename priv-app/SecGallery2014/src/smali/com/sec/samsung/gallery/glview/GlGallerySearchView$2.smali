.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CMD_UPDATE_ITEM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    .line 189
    return-void
.end method

.method public onChanged(II)V
    .locals 5
    .param p1, "index"    # I
    .param p2, "parm"    # I

    .prologue
    .line 193
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 196
    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gt v0, p1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-gt p1, v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0, v2, p1, v3, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    .line 200
    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
