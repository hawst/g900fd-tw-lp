.class public Lcom/sec/samsung/gallery/controller/DestroyCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "DestroyCmd.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private getComponentName(Ljava/lang/reflect/Field;)Ljava/lang/String;
    .locals 4
    .param p1, "field"    # Ljava/lang/reflect/Field;

    .prologue
    .line 64
    const/4 v2, 0x0

    .line 66
    .local v2, "name":Ljava/lang/String;
    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {p1, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/lang/String;

    move-object v2, v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 72
    :goto_0
    return-object v2

    .line 67
    :catch_0
    move-exception v1

    .line 68
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 69
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0
.end method

.method private removeAllCommands(Lcom/sec/samsung/gallery/core/GalleryFacade;)V
    .locals 7
    .param p1, "galleryFacade"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 55
    const-class v6, Lcom/sec/samsung/gallery/core/NotificationNames;

    invoke-virtual {v6}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 56
    .local v2, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 57
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->getComponentName(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v5

    .line 58
    .local v5, "notiName":Ljava/lang/String;
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->hasCommand(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 59
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeCommand(Ljava/lang/String;)V

    .line 56
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 61
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "notiName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private removeAllMediators(Lcom/sec/samsung/gallery/core/GalleryFacade;)V
    .locals 7
    .param p1, "galleryFacade"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 37
    const-class v6, Lcom/sec/samsung/gallery/core/MediatorNames;

    invoke-virtual {v6}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 38
    .local v2, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 39
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->getComponentName(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v5

    .line 40
    .local v5, "mediatorName":Ljava/lang/String;
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->hasMediator(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 41
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 38
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 43
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "mediatorName":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private removeAllProxies(Lcom/sec/samsung/gallery/core/GalleryFacade;)V
    .locals 7
    .param p1, "galleryFacade"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 46
    const-class v6, Lcom/sec/samsung/gallery/core/ProxyNames;

    invoke-virtual {v6}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    .line 47
    .local v2, "fields":[Ljava/lang/reflect/Field;
    move-object v0, v2

    .local v0, "arr$":[Ljava/lang/reflect/Field;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 48
    .local v1, "field":Ljava/lang/reflect/Field;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->getComponentName(Ljava/lang/reflect/Field;)Ljava/lang/String;

    move-result-object v5

    .line 49
    .local v5, "proxyName":Ljava/lang/String;
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->hasProxy(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 50
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeProxy(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IProxy;

    .line 47
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 52
    .end local v1    # "field":Ljava/lang/reflect/Field;
    .end local v5    # "proxyName":Ljava/lang/String;
    :cond_1
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 21
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .local v0, "activity":Landroid/app/Activity;
    move-object v2, v0

    .line 24
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    .local v1, "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    move-object v2, v0

    .line 25
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    move-object v2, v0

    .line 26
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    move-object v2, v0

    .line 27
    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->stop()V

    .line 29
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->removeAllProxies(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    .line 30
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->removeAllMediators(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    .line 31
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/DestroyCmd;->removeAllCommands(Lcom/sec/samsung/gallery/core/GalleryFacade;)V

    .line 33
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 34
    return-void
.end method
