.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 508
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/16 v2, 0x8

    .line 511
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 512
    .local v0, "action":I
    if-nez v0, :cond_1

    .line 513
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->isColorPickerMode()Z

    move-result v1

    if-nez v1, :cond_2

    .line 514
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingMoved:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$600(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 515
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 516
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$700(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 517
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 521
    :cond_1
    :goto_0
    const/4 v1, 0x0

    return v1

    .line 519
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    goto :goto_0
.end method
