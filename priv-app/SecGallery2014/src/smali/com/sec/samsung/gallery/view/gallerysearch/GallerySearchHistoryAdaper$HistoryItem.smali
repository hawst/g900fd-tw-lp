.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;
.super Ljava/lang/Object;
.source "GallerySearchHistoryAdaper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HistoryItem"
.end annotation


# instance fields
.field public mTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->mTitle:Ljava/lang/String;

    .line 125
    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchHistoryAdaper$HistoryItem;->mTitle:Ljava/lang/String;

    .line 129
    return-void
.end method
