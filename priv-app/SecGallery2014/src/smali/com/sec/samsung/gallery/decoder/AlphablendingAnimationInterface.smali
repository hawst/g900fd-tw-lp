.class public Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;
.super Ljava/lang/Object;
.source "AlphablendingAnimationInterface.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "AlphablendingAnimationInterface"


# instance fields
.field private mAnimationStarted:Z

.field private mCurrentIndex:I

.field private mCurrentScale:F

.field private mDecodingComplete:Z

.field private mDuration:F

.field private mEndAlpha:F

.field private mImageChanging:Z

.field private mImageH:I

.field private mImageW:I

.field private mNeedToAnimate:Z

.field private mNextDirection:Z

.field private mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private mScrolling:Z

.field private mStartAlpha:F

.field private mStartTime:J

.field private mTileBorder:I

.field private mTileSize:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mAnimationStarted:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDecodingComplete:Z

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileBorder:I

    return-void
.end method

.method private drawDecodedTiles(Landroid/util/LongSparseArray;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILandroid/graphics/Rect;FI)V
    .locals 23
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "offsetX"    # I
    .param p4, "offsetY"    # I
    .param p5, "r"    # Landroid/graphics/Rect;
    .param p6, "length"    # F
    .param p7, "level"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lcom/sec/samsung/gallery/decoder/LargeImageTile;",
            ">;",
            "Lcom/sec/android/gallery3d/glrenderer/GLCanvas;",
            "II",
            "Landroid/graphics/Rect;",
            "FI)V"
        }
    .end annotation

    .prologue
    .line 494
    .local p1, "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v19, v0

    shl-int v11, v19, p7

    .line 495
    .local v11, "size":I
    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v11

    move/from16 v20, v0

    div-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v10, v0

    .line 496
    .local v10, "nBlockW":I
    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    sub-int v19, v19, v20

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v11

    move/from16 v20, v0

    div-float v19, v19, v20

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v9, v0

    .line 497
    .local v9, "nBlockH":I
    const/4 v4, 0x0

    .line 499
    .local v4, "direction":I
    move-object/from16 v0, p5

    iget v15, v0, Landroid/graphics/Rect;->left:I

    .line 500
    .local v15, "tx":I
    move-object/from16 v0, p5

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v16, v0

    .line 502
    .local v16, "ty":I
    const/4 v6, 0x0

    .line 503
    .local v6, "iX":I
    const/4 v7, 0x0

    .line 504
    .local v7, "iY":I
    const/4 v5, 0x0

    .line 505
    .local v5, "i":I
    const/4 v8, 0x0

    .line 506
    .local v8, "j":I
    const/16 v17, 0x0

    .local v17, "x":F
    const/16 v18, 0x0

    .line 507
    .local v18, "y":F
    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12}, Landroid/graphics/RectF;-><init>()V

    .line 508
    .local v12, "source":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    .line 509
    .local v13, "target":Landroid/graphics/RectF;
    :cond_0
    :goto_0
    if-ge v6, v10, :cond_2

    if-ge v7, v9, :cond_2

    .line 510
    invoke-virtual/range {p1 .. p1}, Landroid/util/LongSparseArray;->size()I

    move-result v19

    if-eqz v19, :cond_2

    .line 511
    move/from16 v0, v16

    move/from16 v1, p7

    invoke-static {v15, v0, v1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->makeKey(III)J

    move-result-wide v20

    move-object/from16 v0, p1

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 512
    .local v14, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v14, :cond_1

    iget v0, v14, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    move/from16 v19, v0

    const/16 v20, 0x4

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_1

    .line 513
    move/from16 v0, p3

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v5

    move/from16 v20, v0

    mul-float v20, v20, p6

    add-float v17, v19, v20

    .line 514
    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v19, v0

    int-to-float v0, v8

    move/from16 v20, v0

    mul-float v20, v20, p6

    add-float v18, v19, v20

    .line 515
    add-float v19, v17, p6

    add-float v20, v18, p6

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v13, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 516
    const/16 v19, 0x0

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v21, v0

    move/from16 v0, v21

    int-to-float v0, v0

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v22, v0

    move/from16 v0, v22

    int-to-float v0, v0

    move/from16 v22, v0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 517
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileBorder:I

    move/from16 v19, v0

    move/from16 v0, v19

    int-to-float v0, v0

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileBorder:I

    move/from16 v20, v0

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v12, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    .line 518
    move-object/from16 v0, p2

    invoke-interface {v0, v14, v12, v13}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 524
    :cond_1
    packed-switch v4, :pswitch_data_0

    goto/16 :goto_0

    .line 526
    :pswitch_0
    add-int/2addr v15, v11

    .line 527
    add-int/lit8 v5, v5, 0x1

    .line 528
    if-lt v5, v10, :cond_0

    .line 529
    const/4 v4, 0x1

    .line 530
    add-int/lit8 v5, v5, -0x1

    .line 531
    add-int/lit8 v8, v8, 0x1

    .line 532
    add-int/lit8 v7, v7, 0x1

    .line 533
    sub-int/2addr v15, v11

    .line 534
    add-int v16, v16, v11

    goto/16 :goto_0

    .line 538
    :pswitch_1
    add-int v16, v16, v11

    .line 539
    add-int/lit8 v8, v8, 0x1

    .line 540
    if-lt v8, v9, :cond_0

    .line 541
    const/4 v4, 0x2

    .line 542
    add-int/lit8 v5, v5, -0x1

    .line 543
    add-int/lit8 v8, v8, -0x1

    .line 544
    add-int/lit8 v10, v10, -0x1

    .line 545
    sub-int/2addr v15, v11

    .line 546
    sub-int v16, v16, v11

    goto/16 :goto_0

    .line 550
    :pswitch_2
    sub-int/2addr v15, v11

    .line 551
    add-int/lit8 v5, v5, -0x1

    .line 552
    if-ge v5, v6, :cond_0

    .line 553
    const/4 v4, 0x3

    .line 554
    add-int/lit8 v5, v5, 0x1

    .line 555
    add-int/lit8 v8, v8, -0x1

    .line 556
    add-int/lit8 v9, v9, -0x1

    .line 557
    add-int/2addr v15, v11

    .line 558
    sub-int v16, v16, v11

    goto/16 :goto_0

    .line 562
    :pswitch_3
    sub-int v16, v16, v11

    .line 563
    add-int/lit8 v8, v8, -0x1

    .line 564
    if-ge v8, v7, :cond_0

    .line 565
    const/4 v4, 0x0

    .line 566
    add-int/lit8 v5, v5, 0x1

    .line 567
    add-int/lit8 v8, v8, 0x1

    .line 568
    add-int/lit8 v6, v6, 0x1

    .line 569
    add-int/2addr v15, v11

    .line 570
    add-int v16, v16, v11

    goto/16 :goto_0

    .line 577
    .end local v14    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    :cond_2
    return-void

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private drawOnZoom(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;FLcom/sec/android/gallery3d/ui/ScreenNail;IIIII)V
    .locals 27
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "scale"    # F
    .param p4, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p5, "level"    # I
    .param p6, "imageWidth"    # I
    .param p7, "imageHeight"    # I
    .param p8, "offsetX"    # I
    .param p9, "offsetY"    # I

    .prologue
    .line 291
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v3, :cond_1

    .line 348
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v16

    .line 296
    .local v16, "largeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v16, :cond_4

    .line 297
    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 298
    .local v4, "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    if-eqz v4, :cond_3

    move-object/from16 v0, v16

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    if-eqz v3, :cond_3

    .line 299
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v5, 0x0

    aget v3, v3, v5

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/16 v23, 0x1

    aget v5, v5, v23

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v10

    .line 300
    .local v10, "largeImageLevel":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    shl-int v17, v3, v10

    .line 301
    .local v17, "largeImageTileSize":I
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    shl-int v21, v3, p5

    .line 302
    .local v21, "size":I
    move/from16 v0, v21

    int-to-float v3, v0

    mul-float v18, v3, p3

    .line 303
    .local v18, "length":F
    const/4 v3, 0x1

    sub-int v5, v10, p5

    shl-int/2addr v3, v5

    int-to-float v3, v3

    mul-float v9, v18, v3

    .line 304
    .local v9, "largeImageLength":F
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 305
    .local v8, "drawRect":Landroid/graphics/Rect;
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->left:I

    div-int v5, v5, v17

    mul-int v5, v5, v17

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v8, Landroid/graphics/Rect;->left:I

    .line 306
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    div-int v5, v5, v17

    mul-int v5, v5, v17

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    iput v3, v8, Landroid/graphics/Rect;->top:I

    .line 307
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v5, v5, -0x1

    div-int v5, v5, v17

    mul-int v5, v5, v17

    add-int v5, v5, v17

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v8, Landroid/graphics/Rect;->right:I

    .line 308
    move-object/from16 v0, v16

    iget v3, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v5, v5, -0x1

    div-int v5, v5, v17

    mul-int v5, v5, v17

    add-int v5, v5, v17

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v8, Landroid/graphics/Rect;->bottom:I

    .line 310
    iget v3, v8, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    shl-int v5, v5, p5

    div-int v14, v3, v5

    .line 311
    .local v14, "diffX":I
    iget v3, v8, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    shl-int v5, v5, p5

    div-int v15, v3, v5

    .line 313
    .local v15, "diffY":I
    move/from16 v0, p8

    int-to-float v3, v0

    int-to-float v5, v14

    mul-float v5, v5, v18

    add-float/2addr v3, v5

    float-to-int v6, v3

    .line 314
    .local v6, "newOffsetX":I
    move/from16 v0, p9

    int-to-float v3, v0

    int-to-float v5, v15

    mul-float v5, v5, v18

    add-float/2addr v3, v5

    float-to-int v7, v3

    .line 315
    .local v7, "newOffsetY":I
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    if-eqz v3, :cond_2

    .line 316
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->getProgress()F

    move-result v11

    .line 317
    .local v11, "alpha":F
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getAlpha()F

    move-result v12

    .line 318
    .local v12, "canvasAlpha":F
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p6

    int-to-float v5, v0

    div-float v19, v3, v5

    .line 319
    .local v19, "scaleX":F
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p7

    int-to-float v5, v0

    div-float v20, v3, v5

    .line 320
    .local v20, "scaleY":F
    new-instance v22, Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    mul-float v3, v3, v19

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    mul-float v5, v5, v20

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v19

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v20

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v3, v5, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 321
    .local v22, "source":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    move/from16 v0, p8

    int-to-float v3, v0

    move/from16 v0, p9

    int-to-float v5, v0

    move/from16 v0, p8

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, p3

    add-float v23, v23, v24

    move/from16 v0, p9

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, p3

    add-float v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v13, v3, v5, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 322
    .local v13, "dest":Landroid/graphics/RectF;
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2, v13}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 323
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 324
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    .line 325
    :try_start_0
    invoke-direct/range {v3 .. v10}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawDecodedTiles(Landroid/util/LongSparseArray;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILandroid/graphics/Rect;FI)V

    .line 326
    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 327
    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    goto/16 :goto_0

    .line 326
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 329
    .end local v11    # "alpha":F
    .end local v12    # "canvasAlpha":F
    .end local v13    # "dest":Landroid/graphics/RectF;
    .end local v19    # "scaleX":F
    .end local v20    # "scaleY":F
    .end local v22    # "source":Landroid/graphics/RectF;
    :cond_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    move-object/from16 v3, p0

    move-object/from16 v5, p1

    .line 330
    :try_start_2
    invoke-direct/range {v3 .. v10}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawDecodedTiles(Landroid/util/LongSparseArray;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILandroid/graphics/Rect;FI)V

    .line 331
    monitor-exit v23

    goto/16 :goto_0

    :catchall_1
    move-exception v3

    monitor-exit v23
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v3

    .line 334
    .end local v6    # "newOffsetX":I
    .end local v7    # "newOffsetY":I
    .end local v8    # "drawRect":Landroid/graphics/Rect;
    .end local v9    # "largeImageLength":F
    .end local v10    # "largeImageLevel":I
    .end local v14    # "diffX":I
    .end local v15    # "diffY":I
    .end local v17    # "largeImageTileSize":I
    .end local v18    # "length":F
    .end local v21    # "size":I
    :cond_3
    if-eqz p4, :cond_0

    .line 335
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p6

    int-to-float v5, v0

    div-float v19, v3, v5

    .line 336
    .restart local v19    # "scaleX":F
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p7

    int-to-float v5, v0

    div-float v20, v3, v5

    .line 337
    .restart local v20    # "scaleY":F
    new-instance v22, Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    mul-float v3, v3, v19

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    mul-float v5, v5, v20

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v19

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v20

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v3, v5, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 338
    .restart local v22    # "source":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    move/from16 v0, p8

    int-to-float v3, v0

    move/from16 v0, p9

    int-to-float v5, v0

    move/from16 v0, p8

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, p3

    add-float v23, v23, v24

    move/from16 v0, p9

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, p3

    add-float v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v13, v3, v5, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 339
    .restart local v13    # "dest":Landroid/graphics/RectF;
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2, v13}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 341
    .end local v4    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .end local v13    # "dest":Landroid/graphics/RectF;
    .end local v19    # "scaleX":F
    .end local v20    # "scaleY":F
    .end local v22    # "source":Landroid/graphics/RectF;
    :cond_4
    if-eqz p4, :cond_0

    .line 342
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getWidth()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p6

    int-to-float v5, v0

    div-float v19, v3, v5

    .line 343
    .restart local v19    # "scaleX":F
    invoke-interface/range {p4 .. p4}, Lcom/sec/android/gallery3d/ui/ScreenNail;->getHeight()I

    move-result v3

    int-to-float v3, v3

    move/from16 v0, p7

    int-to-float v5, v0

    div-float v20, v3, v5

    .line 344
    .restart local v20    # "scaleY":F
    new-instance v22, Landroid/graphics/RectF;

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    mul-float v3, v3, v19

    move-object/from16 v0, p2

    iget v5, v0, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    mul-float v5, v5, v20

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v23, v0

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    mul-float v23, v23, v19

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, v20

    move-object/from16 v0, v22

    move/from16 v1, v23

    move/from16 v2, v24

    invoke-direct {v0, v3, v5, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 345
    .restart local v22    # "source":Landroid/graphics/RectF;
    new-instance v13, Landroid/graphics/RectF;

    move/from16 v0, p8

    int-to-float v3, v0

    move/from16 v0, p9

    int-to-float v5, v0

    move/from16 v0, p8

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    mul-float v24, v24, p3

    add-float v23, v23, v24

    move/from16 v0, p9

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v25, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v26, v0

    sub-int v25, v25, v26

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    mul-float v25, v25, p3

    add-float v24, v24, v25

    move/from16 v0, v23

    move/from16 v1, v24

    invoke-direct {v13, v3, v5, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 346
    .restart local v13    # "dest":Landroid/graphics/RectF;
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, v22

    invoke-interface {v0, v1, v2, v13}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto/16 :goto_0
.end method

.method private static makeKey(III)J
    .locals 7
    .param p0, "x"    # I
    .param p1, "y"    # I
    .param p2, "level"    # I

    .prologue
    const/16 v6, 0x10

    .line 76
    int-to-long v0, p0

    .line 77
    .local v0, "result":J
    shl-long v2, v0, v6

    int-to-long v4, p1

    or-long v0, v2, v4

    .line 78
    shl-long v2, v0, v6

    int-to-long v4, p2

    or-long v0, v2, v4

    .line 79
    return-wide v0
.end method

.method private static mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFFFF)V
    .locals 4
    .param p0, "output"    # Landroid/graphics/RectF;
    .param p1, "src"    # Landroid/graphics/RectF;
    .param p2, "x0"    # F
    .param p3, "y0"    # F
    .param p4, "x"    # F
    .param p5, "y"    # F
    .param p6, "scaleX"    # F
    .param p7, "scaleY"    # F

    .prologue
    .line 665
    iget v0, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p2

    mul-float/2addr v0, p6

    add-float/2addr v0, p4

    iget v1, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, p3

    mul-float/2addr v1, p7

    add-float/2addr v1, p5

    iget v2, p1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, p2

    mul-float/2addr v2, p6

    add-float/2addr v2, p4

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v3, p3

    mul-float/2addr v3, p7

    add-float/2addr v3, p5

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 669
    return-void
.end method


# virtual methods
.method public centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V
    .locals 19
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "scale"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I
    .param p9, "backup"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p10, "imageWidth"    # I
    .param p11, "imageHeight"    # I

    .prologue
    .line 216
    move-object/from16 v2, p0

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move/from16 v5, p6

    move-object/from16 v6, p9

    move/from16 v7, p4

    move/from16 v8, p10

    move/from16 v9, p11

    move/from16 v10, p7

    move/from16 v11, p8

    invoke-direct/range {v2 .. v11}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawOnZoom(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;FLcom/sec/android/gallery3d/ui/ScreenNail;IIIII)V

    .line 217
    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v9, v2, p6

    .line 218
    .local v9, "length":F
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move/from16 v0, p5

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v18, v0

    .line 219
    .local v18, "nBlockW":I
    move-object/from16 v0, p3

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p3

    iget v3, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    move/from16 v0, p5

    int-to-float v3, v0

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    move/from16 v17, v0

    .line 220
    .local v17, "nBlockH":I
    const/4 v12, 0x0

    .line 222
    .local v12, "direction":I
    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    .line 223
    .local v4, "tx":I
    move-object/from16 v0, p3

    iget v5, v0, Landroid/graphics/Rect;->top:I

    .line 225
    .local v5, "ty":I
    const/4 v14, 0x0

    .line 226
    .local v14, "iX":I
    const/4 v15, 0x0

    .line 227
    .local v15, "iY":I
    const/4 v13, 0x0

    .line 228
    .local v13, "i":I
    const/16 v16, 0x0

    .line 229
    .local v16, "j":I
    const/4 v7, 0x0

    .local v7, "x":F
    const/4 v8, 0x0

    .line 230
    .local v8, "y":F
    :cond_0
    :goto_0
    move/from16 v0, v18

    if-ge v14, v0, :cond_1

    move/from16 v0, v17

    if-ge v15, v0, :cond_1

    .line 231
    move/from16 v0, p7

    int-to-float v2, v0

    int-to-float v3, v13

    mul-float/2addr v3, v9

    add-float v7, v2, v3

    .line 232
    move/from16 v0, p8

    int-to-float v2, v0

    move/from16 v0, v16

    int-to-float v3, v0

    mul-float/2addr v3, v9

    add-float v8, v2, v3

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v6, p4

    .line 233
    invoke-virtual/range {v2 .. v9}, Lcom/sec/android/gallery3d/ui/TileImageView;->drawTile(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIFFF)V

    .line 235
    packed-switch v12, :pswitch_data_0

    goto :goto_0

    .line 237
    :pswitch_0
    add-int v4, v4, p5

    .line 238
    add-int/lit8 v13, v13, 0x1

    .line 239
    move/from16 v0, v18

    if-lt v13, v0, :cond_0

    .line 240
    const/4 v12, 0x1

    .line 241
    add-int/lit8 v13, v13, -0x1

    .line 242
    add-int/lit8 v16, v16, 0x1

    .line 243
    add-int/lit8 v15, v15, 0x1

    .line 244
    sub-int v4, v4, p5

    .line 245
    add-int v5, v5, p5

    goto :goto_0

    .line 249
    :pswitch_1
    add-int v5, v5, p5

    .line 250
    add-int/lit8 v16, v16, 0x1

    .line 251
    move/from16 v0, v16

    move/from16 v1, v17

    if-lt v0, v1, :cond_0

    .line 252
    const/4 v12, 0x2

    .line 253
    add-int/lit8 v13, v13, -0x1

    .line 254
    add-int/lit8 v16, v16, -0x1

    .line 255
    add-int/lit8 v18, v18, -0x1

    .line 256
    sub-int v4, v4, p5

    .line 257
    sub-int v5, v5, p5

    goto :goto_0

    .line 261
    :pswitch_2
    sub-int v4, v4, p5

    .line 262
    add-int/lit8 v13, v13, -0x1

    .line 263
    if-ge v13, v14, :cond_0

    .line 264
    const/4 v12, 0x3

    .line 265
    add-int/lit8 v13, v13, 0x1

    .line 266
    add-int/lit8 v16, v16, -0x1

    .line 267
    add-int/lit8 v17, v17, -0x1

    .line 268
    add-int v4, v4, p5

    .line 269
    sub-int v5, v5, p5

    goto :goto_0

    .line 273
    :pswitch_3
    sub-int v5, v5, p5

    .line 274
    add-int/lit8 v16, v16, -0x1

    .line 275
    move/from16 v0, v16

    if-ge v0, v15, :cond_0

    .line 276
    const/4 v12, 0x0

    .line 277
    add-int/lit8 v13, v13, 0x1

    .line 278
    add-int/lit8 v16, v16, 0x1

    .line 279
    add-int/lit8 v14, v14, 0x1

    .line 280
    add-int v4, v4, p5

    .line 281
    add-int v5, v5, p5

    goto :goto_0

    .line 288
    :cond_1
    return-void

    .line 235
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public drawBlended(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;F)V
    .locals 2
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "texture"    # Lcom/sec/android/gallery3d/glrenderer/BasicTexture;
    .param p3, "source"    # Landroid/graphics/RectF;
    .param p4, "target"    # Landroid/graphics/RectF;
    .param p5, "alpha"    # F

    .prologue
    .line 92
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getAlpha()F

    move-result v0

    .line 93
    .local v0, "canvasAlpha":F
    mul-float v1, v0, p5

    invoke-interface {p1, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 94
    invoke-interface {p1, p2, p3, p4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 95
    invoke-interface {p1, v0}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 96
    return-void
.end method

.method public drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z
    .locals 24
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "mRealOffsetX"    # I
    .param p3, "mRealOffsetY"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I
    .param p6, "scale"    # F
    .param p7, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p8, "level"    # I

    .prologue
    .line 408
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v2, :cond_0

    .line 409
    const/16 v20, 0x1

    .line 489
    :goto_0
    return v20

    .line 412
    :cond_0
    const/16 v20, 0x1

    .line 413
    .local v20, "renderComplete":Z
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v16

    .line 414
    .local v16, "largeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v16, :cond_f

    .line 415
    move-object/from16 v0, v16

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-nez v2, :cond_3

    .line 417
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v18

    .line 418
    .local v18, "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v19

    .line 420
    .local v19, "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v18, :cond_2

    move-object/from16 v0, v18

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-eqz v2, :cond_2

    .line 421
    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/LargeImage;->stopDecodeThread(Z)V

    .line 426
    :cond_1
    :goto_1
    invoke-virtual/range {v16 .. v16}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    .line 427
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 428
    const/16 v20, 0x1

    goto :goto_0

    .line 422
    :cond_2
    if-eqz v19, :cond_1

    move-object/from16 v0, v19

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-eqz v2, :cond_1

    .line 423
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/LargeImage;->stopDecodeThread(Z)V

    goto :goto_1

    .line 430
    .end local v18    # "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    .end local v19    # "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :cond_3
    move-object/from16 v0, v16

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    if-nez v2, :cond_4

    .line 431
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 432
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 435
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v12

    .line 436
    .local v12, "currentIndex":I
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    add-int/lit8 v3, v12, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v18

    .line 437
    .restart local v18    # "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :goto_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    if-eqz v2, :cond_b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v17

    .line 438
    .local v17, "nextItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_3
    if-eqz v18, :cond_5

    .line 439
    move-object/from16 v0, v18

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-nez v2, :cond_5

    .line 440
    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    .line 444
    :cond_5
    const/16 v19, 0x0

    .line 445
    .restart local v19    # "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v17, :cond_6

    if-eqz v18, :cond_7

    move-object/from16 v0, v18

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    if-eqz v2, :cond_7

    .line 446
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v19

    .line 447
    :goto_4
    if-eqz v19, :cond_7

    .line 448
    move-object/from16 v0, v19

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    if-nez v2, :cond_7

    .line 449
    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    .line 454
    :cond_7
    move-object/from16 v0, v16

    iget-object v13, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 455
    .local v13, "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    if-eqz v13, :cond_e

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move/from16 v0, p8

    if-eq v0, v2, :cond_8

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    move/from16 v0, p8

    if-ne v0, v2, :cond_e

    .line 456
    :cond_8
    move-object/from16 v0, v16

    iget-object v15, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    .line 457
    .local v15, "lSize":[I
    move-object/from16 v0, v16

    iget v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    int-to-float v2, v2

    mul-float v21, v2, p6

    .line 458
    .local v21, "viewH":F
    move-object/from16 v0, v16

    iget v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    int-to-float v2, v2

    mul-float v22, v2, p6

    .line 459
    .local v22, "viewW":F
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 460
    .local v9, "lLevelmin":I
    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/4 v3, 0x0

    aget v3, v15, v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static/range {v21 .. v22}, Ljava/lang/Math;->min(FF)F

    move-result v3

    const/4 v4, 0x1

    aget v4, v15, v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v14

    .line 461
    .local v14, "lScale":F
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    int-to-float v2, v2

    mul-float v8, v2, v14

    .line 463
    .local v8, "lLength":F
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    if-eqz v2, :cond_d

    .line 464
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->getProgress()F

    move-result v10

    .line 465
    .local v10, "alpha":F
    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v2, v10, v2

    if-gez v2, :cond_9

    .line 466
    const/16 v20, 0x0

    .line 468
    :cond_9
    invoke-interface/range {p1 .. p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->getAlpha()F

    move-result v11

    .line 469
    .local v11, "canvasAlpha":F
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 470
    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    .line 471
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 472
    :try_start_0
    new-instance v7, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v7, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v2, p0

    move-object v3, v13

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v2 .. v9}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawDecodedTiles(Landroid/util/LongSparseArray;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILandroid/graphics/Rect;FI)V

    .line 473
    monitor-exit v23
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    move-object/from16 v0, p1

    invoke-interface {v0, v11}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->setAlpha(F)V

    goto/16 :goto_0

    .line 436
    .end local v8    # "lLength":F
    .end local v9    # "lLevelmin":I
    .end local v10    # "alpha":F
    .end local v11    # "canvasAlpha":F
    .end local v13    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .end local v14    # "lScale":F
    .end local v15    # "lSize":[I
    .end local v17    # "nextItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v18    # "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    .end local v19    # "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    .end local v21    # "viewH":F
    .end local v22    # "viewW":F
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    add-int/lit8 v3, v12, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v18

    goto/16 :goto_2

    .line 437
    .restart local v18    # "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v17

    goto/16 :goto_3

    .line 446
    .restart local v17    # "nextItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v19    # "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v19

    goto/16 :goto_4

    .line 473
    .restart local v8    # "lLength":F
    .restart local v9    # "lLevelmin":I
    .restart local v10    # "alpha":F
    .restart local v11    # "canvasAlpha":F
    .restart local v13    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .restart local v14    # "lScale":F
    .restart local v15    # "lSize":[I
    .restart local v21    # "viewH":F
    .restart local v22    # "viewW":F
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v23
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    .line 476
    .end local v10    # "alpha":F
    .end local v11    # "canvasAlpha":F
    :cond_d
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    move-object/from16 v23, v0

    monitor-enter v23

    .line 477
    :try_start_2
    new-instance v7, Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move/from16 v0, p4

    move/from16 v1, p5

    invoke-direct {v7, v2, v3, v0, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v2, p0

    move-object v3, v13

    move-object/from16 v4, p1

    move/from16 v5, p2

    move/from16 v6, p3

    invoke-direct/range {v2 .. v9}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawDecodedTiles(Landroid/util/LongSparseArray;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IILandroid/graphics/Rect;FI)V

    .line 478
    monitor-exit v23

    goto/16 :goto_0

    :catchall_1
    move-exception v2

    monitor-exit v23
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    .line 481
    .end local v8    # "lLength":F
    .end local v9    # "lLevelmin":I
    .end local v14    # "lScale":F
    .end local v15    # "lSize":[I
    .end local v21    # "viewH":F
    .end local v22    # "viewW":F
    :cond_e
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 482
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 488
    .end local v12    # "currentIndex":I
    .end local v13    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .end local v17    # "nextItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v18    # "nextLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    .end local v19    # "prevLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :cond_f
    move/from16 v0, p4

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v6

    move/from16 v0, p5

    int-to-float v2, v0

    mul-float v2, v2, p6

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    move-object/from16 v2, p7

    move-object/from16 v3, p1

    move/from16 v4, p2

    move/from16 v5, p3

    invoke-interface/range {v2 .. v7}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    .line 489
    const/16 v20, 0x1

    goto/16 :goto_0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V
    .locals 28
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backup"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "cx"    # I
    .param p5, "cy"    # I
    .param p6, "drawW"    # I
    .param p7, "drawH"    # I
    .param p8, "rotation"    # I
    .param p9, "diff"    # I

    .prologue
    .line 596
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v6, :cond_0

    .line 647
    :goto_0
    return-void

    .line 600
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v7

    add-int v7, v7, p9

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v21

    .line 601
    .local v21, "largeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v21, :cond_6

    invoke-virtual/range {v21 .. v21}, Lcom/sec/samsung/gallery/decoder/LargeImage;->isLocalImage()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 602
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    move-object/from16 v18, v0

    .line 603
    .local v18, "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    if-eqz v18, :cond_5

    move-object/from16 v0, v21

    iget-boolean v6, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    if-eqz v6, :cond_5

    .line 604
    const/4 v10, 0x0

    .line 605
    .local v10, "scaleX":F
    const/4 v11, 0x0

    .line 606
    .local v11, "scaleY":F
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    move-object/from16 v24, v0

    .line 607
    .local v24, "tSize":[I
    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    move/from16 v19, v0

    .line 608
    .local v19, "imageHeight":I
    move-object/from16 v0, v21

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    move/from16 v20, v0

    .line 610
    .local v20, "imageWidth":I
    move/from16 v0, p6

    move/from16 v1, p7

    if-le v0, v1, :cond_2

    .line 611
    move/from16 v0, p6

    int-to-float v6, v0

    const/4 v7, 0x0

    aget v7, v24, v7

    int-to-float v7, v7

    div-float v10, v6, v7

    .line 612
    move/from16 v0, p7

    int-to-float v6, v0

    const/4 v7, 0x1

    aget v7, v24, v7

    int-to-float v7, v7

    div-float v11, v6, v7

    .line 618
    :goto_1
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    .line 619
    .local v5, "source":Landroid/graphics/RectF;
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    .line 620
    .local v4, "target":Landroid/graphics/RectF;
    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v22

    .line 621
    .local v22, "level":I
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    shl-int v23, v6, v22

    .line 622
    .local v23, "size":I
    move-object/from16 v0, v21

    iget-object v12, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    monitor-enter v12

    .line 624
    const/16 v27, 0x0

    .local v27, "ty":I
    :goto_2
    move/from16 v0, v27

    move/from16 v1, v19

    if-ge v0, v1, :cond_4

    .line 625
    const/16 v26, 0x0

    .local v26, "tx":I
    :goto_3
    move/from16 v0, v26

    move/from16 v1, v20

    if-ge v0, v1, :cond_3

    .line 626
    :try_start_0
    invoke-virtual/range {v18 .. v18}, Landroid/util/LongSparseArray;->size()I

    move-result v6

    if-lez v6, :cond_4

    .line 627
    move/from16 v0, v26

    move/from16 v1, v27

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->makeKey(III)J

    move-result-wide v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v6, v7}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 628
    .local v25, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v25, :cond_1

    .line 629
    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    int-to-float v8, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    int-to-float v9, v9

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;->set(FFFF)V

    .line 630
    move-object/from16 v0, v25

    iget v6, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mX:I

    shr-int v6, v6, v22

    int-to-float v6, v6

    move-object/from16 v0, v25

    iget v7, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mY:I

    shr-int v7, v7, v22

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 631
    const/4 v6, 0x0

    const/4 v7, 0x0

    move/from16 v0, p4

    int-to-float v8, v0

    move/from16 v0, p5

    int-to-float v9, v0

    invoke-static/range {v4 .. v11}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;FFFFFF)V

    .line 632
    move-object/from16 v0, v25

    iget v6, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mX:I

    shr-int v6, v6, v22

    neg-int v6, v6

    int-to-float v6, v6

    move-object/from16 v0, v25

    iget v7, v0, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mY:I

    shr-int v7, v7, v22

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Landroid/graphics/RectF;->offset(FF)V

    .line 633
    move-object/from16 v0, p1

    move-object/from16 v1, v25

    invoke-interface {v0, v1, v5, v4}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 625
    :cond_1
    add-int v26, v26, v23

    goto :goto_3

    .line 614
    .end local v4    # "target":Landroid/graphics/RectF;
    .end local v5    # "source":Landroid/graphics/RectF;
    .end local v22    # "level":I
    .end local v23    # "size":I
    .end local v25    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    .end local v26    # "tx":I
    .end local v27    # "ty":I
    :cond_2
    move/from16 v0, p6

    int-to-float v6, v0

    const/4 v7, 0x1

    aget v7, v24, v7

    int-to-float v7, v7

    div-float v10, v6, v7

    .line 615
    move/from16 v0, p7

    int-to-float v6, v0

    const/4 v7, 0x0

    aget v7, v24, v7

    int-to-float v7, v7

    div-float v11, v6, v7

    goto/16 :goto_1

    .line 624
    .restart local v4    # "target":Landroid/graphics/RectF;
    .restart local v5    # "source":Landroid/graphics/RectF;
    .restart local v22    # "level":I
    .restart local v23    # "size":I
    .restart local v26    # "tx":I
    .restart local v27    # "ty":I
    :cond_3
    add-int v27, v27, v23

    goto/16 :goto_2

    .line 640
    .end local v26    # "tx":I
    :cond_4
    :try_start_1
    monitor-exit v12

    goto/16 :goto_0

    :catchall_0
    move-exception v6

    monitor-exit v12
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .end local v4    # "target":Landroid/graphics/RectF;
    .end local v5    # "source":Landroid/graphics/RectF;
    .end local v10    # "scaleX":F
    .end local v11    # "scaleY":F
    .end local v19    # "imageHeight":I
    .end local v20    # "imageWidth":I
    .end local v22    # "level":I
    .end local v23    # "size":I
    .end local v24    # "tSize":[I
    .end local v27    # "ty":I
    :cond_5
    move-object/from16 v12, p3

    move-object/from16 v13, p1

    move/from16 v14, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move/from16 v17, p7

    .line 642
    invoke-interface/range {v12 .. v17}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_0

    .end local v18    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    :cond_6
    move-object/from16 v12, p3

    move-object/from16 v13, p1

    move/from16 v14, p4

    move/from16 v15, p5

    move/from16 v16, p6

    move/from16 v17, p7

    .line 645
    invoke-interface/range {v12 .. v17}, Lcom/sec/android/gallery3d/ui/ScreenNail;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)V

    goto/16 :goto_0
.end method

.method public drawTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)Z
    .locals 42
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "level"    # I
    .param p3, "size"    # I
    .param p4, "mRealOffsetX"    # I
    .param p5, "mRealOffsetY"    # I

    .prologue
    .line 99
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v38, v0

    if-nez v38, :cond_0

    .line 100
    const/16 v38, 0x0

    .line 212
    :goto_0
    return v38

    .line 103
    :cond_0
    const/4 v8, 0x0

    .line 104
    .local v8, "decodedTileListEmpty":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v39

    invoke-virtual/range {v38 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v15

    .line 105
    .local v15, "largeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v15, :cond_b

    .line 106
    iget-boolean v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    move/from16 v38, v0

    if-nez v38, :cond_1

    .line 107
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    .line 109
    :cond_1
    iget-object v7, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodedTileList:Landroid/util/LongSparseArray;

    .line 110
    .local v7, "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    if-eqz v7, :cond_f

    iget-object v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    move-object/from16 v38, v0

    const/16 v39, 0x0

    aget v38, v38, v39

    move/from16 v0, p2

    move/from16 v1, v38

    if-eq v0, v1, :cond_2

    iget-object v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    move-object/from16 v38, v0

    const/16 v39, 0x1

    aget v38, v38, v39

    move/from16 v0, p2

    move/from16 v1, v38

    if-ne v0, v1, :cond_f

    .line 111
    :cond_2
    iget-object v14, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mSize:[I

    .line 112
    .local v14, "lSize":[I
    iget-object v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    move-object/from16 v38, v0

    const/16 v39, 0x0

    aget v38, v38, v39

    iget-object v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mLevel:[I

    move-object/from16 v39, v0

    const/16 v40, 0x1

    aget v39, v39, v40

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->min(II)I

    move-result v17

    .line 114
    .local v17, "mLevelmin":I
    iget v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageHeight:I

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentScale:F

    move/from16 v39, v0

    mul-float v33, v38, v39

    .line 115
    .local v33, "viewH":F
    iget v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mImageWidth:I

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentScale:F

    move/from16 v39, v0

    mul-float v34, v38, v39

    .line 116
    .local v34, "viewW":F
    invoke-static/range {v33 .. v34}, Ljava/lang/Math;->max(FF)F

    move-result v38

    const/16 v39, 0x0

    aget v39, v14, v39

    move/from16 v0, v39

    int-to-float v0, v0

    move/from16 v39, v0

    div-float v38, v38, v39

    invoke-static/range {v33 .. v34}, Ljava/lang/Math;->min(FF)F

    move-result v39

    const/16 v40, 0x1

    aget v40, v14, v40

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    div-float v39, v39, v40

    invoke-static/range {v38 .. v39}, Ljava/lang/Math;->min(FF)F

    move-result v22

    .line 117
    .local v22, "scale":F
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v38, v0

    move/from16 v0, v38

    int-to-float v0, v0

    move/from16 v38, v0

    mul-float v13, v38, v22

    .line 118
    .local v13, "lLength":F
    const/16 v38, 0x1

    move/from16 v0, v38

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDecodingComplete:Z

    .line 119
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v38, v0

    shl-int p3, v38, v17

    .line 120
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mImageW:I

    move/from16 v38, v0

    add-int/lit8 v38, v38, -0x1

    div-int v38, v38, p3

    add-int/lit8 v19, v38, 0x1

    .line 121
    .local v19, "nBlockW":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mImageH:I

    move/from16 v38, v0

    add-int/lit8 v38, v38, -0x1

    div-int v38, v38, p3

    add-int/lit8 v18, v38, 0x1

    .line 122
    .local v18, "nBlockH":I
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v38

    add-int/lit8 v38, v38, 0x1

    div-int/lit8 v20, v38, 0x2

    .line 124
    .local v20, "numOfRect":I
    add-int/lit8 v38, v20, -0x1

    mul-int/lit8 v38, v38, 0x2

    sub-int v24, v19, v38

    .line 125
    .local v24, "startX":I
    add-int/lit8 v38, v20, -0x1

    mul-int/lit8 v38, v38, 0x2

    sub-int v25, v18, v38

    .line 127
    .local v25, "startY":I
    new-instance v23, Landroid/graphics/RectF;

    invoke-direct/range {v23 .. v23}, Landroid/graphics/RectF;-><init>()V

    .line 128
    .local v23, "source":Landroid/graphics/RectF;
    new-instance v27, Landroid/graphics/RectF;

    invoke-direct/range {v27 .. v27}, Landroid/graphics/RectF;-><init>()V

    .line 129
    .local v27, "target":Landroid/graphics/RectF;
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    move/from16 v0, v20

    if-ge v10, v0, :cond_a

    .line 130
    mul-int/lit8 v38, v10, 0x2

    add-int v35, v24, v38

    .line 131
    .local v35, "widthOfRect":I
    mul-int/lit8 v38, v10, 0x2

    add-int v9, v25, v38

    .line 133
    .local v9, "heightOfRect":I
    add-int/lit8 v38, v20, -0x1

    sub-int v16, v38, v10

    .line 134
    .local v16, "leftOfRect":I
    add-int/lit8 v38, v20, -0x1

    sub-int v30, v38, v10

    .line 135
    .local v30, "topOfRect":I
    add-int v38, v16, v35

    add-int/lit8 v21, v38, -0x1

    .line 136
    .local v21, "rightOfRect":I
    add-int v38, v30, v9

    add-int/lit8 v6, v38, -0x1

    .line 138
    .local v6, "bottomOfRect":I
    move/from16 v11, v16

    .line 139
    .local v11, "iX":I
    move/from16 v12, v30

    .line 140
    .local v12, "iY":I
    const/16 v26, 0x0

    .line 141
    .local v26, "state":I
    :cond_3
    :goto_2
    const/16 v38, 0x4

    move/from16 v0, v26

    move/from16 v1, v38

    if-ge v0, v1, :cond_9

    .line 142
    packed-switch v26, :pswitch_data_0

    .line 173
    :goto_3
    mul-int v38, p3, v11

    add-int/lit8 v31, v38, 0x0

    .line 174
    .local v31, "tx":I
    mul-int v38, p3, v12

    add-int/lit8 v32, v38, 0x0

    .line 175
    .local v32, "ty":I
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v38

    if-eqz v38, :cond_8

    .line 176
    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->makeKey(III)J

    move-result-wide v38

    move-wide/from16 v0, v38

    invoke-virtual {v7, v0, v1}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    .line 177
    .local v29, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    if-eqz v29, :cond_3

    .line 178
    move/from16 v0, p4

    int-to-float v0, v0

    move/from16 v38, v0

    int-to-float v0, v11

    move/from16 v39, v0

    mul-float v39, v39, v13

    add-float v36, v38, v39

    .line 179
    .local v36, "x":F
    move/from16 v0, p5

    int-to-float v0, v0

    move/from16 v38, v0

    int-to-float v0, v12

    move/from16 v39, v0

    mul-float v39, v39, v13

    add-float v37, v38, v39

    .line 180
    .local v37, "y":F
    add-float v38, v36, v13

    add-float v39, v37, v13

    move-object/from16 v0, v27

    move/from16 v1, v36

    move/from16 v2, v37

    move/from16 v3, v38

    move/from16 v4, v39

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 181
    const/16 v38, 0x0

    const/16 v39, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v40, v0

    move/from16 v0, v40

    int-to-float v0, v0

    move/from16 v40, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    move/from16 v41, v0

    move/from16 v0, v41

    int-to-float v0, v0

    move/from16 v41, v0

    move-object/from16 v0, v23

    move/from16 v1, v38

    move/from16 v2, v39

    move/from16 v3, v40

    move/from16 v4, v41

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 182
    move-object/from16 v0, p1

    move-object/from16 v1, v29

    move-object/from16 v2, v23

    move-object/from16 v3, v27

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->drawTexture(Lcom/sec/android/gallery3d/glrenderer/BasicTexture;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto/16 :goto_2

    .line 144
    .end local v29    # "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    .end local v31    # "tx":I
    .end local v32    # "ty":I
    .end local v36    # "x":F
    .end local v37    # "y":F
    :pswitch_0
    move/from16 v0, v21

    if-ge v11, v0, :cond_4

    .line 145
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_3

    .line 147
    :cond_4
    add-int/lit8 v26, v26, 0x1

    .line 149
    goto/16 :goto_3

    .line 151
    :pswitch_1
    if-ge v12, v6, :cond_5

    .line 152
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_3

    .line 154
    :cond_5
    add-int/lit8 v26, v26, 0x1

    .line 156
    goto/16 :goto_3

    .line 158
    :pswitch_2
    move/from16 v0, v16

    if-le v11, v0, :cond_6

    .line 159
    add-int/lit8 v11, v11, -0x1

    goto/16 :goto_3

    .line 161
    :cond_6
    add-int/lit8 v26, v26, 0x1

    .line 163
    goto/16 :goto_3

    .line 165
    :pswitch_3
    move/from16 v0, v30

    if-le v12, v0, :cond_7

    .line 166
    add-int/lit8 v12, v12, -0x1

    goto/16 :goto_3

    .line 168
    :cond_7
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_3

    .line 185
    .restart local v31    # "tx":I
    .restart local v32    # "ty":I
    :cond_8
    const/4 v8, 0x1

    .line 190
    .end local v31    # "tx":I
    .end local v32    # "ty":I
    :cond_9
    if-eqz v8, :cond_c

    .line 194
    .end local v6    # "bottomOfRect":I
    .end local v9    # "heightOfRect":I
    .end local v11    # "iX":I
    .end local v12    # "iY":I
    .end local v16    # "leftOfRect":I
    .end local v21    # "rightOfRect":I
    .end local v26    # "state":I
    .end local v30    # "topOfRect":I
    .end local v35    # "widthOfRect":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v39

    add-int/lit8 v39, v39, 0x1

    invoke-virtual/range {v38 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v15

    .line 195
    if-eqz v15, :cond_d

    iget-boolean v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    move/from16 v38, v0

    if-nez v38, :cond_d

    .line 196
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    .line 212
    .end local v7    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .end local v10    # "i":I
    .end local v13    # "lLength":F
    .end local v14    # "lSize":[I
    .end local v17    # "mLevelmin":I
    .end local v18    # "nBlockH":I
    .end local v19    # "nBlockW":I
    .end local v20    # "numOfRect":I
    .end local v22    # "scale":F
    .end local v23    # "source":Landroid/graphics/RectF;
    .end local v24    # "startX":I
    .end local v25    # "startY":I
    .end local v27    # "target":Landroid/graphics/RectF;
    .end local v33    # "viewH":F
    .end local v34    # "viewW":F
    :cond_b
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDecodingComplete:Z

    move/from16 v38, v0

    goto/16 :goto_0

    .line 129
    .restart local v6    # "bottomOfRect":I
    .restart local v7    # "decodedTileList":Landroid/util/LongSparseArray;, "Landroid/util/LongSparseArray<Lcom/sec/samsung/gallery/decoder/LargeImageTile;>;"
    .restart local v9    # "heightOfRect":I
    .restart local v10    # "i":I
    .restart local v11    # "iX":I
    .restart local v12    # "iY":I
    .restart local v13    # "lLength":F
    .restart local v14    # "lSize":[I
    .restart local v16    # "leftOfRect":I
    .restart local v17    # "mLevelmin":I
    .restart local v18    # "nBlockH":I
    .restart local v19    # "nBlockW":I
    .restart local v20    # "numOfRect":I
    .restart local v21    # "rightOfRect":I
    .restart local v22    # "scale":F
    .restart local v23    # "source":Landroid/graphics/RectF;
    .restart local v24    # "startX":I
    .restart local v25    # "startY":I
    .restart local v26    # "state":I
    .restart local v27    # "target":Landroid/graphics/RectF;
    .restart local v30    # "topOfRect":I
    .restart local v33    # "viewH":F
    .restart local v34    # "viewW":F
    .restart local v35    # "widthOfRect":I
    :cond_c
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 197
    .end local v6    # "bottomOfRect":I
    .end local v9    # "heightOfRect":I
    .end local v11    # "iX":I
    .end local v12    # "iY":I
    .end local v16    # "leftOfRect":I
    .end local v21    # "rightOfRect":I
    .end local v26    # "state":I
    .end local v30    # "topOfRect":I
    .end local v35    # "widthOfRect":I
    :cond_d
    if-eqz v15, :cond_e

    iget-boolean v0, v15, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingComplete:Z

    move/from16 v38, v0

    if-eqz v38, :cond_e

    .line 198
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v39

    add-int/lit8 v39, v39, -0x1

    invoke-virtual/range {v38 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v28

    .line 199
    .local v28, "tempLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v28, :cond_b

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    move/from16 v38, v0

    if-nez v38, :cond_b

    .line 200
    invoke-virtual/range {v28 .. v28}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    goto :goto_4

    .line 202
    .end local v28    # "tempLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    :cond_e
    if-nez v15, :cond_b

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v38, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    move-object/from16 v39, v0

    invoke-virtual/range {v39 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v39

    add-int/lit8 v39, v39, -0x1

    invoke-virtual/range {v38 .. v39}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLargeImage(I)Lcom/sec/samsung/gallery/decoder/LargeImage;

    move-result-object v28

    .line 204
    .restart local v28    # "tempLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    if-eqz v28, :cond_b

    move-object/from16 v0, v28

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodingStarted:Z

    move/from16 v38, v0

    if-nez v38, :cond_b

    .line 205
    invoke-virtual/range {v28 .. v28}, Lcom/sec/samsung/gallery/decoder/LargeImage;->decode()V

    goto :goto_4

    .line 209
    .end local v10    # "i":I
    .end local v13    # "lLength":F
    .end local v14    # "lSize":[I
    .end local v17    # "mLevelmin":I
    .end local v18    # "nBlockH":I
    .end local v19    # "nBlockW":I
    .end local v20    # "numOfRect":I
    .end local v22    # "scale":F
    .end local v23    # "source":Landroid/graphics/RectF;
    .end local v24    # "startX":I
    .end local v25    # "startY":I
    .end local v27    # "target":Landroid/graphics/RectF;
    .end local v28    # "tempLargeImage":Lcom/sec/samsung/gallery/decoder/LargeImage;
    .end local v33    # "viewH":F
    .end local v34    # "viewW":F
    :cond_f
    const/16 v38, 0x0

    move/from16 v0, v38

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDecodingComplete:Z

    goto :goto_4

    .line 142
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getProgress()F
    .locals 6

    .prologue
    .line 673
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mAnimationStarted:Z

    if-eqz v1, :cond_1

    .line 675
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartTime:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDuration:F

    div-float v0, v1, v2

    .line 676
    .local v0, "progress":F
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 678
    const/high16 v0, 0x3f800000    # 1.0f

    .line 679
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    .line 681
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartAlpha:F

    iget v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mEndAlpha:F

    iget v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartAlpha:F

    sub-float/2addr v2, v3

    mul-float/2addr v2, v0

    add-float/2addr v1, v2

    .line 686
    .end local v0    # "progress":F
    :goto_0
    return v1

    .line 684
    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mAnimationStarted:Z

    .line 685
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartTime:J

    .line 686
    iget v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartAlpha:F

    goto :goto_0
.end method

.method public init()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartTime:J

    .line 62
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mStartAlpha:F

    .line 63
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mEndAlpha:F

    .line 64
    const/high16 v0, 0x43480000    # 200.0f

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mDuration:F

    .line 65
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    .line 66
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mScrolling:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mImageChanging:Z

    .line 68
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentScale:F

    .line 69
    invoke-static {}, Lcom/sec/android/gallery3d/ui/TileImageView;->getTileSize()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mTileSize:I

    .line 71
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentIndex:I

    .line 73
    return-void
.end method

.method isAllUploadingDone(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage;)Z
    .locals 5
    .param p1, "current"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p2, "prev"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p3, "next"    # Lcom/sec/samsung/gallery/decoder/LargeImage;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 745
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/decoder/LargeImage;->isUploadCompleted()Z

    move-result v2

    .line 746
    .local v2, "prevDone":Z
    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/decoder/LargeImage;->isUploadCompleted()Z

    move-result v0

    .line 747
    .local v0, "centerDone":Z
    :goto_1
    if-eqz p3, :cond_2

    invoke-virtual {p3}, Lcom/sec/samsung/gallery/decoder/LargeImage;->isUploadCompleted()Z

    move-result v1

    .line 748
    .local v1, "nextDone":Z
    :goto_2
    if-eqz v2, :cond_3

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    :goto_3
    return v3

    .end local v0    # "centerDone":Z
    .end local v1    # "nextDone":Z
    .end local v2    # "prevDone":Z
    :cond_0
    move v2, v3

    .line 745
    goto :goto_0

    .restart local v2    # "prevDone":Z
    :cond_1
    move v0, v4

    .line 746
    goto :goto_1

    .restart local v0    # "centerDone":Z
    :cond_2
    move v1, v3

    .line 747
    goto :goto_2

    .restart local v1    # "nextDone":Z
    :cond_3
    move v3, v4

    .line 748
    goto :goto_3
.end method

.method public isImageChanging()Z
    .locals 1

    .prologue
    .line 592
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mImageChanging:Z

    return v0
.end method

.method public isLevelChanged(I)Z
    .locals 5
    .param p1, "currLevel"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 399
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v3, :cond_1

    .line 400
    iget-object v3, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLevel()[I

    move-result-object v0

    .line 402
    .local v0, "lLevel":[I
    if-eqz v0, :cond_0

    aget v3, v0, v2

    aget v4, v0, v1

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    if-ge p1, v3, :cond_0

    .line 404
    .end local v0    # "lLevel":[I
    :goto_0
    return v1

    .restart local v0    # "lLevel":[I
    :cond_0
    move v1, v2

    .line 402
    goto :goto_0

    .end local v0    # "lLevel":[I
    :cond_1
    move v1, v2

    .line 404
    goto :goto_0
.end method

.method public isPanorama()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 691
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v2, :cond_1

    .line 692
    iget-object v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 693
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_1

    .line 694
    invoke-static {v0}, Lcom/sec/samsung/gallery/util/Panorama3DUtil;->is3DPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 697
    .end local v0    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return v1
.end method

.method public isScrolling()Z
    .locals 1

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mScrolling:Z

    return v0
.end method

.method public isZoomState(I)Z
    .locals 9
    .param p1, "level"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 383
    const/4 v2, 0x0

    .line 384
    .local v2, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v5, :cond_2

    .line 385
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getLevel()[I

    move-result-object v1

    .line 387
    .local v1, "lLevel":[I
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    monitor-enter v5

    .line 388
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/PhotoView;->getCurrentScale()F

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/PhotoView;->getScaleMin()F

    move-result v7

    sub-float v0, v6, v7

    .line 389
    .local v0, "diff":F
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 390
    cmpg-float v5, v0, v8

    if-gez v5, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    cmpl-float v5, v0, v8

    if-lez v5, :cond_3

    move v2, v3

    .line 391
    :goto_0
    if-eqz v2, :cond_2

    .line 392
    if-eqz v1, :cond_4

    aget v5, v1, v4

    if-lt p1, v5, :cond_1

    aget v5, v1, v3

    if-ge p1, v5, :cond_4

    :cond_1
    move v2, v3

    .line 395
    .end local v1    # "lLevel":[I
    :cond_2
    :goto_1
    return v2

    .line 389
    .restart local v1    # "lLevel":[I
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    :cond_3
    move v2, v4

    .line 390
    goto :goto_0

    :cond_4
    move v2, v4

    .line 392
    goto :goto_1
.end method

.method public moveTo(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 701
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentIndex:I

    sub-int v0, p1, v0

    packed-switch v0, :pswitch_data_0

    .line 723
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    .line 726
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentIndex:I

    .line 727
    return-void

    .line 707
    :pswitch_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    goto :goto_0

    .line 711
    :pswitch_2
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    goto :goto_0

    .line 715
    :pswitch_3
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    goto :goto_0

    .line 719
    :pswitch_4
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNextDirection:Z

    goto :goto_0

    .line 701
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public needToAnimate()Z
    .locals 1

    .prologue
    .line 88
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    return v0
.end method

.method public prepareAlphablendingAnimation()V
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mNeedToAnimate:Z

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mAnimationStarted:Z

    .line 85
    return-void
.end method

.method public setImageChanging(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 588
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mImageChanging:Z

    .line 589
    return-void
.end method

.method public setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-nez v0, :cond_0

    .line 360
    monitor-enter p0

    .line 361
    :try_start_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 362
    monitor-exit p0

    .line 364
    :cond_0
    return-void

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-nez v0, :cond_0

    .line 352
    monitor-enter p0

    .line 353
    :try_start_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 354
    monitor-exit p0

    .line 356
    :cond_0
    return-void

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setScrolling(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 580
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mScrolling:Z

    .line 581
    return-void
.end method

.method public switchToNext()V
    .locals 2

    .prologue
    .line 730
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v0

    .line 731
    .local v0, "currentIndex":I
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 732
    add-int/lit8 v1, v0, 0x1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->moveTo(I)V

    .line 734
    :cond_0
    return-void

    .line 730
    .end local v0    # "currentIndex":I
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentIndex:I

    goto :goto_0
.end method

.method public switchToPrev()V
    .locals 2

    .prologue
    .line 737
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->getCurrentIndex()I

    move-result v0

    .line 738
    .local v0, "currentIndex":I
    :goto_0
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 739
    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->moveTo(I)V

    .line 741
    :cond_0
    return-void

    .line 737
    .end local v0    # "currentIndex":I
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mCurrentIndex:I

    goto :goto_0
.end method

.method public unsetPhotoDataAdapter()V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_0

    .line 376
    monitor-enter p0

    .line 377
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoDataAdapter:Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .line 378
    monitor-exit p0

    .line 380
    :cond_0
    return-void

    .line 378
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unsetPhotoView()V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v0, :cond_0

    .line 368
    monitor-enter p0

    .line 369
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 370
    monitor-exit p0

    .line 372
    :cond_0
    return-void

    .line 370
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
