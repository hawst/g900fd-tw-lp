.class public Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;
.super Landroid/widget/ExpandableListView;
.source "TagCloudExpandableList.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;)V

    .line 25
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->init()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->init()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 14
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ExpandableListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 15
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->init()V

    .line 16
    return-void
.end method

.method private init()V
    .locals 7

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 31
    .local v5, "res":Landroid/content/res/Resources;
    const v6, 0x7f0d0273

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 32
    .local v0, "leftPaneWidth":I
    const v6, 0x7f0d0270

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 33
    .local v3, "paddingTop":I
    const v6, 0x7f0d0271

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 34
    .local v2, "paddingRight":I
    const v6, 0x7f0d0272

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 36
    .local v1, "paddingLeft":I
    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v6, -0x1

    invoke-direct {v4, v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 37
    .local v4, "param":Landroid/widget/RelativeLayout$LayoutParams;
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 38
    const/4 v6, 0x0

    invoke-virtual {p0, v1, v3, v2, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setPadding(IIII)V

    .line 39
    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setGroupIndicator(Landroid/graphics/drawable/Drawable;)V

    .line 40
    const/high16 v6, 0x2000000

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setScrollBarStyle(I)V

    .line 41
    const v6, 0x7f020228

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 42
    const v6, 0x7f0b003b

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableList;->setBackgroundResource(I)V

    .line 43
    return-void
.end method
