.class public Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;
.super Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
.source "ShowShareDialogCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;
    }
.end annotation


# instance fields
.field private mShareLocationConfirmDialog:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;-><init>()V

    .line 39
    const v0, 0x7f0e0047

    iput v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mTitleOfDialog:I

    .line 40
    return-void
.end method

.method private exitSelectionMode()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 141
    return-void
.end method

.method private hasValidLocation()Z
    .locals 18

    .prologue
    .line 150
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    check-cast v15, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v14

    .line 151
    .local v14, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v12

    .line 153
    .local v12, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 155
    .local v7, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v7, :cond_0

    .line 159
    const-wide/16 v8, 0x0

    .line 160
    .local v8, "latitude":D
    const-wide/16 v10, 0x0

    .line 162
    .local v10, "longitude":D
    instance-of v15, v7, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v15, :cond_3

    move-object v13, v7

    .line 163
    check-cast v13, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 164
    .local v13, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    .line 165
    .local v2, "count":I
    const/4 v15, 0x0

    invoke-virtual {v13, v15, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v6

    .line 167
    .local v6, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 168
    .local v5, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v8

    .line 169
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v10

    .line 171
    const-wide/16 v16, 0x0

    cmpl-double v15, v8, v16

    if-nez v15, :cond_2

    const-wide/16 v16, 0x0

    cmpl-double v15, v10, v16

    if-eqz v15, :cond_1

    .line 173
    :cond_2
    const/4 v15, 0x1

    .line 187
    .end local v2    # "count":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v7    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v8    # "latitude":D
    .end local v10    # "longitude":D
    .end local v13    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v15

    .restart local v7    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v8    # "latitude":D
    .restart local v10    # "longitude":D
    :cond_3
    move-object v5, v7

    .line 177
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 178
    .restart local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatitude()D

    move-result-wide v8

    .line 179
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getLongitude()D

    move-result-wide v10

    .line 181
    const-wide/16 v16, 0x0

    cmpl-double v15, v8, v16

    if-nez v15, :cond_4

    const-wide/16 v16, 0x0

    cmpl-double v15, v10, v16

    if-eqz v15, :cond_0

    .line 182
    :cond_4
    const/4 v15, 0x1

    goto :goto_0

    .line 187
    .end local v5    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v7    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v8    # "latitude":D
    .end local v10    # "longitude":D
    :cond_5
    const/4 v15, 0x0

    goto :goto_0
.end method

.method private isConfirmLocationShareOn()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 145
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    const-string v3, "ShareActionCheckDialogOff"

    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    .line 146
    .local v0, "result":Z
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public static isOrangeCloudExist(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    const/4 v2, 0x0

    .line 88
    .local v2, "result":Z
    const/4 v1, 0x0

    .line 90
    .local v1, "pi":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.orange.fr.cloudorange"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 94
    :goto_0
    if-nez v1, :cond_0

    .line 96
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.orange.cloud.android"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 101
    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    .line 102
    const/4 v2, 0x1

    .line 104
    :cond_1
    return v2

    .line 91
    :catch_0
    move-exception v0

    .line 92
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 97
    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v0

    .line 98
    .restart local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1
.end method

.method private moveOrangeCloudToTop(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 191
    .local p1, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 192
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v2, "com.orange.fr.cloudorange.common.activities.SendToMyCoActivity"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v2, "com.funambol.android.activities.AndroidReceiveShare"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 196
    :cond_0
    const/4 v1, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 197
    add-int/lit8 v1, v0, 0x1

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 199
    :cond_1
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    iget-object v1, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v1, v1, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    const-string v2, "com.dropbox.android.activity.DropboxSendTo"

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 200
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 201
    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 191
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 204
    :cond_3
    return-void
.end method

.method private needToShowShareLocationConfirmDialog()Z
    .locals 1

    .prologue
    .line 108
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShareLocationConfirmDialog:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->isConfirmLocationShareOn()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->hasValidLocation()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showShareLocationConfirmDialog(Lcom/sec/samsung/gallery/core/Event;)V
    .locals 2
    .param p1, "event"    # Lcom/sec/samsung/gallery/core/Event;

    .prologue
    .line 131
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_CONFIRM_LOCATION_SHARE:I

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    .line 133
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/core/Event;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareLocationConfirmDialog:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareLocationConfirmDialog:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->addObserver(Ljava/util/Observer;)V

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareLocationConfirmDialog:Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->showDialog()V

    .line 136
    return-void
.end method

.method private startShareApp(Landroid/content/pm/ResolveInfo;)V
    .locals 6
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 112
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v2

    .line 114
    .local v2, "intent":Landroid/content/Intent;
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 117
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    :try_start_0
    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v4, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 119
    .local v3, "packageName":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string v4, "com.att.android.mobile.attmessages"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 120
    const/high16 v4, 0x80000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 121
    :cond_0
    new-instance v4, Landroid/content/ComponentName;

    iget-object v5, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v4, v3, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 123
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v4, p1}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->saveLastShareAppInfo(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    .line 124
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    .end local v3    # "packageName":Ljava/lang/String;
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v1

    .line 126
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected createAppList()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 46
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 47
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 48
    .local v2, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getFilteredShareList(Ljava/util/List;)Ljava/util/List;

    .line 52
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-nez v3, :cond_0

    .line 53
    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;

    invoke-direct {v3, p0, v1}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd$PriorityPackageComparator;-><init>(Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;Landroid/content/pm/PackageManager;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 55
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->isOrangeCloudExist(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 56
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->moveOrangeCloudToTop(Ljava/util/List;)V

    .line 59
    :cond_1
    return-object v2
.end method

.method public bridge synthetic execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 0
    .param p1, "x0"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->execute(Lorg/puremvc/java/interfaces/INotification;)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 64
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 65
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 67
    .local v1, "eventType":I
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    if-ne v1, v3, :cond_2

    .line 68
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->needToShowShareLocationConfirmDialog()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 69
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->showShareLocationConfirmDialog(Lcom/sec/samsung/gallery/core/Event;)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 72
    .local v2, "info":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->startShareApp(Landroid/content/pm/ResolveInfo;)V

    .line 74
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->exitSelectionMode()V

    .line 75
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    goto :goto_0

    .line 77
    .end local v2    # "info":Landroid/content/pm/ResolveInfo;
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_CONFIRM_LOCATION_SHARE:I

    if-ne v1, v3, :cond_0

    .line 78
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 79
    .restart local v2    # "info":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->startShareApp(Landroid/content/pm/ResolveInfo;)V

    .line 81
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->exitSelectionMode()V

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowShareDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    goto :goto_0
.end method
