.class public Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "AppChoiceDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$1;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;,
        Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    }
.end annotation


# static fields
.field private static final SCHEME:Ljava/lang/String; = "package"

.field private static final TAG:Ljava/lang/String;

.field public static mPrivateModeChangeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;


# instance fields
.field private mAppList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mChoiceDialog:Landroid/app/Dialog;

.field private final mCtx:Landroid/content/Context;

.field private final mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

.field private mDisplayAsGridView:Z

.field private final mEvent:Lcom/sec/samsung/gallery/core/Event;

.field private mGridView:Landroid/widget/GridView;

.field private mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

.field private mOnDialogDismissListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

.field private mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

.field private mOnResumeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;Z)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;
    .param p3, "event"    # Lcom/sec/samsung/gallery/core/Event;
    .param p4, "showAsGrid"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Lcom/sec/samsung/gallery/core/Event;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 78
    .local p2, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;ZLcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;)V

    .line 79
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;ZLcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;)V
    .locals 2
    .param p1, "ctx"    # Landroid/content/Context;
    .param p3, "event"    # Lcom/sec/samsung/gallery/core/Event;
    .param p4, "showAsGrid"    # Z
    .param p5, "itemHander"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;",
            "Lcom/sec/samsung/gallery/core/Event;",
            "Z",
            "Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p2, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mCtx:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;

    .line 84
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    .line 86
    iput-boolean p4, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDisplayAsGridView:Z

    .line 87
    iput-object p5, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    .line 88
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnResumeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    return-object v0
.end method

.method static synthetic access$1300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/core/Event;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDisplayAsGridView:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/widget/GridView;)Landroid/widget/GridView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    .param p1, "x1"    # Landroid/widget/GridView;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Landroid/app/Dialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mChoiceDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnDialogDismissListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

    return-object v0
.end method

.method public static getAppIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resolveInfo"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 91
    const/4 v0, 0x0

    .line 92
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 93
    invoke-static {p0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getResizedIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 94
    .local v1, "newIcon":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_0

    .line 95
    move-object v0, v1

    .line 97
    .end local v1    # "newIcon":Landroid/graphics/drawable/Drawable;
    :cond_0
    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 98
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 99
    .local v2, "pm":Landroid/content/pm/PackageManager;
    invoke-virtual {p1, v2}, Landroid/content/pm/ResolveInfo;->loadIcon(Landroid/content/pm/PackageManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 101
    .end local v2    # "pm":Landroid/content/pm/PackageManager;
    :cond_1
    return-object v0
.end method


# virtual methods
.method public dismissDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->dismiss()V

    .line 375
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    .line 376
    sput-object v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mPrivateModeChangeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;

    .line 377
    return-void
.end method

.method public isShowing()Z
    .locals 2

    .prologue
    .line 384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 385
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 386
    const/4 v1, 0x1

    .line 387
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 427
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;

    if-eqz v1, :cond_0

    .line 428
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mGridView:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    .line 429
    .local v0, "adapter":Landroid/widget/BaseAdapter;
    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 431
    .end local v0    # "adapter":Landroid/widget/BaseAdapter;
    :cond_0
    return-void
.end method

.method public setAppList(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 391
    .local p1, "appList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;

    .line 392
    return-void
.end method

.method public setOnDialogDismissListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnDialogDismissListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;

    .line 404
    return-void
.end method

.method public setOnDialogOrientationListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    .prologue
    .line 395
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnOrientationChangedListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;

    .line 396
    return-void
.end method

.method public setOnResumeListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;)V
    .locals 0
    .param p1, "resumeListener"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;

    .prologue
    .line 423
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mOnResumeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;

    .line 424
    return-void
.end method

.method public setPrivateModeChangeListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;

    .prologue
    .line 411
    sput-object p1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mPrivateModeChangeListener:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;

    .line 412
    return-void
.end method

.method public setTitle(I)V
    .locals 1
    .param p1, "titleId"    # I

    .prologue
    .line 380
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->setTitle(I)V

    .line 381
    return-void
.end method

.method public showDialog()V
    .locals 4

    .prologue
    .line 367
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mCtx:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$AppChoiceDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    :goto_0
    return-void

    .line 368
    :catch_0
    move-exception v0

    .line 369
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public startSingleShareApp()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 434
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mItemInfoHandler:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;

    invoke-interface {v0, v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$ItemInfoAdapter;->getItemObject(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    .line 435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->notifyObservers(Ljava/lang/Object;)V

    .line 436
    return-void

    .line 434
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->mAppList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
