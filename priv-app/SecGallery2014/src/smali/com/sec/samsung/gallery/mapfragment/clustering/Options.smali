.class public Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
.super Ljava/lang/Object;
.source "Options.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;
    }
.end annotation


# static fields
.field private static final DEFAULT_EXPAND_BOUNDS_FACTOR:D = 0.0

.field private static final DEFAULT_PIXEL_DISTANCE_TO_JOIN_CLUSTER:I = 0x12c

.field private static final DEFAULT_TRANSITION_DURATION:I = 0xb4


# instance fields
.field private final DEFAULT_SHOW_INFO_WINDOW_ANIMATION_DURATION:I

.field private final DEFAULT_ZOOM_TO_BOUNDS_ANIMATION_DURATION:I

.field private clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

.field private clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

.field private clusteringOnCameraChangeListenerDirtyLifetimeMillis:J

.field private expandBoundsFactor:D

.field private markerOptionsChooser:Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

.field private onInfoWindowClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;

.field private onMarkerClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;

.field private pixelDistanceToJoinCluster:I

.field private processingListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

.field private showInfoWindowAnimationDuration:I

.field private singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

.field private singlePointInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

.field private transitionDuration:I

.field private transitionInterpolator:Landroid/view/animation/Interpolator;

.field private zoomToBoundsAnimationDuration:I

.field private zoomToBoundsPadding:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x12c

    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    const/16 v0, 0xb4

    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionDuration:I

    .line 22
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionInterpolator:Landroid/view/animation/Interpolator;

    .line 29
    iput v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->pixelDistanceToJoinCluster:I

    .line 43
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->expandBoundsFactor:D

    .line 67
    const/16 v0, 0x4b

    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsPadding:I

    .line 69
    iput v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->DEFAULT_ZOOM_TO_BOUNDS_ANIMATION_DURATION:I

    .line 73
    iput v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsAnimationDuration:I

    .line 75
    iput v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->DEFAULT_SHOW_INFO_WINDOW_ANIMATION_DURATION:I

    .line 79
    iput v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->showInfoWindowAnimationDuration:I

    .line 84
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;->ZOOM_TO_BOUNDS:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    .line 89
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    .line 94
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->SHOW_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    .line 99
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;->HIDE_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    .line 108
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusteringOnCameraChangeListenerDirtyLifetimeMillis:J

    .line 361
    return-void
.end method


# virtual methods
.method getClusterClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    return-object v0
.end method

.method getClusterInfoWindowClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    return-object v0
.end method

.method getClusteringOnCameraChangeListenerDirtyLifetimeMillis()J
    .locals 2

    .prologue
    .line 331
    iget-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusteringOnCameraChangeListenerDirtyLifetimeMillis:J

    return-wide v0
.end method

.method getExpandBoundsFactor()D
    .locals 2

    .prologue
    .line 167
    iget-wide v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->expandBoundsFactor:D

    return-wide v0
.end method

.method getMarkerOptionsChooser()Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->markerOptionsChooser:Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    return-object v0
.end method

.method getOnInfoWindowClickDownstreamListener()Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->onInfoWindowClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;

    return-object v0
.end method

.method getOnMarkerClickDownstreamListener()Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->onMarkerClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;

    return-object v0
.end method

.method getPixelDistanceToJoinCluster()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->pixelDistanceToJoinCluster:I

    return v0
.end method

.method getProcessingListener()Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->processingListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    return-object v0
.end method

.method getShowInfoWindowAnimationDuration()I
    .locals 1

    .prologue
    .line 286
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->showInfoWindowAnimationDuration:I

    return v0
.end method

.method getSinglePointClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    return-object v0
.end method

.method getSinglePointInfoWindowClickBehavior()Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    return-object v0
.end method

.method getTransitionDuration()I
    .locals 1

    .prologue
    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionDuration:I

    return v0
.end method

.method getTransitionInterpolator()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionInterpolator:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method getZoomToBoundsAnimationDuration()I
    .locals 1

    .prologue
    .line 271
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsAnimationDuration:I

    return v0
.end method

.method getZoomToBoundsPadding()I
    .locals 1

    .prologue
    .line 256
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsPadding:I

    return v0
.end method

.method public setClusterClickBehavior(Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;)V
    .locals 0
    .param p1, "clusterClickBehavior"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterClickBehavior;

    .line 235
    return-void
.end method

.method public setClusterInfoWindowClickBehavior(Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;)V
    .locals 0
    .param p1, "clusterInfoWindowClickBehavior"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusterInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$ClusterInfoWindowClickBehavior;

    .line 250
    return-void
.end method

.method public setClusteringOnCameraChangeListenerDirtyLifetimeMillis(J)V
    .locals 1
    .param p1, "clusteringOnCameraChangeListenerDirtyLifetimeMillis"    # J

    .prologue
    .line 338
    iput-wide p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->clusteringOnCameraChangeListenerDirtyLifetimeMillis:J

    .line 339
    return-void
.end method

.method public setExpandBoundsFactor(D)V
    .locals 1
    .param p1, "expandBoundsFactor"    # D

    .prologue
    .line 174
    iput-wide p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->expandBoundsFactor:D

    .line 175
    return-void
.end method

.method public setMarkerOptionsChooser(Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;)V
    .locals 0
    .param p1, "markerOptionsChooser"    # Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->markerOptionsChooser:Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    .line 190
    return-void
.end method

.method public setOnInfoWindowClickDownstreamListener(Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;)V
    .locals 0
    .param p1, "onInfoWindowClickDownstreamListener"    # Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;

    .prologue
    .line 219
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->onInfoWindowClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnInfoWindowClickDownstreamListener;

    .line 220
    return-void
.end method

.method public setOnMarkerClickDownstreamListener(Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;)V
    .locals 0
    .param p1, "onMarkerClickDownstreamListener"    # Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->onMarkerClickDownstreamListener:Lcom/sec/samsung/gallery/mapfragment/clustering/OnMarkerClickDownstreamListener;

    .line 205
    return-void
.end method

.method public setPixelDistanceToJoinCluster(I)V
    .locals 0
    .param p1, "pixelDistanceToJoinCluster"    # I

    .prologue
    .line 160
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->pixelDistanceToJoinCluster:I

    .line 161
    return-void
.end method

.method public setProcessingListener(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;)V
    .locals 0
    .param p1, "processingListener"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    .prologue
    .line 346
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->processingListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    .line 347
    return-void
.end method

.method public setShowInfoWindowAnimationDuration(I)V
    .locals 0
    .param p1, "showInfoWindowAnimationDuration"    # I

    .prologue
    .line 294
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->showInfoWindowAnimationDuration:I

    .line 295
    return-void
.end method

.method public setSinglePointClickBehavior(Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;)V
    .locals 0
    .param p1, "singlePointClickBehavior"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    .prologue
    .line 309
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    .line 310
    return-void
.end method

.method public setSinglePointInfoWindowClickBehavior(Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;)V
    .locals 0
    .param p1, "singlePointInfoWindowClickBehavior"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    .prologue
    .line 324
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->singlePointInfoWindowClickBehavior:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointInfoWindowClickBehavior;

    .line 325
    return-void
.end method

.method public setTransitionDuration(I)V
    .locals 0
    .param p1, "transitionDuration"    # I

    .prologue
    .line 127
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionDuration:I

    .line 128
    return-void
.end method

.method public setTransitionInterpolator(Landroid/view/animation/Interpolator;)V
    .locals 0
    .param p1, "transitionInterpolator"    # Landroid/view/animation/Interpolator;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->transitionInterpolator:Landroid/view/animation/Interpolator;

    .line 143
    return-void
.end method

.method public setZoomToBoundsAnimationDuration(I)V
    .locals 0
    .param p1, "zoomToBoundsAnimationDuration"    # I

    .prologue
    .line 279
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsAnimationDuration:I

    .line 280
    return-void
.end method

.method public setZoomToBoundsPadding(I)V
    .locals 0
    .param p1, "zoomToBoundsPadding"    # I

    .prologue
    .line 264
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->zoomToBoundsPadding:I

    .line 265
    return-void
.end method
