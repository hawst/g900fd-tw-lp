.class public Lcom/sec/samsung/gallery/access/face/PersonList;
.super Lcom/sec/samsung/gallery/access/face/List;
.source "PersonList.java"


# static fields
.field public static final DCM_ONLY_CLASSIFIED_PERSION_ID:I = -0x1

.field private static final TAG:Ljava/lang/String; = "PersonList"

.field public static final UNKOWN_PERSON_ID:I = 0x1

.field private static final keys:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static volatile mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider; = null

.field public static mUnkownGroup:Z = false

.field public static final mUnkownGroupFaceOffset:I = 0xf4240

.field private static final mergedData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;>;"
        }
    .end annotation
.end field


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/access/face/PersonList;->mUnkownGroup:Z

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/access/face/List;-><init>()V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    .line 57
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContext:Landroid/content/Context;

    .line 58
    sget-object v0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    iget-object v1, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    .line 61
    :cond_0
    return-void
.end method

.method public static addPerson(Landroid/content/Context;Ljava/lang/String;)I
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 482
    const/4 v7, 0x0

    .line 483
    .local v7, "id":I
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move v8, v7

    .line 507
    .end local v7    # "id":I
    .local v8, "id":I
    :goto_0
    return v8

    .line 485
    .end local v8    # "id":I
    .restart local v7    # "id":I
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 487
    .local v0, "cr":Landroid/content/ContentResolver;
    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v3

    .line 488
    .local v2, "projection":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 490
    .local v9, "nameCursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 492
    if-eqz v9, :cond_3

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 493
    const/4 v1, 0x0

    invoke-interface {v9, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 504
    :cond_2
    :goto_1
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    :goto_2
    move v8, v7

    .line 507
    .end local v7    # "id":I
    .restart local v8    # "id":I
    goto :goto_0

    .line 495
    .end local v8    # "id":I
    .restart local v7    # "id":I
    :cond_3
    :try_start_1
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 496
    .local v11, "values":Landroid/content/ContentValues;
    const-string v1, "name"

    invoke-virtual {v11, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v10

    .line 498
    .local v10, "uri":Landroid/net/Uri;
    if-eqz v10, :cond_2

    .line 499
    invoke-static {v10}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    long-to-int v7, v4

    goto :goto_1

    .line 501
    .end local v10    # "uri":Landroid/net/Uri;
    .end local v11    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v6

    .line 502
    .local v6, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v6}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 504
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_2

    .end local v6    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private close()V
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 151
    return-void
.end method

.method public static close(Lcom/sec/samsung/gallery/access/face/PersonList;)V
    .locals 0
    .param p0, "personList"    # Lcom/sec/samsung/gallery/access/face/PersonList;

    .prologue
    .line 190
    if-eqz p0, :cond_0

    .line 191
    invoke-direct {p0}, Lcom/sec/samsung/gallery/access/face/PersonList;->close()V

    .line 192
    :cond_0
    return-void
.end method

.method public static getCover(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 511
    const/4 v0, 0x0

    .line 512
    .local v0, "cover":Ljava/lang/String;
    if-nez p0, :cond_0

    move-object v1, v0

    .line 517
    .end local v0    # "cover":Ljava/lang/String;
    .local v1, "cover":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 514
    .end local v1    # "cover":Ljava/lang/String;
    .restart local v0    # "cover":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 515
    .local v2, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v6, p1

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 516
    .local v3, "uri":Landroid/net/Uri;
    const-string v4, "cover"

    invoke-static {v3, v4, v2}, Lcom/sec/samsung/gallery/access/face/PersonList;->getString(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 517
    .end local v0    # "cover":Ljava/lang/String;
    .restart local v1    # "cover":Ljava/lang/String;
    goto :goto_0
.end method

.method public static getName(Landroid/content/Context;I)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    const v2, 0xf4240

    .line 261
    const/4 v8, 0x0

    .line 262
    .local v8, "name":Ljava/lang/String;
    const/4 v7, 0x0

    .line 263
    .local v7, "joinedName":Ljava/lang/String;
    if-nez p0, :cond_0

    move-object v9, v8

    .line 286
    .end local v8    # "name":Ljava/lang/String;
    .local v9, "name":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 265
    .end local v9    # "name":Ljava/lang/String;
    .restart local v8    # "name":Ljava/lang/String;
    :cond_0
    if-ne p1, v2, :cond_1

    .line 266
    const-string v8, "Unkown"

    :goto_1
    move-object v9, v8

    .line 286
    .end local v8    # "name":Ljava/lang/String;
    .restart local v9    # "name":Ljava/lang/String;
    goto :goto_0

    .line 267
    .end local v9    # "name":Ljava/lang/String;
    .restart local v8    # "name":Ljava/lang/String;
    :cond_1
    if-le p1, v2, :cond_2

    .line 268
    sub-int/2addr p1, v2

    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unkown"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 271
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 272
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 273
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 275
    .local v6, "c":Landroid/database/Cursor;
    const/4 v2, 0x2

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "name"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "user_data"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 276
    if-eqz v6, :cond_3

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 277
    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 278
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 279
    if-eqz v8, :cond_3

    .line 280
    invoke-static {v8, v7}, Lcom/sec/samsung/gallery/access/face/PersonList;->getPersonName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 283
    :cond_3
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public static getPersonName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p0, "personName"    # Ljava/lang/String;
    .param p1, "joinedName"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 290
    if-eqz p1, :cond_1

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "none"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object p0, p1

    .line 304
    .end local p0    # "personName":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p0

    .line 292
    .restart local p0    # "personName":Ljava/lang/String;
    :cond_1
    if-nez p1, :cond_2

    const-string v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_2
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string v2, "none"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    const-string v2, "."

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 294
    :cond_4
    if-eqz p1, :cond_0

    .line 295
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 296
    .local v1, "values":[Ljava/lang/String;
    aget-object v2, v1, v4

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 297
    aget-object v2, v1, v4

    const-string v3, "\\."

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    aget-object v0, v2, v4

    .line 298
    .local v0, "lookupkey":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x1

    aget-object v3, v1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method public static getUserData(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 318
    const/4 v2, 0x0

    .line 319
    .local v2, "userData":Ljava/lang/String;
    if-eqz p0, :cond_0

    const v4, 0xf4240

    if-ge p1, v4, :cond_0

    const/4 v4, -0x1

    if-ne p1, v4, :cond_1

    :cond_0
    move-object v3, v2

    .line 324
    .end local v2    # "userData":Ljava/lang/String;
    .local v3, "userData":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 321
    .end local v3    # "userData":Ljava/lang/String;
    .restart local v2    # "userData":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 322
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v6, p1

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 323
    .local v1, "uri":Landroid/net/Uri;
    const-string/jumbo v4, "user_data"

    invoke-static {v1, v4, v0}, Lcom/sec/samsung/gallery/access/face/PersonList;->getString(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 324
    .end local v2    # "userData":Ljava/lang/String;
    .restart local v3    # "userData":Ljava/lang/String;
    goto :goto_0
.end method

.method private isInList(Ljava/util/ArrayList;I)Z
    .locals 4
    .param p2, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .local p1, "listHasFacePerson":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .line 69
    if-nez p1, :cond_0

    move v2, v3

    .line 76
    :goto_0
    return v2

    .line 71
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 72
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 73
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne p2, v2, :cond_1

    .line 74
    const/4 v2, 0x1

    goto :goto_0

    .line 72
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v2, v3

    .line 76
    goto :goto_0
.end method

.method public static mergePersons(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "personIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v13, 0x1

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    .line 544
    const/4 v2, 0x0

    .line 546
    .local v2, "length":I
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 577
    :cond_0
    return-void

    .line 549
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 551
    if-lt v2, v12, :cond_0

    .line 554
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 555
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x0

    .line 556
    .local v3, "sql":Ljava/lang/String;
    const/4 v4, 0x0

    .line 557
    .local v4, "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 558
    .local v5, "values":Landroid/content/ContentValues;
    const/4 v1, 0x1

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 559
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v8, "update faces set person_id=%d where person_id=%d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v11

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v13

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 561
    sget-object v6, Lcom/sec/samsung/gallery/access/face/PersonList;->RAW_SQL_PERSON_DB:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 562
    invoke-virtual {v0, v4, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 564
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v8, "update faces set person_id=%d,recommended_id=%d where person_id=%d"

    const/4 v6, 0x3

    new-array v9, v6, [Ljava/lang/Object;

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v11

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v13

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 566
    sget-object v6, Lcom/sec/samsung/gallery/access/face/PersonList;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 567
    invoke-virtual {v0, v4, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 569
    sget-object v7, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string/jumbo v8, "update faces set recommended_id=%d where recommended_id=%d"

    new-array v9, v12, [Ljava/lang/Object;

    invoke-virtual {p1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v11

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v9, v13

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 571
    sget-object v6, Lcom/sec/samsung/gallery/access/face/PersonList;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v4

    .line 572
    invoke-virtual {v0, v4, v5, v10, v10}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 574
    sget-object v7, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    int-to-long v8, v6

    invoke-static {v7, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 575
    invoke-virtual {v0, v4, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 558
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0
.end method

.method public static open(Landroid/content/Context;Z)Lcom/sec/samsung/gallery/access/face/PersonList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bHasFace"    # Z

    .prologue
    .line 181
    const/4 v0, 0x0

    .line 182
    .local v0, "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    if-nez p0, :cond_0

    move-object v1, v0

    .line 186
    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .local v1, "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    :goto_0
    return-object v1

    .line 184
    .end local v1    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .restart local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/access/face/PersonList;

    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/access/face/PersonList;-><init>(Landroid/content/Context;)V

    .line 185
    .restart local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->open(Z)V

    move-object v1, v0

    .line 186
    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .restart local v1    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    goto :goto_0
.end method

.method private open(II)V
    .locals 9
    .param p1, "faceId"    # I
    .param p2, "critical"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 154
    iget-object v1, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 155
    .local v0, "cr":Landroid/content/ContentResolver;
    new-array v2, v8, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v5

    .line 156
    .local v2, "projection":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 157
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 158
    .local v7, "id":I
    const-string v3, "_id>?"

    .line 159
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 161
    .local v4, "selectionArgs":[Ljava/lang/String;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    const-string v5, "name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 162
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 164
    :cond_0
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 165
    iget-object v1, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 169
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 171
    return-void

    .line 169
    :catchall_0
    move-exception v1

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private open(Z)V
    .locals 14
    .param p1, "bHasFace"    # Z

    .prologue
    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 80
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x1

    new-array v12, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "_id"

    aput-object v4, v12, v2

    .line 81
    .local v12, "projection":[Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 82
    .local v11, "listHasFacePerson":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v9, 0x0

    .line 83
    .local v9, "cursor":Landroid/database/Cursor;
    const/4 v10, 0x0

    .line 84
    .local v10, "id":I
    if-eqz p1, :cond_2

    .line 85
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "distinct"

    const-string/jumbo v5, "true"

    invoke-virtual {v2, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 86
    .local v1, "faceUri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 88
    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "recommended_id"

    aput-object v5, v2, v4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 89
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 91
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 92
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 96
    :cond_1
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 100
    .end local v1    # "faceUri":Landroid/net/Uri;
    :cond_2
    const/4 v9, 0x0

    .line 102
    :try_start_1
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "name"

    move-object v2, v0

    move-object v4, v12

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 103
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 105
    :cond_3
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 106
    if-eqz p1, :cond_a

    .line 107
    invoke-direct {p0, v11, v10}, Lcom/sec/samsung/gallery/access/face/PersonList;->isInList(Ljava/util/ArrayList;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_4
    :goto_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_3

    .line 115
    :cond_5
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 118
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_9

    .line 119
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 120
    const/4 v8, 0x0

    .line 121
    .local v8, "bUngroup":Z
    sget-boolean v2, Lcom/sec/samsung/gallery/access/face/PersonList;->mUnkownGroup:Z

    if-eqz v2, :cond_c

    .line 122
    const-string v13, "select distinct(group_id) from faces where recommended_id=1 order by group_id "

    .line 123
    .local v13, "sql":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/PersonList;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v13}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 124
    .local v3, "uri":Landroid/net/Uri;
    const/4 v9, 0x0

    .line 126
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, v0

    :try_start_2
    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 127
    if-eqz v9, :cond_7

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 129
    :cond_6
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 130
    if-nez v10, :cond_b

    .line 131
    const/4 v8, 0x1

    .line 136
    :goto_1
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_6

    .line 138
    :cond_7
    if-eqz v8, :cond_8

    .line 139
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    const v4, 0xf4240

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 142
    :cond_8
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 148
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v8    # "bUngroup":Z
    .end local v13    # "sql":Ljava/lang/String;
    :cond_9
    :goto_2
    return-void

    .line 96
    .restart local v1    # "faceUri":Landroid/net/Uri;
    :catchall_0
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 110
    .end local v1    # "faceUri":Landroid/net/Uri;
    :cond_a
    :try_start_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 115
    :catchall_1
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 133
    .restart local v3    # "uri":Landroid/net/Uri;
    .restart local v8    # "bUngroup":Z
    .restart local v13    # "sql":Ljava/lang/String;
    :cond_b
    const v2, 0xf4240

    add-int/2addr v10, v2

    .line 134
    :try_start_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    goto :goto_1

    .line 142
    :catchall_2
    move-exception v2

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 145
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v13    # "sql":Ljava/lang/String;
    :cond_c
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public static openSimailar(Landroid/content/Context;II)Lcom/sec/samsung/gallery/access/face/PersonList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "faceId"    # I
    .param p2, "critical"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    .line 174
    .local v0, "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    if-nez p0, :cond_0

    move-object v1, v0

    .line 178
    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .local v1, "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    :goto_0
    return-object v1

    .line 176
    .end local v1    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .restart local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/access/face/PersonList;

    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/access/face/PersonList;-><init>(Landroid/content/Context;)V

    .line 177
    .restart local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    invoke-direct {v0, p1, p2}, Lcom/sec/samsung/gallery/access/face/PersonList;->open(II)V

    move-object v1, v0

    .line 178
    .end local v0    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    .restart local v1    # "personList":Lcom/sec/samsung/gallery/access/face/PersonList;
    goto :goto_0
.end method

.method public static remove(Landroid/content/Context;I)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    const/4 v11, 0x0

    const v5, 0xf4240

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 207
    if-nez p0, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 209
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 210
    .local v1, "cr":Landroid/content/ContentResolver;
    if-ge p1, v5, :cond_3

    .line 211
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 212
    .local v4, "values":Landroid/content/ContentValues;
    const-string v5, "person_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 213
    const-string v5, "recommended_id"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 214
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const-string v6, "recommended_id=?"

    new-array v7, v9, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v1, v5, v4, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 216
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->KEY_FACES_URI:Landroid/net/Uri;

    const-string v6, "person_id=?"

    new-array v7, v9, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 217
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/access/face/PersonList;->getCover(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 218
    .local v0, "cover":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 219
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 220
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 221
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 223
    .end local v2    # "file":Ljava/io/File;
    :cond_2
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v6, p1

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 224
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v1, v3, v11, v11}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 225
    .end local v0    # "cover":Ljava/lang/String;
    .end local v3    # "uri":Landroid/net/Uri;
    .end local v4    # "values":Landroid/content/ContentValues;
    :cond_3
    if-le p1, v5, :cond_0

    .line 226
    sub-int/2addr p1, v5

    .line 227
    sget-object v5, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const-string v6, "group_id=?"

    new-array v7, v9, [Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static rename(Landroid/content/Context;ILjava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 232
    if-nez p0, :cond_1

    .line 244
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    const v3, 0xf4240

    if-ge p1, v3, :cond_0

    .line 237
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 238
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 239
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 240
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "name"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static setContactPhotoEx(Landroid/content/Context;Landroid/graphics/Bitmap;Ljava/lang/String;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "photo"    # Landroid/graphics/Bitmap;
    .param p2, "lookupkey"    # Ljava/lang/String;

    .prologue
    .line 523
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    .line 524
    const-string v2, "PersonList"

    const-string v3, "onActivityResult FACE_TAG_REQ_CROP  sendBroadcast"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 528
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 530
    .local v1, "strContactPackageName":Ljava/lang/String;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 531
    :cond_0
    const-string v1, "com.android.contacts"

    .line 534
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".action.SAVE_CROPPED_PHOTO"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 536
    const-string v2, "lookupkey"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    const-string v2, "photo"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 538
    const-string v2, "output"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "file://"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p0}, Lcom/sec/android/gallery3d/app/CropImage;->getCallerIdSavePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 539
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 541
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "strContactPackageName":Ljava/lang/String;
    :cond_2
    return-void
.end method

.method public static setUserData(Landroid/content/Context;ILjava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "userData"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 308
    if-eqz p0, :cond_0

    const v3, 0xf4240

    if-lt p1, v3, :cond_1

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 311
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 312
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 313
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "user_data"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static declared-synchronized syncAllContacts(Landroid/content/Context;Ljava/util/HashMap;)V
    .locals 26
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 328
    .local p1, "allLookupKeys":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;>;"
    const-class v25, Lcom/sec/samsung/gallery/access/face/PersonList;

    monitor-enter v25

    :try_start_0
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 329
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "name"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string v5, "_id"

    aput-object v5, v4, v3

    .line 330
    .local v4, "projection":[Ljava/lang/String;
    const/16 v22, 0x0

    .line 331
    .local v22, "nameCursor":Landroid/database/Cursor;
    const/16 v19, 0x0

    .local v19, "lookupKey":Ljava/lang/String;
    const/16 v16, 0x0

    .line 332
    .local v16, "joinedLookupKey":Ljava/lang/String;
    const/16 v18, 0x0

    .line 333
    .local v18, "key":Ljava/lang/String;
    const/4 v10, 0x0

    .line 334
    .local v10, "head":Ljava/lang/String;
    const/4 v8, 0x0

    .line 335
    .local v8, "actualName":Ljava/lang/String;
    const/16 v23, 0x0

    .line 336
    .local v23, "newKey":Ljava/lang/String;
    const/4 v15, 0x0

    .line 337
    .local v15, "info":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    const/16 v24, 0x0

    .line 339
    .local v24, "updatedLookupKey":Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 340
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 341
    const/4 v13, 0x0

    .line 343
    .local v13, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    if-nez v3, :cond_0

    .line 344
    new-instance v3, Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;-><init>(Landroid/content/Context;)V

    sput-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 348
    :cond_0
    :try_start_1
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v22

    .line 350
    if-eqz v22, :cond_4

    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 352
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v21

    .line 353
    .local v21, "name":Ljava/lang/String;
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 354
    .local v12, "id":I
    if-eqz v21, :cond_3

    const-string v3, "/"

    move-object/from16 v0, v21

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 355
    const-string v3, "person"

    const-string v5, "name = "

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v3, "person"

    const-string v5, "id = "

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v18, v3, v5

    .line 358
    const-string v3, "person"

    const-string v5, "key = "

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    const-string v3, "."

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 361
    const-string v3, "\\."

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v10, v3, v5

    .line 365
    :goto_0
    const-string v3, "-"

    invoke-virtual {v10, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v3, v3, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    move-object v15, v0

    .line 367
    if-eqz v15, :cond_2

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_d

    .line 368
    :cond_2
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mContactProvider:Lcom/sec/samsung/gallery/access/contact/ContactProvider;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getLookupKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    .line 369
    if-nez v24, :cond_6

    .line 370
    const-string v3, "query"

    const-string v5, "remove"

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/sec/samsung/gallery/access/face/PersonList;->remove(Landroid/content/Context;I)V

    .line 460
    :cond_3
    :goto_1
    invoke-interface/range {v22 .. v22}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v3

    if-nez v3, :cond_1

    .line 465
    .end local v12    # "id":I
    .end local v21    # "name":Ljava/lang/String;
    :cond_4
    :try_start_2
    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 468
    :goto_2
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    .local v11, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 469
    .local v17, "k":Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    check-cast v13, Ljava/util/ArrayList;

    .line 470
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/samsung/gallery/access/face/PersonList;->mergePersons(Landroid/content/Context;Ljava/util/ArrayList;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    .line 328
    .end local v2    # "cr":Landroid/content/ContentResolver;
    .end local v4    # "projection":[Ljava/lang/String;
    .end local v8    # "actualName":Ljava/lang/String;
    .end local v10    # "head":Ljava/lang/String;
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .end local v15    # "info":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .end local v16    # "joinedLookupKey":Ljava/lang/String;
    .end local v17    # "k":Ljava/lang/String;
    .end local v18    # "key":Ljava/lang/String;
    .end local v19    # "lookupKey":Ljava/lang/String;
    .end local v22    # "nameCursor":Landroid/database/Cursor;
    .end local v23    # "newKey":Ljava/lang/String;
    .end local v24    # "updatedLookupKey":Ljava/lang/String;
    :catchall_0
    move-exception v3

    monitor-exit v25

    throw v3

    .line 363
    .restart local v2    # "cr":Landroid/content/ContentResolver;
    .restart local v4    # "projection":[Ljava/lang/String;
    .restart local v8    # "actualName":Ljava/lang/String;
    .restart local v10    # "head":Ljava/lang/String;
    .restart local v12    # "id":I
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v15    # "info":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v16    # "joinedLookupKey":Ljava/lang/String;
    .restart local v18    # "key":Ljava/lang/String;
    .restart local v19    # "lookupKey":Ljava/lang/String;
    .restart local v21    # "name":Ljava/lang/String;
    .restart local v22    # "nameCursor":Landroid/database/Cursor;
    .restart local v23    # "newKey":Ljava/lang/String;
    .restart local v24    # "updatedLookupKey":Ljava/lang/String;
    :cond_5
    move-object/from16 v10, v18

    goto :goto_0

    .line 373
    :cond_6
    :try_start_3
    const-string v3, "query"

    const-string/jumbo v5, "update"

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    move-object/from16 v18, v24

    .line 376
    const-string v3, "."

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 377
    const-string v3, "\\."

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v10, v3, v5

    .line 381
    :goto_4
    const-string v3, "-"

    invoke-virtual {v10, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    aget-object v3, v3, v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    move-object v15, v0

    .line 383
    if-eqz v15, :cond_7

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_9

    .line 384
    :cond_7
    const-string v3, "query"

    const-string v5, "remove 1"

    invoke-static {v3, v5}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/sec/samsung/gallery/access/face/PersonList;->remove(Landroid/content/Context;I)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    .line 462
    .end local v12    # "id":I
    .end local v21    # "name":Ljava/lang/String;
    :catch_0
    move-exception v9

    .line 463
    .local v9, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_4
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 465
    :try_start_5
    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 379
    .end local v9    # "e":Ljava/lang/Exception;
    .restart local v12    # "id":I
    .restart local v21    # "name":Ljava/lang/String;
    :cond_8
    move-object/from16 v10, v18

    goto :goto_4

    .line 387
    :cond_9
    :try_start_6
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getSingleLookupKey()Ljava/lang/String;

    move-result-object v19

    .line 388
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getJoinedLookupKey()Ljava/lang/String;

    move-result-object v16

    .line 389
    const-string v3, "profile"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 391
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 393
    if-eqz v16, :cond_c

    .line 394
    const-string v3, "\\."

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 398
    .local v20, "lookupKeys":[Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/ArrayList;

    move-object v13, v0

    .line 399
    if-nez v13, :cond_a

    .line 400
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 402
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .local v14, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_7
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    move-object v13, v14

    .line 405
    .end local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_a
    const/4 v3, 0x0

    :try_start_8
    aget-object v3, v20, v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 406
    const/4 v3, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v13, v3, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 411
    :goto_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    aget-object v5, v20, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 412
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->rename(Landroid/content/Context;ILjava/lang/String;)V

    .line 414
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 415
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    goto/16 :goto_1

    .line 465
    .end local v12    # "id":I
    .end local v20    # "lookupKeys":[Ljava/lang/String;
    .end local v21    # "name":Ljava/lang/String;
    :catchall_1
    move-exception v3

    :goto_7
    :try_start_9
    invoke-static/range {v22 .. v22}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 408
    .restart local v12    # "id":I
    .restart local v20    # "lookupKeys":[Ljava/lang/String;
    .restart local v21    # "name":Ljava/lang/String;
    :cond_b
    :try_start_a
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 417
    .end local v20    # "lookupKeys":[Ljava/lang/String;
    :cond_c
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 418
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->rename(Landroid/content/Context;ILjava/lang/String;)V

    .line 419
    const-string v3, ""

    move-object/from16 v0, p0

    invoke-static {v0, v12, v3}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 424
    :cond_d
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getSingleLookupKey()Ljava/lang/String;

    move-result-object v19

    .line 425
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getJoinedLookupKey()Ljava/lang/String;

    move-result-object v16

    .line 426
    const-string v3, "profile"

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 428
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v8

    .line 430
    if-eqz v16, :cond_10

    .line 431
    const-string v3, "\\."

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v20

    .line 435
    .restart local v20    # "lookupKeys":[Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/util/ArrayList;

    move-object v13, v0

    .line 436
    if-nez v13, :cond_e

    .line 437
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 439
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_b
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    move-object/from16 v0, v16

    invoke-virtual {v3, v0, v14}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    move-object v13, v14

    .line 442
    .end local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :cond_e
    const/4 v3, 0x0

    :try_start_c
    aget-object v3, v20, v3

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 443
    const/4 v3, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v13, v3, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 448
    :goto_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x0

    aget-object v5, v20, v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 449
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->rename(Landroid/content/Context;ILjava/lang/String;)V

    .line 451
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 452
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V

    goto/16 :goto_1

    .line 445
    :cond_f
    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    .line 454
    .end local v20    # "lookupKeys":[Ljava/lang/String;
    :cond_10
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v23

    .line 455
    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v12, v1}, Lcom/sec/samsung/gallery/access/face/PersonList;->rename(Landroid/content/Context;ILjava/lang/String;)V

    .line 456
    const-string v3, ""

    move-object/from16 v0, p0

    invoke-static {v0, v12, v3}, Lcom/sec/samsung/gallery/access/face/PersonList;->updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_1

    .line 473
    .end local v12    # "id":I
    .end local v21    # "name":Ljava/lang/String;
    .restart local v11    # "i$":Ljava/util/Iterator;
    :cond_11
    :try_start_d
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->mergedData:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    .line 474
    sget-object v3, Lcom/sec/samsung/gallery/access/face/PersonList;->keys:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 475
    if-eqz v13, :cond_12

    .line 476
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 477
    const/4 v13, 0x0

    .line 479
    :cond_12
    monitor-exit v25

    return-void

    .line 465
    .end local v11    # "i$":Ljava/util/Iterator;
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v12    # "id":I
    .restart local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v20    # "lookupKeys":[Ljava/lang/String;
    .restart local v21    # "name":Ljava/lang/String;
    :catchall_2
    move-exception v3

    move-object v13, v14

    .end local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    goto/16 :goto_7

    .line 462
    .end local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :catch_1
    move-exception v9

    move-object v13, v14

    .end local v14    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    .restart local v13    # "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    goto/16 :goto_5
.end method

.method public static updateJoinedName(Landroid/content/Context;ILjava/lang/String;)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 247
    if-nez p0, :cond_1

    .line 258
    :cond_0
    :goto_0
    return-void

    .line 249
    :cond_1
    const v3, 0xf4240

    if-ge p1, v3, :cond_0

    .line 252
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 253
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->PERSONS_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 254
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 255
    .local v2, "values":Landroid/content/ContentValues;
    const-string/jumbo v3, "user_data"

    invoke-virtual {v2, v3, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/PersonList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 198
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public remove(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 204
    return-void
.end method
