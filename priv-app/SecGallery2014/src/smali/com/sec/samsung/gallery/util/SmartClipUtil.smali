.class public Lcom/sec/samsung/gallery/util/SmartClipUtil;
.super Ljava/lang/Object;
.source "SmartClipUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/SmartClipUtil$SmartClipListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/sec/samsung/gallery/util/SmartClipUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/SmartClipUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    return-void
.end method

.method public static getCurrentImageRect(Landroid/app/Activity;Lcom/sec/android/gallery3d/ui/PhotoView;Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 22
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "cropRect"    # Landroid/graphics/Rect;

    .prologue
    .line 33
    const/4 v11, 0x0

    .line 34
    .local v11, "rect":Landroid/graphics/Rect;
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getRectOfImage()Landroid/graphics/Rect;

    move-result-object v14

    .line 35
    .local v14, "rectf":Landroid/graphics/Rect;
    if-nez v14, :cond_0

    .line 36
    sget-object v18, Lcom/sec/samsung/gallery/util/SmartClipUtil;->TAG:Ljava/lang/String;

    const-string v19, "currnet image rect is null"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    const/16 v18, 0x0

    .line 76
    :goto_0
    return-object v18

    .line 39
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->bounds()Landroid/graphics/Rect;

    move-result-object v13

    .line 40
    .local v13, "rect1":Landroid/graphics/Rect;
    const-string/jumbo v18, "window"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Landroid/view/WindowManager;

    .line 41
    .local v17, "wm":Landroid/view/WindowManager;
    invoke-interface/range {v17 .. v17}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    .line 42
    .local v6, "display":Landroid/view/Display;
    invoke-virtual {v6}, Landroid/view/Display;->getWidth()I

    move-result v8

    .line 43
    .local v8, "displayWidth":I
    invoke-virtual {v6}, Landroid/view/Display;->getHeight()I

    move-result v7

    .line 45
    .local v7, "displayHeight":I
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v18, :cond_6

    move-object/from16 v18, p0

    .line 46
    check-cast v18, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getMultiWindowActivity()Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v10

    .line 47
    .local v10, "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-virtual {v10}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v18

    if-eqz v18, :cond_3

    .line 48
    invoke-virtual {v10}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v5

    .line 49
    .local v5, "currentZone":I
    sget v18, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_B:I

    move/from16 v0, v18

    if-ne v0, v5, :cond_2

    .line 50
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v18, v0

    const/16 v19, 0x2

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 51
    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v18, v8, v18

    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v9, v0

    .line 52
    .local v9, "left":F
    iget v0, v13, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v18, v8, v18

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v15, v0

    .line 53
    .local v15, "right":F
    new-instance v11, Landroid/graphics/Rect;

    .end local v11    # "rect":Landroid/graphics/Rect;
    float-to-int v0, v9

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    float-to-int v0, v15

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .end local v9    # "left":F
    .end local v15    # "right":F
    .restart local v11    # "rect":Landroid/graphics/Rect;
    :goto_1
    move-object v12, v11

    .line 70
    .end local v5    # "currentZone":I
    .end local v10    # "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .end local v11    # "rect":Landroid/graphics/Rect;
    .local v12, "rect":Landroid/graphics/Rect;
    :goto_2
    if-eqz v12, :cond_4

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    move-object/from16 v0, p2

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-virtual {v12, v0, v1, v2, v3}, Landroid/graphics/Rect;->intersects(IIII)Z

    move-result v18

    if-eqz v18, :cond_4

    .line 72
    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    if-ltz v18, :cond_5

    iget v0, v12, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v0, v8, :cond_5

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v18, v0

    if-ltz v18, :cond_5

    iget v0, v12, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    move/from16 v0, v18

    if-lt v0, v7, :cond_5

    .line 73
    new-instance v11, Landroid/graphics/Rect;

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v12, Landroid/graphics/Rect;->left:I

    move/from16 v20, v0

    sub-int v20, v8, v20

    iget v0, v12, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    sub-int v21, v7, v21

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .end local v12    # "rect":Landroid/graphics/Rect;
    .restart local v11    # "rect":Landroid/graphics/Rect;
    :goto_3
    move-object/from16 v18, v11

    .line 74
    goto/16 :goto_0

    .line 56
    .restart local v5    # "currentZone":I
    .restart local v10    # "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_1
    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v16, v0

    .line 57
    .local v16, "top":F
    iget v0, v13, Landroid/graphics/Rect;->bottom:I

    move/from16 v18, v0

    sub-int v18, v7, v18

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v19, v0

    add-int v18, v18, v19

    move/from16 v0, v18

    int-to-float v4, v0

    .line 58
    .local v4, "bottom":F
    new-instance v11, Landroid/graphics/Rect;

    .end local v11    # "rect":Landroid/graphics/Rect;
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    move/from16 v0, v16

    float-to-int v0, v0

    move/from16 v19, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    float-to-int v0, v4

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 60
    .restart local v11    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_1

    .line 62
    .end local v4    # "bottom":F
    .end local v16    # "top":F
    :cond_2
    new-instance v11, Landroid/graphics/Rect;

    .end local v11    # "rect":Landroid/graphics/Rect;
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v11    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_1

    .line 66
    .end local v5    # "currentZone":I
    :cond_3
    new-instance v11, Landroid/graphics/Rect;

    .end local v11    # "rect":Landroid/graphics/Rect;
    iget v0, v14, Landroid/graphics/Rect;->left:I

    move/from16 v18, v0

    iget v0, v14, Landroid/graphics/Rect;->top:I

    move/from16 v19, v0

    iget v0, v14, Landroid/graphics/Rect;->right:I

    move/from16 v20, v0

    iget v0, v14, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    move/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v21

    invoke-direct {v11, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .restart local v11    # "rect":Landroid/graphics/Rect;
    move-object v12, v11

    .end local v11    # "rect":Landroid/graphics/Rect;
    .restart local v12    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_2

    .line 76
    .end local v10    # "multiWindow":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    :cond_4
    const/4 v11, 0x0

    move-object/from16 v18, v11

    move-object v11, v12

    .end local v12    # "rect":Landroid/graphics/Rect;
    .restart local v11    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_0

    .end local v11    # "rect":Landroid/graphics/Rect;
    .restart local v12    # "rect":Landroid/graphics/Rect;
    :cond_5
    move-object v11, v12

    .end local v12    # "rect":Landroid/graphics/Rect;
    .restart local v11    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_3

    :cond_6
    move-object v12, v11

    .end local v11    # "rect":Landroid/graphics/Rect;
    .restart local v12    # "rect":Landroid/graphics/Rect;
    goto/16 :goto_2
.end method
