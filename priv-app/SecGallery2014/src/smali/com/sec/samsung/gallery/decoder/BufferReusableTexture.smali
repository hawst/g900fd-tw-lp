.class public Lcom/sec/samsung/gallery/decoder/BufferReusableTexture;
.super Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;
.source "BufferReusableTexture.java"


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V
    .locals 6
    .param p1, "cacheInterface"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p2, "bufferData"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p3, "bufferType"    # I
    .param p4, "fastReuse"    # Z

    .prologue
    .line 31
    iget-object v1, p1, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v2, p1, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-object v0, p0

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/BufferReusableTexture;-><init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/decoder/CacheInterface;Ljava/nio/ByteBuffer;IIIZ)V
    .locals 6
    .param p1, "cacheInterface"    # Lcom/sec/samsung/gallery/decoder/CacheInterface;
    .param p2, "buffer"    # Ljava/nio/ByteBuffer;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "type"    # I
    .param p6, "fastReuse"    # Z

    .prologue
    .line 27
    iget-object v1, p1, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    iget-object v2, p1, Lcom/sec/samsung/gallery/decoder/CacheInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    new-instance v3, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;

    invoke-direct {v3, p3, p4, p2}, Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;-><init>(IILjava/nio/ByteBuffer;)V

    move-object v0, p0

    move v4, p5

    move v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/BufferReusableTexture;-><init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V

    .line 28
    return-void
.end method

.method private constructor <init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V
    .locals 0
    .param p1, "xivInterface2"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .param p2, "xivInterface"    # Lcom/sec/samsung/gallery/decoder/XIVInterface;
    .param p3, "bufferData"    # Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;
    .param p4, "type"    # I
    .param p5, "fastReuse"    # Z

    .prologue
    .line 35
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/decoder/XIVInterface$BufferReusableTexture;-><init>(Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/XIVInterface;Lcom/sec/samsung/gallery/decoder/CacheInterface$ImageBufferInfo;IZ)V

    .line 36
    return-void
.end method
