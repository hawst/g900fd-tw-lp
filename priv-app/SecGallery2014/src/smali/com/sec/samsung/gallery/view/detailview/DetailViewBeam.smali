.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;
.super Ljava/lang/Object;
.source "DetailViewBeam.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mObserver:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 28
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mObserver:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public registerSettingObserver()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mObserver:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->register()V

    .line 72
    return-void
.end method

.method public setBeamListener()V
    .locals 4

    .prologue
    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)V

    .line 45
    .local v0, "onGetFilePathListener":Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 49
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "ANDROID_BEAM"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 51
    return-void
.end method

.method public unregisterSettingObserver()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mObserver:Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$SBeamStateObserver;->unregister()V

    .line 76
    return-void
.end method

.method public unsetBeamListener()V
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;)V

    .line 62
    .local v0, "onGetFilePathListener":Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object v0, v1, v2

    .line 66
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "ANDROID_BEAM"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 68
    return-void
.end method
