.class Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;
.super Landroid/os/AsyncTask;
.source "SoundShotEditorCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteSoundShot"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaItem;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "DeleteSoundDeleteSoundShot"


# instance fields
.field private mDialog:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$1;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;-><init>(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)V

    return-void
.end method

.method private delete(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 3
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 87
    :try_start_0
    const-string v1, "TAG"

    const-string v2, "Delete the audio data"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->stopSoundScene()V

    .line 90
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SoundShot_000"

    invoke-static {v1, v2}, Lcom/sec/android/secvision/sef/SEF;->deleteSEFData(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_0

    .line 91
    const-string v1, "TAG"

    const-string v2, "Error while delete the audio data"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->update(Lcom/sec/android/gallery3d/data/MediaItem;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return-void

    .line 96
    :catch_0
    move-exception v0

    .line 97
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 98
    const-string v1, "DeleteSoundDeleteSoundShot"

    const-string v2, "Error while delete the audio data"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showProgressDialog()V
    .locals 3

    .prologue
    .line 112
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->mDialog:Landroid/app/ProgressDialog;

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->mDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00ca

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->mDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 116
    return-void
.end method

.method private stopSoundScene()V
    .locals 3

    .prologue
    .line 103
    sget-boolean v1, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isPlayingState:Z

    if-eqz v1, :cond_0

    .line 104
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;->STOP_PLAY_SOUND:Lcom/sec/samsung/gallery/controller/SoundSceneCmd$SoundSceneCmdType;

    aput-object v2, v0, v1

    .line 107
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$100(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SOUND_SCENE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 109
    .end local v0    # "params":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method private update(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 119
    check-cast p1, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p1    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/LocalImage;->updateSoundShotAttribute()V

    .line 120
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 55
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->doInBackground([Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaItem;)Ljava/lang/Void;
    .locals 1
    .param p1, "mediaItem"    # [Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 81
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->delete(Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 82
    const/4 v0, 0x0

    return-object v0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 55
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 5
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 62
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->mDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v3}, Landroid/app/ProgressDialog;->dismiss()V

    .line 64
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 66
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/ActivityState;->onRefresh()V

    .line 67
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->this$0:Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;->access$000(Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e04a5

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 68
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v3

    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE_CONFIRM:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    .line 69
    .local v1, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v2, Ljava/util/Observable;

    invoke-direct {v2}, Ljava/util/Observable;-><init>()V

    .line 70
    .local v2, "observable":Ljava/util/Observable;
    invoke-virtual {v0, v2, v1}, Lcom/sec/android/gallery3d/app/ActivityState;->update(Ljava/util/Observable;Ljava/lang/Object;)V

    .line 71
    return-void
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 75
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/SoundShotEditorCmd$DeleteSoundShot;->showProgressDialog()V

    .line 77
    return-void
.end method
