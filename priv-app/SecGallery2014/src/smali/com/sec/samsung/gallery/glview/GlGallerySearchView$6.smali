.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 460
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->isScreenLocked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 479
    :cond_0
    :goto_0
    return v5

    :cond_1
    move-object v1, p1

    .line 463
    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 465
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 466
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-ne v1, v2, :cond_4

    .line 467
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 472
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v1, :cond_0

    .line 473
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mMode:I

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-ne v1, v2, :cond_3

    .line 474
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iput v0, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 476
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-interface {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 477
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v4, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    goto :goto_0

    .line 469
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$6;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$600(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1
.end method
