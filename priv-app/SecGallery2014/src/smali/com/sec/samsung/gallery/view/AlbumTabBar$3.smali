.class Lcom/sec/samsung/gallery/view/AlbumTabBar$3;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;->initActionBarTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private getList()[Ljava/lang/String;
    .locals 14

    .prologue
    const v13, 0x7f09003b

    const v12, 0x7f09003a

    const v11, 0x7f090039

    .line 125
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 126
    .local v1, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v5

    .line 127
    .local v5, "isKnox":Z
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v10, :cond_1

    .line 128
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09003d

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 187
    :cond_0
    :goto_0
    return-object v7

    .line 129
    :cond_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$000(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 130
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f09003c

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 132
    :cond_2
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v10, :cond_5

    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DViewMode:Z

    if-eqz v10, :cond_5

    .line 133
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v10, :cond_3

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v10

    if-eqz v10, :cond_3

    if-nez v5, :cond_3

    .line 134
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 135
    :cond_3
    if-eqz v5, :cond_4

    .line 136
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 138
    :cond_4
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 141
    :cond_5
    const/4 v9, 0x0

    .line 142
    .local v9, "tempOptions":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 143
    .local v7, "resultOptions":[Ljava/lang/String;
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v10, :cond_b

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v10

    if-eqz v10, :cond_b

    if-nez v5, :cond_b

    .line 144
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v12}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    .line 150
    :goto_1
    const/4 v4, 0x1

    .line 151
    .local v4, "isAdd":Z
    move-object v0, v9

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v6, :cond_d

    aget-object v8, v0, v2

    .line 152
    .local v8, "tab":Ljava/lang/String;
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-nez v10, :cond_6

    .line 153
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e00a7

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 154
    const/4 v4, 0x0

    .line 158
    :cond_6
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUse3DViewMode:Z

    if-nez v10, :cond_7

    .line 159
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e0073

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 160
    const/4 v4, 0x0

    .line 164
    :cond_7
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v10, :cond_8

    .line 165
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e00a9

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 166
    const/4 v4, 0x0

    .line 170
    :cond_8
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-nez v10, :cond_9

    .line 171
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e0441

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 172
    const/4 v4, 0x0

    .line 176
    :cond_9
    if-eqz v4, :cond_a

    .line 177
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_a
    const/4 v4, 0x1

    .line 151
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 145
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v2    # "i$":I
    .end local v4    # "isAdd":Z
    .end local v6    # "len$":I
    .end local v8    # "tab":Ljava/lang/String;
    :cond_b
    if-eqz v5, :cond_c

    .line 146
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v13}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 148
    :cond_c
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v10, v10, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_1

    .line 181
    .restart local v0    # "arr$":[Ljava/lang/String;
    .restart local v2    # "i$":I
    .restart local v4    # "isAdd":Z
    .restart local v6    # "len$":I
    :cond_d
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v7, v10, [Ljava/lang/String;

    .line 182
    const/4 v3, 0x0

    .line 183
    .local v3, "idx":I
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 184
    .restart local v8    # "tab":Ljava/lang/String;
    aput-object v8, v7, v3

    .line 185
    add-int/lit8 v3, v3, 0x1

    .line 186
    goto :goto_3
.end method

.method private initTabForPhone()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationMode()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$400(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$500(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 196
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->getList()[Ljava/lang/String;

    move-result-object v5

    .line 197
    .local v5, "options":[Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    new-instance v0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v2, v1, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f03000a

    const v4, 0x1020014

    move-object v1, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$3;Landroid/content/Context;II[Ljava/lang/String;[Ljava/lang/String;)V

    # setter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v7, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$402(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/widget/ArrayAdapter;)Landroid/widget/ArrayAdapter;

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$400(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ArrayAdapter;

    move-result-object v0

    const v1, 0x7f030009

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsForceInitDropdownlist:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$502(Lcom/sec/samsung/gallery/view/AlbumTabBar;Z)Z

    .line 258
    .end local v5    # "options":[Ljava/lang/String;
    :cond_2
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsTablet:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$300(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 122
    :goto_0
    return-void

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->initTabForPhone()V

    goto :goto_0
.end method
