.class public Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "HelpDetailActionBarForNormal.java"


# static fields
.field public static ACTIONBAR_BG_TYPE_DEFAULT:I

.field public static ACTIONBAR_BG_TYPE_SHADOW:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->ACTIONBAR_BG_TYPE_DEFAULT:I

    .line 17
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->ACTIONBAR_BG_TYPE_SHADOW:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 23
    sget v0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->ACTIONBAR_BG_TYPE_SHADOW:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->initActionBarBg(I)V

    .line 24
    return-void
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f120024

    return v0
.end method


# virtual methods
.method protected initActionBarBg(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 27
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02003c

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 32
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->ACTIONBAR_BG_TYPE_SHADOW:I

    if-ne p1, v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ab

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 37
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 38
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 39
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 40
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 56
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x0

    .line 44
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 45
    const v0, 0x7f0f02c0

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuGroupVisibility(Landroid/view/Menu;IZ)V

    .line 47
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    if-eqz v0, :cond_0

    .line 48
    const v0, 0x7f0f029c

    invoke-static {p1, v0, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    const v0, 0x7f0f02c1

    invoke-static {p1, v0, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method
