.class public Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "NewAlbumAlertDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;
    }
.end annotation


# static fields
.field private static final HIDE_SOFT_INPUT:I = 0x0

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"


# instance fields
.field private alertEditText:Landroid/widget/EditText;

.field private isSipVisible:Z

.field private final mCtx:Landroid/content/Context;

.field private mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

.field mDoDismissDialog:Z

.field private mHandler:Landroid/os/Handler;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mNewAlbumDialogIsReady:Z

.field private mReceiveTime:J

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mSipReceiver:Landroid/content/BroadcastReceiver;

.field private mTooLongToast:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "ctx"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 69
    move-object v0, p0

    move-object v1, p1

    move v4, v2

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;-><init>(Landroid/content/Context;ZLjava/util/ArrayList;ILjava/lang/String;)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLjava/util/ArrayList;ILjava/lang/String;)V
    .locals 7
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "isNewEvent"    # Z
    .param p4, "bucketId"    # I
    .param p5, "albumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Z",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 72
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 55
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDoDismissDialog:Z

    .line 56
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z

    .line 57
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mHandler:Landroid/os/Handler;

    .line 62
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mReceiveTime:J

    .line 64
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;

    .line 73
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    .line 74
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    move-object v1, p0

    move v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/content/Context;ZLjava/util/ArrayList;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    .line 75
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mHandler:Landroid/os/Handler;

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->hideSoftInput(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/EditText;)Landroid/widget/EditText;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Landroid/widget/EditText;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->alertEditText:Landroid/widget/EditText;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->setSipBroadcastReceiver()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->isSipVisible:Z

    return v0
.end method

.method static synthetic access$1502(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->isSipVisible:Z

    return p1
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mReceiveTime:J

    return-wide v0
.end method

.method static synthetic access$1702(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # J

    .prologue
    .line 49
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mTooLongToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mTooLongToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->getSuggestedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z

    return p1
.end method

.method private getSuggestedName()Ljava/lang/String;
    .locals 9

    .prologue
    .line 499
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e01a6

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 501
    .local v2, "basePathStr":Ljava/lang/String;
    const-wide/16 v0, 0x1

    .line 502
    .local v0, "albumCount":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 503
    .local v5, "tgtPathStr":Ljava/lang/String;
    const/4 v4, 0x0

    .line 505
    .local v4, "isFind":Z
    :goto_0
    if-nez v4, :cond_1

    .line 506
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v5}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 507
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v6

    if-nez v6, :cond_0

    .line 508
    sget-object v6, Lcom/sec/android/gallery3d/util/GalleryUtils;->DEFAULT_NEW_ALBUM_DIR:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    .line 510
    const/4 v4, 0x1

    goto :goto_0

    .line 512
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 513
    const-wide/16 v6, 0x1

    add-long/2addr v0, v6

    goto :goto_0

    .line 516
    .end local v3    # "file":Ljava/io/File;
    :cond_1
    return-object v5
.end method

.method private hideSoftInput(Landroid/os/IBinder;)V
    .locals 4
    .param p1, "windowToken"    # Landroid/os/IBinder;

    .prologue
    .line 480
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 482
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 484
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 488
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 485
    :catch_0
    move-exception v0

    .line 486
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private setSipBroadcastReceiver()V
    .locals 3

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 434
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 437
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 438
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    .line 446
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 447
    return-void
.end method


# virtual methods
.method public dismissDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 460
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 461
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 462
    const v3, 0x7f0f0069

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 463
    .local v1, "edit":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 464
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 465
    .local v2, "msg":Landroid/os/Message;
    iput v4, v2, Landroid/os/Message;->what:I

    .line 466
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 467
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 470
    .end local v1    # "edit":Landroid/view/View;
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z

    if-eqz v3, :cond_1

    .line 471
    if-eqz v0, :cond_1

    .line 472
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 473
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z

    .line 476
    :cond_1
    return-void
.end method

.method public getNewAlbumDialogIsReady()Z
    .locals 1

    .prologue
    .line 520
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mNewAlbumDialogIsReady:Z

    return v0
.end method

.method public setCurrentName(Ljava/lang/String;)V
    .locals 1
    .param p1, "currentName"    # Ljava/lang/String;

    .prologue
    .line 491
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->setCurrentName(Ljava/lang/String;)V

    .line 492
    return-void
.end method

.method public setDialogDismiss(Z)V
    .locals 0
    .param p1, "dismiss"    # Z

    .prologue
    .line 495
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDoDismissDialog:Z

    .line 496
    return-void
.end method

.method public showNewAlbumDialog()V
    .locals 4

    .prologue
    .line 450
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 452
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mCtx:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v0

    .line 454
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
