.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "HiddenMediaViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 111
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v0

    .line 112
    .local v0, "notiName":Ljava/lang/String;
    const-string v2, "EXIT_SELECTION_MODE"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 114
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 115
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 116
    .local v1, "reset":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 117
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 118
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 119
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$1;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged()V

    .line 123
    .end local v1    # "reset":I
    :cond_0
    return-void
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    return-object v0
.end method
