.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;
.super Ljava/lang/Object;
.source "GlComposeAlbumView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DragInfo"
.end annotation


# instance fields
.field public mActive:Z

.field private mDnAreaY:F

.field private mThumbOjbectHeight:F

.field private mUpAreaY:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V
    .locals 1

    .prologue
    .line 560
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 561
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mActive:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$1;

    .prologue
    .line 560
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V

    return-void
.end method


# virtual methods
.method public getIntensity(FF)F
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 577
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float/2addr v0, v1

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    .line 580
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeightSpace:F

    div-float/2addr v0, v2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float p2, v0, v1

    .line 585
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mThumbOjbectHeight:F

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    sub-float/2addr p2, v0

    .line 587
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mUpAreaY:F

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    .line 588
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mUpAreaY:F

    sub-float v0, p2, v0

    .line 593
    :goto_0
    return v0

    .line 590
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mDnAreaY:F

    cmpg-float v0, p2, v0

    if-gez v0, :cond_2

    .line 591
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mDnAreaY:F

    sub-float v0, p2, v0

    goto :goto_0

    .line 593
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 567
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeight(I)F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeight(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mThumbOjbectHeight:F

    .line 571
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeightSpace:F

    div-float/2addr v0, v3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mThumbOjbectHeight:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mUpAreaY:F

    .line 573
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeightSpace:F

    neg-float v0, v0

    div-float/2addr v0, v3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mThumbOjbectHeight:F

    const/high16 v2, 0x40400000    # 3.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mDnAreaY:F

    .line 574
    return-void
.end method
