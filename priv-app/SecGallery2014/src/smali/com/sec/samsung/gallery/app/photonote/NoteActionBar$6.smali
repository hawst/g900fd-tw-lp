.class Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;
.super Ljava/lang/Object;
.source "NoteActionBar.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "arg1"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const v4, 0x7f0f014c

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 226
    if-nez p3, :cond_1

    .line 244
    :cond_0
    :goto_0
    return v0

    .line 229
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-ne v2, v1, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x42

    if-eq v2, v3, :cond_2

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x17

    if-ne v2, v3, :cond_0

    .line 232
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->openToolBar()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    const v2, 0x7f0f014b

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->requestFocus(I)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$300(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;I)V

    .line 241
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$100(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    move v0, v1

    .line 242
    goto :goto_0

    .line 235
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0f013e

    if-ne v2, v3, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->closeToolBar()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$400(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$6;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->requestFocus(I)V
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$300(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;I)V

    goto :goto_1
.end method
