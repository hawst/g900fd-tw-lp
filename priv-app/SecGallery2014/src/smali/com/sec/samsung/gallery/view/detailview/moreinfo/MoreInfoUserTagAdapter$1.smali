.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;
.super Ljava/lang/Object;
.source "MoreInfoUserTagAdapter.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)V
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 31
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 32
    .local v1, "position":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v5, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    monitor-enter v5

    .line 33
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 34
    .local v3, "tag":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Ljava/util/HashMap;

    move-result-object v4

    if-nez v4, :cond_0

    .line 35
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->getBaseUri()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTagCountMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 38
    :cond_0
    const/4 v2, 0x0

    .line 39
    .local v2, "showToast":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 40
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mUserTagCountMap:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 41
    .local v0, "c":Ljava/lang/Integer;
    if-eqz v0, :cond_3

    .line 42
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInfos:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-nez v4, :cond_3

    .line 43
    :cond_2
    const/4 v2, 0x1

    .line 50
    .end local v0    # "c":Ljava/lang/Integer;
    :cond_3
    :goto_0
    if-eqz v2, :cond_5

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mDeleteTags:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Landroid/widget/Toast;

    move-result-object v4

    if-nez v4, :cond_4

    .line 52
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mContext:Landroid/content/Context;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mContext:Landroid/content/Context;

    const v8, 0x7f0e0470

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v6

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mDeleteTags:Landroid/widget/Toast;
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$102(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 55
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mDeleteTags:Landroid/widget/Toast;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 57
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mInterim:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 58
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    if-eqz v4, :cond_6

    .line 59
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->isEdited()Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;)Z

    move-result v6

    invoke-interface {v4, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;->onEdit(Z)V

    .line 61
    :cond_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;->notifyDirty()V

    .line 62
    monitor-exit v5

    .line 63
    return-void

    .line 47
    :cond_7
    const/4 v2, 0x1

    goto :goto_0

    .line 62
    .end local v2    # "showToast":Z
    .end local v3    # "tag":Ljava/lang/String;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method
