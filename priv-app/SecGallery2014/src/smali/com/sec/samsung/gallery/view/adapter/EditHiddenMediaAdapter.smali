.class public Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;
.source "EditHiddenMediaAdapter.java"


# instance fields
.field private mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ZLcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "pick"    # Z
    .param p4, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 17
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ZLcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 14
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->mConfig:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 19
    return-void
.end method


# virtual methods
.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 23
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 25
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 27
    return-object v0
.end method
