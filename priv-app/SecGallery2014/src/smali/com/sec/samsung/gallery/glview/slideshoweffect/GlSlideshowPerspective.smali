.class public Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;
.super Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;
.source "GlSlideshowPerspective.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;
    }
.end annotation


# static fields
.field private static final IMAGE_DEPTH:I = 0xfa

.field private static final IMAGE_SET_COUNT:I = 0x6

.field private static final ROTATION_VALUE:F = -40.0f

.field private static mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsFirstframe:Z

.field private mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

.field private final mPerspectiveOrder:[I

.field private mResetIndex:I


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;-><init>(Lcom/sec/samsung/gallery/glview/GlSlideShowView;Landroid/content/Context;)V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    .line 63
    const/4 v0, 0x6

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    .line 65
    const/4 v0, 0x5

    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    .line 67
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsFirstframe:Z

    .line 72
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    .line 73
    return-void
.end method

.method private resetOutOfScreenObj(I)V
    .locals 7
    .param p1, "objIndex"    # I

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x5

    .line 365
    const/4 v1, 0x5

    .line 366
    .local v1, "resetPos":I
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 367
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v5

    iget v2, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v3, v3, v5

    iget v3, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v4, v4, v5

    iget v4, v4, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    invoke-virtual {v0, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 369
    invoke-virtual {v0, v6, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 370
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v5

    iget v2, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    .line 371
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v5

    iget v2, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDimEx(F)V

    .line 372
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 373
    return-void
.end method

.method private setInitialArrayPosition()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x3f8ccccd    # 1.1f

    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, v4

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 377
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, v4

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 380
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, v5

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 383
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v1, v1

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 385
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    const/high16 v2, 0x40c00000    # 6.0f

    div-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 386
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v1, v1

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v1, v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 391
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    neg-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    .line 392
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v1, v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    .line 393
    return-void
.end method

.method private setObjInitialposition(Lcom/sec/samsung/gallery/glview/GlBaseObject;I)V
    .locals 4
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "index"    # I

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 396
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, p2

    iget v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v1, v1, p2

    iget v1, v1, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, p2

    iget v2, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    invoke-virtual {p1, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 398
    invoke-virtual {p1, v3, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setScale(FF)V

    .line 399
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, p2

    iget v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    .line 400
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v0, v0, p2

    iget v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDimEx(F)V

    .line 401
    invoke-virtual {p1, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 402
    return-void
.end method


# virtual methods
.method public checkForVideoItem(I)V
    .locals 3
    .param p1, "itemIndex"    # I

    .prologue
    .line 271
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 272
    .local v0, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v1

    if-nez v1, :cond_0

    .line 273
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    .line 274
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    new-instance v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$1;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;Lcom/sec/android/gallery3d/data/MediaItem;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 282
    :cond_0
    return-void
.end method

.method public getDistance()F
    .locals 1

    .prologue
    .line 77
    const/high16 v0, 0x43e10000    # 450.0f

    return v0
.end method

.method public getMaxDisplayObjCount()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x6

    return v0
.end method

.method public getWaitDuration()J
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsFirstframe:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->getWaitDuration()J

    move-result-wide v0

    .line 90
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0xc8

    goto :goto_0
.end method

.method public isBitmapdecoded()Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v2, :cond_1

    .line 96
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v3

    if-lt v2, v3, :cond_0

    .line 97
    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 99
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 100
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 114
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return v1

    .line 103
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_2
    if-eqz v0, :cond_1

    .line 105
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 106
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 107
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    .line 108
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    const/4 v3, 0x6

    if-lt v2, v3, :cond_1

    .line 109
    iget-wide v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    iget-wide v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mWaitDuration:J

    add-long/2addr v2, v4

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setDuration(J)V

    .line 110
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isInitialLoaded()Z
    .locals 10

    .prologue
    const/4 v7, 0x6

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 118
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v3, :cond_4

    .line 120
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    if-nez v3, :cond_2

    .line 122
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v3, v6, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 123
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v3

    if-eqz v3, :cond_0

    move v3, v4

    .line 145
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :goto_0
    return v3

    .line 126
    .restart local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 127
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 128
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v6

    if-lt v3, v6, :cond_1

    .line 129
    iput v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 130
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    .line 132
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    if-lt v3, v7, :cond_4

    .line 133
    iput v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSetImageCount:I

    .line 134
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v7, :cond_3

    .line 135
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    .line 136
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageCache:Ljava/util/LinkedList;

    invoke-virtual {v3, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 137
    invoke-direct {p0, v2, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setObjInitialposition(Lcom/sec/samsung/gallery/glview/GlBaseObject;I)V

    .line 138
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    .line 134
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 140
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget-wide v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    iget-wide v8, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mWaitDuration:J

    add-long/2addr v6, v8

    invoke-virtual {p0, v6, v7}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setDuration(J)V

    .line 141
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsFirstframe:Z

    move v3, v5

    .line 142
    goto :goto_0

    .end local v1    # "i":I
    :cond_4
    move v3, v4

    .line 145
    goto :goto_0
.end method

.method public isNextLoaded()Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 150
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    if-eqz v5, :cond_0

    .line 151
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 152
    .local v2, "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v5

    sput-object v5, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 153
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v5, :cond_1

    sget-object v5, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v5

    if-eqz v5, :cond_1

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    const/4 v6, 0x4

    if-ne v5, v6, :cond_1

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v5

    if-nez v5, :cond_1

    .line 155
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    if-eqz v5, :cond_1

    .line 185
    .end local v2    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return v4

    .line 158
    .restart local v2    # "mediaitem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getSlideshowVideoView()Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    move-result-object v5

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    if-nez v5, :cond_0

    .line 160
    :cond_2
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsFirstframe:Z

    .line 161
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v5, v6, v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 162
    .local v0, "bmp":Landroid/graphics/Bitmap;
    if-nez v0, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->isRequestInProgress(I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 166
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 167
    iget v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v6

    if-lt v5, v6, :cond_4

    .line 168
    iput v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 171
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v3

    .line 173
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v4

    if-eqz v4, :cond_5

    .line 174
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getContentBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 175
    .local v1, "frameBmp":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_5

    .line 176
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 181
    .end local v1    # "frameBmp":Landroid/graphics/Bitmap;
    :cond_5
    invoke-virtual {p0, v3, v0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V

    .line 182
    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->resetOutOfScreenObj(I)V

    .line 183
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public onCreate()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x4

    .line 190
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    sput-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 191
    sget-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getImageIndex()I

    move-result v1

    .line 193
    .local v1, "lastIndex":I
    sget-boolean v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    if-eqz v2, :cond_0

    .line 194
    iput v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    .line 195
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isSlideShowPaused:Z

    .line 197
    :cond_0
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    if-nez v2, :cond_1

    .line 198
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    iput v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mImageIndex:I

    .line 199
    :cond_1
    new-array v2, v8, [Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    .line 201
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_2

    .line 202
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    new-instance v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;-><init>(Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;)V

    aput-object v3, v2, v0

    .line 204
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDistance()F

    move-result v3

    neg-float v3, v3

    mul-int/lit16 v4, v0, 0xfa

    add-int/lit8 v4, v4, 0x32

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    .line 205
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v0

    const/high16 v3, 0x3f800000    # 1.0f

    int-to-float v4, v0

    const v5, 0x3e4ccccd    # 0.2f

    mul-float/2addr v4, v5

    sub-float/2addr v3, v4

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    .line 206
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v0

    const/high16 v3, -0x3de00000    # -40.0f

    int-to-float v4, v0

    mul-float/2addr v3, v4

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    .line 207
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    aput v0, v2, v0

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v3, v3, v6

    iget v3, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    .line 210
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v3, v3, v6

    iget v3, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v3, v3, v6

    iget v3, v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    iput v3, v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    .line 213
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setInitialArrayPosition()V

    .line 214
    return-void
.end method

.method protected onDestroy()V
    .locals 5

    .prologue
    .line 226
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v3

    sput-object v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 227
    sget-object v3, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->setImageIndex(I)V

    .line 228
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 229
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    const/4 v3, 0x6

    if-ge v2, v3, :cond_1

    .line 230
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v1

    .line 231
    .local v1, "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 232
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getContentBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 233
    .local v0, "frameBmp":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 234
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 229
    .end local v0    # "frameBmp":Landroid/graphics/Bitmap;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 239
    .end local v1    # "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_1
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onDestroy()V

    .line 240
    return-void
.end method

.method public onLayout()V
    .locals 4

    .prologue
    .line 244
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setInitialArrayPosition()V

    .line 246
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v2, :cond_0

    .line 255
    :goto_0
    return-void

    .line 249
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v2, 0x6

    if-ge v1, v2, :cond_1

    .line 250
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    .line 251
    .local v0, "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    aget v2, v2, v1

    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setObjInitialposition(Lcom/sec/samsung/gallery/glview/GlBaseObject;I)V

    .line 249
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 254
    .end local v0    # "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    goto :goto_0
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 259
    const/4 v1, 0x0

    .line 260
    .local v1, "orderTemp":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    const/4 v3, 0x5

    aget v1, v2, v3

    .line 261
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-lez v0, :cond_0

    .line 262
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    add-int/lit8 v4, v0, -0x1

    aget v3, v3, v4

    aput v3, v2, v0

    .line 261
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 264
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    const/4 v3, 0x0

    aput v1, v2, v3

    .line 265
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    sput-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 266
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v2, :cond_1

    sget-object v2, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 267
    iget v2, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->checkForVideoItem(I)V

    .line 268
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 218
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowEffect;->onStop()V

    .line 220
    iget v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    .line 221
    iget v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mAdapter:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowAdapter;->getCount()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mCurrentIndex:I

    .line 222
    return-void
.end method

.method protected onUpdate(F)V
    .locals 18
    .param p1, "ratio"    # F

    .prologue
    .line 286
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mWaitDuration:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    long-to-float v13, v14

    mul-float v2, p1, v13

    .line 287
    .local v2, "currentDuration":F
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->isPlayingVideoItem:Z

    if-eqz v13, :cond_1

    .line 362
    :cond_0
    return-void

    .line 289
    :cond_1
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    long-to-float v13, v14

    cmpg-float v13, v2, v13

    if-lez v13, :cond_2

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsSlideShowResumed:Z

    if-eqz v13, :cond_6

    .line 290
    :cond_2
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    long-to-float v13, v14

    div-float v3, v2, v13

    .line 291
    .local v3, "dRatio":F
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mIsSlideShowResumed:Z

    .line 292
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    const/4 v13, 0x6

    if-ge v11, v13, :cond_0

    .line 293
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v9

    .line 294
    .local v9, "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v9, v13}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 295
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    aget v12, v13, v11

    .line 296
    .local v12, "targetIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    add-int/lit8 v14, v11, 0x1

    rem-int/lit8 v14, v14, 0x6

    aget v10, v13, v14

    .line 298
    .local v10, "fromIndex":I
    if-eqz v10, :cond_4

    .line 299
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v5

    .line 302
    .local v5, "deltaRoll":F
    const v13, -0x40cccccd    # -0.7f

    cmpl-float v13, v5, v13

    if-lez v13, :cond_3

    const v13, 0x3f333333    # 0.7f

    cmpg-float v13, v5, v13

    if-gez v13, :cond_3

    .line 303
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    .line 307
    :goto_1
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v6

    .line 310
    .local v6, "deltaX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v7

    .line 314
    .local v7, "deltaY":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v8

    .line 318
    .local v8, "deltaZ":F
    invoke-virtual {v9, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 320
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mDim:F

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v4

    .line 323
    .local v4, "deltaDim":F
    invoke-virtual {v9, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDimEx(F)V

    .line 324
    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v9, v13}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setAlphaEx(F)V

    .line 292
    .end local v4    # "deltaDim":F
    .end local v8    # "deltaZ":F
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    .line 305
    .end local v6    # "deltaX":F
    .end local v7    # "deltaY":F
    :cond_3
    invoke-virtual {v9, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    goto :goto_1

    .line 326
    .end local v5    # "deltaRoll":F
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v10

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mRoll:F

    const/high16 v15, -0x3de00000    # -40.0f

    sub-float/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v5

    .line 329
    .restart local v5    # "deltaRoll":F
    const v13, -0x40cccccd    # -0.7f

    cmpl-float v13, v5, v13

    if-lez v13, :cond_5

    const v13, 0x3f333333    # 0.7f

    cmpg-float v13, v5, v13

    if-gez v13, :cond_5

    .line 330
    const/4 v13, 0x0

    invoke-virtual {v9, v13}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    .line 334
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mWidthSpace:F

    const v15, 0x3f99999a    # 1.2f

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v6

    .line 336
    .restart local v6    # "deltaX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->mHeightSpace:F

    neg-float v14, v14

    const/high16 v15, 0x40000000    # 2.0f

    mul-float/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v7

    .line 339
    .restart local v7    # "deltaY":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v10

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    invoke-virtual {v9, v6, v7, v13}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 340
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    goto :goto_2

    .line 332
    .end local v6    # "deltaX":F
    .end local v7    # "deltaY":F
    :cond_5
    invoke-virtual {v9, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V

    goto :goto_3

    .line 344
    .end local v3    # "dRatio":F
    .end local v5    # "deltaRoll":F
    .end local v9    # "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .end local v10    # "fromIndex":I
    .end local v11    # "i":I
    .end local v12    # "targetIndex":I
    :cond_6
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mEffectDuration:J

    long-to-float v13, v14

    sub-float v13, v2, v13

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mWaitDuration:J

    long-to-float v14, v14

    div-float v3, v13, v14

    .line 346
    .restart local v3    # "dRatio":F
    const/4 v11, 0x0

    .restart local v11    # "i":I
    :goto_4
    const/4 v13, 0x6

    if-ge v11, v13, :cond_0

    .line 347
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mResetIndex:I

    if-ne v13, v11, :cond_7

    .line 346
    :goto_5
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 351
    :cond_7
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v9

    .line 352
    .restart local v9    # "from":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveOrder:[I

    aget v12, v13, v11

    .line 353
    .restart local v12    # "targetIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v12

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mZ:F

    const/high16 v15, 0x42200000    # 40.0f

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v13, v14}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->getDelta_linear(FFF)F

    move-result v8

    .line 357
    .restart local v8    # "deltaZ":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v13, v13, v12

    iget v13, v13, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mX:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mPerspectiveObjInfoArray:[Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;

    aget-object v14, v14, v12

    iget v14, v14, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective$PerspectiveObjPositionInfo;->mY:F

    invoke-virtual {v9, v13, v14, v8}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    goto :goto_5
.end method

.method public updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;

    .prologue
    .line 406
    if-nez p2, :cond_0

    .line 430
    :goto_0
    return-void

    .line 408
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0051

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 410
    .local v0, "borderWidth":I
    :try_start_0
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    mul-int/lit8 v6, v0, 0x2

    add-int/2addr v5, v6

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    mul-int/lit8 v7, v0, 0x2

    add-int/2addr v6, v7

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 412
    .local v3, "frameBmp":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 413
    .local v1, "canvas":Landroid/graphics/Canvas;
    const/4 v5, -0x1

    invoke-virtual {v1, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 414
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_1

    .line 415
    int-to-float v5, v0

    int-to-float v6, v0

    const/4 v7, 0x0

    invoke-virtual {v1, p2, v5, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 417
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v4

    .line 418
    .local v4, "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    if-nez v4, :cond_2

    .line 419
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->mView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v6

    invoke-direct {v5, v6, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 425
    :goto_1
    invoke-virtual {p0, p1, v3}, Lcom/sec/samsung/gallery/glview/slideshoweffect/GlSlideshowPerspective;->setObjectAttrib(Lcom/sec/samsung/gallery/glview/GlBaseObject;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 427
    .end local v1    # "canvas":Landroid/graphics/Canvas;
    .end local v3    # "frameBmp":Landroid/graphics/Bitmap;
    .end local v4    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :catch_0
    move-exception v2

    .line 428
    .local v2, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v2}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_0

    .line 421
    .end local v2    # "e":Ljava/lang/OutOfMemoryError;
    .restart local v1    # "canvas":Landroid/graphics/Canvas;
    .restart local v3    # "frameBmp":Landroid/graphics/Bitmap;
    .restart local v4    # "glCanvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :cond_2
    :try_start_1
    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 422
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->requestFullUpload()V

    .line 423
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->invalidateContent()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
