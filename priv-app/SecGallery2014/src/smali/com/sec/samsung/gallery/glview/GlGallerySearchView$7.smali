.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;
.super Ljava/lang/Object;
.source "GlGallerySearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V
    .locals 0

    .prologue
    .line 483
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 485
    move-object v3, p1

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 487
    .local v1, "index":I
    const/4 v0, 0x0

    .line 488
    .local v0, "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v3, :cond_0

    .line 489
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .end local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .line 490
    .restart local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 491
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_0

    move v3, v4

    .line 499
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return v3

    .line 495
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    if-eqz v3, :cond_1

    .line 496
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-interface {v3, v5, v6, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 498
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$7;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    const/4 v5, 0x4

    invoke-virtual {v3, v5, v1, v4, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    .line 499
    const/4 v3, 0x1

    goto :goto_0
.end method
