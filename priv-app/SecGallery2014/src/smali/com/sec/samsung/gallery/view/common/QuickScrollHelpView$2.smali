.class Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;
.super Ljava/lang/Object;
.source "QuickScrollHelpView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->removeView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$002(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)Z

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;->this$0:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsDrawerEnabled:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->access$900(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    .line 131
    return-void
.end method
