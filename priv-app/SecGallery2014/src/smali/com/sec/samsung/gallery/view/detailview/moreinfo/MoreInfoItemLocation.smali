.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
.super Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
.source "MoreInfoItemLocation.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;
.implements Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "MoreInfoItemLocation"

.field private static final ZOOM_RATIO:F = 13.0f


# instance fields
.field private mAddress:Ljava/lang/String;

.field private mAddresslistener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

.field private mDeleteClickListener:Landroid/view/View$OnClickListener;

.field private mInerimLatLong:[D

.field private mInterimAddress:Ljava/lang/String;

.field private mLatLong:[D

.field private mMap:Lcom/google/android/gms/maps/GoogleMap;

.field private final mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

.field private mMapViewListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

.field private mMarkerClickListener:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "entry"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;
    .param p4, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 115
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;)V

    .line 78
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mDeleteClickListener:Landroid/view/View$OnClickListener;

    .line 89
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMarkerClickListener:Landroid/view/View$OnClickListener;

    .line 103
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapViewListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    .line 122
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$4;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddresslistener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "googlemap"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapViewListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->setMapViewListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;)V

    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->getAddress()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddress:Ljava/lang/String;

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)[D
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/GoogleMap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
    .param p1, "x1"    # Lcom/google/android/gms/maps/GoogleMap;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddress:Ljava/lang/String;

    return-object p1
.end method

.method private addMarker(DD)V
    .locals 5
    .param p1, "latitude"    # D
    .param p3, "longitude"    # D

    .prologue
    .line 147
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    .line 151
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v2, :cond_1

    .line 152
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 153
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    .line 154
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    new-instance v3, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v4, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    const v4, 0x7f020210

    invoke-static {v4}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    .line 156
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 157
    new-instance v2, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    new-instance v3, Lcom/google/android/gms/maps/model/LatLng;

    invoke-direct {v3, p1, p2, p3, p4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v2

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 159
    .local v0, "cameraPosition":Lcom/google/android/gms/maps/model/CameraPosition;
    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    .line 160
    .local v1, "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 162
    .end local v0    # "cameraPosition":Lcom/google/android/gms/maps/model/CameraPosition;
    .end local v1    # "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    :cond_1
    return-void
.end method

.method private getAddress()Ljava/lang/String;
    .locals 3

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 142
    :goto_0
    return-object v0

    .line 138
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [D

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatLong([D)V

    .line 140
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    invoke-virtual {v0}, [D->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddresslistener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->resolveAddress(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;[DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private updateGalleryLocalDB(I[D)V
    .locals 6
    .param p1, "id"    # I
    .param p2, "latlong"    # [D

    .prologue
    .line 363
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v3, :cond_0

    .line 364
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 365
    .local v2, "values":Landroid/content/ContentValues;
    const-string v1, "images_event_album"

    .line 366
    .local v1, "table":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 367
    .local v0, "selection":Ljava/lang/String;
    const-string v3, "latitude"

    const/4 v4, 0x0

    aget-wide v4, p2, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const-string v3, "longitude"

    const/4 v4, 0x1

    aget-wide v4, p2, v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v2, v0, v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 372
    .end local v0    # "selection":Ljava/lang/String;
    .end local v1    # "table":Ljava/lang/String;
    .end local v2    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void
.end method


# virtual methods
.method public actionEditCancel()V
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddress:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    .line 318
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    invoke-virtual {v0}, [D->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [D

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    .line 319
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->invalidate()V

    .line 320
    return-void
.end method

.method public actionEditDone()V
    .locals 15

    .prologue
    .line 248
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v12, 0x0

    aget-wide v12, v9, v12

    cmpl-double v9, v10, v12

    if-nez v9, :cond_1

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    const/4 v10, 0x1

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v12, 0x1

    aget-wide v12, v9, v12

    cmpl-double v9, v10, v12

    if-nez v9, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddress:Ljava/lang/String;

    .line 254
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    invoke-virtual {v9}, [D->clone()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [D

    iput-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    .line 256
    const/4 v5, 0x0

    .line 257
    .local v5, "qdioJpegData":Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;
    const/4 v7, 0x0

    .line 259
    .local v7, "soundData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v10, 0x10

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 260
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/sec/android/secvision/sef/SEF;->checkAudioInJPEG(Ljava/lang/String;)Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;

    move-result-object v5

    .line 261
    if-eqz v5, :cond_3

    .line 262
    new-instance v7, Ljava/util/ArrayList;

    .end local v7    # "soundData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 263
    .restart local v7    # "soundData":Ljava/util/ArrayList;, "Ljava/util/ArrayList<[B>;"
    invoke-virtual {v5}, Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;->getAudioListSize()I

    move-result v0

    .line 264
    .local v0, "audioListSize":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v0, :cond_3

    .line 266
    :try_start_0
    invoke-static {v5, v4}, Lcom/sec/android/secvision/sef/SEF;->getAudioStreamBuffer(Lcom/sec/android/secvision/sef/SEF$QdioJPEGData;I)[B

    move-result-object v6

    .line 267
    .local v6, "sb":[B
    if-eqz v6, :cond_2

    .line 268
    array-length v9, v6

    invoke-static {v6, v9}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v9

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 264
    .end local v6    # "sb":[B
    :cond_2
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 270
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 277
    .end local v0    # "audioListSize":I
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "i":I
    :cond_3
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 278
    .local v8, "values":Landroid/content/ContentValues;
    const-string v9, "latitude"

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 279
    const-string v9, "longitude"

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x1

    aget-wide v10, v10, v11

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 280
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const-string v11, "_id =?"

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v9, v10, v8, v11, v12}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 285
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/data/MediaItem;->setLatLong([D)V

    .line 287
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    invoke-direct {p0, v9, v10}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->updateGalleryLocalDB(I[D)V

    .line 289
    :try_start_1
    new-instance v3, Lcom/sec/android/gallery3d/exif/ExifInterface;

    invoke-direct {v3}, Lcom/sec/android/gallery3d/exif/ExifInterface;-><init>()V

    .line 290
    .local v3, "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v10, 0x0

    aget-wide v10, v9, v10

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v12, 0x1

    aget-wide v12, v9, v12

    invoke-virtual {v3, v10, v11, v12, v13}, Lcom/sec/android/gallery3d/exif/ExifInterface;->addGpsTags(DD)Z

    .line 291
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v9}, Lcom/sec/android/gallery3d/exif/ExifInterface;->forceRewriteExif(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    if-eqz v7, :cond_0

    .line 307
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_3
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_4

    .line 308
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    invoke-static {v10, v9}, Lcom/sec/android/secvision/sef/SEF;->saveAudioJPEG(Ljava/lang/String;[B)I

    .line 307
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 310
    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .line 292
    .end local v3    # "exif":Lcom/sec/android/gallery3d/exif/ExifInterface;
    .end local v4    # "i":I
    :catch_1
    move-exception v1

    .line 293
    .local v1, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v9, "MoreInfoItemLocation"

    const-string/jumbo v10, "write location info to exif - retry"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 296
    :try_start_3
    new-instance v3, Landroid/media/ExifInterface;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v9}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 297
    .local v3, "exif":Landroid/media/ExifInterface;
    const-string v9, "GPSLatitude"

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    const-string v9, "GPSLongitude"

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x1

    aget-wide v10, v10, v11

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/media/ExifInterface;->setAttribute(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    invoke-virtual {v3}, Landroid/media/ExifInterface;->saveAttributes()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 306
    .end local v3    # "exif":Landroid/media/ExifInterface;
    :goto_4
    if-eqz v7, :cond_0

    .line 307
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_5

    .line 308
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    invoke-static {v10, v9}, Lcom/sec/android/secvision/sef/SEF;->saveAudioJPEG(Ljava/lang/String;[B)I

    .line 307
    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    .line 302
    .end local v4    # "i":I
    :catch_2
    move-exception v2

    .line 303
    .local v2, "e1":Ljava/io/IOException;
    :try_start_4
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 306
    .end local v1    # "e":Ljava/lang/Exception;
    .end local v2    # "e1":Ljava/io/IOException;
    :catchall_0
    move-exception v9

    move-object v10, v9

    if-eqz v7, :cond_7

    .line 307
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v4, v9, :cond_6

    .line 308
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, [B

    invoke-static {v11, v9}, Lcom/sec/android/secvision/sef/SEF;->saveAudioJPEG(Ljava/lang/String;[B)I

    .line 307
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 310
    .restart local v1    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    goto/16 :goto_0

    .end local v1    # "e":Ljava/lang/Exception;
    :cond_6
    invoke-virtual {v7}, Ljava/util/ArrayList;->clear()V

    .end local v4    # "i":I
    :cond_7
    throw v10
.end method

.method public dataEdited()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 241
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    aget-wide v2, v2, v0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    aget-wide v4, v4, v0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    aget-wide v2, v2, v1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    aget-wide v4, v4, v1

    cmpl-double v2, v2, v4

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 14

    .prologue
    .line 166
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mView:Landroid/view/View;

    const v11, 0x7f0f019f

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 167
    .local v0, "content":Landroid/widget/TextView;
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-eqz v10, :cond_2

    const/16 v10, 0x8

    :goto_0
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    const/16 v6, 0x8

    .line 169
    .local v6, "mapVisible":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v13, 0x1

    aget-wide v12, v12, v13

    invoke-static {v10, v11, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isValidLocation(DD)Z

    move-result v9

    .line 170
    .local v9, "validLocation":Z
    if-eqz v9, :cond_3

    .line 171
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_0

    .line 172
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 174
    :cond_0
    const/4 v6, 0x0

    .line 175
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v11, 0x0

    aget-wide v10, v10, v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v13, 0x1

    aget-wide v12, v12, v13

    invoke-direct {p0, v10, v11, v12, v13}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->addMarker(DD)V

    .line 180
    :goto_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mView:Landroid/view/View;

    const v11, 0x7f0f01a0

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 181
    .local v5, "map":Landroid/view/View;
    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 184
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mView:Landroid/view/View;

    const v11, 0x7f0f0192

    invoke-virtual {v10, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout;

    .line 185
    .local v3, "editView":Landroid/widget/RelativeLayout;
    const v10, 0x7f0f0187

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 186
    .local v2, "deleteSet":Landroid/widget/LinearLayout;
    const v10, 0x7f0f0188

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 187
    .local v4, "locationTextView":Landroid/widget/TextView;
    if-eqz v9, :cond_4

    .line 188
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    if-eqz v10, :cond_1

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    invoke-virtual {v10}, Ljava/lang/String;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_1

    .line 189
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 194
    :cond_1
    :goto_2
    const v10, 0x7f0f0189

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 195
    .local v1, "deleteIcon":Landroid/widget/ImageView;
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mDeleteClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 198
    const v10, 0x7f0f0194

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 201
    .local v8, "noLocationTextView":Landroid/widget/TextView;
    const v10, 0x7f0f0193

    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageButton;

    .line 202
    .local v7, "marker":Landroid/widget/ImageButton;
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isJpeg(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 203
    const/4 v10, 0x0

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 204
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMarkerClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    const/4 v10, 0x0

    const/16 v11, 0x9

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v8, v10, v11, v12, v13}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 210
    :goto_3
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-eqz v10, :cond_6

    const/4 v10, 0x0

    :goto_4
    invoke-virtual {v3, v10}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 211
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-eqz v10, :cond_7

    if-eqz v9, :cond_7

    .line 212
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 213
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 222
    :goto_5
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->getView()Landroid/view/View;

    .line 223
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mView:Landroid/view/View;

    return-object v10

    .line 167
    .end local v1    # "deleteIcon":Landroid/widget/ImageView;
    .end local v2    # "deleteSet":Landroid/widget/LinearLayout;
    .end local v3    # "editView":Landroid/widget/RelativeLayout;
    .end local v4    # "locationTextView":Landroid/widget/TextView;
    .end local v5    # "map":Landroid/view/View;
    .end local v6    # "mapVisible":I
    .end local v7    # "marker":Landroid/widget/ImageButton;
    .end local v8    # "noLocationTextView":Landroid/widget/TextView;
    .end local v9    # "validLocation":Z
    :cond_2
    const/4 v10, 0x0

    goto/16 :goto_0

    .line 177
    .restart local v6    # "mapVisible":I
    .restart local v9    # "validLocation":Z
    :cond_3
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e0069

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 178
    const/16 v6, 0x8

    goto/16 :goto_1

    .line 192
    .restart local v2    # "deleteSet":Landroid/widget/LinearLayout;
    .restart local v3    # "editView":Landroid/widget/RelativeLayout;
    .restart local v4    # "locationTextView":Landroid/widget/TextView;
    .restart local v5    # "map":Landroid/view/View;
    :cond_4
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0e0069

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 207
    .restart local v1    # "deleteIcon":Landroid/widget/ImageView;
    .restart local v7    # "marker":Landroid/widget/ImageButton;
    .restart local v8    # "noLocationTextView":Landroid/widget/TextView;
    :cond_5
    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    .line 210
    :cond_6
    const/16 v10, 0x8

    goto :goto_4

    .line 214
    :cond_7
    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-eqz v10, :cond_8

    if-nez v9, :cond_8

    .line 215
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 216
    const/4 v10, 0x0

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    .line 218
    :cond_8
    const/16 v10, 0x8

    invoke-virtual {v2, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 219
    const/16 v10, 0x8

    invoke-virtual {v8, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->getView()Landroid/view/View;

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 230
    return-void
.end method

.method public onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 4
    .param p1, "arg0"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    const/4 v3, 0x0

    .line 336
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-nez v1, :cond_0

    .line 337
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mRootView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 338
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 339
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 340
    const-string v1, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const-string v1, "KEY_MEDIA_ITEM_LOCATION"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 342
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 345
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    return-void
.end method

.method public onMarkerClick(Lcom/google/android/gms/maps/model/Marker;)Z
    .locals 4
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    const/4 v3, 0x0

    .line 349
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditMode:Z

    if-nez v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mRootView:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 351
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 352
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    const-string v1, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    const-string v1, "KEY_MEDIA_ITEM_LOCATION"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mLatLong:[D

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 355
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 357
    const/4 v1, 0x1

    .line 359
    .end local v0    # "bundle":Landroid/os/Bundle;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setLatLng(Ljava/lang/Double;Ljava/lang/Double;)V
    .locals 5
    .param p1, "lat"    # Ljava/lang/Double;
    .param p2, "log"    # Ljava/lang/Double;

    .prologue
    const/4 v4, 0x1

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    aput-wide v2, v0, v1

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    aput-wide v2, v0, v4

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mAddresslistener:Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->resolveAddress(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;[DLcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInterimAddress:Ljava/lang/String;

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-interface {v0, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;->onEdit(Z)V

    .line 330
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->invalidate()V

    .line 331
    return-void
.end method

.method public setMode(Z)V
    .locals 0
    .param p1, "editMode"    # Z

    .prologue
    .line 235
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setMode(Z)V

    .line 236
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->invalidate()V

    .line 237
    return-void
.end method
