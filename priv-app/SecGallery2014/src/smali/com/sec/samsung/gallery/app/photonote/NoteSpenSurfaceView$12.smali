.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$12;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenControlListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 666
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$12;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClosed(Ljava/util/ArrayList;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 669
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z
    .locals 1
    .param p5, "arg4"    # I
    .param p6, "arg5"    # Landroid/graphics/PointF;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/graphics/Rect;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;I",
            "Landroid/graphics/PointF;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 676
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    .local p2, "arg1":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/graphics/Rect;>;"
    .local p3, "arg2":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;>;"
    .local p4, "arg3":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onMenuSelected(Ljava/util/ArrayList;I)Z
    .locals 1
    .param p2, "arg1"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;I)Z"
        }
    .end annotation

    .prologue
    .line 681
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onObjectChanged(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/document/SpenObjectBase;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 686
    .local p1, "arg0":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/pen/document/SpenObjectBase;>;"
    return-void
.end method

.method public onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "arg0"    # Landroid/graphics/RectF;
    .param p2, "arg1"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 690
    return-void
.end method

.method public onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0
    .param p1, "arg0"    # F
    .param p2, "arg1"    # Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    .prologue
    .line 694
    return-void
.end method
