.class Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
.super Lcom/sec/samsung/gallery/glview/GlThumbObject;
.source "GlReorderHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlReorderHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "GlReorderObject"
.end annotation


# static fields
.field static final INVISIBLE_INDEX:I = -0x3e8


# instance fields
.field mDisplayIndex:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V
    .locals 6
    .param p2, "baseLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "textureWidth"    # I
    .param p6, "textureHeight"    # I

    .prologue
    .line 622
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-object v0, p0

    move-object v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 623
    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V

    .line 620
    const/16 v0, -0x3e8

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 624
    return-void
.end method


# virtual methods
.method public setIndex(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 628
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    .line 629
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->this$0:Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    # getter for: Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->access$400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Landroid/util/SparseIntArray;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    invoke-virtual {v1, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 630
    .local v0, "globalOffsetMap":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 631
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 635
    :goto_0
    return-void

    .line 633
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    goto :goto_0
.end method
