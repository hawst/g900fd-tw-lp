.class public Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.super Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
.source "NoteSpenSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;,
        Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ToolBarActionListener;,
        Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    }
.end annotation


# static fields
.field private static final APPLICATION_ID_NOTE:Ljava/lang/String; = "GalleryMemo"

.field private static final APPLICATION_ID_VERSION_MAJOR:I = 0x1

.field private static final APPLICATION_ID_VERSION_MINOR:I = 0x0

.field private static final APPLICATION_ID_VERSION_PATCHNAME:Ljava/lang/String; = "Debug"

.field private static final MSG_HIDE_FAKEVIEW:I = 0x0

.field public static SETTING_GONE:I = 0x0

.field public static SETTING_VISIABLE:I = 0x0

.field private static final SUPPORT_ONLY_SPEN_SDK_30:I = 0x2

.field private static final SUPPORT_SPEN_SDK_30_N_DEVICE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "NoteSpenSurfaceView"

.field public static TYPE_SETTING_PEN:I

.field public static TYPE_SETTING_REMOVER:I

.field private static final UNSUPPORT_SPEN_SDK_30:I

.field private static mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;


# instance fields
.field private ID_BRUSH_MAP:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;",
            ">;"
        }
    .end annotation
.end field

.field protected canvas_height_margin:I

.field protected colorpicker_offset_min_width:I

.field private final mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

.field protected mCanvasContainer:Landroid/widget/RelativeLayout;

.field private mCanvasHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mCanvasTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field protected mCanvasViewSize:Landroid/graphics/Rect;

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field protected final mContext:Landroid/content/Context;

.field private mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

.field protected mFilePath:Ljava/lang/String;

.field private mHistoryUpdateListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

.field private mHoverButtonAction:I

.field protected mImageRect:Landroid/graphics/Point;

.field private mInitPenSettingPosition:Landroid/graphics/PointF;

.field private mInitRemoverSettingPosition:Landroid/graphics/PointF;

.field private mIsInMultiWindow:Z

.field private mMainHandler:Landroid/os/Handler;

.field private mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field protected mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

.field protected mNoteType:I

.field protected mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mPenSettingActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

.field protected mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

.field private mPenSettingMoved:Z

.field private mRemoverActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

.field private mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

.field private mRemoverListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

.field protected mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

.field private mRemoverSettingMoved:Z

.field protected mScreenSize:Landroid/graphics/Point;

.field protected mSourceRect:Landroid/graphics/Rect;

.field protected mSpenDocInitialized:Z

.field private mSpenSettingPenInfoList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;",
            ">;"
        }
    .end annotation
.end field

.field protected mUri:Landroid/net/Uri;

.field protected final mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    sput v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_PEN:I

    .line 70
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_REMOVER:I

    .line 75
    sput v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->SETTING_VISIABLE:I

    .line 76
    const/16 v0, 0x8

    sput v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->SETTING_GONE:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;
    .param p3, "listener"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    .prologue
    .line 172
    invoke-direct {p0, p1, p3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V

    .line 173
    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initData(Landroid/content/Intent;)V

    .line 174
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initView()V

    .line 175
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    .prologue
    const/4 v1, 0x0

    .line 153
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;-><init>(Landroid/content/Context;)V

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->ID_BRUSH_MAP:Ljava/util/HashMap;

    .line 97
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mScreenSize:Landroid/graphics/Point;

    .line 98
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mImageRect:Landroid/graphics/Point;

    .line 99
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingMoved:Z

    .line 100
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z

    .line 112
    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mHoverButtonAction:I

    .line 118
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenDocInitialized:Z

    .line 474
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$3;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    .line 508
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$4;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    .line 525
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$5;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    .line 578
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$6;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mHistoryUpdateListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    .line 598
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$7;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    .line 610
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    .line 624
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$9;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    .line 638
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$10;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    .line 654
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$11;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    .line 666
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$12;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$12;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    .line 154
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    .line 155
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    .line 156
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-direct {v0, p1, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    .line 157
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$1;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMainHandler:Landroid/os/Handler;

    .line 169
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mIsInMultiWindow:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitPenSettingPosition:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mIsInMultiWindow:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Landroid/graphics/PointF;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitRemoverSettingPosition:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenSettingPenInfoList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->ID_BRUSH_MAP:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingMoved:Z

    return v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingMoved:Z

    return p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z

    return v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .param p1, "x1"    # Z

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingMoved:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mHoverButtonAction:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->toggleVisibility(Landroid/view/View;)V

    return-void
.end method

.method public static getAppId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    const-string v0, "GalleryMemo"

    return-object v0
.end method

.method private getBrush(Ljava/lang/String;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    .locals 1
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 698
    const-string v0, "InkPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 699
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PEN:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    .line 711
    :goto_0
    return-object v0

    .line 700
    :cond_0
    const-string v0, "ChineseBrush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->CHINESE_BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0

    .line 702
    :cond_1
    const-string v0, "Brush"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 703
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0

    .line 704
    :cond_2
    const-string v0, "Pencil"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 705
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PENCIL:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0

    .line 706
    :cond_3
    const-string v0, "Marker"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 707
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKER:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0

    .line 708
    :cond_4
    const-string v0, "MagicPen"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 709
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKERALPHA:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0

    .line 711
    :cond_5
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PEN:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    goto :goto_0
.end method

.method private initData(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 184
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mUri:Landroid/net/Uri;

    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mUri:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getFilePathFrom(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mFilePath:Ljava/lang/String;

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mFilePath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/SPenSDKUtils;->getImageSize(Ljava/lang/String;)Landroid/graphics/Rect;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSourceRect:Landroid/graphics/Rect;

    .line 187
    return-void
.end method

.method private initListener()V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mHistoryUpdateListener:Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;)V

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setRemoverListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;)V

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$ActionListener;)V

    .line 304
    return-void
.end method

.method private initSettingInfo()V
    .locals 14

    .prologue
    .line 388
    new-instance v7, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-direct {v7, v11}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    .line 389
    .local v7, "penManager":Lcom/samsung/android/sdk/pen/pen/SpenPenManager;
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v11

    iput-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    .line 390
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    if-eqz v11, :cond_0

    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-lez v11, :cond_0

    .line 391
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 392
    .local v6, "penInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->ID_BRUSH_MAP:Ljava/util/HashMap;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "com.samsung.android.sdk.pen.pen.preload."

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v6, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v13, v6, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v13}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getBrush(Ljava/lang/String;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 394
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v6    # "penInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    :cond_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    const-string v12, "SettingInfo"

    const/4 v13, 0x0

    invoke-virtual {v11, v12, v13}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 395
    .local v8, "pref":Landroid/content/SharedPreferences;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    iput-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenSettingPenInfoList:Ljava/util/ArrayList;

    .line 397
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v9

    .line 398
    .local v9, "removerInfo":Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    if-eqz v9, :cond_1

    .line 399
    const-string v11, "mLastRemoverSize"

    const/high16 v12, 0x41f00000    # 30.0f

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v11

    iput v11, v9, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    .line 400
    const/4 v11, 0x1

    iput v11, v9, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    .line 401
    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 402
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v11, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    .line 404
    :cond_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    if-eqz v11, :cond_4

    .line 405
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v11

    if-ge v1, v11, :cond_3

    .line 406
    new-instance v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    .line 407
    .local v3, "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenName_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    invoke-interface {v8, v12, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 408
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenSize_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/high16 v12, 0x41f00000    # 30.0f

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v11

    iput v11, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 409
    const/4 v11, 0x4

    if-ne v1, v11, :cond_2

    .line 410
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenColor_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/high16 v12, 0x7f000000

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    iput v11, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 415
    :goto_2
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenSettingPenInfoList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 412
    :cond_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenColor_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/high16 v12, -0x1000000

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    iput v11, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    goto :goto_2

    .line 418
    .end local v3    # "info":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_3
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v12, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenSettingPenInfoList:Ljava/util/ArrayList;

    invoke-virtual {v11, v12}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setPenInfoList(Ljava/util/List;)V

    .line 421
    const-string v11, "mLastPenIndex"

    const/4 v12, 0x0

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 422
    .local v4, "mLastPenIndex":I
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenInfoList:Ljava/util/List;

    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    .line 423
    .local v5, "mLastpenInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v10

    .line 425
    .local v10, "settingSpenPenInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    if-eqz v10, :cond_4

    .line 426
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenName_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    iget-object v12, v5, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    .line 427
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenColor_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/high16 v12, -0x1000000

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    iput v11, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    .line 428
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "mLastPenSize_"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/high16 v12, 0x41200000    # 10.0f

    invoke-interface {v8, v11, v12}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v11

    iput v11, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    .line 430
    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 431
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v11, v10}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    .line 434
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    iget v12, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v11, v12}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updatePenColorView(I)V

    .line 436
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->ID_BRUSH_MAP:Ljava/util/HashMap;

    iget-object v12, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    .line 437
    .local v0, "brush":Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    if-eqz v0, :cond_5

    .line 438
    iget-object v11, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v11, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateBrushIcon(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;)V

    .line 444
    .end local v0    # "brush":Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    .end local v1    # "i":I
    .end local v4    # "mLastPenIndex":I
    .end local v5    # "mLastpenInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    .end local v10    # "settingSpenPenInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_4
    :goto_3
    return-void

    .line 440
    .restart local v0    # "brush":Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    .restart local v1    # "i":I
    .restart local v4    # "mLastPenIndex":I
    .restart local v5    # "mLastpenInfo":Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;
    .restart local v10    # "settingSpenPenInfo":Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    :cond_5
    const-string v11, "NoteSpenSurfaceView"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Stroke Style ("

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    iget-object v13, v10, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ") not supported"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method

.method private initSettingView()V
    .locals 7

    .prologue
    const/high16 v6, 0x3fc00000    # 1.5f

    .line 255
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0f01f6

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/FrameLayout;

    .line 258
    .local v1, "settingViewParent":Landroid/widget/FrameLayout;
    const/4 v0, 0x0

    .line 259
    .local v0, "settingViewContainer":Landroid/widget/RelativeLayout;
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const v3, 0x7f0f0152

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "settingViewContainer":Landroid/widget/RelativeLayout;
    check-cast v0, Landroid/widget/RelativeLayout;

    .line 262
    .restart local v0    # "settingViewContainer":Landroid/widget/RelativeLayout;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 263
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/PenSetting.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 269
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->getPenPosition()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setIndicatorPosition(I)V

    .line 270
    new-instance v2, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitPenSettingPosition:Landroid/graphics/PointF;

    .line 271
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 272
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 274
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/RemoverSetting.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 281
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->getRemoverPosition()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setIndicatorPosition(I)V

    .line 282
    new-instance v2, Landroid/graphics/PointF;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getX()F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mInitRemoverSettingPosition:Landroid/graphics/PointF;

    .line 284
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    .line 285
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 286
    const/high16 v2, -0x1000000

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setBlankColor(I)V

    .line 287
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTipEnabled(Z)V

    .line 288
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initSettingInfo()V

    .line 289
    return-void

    .line 266
    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/PenSetting.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    goto/16 :goto_0

    .line 278
    :cond_1
    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/RemoverSetting.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    goto :goto_1
.end method

.method private initView()V
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initResource()V

    .line 192
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setContainer()V

    .line 193
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setSCanvasViewLayout()V

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 196
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initializeSPenModel()V

    .line 197
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initSettingView()V

    .line 199
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initListener()V

    .line 200
    return-void
.end method

.method public static declared-synchronized initializeSPenSDK(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 123
    const-class v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    monitor-enter v2

    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    if-nez v3, :cond_0

    .line 124
    new-instance v3, Lcom/samsung/android/sdk/pen/Spen;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/Spen;-><init>()V

    sput-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    .line 125
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    invoke-virtual {v3, p0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;)V

    .line 128
    :cond_0
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    if-eqz v3, :cond_1

    .line 129
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/Spen;->isFeatureEnabled(I)Z
    :try_end_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    .line 130
    const/4 v1, 0x1

    .line 140
    :cond_1
    :goto_0
    monitor-exit v2

    return v1

    .line 132
    :cond_2
    const/4 v1, 0x2

    goto :goto_0

    .line 137
    :catch_0
    move-exception v0

    .line 138
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    const/4 v3, 0x0

    :try_start_1
    sput-object v3, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSPenSdk:Lcom/samsung/android/sdk/pen/Spen;

    .line 139
    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 123
    .end local v0    # "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method private setContainer()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0f0150

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    .line 204
    return-void
.end method

.method private toggleVisibility(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 715
    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 716
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 720
    :goto_0
    return-void

    .line 718
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateAppId()V
    .locals 4

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    const-string v1, "GalleryMemo"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->setAppName(Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-string v3, "Debug"

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->setAppVersion(IILjava/lang/String;)V

    .line 310
    return-void
.end method


# virtual methods
.method public clearDataInSpenPageDoc(Z)V
    .locals 1
    .param p1, "updatePageDoc"    # Z

    .prologue
    .line 855
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 858
    if-eqz p1, :cond_0

    .line 859
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->update()V

    .line 861
    :cond_0
    return-void
.end method

.method public destroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v1, :cond_0

    .line 349
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->close()V

    .line 350
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    if-eqz v1, :cond_1

    .line 351
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->close()V

    .line 352
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_2

    .line 353
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    .line 354
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    .line 356
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 361
    :cond_3
    :goto_0
    return-void

    .line 357
    :catch_0
    move-exception v0

    .line 358
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 359
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_3

    iput-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    goto :goto_0
.end method

.method public finish_note(Z)V
    .locals 1
    .param p1, "dataChanged"    # Z

    .prologue
    .line 891
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    if-eqz v0, :cond_0

    .line 892
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;->finishNote(Z)V

    .line 894
    :cond_0
    return-void
.end method

.method public getCapturePageBitmp()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 868
    const/4 v2, 0x0

    .line 871
    .local v2, "mCapturePage":Landroid/graphics/Bitmap;
    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_0
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->capturePage(F)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    move-object v3, v2

    .line 887
    :goto_0
    return-object v3

    .line 873
    :catch_0
    move-exception v0

    .line 874
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 887
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :goto_1
    const/4 v3, 0x0

    goto :goto_0

    .line 875
    :catch_1
    move-exception v0

    .line 876
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    .line 877
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_2
    move-exception v0

    .line 878
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    .line 879
    invoke-static {}, Ljava/lang/System;->gc()V

    .line 881
    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_1
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->capturePage(F)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v2

    move-object v3, v2

    .line 882
    goto :goto_0

    .line 883
    :catch_3
    move-exception v1

    .line 884
    .local v1, "e2":Ljava/lang/OutOfMemoryError;
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V

    goto :goto_1
.end method

.method protected getMaximumCanvasRect(Landroid/graphics/Rect;III)Landroid/graphics/Rect;
    .locals 6
    .param p1, "rectImage"    # Landroid/graphics/Rect;
    .param p2, "nMarginWidth"    # I
    .param p3, "nMarginHeight"    # I
    .param p4, "maxWidth"    # I

    .prologue
    .line 365
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getMaximumCanvasRect(Landroid/graphics/Rect;IIII)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method protected getMaximumCanvasRect(Landroid/graphics/Rect;IIII)Landroid/graphics/Rect;
    .locals 14
    .param p1, "rectImage"    # Landroid/graphics/Rect;
    .param p2, "nMarginWidth"    # I
    .param p3, "nMarginHeight"    # I
    .param p4, "maxWidth"    # I
    .param p5, "maxHeight"    # I

    .prologue
    .line 370
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 371
    .local v0, "displayMetrics":Landroid/util/DisplayMetrics;
    iget-object v9, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    const-string/jumbo v10, "window"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/WindowManager;

    .line 372
    .local v8, "wm":Landroid/view/WindowManager;
    invoke-interface {v8}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 373
    if-lez p4, :cond_0

    move/from16 v7, p4

    .line 374
    .local v7, "nScreenWidth":I
    :goto_0
    if-lez p5, :cond_1

    move/from16 v6, p5

    .line 376
    .local v6, "nScreenHeight":I
    :goto_1
    iget v9, p1, Landroid/graphics/Rect;->right:I

    iget v10, p1, Landroid/graphics/Rect;->left:I

    sub-int v3, v9, v10

    .line 377
    .local v3, "nImageWidth":I
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    iget v10, p1, Landroid/graphics/Rect;->top:I

    sub-int v2, v9, v10

    .line 380
    .local v2, "nImageHeight":I
    int-to-float v9, v7

    int-to-float v10, v3

    div-float v1, v9, v10

    .line 382
    .local v1, "fResizeRatio":F
    int-to-float v9, v3

    mul-float/2addr v9, v1

    float-to-int v5, v9

    .line 383
    .local v5, "nResizeWidth":I
    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-int v9, v5, v2

    int-to-float v9, v9

    int-to-float v12, v3

    div-float/2addr v9, v12

    float-to-double v12, v9

    add-double/2addr v10, v12

    double-to-int v4, v10

    .line 384
    .local v4, "nResizeHeight":I
    new-instance v9, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v9, v10, v11, v5, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v9

    .line 373
    .end local v1    # "fResizeRatio":F
    .end local v2    # "nImageHeight":I
    .end local v3    # "nImageWidth":I
    .end local v4    # "nResizeHeight":I
    .end local v5    # "nResizeWidth":I
    .end local v6    # "nScreenHeight":I
    .end local v7    # "nScreenWidth":I
    :cond_0
    iget v9, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    sub-int v7, v9, p2

    goto :goto_0

    .line 374
    .restart local v7    # "nScreenWidth":I
    :cond_1
    iget v9, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v6, v9, p3

    goto :goto_1
.end method

.method public getNoteType()I
    .locals 1

    .prologue
    .line 814
    const/4 v0, 0x0

    return v0
.end method

.method public getSpenNoteDoc()Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .locals 1

    .prologue
    .line 864
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    return-object v0
.end method

.method public getSpenPageDoc()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    .prologue
    .line 844
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method public hasDataInSpenPageDoc()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 848
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectCount(Z)I

    move-result v1

    if-lez v1, :cond_0

    .line 849
    const/4 v0, 0x1

    .line 851
    :cond_0
    return v0
.end method

.method protected initResource()V
    .locals 2

    .prologue
    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 179
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d0135

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->canvas_height_margin:I

    .line 180
    const v1, 0x7f0d0149

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->colorpicker_offset_min_width:I

    .line 181
    return-void
.end method

.method protected initializeSPenModel()V
    .locals 0

    .prologue
    .line 218
    return-void
.end method

.method public isColorPickerMode()Z
    .locals 3

    .prologue
    const/4 v2, 0x5

    const/4 v0, 0x1

    .line 748
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 752
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMovingMode()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 740
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 744
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPenMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x2

    .line 723
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 727
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRedoable()Z
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 826
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isRedoable()Z

    move-result v0

    .line 828
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRemoverMode()Z
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v0, 0x1

    .line 732
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getToolTypeAction(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    .line 736
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSpenDocInitialized()Z
    .locals 1

    .prologue
    .line 903
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenDocInitialized:Z

    return v0
.end method

.method public isUndoable()Z
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 819
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isUndoable()Z

    move-result v0

    .line 821
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public mwLayoutListener(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 319
    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 320
    .local v0, "deco":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    .line 321
    .local v1, "vto2":Landroid/view/ViewTreeObserver;
    new-instance v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;-><init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V

    invoke-virtual {v1, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 344
    return-void
.end method

.method public onSave()V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    if-eqz v0, :cond_0

    .line 898
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mViewActionListener:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$ViewActionListener;->onSave()V

    .line 900
    :cond_0
    return-void
.end method

.method protected setBackgroundToNoteDoc()V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public setColorPickerMode()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 777
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 778
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 779
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 780
    return-void
.end method

.method public setMovingMode()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 770
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 771
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 772
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 773
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 774
    return-void
.end method

.method public setMultiWindow(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V
    .locals 1
    .param p1, "multiWindow"    # Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .prologue
    .line 313
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 314
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mwLayoutListener(Landroid/app/Activity;)V

    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setMultiWindow(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;)V

    .line 316
    return-void
.end method

.method public setPenMode()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x2

    .line 756
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 757
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 758
    invoke-virtual {p0, v2, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 759
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 760
    return-void
.end method

.method public setPenSettingVisibility(I)V
    .locals 1
    .param p1, "isVisible"    # I

    .prologue
    .line 783
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    .line 784
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    .line 786
    :cond_0
    return-void
.end method

.method public setRemoverMode()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 763
    const/4 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 764
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 765
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 766
    const/4 v0, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setToolTypeAction(II)V

    .line 767
    return-void
.end method

.method public setRemoverSettingVisibility(I)V
    .locals 1
    .param p1, "isVisible"    # I

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    if-eqz v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 792
    :cond_0
    return-void
.end method

.method public setSCanvasViewLayout()V
    .locals 0

    .prologue
    .line 207
    return-void
.end method

.method public toggleSettingViewVisibility(I)V
    .locals 3
    .param p1, "settingViewType"    # I

    .prologue
    const/4 v2, 0x0

    .line 795
    const/4 v0, 0x0

    .line 796
    .local v0, "view":Landroid/view/View;
    sget v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_PEN:I

    if-ne p1, v1, :cond_2

    .line 797
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    .line 798
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPenSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setViewMode(I)V

    .line 804
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 805
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    .line 806
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 811
    :cond_1
    :goto_1
    return-void

    .line 799
    :cond_2
    sget v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_REMOVER:I

    if-ne p1, v1, :cond_0

    .line 800
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    .line 801
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setViewMode(I)V

    goto :goto_0

    .line 808
    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public updateAnimation()V
    .locals 0

    .prologue
    .line 252
    return-void
.end method

.method public updateColorPickerOffset()V
    .locals 8

    .prologue
    const v7, 0x7f0f013d

    const/4 v6, 0x0

    .line 447
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSourceRect:Landroid/graphics/Rect;

    if-nez v5, :cond_1

    .line 472
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 450
    .local v1, "openToolBar":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 453
    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4, v6, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 454
    .local v4, "toolbarCorrection":Landroid/graphics/Point;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v6, v6}, Landroid/graphics/Point;-><init>(II)V

    .line 456
    .local v2, "sCanvasOffset":Landroid/graphics/Point;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    .line 457
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->findViewById(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->y:I

    .line 462
    :goto_1
    const/4 v5, 0x2

    new-array v0, v5, [I

    fill-array-data v0, :array_0

    .line 465
    .local v0, "location":[I
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getLocationInWindow([I)V

    .line 466
    aget v5, v0, v6

    iput v5, v2, Landroid/graphics/Point;->x:I

    .line 467
    const/4 v5, 0x1

    aget v5, v0, v5

    iput v5, v2, Landroid/graphics/Point;->y:I

    .line 469
    iget-object v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v3

    .line 470
    .local v3, "screenSize":Landroid/graphics/Point;
    iget v5, v3, Landroid/graphics/Point;->x:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getWidth()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Point;->x:I

    .line 471
    iget v5, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->getHeight()I

    move-result v6

    sub-int/2addr v5, v6

    iput v5, v2, Landroid/graphics/Point;->y:I

    goto :goto_0

    .line 459
    .end local v0    # "location":[I
    .end local v3    # "screenSize":Landroid/graphics/Point;
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->colorpicker_offset_min_width:I

    iput v5, v4, Landroid/graphics/Point;->x:I

    goto :goto_1

    .line 462
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected updateNoteDoc()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-nez v0, :cond_0

    .line 223
    const-string v0, "NoteSpenSurfaceView"

    const-string v1, "mNoteDoc is null. Fail to initialize."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    const v1, 0x7f0e0033

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 225
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->finish_note(Z)V

    .line 246
    :goto_0
    return-void

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPageCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    .line 234
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateAppId()V

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setBackgroundImageMode(I)V

    .line 236
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setBackgroundToNoteDoc()V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistory()V

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mCanvasContainer:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x32

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {p0, v0, v5}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    .line 242
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setScrollBarEnabled(Z)V

    .line 243
    invoke-static {}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->deleteBGimage()V

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/view/Window;->clearFlags(I)V

    .line 245
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mSpenDocInitialized:Z

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mNoteDoc:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_1
.end method

.method public updateRedo()V
    .locals 1

    .prologue
    .line 838
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 839
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->redo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 841
    :cond_0
    return-void
.end method

.method public updateSCanvasViewLayout()V
    .locals 0

    .prologue
    .line 210
    return-void
.end method

.method public updateUndo()V
    .locals 1

    .prologue
    .line 832
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undo()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    .line 835
    :cond_0
    return-void
.end method

.method public writeData(Ljava/lang/String;Z)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "clearNoteDocData"    # Z

    .prologue
    .line 214
    const/4 v0, 0x0

    return v0
.end method
