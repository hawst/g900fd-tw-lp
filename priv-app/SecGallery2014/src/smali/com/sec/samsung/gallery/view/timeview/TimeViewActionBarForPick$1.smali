.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;
.super Ljava/lang/Object;
.source "TimeViewActionBarForPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 39
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030006

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 41
    .local v0, "customView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 43
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 46
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 47
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 48
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 50
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 53
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 54
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 55
    return-void
.end method
