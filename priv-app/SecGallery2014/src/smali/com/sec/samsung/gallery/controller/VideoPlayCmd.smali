.class public Lcom/sec/samsung/gallery/controller/VideoPlayCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "VideoPlayCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;
    }
.end annotation


# static fields
.field private static final ACTION_SECURE_LOCK:Ljava/lang/String; = "android.intent.action.SECURE_LOCK"

.field private static final ACTION_VIDEO_PLAY_COVERMODE:Ljava/lang/String; = "android.intent.action.START_SCOVER_PLAYBACK"

.field private static final TAG:Ljava/lang/String; = "VideoPlayCmd"

.field private static mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 126
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/VideoPlayCmd;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->launchVideoPlayer(Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method public static cancelTask()V
    .locals 2

    .prologue
    .line 170
    sget-object v0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    if-eqz v0, :cond_0

    .line 171
    sget-object v0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->cancel(Z)Z

    .line 172
    :cond_0
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    .line 173
    return-void
.end method

.method private launchVideoPlayer(Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 10
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 69
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v5

    .line 70
    .local v5, "title":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v3

    .line 72
    .local v3, "mimeType":Ljava/lang/String;
    if-eqz v3, :cond_0

    const-string/jumbo v7, "video/mpeg4"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string/jumbo v7, "video/mp2p"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 73
    :cond_0
    const-string/jumbo v3, "video/*"

    .line 84
    :cond_1
    if-nez p1, :cond_2

    .line 85
    :try_start_0
    new-instance v7, Landroid/content/ActivityNotFoundException;

    invoke-direct {v7}, Landroid/content/ActivityNotFoundException;-><init>()V

    throw v7
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 120
    :catch_0
    move-exception v0

    .line 121
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v7, "VideoPlayCmd"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Can not play video : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    const v8, 0x7f0e0065

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 124
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :goto_0
    return-void

    .line 86
    :cond_2
    :try_start_1
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 87
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_4

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 88
    const-string v7, "file://"

    invoke-virtual {v1, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 89
    new-instance v4, Ljava/lang/StringBuffer;

    const-string v7, "file://"

    invoke-direct {v4, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 90
    .local v4, "sb":Ljava/lang/StringBuffer;
    invoke-virtual {v4, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    .line 92
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    :cond_3
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 94
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->launchedFromSetupWidzard(Landroid/app/Activity;)Z

    move-result v2

    .line 97
    .local v2, "fromSetupWidzard":Z
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCameraQuickViewOnLockscreen(Landroid/app/Activity;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 98
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/GalleryActivity;->disableFinishingAtSecureLock()V

    .line 99
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.SECURE_LOCK"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    .local v6, "videoPlayIntent":Landroid/content/Intent;
    const-string v7, "createBySecureLock"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 107
    :goto_1
    invoke-virtual {v6, p1, v3}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 108
    const-string v7, "android.intent.extra.TITLE"

    invoke-virtual {v6, v7, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 109
    const-string v7, "from-sw"

    invoke-virtual {v6, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 110
    const-string v7, "from_gallery_to_videoplayer"

    const/4 v8, 0x1

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 111
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 112
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v7

    if-eqz v7, :cond_5

    .line 113
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryCoverMode()Lcom/sec/android/gallery3d/app/GalleryCoverMode;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/app/GalleryCoverMode;->isVideoStarted(Z)V

    .line 114
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v8, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/4 v8, 0x0

    invoke-virtual {v7, v6, v8}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 118
    :goto_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 119
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 101
    .end local v6    # "videoPlayIntent":Landroid/content/Intent;
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    check-cast v7, Landroid/app/Activity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCoverMode(Landroid/app/Activity;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 102
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.START_SCOVER_PLAYBACK"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 103
    .restart local v6    # "videoPlayIntent":Landroid/content/Intent;
    const-string v7, "com.sec.android.app.videoplayer"

    const-string v8, "com.sec.android.app.videoplayer.activity.SCoverPlayer"

    invoke-virtual {v6, v7, v8}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    .line 105
    .end local v6    # "videoPlayIntent":Landroid/content/Intent;
    :cond_7
    new-instance v6, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v6    # "videoPlayIntent":Landroid/content/Intent;
    goto :goto_1

    .line 116
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v7, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method private launchedFromSetupWidzard(Landroid/app/Activity;)Z
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 63
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    .line 64
    .local v1, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isLaunchFromSetupWidzard()Z

    move-result v0

    .line 65
    .local v0, "fromSetupWidzard":Z
    return v0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v5, 0x0

    .line 40
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 41
    .local v3, "params":[Ljava/lang/Object;
    aget-object v0, v3, v5

    check-cast v0, Landroid/app/Activity;

    .line 42
    .local v0, "activity":Landroid/app/Activity;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;

    .line 43
    const/4 v4, 0x1

    aget-object v2, v3, v4

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 45
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_0

    .line 60
    :goto_0
    return-void

    .line 48
    :cond_0
    instance-of v4, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v4, :cond_1

    .line 49
    invoke-static {}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->cancelTask()V

    .line 50
    new-instance v4, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    invoke-direct {v4, p0, v2}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;-><init>(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;Lcom/sec/android/gallery3d/data/MediaItem;)V

    sput-object v4, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    .line 51
    sget-object v4, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mPlayUriTask:Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 54
    :cond_1
    :try_start_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getPlayUri()Landroid/net/Uri;

    move-result-object v4

    invoke-direct {p0, v4, v2}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->launchVideoPlayer(Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 55
    :catch_0
    move-exception v1

    .line 56
    .local v1, "e":Ljava/lang/UnsupportedOperationException;
    invoke-virtual {v1}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V

    goto :goto_0
.end method
