.class Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;
.super Ljava/lang/Object;
.source "AirButtonViewer.java"

# interfaces
.implements Lcom/samsung/android/airbutton/AirButtonImpl$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/airbutton/AirButtonImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/view/View;ILjava/lang/Object;)V
    .locals 12
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "itemIndex"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 56
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    if-nez v5, :cond_0

    .line 122
    :goto_0
    return-void

    .line 57
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v5, :cond_1

    .line 58
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v5

    # setter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v6, v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$002(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/data/MediaItem;

    .line 62
    :cond_1
    packed-switch p2, :pswitch_data_0

    .line 117
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    const v6, 0x7f0f00bf

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5, v8}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setShowAirButton(Z)V

    .line 119
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Landroid/app/Activity;

    const v6, 0x7f0f00bf

    invoke-virtual {v5, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/android/airbutton/AirButtonImpl;->unlinkWithParentView()V

    goto :goto_0

    .line 64
    :pswitch_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v5

    if-nez v5, :cond_3

    .line 65
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # invokes: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->createThreadAndDialog()V
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$100(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)V

    .line 66
    new-array v1, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v1, v9

    .line 67
    .local v1, "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "START_IMAGE_NOTE"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 69
    .end local v1    # "params":[Ljava/lang/Object;
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 70
    .local v0, "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 71
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 72
    new-array v1, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v9

    .line 73
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "SHOW_SHARE_DIALOG"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 74
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto/16 :goto_1

    .line 78
    .end local v0    # "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .end local v1    # "params":[Ljava/lang/Object;
    :pswitch_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_4

    .line 79
    new-array v1, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    aput-object v5, v1, v7

    .line 80
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "CROP_IMAGE"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 82
    .end local v1    # "params":[Ljava/lang/Object;
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 83
    .local v3, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 84
    new-array v1, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v7

    .line 85
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "VIDEO_TRIM"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 89
    .end local v1    # "params":[Ljava/lang/Object;
    .end local v3    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :pswitch_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v5

    if-nez v5, :cond_5

    .line 90
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 91
    .local v2, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    .line 92
    .local v4, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    new-array v1, v11, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    aput-object v5, v1, v8

    aput-object v2, v1, v7

    const/4 v5, 0x0

    aput-object v5, v1, v9

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v10

    .line 94
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "IMAGE_EDIT"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 96
    .end local v1    # "params":[Ljava/lang/Object;
    .end local v2    # "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_5
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 97
    .restart local v3    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 98
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 99
    new-array v1, v11, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v9

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v10

    .line 100
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "SHOW_DELETE_DIALOG"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 104
    .end local v1    # "params":[Ljava/lang/Object;
    .end local v3    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :pswitch_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_2

    .line 105
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 106
    .restart local v0    # "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 107
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 108
    new-array v1, v10, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v8

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v7

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v1, v9

    .line 109
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$200(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Landroid/content/Context;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "SHOW_SHARE_DIALOG"

    invoke-virtual {v5, v6, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 110
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/view/common/AirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/AirButtonViewer;->access$000(Lcom/sec/samsung/gallery/view/common/AirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto/16 :goto_1

    .line 62
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
