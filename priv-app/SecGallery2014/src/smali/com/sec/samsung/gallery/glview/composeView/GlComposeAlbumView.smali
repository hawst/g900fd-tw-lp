.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.source "GlComposeAlbumView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;
    }
.end annotation


# static fields
.field protected static final CMD_SELECT_ALBUM:I = 0x65

.field public static final STATUS_REORDER_CANCEL:I = 0x5

.field public static final STATUS_REORDER_DONE:I = 0x4

.field public static final STATUS_REORDER_START:I = 0x3

.field public static final TAG:Ljava/lang/String;


# instance fields
.field private final MOVETO_ANIMAITION_DURATION:I

.field private final REORDER_ANIMATION_DURATION:I

.field private mDragIndex:I

.field private mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

.field private mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

.field private mFlags:I

.field private mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

.field private mIsRunningReorderAnim:Z

.field public mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mMoveToAnim:Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

.field mMoveToAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field private mNeedSetMode:Z

.field private mPressed:Z

.field private mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

.field private mReorderAnimaitionListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field public mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

.field public mReorderMode:Z

.field private mStartIndex:I

.field private mValidView:Landroid/graphics/Rect;

.field private mViewMode:I

.field private reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "initCode"    # I
    .param p3, "initItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "option"    # I
    .param p5, "config"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 104
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    .line 36
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->REORDER_ANIMATION_DURATION:I

    .line 37
    const/16 v0, 0x96

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->MOVETO_ANIMAITION_DURATION:I

    .line 44
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPressed:Z

    .line 45
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mNeedSetMode:Z

    .line 46
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlags:I

    .line 48
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderMode:Z

    .line 50
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 51
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 52
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 53
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 54
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I

    .line 55
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    .line 57
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    .line 58
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 59
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

    .line 60
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mIsRunningReorderAnim:Z

    .line 62
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnimaitionListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 81
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 106
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mIsRunningReorderAnim:Z

    return v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mIsRunningReorderAnim:Z

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->onDragEnd()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->resetReorderData()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .prologue
    .line 32
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mViewMode:I

    return v0
.end method

.method private controlByDrag(Z)Z
    .locals 10
    .param p1, "active"    # Z

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v5, 0x0

    .line 439
    const/4 v2, 0x0

    .line 440
    .local v2, "moveToFolder":Z
    if-nez p1, :cond_2

    .line 441
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 442
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-eqz v6, :cond_0

    .line 443
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setFocused(Z)V

    .line 444
    iput-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 445
    const/4 v2, 0x1

    .line 447
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    iput-boolean v5, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mActive:Z

    move v5, v2

    .line 491
    :cond_1
    :goto_0
    return v5

    .line 451
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsX()F

    move-result v3

    .line 452
    .local v3, "x":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getAbsY()F

    move-result v4

    .line 453
    .local v4, "y":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    invoke-virtual {v6, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->getIntensity(FF)F

    move-result v1

    .line 455
    .local v1, "intensity":F
    cmpl-float v6, v1, v8

    if-eqz v6, :cond_4

    cmpl-float v6, v1, v8

    if-lez v6, :cond_3

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    cmpl-float v6, v6, v8

    if-eqz v6, :cond_4

    :cond_3
    cmpg-float v6, v1, v8

    if-gez v6, :cond_9

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v6, v6, v7

    if-nez v6, :cond_9

    .line 456
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mActive:Z

    if-eqz v6, :cond_5

    .line 457
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v6, v8}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 458
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    iput-boolean v5, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mActive:Z

    goto :goto_0

    .line 460
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->isRunning()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->isReady()Z

    move-result v6

    if-nez v6, :cond_1

    .line 463
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->findFocusAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 465
    .local v0, "focusedObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-nez v0, :cond_6

    .line 466
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    goto :goto_0

    .line 470
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-eqz v6, :cond_7

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v7

    if-eq v6, v7, :cond_1

    .line 471
    :cond_7
    iget-object v6, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mCoverObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mReorderEnable:Z

    if-eqz v6, :cond_1

    .line 474
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v7

    if-eq v6, v7, :cond_8

    .line 475
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->startReorder(IILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V

    .line 476
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    .line 478
    :cond_8
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    goto/16 :goto_0

    .line 482
    .end local v0    # "focusedObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-eqz v6, :cond_a

    .line 483
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setFocused(Z)V

    .line 484
    iput-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 487
    :cond_a
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-float v7, v1

    const/high16 v8, 0x41200000    # 10.0f

    mul-float/2addr v7, v8

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 488
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    const/4 v7, 0x1

    iput-boolean v7, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->mActive:Z

    goto/16 :goto_0
.end method

.method private createDragObject()V
    .locals 17

    .prologue
    .line 340
    const v1, 0x3f666666    # 0.9f

    .line 341
    .local v1, "DST_RESIZE_FACTOR":F
    const/high16 v9, 0x41200000    # 10.0f

    .line 342
    .local v9, "stackZ":F
    const/4 v8, 0x2

    .line 343
    .local v8, "posLatency":I
    const v3, 0x3e4ccccd    # 0.2f

    .line 344
    .local v3, "MIN_DIM_VALUE":F
    const/high16 v2, 0x3f800000    # 1.0f

    .line 346
    .local v2, "MAX_DIM_VALUE":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v10

    .line 347
    .local v10, "sx":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v11

    .line 348
    .local v11, "sy":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v12

    .line 349
    .local v12, "sz":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v13

    mul-float v6, v13, v1

    .line 350
    .local v6, "dstW":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v13

    mul-float v5, v13, v1

    .line 352
    .local v5, "dstH":F
    new-instance v13, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 354
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v7, v13, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 355
    .local v7, "objectIndex":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iput v7, v13, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mIndex:I

    .line 356
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPosIndex()I

    move-result v14

    iput v14, v13, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    .line 357
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v15, 0x0

    invoke-virtual {v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/16 v16, 0x0

    invoke-virtual/range {v15 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v15

    invoke-virtual {v13, v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSize(FF)V

    .line 359
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v16

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(FFF)V

    .line 361
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setCurrentPosToSource()V

    .line 362
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mWidth:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v15, v15, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mHeight:F

    invoke-virtual {v13, v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setSourceSize(FF)V

    .line 364
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v13, v6, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetSize(FF)V

    .line 365
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    add-float v14, v12, v9

    invoke-virtual {v13, v10, v11, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTargetPos(FFF)V

    .line 366
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v13, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setTrailLatency(I)V

    .line 367
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    const/4 v14, 0x0

    iput-boolean v14, v13, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mTitleVisible:Z

    .line 368
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    .line 370
    const/4 v4, 0x0

    .line 371
    .local v4, "dim":F
    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v13, :cond_0

    .line 372
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    const v14, 0x3ecccccd    # 0.4f

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setAlpha(F)V

    .line 373
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    const/4 v14, 0x0

    const v15, 0x3f10902e    # 0.5647f

    const v16, 0x3f2bac71    # 0.6706f

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setColor(FFF)V

    .line 379
    :goto_0
    return-void

    .line 375
    :cond_0
    sub-float v13, v2, v3

    const/high16 v14, 0x3f800000    # 1.0f

    div-float/2addr v13, v14

    add-float v4, v13, v3

    .line 376
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v13, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setDim(F)V

    .line 377
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    const/4 v14, 0x0

    const/high16 v15, 0x3f100000    # 0.5625f

    const v16, 0x3f2afb7f    # 0.6679f

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setColor(FFF)V

    goto :goto_0
.end method

.method private findFocusAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 7
    .param p1, "current"    # Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .prologue
    .line 537
    const/4 v0, 0x0

    .line 538
    .local v0, "focusedObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getActiveGroup()Landroid/util/SparseArray;

    move-result-object v1

    .line 539
    .local v1, "groupObjects":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    if-nez v1, :cond_0

    .line 540
    const/4 v6, 0x0

    .line 557
    :goto_0
    return-object v6

    .line 545
    :cond_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 546
    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 547
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-nez v3, :cond_2

    .line 545
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 549
    :cond_2
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getAbsX()F

    move-result v4

    .line 550
    .local v4, "x":F
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getAbsY()F

    move-result v5

    .line 552
    .local v5, "y":F
    invoke-virtual {p1, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->isInclude(FF)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 553
    move-object v0, v3

    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v4    # "x":F
    .end local v5    # "y":F
    :cond_3
    move-object v6, v0

    .line 557
    goto :goto_0
.end method

.method private onDrag(IIZ)Z
    .locals 7
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "onDragging"    # Z

    .prologue
    const/4 v6, 0x1

    .line 420
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->isScreenLocked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 431
    :goto_0
    return v6

    .line 424
    :cond_0
    if-eqz p3, :cond_1

    .line 425
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mWidthSpace:F

    int-to-float v3, p1

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 426
    .local v0, "sx":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeightSpace:F

    int-to-float v3, p2

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeight:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 427
    .local v1, "sy":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mHndDispmnt:I

    neg-float v4, v1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(IFFF)V

    .line 428
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->moveToLast()V

    .line 430
    .end local v0    # "sx":F
    .end local v1    # "sy":F
    :cond_1
    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->controlByDrag(Z)Z

    goto :goto_0
.end method

.method private onDragEnd()Z
    .locals 5

    .prologue
    .line 495
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->TAG:Ljava/lang/String;

    const-string v1, "onDragEnd+"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 496
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 497
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    const/16 v4, 0x96

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->startAnimation(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;II)V

    .line 499
    const/4 v0, 0x1

    return v0
.end method

.method private resetReorderData()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 510
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    .line 511
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 513
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setColor(FFF)V

    .line 514
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderVisible(Z)V

    .line 515
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 518
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_2

    .line 519
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 520
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 523
    :cond_2
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 524
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 525
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I

    .line 526
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->reset()V

    .line 527
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v0, :cond_3

    .line 529
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->remove()V

    .line 530
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 532
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setAlwaysActiveGroupObjectIndex(I)V

    .line 533
    return-void
.end method

.method private setValidView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mValidView:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 172
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeight:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mValidView:Landroid/graphics/Rect;

    .line 175
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mValidView:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHeight:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method private startDragAnimation(I)V
    .locals 13
    .param p1, "index"    # I

    .prologue
    const v12, 0x7f0200f9

    const/16 v11, 0x140

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 382
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-nez v6, :cond_0

    .line 383
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 385
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v6, :cond_2

    .line 386
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v1, v6, 0x10

    .line 387
    .local v1, "groupIndex":I
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    .line 388
    sget-object v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "startReorder startDragAnimation : mLongClickedObj.mIndex : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", groupIndex : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getGroupObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    move-result-object v2

    .line 394
    .local v2, "reorderObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-nez v6, :cond_1

    .line 395
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 396
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v6, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setAlwaysActiveGroupObjectIndex(I)V

    .line 397
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->createDragObject()V

    .line 399
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->cancelAnimation()V

    .line 400
    new-instance v6, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v8

    invoke-direct {v6, p0, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 401
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v7, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v7, v8, v11, v11}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 402
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 403
    .local v4, "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    new-array v6, v10, [I

    const v7, -0x50506

    aput v7, v6, v9

    sget-object v7, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v10, v10, v7}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 404
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    invoke-direct {v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 405
    .local v5, "viewSub":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 406
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v12}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    .line 407
    .local v3, "strokeDrawer":Landroid/graphics/drawable/Drawable;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b0008

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 408
    invoke-virtual {v4, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 409
    invoke-virtual {v5, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;)I

    .line 410
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v6, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 411
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->reorderStorke:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 413
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-eqz v6, :cond_2

    .line 414
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderGroupObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPosIndex()I

    move-result v7

    iput v7, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mHndDispmnt:I

    .line 417
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    .end local v1    # "groupIndex":I
    .end local v2    # "reorderObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v3    # "strokeDrawer":Landroid/graphics/drawable/Drawable;
    .end local v4    # "view":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v5    # "viewSub":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    return-void
.end method


# virtual methods
.method public getFirstThumbObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getItemWidth()F
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemW:F

    return v0
.end method

.method protected getParam()I
    .locals 3

    .prologue
    .line 503
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mStartIndex:I

    .line 504
    .local v1, "start":I
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragIndex:I

    .line 506
    .local v0, "end":I
    shl-int/lit8 v2, v0, 0x10

    or-int/2addr v2, v1

    return v2
.end method

.method protected handleLongClick(III)Z
    .locals 4
    .param p1, "index"    # I
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 236
    shr-int/lit8 v0, p1, 0x10

    .line 237
    .local v0, "i":I
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "startReorder handleLongClick : index : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", index>>16 : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->handleLongClick(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 239
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->startDragAnimation(I)V

    .line 240
    const/4 v1, 0x1

    .line 242
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public handleSelectAlbum(II)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "changed"    # I

    .prologue
    const/4 v5, 0x1

    .line 262
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v3, :cond_0

    .line 279
    :goto_0
    return-void

    .line 265
    :cond_0
    if-eqz p2, :cond_1

    .line 266
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 269
    :cond_1
    shl-int/lit8 v0, p1, 0x10

    .line 270
    .local v0, "code":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v2

    .line 271
    .local v2, "scroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getVisibleScrollDelta(F)F

    move-result v1

    .line 272
    .local v1, "newScroll":F
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_2

    .line 273
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    add-float/2addr v1, v3

    .line 274
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v1

    .line 275
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 276
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 278
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 6

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setValidView()V

    .line 111
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onCreate()V

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mInitialCode:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->IsCheckMode()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mNeedSetMode:Z

    .line 115
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    .line 116
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->setDuration(J)V

    .line 117
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 118
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnimaitionListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 120
    const/4 v1, 0x0

    .line 121
    .local v1, "mode":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 123
    .local v0, "mStatusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v3, :cond_1

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    const-string v3, "peopleViewMode"

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDefaultMode()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 133
    :goto_0
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mViewMode:I

    .line 135
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$1;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    .line 136
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragInfo:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView$DragInfo;->reset()V

    .line 138
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

    invoke-direct {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;-><init>()V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

    .line 139
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMoveToAnimationListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlMovetoAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 141
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v2, :cond_0

    .line 142
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mComposeViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 144
    :cond_0
    return-void

    .line 129
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    const-string v3, "albumViewMode"

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDefaultMode()I

    move-result v4

    invoke-static {v2, v3, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 290
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->isSlideShowMode:Z

    if-eqz v5, :cond_1

    .line 321
    :cond_0
    :goto_0
    return v3

    .line 292
    :cond_1
    const/16 v5, 0xa8

    if-eq p1, v5, :cond_2

    const/16 v5, 0xa9

    if-eq p1, v5, :cond_2

    const/16 v5, 0x118

    if-eq p1, v5, :cond_2

    const/16 v5, 0x117

    if-ne p1, v5, :cond_3

    :cond_2
    move v3, v4

    .line 296
    goto :goto_0

    .line 299
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->isScreenLocked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 302
    const/16 v5, 0x42

    if-eq p1, v5, :cond_4

    const/16 v5, 0x17

    if-ne p1, v5, :cond_6

    .line 304
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v5, :cond_5

    .line 305
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v5, :cond_5

    .line 306
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    .line 307
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 308
    if-eqz v2, :cond_0

    .line 311
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v5, 0x10

    .line 312
    .local v0, "albumIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v6, 0xffff

    and-int v1, v5, v6

    .line 313
    .local v1, "itemIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-interface {v5, v6, v7, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 315
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v6, 0x4

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v5, v6, v7, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .end local v0    # "albumIndex":I
    .end local v1    # "itemIndex":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_5
    move v3, v4

    .line 319
    goto :goto_0

    .line 321
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method protected onMessageExtra(ILjava/lang/Object;III)V
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 180
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 181
    invoke-virtual {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->handleSelectAlbum(II)V

    .line 183
    :cond_0
    return-void
.end method

.method protected onMoved(II)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 206
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPressed:Z

    if-nez v0, :cond_0

    .line 207
    const/4 v0, 0x0

    .line 212
    :goto_0
    return v0

    .line 209
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mLongClickedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 210
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->onDrag(IIZ)Z

    move-result v0

    goto :goto_0

    .line 212
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMoved(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 196
    if-ne p1, v1, :cond_0

    if-eq p2, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 197
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPressed:Z

    .line 198
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onPressed(II)Z

    move-result v0

    .line 201
    :goto_0
    return v0

    .line 200
    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPressed:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mPressed:Z

    if-nez v0, :cond_0

    .line 219
    const/4 v0, 0x0

    .line 227
    :goto_0
    return v0

    .line 221
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mReorderAnim:Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlReorderAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mIsRunningReorderAnim:Z

    .line 227
    :cond_1
    :goto_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onReleased(IIII)Z

    move-result v0

    goto :goto_0

    .line 225
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->onDragEnd()Z

    goto :goto_1
.end method

.method protected onSetMode(IILjava/lang/Object;)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 232
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 162
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mDragObject:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v0, :cond_0

    .line 163
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->resetReorderData()V

    .line 164
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mOnExtendListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;->onExtendRequest(I)V

    .line 168
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onSurfaceChanged(II)V

    .line 169
    return-void
.end method

.method protected playSoundOnClickThumb()V
    .locals 3

    .prologue
    .line 326
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    .line 327
    .local v0, "soundUtils":Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mMode:I

    if-nez v1, :cond_1

    .line 328
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    if-eqz v0, :cond_0

    .line 331
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_0
.end method

.method protected resetLayout()V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setValidView()V

    .line 157
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetLayout()V

    .line 158
    return-void
.end method

.method protected resetScrollBar()V
    .locals 3

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-nez v0, :cond_0

    .line 192
    :goto_0
    return-void

    .line 189
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetScrollBar()V

    .line 190
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setValidView()V

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mWidth:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mValidView:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->convX(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setRightPadding(F)V

    goto :goto_0
.end method

.method public selectAlbum(IZ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "changed"    # Z

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 257
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v3, 0x65

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 259
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 257
    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 149
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mNeedSetMode:Z

    if-eqz v0, :cond_0

    .line 150
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mNeedSetMode:Z

    .line 152
    :cond_0
    return-void
.end method

.method public setFlags(IZ)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "set"    # Z

    .prologue
    .line 282
    if-eqz p2, :cond_0

    .line 283
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlags:I

    .line 286
    :goto_0
    return-void

    .line 285
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mFlags:I

    goto :goto_0
.end method

.method public updateFolderItem(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x3

    shl-int/lit8 v2, p1, 0x10

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 253
    :cond_0
    return-void
.end method
