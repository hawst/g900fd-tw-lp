.class Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;
.super Landroid/os/AsyncTask;
.source "VideoPlayCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/VideoPlayCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayUriTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/net/Uri;",
        ">;",
        "Landroid/content/DialogInterface$OnCancelListener;"
    }
.end annotation


# instance fields
.field private mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/VideoPlayCmd;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->this$0:Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 129
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 132
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 133
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/net/Uri;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getPlayUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 126
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->doInBackground([Ljava/lang/Void;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 165
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->cancel(Z)Z

    .line 166
    return-void
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 159
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 161
    return-void
.end method

.method protected onPostExecute(Landroid/net/Uri;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 149
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 151
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->this$0:Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->launchVideoPlayer(Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v0, p1, v1}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->access$100(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;Landroid/net/Uri;Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 126
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->onPostExecute(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    .line 137
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->this$0:Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->access$000(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/VideoPlayCmd$PlayUriTask;->this$0:Lcom/sec/samsung/gallery/controller/VideoPlayCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/VideoPlayCmd;->access$000(Lcom/sec/samsung/gallery/controller/VideoPlayCmd;)Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e0032

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v6, p0

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 140
    return-void
.end method
