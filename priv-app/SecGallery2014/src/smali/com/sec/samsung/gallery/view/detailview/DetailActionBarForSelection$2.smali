.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;
.super Ljava/lang/Object;
.source "DetailActionBarForSelection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;I)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 297
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 299
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 300
    .local v3, "res":Landroid/content/res/Resources;
    const/4 v2, 0x0

    .line 301
    .local v2, "numberOfSelectedItems":Ljava/lang/String;
    const/4 v0, 0x0

    .line 302
    .local v0, "contentDescription":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->getMaxCount()I

    move-result v1

    .line 303
    .local v1, "maxCount":I
    if-lez v1, :cond_4

    .line 304
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v4, :cond_3

    .line 305
    const v4, 0x7f0e0051

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 309
    :goto_1
    const v4, 0x7f0e0052

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 319
    :cond_2
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    iget v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v4, v5, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;ILjava/lang/String;)V

    .line 320
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->setSelectAllButtonTTS(Ljava/lang/String;)V
    invoke-static {v4, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;Ljava/lang/String;)V

    goto :goto_0

    .line 308
    :cond_3
    const v4, 0x7f0e0050

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v9, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 311
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    if-ltz v4, :cond_2

    .line 312
    iget v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    if-ne v4, v7, :cond_5

    .line 313
    const v4, 0x7f0e004f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 317
    :goto_3
    move-object v0, v2

    goto :goto_2

    .line 315
    :cond_5
    const v4, 0x7f0e004e

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;->val$numberOfItemsSelected:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_3
.end method
