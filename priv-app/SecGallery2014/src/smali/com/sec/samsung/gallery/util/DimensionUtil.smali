.class public Lcom/sec/samsung/gallery/util/DimensionUtil;
.super Ljava/lang/Object;
.source "DimensionUtil.java"


# static fields
.field private static final ALBUMTIME_SCREENSIZE_RATIO:I = 0x3


# instance fields
.field private mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

.field private mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

.field private mContext:Landroid/content/Context;

.field private mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

.field private mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

.field private mGl2WinRatio:F

.field private mGlHeight:F

.field private mGlWidth:F

.field private mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

.field private mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

.field private mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

.field private mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

.field private mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

.field private mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

.field private mWin2GlRatio:F

.field private mWinHeight:I

.field private mWinWidth:I

.field protected mbLandscape:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    .line 38
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mContext:Landroid/content/Context;

    .line 39
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    .line 40
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    .line 41
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    .line 42
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    .line 43
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    .line 44
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    .line 45
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    .line 46
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    .line 47
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    .line 48
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    .line 50
    return-void
.end method

.method private calcParameters()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 54
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v0

    .line 55
    .local v0, "rotation":I
    if-eq v0, v3, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    :cond_0
    move v1, v3

    :goto_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    .line 56
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsPortraitTablet:Z

    if-nez v1, :cond_1

    .line 57
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-nez v1, :cond_3

    :goto_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    .line 59
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGlWidth:F

    iget v2, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinWidth:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    .line 60
    return-void

    :cond_2
    move v1, v2

    .line 55
    goto :goto_0

    :cond_3
    move v3, v2

    .line 57
    goto :goto_1
.end method

.method private getAlbumViewDimensionIndex(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 75
    if-gtz p1, :cond_1

    .line 76
    const/4 p1, 0x0

    .line 80
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v1, :cond_2

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v0, v1, 0x1

    .line 81
    .local v0, "dimensionIndex":I
    :goto_1
    return v0

    .line 77
    .end local v0    # "dimensionIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    add-int/lit8 p1, v1, -0x1

    goto :goto_0

    .line 80
    :cond_2
    mul-int/lit8 v0, p1, 0x2

    goto :goto_1
.end method

.method private getDetailViewDimensionIndex(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 119
    if-gtz p1, :cond_1

    .line 120
    const/4 p1, 0x0

    .line 124
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v1, :cond_2

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v0, v1, 0x1

    .line 125
    .local v0, "dimensionIndex":I
    :goto_1
    return v0

    .line 121
    .end local v0    # "dimensionIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    add-int/lit8 p1, v1, -0x1

    goto :goto_0

    .line 124
    :cond_2
    mul-int/lit8 v0, p1, 0x2

    goto :goto_1
.end method

.method private getThumbnailViewDimensionIndex(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 86
    if-gtz p1, :cond_1

    .line 87
    const/4 p1, 0x0

    .line 91
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v1, :cond_2

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v0, v1, 0x1

    .line 92
    .local v0, "dimensionIndex":I
    :goto_1
    return v0

    .line 88
    .end local v0    # "dimensionIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    add-int/lit8 p1, v1, -0x1

    goto :goto_0

    .line 91
    :cond_2
    mul-int/lit8 v0, p1, 0x2

    goto :goto_1
.end method

.method private getThumbnailViewNewAlbumDimensionIndex(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 108
    if-gtz p1, :cond_1

    .line 109
    const/4 p1, 0x0

    .line 113
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v1, :cond_2

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v0, v1, 0x1

    .line 114
    .local v0, "dimensionIndex":I
    :goto_1
    return v0

    .line 110
    .end local v0    # "dimensionIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_mode_count:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_mode_count:I

    add-int/lit8 p1, v1, -0x1

    goto :goto_0

    .line 113
    :cond_2
    mul-int/lit8 v0, p1, 0x2

    goto :goto_1
.end method

.method private getThumbnailViewSplitAlbumDimensionIndex(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 97
    if-gtz p1, :cond_1

    .line 98
    const/4 p1, 0x0

    .line 102
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v1, :cond_2

    mul-int/lit8 v1, p1, 0x2

    add-int/lit8 v0, v1, 0x1

    .line 103
    .local v0, "dimensionIndex":I
    :goto_1
    return v0

    .line 99
    .end local v0    # "dimensionIndex":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_mode_count:I

    add-int/lit8 v1, v1, -0x1

    if-le p1, v1, :cond_0

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_mode_count:I

    add-int/lit8 p1, v1, -0x1

    goto :goto_0

    .line 102
    :cond_2
    mul-int/lit8 v0, p1, 0x2

    goto :goto_1
.end method


# virtual methods
.method public getActionBarHeight()F
    .locals 2

    .prologue
    .line 157
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getActionBarHeightPixel()I
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->actionbar_height_l:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->actionbar_height_p:I

    goto :goto_0
.end method

.method public getAlbumTimeChildScreenSize()[[I
    .locals 9

    .prologue
    const/4 v6, -0x1

    const/4 v8, 0x5

    .line 1203
    new-array v0, v8, [[I

    .line 1204
    .local v0, "array1":[[I
    new-array v1, v8, [[I

    .line 1206
    .local v1, "array2":[[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v8, :cond_1

    .line 1207
    sget-object v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v5, v5, v2

    mul-int/lit8 v5, v5, 0x2

    new-array v5, v5, [I

    aput-object v5, v0, v2

    .line 1208
    sget-object v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v5, v5, v2

    mul-int/lit8 v5, v5, 0x2

    new-array v5, v5, [I

    aput-object v5, v1, v2

    .line 1209
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    sget-object v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v5, v5, v2

    if-ge v3, v5, :cond_0

    .line 1210
    aget-object v5, v0, v2

    aput v6, v5, v3

    .line 1211
    aget-object v5, v1, v2

    aput v6, v5, v3

    .line 1209
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1206
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1216
    .end local v3    # "j":I
    :cond_1
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v8, :cond_3

    .line 1217
    sget-object v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v5, v5, v2

    mul-int/lit8 v4, v5, 0x2

    .line 1218
    .local v4, "modeCount":I
    const/4 v3, 0x0

    .restart local v3    # "j":I
    :goto_3
    if-ge v3, v4, :cond_2

    .line 1219
    aget-object v5, v0, v2

    iget-object v6, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_child_type:[[I

    aget-object v6, v6, v2

    aget v6, v6, v3

    div-int/lit8 v6, v6, 0x3

    aput v6, v5, v3

    .line 1220
    aget-object v5, v1, v2

    iget-object v6, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    const/4 v7, 0x1

    aget-object v6, v6, v7

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_child_type:[[I

    aget-object v6, v6, v2

    aget v6, v6, v3

    div-int/lit8 v6, v6, 0x3

    aput v6, v5, v3

    .line 1218
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1216
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1225
    .end local v3    # "j":I
    .end local v4    # "modeCount":I
    :cond_3
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v5, :cond_4

    .end local v0    # "array1":[[I
    :goto_4
    return-object v0

    .restart local v0    # "array1":[[I
    :cond_4
    move-object v0, v1

    goto :goto_4
.end method

.method public getAlbumTimeChildScreenSizePotrait()[[I
    .locals 8

    .prologue
    const/4 v7, 0x5

    .line 1229
    new-array v0, v7, [[I

    .line 1231
    .local v0, "array1":[[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v7, :cond_1

    .line 1232
    sget-object v4, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v4, v4, v1

    mul-int/lit8 v4, v4, 0x2

    new-array v4, v4, [I

    aput-object v4, v0, v1

    .line 1233
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    sget-object v4, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v4, v4, v1

    if-ge v2, v4, :cond_0

    .line 1234
    aget-object v4, v0, v1

    const/4 v5, -0x1

    aput v5, v4, v2

    .line 1233
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1231
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1237
    .end local v2    # "j":I
    :cond_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, v7, :cond_3

    .line 1238
    sget-object v4, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v4, v4, v1

    mul-int/lit8 v3, v4, 0x2

    .line 1239
    .local v3, "modeCount":I
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_3
    if-ge v2, v3, :cond_2

    .line 1240
    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_child_type:[[I

    aget-object v5, v5, v1

    aget v5, v5, v2

    div-int/lit8 v5, v5, 0x3

    aput v5, v4, v2

    .line 1239
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1237
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1244
    .end local v2    # "j":I
    .end local v3    # "modeCount":I
    :cond_3
    return-object v0
.end method

.method public getAlbumTimeCountFontColor()I
    .locals 1

    .prologue
    .line 1170
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_count_font_color:I

    return v0
.end method

.method public getAlbumTimeCountFontSize()I
    .locals 1

    .prologue
    .line 1178
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_count_font_size:I

    return v0
.end method

.method public getAlbumTimeFontColor()I
    .locals 1

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_font_color:I

    return v0
.end method

.method public getAlbumTimeFontSize()I
    .locals 1

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_font_size:I

    return v0
.end method

.method public getAlbumTimeScreenPadding()[I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1192
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    array-length v3, v3

    new-array v0, v3, [I

    .line 1193
    .local v0, "array1":[I
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    array-length v3, v3

    new-array v1, v3, [I

    .line 1194
    .local v1, "array2":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1195
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    aget v3, v3, v2

    div-int/lit8 v3, v3, 0x3

    aput v3, v0, v2

    .line 1196
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen_padding:[I

    aget v3, v3, v2

    div-int/lit8 v3, v3, 0x3

    aput v3, v1, v2

    .line 1194
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1198
    :cond_0
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v3, :cond_1

    .end local v0    # "array1":[I
    :goto_1
    return-object v0

    .restart local v0    # "array1":[I
    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public getAlbumTimeScreenRatio()I
    .locals 1

    .prologue
    .line 1248
    const/4 v0, 0x3

    return v0
.end method

.method public getAlbumTimeScreenSize()[I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1182
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    array-length v3, v3

    new-array v0, v3, [I

    .line 1183
    .local v0, "array1":[I
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    array-length v3, v3

    new-array v1, v3, [I

    .line 1184
    .local v1, "array2":[I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    array-length v3, v3

    if-ge v2, v3, :cond_0

    .line 1185
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v4

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    aget v3, v3, v2

    div-int/lit8 v3, v3, 0x3

    aput v3, v0, v2

    .line 1186
    iget-object v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->mAlbumTimeSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;

    aget-object v3, v3, v5

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension$Spec;->album_time_screen:[I

    aget v3, v3, v2

    div-int/lit8 v3, v3, 0x3

    aput v3, v1, v2

    .line 1184
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1188
    :cond_0
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v3, :cond_1

    .end local v0    # "array1":[I
    :goto_1
    return-object v0

    .restart local v0    # "array1":[I
    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public getAlbumTimeViewFlexibleThumbnailHeight(I)F
    .locals 2
    .param p1, "timeMode"    # I

    .prologue
    .line 1136
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeViewFlexibleThumbnailHeightPixel(I)F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumTimeViewFlexibleThumbnailHeightPixel(I)F
    .locals 3
    .param p1, "timeMode"    # I

    .prologue
    .line 1151
    const/4 v0, 0x0

    .line 1152
    .local v0, "canvasHeight":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeChildScreenSize()[[I

    move-result-object v1

    aget-object v1, v1, p1

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x3

    int-to-float v0, v1

    .line 1153
    return v0
.end method

.method public getAlbumTimeViewGap(I)F
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1161
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeScreenPadding()[I

    move-result-object v1

    aget v1, v1, p1

    int-to-float v0, v1

    .line 1162
    .local v0, "gap":F
    return v0
.end method

.method public getAlbumTimeViewThumbnailHeight(I)F
    .locals 2
    .param p1, "timeMode"    # I

    .prologue
    .line 1132
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeViewThumbnailHeightPixel(I)F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumTimeViewThumbnailHeightPixel(I)F
    .locals 3
    .param p1, "timeMode"    # I

    .prologue
    .line 1146
    const/4 v0, 0x0

    .line 1147
    .local v0, "canvasHeight":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeScreenSize()[I

    move-result-object v1

    mul-int/lit8 v2, p1, 0x2

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x3

    int-to-float v0, v1

    .line 1148
    return v0
.end method

.method public getAlbumTimeViewThumbnailWidth()F
    .locals 2

    .prologue
    .line 1129
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeViewThumbnailWidthPixel()F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumTimeViewThumbnailWidthPixel()F
    .locals 3

    .prologue
    .line 1140
    const/4 v0, 0x0

    .line 1141
    .local v0, "canvasHeight":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeScreenSize()[I

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    mul-int/lit8 v1, v1, 0x3

    int-to-float v0, v1

    .line 1142
    return v0
.end method

.method public getAlbumTimeViewVHGap()[I
    .locals 1

    .prologue
    .line 1257
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_view_gap:[I

    return-object v0
.end method

.method public getAlbumTimeViewVIDegree()I
    .locals 2

    .prologue
    .line 1253
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mbLandscape:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_VI_degree:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mTimeFolder:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->album_time_VI_degree:[I

    const/4 v1, 0x1

    aget v0, v0, v1

    goto :goto_0
.end method

.method public getAlbumViewAlbumCountFontSize(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->album_view_album_count_font_size:I

    return v0
.end method

.method public getAlbumViewAlbumFontColor()I
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_album_font_color:I

    return v0
.end method

.method public getAlbumViewAlbumFontSize(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->album_view_album_font_size:I

    return v0
.end method

.method public getAlbumViewAlbumLabelLeftMargin()I
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_album_label_padding_left:I

    return v0
.end method

.method public getAlbumViewBubblePopupRightPickerMargin()I
    .locals 1

    .prologue
    .line 204
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right_picker:I

    return v0
.end method

.method public getAlbumViewBubblePopupRigthMargin()I
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right:I

    return v0
.end method

.method public getAlbumViewBubblePopupRigthNoMenuMargin()I
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_right_nomenu:I

    return v0
.end method

.method public getAlbumViewBubblePopupTopMargin()I
    .locals 1

    .prologue
    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_popup_padding_top:I

    return v0
.end method

.method public getAlbumViewColumnItemCount(I)I
    .locals 6
    .param p1, "mode"    # I

    .prologue
    .line 228
    iget v3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinWidth:I

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewLeftPaddingPixel(I)I

    move-result v4

    sub-int/2addr v3, v4

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewRightPaddingPixel(I)I

    move-result v4

    sub-int v2, v3, v4

    .line 229
    .local v2, "viewWidth":I
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidthPixel(I)I

    move-result v0

    .line 230
    .local v0, "albumViewThumbnailWidthPixel":I
    sub-int v3, v2, v0

    int-to-float v3, v3

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewHorizontalGapPixel(I)I

    move-result v4

    add-int/2addr v4, v0

    int-to-float v4, v4

    div-float/2addr v3, v4

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    add-int/lit8 v1, v3, 0x1

    .line 231
    .local v1, "nCount":I
    if-nez v1, :cond_0

    .line 232
    const/4 v1, 0x1

    .line 234
    :cond_0
    return v1
.end method

.method public getAlbumViewDefaultMode()I
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_default_mode:I

    return v0
.end method

.method public getAlbumViewEndAffordanceRowDegree(I)I
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewEndAffordanceRowDegreeCount()I

    move-result v0

    .line 992
    .local v0, "count":I
    if-gez p1, :cond_0

    .line 993
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 997
    :goto_0
    return v1

    .line 994
    :cond_0
    if-ge p1, v0, :cond_1

    .line 995
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree:[I

    aget v1, v1, p1

    goto :goto_0

    .line 997
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree:[I

    add-int/lit8 v2, v0, -0x1

    aget v1, v1, v2

    goto :goto_0
.end method

.method public getAlbumViewEndAffordanceRowDegreeCount()I
    .locals 1

    .prologue
    .line 987
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree_count:I

    return v0
.end method

.method public getAlbumViewHorizontalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 279
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewHorizontalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewHorizontalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hgap:I

    return v0
.end method

.method public getAlbumViewLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 255
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 251
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hpadding_left:I

    return v0
.end method

.method public getAlbumViewMediaTypeIconBottomMargin()I
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mediatype_icon_padding_bottom:I

    return v0
.end method

.method public getAlbumViewMediaTypeIconLeftMargin()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mediatype_icon_padding_left:I

    return v0
.end method

.method public getAlbumViewModeCount()I
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_mode_count:I

    return v0
.end method

.method public getAlbumViewNameTextboxHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 335
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewNameTextboxHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_height:I

    return v0
.end method

.method public getAlbumViewNameTextboxWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 327
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewNameTextboxWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_width:I

    return v0
.end method

.method public getAlbumViewNameTextboxXOffset(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 311
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxXOffsetPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewNameTextboxXOffsetPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_hoffset:I

    return v0
.end method

.method public getAlbumViewNameTextboxYOffset(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxYOffsetPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewNameTextboxYOffsetPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->name_textbox_voffset:I

    return v0
.end method

.method public getAlbumViewNewLabelFontSize()I
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_font_size:I

    return v0
.end method

.method public getAlbumViewNewLabelHeight()I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_height:I

    return v0
.end method

.method public getAlbumViewNewLabelPadding()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_padding:I

    return v0
.end method

.method public getAlbumViewNewLabelRightMargin()I
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_right_margin:I

    return v0
.end method

.method public getAlbumViewNewLabelTopMargin()I
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_top_margin:I

    return v0
.end method

.method public getAlbumViewNewLabelWidth()I
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_new_label_width:I

    return v0
.end method

.method public getAlbumViewRightPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 263
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewRightPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewRightPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->hpadding_right:I

    return v0
.end method

.method public getAlbumViewRowItemCount(I)I
    .locals 7
    .param p1, "mode"    # I

    .prologue
    .line 238
    iget v4, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinHeight:I

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewTopPaddingPixel(I)I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v5

    float-to-int v5, v5

    sub-int v3, v4, v5

    .line 239
    .local v3, "viewHeight":I
    int-to-float v4, v3

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeightPixel(I)I

    move-result v5

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewVerticalGapPixel(I)I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 242
    .local v1, "floatCount":F
    const v0, 0x3c23d70a    # 0.01f

    .line 243
    .local v0, "epsilon":F
    sub-float v4, v1, v0

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v2, v4

    .line 244
    .local v2, "nCount":I
    if-nez v2, :cond_0

    .line 245
    const/4 v2, 0x1

    .line 247
    :cond_0
    return v2
.end method

.method public getAlbumViewScrollbarWidth()I
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_scrollbar_width:I

    return v0
.end method

.method public getAlbumViewThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 303
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->thumbnail_height:I

    return v0
.end method

.method public getAlbumViewThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 295
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->thumbnail_width:I

    return v0
.end method

.method public getAlbumViewTopPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 271
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewTopPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewTopPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->vpadding_top:I

    return v0
.end method

.method public getAlbumViewVerticalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 287
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewVerticalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getAlbumViewVerticalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->mAlbumViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension$Spec;->vgap:I

    return v0
.end method

.method public getAlbumtTimeViewFlexibleThumbnailHeightDP(I)I
    .locals 2
    .param p1, "timeMode"    # I

    .prologue
    .line 1156
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumTimeChildScreenSize()[[I

    move-result-object v0

    aget-object v0, v0, p1

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getDetailViewDefaultMode()I
    .locals 1

    .prologue
    .line 814
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_default_mode:I

    return v0
.end method

.method public getDetailViewHorizontalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 834
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewHorizontalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewHorizontalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 830
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_hgap:I

    return v0
.end method

.method public getDetailViewIconLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 858
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewIconLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 854
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_icon_hpadding_left:I

    return v0
.end method

.method public getDetailViewIconTopPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 866
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewIconTopPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewIconTopPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_icon_vpadding_top:I

    return v0
.end method

.method public getDetailViewLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 826
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 822
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_hpadding_left:I

    return v0
.end method

.method public getDetailViewModeCount()I
    .locals 1

    .prologue
    .line 818
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->detail_view_mode_count:I

    return v0
.end method

.method public getDetailViewStripIconBottomPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 914
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripIconBottomPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripIconBottomPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 910
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_icon_vpadding_bottom:I

    return v0
.end method

.method public getDetailViewStripIconLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 906
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripIconLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripIconLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 902
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_icon_hpadding_left:I

    return v0
.end method

.method public getDetailViewStripLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 874
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 870
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_hpadding_left:I

    return v0
.end method

.method public getDetailViewStripThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 898
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 894
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_thumbnail_height:I

    return v0
.end method

.method public getDetailViewStripThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 890
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 886
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_thumbnail_width:I

    return v0
.end method

.method public getDetailViewStripVerticalTop(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 882
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewStripVerticalTopPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewStripVerticalTopPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 878
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_strip_vpadding_top:I

    return v0
.end method

.method public getDetailViewThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 850
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 846
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_thumbnail_height:I

    return v0
.end method

.method public getDetailViewThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 842
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getDetailViewThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 838
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mDetailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;->mDetailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getDetailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension$Spec;->detail_view_thumbnail_width:I

    return v0
.end method

.method public getEventViewSubTitleFontColor()I
    .locals 1

    .prologue
    .line 925
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_font_color:I

    return v0
.end method

.method public getEventViewSubTitleFontSize()I
    .locals 1

    .prologue
    .line 937
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_font_size:I

    return v0
.end method

.method public getEventViewSubTitleHeight()I
    .locals 1

    .prologue
    .line 949
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_sub_title_height:I

    return v0
.end method

.method public getEventViewSuggestionFontColor()I
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_font_color:I

    return v0
.end method

.method public getEventViewSuggestionFontSize()I
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_font_size:I

    return v0
.end method

.method public getEventViewSuggestionHeight()I
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_height:I

    return v0
.end method

.method public getEventViewSuggestionPadding()I
    .locals 1

    .prologue
    .line 973
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_left_padding:I

    return v0
.end method

.method public getEventViewSuggestionRightMargin()I
    .locals 1

    .prologue
    .line 969
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_right_margin:I

    return v0
.end method

.method public getEventViewSuggestionTopMargin()I
    .locals 1

    .prologue
    .line 965
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_suggestion_top_margin:I

    return v0
.end method

.method public getEventViewTitleFontColor()I
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_font_color:I

    return v0
.end method

.method public getEventViewTitleFontSize()I
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_font_size:I

    return v0
.end method

.method public getEventViewTitleHeight()I
    .locals 1

    .prologue
    .line 945
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_height:I

    return v0
.end method

.method public getEventViewTitleLeftMargin()I
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_left_margin:I

    return v0
.end method

.method public getEventViewTitlePaddingforSSFont()I
    .locals 1

    .prologue
    .line 980
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_padding_for_ssfont:I

    return v0
.end method

.method public getEventViewTitleTopMargin()I
    .locals 1

    .prologue
    .line 957
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_title_top_margin:I

    return v0
.end method

.method public getEventViewVideoIconSize()I
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mEventViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;->event_view_video_icon_size:I

    return v0
.end method

.method public getGlWidthMultiWindowOffset()I
    .locals 1

    .prologue
    .line 1336
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->galleryGlWidthMWOffset:I

    return v0
.end method

.method public getHoveringActionBarButtonHeight()I
    .locals 1

    .prologue
    .line 1080
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_height:I

    return v0
.end method

.method public getHoveringActionBarButtonLabelHeight()I
    .locals 1

    .prologue
    .line 1111
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_label_height:I

    return v0
.end method

.method public getHoveringActionBarButtonLabelWidth()I
    .locals 1

    .prologue
    .line 1108
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_label_width:I

    return v0
.end method

.method public getHoveringActionBarButtonMarginLeft()I
    .locals 1

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_margin_left:I

    return v0
.end method

.method public getHoveringActionBarButtonMarginRight()I
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_margin_right:I

    return v0
.end method

.method public getHoveringActionBarButtonWidth()I
    .locals 1

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_width:I

    return v0
.end method

.method public getHoveringActionBarDividerHeight()I
    .locals 1

    .prologue
    .line 1068
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_divider_height:I

    return v0
.end method

.method public getHoveringActionBarDividerWidth()I
    .locals 1

    .prologue
    .line 1072
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_divider_width:I

    return v0
.end method

.method public getHoveringActionBarHeight()I
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_height:I

    return v0
.end method

.method public getHoveringActionBarMinHeight()I
    .locals 1

    .prologue
    .line 1100
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_min_height:I

    return v0
.end method

.method public getHoveringActionBarMinWidth()I
    .locals 1

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_min_width:I

    return v0
.end method

.method public getHoveringActionBarShadowSize()I
    .locals 1

    .prologue
    .line 1096
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_shadow_size:I

    return v0
.end method

.method public getHoveringBgPaddingBottom()F
    .locals 2

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringBgPaddingInside()F
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_inside:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringBgPaddingLeft()F
    .locals 2

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringBgPaddingRight()F
    .locals 2

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringBgPaddingTop()F
    .locals 2

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageGap()F
    .locals 2

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_gap:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageHeight()F
    .locals 2

    .prologue
    .line 1024
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageOffsetFromOriginalThumbnail()F
    .locals 2

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_offset_from_original_thumbnail:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageSetHeight()F
    .locals 2

    .prologue
    .line 1036
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageSetOffsetFromOriginalThumbnail()F
    .locals 2

    .prologue
    .line 1040
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_offset_from_original_thumbnail:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageSetWidth()F
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getHoveringImageWidth()F
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mHovering:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_width:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getNoItemsHeightLandscape()I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_bg_height_landscape:I

    return v0
.end method

.method public getNoItemsHeightPortrait()I
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->album_view_noitems_bg_height_portrait:I

    return v0
.end method

.method public getNoitemScreenCoef()I
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mAlbumViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;->noitems_screen_position_coef:I

    return v0
.end method

.method public getQuickCenterPopupTextBottomPadding()I
    .locals 1

    .prologue
    .line 1304
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_center_popup_text_bottom_padding:I

    return v0
.end method

.method public getQuickCenterPopupTextTopMargin()I
    .locals 1

    .prologue
    .line 1308
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_center_popup_text_top_margin:I

    return v0
.end method

.method public getQuickScrollBarHeight()I
    .locals 1

    .prologue
    .line 1284
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_height:I

    return v0
.end method

.method public getQuickScrollBarWidth()I
    .locals 1

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_width:I

    return v0
.end method

.method public getQuickScrollCenterPopupHeight()I
    .locals 1

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_cetner_popup_height:I

    return v0
.end method

.method public getQuickScrollCenterPopupWidth()I
    .locals 1

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_cetner_popup_width:I

    return v0
.end method

.method public getQuickScrollFontSize()I
    .locals 1

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_font_size:I

    return v0
.end method

.method public getQuickScrollItemPitch()F
    .locals 1

    .prologue
    .line 1320
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_item_pitch:F

    return v0
.end method

.method public getQuickScrollPopupPadding()I
    .locals 1

    .prologue
    .line 1296
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_popup_padding:I

    return v0
.end method

.method public getQuickScrollPopupTextBottomPadding()I
    .locals 1

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_popup_text_bottom_padding:I

    return v0
.end method

.method public getQuickScrollRootPitchRatio()F
    .locals 1

    .prologue
    .line 1324
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_pitch_ratio:F

    return v0
.end method

.method public getQuickScrollRootY()F
    .locals 1

    .prologue
    .line 1328
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_y:F

    return v0
.end method

.method public getQuickScrollRootZ()F
    .locals 1

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mQuickScroll:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_z:F

    return v0
.end method

.method public getScreenDensity()F
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->density:F

    return v0
.end method

.method public getScreenDensityDpi()I
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->densityDpi:I

    return v0
.end method

.method public getScreenHeight()F
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGlHeight:F

    return v0
.end method

.method public getScreenHeightPixel()I
    .locals 1

    .prologue
    .line 145
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinHeight:I

    return v0
.end method

.method public getScreenWidth()F
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGlWidth:F

    return v0
.end method

.method public getScreenWidthPixel()I
    .locals 1

    .prologue
    .line 137
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinWidth:I

    return v0
.end method

.method public getSearchResultHeight()F
    .locals 2

    .prologue
    .line 1262
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_search_result_title_height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getSelectionModeBarHeight()F
    .locals 2

    .prologue
    .line 1274
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-nez v0, :cond_0

    .line 1275
    const/4 v0, 0x0

    .line 1277
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->selectionModeBar_height:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public getSelectionModeLayoutHeight()I
    .locals 3

    .prologue
    .line 1344
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "pick-max-item"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisplaySelectAllButtonInLimitationPickMode:Z

    if-nez v0, :cond_1

    .line 1345
    :cond_0
    const/4 v0, 0x0

    .line 1347
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->selectionModeBar_height:I

    goto :goto_0
.end method

.method public getSpinnerOffset()I
    .locals 1

    .prologue
    .line 1340
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mScreenDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;->spinnerVerticalOffset:I

    return v0
.end method

.method public getThumbnailNewAlbumTextBoxVOffset(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 730
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTextBoxVOffsetPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewCameraIconMarginLeft()I
    .locals 1

    .prologue
    .line 767
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_camera_icon_margin_left:I

    return v0
.end method

.method public getThumbnailViewColumnItemCount(IZ)I
    .locals 5
    .param p1, "mode"    # I
    .param p2, "expanded"    # Z

    .prologue
    .line 389
    iget v2, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinWidth:I

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewLeftPaddingPixel(I)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewRightPaddingPixel(I)I

    move-result v3

    sub-int v1, v2, v3

    .line 390
    .local v1, "viewWidth":I
    if-nez p2, :cond_0

    .line 391
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidthPixel(I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 393
    :cond_0
    int-to-float v2, v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidthPixel(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewHorizontalGapPixel(I)I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 394
    .local v0, "nCount":I
    if-nez v0, :cond_1

    .line 395
    const/4 v0, 0x1

    .line 397
    :cond_1
    return v0
.end method

.method public getThumbnailViewDateviewLeftPadding()F
    .locals 2

    .prologue
    .line 763
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDateviewLeftPaddingPixel()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewDateviewLeftPaddingPixel()I
    .locals 1

    .prologue
    .line 759
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_dateview_padding_left:I

    return v0
.end method

.method public getThumbnailViewDefaultMode()I
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_default_mode:I

    return v0
.end method

.method public getThumbnailViewEndAffordanceRowDegree(I)I
    .locals 3
    .param p1, "row"    # I

    .prologue
    .line 1006
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewEndAffordanceRowDegreeCount()I

    move-result v0

    .line 1007
    .local v0, "count":I
    if-gez p1, :cond_0

    .line 1008
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    .line 1012
    :goto_0
    return v1

    .line 1009
    :cond_0
    if-ge p1, v0, :cond_1

    .line 1010
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree:[I

    aget v1, v1, p1

    goto :goto_0

    .line 1012
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree:[I

    add-int/lit8 v2, v0, -0x1

    aget v1, v1, v2

    goto :goto_0
.end method

.method public getThumbnailViewEndAffordanceRowDegreeCount()I
    .locals 1

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewVI:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree_count:I

    return v0
.end method

.method public getThumbnailViewHideSplitModeCount()I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_hide_split_mode_count:I

    return v0
.end method

.method public getThumbnailViewHorizontalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 430
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewHorizontalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewHorizontalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 426
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hgap:I

    return v0
.end method

.method public getThumbnailViewIconLeftPadding()I
    .locals 1

    .prologue
    .line 1270
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_icon_left_padding:I

    return v0
.end method

.method public getThumbnailViewLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 414
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hpadding_left:I

    return v0
.end method

.method public getThumbnailViewModeCount()I
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_mode_count:I

    return v0
.end method

.method public getThumbnailViewNewAlbumBackgroundColor()I
    .locals 1

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_background_color:I

    return v0
.end method

.method public getThumbnailViewNewAlbumBottomPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 722
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumBottomPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumBottomPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 718
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_vpadding_bottom:I

    return v0
.end method

.method public getThumbnailViewNewAlbumDividerHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 754
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDividerHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumDividerHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 750
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_divider_height:I

    return v0
.end method

.method public getThumbnailViewNewAlbumFontColor()I
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_color:I

    return v0
.end method

.method public getThumbnailViewNewAlbumFontSize()I
    .locals 2

    .prologue
    .line 642
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 643
    .local v0, "language":Ljava/lang/String;
    const-string/jumbo v1, "uk"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ru"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "pl"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "or"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "vi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "gu"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "bn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "kn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "in"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "pt"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "uz"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "ky"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 648
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_size_ru:I

    .line 649
    :goto_0
    return v1

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v1, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_font_size:I

    goto :goto_0
.end method

.method public getThumbnailViewNewAlbumHandPadding()I
    .locals 1

    .prologue
    .line 630
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_hand_padding:I

    return v0
.end method

.method public getThumbnailViewNewAlbumHandSize()I
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_hand_size:I

    return v0
.end method

.method public getThumbnailViewNewAlbumHeaderPaddingLeft()F
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_header_padding_left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumHeaderPaddingTop()F
    .locals 2

    .prologue
    .line 634
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_header_padding_top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 706
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 702
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_hpadding_left:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountBoxHeight()I
    .locals 1

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_height:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountBoxWidth(Landroid/content/Context;I)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "selectedCount"    # I

    .prologue
    .line 673
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->setThumbnailViewNewAlbumSelectCountBoxWidth(Landroid/content/Context;I)V

    .line 674
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_box_width:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountFontColor()I
    .locals 1

    .prologue
    .line 665
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_font_color:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountFontSize()I
    .locals 1

    .prologue
    .line 682
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_font_size:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountRightPadding()F
    .locals 2

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountRightPaddingPixel()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountRightPaddingPixel()I
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_hpadding_right:I

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountTopPadding()F
    .locals 2

    .prologue
    .line 690
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountTopPaddingPixel()I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumSelectCountTopPaddingPixel()I
    .locals 1

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_select_count_vpadding_top:I

    return v0
.end method

.method public getThumbnailViewNewAlbumTextBoxVOffsetPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 726
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_textbox_voffset:I

    return v0
.end method

.method public getThumbnailViewNewAlbumTextViewBottomPadding()I
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_bottom_padding:I

    return v0
.end method

.method public getThumbnailViewNewAlbumTextViewPadding()I
    .locals 1

    .prologue
    .line 653
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_padding:I

    return v0
.end method

.method public getThumbnailViewNewAlbumTextViewTopMargin()I
    .locals 1

    .prologue
    .line 661
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_album_text_view_top_margin:I

    return v0
.end method

.method public getThumbnailViewNewAlbumThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 746
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 742
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_thumbnail_height:I

    return v0
.end method

.method public getThumbnailViewNewAlbumThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 738
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 734
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_thumbnail_width:I

    return v0
.end method

.method public getThumbnailViewNewAlbumTopPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 714
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTopPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewNewAlbumTopPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 710
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_new_album_vpadding_top:I

    return v0
.end method

.method public getThumbnailViewNewMarkFontSize()I
    .locals 1

    .prologue
    .line 787
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_new_font_size:I

    return v0
.end method

.method public getThumbnailViewNewMarkHeight()I
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_height:I

    return v0
.end method

.method public getThumbnailViewNewMarkPadding()I
    .locals 1

    .prologue
    .line 791
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_new_padding:I

    return v0
.end method

.method public getThumbnailViewNewMarkRightMargin()I
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_right_margin:I

    return v0
.end method

.method public getThumbnailViewNewMarkTextTopMargin()I
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_text_top_margin:I

    return v0
.end method

.method public getThumbnailViewNewMarkTopMargin()I
    .locals 1

    .prologue
    .line 779
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_top_margin:I

    return v0
.end method

.method public getThumbnailViewNewMarkWidth()I
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_new_mark_width:I

    return v0
.end method

.method public getThumbnailViewPlayIconSize()I
    .locals 1

    .prologue
    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_play_icon_size:I

    return v0
.end method

.method public getThumbnailViewRightPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 422
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewRightPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 418
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_hpadding_right:I

    return v0
.end method

.method public getThumbnailViewRowItemCount(I)I
    .locals 5
    .param p1, "mode"    # I

    .prologue
    .line 401
    iget v2, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinHeight:I

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewTopPaddingPixel(I)I

    move-result v3

    sub-int v1, v2, v3

    .line 402
    .local v1, "viewHeight":I
    int-to-float v2, v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailHeightPixel(I)I

    move-result v3

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewVerticalGapPixel(I)I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 403
    .local v0, "nCount":I
    if-nez v0, :cond_0

    .line 404
    const/4 v0, 0x1

    .line 406
    :cond_0
    return v0
.end method

.method public getThumbnailViewShowSplitModeCount()I
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_show_split_mode_count:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumBackgroundHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 604
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumBackgroundHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 600
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_background_height:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumBackgroundWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 596
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumBackgroundWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 592
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_background_width:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumBoraderWidthPixel()I
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_border_width:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumColumnItemCount(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 497
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidthPixel(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumLeftPaddingPixel(I)I

    move-result v2

    sub-int v0, v1, v2

    .line 498
    .local v0, "nCount":I
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidthPixel(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumHorizontalGapPixel(I)I

    move-result v2

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    .line 499
    if-nez v0, :cond_0

    .line 500
    const/4 v0, 0x1

    .line 502
    :cond_0
    return v0
.end method

.method public getThumbnailViewSplitAlbumCountFontSize()I
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_count_font_size:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumDividerHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 612
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDividerHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumDividerHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 608
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_divider_height:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumFontColor()I
    .locals 1

    .prologue
    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_font_color:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumFontSize()I
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_font_size:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumHorizontalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 524
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumHorizontalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumHorizontalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 520
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_hgap:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumLeftPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 516
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumLeftPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumLeftPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 512
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_hpadding_left:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 588
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 584
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_height:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 580
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 576
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_width:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxXOffset(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 564
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxXOffsetPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxXOffsetPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 560
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_hoffset:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxYOffset(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 572
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffsetPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumNameTextboxYOffsetPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 568
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_name_textbox_voffset:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumRowItemCount(I)I
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 506
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundHeightPixel(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPaddingPixel(I)I

    move-result v2

    sub-int v0, v1, v2

    .line 507
    .local v0, "nCount":I
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I

    move-result v1

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumVerticalGapPixel(I)I

    move-result v2

    add-int/2addr v1, v2

    div-int/2addr v0, v1

    .line 508
    add-int/lit8 v1, v0, 0x1

    return v1
.end method

.method public getThumbnailViewSplitAlbumSelectedFontColor()I
    .locals 1

    .prologue
    .line 473
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_selected_font_color:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumSeparatorRatio()F
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_seprator_ratio:F

    return v0
.end method

.method public getThumbnailViewSplitAlbumSeparatorRatioDividerHeight()F
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_seprator_ratio_divider_height:F

    return v0
.end method

.method public getThumbnailViewSplitAlbumThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 556
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 552
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_thumbnail_height:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 548
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 544
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_thumbnail_width:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumTitlePaddingLeft()I
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_album_title_padding_left:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumTopPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 532
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumTopPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 528
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_vpadding_top:I

    return v0
.end method

.method public getThumbnailViewSplitAlbumVerticalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 540
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumVerticalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewSplitAlbumVerticalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 536
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_split_album_vgap:I

    return v0
.end method

.method public getThumbnailViewSplitExtraGapPixel()I
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_extra_gap:I

    return v0
.end method

.method public getThumbnailViewSplitFocusedArrowHeightPixel()I
    .locals 1

    .prologue
    .line 803
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_focused_arrow_height:I

    return v0
.end method

.method public getThumbnailViewSplitFocusedArrowWidthPixel()I
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->thumbnail_view_split_focused_arrow_width:I

    return v0
.end method

.method public getThumbnailViewThumbnailHeight(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 462
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailHeightPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewThumbnailHeightPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 458
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_thumbnail_height:I

    return v0
.end method

.method public getThumbnailViewThumbnailWidth(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidthPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewThumbnailWidthPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_thumbnail_width:I

    return v0
.end method

.method public getThumbnailViewTopPadding(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 438
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewTopPaddingPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewTopPaddingPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_vpadding_top:I

    return v0
.end method

.method public getThumbnailViewVerticalGap(I)F
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 446
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewVerticalGapPixel(I)I

    move-result v0

    int-to-float v0, v0

    iget v1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewVerticalGapPixel(I)I
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mThumbnailViewDimension:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;->mThumbnailViewSpec:[Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDimensionIndex(I)I

    move-result v1

    aget-object v0, v0, v1

    iget v0, v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension$Spec;->thumbnail_view_vgap:I

    return v0
.end method

.method public getWin2WorldRatio()F
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    return v0
.end method

.method public getWindowCoordinateValue(F)F
    .locals 1
    .param p1, "worldVal"    # F

    .prologue
    .line 1122
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    div-float v0, p1, v0

    return v0
.end method

.method public getWorld2WinRatio()F
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGl2WinRatio:F

    return v0
.end method

.method public getWorldCoordinateValue(F)F
    .locals 1
    .param p1, "winVal"    # F

    .prologue
    .line 1118
    iget v0, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWin2GlRatio:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public setDimensionUtil(FFII)V
    .locals 2
    .param p1, "glWidth"    # F
    .param p2, "glHeight"    # F
    .param p3, "winWidth"    # I
    .param p4, "winHeight"    # I

    .prologue
    const/4 v1, 0x0

    .line 63
    cmpl-float v0, p1, v1

    if-eqz v0, :cond_0

    cmpl-float v0, p2, v1

    if-nez v0, :cond_1

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 66
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGlWidth:F

    .line 67
    iput p2, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mGlHeight:F

    .line 68
    iput p3, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinWidth:I

    .line 69
    iput p4, p0, Lcom/sec/samsung/gallery/util/DimensionUtil;->mWinHeight:I

    .line 70
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->calcParameters()V

    .line 71
    return-void
.end method
