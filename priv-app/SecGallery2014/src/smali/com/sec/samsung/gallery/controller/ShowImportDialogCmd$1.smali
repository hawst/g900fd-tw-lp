.class Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;
.super Ljava/lang/Object;
.source "ShowImportDialogCmd.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->handleImportMedias()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 95
    const/4 v0, 0x0

    .line 96
    .local v0, "ret":Z
    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "handleOperation start"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/MtpImage;

    if-eqz v1, :cond_0

    .line 98
    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Call import"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    check-cast p1, Lcom/sec/android/gallery3d/data/MtpImage;

    .end local p1    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MtpImage;->Import()Z

    move-result v0

    .line 101
    :cond_0
    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 106
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 4
    .param p1, "result"    # Z

    .prologue
    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 117
    if-eqz p1, :cond_0

    const v0, 0x7f0e009b

    .line 118
    .local v0, "strId":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 119
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    return-void

    .line 117
    .end local v0    # "strId":I
    :cond_0
    const v0, 0x7f0e009c

    goto :goto_0
.end method

.method public onProgress(II)V
    .locals 1
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowImportDialogCmd;)Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->updateProgress(II)V

    .line 112
    return-void
.end method
