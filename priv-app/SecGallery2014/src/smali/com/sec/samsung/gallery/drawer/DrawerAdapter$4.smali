.class Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;
.super Ljava/lang/Object;
.source "DrawerAdapter.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->notifyDataSetChanged()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V
    .locals 0

    .prologue
    .line 331
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 335
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v6

    monitor-enter v6

    .line 336
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$400(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    .line 337
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 338
    .local v1, "groupSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 339
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/drawer/Group;

    .line 341
    .local v0, "group":Lcom/sec/samsung/gallery/drawer/Group;
    if-eqz v0, :cond_0

    .line 342
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getChildrenCount()I

    move-result v3

    .line 344
    .local v3, "itemSize":I
    if-lez v3, :cond_0

    .line 346
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$400(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getTitleItem()Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$500(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$600(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Landroid/content/res/Resources;

    move-result-object v5

    const v7, 0x7f0e04cb

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/Group;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 338
    .end local v3    # "itemSize":I
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 349
    .restart local v3    # "itemSize":I
    :cond_1
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, v3, :cond_0

    .line 351
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$400(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/Group;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    add-int/lit8 v5, v3, -0x1

    if-ne v4, v5, :cond_2

    .line 353
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/Group;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v5

    const/4 v7, 0x1

    invoke-virtual {v5, v7}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->setGroupLastItem(Z)Z

    .line 349
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 355
    :cond_2
    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/Group;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v5

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->setGroupLastItem(Z)Z

    goto :goto_2

    .line 361
    .end local v0    # "group":Lcom/sec/samsung/gallery/drawer/Group;
    .end local v1    # "groupSize":I
    .end local v2    # "i":I
    .end local v3    # "itemSize":I
    .end local v4    # "j":I
    :catchall_0
    move-exception v5

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .restart local v1    # "groupSize":I
    .restart local v2    # "i":I
    :cond_3
    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    iget-object v5, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    # invokes: Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V
    invoke-static {v5}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->access$701(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V

    .line 363
    return-void
.end method
