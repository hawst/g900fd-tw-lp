.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;
.super Ljava/lang/Object;
.source "GallerySearchView.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)V
    .locals 0

    .prologue
    .line 450
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 459
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/16 v1, 0x8

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setSearchHintTextAndIcon(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;I)V

    .line 464
    :goto_0
    return-void

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    const/4 v1, 0x0

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setSearchHintTextAndIcon(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 452
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "after"    # I

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->onTextChanged(Ljava/lang/CharSequence;)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$1400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;Ljava/lang/CharSequence;)V

    .line 456
    return-void
.end method
