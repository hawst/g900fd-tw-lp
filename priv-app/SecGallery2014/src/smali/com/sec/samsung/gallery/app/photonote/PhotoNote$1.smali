.class Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;
.super Landroid/content/BroadcastReceiver;
.source "PhotoNote.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/PhotoNote;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x0

    .line 99
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 100
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 101
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 102
    .local v1, "uri":Landroid/net/Uri;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 103
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 104
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V
    invoke-static {v2, v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$100(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Z)V

    .line 111
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 107
    .restart local v1    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->mFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$000(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 108
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/PhotoNote$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->finish_note(Z)V
    invoke-static {v2, v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;->access$100(Lcom/sec/samsung/gallery/app/photonote/PhotoNote;Z)V

    goto :goto_0
.end method
