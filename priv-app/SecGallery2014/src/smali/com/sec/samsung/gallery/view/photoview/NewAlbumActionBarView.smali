.class public Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "NewAlbumActionBarView.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mNewAlbumSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSelectionText:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mNewAlbumSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;
    .param p1, "x1"    # I

    .prologue
    .line 18
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setSelectedItemCount(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v3, 0x0

    .line 30
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 31
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v2, 0x7f120045

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 33
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mNewAlbumSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 34
    .local v1, "selectedCount":I
    const v4, 0x7f0f02de

    if-lez v1, :cond_0

    const/4 v2, 0x1

    :goto_0
    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 35
    const v2, 0x7f0f0022

    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 36
    return-void

    :cond_0
    move v2, v3

    .line 34
    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 40
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 48
    :goto_0
    return-void

    .line 42
    :sswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_CANCEL:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_ALBUM_DONE:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 40
    :sswitch_data_0
    .sparse-switch
        0x7f0f0022 -> :sswitch_0
        0x7f0f02de -> :sswitch_1
    .end sparse-switch
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;I)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "numberOfItemsSelected"    # I

    .prologue
    .line 52
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumActionBarView;->setTitle(I)V

    .line 53
    return-void
.end method
