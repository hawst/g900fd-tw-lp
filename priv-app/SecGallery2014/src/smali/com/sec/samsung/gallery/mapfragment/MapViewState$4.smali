.class Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;
.super Landroid/database/DataSetObserver;
.source "MapViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V
    .locals 0

    .prologue
    .line 634
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 638
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 639
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 658
    :cond_0
    :goto_0
    return-void

    .line 642
    :cond_1
    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$500()Ljava/lang/String;

    move-result-object v0

    const-string v1, "DataSetObserver is called"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    if-eqz v0, :cond_2

    .line 645
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->cancel(Z)Z

    .line 646
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iput-object v3, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    .line 648
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$2500(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_3

    .line 649
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 650
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 653
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;)V

    iput-object v1, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    .line 654
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 656
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setUpMapIfNeeded()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    goto :goto_0
.end method
