.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;
.super Ljava/lang/Object;
.source "TimeViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V
    .locals 2
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "status"    # I
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I

    .prologue
    .line 659
    packed-switch p2, :pswitch_data_0

    .line 670
    :goto_0
    return-void

    .line 661
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$4500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const-string/jumbo v1, "timeViewColCnt"

    invoke-static {v0, v1, p3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 664
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startDetailViewInUIThread(II)V
    invoke-static {v0, p3, p4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$3700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V

    goto :goto_0

    .line 667
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setThumbReslevel(I)V

    goto :goto_0

    .line 659
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
