.class public Lcom/sec/samsung/gallery/glview/GlRepositionAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlRepositionAnim.java"


# instance fields
.field private mNx:[F

.field private mNy:[F

.field private mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 11
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 8
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNx:[F

    .line 9
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNy:[F

    .line 12
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->setDuration(J)V

    .line 13
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 14
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 7
    .param p1, "ratio"    # F

    .prologue
    .line 35
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-ge v0, v4, :cond_2

    .line 36
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v0

    .line 37
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v3, :cond_0

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v4, v5

    if-nez v4, :cond_1

    .line 35
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 39
    :cond_1
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLx:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNx:[F

    aget v5, v5, v0

    iget v6, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLx:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v1, v4, v5

    .line 40
    .local v1, "nx":F
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLy:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNy:[F

    aget v5, v5, v0

    iget v6, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mLy:F

    sub-float/2addr v5, v6

    mul-float/2addr v5, p1

    add-float v2, v4, v5

    .line 41
    .local v2, "ny":F
    const/4 v4, 0x0

    const/4 v5, 0x3

    invoke-virtual {v3, v1, v2, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFFI)V

    goto :goto_1

    .line 43
    .end local v1    # "nx":F
    .end local v2    # "ny":F
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNx:[F

    .line 47
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNy:[F

    .line 48
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 49
    return-void
.end method

.method public startAnimation([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 4
    .param p1, "objects"    # [Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 17
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 18
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v0, v2

    .line 19
    .local v0, "count":I
    new-array v2, v0, [F

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNx:[F

    .line 20
    new-array v2, v0, [F

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNy:[F

    .line 22
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 23
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v2, v1

    if-nez v2, :cond_0

    .line 22
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 25
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNx:[F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getX()F

    move-result v3

    aput v3, v2, v1

    .line 26
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mNy:[F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->mObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v3

    aput v3, v2, v1

    goto :goto_1

    .line 28
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->start()V

    .line 29
    return-void
.end method
