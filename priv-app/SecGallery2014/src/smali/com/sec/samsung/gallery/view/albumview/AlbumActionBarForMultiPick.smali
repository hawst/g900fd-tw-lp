.class public Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "AlbumActionBarForMultiPick.java"


# instance fields
.field private mIsPersonPick:Z

.field private mMenu:Landroid/view/Menu;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 22
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mIsPersonPick:Z

    .line 26
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 27
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.action.PERSON_PICK"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mIsPersonPick:Z

    .line 28
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mIsPersonPick:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0024

    const v5, 0x7f0f0022

    const/4 v4, 0x2

    .line 56
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 57
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f120030

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 59
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 60
    .local v1, "done":Landroid/view/MenuItem;
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 61
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 62
    invoke-static {p1, v6, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 63
    invoke-static {p1, v5, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 68
    :goto_0
    return-void

    .line 65
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 66
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x0

    .line 88
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 105
    :goto_0
    return-void

    .line 90
    :sswitch_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 93
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "MULTIPLE_PICKER_SELECTION_COMPLETED"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 97
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 98
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto :goto_0

    .line 101
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 88
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0022 -> :sswitch_1
        0x7f0f0024 -> :sswitch_0
        0x7f0f0289 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v5, 0x7f0f0024

    const v4, 0x7f0f0022

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    .line 73
    const v3, 0x7f0f02cf

    invoke-interface {p1, v3, v1}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 74
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 75
    .local v0, "count":I
    invoke-static {p1, v5, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 76
    if-lez v0, :cond_0

    :goto_0
    invoke-static {p1, v5, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 78
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 79
    const v1, 0x7f0f0284

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 80
    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 84
    :goto_1
    return-void

    :cond_0
    move v1, v2

    .line 76
    goto :goto_0

    .line 82
    :cond_1
    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForMultiPick;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 52
    return-void
.end method
