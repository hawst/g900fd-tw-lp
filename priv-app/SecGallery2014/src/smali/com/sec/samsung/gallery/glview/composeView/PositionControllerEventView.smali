.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;
.super Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.source "PositionControllerEventView.java"


# static fields
.field private static EVENTVIEW_THUMBNAIL_RATIO:F

.field private static EVENTVIEW_TITLE_HEIGHT:I

.field private static GRID_COLCNT_LAND:[I

.field private static GRID_COLCNT_PORT:[I

.field private static GRID_THM_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 15
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_PORT:[I

    .line 16
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_LAND:[I

    .line 17
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_THM_TYPES:[I

    .line 19
    const v0, 0x3ee66666    # 0.45f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_THUMBNAIL_RATIO:F

    .line 20
    const/16 v0, 0x6e

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_TITLE_HEIGHT:I

    return-void

    .line 15
    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 16
    :array_1
    .array-data 4
        0x2
        0x2
    .end array-data

    .line 17
    :array_2
    .array-data 4
        0x3
        0x3
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;-><init>()V

    return-void
.end method

.method private findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 3
    .param p1, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    .line 328
    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupVGap:F

    sub-float v0, v1, v2

    .line 329
    .local v0, "scrollAmount":F
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    sub-float/2addr v0, v1

    .line 331
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 332
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    return-object v1
.end method

.method private fitToLine()V
    .locals 7

    .prologue
    .line 336
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-gez v5, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 344
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v5, 0x10

    .line 345
    .local v0, "groupIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v6, 0xffff

    and-int v2, v5, v6

    .line 347
    .local v2, "itemIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v5, v0

    .line 349
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v2, v5, :cond_0

    .line 352
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float v3, v5, v6

    .line 354
    .local v3, "scrollAccu":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v5, v6

    cmpg-float v5, v3, v5

    if-gez v5, :cond_2

    .line 355
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    sub-float v4, v3, v5

    .line 356
    .local v4, "scrollAmount":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0

    .line 357
    .end local v4    # "scrollAmount":F
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    add-float/2addr v5, v6

    cmpl-float v5, v3, v5

    if-lez v5, :cond_0

    .line 358
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    sub-float v4, v3, v5

    .line 359
    .restart local v4    # "scrollAmount":F
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v5, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_0
.end method

.method private getKey(II)I
    .locals 1
    .param p1, "groupIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 397
    shl-int/lit8 v0, p1, 0x10

    add-int/2addr v0, p2

    return v0
.end method

.method private getPhotoLineCount(I)I
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 513
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v0, :cond_0

    .line 514
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_PORT:[I

    aget v0, v0, p1

    .line 516
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_LAND:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method public static getThumbSizeType(I)I
    .locals 1
    .param p0, "level"    # I

    .prologue
    .line 23
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_THM_TYPES:[I

    aget v0, v0, p0

    return v0
.end method

.method private updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 364
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    if-nez v2, :cond_1

    .line 393
    :cond_0
    :goto_0
    return-object v1

    .line 367
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v2, 0x10

    .line 368
    .local v0, "groupIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 370
    packed-switch p1, :pswitch_data_0

    .line 392
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->getKey(II)I

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 393
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v1, v0

    goto :goto_0

    .line 372
    :pswitch_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 373
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/2addr v0, v1

    goto :goto_1

    .line 377
    :pswitch_1
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 378
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 382
    :pswitch_2
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    sub-int v1, v0, v1

    if-ltz v1, :cond_2

    .line 383
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    sub-int/2addr v0, v1

    goto :goto_1

    .line 387
    :pswitch_3
    add-int/lit8 v1, v0, -0x1

    if-ltz v1, :cond_2

    .line 388
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 370
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v5, 0x0

    .line 470
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    if-eqz v1, :cond_0

    .line 471
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSelected(Z)V

    .line 472
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDisabled:Z

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDimState(Z)V

    .line 475
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->setThumbObjectCanvas(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 492
    :goto_0
    return-void

    .line 479
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 480
    .local v0, "cropRect":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 481
    iget v1, v0, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    invoke-virtual {p1, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTexCoords(FFFF)V

    .line 482
    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 483
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    .line 485
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 487
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0

    .line 489
    :cond_2
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 490
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0
.end method

.method public calcItemPosition(ILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F
    .locals 13
    .param p1, "index"    # I
    .param p2, "groupInfo"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x0

    .line 108
    iget-object v6, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 109
    .local v6, "cx":[F
    iget-object v7, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 110
    .local v7, "cy":[F
    iget-object v5, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    .line 111
    .local v5, "cw":[F
    iget-object v3, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    .line 112
    .local v3, "ch":[F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    .line 114
    .local v4, "colCount":I
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 115
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    .line 117
    rem-int v8, p1, v4

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mGroupCol:I

    .line 118
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    div-int v8, p1, v8

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mGroupRow:I

    .line 120
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapW:F

    add-int/lit8 v10, v4, -0x1

    int-to-float v10, v10

    mul-float/2addr v9, v10

    sub-float/2addr v8, v9

    int-to-float v9, v4

    div-float v2, v8, v9

    .line 121
    .local v2, "SW":F
    sget v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_THUMBNAIL_RATIO:F

    mul-float v1, v2, v8

    .line 122
    .local v1, "SH":F
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapH:F

    add-float v0, v1, v8

    .line 123
    .local v0, "NH":F
    iput v0, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    .line 125
    iput v12, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    .line 126
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    neg-float v8, v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget v9, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRow:I

    int-to-float v9, v9

    mul-float/2addr v9, v0

    sub-float/2addr v8, v9

    iput v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCy:F

    .line 128
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-lez v8, :cond_0

    .line 129
    aput v12, v6, v11

    .line 130
    aput v12, v7, v11

    .line 131
    aput v2, v5, v11

    .line 132
    aput v1, v3, v11

    .line 135
    :cond_0
    iget v8, p2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleTextMarginTop:F

    add-float/2addr v8, v9

    return v8
.end method

.method public doQuickScroll(I)V
    .locals 4
    .param p1, "groupIndex"    # I

    .prologue
    .line 422
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v2, p1

    .line 425
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupVGap:F

    sub-float v1, v2, v3

    .line 426
    .local v1, "scrolAmount":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    :goto_0
    sub-float/2addr v1, v2

    .line 427
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollableMax:F

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 428
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    .line 432
    :goto_1
    return-void

    .line 426
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    goto :goto_0

    .line 430
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->doScroll(F)V

    goto :goto_1
.end method

.method public getCenteredScroll(F)F
    .locals 3
    .param p1, "scroll"    # F

    .prologue
    .line 157
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v1, p1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    add-float v0, v1, v2

    .line 159
    .local v0, "resScroll":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 160
    const/4 v0, 0x0

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 161
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollableMax:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 162
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollableMax:F

    goto :goto_0
.end method

.method public getScrollForIndex(I)F
    .locals 7
    .param p1, "index"    # I

    .prologue
    const/4 v4, 0x0

    .line 143
    shr-int/lit8 v0, p1, 0x10

    .line 144
    .local v0, "albumIdx":I
    const v5, 0xffff

    and-int v2, p1, v5

    .line 145
    .local v2, "photoIdx":I
    if-ltz p1, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    if-lt v0, v5, :cond_1

    .line 152
    :cond_0
    :goto_0
    return v4

    .line 148
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v5, v0

    .line 149
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v5, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    if-lez v5, :cond_0

    .line 151
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    div-int v3, v2, v4

    .line 152
    .local v3, "row":I
    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    int-to-float v5, v3

    iget v6, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    goto :goto_0
.end method

.method public moveTo(I)Z
    .locals 10
    .param p1, "direction"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 267
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v3, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 269
    .local v3, "lastFocus":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    if-nez v8, :cond_5

    .line 270
    const/4 v8, 0x4

    if-eq p1, v8, :cond_1

    .line 323
    :cond_0
    :goto_0
    return v6

    .line 273
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v7, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 274
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGrpContentStart:I

    iput v9, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    .line 275
    const/4 p1, 0x0

    .line 303
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v8, :cond_0

    .line 306
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->updateFocused(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-result-object v2

    .line 308
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-eq v3, v8, :cond_4

    .line 309
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v8, :cond_0

    if-eqz v2, :cond_0

    .line 312
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v8, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 313
    .local v5, "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {p0, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 315
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->fitToLine()V

    .line 316
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v6, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 318
    .local v4, "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v4, :cond_3

    .line 319
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->findNewObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v4

    .line 321
    :cond_3
    invoke-virtual {p0, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .end local v4    # "newThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_4
    move v6, v7

    .line 323
    goto :goto_0

    .line 276
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_5
    if-eq p1, v9, :cond_6

    const/4 v8, 0x5

    if-ne p1, v8, :cond_2

    .line 277
    :cond_6
    if-ne p1, v9, :cond_a

    .line 278
    const/4 v0, 0x0

    .line 279
    .local v0, "column":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v8, :cond_7

    .line 280
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v8, v8

    if-lez v8, :cond_7

    .line 281
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v8, v8, v6

    iget v0, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    .line 284
    :cond_7
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ge v8, v0, :cond_9

    .line 285
    const/4 v1, 0x1

    .line 292
    .end local v0    # "column":I
    .local v1, "focusDisappeared":Z
    :goto_1
    if-eqz v1, :cond_2

    .line 293
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v7, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 294
    .restart local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v5, :cond_8

    .line 295
    invoke-virtual {p0, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 297
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean v6, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsFocusVisible:Z

    .line 298
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    const/4 v8, -0x1

    iput v8, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    goto/16 :goto_0

    .line 287
    .end local v1    # "focusDisappeared":Z
    .end local v5    # "oldThumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v0    # "column":I
    :cond_9
    const/4 v1, 0x0

    .restart local v1    # "focusDisappeared":Z
    goto :goto_1

    .line 289
    .end local v0    # "column":I
    .end local v1    # "focusDisappeared":Z
    :cond_a
    const/4 v1, 0x1

    .restart local v1    # "focusDisappeared":Z
    goto :goto_1
.end method

.method public onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v1, 0x0

    .line 522
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 523
    .local v0, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v0, :cond_0

    .line 524
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local v0    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 525
    .restart local v0    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v4, 0x4b0

    const/16 v5, 0x6e

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    .line 526
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 527
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setObjectListeners(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;I)V

    .line 529
    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mListenerMapClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 531
    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 532
    iput-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 535
    :cond_0
    return-void
.end method

.method public resetAttributes(IZ)V
    .locals 8
    .param p1, "option"    # I
    .param p2, "portraitMode"    # Z

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 36
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mLcdRect:Landroid/graphics/Rect;

    .line 37
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMarginTopPixel:I

    .line 38
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 39
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMarginTopPixel:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d02e0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMarginTopPixel:I

    .line 42
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mResource:Landroid/content/res/Resources;

    const v5, 0x7f0c0057

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    sput v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_TITLE_HEIGHT:I

    .line 43
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->resetValues(Z)V

    .line 45
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d02b0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 46
    .local v0, "itemGap":I
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->convX(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapW:F

    .line 47
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->convY(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapH:F

    .line 49
    sget v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_TITLE_HEIGHT:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->convY(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-eqz v4, :cond_2

    .line 52
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_PORT:[I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mLevel:I

    aget v4, v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    .line 64
    :cond_1
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapW:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    int-to-float v5, v5

    div-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    .line 65
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    sget v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_THUMBNAIL_RATIO:F

    mul-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    .line 66
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleTextMarginTopPixel:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->convY(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleTextMarginTop:F

    .line 68
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->rConvX(F)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleCanvsW:I

    .line 69
    sget v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->EVENTVIEW_TITLE_HEIGHT:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleCanvsH:I

    .line 71
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mSpaceWidth:F

    neg-float v4, v4

    div-float/2addr v4, v7

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargLeft:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    div-float/2addr v5, v7

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemSx:F

    .line 72
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mSpaceHeight:F

    div-float/2addr v4, v7

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargTop:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemSy:F

    .line 74
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->resetItemMaxSize()V

    .line 76
    return-void

    .line 54
    :cond_2
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_COLCNT_LAND:[I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mLevel:I

    aget v4, v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    .line 55
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mLcdRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->convX(I)F

    move-result v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    int-to-float v5, v5

    div-float v1, v4, v5

    .line 57
    .local v1, "minW":F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    int-to-float v5, v5

    mul-float/2addr v5, v1

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 58
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    .line 59
    .local v3, "originalPhotoLineCount":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    div-float/2addr v4, v1

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 60
    .local v2, "newPhotoLineCount":I
    const/4 v4, 0x1

    invoke-static {v2, v4, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    goto :goto_0
.end method

.method protected resetItemMaxSize()V
    .locals 5

    .prologue
    .line 496
    const/4 v1, 0x0

    .line 497
    .local v1, "tempMaxSize":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mIsEasyMode:Z

    if-eqz v2, :cond_0

    .line 498
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    .line 507
    :goto_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->rConvX(F)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemMaxW:F

    .line 508
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02ae

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemMaxH:F

    .line 510
    return-void

    .line 500
    :cond_0
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->getPhotoLineCount(I)I

    move-result v0

    .line 501
    .local v0, "minPhotoCount":I
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    if-lt v0, v2, :cond_1

    .line 502
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    goto :goto_0

    .line 504
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapW:F

    add-int/lit8 v4, v0, -0x1

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    int-to-float v3, v0

    div-float v1, v2, v3

    goto :goto_0
.end method

.method public resetPosition()V
    .locals 6

    .prologue
    .line 81
    const/4 v3, 0x0

    .line 83
    .local v3, "scrollable":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    if-ge v1, v4, :cond_3

    .line 84
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v0, v4, v1

    .line 85
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-nez v0, :cond_0

    .line 83
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->calcItemPosition(ILcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;)F

    move-result v2

    .line 89
    .local v2, "scrollUnit":F
    iput v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    .line 90
    iput v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    .line 91
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    rem-int v4, v1, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/lit8 v5, v5, -0x1

    if-eq v4, v5, :cond_1

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_2

    .line 92
    :cond_1
    add-float/2addr v3, v2

    .line 94
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemGapW:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    rem-int v5, v1, v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    goto :goto_1

    .line 96
    .end local v0    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v2    # "scrollUnit":F
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    cmpg-float v4, v3, v4

    if-gtz v4, :cond_4

    .line 97
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollableMax:F

    .line 100
    :goto_2
    return-void

    .line 99
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    sub-float v4, v3, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollableMax:F

    goto :goto_2
.end method

.method public resetPosition(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 103
    return-void
.end method

.method public setTitleFocusBorderVisible(II)V
    .locals 4
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 403
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    shl-int/lit8 v3, p1, 0x10

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 405
    .local v0, "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_0

    .line 406
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 407
    .local v1, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocusBorderVisible(Z)V

    .line 408
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->updateBorder(I)V

    .line 412
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    shl-int/lit8 v3, p2, 0x10

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 413
    .restart local v0    # "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_1

    .line 414
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 415
    .restart local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocusBorderVisible(Z)V

    .line 416
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->updateBorder(I)V

    .line 418
    .end local v1    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method

.method public setToCurrentCtrl()V
    .locals 3

    .prologue
    .line 29
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->GRID_THM_TYPES:[I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mLevel:I

    aget v0, v1, v2

    .line 30
    .local v0, "thmLevel":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v1, :cond_0

    .line 31
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setThumbReslevel(I)V

    .line 32
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setToCurrentCtrl()V

    .line 33
    return-void
.end method

.method public setVisibleRange()V
    .locals 15

    .prologue
    .line 171
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargBtm:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    add-float v0, v12, v13

    .line 173
    .local v0, "bottomScroll":F
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    add-int/lit8 v9, v12, -0x1

    .line 174
    .local v9, "lastGrp":I
    move v8, v9

    .line 175
    .local v8, "inGrpStart":I
    move v6, v9

    .line 176
    .local v6, "inGrpEnd":I
    move v5, v9

    .line 177
    .local v5, "inCntGrpStart":I
    move v4, v9

    .line 180
    .local v4, "inCntGrpEnd":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    if-ge v3, v12, :cond_3

    .line 181
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v3

    .line 183
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    sub-float v10, v12, v13

    .line 184
    .local v10, "scrollAmount":F
    if-ge v3, v5, :cond_0

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraTop:F

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_0

    .line 185
    move v5, v3

    .line 187
    :cond_0
    if-ge v3, v8, :cond_1

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargTop:F

    neg-float v12, v12

    cmpg-float v12, v12, v10

    if-gtz v12, :cond_1

    .line 188
    move v8, v3

    .line 190
    :cond_1
    if-ne v6, v9, :cond_2

    cmpl-float v12, v10, v0

    if-ltz v12, :cond_2

    .line 191
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/2addr v12, v3

    add-int/lit8 v6, v12, -0x1

    .line 192
    if-le v6, v9, :cond_2

    move v6, v9

    .line 194
    :cond_2
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraBtm:F

    cmpl-float v12, v10, v12

    if-ltz v12, :cond_4

    .line 195
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupLineCount:I

    add-int/2addr v12, v3

    add-int/lit8 v12, v12, -0x1

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroupCount:I

    add-int/lit8 v13, v13, -0x1

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 200
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v10    # "scrollAmount":F
    :cond_3
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v8

    .line 201
    .restart local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    sub-float v1, v12, v13

    .line 202
    .local v1, "exceedAmount":F
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargTop:F

    neg-float v12, v12

    cmpl-float v12, v1, v12

    if-lez v12, :cond_5

    .line 203
    const/4 v7, 0x0

    .line 213
    .local v7, "inGrpIndex":I
    :goto_1
    shl-int/lit8 v12, v8, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGrpActiveStart:I

    .line 215
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v6

    .line 216
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    sub-float v1, v12, v13

    .line 217
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargBtm:F

    add-float/2addr v12, v13

    cmpg-float v12, v1, v12

    if-gtz v12, :cond_7

    .line 218
    iget v7, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 228
    :goto_2
    shl-int/lit8 v12, v6, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGrpActiveEnd:I

    .line 230
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v5

    .line 231
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    sub-float v1, v12, v13

    .line 232
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraTop:F

    cmpl-float v12, v1, v12

    if-lez v12, :cond_9

    .line 233
    const/4 v7, 0x0

    .line 243
    :goto_3
    shl-int/lit8 v12, v5, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGrpContentStart:I

    .line 245
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v2, v12, v4

    .line 246
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v12, v13

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mScrollable:F

    sub-float v1, v12, v13

    .line 247
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraBtm:F

    cmpg-float v12, v1, v12

    if-gtz v12, :cond_b

    .line 248
    iget v7, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 258
    :goto_4
    shl-int/lit8 v12, v4, 0x10

    or-int/2addr v12, v7

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGrpContentEnd:I

    .line 259
    return-void

    .line 180
    .end local v1    # "exceedAmount":F
    .end local v7    # "inGrpIndex":I
    .restart local v10    # "scrollAmount":F
    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 205
    .end local v10    # "scrollAmount":F
    .restart local v1    # "exceedAmount":F
    :cond_5
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargTop:F

    add-float/2addr v12, v1

    neg-float v11, v12

    .line 206
    .local v11, "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_6

    .line 207
    const/4 v7, 0x0

    .restart local v7    # "inGrpIndex":I
    goto :goto_1

    .line 209
    .end local v7    # "inGrpIndex":I
    :cond_6
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v7, v12

    .line 210
    .restart local v7    # "inGrpIndex":I
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v7, v12

    goto :goto_1

    .line 220
    .end local v11    # "scrollExc":F
    :cond_7
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mValidH:F

    sub-float v13, v1, v13

    iget v14, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mMargBtm:F

    sub-float/2addr v13, v14

    sub-float v11, v12, v13

    .line 221
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_8

    .line 222
    const/4 v7, 0x1

    goto :goto_2

    .line 224
    :cond_8
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    const/high16 v13, 0x3f800000    # 1.0f

    add-float/2addr v12, v13

    float-to-int v7, v12

    .line 225
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v7

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_2

    .line 235
    .end local v11    # "scrollExc":F
    :cond_9
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraTop:F

    sub-float v11, v12, v1

    .line 236
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_a

    .line 237
    const/4 v7, 0x0

    goto :goto_3

    .line 239
    :cond_a
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    float-to-int v7, v12

    .line 240
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v7, v12

    goto :goto_3

    .line 250
    .end local v11    # "scrollExc":F
    :cond_b
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mExtraBtm:F

    sub-float v13, v1, v13

    sub-float v11, v12, v13

    .line 251
    .restart local v11    # "scrollExc":F
    const/4 v12, 0x0

    cmpg-float v12, v11, v12

    if-gtz v12, :cond_c

    .line 252
    const/4 v7, 0x1

    goto :goto_4

    .line 254
    :cond_c
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemHG:F

    div-float v12, v11, v12

    const/high16 v13, 0x3f800000    # 1.0f

    add-float/2addr v12, v13

    float-to-int v7, v12

    .line 255
    iget v12, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCol:I

    mul-int/2addr v12, v7

    iget v13, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    invoke-static {v12, v13}, Ljava/lang/Math;->min(II)I

    move-result v7

    goto :goto_4
.end method

.method protected updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z
    .locals 14
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 439
    iget v13, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 440
    .local v13, "index":I
    shr-int/lit8 v1, v13, 0x10

    .line 441
    .local v1, "albumIdx":I
    const v0, 0xffff

    and-int v2, v13, v0

    .line 442
    .local v2, "photoIdx":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v0, v0

    if-lt v1, v0, :cond_1

    .line 465
    :cond_0
    :goto_0
    return v3

    .line 444
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v12, v0, v1

    .line 445
    .local v12, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v0, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v2, v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    aget v4, v4, v2

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 448
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    aget v4, v4, v2

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 449
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mDisplaySelectedCount:Z

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispSelectCnt:Z

    .line 451
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 452
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 453
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getFocusBorderVisible()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 454
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 456
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    invoke-virtual {p1, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSize(FF)V

    .line 457
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    neg-int v0, v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVertexRotation(I)V

    .line 459
    iget-object v10, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mAlbumTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 460
    .local v10, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleCanvsW:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleCanvsH:I

    invoke-virtual {v10, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->updateCanvas(II)V

    .line 461
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemH:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    add-float/2addr v0, v4

    neg-float v0, v0

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    invoke-virtual {v10, v6, v0, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 462
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mItemW:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mTitleH:F

    invoke-virtual {v10, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 463
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v6, -0x1

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move v5, v1

    move v9, v3

    invoke-virtual/range {v4 .. v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v11

    .line 464
    .local v11, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 465
    const/4 v3, 0x1

    goto :goto_0
.end method
