.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "TimeViewActionBarForMultiPick.java"


# instance fields
.field protected mCount:I

.field private mIsDoneEnabled:Z

.field private mMenu:Landroid/view/Menu;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 25
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z

    .line 26
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setSelectAllButtonTTS(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z

    return v0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0024

    const v5, 0x7f0f0022

    const/4 v4, 0x2

    .line 82
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v2

    .line 83
    .local v2, "inflater":Landroid/view/MenuInflater;
    const v3, 0x7f120030

    invoke-virtual {v2, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 85
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 86
    .local v1, "done":Landroid/view/MenuItem;
    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 87
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 88
    invoke-static {p1, v6, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 89
    invoke-static {p1, v5, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 92
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 114
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 130
    :goto_0
    return-void

    .line 116
    :sswitch_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x0

    aput-object v2, v0, v1

    .line 119
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "MULTIPLE_PICKER_SELECTION_COMPLETED"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 123
    .end local v0    # "params":[Ljava/lang/Object;
    :sswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto :goto_0

    .line 126
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 114
    :sswitch_data_0
    .sparse-switch
        0x7f0f0022 -> :sswitch_1
        0x7f0f0024 -> :sswitch_0
        0x7f0f0289 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v5, 0x7f0f0024

    const v4, 0x7f0f0022

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 98
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMenu:Landroid/view/Menu;

    .line 99
    const v1, 0x7f0f02cf

    invoke-interface {p1, v1, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 101
    .local v0, "count":I
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    move v1, v2

    :goto_0
    invoke-static {p1, v5, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z

    if-eqz v1, :cond_1

    if-lez v0, :cond_1

    :goto_1
    invoke-static {p1, v5, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnabled(Landroid/view/Menu;IZ)V

    .line 104
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 105
    const v1, 0x7f0f0284

    invoke-static {p1, v1, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 106
    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 110
    :goto_2
    return-void

    :cond_0
    move v1, v3

    .line 101
    goto :goto_0

    :cond_1
    move v2, v3

    .line 102
    goto :goto_1

    .line 108
    :cond_2
    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_2
.end method

.method public setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 66
    return-void
.end method

.method protected updateDoneButton(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 70
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$2;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 78
    return-void
.end method
