.class public Lcom/sec/samsung/gallery/glview/GlReorderHelper;
.super Ljava/lang/Object;
.source "GlReorderHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;,
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;,
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;,
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;,
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;,
        Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

.field private mDragListener:Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

.field private mDuplicateObjectMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;",
            ">;"
        }
    .end annotation
.end field

.field private mEmptySlot:I

.field private mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field private mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

.field private mGlObject:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field private mIndicesMap:Landroid/util/SparseIntArray;

.field private mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field private mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

.field private mObjectAnimation:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlThumbObject;",
            ">;"
        }
    .end annotation
.end field

.field private mObjectCommit:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlThumbObject;",
            ">;"
        }
    .end annotation
.end field

.field private mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

.field private mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field private mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;)V
    .locals 6
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "listener"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

    .prologue
    const-wide/16 v4, 0x12c

    const/4 v2, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectAnimation:Ljava/util/HashSet;

    .line 46
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectCommit:Ljava/util/HashSet;

    .line 48
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;

    .line 57
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDuplicateObjectMap:Landroid/util/SparseArray;

    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 60
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragListener:Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v0, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 63
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->reset()V

    .line 65
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->setDuration(J)V

    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 68
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/samsung/gallery/glview/GlReorderHelper$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    invoke-virtual {v0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->setDuration(J)V

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 71
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->commitCurrentOrder()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragListener:Lcom/sec/samsung/gallery/glview/GlReorderHelper$OnDragEndListener;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->resetDragObject()V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mViewMode:I

    return v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/util/DimensionUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/android/gallery3d/glcore/GlLayer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    return-object v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Landroid/util/SparseIntArray;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Lcom/sec/samsung/gallery/glview/GlScroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectAnimation:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlReorderHelper;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectCommit:Ljava/util/HashSet;

    return-object v0
.end method

.method private commitCurrentOrder()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 254
    const/4 v0, 0x0

    .line 256
    .local v0, "candidate":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v6, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .line 258
    .local v4, "vf":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v5, v6, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    .line 262
    .local v5, "vl":I
    sget-object v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "vf = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " vl = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " idx = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-lt v6, v4, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-le v6, v5, :cond_2

    .line 267
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->findOutOfVisibleRangeObject()Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    move-result-object v3

    .line 269
    .local v3, "res":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-eqz v3, :cond_1

    .line 273
    sget-object v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "found out of visible range obj display index = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    move-object v0, v3

    .line 306
    :cond_1
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectCommit:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    move-object v6, v2

    .line 308
    check-cast v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    iput v6, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    goto :goto_1

    .line 289
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v3    # "res":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->findDuplicatePositionObject()Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    move-result-object v3

    .line 291
    .restart local v3    # "res":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-eqz v3, :cond_1

    .line 295
    sget-object v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "found duplicate display index = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    move-object v0, v3

    goto :goto_0

    .line 312
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_3
    if-eqz v0, :cond_5

    iget v6, v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    if-ltz v6, :cond_5

    .line 314
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 316
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    iput v7, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    .line 318
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    iput v7, v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 320
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 322
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 324
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 327
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 329
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v6

    invoke-virtual {v6, v9}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 333
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 349
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mObjectCommit:Ljava/util/HashSet;

    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    .line 351
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mIndicesMap:Landroid/util/SparseIntArray;

    invoke-virtual {v6}, Landroid/util/SparseIntArray;->clear()V

    .line 353
    return-void

    .line 345
    :cond_5
    sget-object v6, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    const-string/jumbo v7, "skip setting focus"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private controlByDrag()Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 169
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getAbsX()F

    move-result v2

    .line 170
    .local v2, "x":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getAbsY()F

    move-result v3

    .line 171
    .local v3, "y":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    invoke-virtual {v4, v2, v3}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->getIntensity(FF)F

    move-result v1

    .line 172
    .local v1, "intensity":F
    cmpl-float v4, v1, v6

    if-eqz v4, :cond_1

    cmpl-float v4, v1, v6

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    cmpl-float v4, v4, v6

    if-eqz v4, :cond_1

    :cond_0
    cmpg-float v4, v1, v6

    if-gez v4, :cond_4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    cmpl-float v4, v4, v5

    if-nez v4, :cond_4

    .line 175
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mActive:Z

    if-eqz v4, :cond_3

    .line 176
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 177
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    iput-boolean v7, v4, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mActive:Z

    .line 195
    :cond_2
    :goto_0
    return v7

    .line 179
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->isRunning()Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->isReady()Z

    move-result v4

    if-nez v4, :cond_2

    .line 182
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->findFocusAlbumIndex(Lcom/sec/samsung/gallery/glview/GlBaseObject;)I

    move-result v0

    .line 183
    .local v0, "focus":I
    const/4 v4, -0x1

    if-eq v0, v4, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    if-eq v0, v4, :cond_2

    .line 184
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    invoke-virtual {v4, v5, v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->startReorder(II)V

    .line 185
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    goto :goto_0

    .line 189
    .end local v0    # "focus":I
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->isRunning()Z

    move-result v4

    if-nez v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->isReady()Z

    move-result v4

    if-nez v4, :cond_5

    .line 190
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-float v5, v1

    const/high16 v6, 0x41200000    # 10.0f

    mul-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMoveToward(F)V

    .line 191
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->showScrollBar()V

    .line 193
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    const/4 v5, 0x1

    iput-boolean v5, v4, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->mActive:Z

    goto :goto_0
.end method

.method private findDuplicatePositionObject()Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .locals 11

    .prologue
    .line 415
    const/4 v7, 0x0

    .line 417
    .local v7, "res":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDuplicateObjectMap:Landroid/util/SparseArray;

    invoke-virtual {v8}, Landroid/util/SparseArray;->clear()V

    .line 419
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .local v0, "arr$":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v6, v0, v1

    .local v6, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    move-object v5, v6

    .line 421
    check-cast v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 423
    .local v5, "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-nez v5, :cond_1

    .line 419
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 429
    :cond_1
    iget v2, v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 431
    .local v2, "key":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDuplicateObjectMap:Landroid/util/SparseArray;

    invoke-virtual {v8, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 433
    .local v4, "mapO":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-eqz v4, :cond_5

    .line 435
    iget v8, v4, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    if-ne v8, v2, :cond_3

    .line 437
    move-object v7, v4

    .line 464
    .end local v2    # "key":I
    .end local v4    # "mapO":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .end local v5    # "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    :goto_2
    return-object v7

    .line 439
    .restart local v2    # "key":I
    .restart local v4    # "mapO":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .restart local v5    # "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .restart local v6    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget v8, v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    if-ne v8, v2, :cond_4

    .line 441
    move-object v7, v5

    goto :goto_2

    .line 447
    :cond_4
    sget-object v8, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Reorder Commit error key = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mIndex = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 456
    :cond_5
    const/16 v8, -0x3e8

    if-eq v2, v8, :cond_0

    .line 458
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDuplicateObjectMap:Landroid/util/SparseArray;

    invoke-virtual {v8, v2, v5}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method private findFocusAlbumIndex(Lcom/sec/samsung/gallery/glview/GlBaseObject;)I
    .locals 13
    .param p1, "current"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const v11, 0x3f333333    # 0.7f

    .line 202
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAbsX()F

    move-result v8

    .line 204
    .local v8, "x":F
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getAbsY()F

    move-result v9

    .line 206
    .local v9, "y":F
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlPos;

    invoke-direct {v4}, Lcom/sec/android/gallery3d/glcore/GlPos;-><init>()V

    .line 208
    .local v4, "pos":Lcom/sec/android/gallery3d/glcore/GlPos;
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v10, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    .local v3, "i":I
    :goto_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-gt v3, v10, :cond_1

    .line 210
    invoke-virtual {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->getThumbPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V

    .line 212
    iget v10, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mWidth:F

    mul-float v7, v10, v11

    .line 214
    .local v7, "w":F
    iget v10, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mHeight:F

    mul-float v2, v10, v11

    .line 216
    .local v2, "h":F
    iget v10, v4, Lcom/sec/android/gallery3d/glcore/GlPos;->mX:F

    sub-float v5, v10, v7

    .line 218
    .local v5, "sx":F
    mul-float v10, v12, v7

    add-float v0, v5, v10

    .line 220
    .local v0, "ex":F
    iget v10, v4, Lcom/sec/android/gallery3d/glcore/GlPos;->mY:F

    add-float v6, v10, v2

    .line 222
    .local v6, "sy":F
    mul-float v10, v12, v2

    sub-float v1, v6, v10

    .line 224
    .local v1, "ey":F
    cmpg-float v10, v5, v8

    if-gtz v10, :cond_0

    cmpg-float v10, v8, v0

    if-gtz v10, :cond_0

    cmpl-float v10, v6, v9

    if-ltz v10, :cond_0

    cmpl-float v10, v9, v1

    if-ltz v10, :cond_0

    .line 230
    .end local v0    # "ex":F
    .end local v1    # "ey":F
    .end local v2    # "h":F
    .end local v3    # "i":I
    .end local v5    # "sx":F
    .end local v6    # "sy":F
    .end local v7    # "w":F
    :goto_1
    return v3

    .line 208
    .restart local v0    # "ex":F
    .restart local v1    # "ey":F
    .restart local v2    # "h":F
    .restart local v3    # "i":I
    .restart local v5    # "sx":F
    .restart local v6    # "sy":F
    .restart local v7    # "w":F
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 230
    .end local v0    # "ex":F
    .end local v1    # "ey":F
    .end local v2    # "h":F
    .end local v5    # "sx":F
    .end local v6    # "sy":F
    .end local v7    # "w":F
    :cond_1
    const/4 v3, -0x1

    goto :goto_1
.end method

.method private findOutOfVisibleRangeObject()Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .locals 9

    .prologue
    .line 357
    const/4 v6, 0x0

    .line 359
    .local v6, "res":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    const v4, 0x7fffffff

    .line 361
    .local v4, "min":I
    const/high16 v3, -0x80000000

    .line 363
    .local v3, "max":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v2, v7, :cond_5

    .line 365
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v5, v7, v2

    check-cast v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 367
    .local v5, "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-nez v5, :cond_1

    .line 363
    :cond_0
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 373
    :cond_1
    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    sub-int v1, v7, v8

    .line 375
    .local v1, "deltaMin":I
    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    sub-int v0, v7, v8

    .line 377
    .local v0, "deltaMax":I
    if-gez v1, :cond_3

    .line 379
    if-ge v1, v4, :cond_2

    .line 381
    move v4, v1

    .line 383
    move-object v6, v5

    goto :goto_1

    .line 385
    :cond_2
    if-nez v6, :cond_0

    .line 387
    move-object v6, v5

    goto :goto_1

    .line 391
    :cond_3
    if-lez v0, :cond_0

    .line 393
    if-le v0, v3, :cond_4

    .line 395
    move v3, v0

    .line 397
    move-object v6, v5

    goto :goto_1

    .line 399
    :cond_4
    if-nez v6, :cond_0

    .line 401
    move-object v6, v5

    goto :goto_1

    .line 409
    .end local v0    # "deltaMax":I
    .end local v1    # "deltaMin":I
    .end local v5    # "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    :cond_5
    return-object v6
.end method

.method private resetDragObject()V
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    .line 150
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eqz v5, :cond_0

    .line 151
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v5, v6, v6, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setColor(FFF)V

    .line 152
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 153
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v6, -0x1

    iput v6, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    .line 155
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 156
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->moveToLast()V

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .local v0, "arr$":[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_3

    aget-object v4, v0, v1

    .local v4, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    move-object v3, v4

    .line 159
    check-cast v3, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 160
    .local v3, "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    if-eqz v4, :cond_2

    .line 161
    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setVisibility(Z)V

    .line 162
    iget v5, v3, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    iput v5, v3, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mDisplayIndex:I

    .line 158
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    .end local v3    # "o":Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;
    .end local v4    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    const/4 v5, 0x0

    iput-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 166
    return-void
.end method


# virtual methods
.method getDragObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    return-object v0
.end method

.method public getThumbPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V
    .locals 7
    .param p1, "index"    # I
    .param p2, "retPos"    # Lcom/sec/android/gallery3d/glcore/GlPos;

    .prologue
    .line 240
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    rem-int v0, p1, v4

    .line 242
    .local v0, "col":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int v1, p1, v4

    .line 244
    .local v1, "row":I
    int-to-float v4, v0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapW:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartX:F

    add-float v2, v4, v5

    .line 246
    .local v2, "x":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartY:F

    int-to-float v5, v1

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemGapH:F

    mul-float/2addr v5, v6

    sub-float v3, v4, v5

    .line 248
    .local v3, "y":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    add-float/2addr v4, v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemStartZ:F

    invoke-virtual {p2, v2, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlPos;->set(FFF)V

    .line 250
    return-void
.end method

.method initDragObject(IIII)V
    .locals 7
    .param p1, "thumbWidth"    # I
    .param p2, "thumbHeight"    # I
    .param p3, "textWidth"    # I
    .param p4, "textHeight"    # I

    .prologue
    .line 95
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object v1, p0

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;-><init>(Lcom/sec/samsung/gallery/glview/GlReorderHelper;Lcom/sec/android/gallery3d/glcore/GlLayer;IIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 98
    return-void
.end method

.method isDragging()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onDrag(II)Z
    .locals 6
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    int-to-float v3, p1

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 132
    .local v0, "sx":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    int-to-float v3, p2

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 133
    .local v1, "sy":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    neg-float v4, v1

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPos(IFFF)V

    .line 134
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->controlByDrag()Z

    .line 135
    const/4 v2, 0x1

    return v2
.end method

.method onDragEnd()Z
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 140
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->hideScrollBarImmediately()V

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->startAnimation()V

    .line 142
    const/4 v0, 0x1

    return v0
.end method

.method registerObject([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 0
    .param p1, "objs"    # [Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mGlObject:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 79
    return-void
.end method

.method resetDragInfo()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mDragInfo:Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$DragInfo;->reset()V

    .line 102
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mReorderAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderAnim;->stop()V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mMoveToAnim:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlMoveToAnim;->stop()V

    .line 106
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->resetDragObject()V

    .line 107
    return-void
.end method

.method setFlingAnim(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;)V
    .locals 0
    .param p1, "flingAnim"    # Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .prologue
    .line 82
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 83
    return-void
.end method

.method setScroller(Lcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 0
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    .line 75
    return-void
.end method

.method setViewMode(I)V
    .locals 0
    .param p1, "viewMode"    # I

    .prologue
    .line 86
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mViewMode:I

    .line 87
    return-void
.end method

.method startDragAnim(Lcom/sec/android/gallery3d/glcore/GlObject;II)V
    .locals 4
    .param p1, "srcObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 118
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .end local p1    # "srcObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->setVisibility(ZI)V

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setIndex(I)V

    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mFocusedAlbum:Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlReorderHelper$GlReorderObject;->mIndex:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mEmptySlot:I

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    const/4 v1, 0x0

    const v2, 0x3f10902e    # 0.5647f

    const v3, 0x3f2bac71    # 0.6706f

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setColor(FFF)V

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPosIndex()I

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHndDispmnt:I

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->moveToLast()V

    .line 128
    return-void
.end method

.method updateObject(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 2
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-eq p1, v0, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->mThumbDrag:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-eq v0, v1, :cond_1

    .line 111
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 115
    :goto_0
    return-void

    .line 113
    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-virtual {p1, v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_0
.end method
