.class Lcom/sec/samsung/gallery/view/allview/AllViewState$17;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/OnProgressListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectAll(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 1071
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 1087
    const/4 v0, 0x0

    return v0
.end method

.method public handleOperation(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 1092
    .local p1, "objectList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    return v0
.end method

.method public onCompleted(Z)V
    .locals 2
    .param p1, "result"    # Z

    .prologue
    .line 1078
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$4100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1079
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateCountOnActionBar()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$4200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    .line 1081
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1082
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    .line 1083
    :cond_1
    return-void
.end method

.method public onProgress(II)V
    .locals 0
    .param p1, "progress"    # I
    .param p2, "total"    # I

    .prologue
    .line 1074
    return-void
.end method
