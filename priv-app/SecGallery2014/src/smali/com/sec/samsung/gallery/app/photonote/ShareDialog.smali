.class public Lcom/sec/samsung/gallery/app/photonote/ShareDialog;
.super Ljava/lang/Object;
.source "ShareDialog.java"

# interfaces
.implements Ljava/util/Observer;


# static fields
.field private static final SHARE_FILTER_LIST:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

.field mContext:Landroid/content/Context;

.field mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mUri:Landroid/net/Uri;

    .line 36
    return-void
.end method

.method private createAppList()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->createIntentForShare()Landroid/content/Intent;

    move-result-object v0

    .line 40
    .local v0, "intent":Landroid/content/Intent;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->createFilterList()V

    .line 42
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 43
    .local v1, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 44
    .local v2, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    new-instance v3, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v3, v1}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 45
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->getFilteredShareList(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    return-object v3
.end method

.method private createFilterList()V
    .locals 3

    .prologue
    .line 76
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 78
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuShareViaMsg:Z

    if-eqz v1, :cond_0

    .line 79
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    const-string v2, "com.android.email.activity.MessageCompose"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    const-string v2, "com.google.android.gm.ComposeActivityGmail"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    const-string v2, "com.android.mail.compose.ComposeActivityGmail"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_0
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->getPaperArtistClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 93
    :goto_0
    return-void

    .line 90
    :catch_0
    move-exception v0

    .line 91
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private createIntentForShare()Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 63
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "android.intent.extra.STREAM"

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 66
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 69
    const-string v1, "exit_on_sent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 71
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 72
    return-object v0
.end method

.method private getFilteredShareList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    .local p1, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 97
    .local v2, "numList":I
    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 98
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 99
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v3, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 100
    sget-object v3, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->SHARE_FILTER_LIST:Ljava/util/ArrayList;

    iget-object v4, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 101
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 97
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 104
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    :cond_1
    return-object p1
.end method

.method private getPaperArtistClassName()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 146
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.dama.paperartist"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 147
    .local v0, "info":Landroid/content/pm/PackageInfo;
    const/4 v1, 0x0

    .line 148
    .local v1, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    .line 149
    .local v2, "paperArtistClsName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 150
    iget-object v3, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.dama.paperartist"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 151
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 154
    :cond_0
    return-object v2
.end method

.method private showAppChoiceDialog(Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v3, 0x1

    .line 49
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    .line 50
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v1, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2, p1, v0, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;Z)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .line 51
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    const v2, 0x7f0e0047

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setTitle(I)V

    .line 52
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v1, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->addObserver(Ljava/util/Observer;)V

    .line 54
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 55
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->startSingleShareApp()V

    .line 56
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->showDialog()V

    goto :goto_0
.end method

.method private startShareApp(Landroid/content/pm/ResolveInfo;)V
    .locals 5
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->createIntentForShare()Landroid/content/Intent;

    move-result-object v1

    .line 110
    .local v1, "intent":Landroid/content/Intent;
    iget-object v0, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 111
    .local v0, "activityInfo":Landroid/content/pm/ActivityInfo;
    new-instance v2, Landroid/content/ComponentName;

    iget-object v3, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    iget-object v4, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 114
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->dismissDialog()V

    .line 130
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mUri:Landroid/net/Uri;

    .line 131
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mUri:Landroid/net/Uri;

    if-nez v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0113

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    .line 125
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->createAppList()Ljava/util/List;

    move-result-object v0

    .line 122
    .local v0, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->showAppChoiceDialog(Ljava/util/List;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 136
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 137
    .local v0, "event":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 139
    .local v1, "eventType":I
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    if-ne v1, v3, :cond_0

    .line 140
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/ResolveInfo;

    .line 141
    .local v2, "info":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/app/photonote/ShareDialog;->startShareApp(Landroid/content/pm/ResolveInfo;)V

    .line 143
    .end local v2    # "info":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-void
.end method
