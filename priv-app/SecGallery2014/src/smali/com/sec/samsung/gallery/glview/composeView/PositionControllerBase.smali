.class public abstract Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;,
        Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    }
.end annotation


# static fields
.field protected static final CHK_VISIBILITY_ANIM:I = 0x2

.field protected static final CHK_VISIBILITY_MODE:I = 0x0

.field protected static final CHK_VISIBILITY_TYPE:I = 0x1

.field public static final COMP_OBJ_GRPCHCK:I = 0x3

.field public static final COMP_OBJ_THUMBNAIL:I = 0x1

.field public static final COMP_OBJ_TITLE:I = 0x2

.field protected static final MAX_LEVEL:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final FLAG_ALL:I

.field private final FLAG_SET_SRC:I

.field private final FLAG_SET_TGT:I

.field private final FLAG_SKIP_VISIBLE_CHK:I

.field protected mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field protected mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

.field private mAlwaysActiveGroupObjectIndex:I

.field protected mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

.field protected mCheckH:F

.field protected mCheckHMargin:F

.field protected mCheckHMarginPixel:I

.field protected mCheckVMargin:F

.field protected mCheckVMarginPixel:I

.field protected mCheckW:F

.field protected mCheckboxHeightPixel:I

.field protected mCheckboxWidthPixel:I

.field protected mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field protected mDefDistance:F

.field protected mDisplaySelectedCount:Z

.field protected mExpandButtonHeightPixel:I

.field protected mExpandButtonWidthPixel:I

.field protected mExpansionH:F

.field protected mExpansionHMargin:F

.field protected mExpansionHMarginPixel:I

.field protected mExpansionVMargin:F

.field protected mExpansionVMarginPixel:I

.field protected mExpansionW:F

.field protected mExtraBtm:F

.field protected mExtraLeft:F

.field protected mExtraRight:F

.field protected mExtraTop:F

.field protected mFocusChangedFlag:Z

.field protected mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

.field protected mGroupCount:I

.field protected mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

.field public mGroupLineCount:I

.field protected mGroupVGap:F

.field protected mGroupVGapPixel:I

.field public mGrpActiveEnd:I

.field public mGrpActiveStart:I

.field protected mGrpCheckVMargin:F

.field protected mGrpCheckVMarginPixel:I

.field public mGrpContentEnd:I

.field public mGrpContentStart:I

.field protected mGrpW:F

.field protected mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

.field protected mItemGapH:F

.field protected mItemGapHPixel:I

.field protected mItemGapVPixel:I

.field protected mItemGapW:F

.field protected mItemH:F

.field protected mItemMaxH:F

.field protected mItemMaxW:F

.field protected mItemSx:F

.field protected mItemSy:F

.field protected mItemW:F

.field protected mLcdRect:Landroid/graphics/Rect;

.field protected mLevel:I

.field protected mMargBtm:F

.field protected mMargLeft:F

.field protected mMargRight:F

.field protected mMargTop:F

.field protected mMarginBottomPixel:I

.field protected mMarginLeftPixel:I

.field protected mMarginRightPixel:I

.field protected mMarginTopPixel:I

.field public mPhotoLineCount:I

.field protected mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

.field protected mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field protected mResource:Landroid/content/res/Resources;

.field public mScrollable:F

.field public mScrollableMax:F

.field protected mSpaceHeight:F

.field protected mSpaceWidth:F

.field private mTempRect:Landroid/graphics/RectF;

.field protected mTitleCanvsH:I

.field protected mTitleCanvsW:I

.field protected mTitleH:F

.field protected mTitleHeightPixel:I

.field protected mTitleTextMarginBottom:F

.field protected mTitleTextMarginBottomPixel:I

.field protected mTitleTextMarginLeft:F

.field protected mTitleTextMarginLeftPixel:I

.field protected mTitleTextMarginTop:F

.field protected mTitleTextMarginTopPixel:I

.field protected mTitleW:F

.field protected mTitleWidthPixel:I

.field protected mValidH:F

.field protected mValidW:F

.field protected mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

.field protected mViewHeight:I

.field protected mViewWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginBottomPixel:I

    .line 36
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginRightPixel:I

    .line 38
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginLeftPixel:I

    .line 50
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->FLAG_ALL:I

    .line 51
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->FLAG_SET_SRC:I

    .line 52
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->FLAG_SET_TGT:I

    .line 53
    const/16 v0, 0x8

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->FLAG_SKIP_VISIBLE_CHK:I

    .line 84
    iput-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 87
    new-array v0, v3, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;-><init>()V

    aput-object v1, v0, v2

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .line 90
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    .line 94
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    .line 95
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    .line 101
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mDisplaySelectedCount:Z

    .line 102
    iput-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mLcdRect:Landroid/graphics/Rect;

    .line 103
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTempRect:Landroid/graphics/RectF;

    .line 105
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMarginPixel:I

    .line 106
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMarginPixel:I

    .line 107
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMarginPixel:I

    .line 108
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMarginPixel:I

    .line 109
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpCheckVMarginPixel:I

    .line 117
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginTopPixel:I

    .line 118
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginBottomPixel:I

    .line 119
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeftPixel:I

    .line 126
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mFocusChangedFlag:Z

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    .line 1649
    return-void
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .prologue
    .line 24
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    return v0
.end method

.method private activeObjectRange(IIII)V
    .locals 36
    .param p1, "groupIdx"    # I
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "flag"    # I

    .prologue
    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-object/from16 v31, v0

    aget-object v13, v31, p1

    .line 522
    .local v13, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    const/16 v26, 0x0

    .line 524
    .local v26, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    move-object/from16 v22, v0

    .line 525
    .local v22, "itemW":[F
    iget-object v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    move-object/from16 v21, v0

    .line 526
    .local v21, "itemH":[F
    const/4 v6, 0x0

    .local v6, "bx":F
    const/4 v7, 0x0

    .line 527
    .local v7, "by":F
    const/16 v16, 0x0

    .local v16, "gx":F
    const/16 v17, 0x0

    .line 530
    .local v17, "gy":F
    const/4 v4, 0x0

    .line 534
    .local v4, "added":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 535
    .local v15, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    move/from16 v0, p1

    invoke-virtual {v15, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 536
    .local v14, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-lez p4, :cond_1

    const/4 v12, 0x1

    .line 537
    .local v12, "flagUpd":Z
    :goto_0
    and-int/lit8 v31, p4, 0x1

    if-lez v31, :cond_2

    const/16 v28, 0x1

    .line 538
    .local v28, "updateAll":Z
    :goto_1
    and-int/lit8 v31, p4, 0x2

    if-lez v31, :cond_3

    const/16 v24, 0x1

    .line 539
    .local v24, "keepSrc":Z
    :goto_2
    and-int/lit8 v31, p4, 0x4

    if-lez v31, :cond_4

    const/16 v25, 0x1

    .line 540
    .local v25, "keepTgt":Z
    :goto_3
    if-nez v14, :cond_9

    .line 541
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->activateGroup(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    move-result-object v14

    .line 542
    if-nez v14, :cond_5

    .line 543
    sget-object v31, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    const-string v32, "activeObjectRange grpObj == null"

    invoke-static/range {v31 .. v32}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 661
    :cond_0
    :goto_4
    return-void

    .line 536
    .end local v12    # "flagUpd":Z
    .end local v24    # "keepSrc":Z
    .end local v25    # "keepTgt":Z
    .end local v28    # "updateAll":Z
    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    .line 537
    .restart local v12    # "flagUpd":Z
    :cond_2
    const/16 v28, 0x0

    goto :goto_1

    .line 538
    .restart local v28    # "updateAll":Z
    :cond_3
    const/16 v24, 0x0

    goto :goto_2

    .line 539
    .restart local v24    # "keepSrc":Z
    :cond_4
    const/16 v25, 0x0

    goto :goto_3

    .line 546
    .restart local v25    # "keepTgt":Z
    :cond_5
    const/16 v29, 0x1

    .line 550
    .local v29, "updatePos":Z
    :goto_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSx:F

    move/from16 v31, v0

    iget v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    move/from16 v32, v0

    add-float v16, v31, v32

    .line 551
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSy:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    move/from16 v32, v0

    add-float v31, v31, v32

    iget v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    move/from16 v32, v0

    sub-float v17, v31, v32

    .line 552
    if-eqz v25, :cond_a

    .line 553
    const/16 v31, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v31

    invoke-virtual {v14, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setTargetPos(FFF)V

    .line 560
    :goto_6
    if-eqz v29, :cond_7

    .line 561
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTempRect:Landroid/graphics/RectF;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    move/from16 v34, v0

    iget v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    move/from16 v35, v0

    move/from16 v0, v35

    neg-float v0, v0

    move/from16 v35, v0

    invoke-virtual/range {v31 .. v35}, Landroid/graphics/RectF;->set(FFFF)V

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTempRect:Landroid/graphics/RectF;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    invoke-virtual {v14, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setBorderRect(Landroid/graphics/RectF;)V

    .line 563
    iget-object v8, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 565
    .local v8, "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v8, :cond_6

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    move/from16 v31, v0

    if-eqz v31, :cond_c

    .line 567
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v32, v0

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    sub-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v32, v0

    sub-float v6, v31, v32

    .line 570
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginTop:F

    move/from16 v31, v0

    move/from16 v0, v31

    neg-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleH:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginBottom:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    add-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpCheckVMargin:F

    move/from16 v32, v0

    add-float v7, v31, v32

    .line 571
    if-eqz v25, :cond_d

    .line 572
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 573
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetSize(FF)V

    .line 586
    :cond_6
    :goto_8
    iget-object v8, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 587
    if-eqz v8, :cond_7

    .line 588
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    move/from16 v31, v0

    if-eqz v31, :cond_10

    .line 589
    iget v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    move/from16 v31, v0

    if-eqz v31, :cond_f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    const/high16 v33, 0x40400000    # 3.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    add-float v31, v31, v33

    :goto_9
    sub-float v6, v32, v31

    .line 592
    :goto_a
    iget v7, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCy:F

    .line 593
    if-eqz v25, :cond_12

    .line 594
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 595
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetSize(FF)V

    .line 607
    .end local v8    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_7
    :goto_b
    iget-object v9, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 608
    .local v9, "cxSet":[F
    iget-object v10, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 609
    .local v10, "cySet":[F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    move-object/from16 v27, v0

    .line 610
    .local v27, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    move/from16 v18, p2

    .local v18, "i":I
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mCount:I

    move/from16 v31, v0

    add-int v31, v31, p2

    move/from16 v0, v31

    move/from16 v1, p3

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v31

    move/from16 v0, v18

    move/from16 v1, v31

    if-ge v0, v1, :cond_19

    .line 611
    shl-int/lit8 v31, p1, 0x10

    or-int v20, v31, v18

    .line 612
    .local v20, "itemCode":I
    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v26

    .end local v26    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v26, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 613
    .restart local v26    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v26, :cond_15

    .line 614
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->activateObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v26

    .line 615
    if-nez v26, :cond_14

    .line 610
    :cond_8
    :goto_d
    add-int/lit8 v18, v18, 0x1

    goto :goto_c

    .line 548
    .end local v9    # "cxSet":[F
    .end local v10    # "cySet":[F
    .end local v18    # "i":I
    .end local v20    # "itemCode":I
    .end local v27    # "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    .end local v29    # "updatePos":Z
    :cond_9
    move/from16 v29, v12

    .restart local v29    # "updatePos":Z
    goto/16 :goto_5

    .line 554
    :cond_a
    if-eqz v24, :cond_b

    .line 555
    const/16 v31, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v31

    invoke-virtual {v14, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSourcePos(FFF)V

    goto/16 :goto_6

    .line 557
    :cond_b
    const/16 v31, 0x0

    move/from16 v0, v16

    move/from16 v1, v17

    move/from16 v2, v31

    invoke-virtual {v14, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPos(FFF)V

    goto/16 :goto_6

    .line 569
    .restart local v8    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v32, v0

    add-float v6, v31, v32

    goto/16 :goto_7

    .line 575
    :cond_d
    if-eqz v24, :cond_e

    .line 576
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateGroupCheckBox(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 577
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 578
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceSize(FF)V

    goto/16 :goto_8

    .line 580
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateGroupCheckBox(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 581
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 582
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto/16 :goto_8

    .line 589
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeft:F

    move/from16 v31, v0

    goto/16 :goto_9

    .line 591
    :cond_10
    iget v0, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mTitleCx:F

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    move-object/from16 v31, v0

    move-object/from16 v0, v31

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    move/from16 v31, v0

    if-eqz v31, :cond_11

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    const/high16 v33, 0x40000000    # 2.0f

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v34, v0

    mul-float v33, v33, v34

    add-float v31, v31, v33

    :goto_e
    add-float v6, v32, v31

    goto/16 :goto_a

    :cond_11
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeft:F

    move/from16 v31, v0

    goto :goto_e

    .line 596
    :cond_12
    if-eqz v24, :cond_13

    .line 597
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateTitle(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 598
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 599
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceSize(FF)V

    goto/16 :goto_b

    .line 601
    :cond_13
    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateTitle(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 602
    const/16 v31, 0x0

    move/from16 v0, v31

    invoke-virtual {v8, v6, v7, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 603
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v8, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto/16 :goto_b

    .line 617
    .end local v8    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .restart local v9    # "cxSet":[F
    .restart local v10    # "cySet":[F
    .restart local v18    # "i":I
    .restart local v20    # "itemCode":I
    .restart local v27    # "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move-object/from16 v31, v0

    const/16 v32, 0x0

    move-object/from16 v0, v31

    move/from16 v1, v20

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->addToUpdateQueue(II)V

    .line 618
    move-object/from16 v0, v26

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->add(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    invoke-static {v14, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$800(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 619
    const/16 v29, 0x1

    .line 620
    const/4 v4, 0x1

    .line 626
    :goto_f
    if-eqz v29, :cond_8

    .line 628
    aget v31, v21, v18

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    aget v32, v10, v18

    sub-float v30, v31, v32

    .line 629
    .local v30, "vPos":F
    aget v23, v22, v18

    .line 630
    .local v23, "iw":F
    aget v19, v21, v18

    .line 631
    .local v19, "ih":F
    move-object/from16 v0, v26

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    move/from16 v31, v0

    move/from16 v0, v31

    neg-int v0, v0

    move/from16 v31, v0

    move-object/from16 v0, v26

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVertexRotation(I)V

    .line 632
    move-object/from16 v0, v26

    iget-object v11, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 633
    .local v11, "eObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    move-object/from16 v0, v26

    iget-object v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 634
    .local v5, "bObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v25, :cond_17

    .line 635
    aget v31, v9, v18

    const/16 v32, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v31

    move/from16 v2, v30

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTargetPos(FFF)V

    .line 636
    move-object/from16 v0, v26

    move/from16 v1, v23

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTargetSize(FF)V

    .line 637
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    move/from16 v31, v0

    sub-float v31, v23, v31

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMargin:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    move/from16 v0, v19

    neg-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMargin:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v11, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    .line 638
    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v32, v0

    add-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v32, v0

    add-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    sub-float v32, v19, v32

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMargin:F

    move/from16 v33, v0

    sub-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetPos(FFF)V

    goto/16 :goto_d

    .line 622
    .end local v5    # "bObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v11    # "eObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v19    # "ih":F
    .end local v23    # "iw":F
    .end local v30    # "vPos":F
    :cond_15
    if-eqz v28, :cond_16

    .line 623
    move-object/from16 v0, p0

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    .line 624
    :cond_16
    move/from16 v29, v12

    goto/16 :goto_f

    .line 640
    .restart local v5    # "bObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .restart local v11    # "eObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .restart local v19    # "ih":F
    .restart local v23    # "iw":F
    .restart local v30    # "vPos":F
    :cond_17
    if-eqz v24, :cond_18

    .line 641
    aget v31, v9, v18

    const/16 v32, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v31

    move/from16 v2, v30

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSourcePos(FFF)V

    .line 642
    move-object/from16 v0, v26

    move/from16 v1, v23

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSourceSize(FF)V

    .line 643
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    move/from16 v31, v0

    sub-float v31, v23, v31

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMargin:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    move/from16 v0, v19

    neg-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMargin:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v11, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 644
    move/from16 v0, v23

    neg-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v32, v0

    add-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v32, v0

    add-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    sub-float v32, v19, v32

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMargin:F

    move/from16 v33, v0

    sub-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 651
    :goto_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v11, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 652
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v32, v0

    move/from16 v0, v31

    move/from16 v1, v32

    invoke-virtual {v5, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    goto/16 :goto_d

    .line 646
    :cond_18
    aget v31, v9, v18

    const/16 v32, 0x0

    move-object/from16 v0, v26

    move/from16 v1, v31

    move/from16 v2, v30

    move/from16 v3, v32

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setPos(FFF)V

    .line 647
    move-object/from16 v0, v26

    move/from16 v1, v23

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSize(FF)V

    .line 648
    aget v31, v22, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMargin:F

    move/from16 v32, v0

    sub-float v31, v31, v32

    aget v32, v21, v18

    move/from16 v0, v32

    neg-float v0, v0

    move/from16 v32, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMargin:F

    move/from16 v33, v0

    add-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v11, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 649
    aget v31, v22, v18

    move/from16 v0, v31

    neg-float v0, v0

    move/from16 v31, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    move/from16 v32, v0

    add-float v31, v31, v32

    const/high16 v32, 0x40000000    # 2.0f

    div-float v31, v31, v32

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    move/from16 v32, v0

    add-float v31, v31, v32

    aget v32, v21, v18

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    move/from16 v33, v0

    sub-float v32, v32, v33

    const/high16 v33, 0x40000000    # 2.0f

    div-float v32, v32, v33

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMargin:F

    move/from16 v33, v0

    sub-float v32, v32, v33

    const/16 v33, 0x0

    move/from16 v0, v31

    move/from16 v1, v32

    move/from16 v2, v33

    invoke-virtual {v5, v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    goto/16 :goto_10

    .line 655
    .end local v5    # "bObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v11    # "eObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v19    # "ih":F
    .end local v20    # "itemCode":I
    .end local v23    # "iw":F
    .end local v30    # "vPos":F
    :cond_19
    if-eqz v4, :cond_0

    .line 656
    iget-object v0, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v31, v0

    if-eqz v31, :cond_1a

    .line 657
    iget-object v0, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 658
    :cond_1a
    iget-object v0, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v31, v0

    if-eqz v31, :cond_0

    .line 659
    iget-object v0, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v31, v0

    invoke-virtual/range {v31 .. v31}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    goto/16 :goto_4
.end method

.method private setGrpChkBorderVisible(ZI)V
    .locals 3
    .param p1, "visible"    # Z
    .param p2, "index"    # I

    .prologue
    .line 832
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v2, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 833
    .local v0, "groupObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupSelect:Z

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 847
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 837
    .local v1, "grpChkObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v1, :cond_0

    .line 840
    if-eqz p1, :cond_2

    .line 841
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderVisible(Z)V

    .line 842
    const v2, 0x28fc3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderColor(I)V

    .line 843
    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderWidth(F)V

    goto :goto_0

    .line 845
    :cond_2
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setBorderVisible(Z)V

    goto :goto_0
.end method

.method private setVisibleObjectPosition()V
    .locals 8

    .prologue
    .line 427
    const/4 v3, 0x0

    .line 428
    .local v3, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    const/4 v0, 0x0

    .local v0, "bx":F
    const/4 v1, 0x0

    .line 430
    .local v1, "by":F
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSx:F

    .line 431
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 432
    .local v4, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    const/4 v5, 0x0

    .local v5, "idx":I
    :goto_0
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v6

    if-ge v5, v6, :cond_0

    .line 433
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 434
    .restart local v3    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$700(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)I

    move-result v7

    aget-object v2, v6, v7

    .line 435
    .local v2, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSy:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    add-float/2addr v6, v7

    iget v7, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    sub-float v1, v6, v7

    .line 436
    iget v6, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    add-float/2addr v6, v0

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v1, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setPos(FFF)V

    .line 432
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 438
    .end local v2    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_0
    return-void
.end method

.method private updateForFade(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 924
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->isSetViewToSub()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getViewSub()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move v2, v1

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    .line 926
    .local v7, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setViewSub(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 931
    :goto_0
    return-void

    .line 928
    .end local v7    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move v2, v1

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    .line 929
    .restart local v7    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method

.method private updateGroupCheckBox(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 5
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    .line 1079
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v3, :cond_1

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1081
    :cond_1
    iget v3, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v0, v3, 0x10

    .line 1082
    .local v0, "albumIdx":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v4, -0x1

    invoke-virtual {v3, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCurrentState(II)I

    move-result v3

    if-lez v3, :cond_2

    const/4 v2, 0x1

    .line 1083
    .local v2, "selected":Z
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1084
    .local v1, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v1, :cond_0

    .line 1086
    # invokes: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSelected(Z)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$900(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;Z)V

    goto :goto_0

    .line 1082
    .end local v1    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v2    # "selected":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private updateTitle(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-void

    .line 1067
    :cond_1
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v1, v0, 0x10

    .line 1068
    .local v1, "albumIndex":I
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleCanvsW:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleCanvsH:I

    invoke-virtual {p1, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->updateCanvas(II)V

    .line 1069
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v2, -0x1

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v5, 0x0

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    .line 1071
    .local v7, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method


# virtual methods
.method protected applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1023
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    if-eqz v2, :cond_0

    .line 1024
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSelected(Z)V

    .line 1025
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDisabled:Z

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDimState(Z)V

    .line 1028
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p0, p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setThumbObjectCanvas(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1059
    :cond_1
    :goto_0
    return-void

    .line 1031
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 1032
    .local v0, "cropRect":Landroid/graphics/Rect;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1033
    iget v2, v0, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget v3, v0, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget v5, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-virtual {p1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTexCoords(FFFF)V

    .line 1034
    invoke-virtual {p1, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 1035
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    .line 1037
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCheckBoxVisible:Z

    if-eqz v2, :cond_6

    .line 1038
    const/4 v1, 0x1

    .line 1039
    .local v1, "expansionObjectVisibility":Z
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-eqz v2, :cond_3

    .line 1040
    const/4 v1, 0x0

    .line 1041
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1043
    :cond_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseShowExpansionIcon:Z

    if-eqz v2, :cond_5

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mLevel:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mLevel:I

    if-ne v2, v7, :cond_5

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    if-eqz v2, :cond_5

    .line 1044
    :cond_4
    const/4 v1, 0x0

    .line 1045
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1050
    :goto_1
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1056
    .end local v1    # "expansionObjectVisibility":Z
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDisabled:Z

    if-eqz v2, :cond_1

    .line 1057
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_0

    .line 1047
    .restart local v1    # "expansionObjectVisibility":Z
    :cond_5
    const/4 v1, 0x1

    .line 1048
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_1

    .line 1052
    .end local v1    # "expansionObjectVisibility":Z
    :cond_6
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 1053
    iget-object v2, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    goto :goto_2
.end method

.method public checkAlwaysActiveGroupObjectIndex(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1876
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    shr-int/lit8 v1, v1, 0x10

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    shr-int/lit8 v1, v1, 0x10

    if-le v0, v1, :cond_1

    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    if-ne v0, p1, :cond_1

    .line 1878
    const/4 v0, 0x1

    .line 1880
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public convX(F)F
    .locals 1
    .param p1, "x"    # F

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public convX(I)F
    .locals 2
    .param p1, "x"    # I

    .prologue
    .line 245
    int-to-float v0, p1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public convY(F)F
    .locals 1
    .param p1, "y"    # F

    .prologue
    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    mul-float/2addr v0, p1

    return v0
.end method

.method public convY(I)F
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 249
    int-to-float v0, p1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public abstract doQuickScroll(I)V
.end method

.method public freeGlObjects()V
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->clearActiveObject()V

    .line 324
    return-void
.end method

.method public getActiveGroup()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 869
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    return-object v0
.end method

.method public getActiveObject()Landroid/util/SparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;",
            ">;"
        }
    .end annotation

    .prologue
    .line 864
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    return-object v0
.end method

.method public getCenteredScroll(F)F
    .locals 3
    .param p1, "scroll"    # F

    .prologue
    .line 873
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemH:F

    sub-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    sub-float v0, p1, v1

    .line 875
    .local v0, "resScroll":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 876
    const/4 v0, 0x0

    .line 880
    :cond_0
    :goto_0
    return v0

    .line 877
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 878
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_0
.end method

.method public getFocused()I
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    return v0
.end method

.method public getFocusedGroupIndex(F)I
    .locals 6
    .param p1, "scrollRatio"    # F

    .prologue
    .line 1850
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 1851
    .local v0, "activeGrpSize":I
    const/4 v1, -0x1

    .line 1853
    .local v1, "groupIndex":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 1854
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v1

    .line 1855
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getGroupRatio(I)F

    move-result v3

    cmpl-float v3, v3, p1

    if-ltz v3, :cond_1

    .line 1856
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Focused group index: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1860
    :cond_0
    return v1

    .line 1853
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getFocusedObject(II)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1120
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v0

    return-object v0
.end method

.method public getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 10
    .param p1, "x"    # I
    .param p2, "y"    # I
    .param p3, "includeProximity"    # Z

    .prologue
    const/4 v8, 0x0

    .line 1124
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    if-nez v9, :cond_1

    move-object v3, v8

    .line 1159
    :cond_0
    :goto_0
    return-object v3

    .line 1125
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v4, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1126
    .local v4, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    const/4 v6, 0x0

    .line 1127
    .local v6, "retObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v7

    .line 1128
    .local v7, "size":I
    const/high16 v5, -0x40800000    # -1.0f

    .line 1132
    .local v5, "proximity":F
    add-int/lit8 v1, v7, -0x1

    .local v1, "idx":I
    :goto_1
    if-ltz v1, :cond_3

    .line 1133
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1134
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v3, :cond_2

    invoke-virtual {v3, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->checkPosIn(II)Z

    move-result v9

    if-nez v9, :cond_0

    .line 1132
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 1138
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_3
    if-nez p3, :cond_4

    move-object v3, v8

    .line 1139
    goto :goto_0

    .line 1143
    :cond_4
    add-int/lit8 v1, v7, -0x1

    :goto_2
    if-ltz v1, :cond_8

    .line 1144
    :try_start_0
    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1145
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v3, :cond_6

    .line 1143
    :cond_5
    :goto_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 1146
    :cond_6
    invoke-virtual {v3, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getProxity(II)F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1147
    .local v2, "newProximity":F
    const/high16 v9, -0x40800000    # -1.0f

    cmpl-float v9, v5, v9

    if-eqz v9, :cond_7

    cmpg-float v9, v2, v5

    if-gez v9, :cond_5

    .line 1148
    :cond_7
    move v5, v2

    .line 1149
    move-object v6, v3

    goto :goto_3

    .line 1152
    .end local v2    # "newProximity":F
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :catch_0
    move-exception v0

    .line 1153
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v3, v8

    .line 1154
    goto :goto_0

    .line 1156
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_8
    instance-of v9, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-eqz v9, :cond_9

    move-object v3, v6

    .line 1157
    goto :goto_0

    :cond_9
    move-object v3, v8

    .line 1159
    goto :goto_0
.end method

.method public getGroupObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 859
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 860
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    return-object v0
.end method

.method public getGroupPosition(ILcom/sec/android/gallery3d/glcore/GlPos;)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "retPos"    # Lcom/sec/android/gallery3d/glcore/GlPos;

    .prologue
    .line 1847
    return-void
.end method

.method public getGroupRatio(I)F
    .locals 6
    .param p1, "groupIndex"    # I

    .prologue
    .line 1188
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v1, v3, p1

    .line 1189
    .local v1, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v2, v3, v4

    .line 1191
    .local v2, "groupInfo2":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v3, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v4, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float/2addr v3, v4

    iget v4, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v5, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    add-float/2addr v4, v5

    div-float/2addr v3, v4

    .line 1194
    .end local v1    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    .end local v2    # "groupInfo2":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :goto_0
    return v3

    .line 1192
    :catch_0
    move-exception v0

    .line 1193
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 1194
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public getMarginBtm()F
    .locals 1

    .prologue
    .line 1868
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargBtm:F

    return v0
.end method

.method public getMarginTop()F
    .locals 1

    .prologue
    .line 1864
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargTop:F

    return v0
.end method

.method public getObject(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 2
    .param p1, "code"    # I

    .prologue
    .line 852
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 853
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    return-object v0
.end method

.method public abstract getScrollForIndex(I)F
.end method

.method public getTopObject()Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1164
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    if-nez v7, :cond_1

    move-object v4, v6

    .line 1183
    :cond_0
    :goto_0
    return-object v4

    .line 1165
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v3, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 1166
    .local v3, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    const/4 v4, 0x0

    .line 1167
    .local v4, "retObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v5

    .line 1170
    .local v5, "size":I
    if-gtz v5, :cond_2

    move-object v4, v6

    .line 1171
    goto :goto_0

    .line 1173
    :cond_2
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "retObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1174
    .restart local v4    # "retObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget v1, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 1176
    .local v1, "newIndex":I
    add-int/lit8 v0, v5, -0x1

    .local v0, "idx":I
    :goto_1
    const/4 v6, 0x1

    if-lt v0, v6, :cond_0

    .line 1177
    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1178
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget v6, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    if-ge v6, v1, :cond_3

    .line 1179
    iget v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 1180
    move-object v4, v2

    .line 1176
    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public getValidScroll(F)F
    .locals 1
    .param p1, "scroll"    # F

    .prologue
    .line 884
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 885
    const/4 p1, 0x0

    .line 889
    :cond_0
    :goto_0
    return p1

    .line 886
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 887
    iget p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_0
.end method

.method public getVisibleScrollDelta(F)F
    .locals 4
    .param p1, "scroll"    # F

    .prologue
    const/4 v1, 0x0

    .line 893
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    sub-float v0, p1, v2

    .line 895
    .local v0, "resScroll":F
    cmpg-float v2, v0, v1

    if-gez v2, :cond_0

    .line 900
    .end local v0    # "resScroll":F
    :goto_0
    return v0

    .line 897
    .restart local v0    # "resScroll":F
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemH:F

    sub-float/2addr v2, v3

    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    .line 898
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    sub-float v1, v0, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemH:F

    add-float v0, v1, v2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 900
    goto :goto_0
.end method

.method public init(I)V
    .locals 0
    .param p1, "level"    # I

    .prologue
    .line 207
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mLevel:I

    .line 208
    return-void
.end method

.method public initControlCommon(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;
    .locals 1
    .param p1, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 130
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    return-object v0
.end method

.method public initEnv(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)V
    .locals 3
    .param p1, "comCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    .line 137
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGrpCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$000(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    .line 138
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$100(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    .line 139
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .line 140
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$200(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    .line 142
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    invoke-static {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$300(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mResource:Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d02e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckboxWidthPixel:I

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d02e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckboxHeightPixel:I

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0304

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMarginPixel:I

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0305

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMarginPixel:I

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0306

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpCheckVMarginPixel:I

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d02e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpandButtonWidthPixel:I

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d02e6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpandButtonHeightPixel:I

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d030b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMarginPixel:I

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d030c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMarginPixel:I

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getActionBarHeight()I

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0d0307

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginTopPixel:I

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0308

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginBottomPixel:I

    .line 158
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0309

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginLeftPixel:I

    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d030a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginRightPixel:I

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d031f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginTopPixel:I

    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0320

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginBottomPixel:I

    .line 163
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d0321

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeftPixel:I

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d031e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupVGapPixel:I

    .line 166
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d031c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleHeightPixel:I

    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d00f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapHPixel:I

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mResource:Landroid/content/res/Resources;

    const v1, 0x7f0d00f5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapVPixel:I

    .line 171
    return-void
.end method

.method public initScroll(I)F
    .locals 4
    .param p1, "itemCode"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 352
    const/4 v0, 0x0

    .line 354
    .local v0, "scroll":F
    const/4 v1, -0x1

    if-ne p1, v1, :cond_0

    .line 355
    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 365
    :goto_0
    return v2

    .line 360
    :cond_0
    invoke-virtual {p0, v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    goto :goto_0
.end method

.method public interpolateVisibleObjectPosition(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    .line 485
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v5, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCheckBoxVisible:Z

    .line 487
    .local v5, "isCheckBoxVisible":Z
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v7, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 488
    .local v7, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 489
    .local v0, "activeCount":I
    const/4 v4, 0x0

    .local v4, "idx":I
    :goto_0
    if-ge v4, v0, :cond_1

    .line 490
    invoke-virtual {v7, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 491
    .local v6, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v8, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 492
    if-eqz v5, :cond_0

    .line 493
    iget-object v1, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 494
    .local v1, "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v8, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 495
    iget-object v8, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 489
    .end local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 499
    .end local v6    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v3, v8, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 500
    .local v3, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 501
    const/4 v4, 0x0

    :goto_1
    if-ge v4, v0, :cond_4

    .line 502
    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 503
    .local v2, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v8, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 505
    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 506
    .restart local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v1, :cond_2

    .line 507
    iget-object v8, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 509
    :cond_2
    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 510
    if-eqz v1, :cond_3

    .line 511
    iget-object v8, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 501
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 514
    .end local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v2    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_4
    return-void
.end method

.method public isFocusChanged()Z
    .locals 1

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mFocusChangedFlag:Z

    return v0
.end method

.method public abstract moveTo(I)Z
.end method

.method public abstract onCreateThumbObj(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V
.end method

.method public rConvX(F)I
    .locals 1
    .param p1, "x"    # F

    .prologue
    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthViewRatio:F

    div-float v0, p1, v0

    float-to-int v0, v0

    return v0
.end method

.method public rConvY(F)I
    .locals 1
    .param p1, "y"    # F

    .prologue
    .line 265
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightViewRatio:F

    div-float v0, p1, v0

    float-to-int v0, v0

    return v0
.end method

.method public abstract resetAttributes(IZ)V
.end method

.method public resetCount()V
    .locals 6

    .prologue
    .line 332
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCom:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$500(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;

    move-result-object v1

    .line 333
    .local v1, "groupComSet":[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCount:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->access$600(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;)I

    move-result v2

    .line 334
    .local v2, "groupCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    if-eq v5, v2, :cond_1

    .line 335
    :cond_0
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    .line 336
    new-array v5, v2, [Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    iput-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .line 338
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_3

    .line 339
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v3, v5, v4

    .line 340
    .local v3, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    aget-object v0, v1, v4

    .line 341
    .local v0, "groupComInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    if-nez v3, :cond_2

    .line 342
    new-instance v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    .end local v3    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    invoke-direct {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;-><init>()V

    .line 343
    .restart local v3    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aput-object v3, v5, v4

    .line 345
    :cond_2
    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mCount:I

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->setItemCount(I)V

    .line 346
    iget-object v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mRatio:[B

    iput-object v5, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mRatio:[B

    .line 347
    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;->mLineCount:I

    iput v5, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mLineCount:I

    .line 338
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 349
    .end local v0    # "groupComInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupComInfo;
    .end local v3    # "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    :cond_3
    return-void
.end method

.method protected abstract resetItemMaxSize()V
.end method

.method public abstract resetPosition()V
.end method

.method public abstract resetPosition(II)V
.end method

.method protected resetValues(Z)V
    .locals 5
    .param p1, "portraitMode"    # Z

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f000000    # 0.5f

    const v2, 0x3e99999a    # 0.3f

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iput-boolean p1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    .line 270
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->DEF_DISTANCE:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mDefDistance:F

    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidth:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewWidth:I

    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewHeight:I

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mWidthSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mSpaceWidth:F

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mSpaceHeight:F

    .line 276
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginTopPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargTop:F

    .line 277
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginBottomPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargBtm:F

    .line 278
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginLeftPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargLeft:F

    .line 279
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMarginRightPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargRight:F

    .line 281
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapHPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapW:F

    .line 282
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapVPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemGapH:F

    .line 284
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleHeightPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleH:F

    .line 285
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleWidthPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    .line 286
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginTopPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginTop:F

    .line 287
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginBottomPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginBottom:F

    .line 288
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeftPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleTextMarginLeft:F

    .line 290
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupVGapPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupVGap:F

    .line 292
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckboxWidthPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    .line 293
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckboxHeightPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    .line 295
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMarginPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    .line 296
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMarginPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMargin:F

    .line 298
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpCheckVMarginPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpCheckVMargin:F

    .line 300
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpandButtonWidthPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    .line 301
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpandButtonHeightPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    .line 303
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMarginPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMargin:F

    .line 304
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMarginPixel:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMargin:F

    .line 305
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewWidth:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convX(I)F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargLeft:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargRight:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    .line 307
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewHeight:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->convY(I)F

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargTop:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    .line 309
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpW:F

    .line 311
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mSpaceHeight:F

    div-float/2addr v0, v4

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargTop:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSy:F

    .line 312
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mSpaceWidth:F

    neg-float v0, v0

    div-float/2addr v0, v4

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargLeft:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemSx:F

    .line 313
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargTop:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExtraTop:F

    .line 314
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargBtm:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExtraBtm:F

    .line 315
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargLeft:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExtraLeft:F

    .line 316
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mMargRight:F

    add-float/2addr v0, v1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidW:F

    mul-float/2addr v1, v3

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExtraRight:F

    .line 318
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleWidthPixel:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleCanvsW:I

    .line 319
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleHeightPixel:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleCanvsH:I

    .line 320
    return-void
.end method

.method public resetVisibleObjectAttribute()V
    .locals 14

    .prologue
    .line 447
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v12, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mCheckBoxVisible:Z

    .line 449
    .local v12, "isCheckBoxVisible":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v13, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 450
    .local v13, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v13}, Landroid/util/SparseArray;->size()I

    move-result v9

    .line 451
    .local v9, "activeCount":I
    const/4 v11, 0x0

    .local v11, "idx":I
    :goto_0
    if-ge v11, v9, :cond_1

    .line 452
    invoke-virtual {v13, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 453
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    const/4 v1, 0x1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEnableAnim(ZZZZZ)V

    .line 455
    if-eqz v12, :cond_0

    .line 456
    iget-object v1, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 457
    .local v1, "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 458
    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 451
    .end local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 461
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v10, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 462
    .local v10, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v9

    .line 463
    const/4 v11, 0x0

    :goto_1
    if-ge v11, v9, :cond_4

    .line 464
    invoke-virtual {v10, v11}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 465
    .local v2, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    const/4 v3, 0x1

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setEnableAnim(ZZZZZ)V

    .line 467
    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 468
    .restart local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v1, :cond_2

    .line 469
    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 471
    :cond_2
    iget-object v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 472
    if-eqz v1, :cond_3

    .line 473
    const/4 v4, 0x1

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, v1

    invoke-virtual/range {v3 .. v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 463
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 476
    .end local v1    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v2    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_4
    return-void
.end method

.method public setAlwaysActiveGroupObjectIndex(I)V
    .locals 0
    .param p1, "groupIndex"    # I

    .prologue
    .line 1872
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    .line 1873
    return-void
.end method

.method public setCheckBoxVisibility()V
    .locals 15

    .prologue
    const/4 v11, 0x1

    const/4 v14, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    .line 743
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v9, :cond_1

    .line 777
    :cond_0
    return-void

    .line 745
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 746
    .local v4, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v6, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mGroupCheckBoxVisible:Z

    .line 747
    .local v6, "isCheckBoxVisible":Z
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupSelect:Z

    if-eqz v9, :cond_5

    .line 749
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mTitleW:F

    div-float v1, v9, v13

    .line 750
    .local v1, "bx":F
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v9

    add-int/lit8 v5, v9, -0x1

    .local v5, "idx":I
    :goto_0
    if-ltz v5, :cond_5

    .line 751
    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 752
    .local v3, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    iget-object v2, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mGrpCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 753
    .local v2, "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-nez v2, :cond_3

    .line 750
    :cond_2
    :goto_1
    add-int/lit8 v5, v5, -0x1

    goto :goto_0

    .line 756
    :cond_3
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateGroupCheckBox(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 757
    const/4 v9, 0x0

    invoke-virtual {v2, v6, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(ZI)V

    .line 759
    if-eqz v6, :cond_2

    .line 760
    iget-object v2, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 761
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mIsRTL:Z

    if-eqz v9, :cond_4

    .line 762
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    sub-float v9, v1, v9

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    mul-float/2addr v10, v13

    sub-float/2addr v9, v10

    invoke-virtual {v2, v9, v14, v14, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    goto :goto_1

    .line 764
    :cond_4
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    add-float/2addr v9, v1

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    mul-float/2addr v10, v13

    add-float/2addr v9, v10

    invoke-virtual {v2, v9, v14, v14, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFFI)V

    goto :goto_1

    .line 769
    .end local v1    # "bx":F
    .end local v2    # "cObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v3    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v5    # "idx":I
    :cond_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v8, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 770
    .local v8, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v8}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 771
    .local v0, "activeCount":I
    const/4 v5, 0x0

    .restart local v5    # "idx":I
    :goto_2
    if-ge v5, v0, :cond_0

    .line 772
    invoke-virtual {v8, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 773
    .local v7, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemW:F

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionW:F

    sub-float/2addr v10, v11

    div-float/2addr v10, v13

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionHMargin:F

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemH:F

    neg-float v11, v11

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionH:F

    add-float/2addr v11, v12

    div-float/2addr v11, v13

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mExpansionVMargin:F

    add-float/2addr v11, v12

    invoke-virtual {v9, v10, v11, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 774
    iget-object v9, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemW:F

    neg-float v10, v10

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckW:F

    add-float/2addr v10, v11

    div-float/2addr v10, v13

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckHMargin:F

    add-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemH:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckH:F

    sub-float/2addr v11, v12

    div-float/2addr v11, v13

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCheckVMargin:F

    sub-float/2addr v11, v12

    invoke-virtual {v9, v10, v11, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 775
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    .line 771
    add-int/lit8 v5, v5, 0x1

    goto :goto_2
.end method

.method public setFocusBorderVisible(II)V
    .locals 2
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 781
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 782
    .local v0, "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_0

    .line 783
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 785
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 786
    .restart local v0    # "thumbObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v0, :cond_1

    .line 787
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V

    .line 789
    :cond_1
    return-void
.end method

.method protected setFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Z)V
    .locals 1
    .param p1, "thumbObj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .param p2, "visible"    # Z

    .prologue
    .line 792
    if-nez p1, :cond_0

    .line 801
    :goto_0
    return-void

    .line 794
    :cond_0
    if-eqz p2, :cond_1

    .line 795
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setFocusBorderVisible(Z)V

    .line 796
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateBorder(I)V

    goto :goto_0

    .line 798
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setFocusBorderVisible(Z)V

    .line 799
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateBorder(I)V

    goto :goto_0
.end method

.method public setFocused(IZ)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "applyNow"    # Z

    .prologue
    .line 204
    return-void
.end method

.method public setMaxScrollable()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 908
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    if-gtz v3, :cond_0

    .line 909
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    .line 919
    :goto_0
    return-void

    .line 912
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v3, v4

    .line 913
    .local v0, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAccu:F

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mScrlAmount:F

    add-float v2, v3, v4

    .line 914
    .local v2, "scrollable":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mValidH:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mScrollTopMargine:F

    sub-float v1, v3, v4

    .line 915
    .local v1, "scrollMax":F
    cmpg-float v3, v2, v1

    if-gtz v3, :cond_1

    .line 916
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_0

    .line 918
    :cond_1
    sub-float v3, v2, v1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_0
.end method

.method public setScroll(FZ)V
    .locals 13
    .param p1, "scroll"    # F
    .param p2, "layoutChanged"    # Z

    .prologue
    .line 369
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .line 370
    .local v2, "activeStart":I
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    .line 371
    .local v1, "activeEnd":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    .line 372
    .local v5, "contentStart":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    .line 373
    .local v4, "contentEnd":I
    const/4 v0, 0x0

    .line 374
    .local v0, "activeChanged":Z
    const/4 v3, 0x0

    .line 375
    .local v3, "contentChanged":Z
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    .line 378
    .local v7, "oldScroll":F
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    .line 379
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    if-gtz v8, :cond_1

    .line 380
    const/4 v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .line 381
    const/4 v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    .line 396
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleRange()V

    .line 385
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    if-ne v2, v8, :cond_2

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    if-eq v1, v8, :cond_7

    :cond_2
    const/4 v0, 0x1

    .line 386
    :goto_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    if-ne v5, v8, :cond_3

    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    if-eq v4, v8, :cond_8

    :cond_3
    const/4 v3, 0x1

    .line 387
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v8, :cond_5

    if-nez v0, :cond_4

    if-eqz v3, :cond_5

    .line 388
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setActiveWindow(IIII)V

    .line 390
    :cond_5
    if-nez v0, :cond_6

    if-eqz p2, :cond_a

    .line 391
    :cond_6
    if-eqz p2, :cond_9

    const/4 v6, 0x1

    .line 392
    .local v6, "flag":I
    :goto_3
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    invoke-virtual {p0, v8, v9, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleObject(III)V

    goto :goto_0

    .line 385
    .end local v6    # "flag":I
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 386
    :cond_8
    const/4 v3, 0x0

    goto :goto_2

    .line 391
    :cond_9
    const/4 v6, 0x0

    goto :goto_3

    .line 393
    :cond_a
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    cmpl-float v8, v8, v7

    if-eqz v8, :cond_0

    .line 394
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleObjectPosition()V

    goto :goto_0
.end method

.method public setScrollRef(FF)V
    .locals 6
    .param p1, "scroll"    # F
    .param p2, "scrollRef"    # F

    .prologue
    const/4 v5, -0x1

    .line 404
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    .line 405
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iput p2, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    .line 406
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    if-gtz v4, :cond_0

    .line 407
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .line 408
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    .line 422
    :goto_0
    return-void

    .line 411
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleRange()V

    .line 412
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleRange()V

    .line 413
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 414
    .local v1, "newActiveStart":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 415
    .local v0, "newActiveEnd":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentStart:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 416
    .local v3, "newContentStart":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpContentEnd:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 417
    .local v2, "newContentEnd":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v4, :cond_1

    .line 418
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v4, v1, v0, v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setActiveWindow(IIII)V

    .line 420
    :cond_1
    const/4 v4, 0x2

    invoke-virtual {p0, v1, v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleObject(III)V

    .line 421
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mRefer:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/16 v5, 0xc

    invoke-virtual {v4, v1, v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setVisibleObject(III)V

    goto :goto_0
.end method

.method protected setThumbObjectCanvas(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;)Z
    .locals 6
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .param p2, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    .line 1003
    iget-object v0, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 1005
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    iget v3, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    if-eqz v3, :cond_0

    .line 1006
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v4, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->getBrokenItemCanvas(I)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v1

    .line 1007
    .local v1, "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1019
    .end local v1    # "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    :goto_0
    return v2

    .line 1008
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1009
    :cond_1
    iget v3, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    .line 1010
    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1011
    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 1012
    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1013
    const/4 v2, 0x0

    goto :goto_0

    .line 1015
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mCanvasMgr:Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlCanvasManager;->findInstance(II)Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v1

    .line 1016
    .restart local v1    # "canvas":Lcom/sec/android/gallery3d/glcore/GlCanvas;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1017
    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    goto :goto_0
.end method

.method public setTitleFocusBorderVisible(II)V
    .locals 2
    .param p1, "oldIndex"    # I
    .param p2, "newIndex"    # I

    .prologue
    .line 805
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 806
    .local v0, "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 807
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 810
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 811
    .restart local v0    # "titleObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_1

    .line 812
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V

    .line 814
    :cond_1
    return-void
.end method

.method protected setTitleFocusBorderVisible(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)V
    .locals 2
    .param p1, "titleObj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p2, "visible"    # Z

    .prologue
    .line 817
    if-nez p1, :cond_0

    .line 829
    :goto_0
    return-void

    .line 820
    :cond_0
    if-eqz p2, :cond_1

    .line 821
    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocusBorderVisible(Z)V

    .line 822
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateTitle(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 827
    :goto_1
    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v0, v1, 0x10

    .line 828
    .local v0, "index":I
    invoke-direct {p0, p2, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setGrpChkBorderVisible(ZI)V

    goto :goto_0

    .line 824
    .end local v0    # "index":I
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setFocusBorderVisible(Z)V

    .line 825
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateTitle(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    goto :goto_1
.end method

.method public setToCurrentCtrl()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 215
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    if-nez v9, :cond_0

    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsDecorAlwaysVisible:Z

    if-eqz v9, :cond_3

    :cond_0
    move v4, v8

    .line 217
    .local v4, "isDecorVisible":Z
    :goto_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    if-ne v9, v8, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-nez v9, :cond_1

    .line 218
    const/4 v4, 0x0

    .line 220
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v6, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    .line 221
    .local v6, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual {v6}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 223
    .local v0, "activeCount":I
    const/4 v3, 0x0

    .local v3, "idx":I
    :goto_1
    if-ge v3, v0, :cond_7

    .line 224
    invoke-virtual {v6, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 225
    .local v5, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-nez v9, :cond_4

    .line 223
    :cond_2
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "activeCount":I
    .end local v3    # "idx":I
    .end local v4    # "isDecorVisible":Z
    .end local v5    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .end local v6    # "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    :cond_3
    move v4, v7

    .line 215
    goto :goto_0

    .line 228
    .restart local v0    # "activeCount":I
    .restart local v3    # "idx":I
    .restart local v4    # "isDecorVisible":Z
    .restart local v5    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    .restart local v6    # "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    :cond_4
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mHideIconMinLevel:Z

    if-eqz v9, :cond_2

    .line 229
    # invokes: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getExceptViews()Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->access$400(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Ljava/util/ArrayList;

    move-result-object v2

    .line 230
    .local v2, "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    if-eqz v4, :cond_5

    .line 231
    iget-object v1, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 232
    .local v1, "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {v1, v7, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->setChildVisibility(ILjava/util/ArrayList;)V

    .line 233
    invoke-virtual {v5, v8, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_2

    .line 234
    .end local v1    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_5
    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_6

    .line 235
    iget-object v9, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v9, v8, v2}, Lcom/sec/android/gallery3d/glcore/GlView;->setChildVisibility(ILjava/util/ArrayList;)V

    .line 236
    invoke-virtual {v5, v8, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_2

    .line 238
    :cond_6
    invoke-virtual {v5, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvasSubVisiblity(ZI)V

    goto :goto_2

    .line 242
    .end local v2    # "exceptViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/glcore/GlView;>;"
    .end local v5    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_7
    return-void
.end method

.method public setVisibleObject(III)V
    .locals 22
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "flag"    # I

    .prologue
    .line 667
    const/4 v9, 0x0

    .line 671
    .local v9, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    const/4 v4, -0x1

    .local v4, "curGrpIdx":I
    const/4 v5, 0x0

    .line 674
    .local v5, "curGrpMax":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v10, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    .line 675
    .local v10, "grpObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;>;"
    shr-int/lit8 v8, p1, 0x10

    .line 676
    .local v8, "groupStart":I
    shr-int/lit8 v6, p2, 0x10

    .line 679
    .local v6, "groupEnd":I
    and-int/lit8 v19, p3, 0x8

    if-nez v19, :cond_b

    .line 680
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    move-object/from16 v18, v0

    .line 681
    .local v18, "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    invoke-virtual/range {v18 .. v18}, Landroid/util/SparseArray;->size()I

    move-result v19

    add-int/lit8 v12, v19, -0x1

    .local v12, "idx":I
    :goto_0
    if-ltz v12, :cond_8

    .line 682
    move-object/from16 v0, v18

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 683
    .local v17, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-nez v17, :cond_1

    .line 681
    :cond_0
    :goto_1
    add-int/lit8 v12, v12, -0x1

    goto :goto_0

    .line 684
    :cond_1
    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v19, v0

    if-gez v19, :cond_2

    .line 685
    sget-object v19, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v21, "setVisibleObject continue! obj.mIndex:"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 688
    :cond_2
    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v19, v0

    shr-int/lit8 v15, v19, 0x10

    .line 689
    .local v15, "newGrpIdx":I
    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v19, v0

    const v20, 0xffff

    and-int v16, v19, v20

    .line 691
    .local v16, "newItmIdx":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    move/from16 v19, v0

    const/16 v20, -0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAlwaysActiveGroupObjectIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-eq v15, v0, :cond_0

    .line 695
    :cond_3
    if-eq v4, v15, :cond_5

    .line 696
    move v4, v15

    .line 697
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCount:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ge v4, v0, :cond_4

    if-gez v4, :cond_7

    :cond_4
    const/4 v5, 0x0

    .line 698
    :goto_2
    invoke-virtual {v10, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    check-cast v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 701
    .restart local v9    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_5
    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, p1

    if-lt v0, v1, :cond_6

    move-object/from16 v0, v17

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, p2

    if-ge v0, v1, :cond_6

    move/from16 v0, v16

    if-gt v5, v0, :cond_0

    .line 702
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->inActivateObject(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 703
    if-eqz v9, :cond_0

    .line 704
    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->remove(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    goto/16 :goto_1

    .line 697
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-object/from16 v19, v0

    aget-object v19, v19, v4

    move-object/from16 v0, v19

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    goto :goto_2

    .line 708
    .end local v15    # "newGrpIdx":I
    .end local v16    # "newItmIdx":I
    .end local v17    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_8
    invoke-virtual {v10}, Landroid/util/SparseArray;->size()I

    move-result v19

    add-int/lit8 v12, v19, -0x1

    :goto_3
    if-ltz v12, :cond_b

    .line 709
    invoke-virtual {v10, v12}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    check-cast v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 710
    .restart local v9    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$700(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)I

    move-result v19

    move/from16 v0, v19

    if-lt v0, v8, :cond_9

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mIndex:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$700(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)I

    move-result v19

    move/from16 v0, v19

    if-le v0, v6, :cond_a

    .line 711
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->inActivateGroup(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;)V

    .line 708
    :cond_a
    add-int/lit8 v12, v12, -0x1

    goto :goto_3

    .line 717
    .end local v12    # "idx":I
    .end local v18    # "objs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;>;"
    :cond_b
    const v19, 0xffff

    and-int v14, p1, v19

    .line 718
    .local v14, "itemStart":I
    const v19, 0xffff

    and-int v13, p2, v19

    .line 719
    .local v13, "itemEnd":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    array-length v0, v0

    move/from16 v19, v0

    move/from16 v0, v19

    if-gt v0, v8, :cond_d

    .line 731
    :cond_c
    return-void

    .line 721
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-object/from16 v19, v0

    aget-object v7, v19, v8

    .line 722
    .local v7, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    if-ne v8, v6, :cond_e

    .line 723
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v8, v14, v13, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->activeObjectRange(IIII)V

    .line 728
    :goto_4
    add-int/lit8 v11, v8, 0x1

    .local v11, "i":I
    :goto_5
    if-ge v11, v6, :cond_c

    .line 729
    const/16 v19, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    move-object/from16 v20, v0

    aget-object v20, v20, v11

    move-object/from16 v0, v20

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, p3

    invoke-direct {v0, v11, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->activeObjectRange(IIII)V

    .line 728
    add-int/lit8 v11, v11, 0x1

    goto :goto_5

    .line 725
    .end local v11    # "i":I
    :cond_e
    iget v0, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, p3

    invoke-direct {v0, v8, v14, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->activeObjectRange(IIII)V

    .line 726
    const/16 v19, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v19

    move/from16 v2, p3

    invoke-direct {v0, v6, v1, v13, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->activeObjectRange(IIII)V

    goto :goto_4
.end method

.method public abstract setVisibleRange()V
.end method

.method public update(II)Z
    .locals 6
    .param p1, "index"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v2, 0x0

    .line 937
    if-eqz p2, :cond_2

    .line 938
    const/4 v3, 0x2

    if-ne p2, v3, :cond_1

    .line 939
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->startEnlargeAnimation()V

    .line 954
    :cond_0
    :goto_0
    return v2

    .line 941
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update wrong type = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 945
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v3, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 946
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->isActive()Z

    move-result v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    .line 949
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    .line 950
    .local v1, "shrinkAnim":Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;
    if-eqz v1, :cond_3

    iget-boolean v3, v1, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v3, :cond_3

    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-ne v3, v0, :cond_3

    .line 951
    sget-object v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "update skipped for shrink 1= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 954
    :cond_3
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    move-result v2

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z
    .locals 5
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v1, 0x0

    .line 960
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->isActive()Z

    move-result v2

    if-nez v2, :cond_1

    .line 968
    :cond_0
    :goto_0
    return v1

    .line 963
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    .line 964
    .local v0, "shrinkAnim":Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;
    if-eqz v0, :cond_2

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-ne v2, p1, :cond_2

    .line 965
    sget-object v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "update skipped for shrink 2= "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 968
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    move-result v1

    goto :goto_0
.end method

.method public updateBorder(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 1113
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1114
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 1117
    :cond_0
    :goto_0
    return-void

    .line 1116
    :cond_1
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    goto :goto_0
.end method

.method public updateCheckBox(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 1095
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v4, :cond_1

    .line 1109
    :cond_0
    :goto_0
    return-void

    .line 1097
    :cond_1
    shr-int/lit8 v0, p1, 0x10

    .line 1098
    .local v0, "albumIdx":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseGroupSelect:Z

    if-eqz v4, :cond_2

    .line 1099
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v5, -0x1

    invoke-virtual {v4, v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCurrentState(II)I

    move-result v4

    if-lez v4, :cond_3

    const/4 v3, 0x1

    .line 1100
    .local v3, "selected":Z
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 1101
    .local v1, "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v1, :cond_2

    .line 1102
    # invokes: Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->setSelected(Z)V
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->access$900(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;Z)V

    .line 1105
    .end local v1    # "grpObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v3    # "selected":Z
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 1106
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    if-eqz v2, :cond_0

    .line 1108
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z

    goto :goto_0

    .line 1099
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method protected updateThumbnail(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)Z
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .prologue
    const/4 v3, 0x0

    .line 975
    iget v7, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    .line 976
    .local v7, "index":I
    shr-int/lit8 v1, v7, 0x10

    .line 977
    .local v1, "albumIdx":I
    const v0, 0xffff

    and-int v2, v7, v0

    .line 978
    .local v2, "photoIdx":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    array-length v0, v0

    if-lt v1, v0, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v3

    .line 980
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    aget-object v6, v0, v1

    .line 981
    .local v6, "groupInfo":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
    iget v0, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-ge v2, v0, :cond_0

    .line 983
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    aget v4, v4, v2

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 984
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    aget v4, v4, v2

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 985
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseItemSelect:Z

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    .line 986
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mDisplaySelectedCount:Z

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispSelectCnt:Z

    .line 987
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v4, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 988
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 989
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mAdapterInter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getFocusBorderVisible()Z

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 990
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->applyThumbnailBitmap(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;)V

    .line 991
    iget v0, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mRotation:I

    neg-int v0, v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVertexRotation(I)V

    .line 992
    const/4 v3, 0x1

    goto :goto_0
.end method
