.class Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;
.super Landroid/content/BroadcastReceiver;
.source "SASlideShowService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, -0x1

    .line 117
    if-nez p2, :cond_1

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 120
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "VideoUri"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 122
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "VideoUri"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mUri:Landroid/net/Uri;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$002(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Landroid/net/Uri;)Landroid/net/Uri;

    .line 124
    :cond_2
    const-string v2, "ACTION_PRESENTATION_VIDEO_VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 125
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 126
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/glview/GlSlideShowView;)V

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$102(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    .line 129
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 131
    .local v1, "params":Landroid/widget/FrameLayout$LayoutParams;
    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mUri:Landroid/net/Uri;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->setUri(Landroid/net/Uri;)V

    .line 133
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->addCustomView(Landroid/view/View;Landroid/widget/FrameLayout$LayoutParams;)V

    .line 137
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->bringToFront()V

    .line 138
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    goto/16 :goto_0

    .line 140
    .end local v1    # "params":Landroid/widget/FrameLayout$LayoutParams;
    :cond_4
    const-string v2, "ACTION_REMOVE_PRESENTATION_VIDEO_VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 141
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewManager;

    if-eqz v2, :cond_5

    .line 142
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideshowPresentationVideoView:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    move-result-object v3

    invoke-interface {v2, v3}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    .line 144
    :cond_5
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 145
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mSlideShowViewState:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$200()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->playSlideshowMusic()V

    .line 146
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    const/4 v3, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->isSlideshowVideoPlaying:Z
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$402(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;Z)Z

    goto/16 :goto_0

    .line 147
    :cond_7
    const-string v2, "ACTION_ADD_GLSURFACE_VIEW"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 148
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 149
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->mPresentation:Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService;)Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SASlideShowService$SlideShowViewPresentation;->reInitializeGLSurfaceView()V

    goto/16 :goto_0
.end method
