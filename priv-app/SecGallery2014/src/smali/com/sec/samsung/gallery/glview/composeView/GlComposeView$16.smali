.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mSourceY:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1

    .prologue
    .line 1382
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1383
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->mSourceY:F

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    const/16 v6, 0x9

    const/4 v2, 0x0

    .line 1397
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isShowingHelpView()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1398
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->mSourceY:F

    int-to-float v4, p3

    add-float v0, v3, v4

    .line 1399
    .local v0, "currentY":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeight:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->rConvY(F)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollBarHeight()I

    move-result v4

    sub-int/2addr v3, v4

    int-to-float v1, v3

    .line 1401
    .local v1, "scrollArea":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->calcuScrollRatio(FF)V

    .line 1403
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/glcore/GlHandler;->hasMessage(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1404
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v4, 0x2

    invoke-virtual {v3, v6, v4, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1407
    :cond_0
    const/4 v2, 0x1

    .line 1409
    .end local v0    # "currentY":F
    .end local v1    # "scrollArea":F
    :cond_1
    return v2
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 7
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x1

    .line 1387
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHeightSpace:F

    div-float/2addr v1, v4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getY()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getMarginTop()F

    move-result v2

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->getHeight(Z)F

    move-result v2

    div-float/2addr v2, v4

    sub-float v0, v1, v2

    .line 1388
    .local v0, "objY":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->rConvY(F)I

    move-result v1

    int-to-float v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->mSourceY:F

    .line 1389
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glcore/GlHandler;->hasMessage(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isShowingHelpView()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1390
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v1, v6, v3, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1392
    :cond_0
    return v3
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 4
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    const/4 v3, 0x0

    .line 1416
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnscroll:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isShowingHelpView()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1417
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$16;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v1, 0x8

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1419
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
