.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;
.super Landroid/widget/SimpleCursorAdapter;
.source "GallerySearchActionBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "HistoryAdapter"
.end annotation


# instance fields
.field private mFrom:[I

.field private mTo:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "flags"    # I

    .prologue
    .line 415
    invoke-direct/range {p0 .. p6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 416
    iput-object p5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mTo:[I

    .line 417
    invoke-direct {p0, p3, p4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->findColumns(Landroid/database/Cursor;[Ljava/lang/String;)V

    .line 419
    return-void
.end method

.method private findColumns(Landroid/database/Cursor;[Ljava/lang/String;)V
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "from"    # [Ljava/lang/String;

    .prologue
    .line 505
    if-eqz p1, :cond_2

    .line 507
    array-length v0, p2

    .line 508
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    array-length v2, v2

    if-eq v2, v0, :cond_1

    .line 509
    :cond_0
    new-array v2, v0, [I

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    .line 511
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 512
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    aget-object v3, p2, v1

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v1

    .line 511
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 515
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    .line 517
    :cond_3
    return-void
.end method

.method private getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 12
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/CharSequence;
    .param p3, "queryText"    # Ljava/lang/String;

    .prologue
    const v11, 0x7f0b0063

    const/16 v10, 0x21

    const/4 v9, -0x1

    .line 465
    if-nez p2, :cond_0

    .line 466
    const-string p2, ""

    .line 469
    :cond_0
    if-eqz p3, :cond_4

    .line 471
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v7

    invoke-virtual {p3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v8

    invoke-static {v7, p2, v8}, Landroid/text/TextUtils;->getPrefixCharForIndian(Landroid/text/TextPaint;Ljava/lang/CharSequence;[C)[C

    move-result-object v4

    .line 472
    .local v4, "spanChar":[C
    if-eqz v4, :cond_2

    .line 473
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 474
    .local v5, "spanString":Landroid/text/SpannableString;
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    .line 475
    .local v6, "startpoint":I
    const/4 v0, 0x0

    .line 477
    .local v0, "endpoint":I
    array-length v7, v4

    if-eqz v7, :cond_1

    if-eq v6, v9, :cond_1

    .line 478
    array-length v7, v4

    add-int v0, v6, v7

    .line 479
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v5, v7, v6, v0, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 501
    .end local v0    # "endpoint":I
    .end local v4    # "spanChar":[C
    .end local v5    # "spanString":Landroid/text/SpannableString;
    .end local v6    # "startpoint":I
    :cond_1
    :goto_0
    return-object v5

    .line 483
    .restart local v4    # "spanChar":[C
    :cond_2
    const-string v7, "[  ]+"

    invoke-static {v7}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    .line 484
    .local v2, "p":Ljava/util/regex/Pattern;
    invoke-virtual {v2, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    .line 485
    const-string v7, " "

    invoke-virtual {p3, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 486
    const/4 v7, 0x1

    invoke-virtual {p3, v7}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p3

    .line 488
    :cond_3
    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 490
    .local v1, "index":I
    if-eq v1, v9, :cond_4

    .line 492
    new-instance v3, Landroid/text/SpannableString;

    invoke-direct {v3, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 493
    .local v3, "span":Landroid/text/SpannableString;
    new-instance v7, Landroid/text/style/ForegroundColorSpan;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8, v11}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    invoke-direct {v7, v8}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v8

    add-int/2addr v8, v1

    invoke-virtual {v3, v7, v1, v8, v10}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    move-object v5, v3

    .line 495
    goto :goto_0

    .line 501
    .end local v1    # "index":I
    .end local v2    # "p":Ljava/util/regex/Pattern;
    .end local v3    # "span":Landroid/text/SpannableString;
    .end local v4    # "spanChar":[C
    :cond_4
    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, p2}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 14
    .param p1, "view"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->getViewBinder()Landroid/widget/SimpleCursorAdapter$ViewBinder;

    move-result-object v1

    .line 423
    .local v1, "binder":Landroid/widget/SimpleCursorAdapter$ViewBinder;
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mTo:[I

    array-length v3, v11

    .line 424
    .local v3, "count":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mFrom:[I

    .line 425
    .local v6, "from":[I
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->mTo:[I

    .line 426
    .local v9, "to":[I
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    .line 428
    .local v4, "cursorColumns":I
    add-int/lit8 v11, v4, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 429
    .local v5, "filterText":Ljava/lang/String;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v3, :cond_6

    .line 430
    aget v11, v9, v7

    invoke-virtual {p1, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    .line 431
    .local v10, "v":Landroid/view/View;
    if-eqz v10, :cond_2

    .line 432
    const/4 v2, 0x0

    .line 433
    .local v2, "bound":Z
    if-eqz v1, :cond_0

    .line 434
    aget v11, v6, v7

    move-object/from16 v0, p3

    invoke-interface {v1, v10, v0, v11}, Landroid/widget/SimpleCursorAdapter$ViewBinder;->setViewValue(Landroid/view/View;Landroid/database/Cursor;I)Z

    move-result v2

    .line 437
    :cond_0
    if-nez v2, :cond_2

    .line 438
    aget v11, v6, v7

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 439
    .local v8, "text":Ljava/lang/String;
    if-nez v8, :cond_1

    .line 440
    const-string v8, ""

    .line 443
    :cond_1
    instance-of v11, v10, Landroid/widget/TextView;

    if-eqz v11, :cond_4

    .line 444
    const/4 v11, 0x1

    if-ne v7, v11, :cond_3

    if-eqz v5, :cond_3

    const-string v11, ""

    invoke-virtual {v5, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_3

    .line 445
    check-cast v10, Landroid/widget/TextView;

    .end local v10    # "v":Landroid/view/View;
    invoke-virtual {p0, v10, v8, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    .end local v2    # "bound":Z
    .end local v8    # "text":Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 447
    .restart local v2    # "bound":Z
    .restart local v8    # "text":Ljava/lang/String;
    .restart local v10    # "v":Landroid/view/View;
    :cond_3
    check-cast v10, Landroid/widget/TextView;

    .end local v10    # "v":Landroid/view/View;
    invoke-virtual {p0, v10, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->setViewText(Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_1

    .line 449
    .restart local v10    # "v":Landroid/view/View;
    :cond_4
    instance-of v11, v10, Landroid/widget/ImageView;

    if-eqz v11, :cond_5

    .line 450
    check-cast v10, Landroid/widget/ImageView;

    .end local v10    # "v":Landroid/view/View;
    invoke-virtual {p0, v10, v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->setViewImage(Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_1

    .line 452
    .restart local v10    # "v":Landroid/view/View;
    :cond_5
    new-instance v11, Ljava/lang/IllegalStateException;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " is not a "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " view that can be bounds by this SimpleCursorAdapter"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v11

    .line 458
    .end local v2    # "bound":Z
    .end local v8    # "text":Ljava/lang/String;
    .end local v10    # "v":Landroid/view/View;
    :cond_6
    return-void
.end method

.method public setViewText(Landroid/widget/TextView;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/String;
    .param p3, "filter"    # Ljava/lang/String;

    .prologue
    .line 460
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$HistoryAdapter;->getColoredSpannableString(Landroid/widget/TextView;Ljava/lang/CharSequence;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    return-void
.end method
