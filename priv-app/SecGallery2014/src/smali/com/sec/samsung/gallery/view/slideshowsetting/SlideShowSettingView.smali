.class public Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/slideshowsetting/ISlideShowSettingViewContainer;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;
    }
.end annotation


# static fields
.field private static final ACTION_PICK_SOUND:Ljava/lang/String; = "com.sec.android.music.intent.action.SOUND_PICKER"

.field public static LOAD_MUSIC_NONE_SELECTED:Z = false

.field private static final SLIDESHOW_FILTER_GREY:I = 0x0

.field private static final SLIDESHOW_FILTER_SEPIA:I = 0x2

.field private static final SLIDESHOW_FILTER_SIMPLE:I = 0x1

.field private static final SLIDESHOW_FILTER_VINTAGE:I = 0x3

.field private static final TAG:Ljava/lang/String;

.field private static mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;


# instance fields
.field filterClickListener:Landroid/view/View$OnClickListener;

.field filterFocusListener:Landroid/view/View$OnFocusChangeListener;

.field private filterViews:[Landroid/widget/ImageView;

.field filter_linear_layout:Landroid/widget/LinearLayout;

.field filter_view:Landroid/widget/HorizontalScrollView;

.field grayscale_image:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mGalleryActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

.field private mMusicLayout:Landroid/widget/RelativeLayout;

.field private mMusicTitleView:Landroid/widget/TextView;

.field private mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field private mPreviousEffectSelected:I

.field private mRootView:Landroid/view/View;

.field private mVideoCheckedTextView:Landroid/widget/CheckedTextView;

.field private mfilter:I

.field sepia_image:Landroid/widget/ImageView;

.field simple_image:Landroid/widget/ImageView;

.field vintage_image:Landroid/widget/ImageView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->TAG:Ljava/lang/String;

    .line 69
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mPreviousEffectSelected:I

    .line 145
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$3;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    .line 193
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$4;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    .line 247
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    .line 500
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    .line 593
    return-void
.end method

.method static synthetic access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .prologue
    .line 43
    sput-object p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    return-object p0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .prologue
    .line 43
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;
    .param p1, "x1"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mfilter:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)[Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/widget/CheckedTextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;

    return-object v0
.end method

.method private setNormalBackground()V
    .locals 4

    .prologue
    const v3, 0x7f0200d4

    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 312
    return-void
.end method

.method private setupEffectView(Landroid/content/Context;Landroid/view/View;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 79
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090092

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 85
    .local v7, "stringArray":[Ljava/lang/String;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v6, :cond_0

    aget-object v5, v0, v4

    .line 86
    .local v5, "item":Ljava/lang/String;
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 89
    .end local v5    # "item":Ljava/lang/String;
    :cond_0
    new-instance v3, Landroid/widget/ArrayAdapter;

    const v8, 0x7f0300d8

    invoke-direct {v3, p1, v8, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 92
    .local v3, "effectsAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v8, 0x7f0f00c8

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/touchwiz/widget/TwListView;

    .line 94
    .local v2, "effectList":Lcom/sec/android/touchwiz/widget/TwListView;
    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 95
    sget-object v8, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getEffect()I

    move-result v8

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Lcom/sec/android/touchwiz/widget/TwListView;->setItemChecked(IZ)V

    .line 96
    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwListView;->getCheckedItemPosition()I

    move-result v8

    iput v8, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mPreviousEffectSelected:I

    .line 97
    new-instance v8, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$1;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$1;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    invoke-virtual {v2, v8}, Lcom/sec/android/touchwiz/widget/TwListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 104
    return-void
.end method

.method private setupFilterView(Landroid/content/Context;Landroid/view/View;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f020564

    const v7, 0x7f020565

    const/4 v6, 0x1

    .line 315
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowFilter:Z

    if-nez v3, :cond_1

    .line 316
    sget-object v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->TAG:Ljava/lang/String;

    const-string v4, "filter is not supported"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    const v3, 0x7f0f0225

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    .line 467
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    const v3, 0x7f0f0227

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/HorizontalScrollView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    .line 328
    const v3, 0x7f0f022c

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    .line 330
    const v3, 0x7f0f022a

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    .line 332
    const v3, 0x7f0f022e

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    .line 334
    const v3, 0x7f0f0230

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    .line 338
    const v3, 0x7f0f0228

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/LinearLayout;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_linear_layout:Landroid/widget/LinearLayout;

    .line 341
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    aput-object v4, v3, v9

    .line 342
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    aput-object v4, v3, v6

    .line 343
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    const/4 v4, 0x2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    aput-object v5, v3, v4

    .line 344
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;

    const/4 v4, 0x3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    aput-object v5, v3, v4

    .line 345
    const-string v3, "SLIDE_SHOW_FILTER_PREFS"

    invoke-virtual {p1, v3, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 347
    .local v2, "preferences":Landroid/content/SharedPreferences;
    const-string v3, "FILTER"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 349
    .local v0, "filter":I
    packed-switch v0, :pswitch_data_0

    .line 377
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 378
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 379
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 380
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 384
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 385
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 386
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 388
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 389
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 390
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 391
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 393
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_linear_layout:Landroid/widget/LinearLayout;

    if-eqz v3, :cond_2

    .line 394
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_linear_layout:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterFocusListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 398
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    if-eqz v3, :cond_0

    .line 399
    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;Landroid/content/Context;)V

    .line 465
    .local v1, "filterOnKeyListener":Landroid/view/View$OnKeyListener;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3, v1}, Landroid/widget/HorizontalScrollView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto/16 :goto_0

    .line 351
    .end local v1    # "filterOnKeyListener":Landroid/view/View$OnKeyListener;
    :pswitch_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 353
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    const v4, 0x7f0200d4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 354
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 357
    :pswitch_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 359
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 360
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 363
    :pswitch_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 365
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 366
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 369
    :pswitch_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 371
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v8}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 372
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    goto/16 :goto_1

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private setupMusicView(Landroid/content/Context;Landroid/view/View;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 503
    sget-object v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 505
    .local v1, "music":Ljava/lang/String;
    const v4, 0x7f0f0236

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicLayout:Landroid/widget/RelativeLayout;

    .line 508
    const v4, 0x7f0f0238

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 510
    const v4, 0x7f0f0239

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    .line 511
    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 512
    .local v3, "musicUri":Landroid/net/Uri;
    invoke-static {p1, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 514
    .local v2, "musicTitle":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 515
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setSelected(Z)V

    .line 516
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 517
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    sget-object v5, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 518
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v6}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    .line 520
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicLayout:Landroid/widget/RelativeLayout;

    new-instance v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$8;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$8;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 528
    sget-object v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 530
    .local v0, "isMusicEnabled":Z
    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-direct {v4, p0, p1, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;Landroid/content/Context;Z)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    .line 533
    return-void
.end method

.method private setupSpeedView(Landroid/content/Context;Landroid/view/View;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v1, "array":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f090093

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v7

    .line 114
    .local v7, "stringArray":[Ljava/lang/String;
    move-object v0, v7

    .local v0, "arr$":[Ljava/lang/String;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v3, v0, v2

    .line 115
    .local v3, "item":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 118
    .end local v3    # "item":Ljava/lang/String;
    :cond_0
    new-instance v5, Landroid/widget/ArrayAdapter;

    const v8, 0x7f0300d8

    invoke-direct {v5, p1, v8, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 121
    .local v5, "speedAdapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/String;>;"
    const v8, 0x7f0f0224

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/sec/android/touchwiz/widget/TwListView;

    .line 123
    .local v6, "speedList":Lcom/sec/android/touchwiz/widget/TwListView;
    invoke-virtual {v6, v5}, Lcom/sec/android/touchwiz/widget/TwListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 124
    sget-object v8, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSpeed()I

    move-result v8

    const/4 v9, 0x1

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/touchwiz/widget/TwListView;->setItemChecked(IZ)V

    .line 125
    invoke-virtual {v6}, Lcom/sec/android/touchwiz/widget/TwListView;->getCheckedItemPosition()I

    move-result v8

    iput v8, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mPreviousEffectSelected:I

    .line 126
    new-instance v8, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$2;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$2;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    invoke-virtual {v6, v8}, Lcom/sec/android/touchwiz/widget/TwListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 133
    return-void
.end method

.method private setupVideoView(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # Landroid/view/View;

    .prologue
    .line 472
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-nez v1, :cond_1

    .line 473
    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->TAG:Ljava/lang/String;

    const-string v2, "filter is not supported"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    const v1, 0x7f0f0232

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 475
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 476
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 494
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 479
    :cond_1
    const v1, 0x7f0f0234

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;

    .line 480
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;

    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getVideoPlayEnable()Z

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 482
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mVideoCheckedTextView:Landroid/widget/CheckedTextView;

    new-instance v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$7;-><init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method


# virtual methods
.method public createLayout()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 632
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    .line 634
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 636
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupEffectView(Landroid/content/Context;Landroid/view/View;)V

    .line 637
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupSpeedView(Landroid/content/Context;Landroid/view/View;)V

    .line 638
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupFilterView(Landroid/content/Context;Landroid/view/View;)V

    .line 639
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupVideoView(Landroid/content/Context;Landroid/view/View;)V

    .line 640
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupMusicView(Landroid/content/Context;Landroid/view/View;)V

    .line 642
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public destroyView()V
    .locals 3

    .prologue
    .line 663
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 672
    :goto_0
    return-void

    .line 666
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mRootView:Landroid/view/View;

    invoke-interface {v1, v2}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public hide()V
    .locals 0

    .prologue
    .line 682
    return-void
.end method

.method public reloadSetting()V
    .locals 4

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->destroyView()V

    .line 648
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    sput-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 649
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->createLayout()Landroid/view/View;

    .line 650
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 652
    .local v0, "music":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 654
    .local v1, "musicEnable":Z
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->updateMusicTitle(Ljava/lang/String;)V

    .line 655
    sget-boolean v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    if-eqz v2, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 656
    const/4 v1, 0x0

    .line 658
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    .line 660
    return-void
.end method

.method public reloadSlideShowSettings()V
    .locals 4

    .prologue
    .line 685
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    sput-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 686
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 688
    .local v0, "music":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 690
    .local v1, "musicEnable":Z
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->updateMusicTitle(Ljava/lang/String;)V

    .line 691
    sget-boolean v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    if-eqz v2, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 692
    const/4 v1, 0x0

    .line 694
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    .line 696
    return-void
.end method

.method public setContext(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mGalleryActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 73
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 74
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    .line 75
    return-void
.end method

.method public setupRingtoneChoice(Landroid/net/Uri;)V
    .locals 5
    .param p1, "ringtoneUri"    # Landroid/net/Uri;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 562
    if-eqz p1, :cond_1

    .line 563
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v1, 0x5

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 564
    sget-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v1, 0x3

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 566
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 567
    const-string v0, ""

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    sput-boolean v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    .line 569
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    .line 572
    :goto_0
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->updateMusicTitle(Ljava/lang/String;)V

    .line 577
    :goto_1
    return-void

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    goto :goto_0

    .line 574
    :cond_1
    sput-boolean v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    .line 575
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicEnabler:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    goto :goto_1
.end method

.method public show()V
    .locals 0

    .prologue
    .line 677
    return-void
.end method

.method public showMusicListDialog(Z)V
    .locals 5
    .param p1, "show"    # Z

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 549
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v2, 0x2

    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 557
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_SLIDESHOW_MUSIC_LIST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 559
    return-void
.end method

.method public startSoundPick()Z
    .locals 4

    .prologue
    .line 580
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.music.intent.action.SOUND_PICKER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 581
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v2, "vnd.android.cursor.dir/audio"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 584
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mGalleryActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v3, 0x9

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 586
    const/4 v2, 0x1

    .line 589
    :goto_0
    return v2

    .line 587
    :catch_0
    move-exception v0

    .line 588
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 589
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public stopCurrentMusic()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 536
    const/4 v1, 0x5

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v3

    const/4 v2, 0x2

    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 544
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_SLIDESHOW_MUSIC_LIST"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 546
    return-void
.end method

.method public updateMusicTitle(Ljava/lang/String;)V
    .locals 3
    .param p1, "music"    # Ljava/lang/String;

    .prologue
    .line 699
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 700
    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    const v1, 0x7f0e0174

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 708
    :cond_0
    :goto_0
    return-void

    .line 703
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mMusicTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
