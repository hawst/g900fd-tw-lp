.class public Lcom/sec/samsung/gallery/controller/ShareAppUtil;
.super Ljava/lang/Object;
.source "ShareAppUtil.java"


# static fields
.field static final ACTIVITIINFO_NAME_FACEBOOK:Ljava/lang/String; = "com.facebook.katana.activity.composer.ImplicitShareIntentHandler"

.field static final APP_DEFALUT_MIME_TYPE:Ljava/lang/String; = "application/octet-stream"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field isImageNotCached:Z

.field private mContext:Landroid/content/Context;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field mDisplaySamsungShare:Z

.field private mMimeType:Ljava/lang/String;

.field private mSelectedItemCloudImage:Z

.field private mSelectedItemPicasaImage:Z

.field private mSelectedItemSNSImage:Z

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mTypeBits:I

.field private mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    const-class v0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    .line 55
    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    .line 57
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    .line 58
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    .line 59
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    .line 60
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mDisplaySamsungShare:Z

    .line 61
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->isImageNotCached:Z

    .line 64
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    move-object v0, p1

    .line 65
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    move-object v0, p1

    .line 66
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 67
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 68
    return-void
.end method

.method private addShareFilter(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p1, "shareFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->checkSNSImage_PicasaImage()V

    .line 309
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    if-eqz v0, :cond_1

    .line 311
    const-string v0, "com.dropbox.android.activity.DropboxSendTo"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 318
    const-string v0, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    const-string v0, "com.samsung.groupcast.sessioncreator.SessionCreatorActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    const-string v0, "com.samsung.groupcast.start.StartActivityForExternalApp"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 326
    const-string v0, "flipboard.activities.ComposeActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    const-string v0, "com.sec.android.app.FileShareClient.DeviceSelectActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    const-string v0, "com.facebook.katana.activity.composer.ImplicitShareIntentHandler"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    const-string v0, "com.sec.android.mimage.photoretouching.PhotoRetouching"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    if-eqz v0, :cond_2

    .line 339
    const-string v0, "com.dropbox.android.activity.DropboxSendTo"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    const-string v0, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    const-string v0, "com.samsung.groupcast.sessioncreator.SessionCreatorActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    const-string v0, "com.samsung.groupcast.start.StartActivityForExternalApp"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    const-string v0, "flipboard.activities.ComposeActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    const-string v0, "com.sec.android.app.FileShareClient.DeviceSelectActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 362
    const-string v0, "com.facebook.katana.activity.composer.ImplicitShareIntentHandler"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    const-string v0, "com.sec.android.mimage.photoretouching.PhotoRetouching"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 367
    :cond_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    if-eqz v0, :cond_4

    .line 369
    const-string v0, "com.dropbox.android.activity.DropboxSendTo"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    const-string v0, "com.samsung.groupcast.sessioncreator.SessionCreatorActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    const-string v0, "com.samsung.groupcast.start.StartActivityForExternalApp"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    const-string v0, "flipboard.activities.ComposeActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    const-string v0, "com.sec.android.app.FileShareClient.DeviceSelectActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    if-eqz v0, :cond_3

    .line 388
    const-string v0, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    :cond_3
    const-string v0, "com.facebook.katana.activity.composer.ImplicitShareIntentHandler"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 392
    :cond_4
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    if-eqz v0, :cond_0

    .line 394
    const-string v0, "com.android.bluetooth.opp.BluetoothOppLauncherActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    const-string v0, "com.samsung.groupcast.sessioncreator.SessionCreatorActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    const-string v0, "com.samsung.groupcast.start.StartActivityForExternalApp"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    const-string v0, "flipboard.activities.ComposeActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    const-string v0, "com.sec.android.app.FileShareClient.DeviceSelectActivity"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 408
    const-string v0, "com.facebook.katana.activity.composer.ImplicitShareIntentHandler"

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private checkSNSImage_PicasaImage()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 526
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    .line 527
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    .line 528
    iput-boolean v10, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    .line 530
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    .line 532
    .local v6, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    if-ge v1, v8, :cond_4

    .line 533
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 534
    .local v5, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v5, :cond_1

    .line 532
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 537
    :cond_1
    instance-of v8, v5, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v8, :cond_8

    move-object v7, v5

    .line 538
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 539
    .local v7, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 540
    .local v0, "count":I
    invoke-virtual {v7, v10, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    .line 542
    .local v3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 543
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 544
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_3

    .line 542
    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 546
    :cond_3
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    if-eqz v8, :cond_5

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    if-eqz v8, :cond_5

    .line 567
    .end local v0    # "count":I
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "j":I
    .end local v5    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v7    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    return-void

    .line 549
    .restart local v0    # "count":I
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v4    # "j":I
    .restart local v5    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v7    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v8, :cond_6

    .line 550
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    goto :goto_3

    .line 551
    :cond_6
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v8, :cond_7

    .line 552
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    goto :goto_3

    .line 553
    :cond_7
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v8, :cond_2

    .line 554
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    goto :goto_3

    .end local v0    # "count":I
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v4    # "j":I
    .end local v7    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    move-object v2, v5

    .line 557
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 559
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v8, :cond_9

    .line 560
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemSNSImage:Z

    goto :goto_1

    .line 561
    :cond_9
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v8, :cond_a

    .line 562
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemPicasaImage:Z

    goto :goto_1

    .line 563
    :cond_a
    instance-of v8, v2, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-eqz v8, :cond_0

    .line 564
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectedItemCloudImage:Z

    goto :goto_1
.end method

.method private createFilterList()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 281
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 282
    .local v2, "shareFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 283
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuShareViaMsg:Z

    if-eqz v3, :cond_0

    instance-of v3, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v3, :cond_0

    .line 284
    const-string v3, "com.android.email.activity.MessageCompose"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    const-string v3, "com.google.android.gm.ComposeActivityGmail"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 286
    const-string v3, "com.android.mail.compose.ComposeActivityGmail"

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 294
    :cond_0
    :try_start_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getPaperArtistClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 302
    :goto_0
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->addShareFilter(Ljava/util/ArrayList;)V

    .line 303
    return-object v2

    .line 296
    :catch_0
    move-exception v1

    .line 297
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 298
    .end local v1    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catch_1
    move-exception v1

    .line 299
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private getPaperArtistClassName()Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/content/pm/PackageManager$NameNotFoundException;
        }
    .end annotation

    .prologue
    .line 570
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.dama.paperartist"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 571
    .local v0, "info":Landroid/content/pm/PackageInfo;
    const/4 v1, 0x0

    .line 572
    .local v1, "intent":Landroid/content/Intent;
    const/4 v2, 0x0

    .line 573
    .local v2, "paperArtistClsName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 574
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "com.dama.paperartist"

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 575
    invoke-virtual {v1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    .line 578
    :cond_0
    return-object v2
.end method

.method private getUriList(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 157
    .local p1, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 158
    .local v1, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    const/4 v0, 0x0

    .line 159
    .local v0, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    .line 161
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v2, :cond_0

    instance-of v2, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-eqz v2, :cond_1

    .line 162
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromAlbums(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    .line 169
    :goto_0
    return-object v0

    .line 163
    :cond_1
    instance-of v2, v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-nez v2, :cond_2

    instance-of v2, v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    if-eqz v2, :cond_3

    .line 164
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromHiddenMedias(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromMedias(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method private getUriListFromAlbums(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 217
    .local p1, "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 219
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    .line 221
    const/4 v1, 0x0

    .line 222
    .local v1, "itemCount":I
    monitor-enter p1

    .line 223
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 224
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v5, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_0

    .line 225
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v5    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    add-int/2addr v1, v7

    .line 226
    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v1, v7, :cond_0

    .line 227
    monitor-exit p1

    .line 249
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "itemCount":I
    :cond_1
    :goto_0
    return-object v6

    .line 232
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v1    # "itemCount":I
    :cond_2
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_3
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 233
    .local v4, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v4, :cond_3

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    if-eqz v7, :cond_3

    .line 234
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v7, :cond_4

    .line 235
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItemList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    .line 236
    .local v3, "mediaItemList":Ljava/util/List;
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromMedias(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 246
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v3    # "mediaItemList":Ljava/util/List;
    .end local v4    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :catchall_0
    move-exception v7

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 237
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v4    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    :try_start_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v7

    instance-of v7, v7, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-eqz v7, :cond_3

    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v7, :cond_3

    .line 238
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 239
    .local v2, "mediaItem":Ljava/util/List;
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 240
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromMedias(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 246
    .end local v2    # "mediaItem":Ljava/util/List;
    .end local v4    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    monitor-exit p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private getUriListFromHiddenMedias(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    .local p1, "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 257
    .local v3, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x0

    .line 259
    .local v1, "i":I
    if-eqz p1, :cond_2

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 260
    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_2

    .line 261
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 262
    .local v0, "currentMedia":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v0, :cond_0

    .line 263
    instance-of v4, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_1

    .line 264
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 265
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItemList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    .line 266
    .local v2, "mediaItemList":Ljava/util/List;
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriListFromMedias(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 260
    .end local v0    # "currentMedia":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v2    # "mediaItemList":Ljava/util/List;
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 268
    .restart local v0    # "currentMedia":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v4, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_0

    .line 269
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    iget v4, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v0    # "currentMedia":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    or-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    .line 271
    iget v4, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->getMimeType(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    goto :goto_1

    .line 277
    :cond_2
    return-object v3
.end method

.method private getUriListFromMedias(Ljava/util/List;)Ljava/util/ArrayList;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 176
    .local v6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 177
    .local v5, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v5, :cond_1

    .line 178
    sget-object v7, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->TAG:Ljava/lang/String;

    const-string v8, "mediaObject is null"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 207
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v1

    .line 208
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 210
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_1
    return-object v6

    .line 181
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :try_start_1
    instance-of v7, v5, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v7, :cond_4

    .line 182
    move-object v0, v5

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v4, v0

    .line 183
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 186
    :cond_2
    instance-of v7, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-eqz v7, :cond_4

    move-object v0, v4

    check-cast v0, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->getCacheStatus()I

    move-result v7

    if-eqz v7, :cond_3

    check-cast v4, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    .end local v4    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->getCachedPath()Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_4

    .line 189
    :cond_3
    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->isImageNotCached:Z

    goto :goto_0

    .line 193
    :cond_4
    instance-of v7, v5, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v7, :cond_5

    move-object v0, v5

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v7, v0

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 194
    sget-object v7, Lcom/sec/android/gallery3d/golf/GolfMgr;->GOLF_TEMP_PATH:Ljava/lang/String;

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v7, v8, v9}, Lcom/sec/android/gallery3d/golf/GolfMgr;->getJpgTempFilePath(Ljava/lang/String;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    .line 196
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_0

    .line 197
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v7}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    iget v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    or-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    goto :goto_0

    .line 201
    .end local v2    # "filePath":Ljava/lang/String;
    .restart local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 202
    iget v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v8

    or-int/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    goto/16 :goto_0

    .line 206
    :cond_6
    iget v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mTypeBits:I

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->getMimeType(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method createIntentForShare(Ljava/util/ArrayList;)Landroid/content/Intent;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x1

    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 72
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v3, :cond_1

    .line 73
    const-string v2, "android.intent.extra.STREAM"

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Parcelable;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 74
    const-string v1, "android.intent.action.SEND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 86
    const-string v1, "application/octet-stream"

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 89
    const-string v1, "exit_on_sent"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 95
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 96
    return-object v0

    .line 82
    :cond_1
    const-string v1, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 83
    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public existImageNotCached()Z
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->isImageNotCached:Z

    return v0
.end method

.method public getFilteredShareList(Ljava/util/ArrayList;Landroid/content/Intent;)Ljava/util/List;
    .locals 13
    .param p2, "shareIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/LabeledIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 426
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 427
    .local v3, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 428
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, p2, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 429
    .local v6, "resInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createFilterList()Ljava/util/ArrayList;

    move-result-object v7

    .line 431
    .local v7, "shareFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v8, v4}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v6, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 432
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_5

    .line 434
    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 435
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 437
    .local v5, "packageName":Ljava/lang/String;
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 438
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 432
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 441
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 442
    invoke-virtual {p2, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 443
    invoke-interface {v6, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 444
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_1

    .line 446
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 447
    .local v2, "intent":Landroid/content/Intent;
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v5, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 448
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v8, v11, :cond_4

    .line 449
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Parcelable;

    invoke-virtual {v2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 450
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 456
    const-string v8, "application/octet-stream"

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    .line 458
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    const-string v8, "exit_on_sent"

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 466
    invoke-virtual {v2, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 467
    new-instance v8, Landroid/content/pm/LabeledIntent;

    invoke-virtual {v1, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    iget v10, v1, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-direct {v8, v2, v5, v9, v10}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 452
    :cond_4
    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_2

    .line 473
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_5
    return-object v3
.end method

.method public getFilteredShareList(Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 413
    .local p1, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createFilterList()Ljava/util/ArrayList;

    move-result-object v3

    .line 414
    .local v3, "shareFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 415
    .local v2, "numList":I
    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_1

    .line 416
    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 417
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v4, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v4, :cond_0

    .line 418
    iget-object v4, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v4, v4, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 419
    invoke-interface {p1, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 415
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 422
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    :cond_1
    return-object p1
.end method

.method public getFilteredShareListForOrange(Ljava/util/ArrayList;Landroid/content/Intent;)Ljava/util/List;
    .locals 13
    .param p2, "shareIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/LabeledIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 477
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 478
    .local v3, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 479
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, p2, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 480
    .local v6, "resInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->createFilterList()Ljava/util/ArrayList;

    move-result-object v7

    .line 482
    .local v7, "shareFilterList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v8, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v8, v4}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v6, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 483
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v0, v8, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_6

    .line 485
    invoke-interface {v6, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 486
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 488
    .local v5, "packageName":Ljava/lang/String;
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    if-eqz v8, :cond_0

    .line 489
    iget-object v8, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v8, v8, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 483
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 492
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 493
    const-string v8, "com.dropbox.android"

    invoke-virtual {p2, v8}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 494
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_1

    .line 497
    :cond_2
    const-string v8, "com.dropbox.android"

    if-ne v5, v8, :cond_3

    .line 498
    invoke-interface {v6, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    .line 501
    :cond_3
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 502
    .local v2, "intent":Landroid/content/Intent;
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v5, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 503
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v8, v11, :cond_5

    .line 504
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Parcelable;

    invoke-virtual {v2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 505
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 510
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    if-nez v8, :cond_4

    .line 511
    const-string v8, "application/octet-stream"

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    .line 513
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 514
    const-string v8, "exit_on_sent"

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 515
    invoke-virtual {v2, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 516
    new-instance v8, Landroid/content/pm/LabeledIntent;

    invoke-virtual {v1, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    iget v10, v1, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-direct {v8, v2, v5, v9, v10}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 507
    :cond_5
    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 508
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_2

    .line 522
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "packageName":Ljava/lang/String;
    :cond_6
    return-object v3
.end method

.method getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    return-object v0
.end method

.method getUriList()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation

    .prologue
    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 101
    .local v0, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList(Ljava/util/List;)Ljava/util/ArrayList;

    move-result-object v1

    return-object v1
.end method

.method shareFilterForAUS(Ljava/util/ArrayList;Landroid/content/Intent;)Ljava/util/List;
    .locals 13
    .param p2, "shareIntent"    # Landroid/content/Intent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/content/Intent;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/LabeledIntent;",
            ">;"
        }
    .end annotation

    .prologue
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 109
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 110
    .local v3, "intentList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/LabeledIntent;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 111
    .local v4, "packageManager":Landroid/content/pm/PackageManager;
    invoke-virtual {v4, p2, v12}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v6

    .line 112
    .local v6, "resInfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 114
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    new-instance v8, Landroid/content/pm/ResolveInfo$DisplayNameComparator;

    invoke-direct {v8, v4}, Landroid/content/pm/ResolveInfo$DisplayNameComparator;-><init>(Landroid/content/pm/PackageManager;)V

    invoke-static {v6, v8}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 116
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v1, v8, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_5

    .line 118
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/content/pm/ResolveInfo;

    .line 119
    .local v7, "ri":Landroid/content/pm/ResolveInfo;
    iget-object v8, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v8, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    .line 121
    .local v5, "packageName":Ljava/lang/String;
    const-string v8, "com.android.email"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_0

    const-string v8, "com.google.android.gm"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    :cond_0
    instance-of v8, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v8, :cond_1

    .line 116
    :goto_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 125
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 126
    invoke-virtual {p2, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 127
    invoke-interface {v6, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 128
    invoke-static {v6}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    goto :goto_1

    .line 130
    :cond_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 131
    .local v2, "intent":Landroid/content/Intent;
    new-instance v8, Landroid/content/ComponentName;

    iget-object v9, v7, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v9, v9, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v8, v5, v9}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 132
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ne v8, v11, :cond_4

    .line 133
    const-string v9, "android.intent.extra.STREAM"

    invoke-virtual {p1, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/os/Parcelable;

    invoke-virtual {v2, v9, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 134
    const-string v8, "android.intent.action.SEND"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 140
    :goto_2
    const-string/jumbo v8, "text/plain"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    if-nez v8, :cond_3

    .line 142
    const-string v8, "application/octet-stream"

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    .line 144
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->mMimeType:Ljava/lang/String;

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v8, "exit_on_sent"

    invoke-virtual {v2, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 147
    invoke-virtual {v2, v11}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 148
    new-instance v8, Landroid/content/pm/LabeledIntent;

    invoke-virtual {v7, v4}, Landroid/content/pm/ResolveInfo;->loadLabel(Landroid/content/pm/PackageManager;)Ljava/lang/CharSequence;

    move-result-object v9

    iget v10, v7, Landroid/content/pm/ResolveInfo;->icon:I

    invoke-direct {v8, v2, v5, v9, v10}, Landroid/content/pm/LabeledIntent;-><init>(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/CharSequence;I)V

    invoke-interface {v3, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 136
    :cond_4
    const-string v8, "android.intent.action.SEND_MULTIPLE"

    invoke-virtual {v2, v8}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v2, v8, p1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    goto :goto_2

    .line 153
    .end local v2    # "intent":Landroid/content/Intent;
    .end local v5    # "packageName":Ljava/lang/String;
    .end local v7    # "ri":Landroid/content/pm/ResolveInfo;
    :cond_5
    return-object v3
.end method
