.class public Lcom/sec/samsung/gallery/controller/StartGifEditCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartGifEditCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final DEFAULT_GIF_DIRECTORY:Ljava/lang/String; = "GIF"

.field private static final GIFMAKER_CLASS_NAME:Ljava/lang/String; = "com.sec.agif_maker.GifMakerActivity"

.field private static final GIFMAKER_PACKAGE_NAME:Ljava/lang/String; = "com.sec.agif_maker"

.field public static final MAKING_GIF_FOLDER:Ljava/lang/String;

.field private static final MEDIA_TYPE_VIDEO:I = 0x4


# instance fields
.field public MAX_MEDIA_COUNT:I

.field public filePath:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mFinishMediaScan:Z

.field private mFolder:Ljava/io/File;

.field private mMediaScanReceiver:Landroid/content/BroadcastReceiver;

.field private mReceiverRegistered:Z

.field public mSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-static {v1}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "GIF"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->MAKING_GIF_FOLDER:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 41
    const/16 v0, 0x14

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->MAX_MEDIA_COUNT:I

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mFolder:Ljava/io/File;

    .line 44
    iput v1, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mSize:I

    .line 45
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mFinishMediaScan:Z

    .line 46
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mReceiverRegistered:Z

    .line 53
    new-instance v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/StartGifEditCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartGifEditCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mMediaScanReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/controller/StartGifEditCmd;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartGifEditCmd;
    .param p1, "x1"    # Z

    .prologue
    .line 35
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mFinishMediaScan:Z

    return p1
.end method

.method private getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;
    .locals 12
    .param p1, "uriFile"    # Landroid/net/Uri;

    .prologue
    const/4 v11, 0x1

    .line 132
    const/4 v6, 0x0

    .line 133
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 135
    .local v8, "filePath":Ljava/lang/String;
    const/4 v0, 0x1

    :try_start_0
    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_data"

    aput-object v1, v2, v0

    .line 136
    .local v2, "proj":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 137
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v11, :cond_0

    .line 138
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 139
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 141
    .local v10, "index":I
    invoke-interface {v6, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 144
    .end local v10    # "index":I
    :cond_0
    if-eqz v6, :cond_1

    .line 145
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move-object v9, v8

    .line 150
    .end local v2    # "proj":[Ljava/lang/String;
    .end local v8    # "filePath":Ljava/lang/String;
    .local v9, "filePath":Ljava/lang/String;
    :goto_0
    return-object v9

    .line 147
    .end local v9    # "filePath":Ljava/lang/String;
    .restart local v8    # "filePath":Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 148
    .local v7, "e":Ljava/lang/Exception;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v9, v8

    .line 150
    .end local v8    # "filePath":Ljava/lang/String;
    .restart local v9    # "filePath":Ljava/lang/String;
    goto :goto_0
.end method

.method private startGifMaker(Landroid/app/Activity;)V
    .locals 19
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 77
    const/4 v5, 0x1

    .line 78
    .local v5, "mCheckGifMaker":Z
    const/4 v7, 0x1

    .local v7, "mSupportFormat":Z
    move-object/from16 v14, p1

    .line 80
    check-cast v14, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v11

    .line 82
    .local v11, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v14

    if-eqz v14, :cond_0

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v14

    if-nez v14, :cond_1

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 87
    .local v4, "intent":Landroid/content/Intent;
    new-instance v6, Landroid/content/ComponentName;

    const-string v14, "com.sec.agif_maker"

    const-string v15, "com.sec.agif_maker.GifMakerActivity"

    invoke-direct {v6, v14, v15}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    .local v6, "mComponent":Landroid/content/ComponentName;
    invoke-virtual {v4, v6}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 89
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    .line 90
    .local v10, "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v9, Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/LinkedList;->size()I

    move-result v14

    invoke-direct {v9, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    .local v9, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v10}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 92
    .local v8, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v8, :cond_3

    instance-of v14, v8, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_4

    move-object v14, v8

    check-cast v14, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v14

    if-eqz v14, :cond_4

    .line 93
    :cond_3
    const/4 v5, 0x0

    goto :goto_1

    .line 94
    :cond_4
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v14

    const/4 v15, 0x4

    if-ne v14, v15, :cond_5

    .line 95
    const/4 v7, 0x0

    goto :goto_1

    .line 97
    :cond_5
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v13

    .line 98
    .local v13, "uri":Landroid/net/Uri;
    if-eqz v13, :cond_2

    .line 100
    move-object/from16 v0, p0

    invoke-direct {v0, v13}, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->getFilePathFromUri(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    .line 101
    .local v2, "filePath":Ljava/lang/String;
    if-eqz v2, :cond_6

    const-string v14, ".gif"

    invoke-virtual {v2, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_6

    .line 102
    const/4 v5, 0x0

    goto :goto_1

    .line 104
    :cond_6
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v14

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 108
    .end local v2    # "filePath":Ljava/lang/String;
    .end local v8    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v13    # "uri":Landroid/net/Uri;
    :cond_7
    if-nez v7, :cond_8

    .line 109
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    const v15, 0x7f0e04c6

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 111
    :cond_8
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v14

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->MAX_MEDIA_COUNT:I

    if-le v14, v15, :cond_9

    .line 112
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0e0114

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->MAX_MEDIA_COUNT:I

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 114
    .local v12, "text":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    invoke-static {v14, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 116
    .end local v12    # "text":Ljava/lang/String;
    :cond_9
    if-nez v5, :cond_a

    .line 117
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    const v15, 0x7f0e0107

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    :cond_a
    move-object/from16 v14, p1

    .line 120
    check-cast v14, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v14}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v14

    const-string v15, "EXIT_SELECTION_MODE"

    const/16 v16, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    invoke-virtual/range {v14 .. v16}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 123
    const-string v14, "selectedItems"

    invoke-virtual {v4, v14, v9}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 125
    :try_start_0
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 126
    :catch_0
    move-exception v1

    .line 127
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 68
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 69
    .local v1, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v0, v1, v2

    check-cast v0, Landroid/app/Activity;

    .line 70
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->mContext:Landroid/content/Context;

    .line 71
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/StartGifEditCmd;->startGifMaker(Landroid/app/Activity;)V

    .line 74
    return-void
.end method
