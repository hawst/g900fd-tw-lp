.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;
.super Ljava/lang/Object;
.source "PhotoViewEventHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$1;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;,
        Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;
    }
.end annotation


# instance fields
.field private final mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

.field private mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

.field private final mPhotoViewMultiPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;

.field private final mPhotoViewNormal:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;

.field private final mPhotoViewPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;

.field private final mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "photoViewState"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 31
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    .line 32
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 34
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewNormal:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;

    .line 35
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;

    .line 36
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewMultiPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;

    .line 37
    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    return-object v0
.end method


# virtual methods
.method initializeView()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;->initializeView()V

    .line 60
    :cond_0
    return-void
.end method

.method isSlideshowcheck()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->checkEnable()Z

    move-result v0

    .line 78
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method onBackPressed()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;->onBackPressed()V

    .line 72
    :cond_0
    return-void
.end method

.method onItemClick(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;->onItemClick(I)V

    .line 66
    :cond_0
    return-void
.end method

.method public resetSlideshowLayout()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    if-eqz v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->resetSlideshowLayout()V

    .line 85
    :cond_0
    return-void
.end method

.method setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V
    .locals 2
    .param p1, "mode"    # Lcom/sec/samsung/gallery/core/LaunchModeType;

    .prologue
    .line 40
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$1;->$SwitchMap$com$sec$samsung$gallery$core$LaunchModeType:[I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 53
    :goto_0
    return-void

    .line 42
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewNormal:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewNormal;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    goto :goto_0

    .line 45
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    goto :goto_0

    .line 48
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mPhotoViewMultiPick:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$PhotoViewMultiPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/photoview/PhotoViewEventHandle$IPhotoViewMode;

    goto :goto_0

    .line 40
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
