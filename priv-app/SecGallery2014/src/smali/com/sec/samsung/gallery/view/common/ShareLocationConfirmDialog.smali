.class public Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "ShareLocationConfirmDialog.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEvent:Lcom/sec/samsung/gallery/core/Event;

.field private mShareActionCheckDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/core/Event;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Lcom/sec/samsung/gallery/core/Event;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mShareActionCheckDialog:Landroid/app/Dialog;

    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mContext:Landroid/content/Context;

    .line 28
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->setShareActionDialogOff(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;)Lcom/sec/samsung/gallery/core/Event;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 20
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mShareActionCheckDialog:Landroid/app/Dialog;

    return-object p1
.end method

.method private createAndShowDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 36
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mShareActionCheckDialog:Landroid/app/Dialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mShareActionCheckDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 90
    :goto_0
    return-void

    .line 38
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 39
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 41
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f0300d1

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 42
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 43
    .local v4, "message1":Landroid/widget/TextView;
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 45
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const v5, 0x7f0e01a4

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 47
    const v5, 0x1080027

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 48
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 49
    const v5, 0x7f0e01a3

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 50
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 52
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 53
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 54
    new-instance v5, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 60
    new-instance v5, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 67
    const v5, 0x7f0e00db

    new-instance v6, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;

    invoke-direct {v6, p0, v1}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    const v5, 0x7f0e0046

    new-instance v6, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$4;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$4;-><init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 84
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    new-instance v6, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$5;

    invoke-direct {v6, p0, v0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog$5;-><init>(Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;Landroid/app/AlertDialog$Builder;)V

    invoke-virtual {v5, v6}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method private setShareActionDialogOff(Z)V
    .locals 2
    .param p1, "shareActionDialogOff"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->mContext:Landroid/content/Context;

    const-string v1, "ShareActionCheckDialogOff"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 94
    return-void
.end method


# virtual methods
.method public showDialog()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/ShareLocationConfirmDialog;->createAndShowDialog()V

    .line 33
    return-void
.end method
