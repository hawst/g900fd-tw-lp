.class Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;
.super Ljava/lang/Object;
.source "HideActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->setTitle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

.field final synthetic val$title:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->val$title:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setTitleOnCustomView(Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0f0010

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    .local v0, "titleView":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 73
    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$1000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cannot set title because textview resource is not be"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_0
    return-void

    .line 77
    :cond_0
    if-nez p1, :cond_1

    .line 78
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 79
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 81
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 82
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 63
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->val$title:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->setTitleOnCustomView(Ljava/lang/String;)V

    .line 68
    :goto_0
    return-void

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal$2;->val$title:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
