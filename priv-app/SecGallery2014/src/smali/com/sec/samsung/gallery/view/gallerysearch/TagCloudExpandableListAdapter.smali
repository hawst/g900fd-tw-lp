.class public Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;
.super Landroid/widget/BaseExpandableListAdapter;
.source "TagCloudExpandableListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    }
.end annotation


# static fields
.field public static GROUP_INDEX_LOCATION_TAG:I

.field public static GROUP_INDEX_USER_DEF_TAG:I


# instance fields
.field public mContext:Landroid/content/Context;

.field private mEnabled:Z

.field private mTagGroupIconList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTagGroupList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;>;"
        }
    .end annotation
.end field

.field private mTagGroupTitleList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x2

    sput v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->GROUP_INDEX_USER_DEF_TAG:I

    .line 22
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->GROUP_INDEX_LOCATION_TAG:I

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Landroid/widget/BaseExpandableListAdapter;-><init>()V

    .line 24
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    .line 26
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    .line 30
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    .line 33
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    .line 38
    const v0, 0x7f0f024a

    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    .line 39
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->refreshTagList()V

    .line 40
    return-void
.end method


# virtual methods
.method public getChild(II)Ljava/lang/Object;
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 101
    .local v0, "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, p2, :cond_0

    .line 102
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 105
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getChildId(II)J
    .locals 2
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 110
    const v0, 0xf4240

    mul-int/2addr v0, p1

    add-int/2addr v0, p2

    int-to-long v0, v0

    return-wide v0
.end method

.method public getChildView(IIZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I
    .param p3, "isLastChild"    # Z
    .param p4, "convertView"    # Landroid/view/View;
    .param p5, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 122
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 123
    .local v0, "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    const/4 v2, 0x0

    .line 124
    .local v2, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    if-eqz v0, :cond_0

    .line 125
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    .line 126
    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    check-cast v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 129
    .restart local v2    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_0
    if-nez v2, :cond_2

    .line 130
    if-nez p4, :cond_1

    new-instance v3, Landroid/widget/FrameLayout;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 156
    :goto_0
    return-object v3

    :cond_1
    move-object v3, p4

    .line 130
    goto :goto_0

    .line 132
    :cond_2
    const/4 v1, 0x0

    .line 134
    .local v1, "holder":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    if-nez p4, :cond_3

    .line 135
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v6, 0x7f03007b

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p4

    .line 136
    const v3, 0x7f0f015e

    invoke-virtual {p4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;

    .line 137
    .restart local v1    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 138
    invoke-virtual {p4, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 147
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 150
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v4

    :goto_2
    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setEnabled(Z)V

    .line 151
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    move v3, v4

    :goto_3
    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setClickable(Z)V

    .line 152
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_4
    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setFocusable(Z)V

    .line 153
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isSelected()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 154
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTextTagData()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setText(Ljava/lang/CharSequence;)V

    move-object v3, p4

    .line 156
    goto :goto_0

    .line 140
    :cond_3
    invoke-virtual {p4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    check-cast v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;

    .line 141
    .restart local v1    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setTagData(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    goto :goto_1

    :cond_4
    move v3, v5

    .line 150
    goto :goto_2

    :cond_5
    move v3, v5

    .line 151
    goto :goto_3

    :cond_6
    move v4, v5

    .line 152
    goto :goto_4
.end method

.method public getChildrenCount(I)I
    .locals 3
    .param p1, "groupPosition"    # I

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-gt v2, p1, :cond_1

    .line 173
    :cond_0
    :goto_0
    return v1

    .line 165
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 167
    .local v0, "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    if-eqz v0, :cond_0

    .line 168
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_2

    .line 169
    const/4 v1, 0x1

    goto :goto_0

    .line 171
    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_0
.end method

.method public getGroup(I)Ljava/lang/Object;
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 180
    .local v0, "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    if-eqz v0, :cond_0

    .line 184
    .end local v0    # "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    :goto_0
    return-object v0

    .restart local v0    # "groupTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getGroupCount()I
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getGroupId(I)J
    .locals 2
    .param p1, "groupPosition"    # I

    .prologue
    .line 194
    int-to-long v0, p1

    return-wide v0
.end method

.method public getGroupView(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "groupPosition"    # I
    .param p2, "isExpanded"    # Z
    .param p3, "convertView"    # Landroid/view/View;
    .param p4, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v4, 0x0

    .line 199
    const/4 v0, 0x0

    .line 201
    .local v0, "holder":Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    if-nez p3, :cond_0

    .line 202
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v5, 0x7f03006b

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 204
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;

    .end local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;-><init>()V

    .line 205
    .restart local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    const v3, 0x7f0f0125

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->tvTitle:Landroid/widget/TextView;

    .line 206
    const v3, 0x7f0f0124

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivIcon:Landroid/widget/ImageView;

    .line 207
    const v3, 0x7f0f0126

    invoke-virtual {p3, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivBtn:Landroid/widget/ImageView;

    .line 209
    invoke-virtual {p3, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 214
    :goto_0
    const/4 v1, -0x1

    .line 215
    .local v1, "iconId":I
    const/4 v2, 0x0

    .line 217
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 218
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    invoke-interface {v3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "title":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 219
    .restart local v2    # "title":Ljava/lang/String;
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->tvTitle:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v5, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivIcon:Landroid/widget/ImageView;

    if-lez v1, :cond_1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_1
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 221
    iget-object v4, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivBtn:Landroid/widget/ImageView;

    if-eqz p2, :cond_2

    const v3, 0x7f0200bf

    :goto_2
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 224
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivBtn:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 225
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivBtn:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 226
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivBtn:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 228
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivIcon:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 229
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivIcon:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 230
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->ivIcon:Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setFocusable(Z)V

    .line 232
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->tvTitle:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 233
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->tvTitle:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setClickable(Z)V

    .line 234
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;->tvTitle:Landroid/widget/TextView;

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 236
    return-object p3

    .line 211
    .end local v1    # "iconId":I
    .end local v2    # "title":Ljava/lang/String;
    :cond_0
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;

    .restart local v0    # "holder":Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter$TagCloudGroupItemViewHolder;
    goto :goto_0

    .restart local v1    # "iconId":I
    .restart local v2    # "title":Ljava/lang/String;
    :cond_1
    move-object v3, v4

    .line 220
    goto :goto_1

    .line 221
    :cond_2
    const v3, 0x7f0200c0

    goto :goto_2
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public isChildSelectable(II)Z
    .locals 1
    .param p1, "groupPosition"    # I
    .param p2, "childPosition"    # I

    .prologue
    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public refreshTagList()V
    .locals 4

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mLocationTagList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mPersonTagList:Ljava/util/List;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mEventTagList:Ljava/util/List;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mUserTagScrollView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;->mUserTagTagList:Ljava/util/List;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->setTagGroupData(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 48
    return-void
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mEnabled:Z

    .line 115
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->notifyDataSetChanged()V

    .line 116
    return-void
.end method

.method protected setTagGroupData(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 54
    .local p1, "locationTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    .local p2, "personTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    .local p3, "eventTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    .local p4, "userDefTagData":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    const v1, 0x7f02046a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e044e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->GROUP_INDEX_USER_DEF_TAG:I

    .line 74
    :cond_0
    if-eqz p3, :cond_1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    const v1, 0x7f020461

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0451

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_1
    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    const v1, 0x7f020467

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0450

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_2
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupIconList:Ljava/util/List;

    const v1, 0x7f020464

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mTagGroupTitleList:Ljava/util/List;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e044f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->notifyDataSetChanged()V

    .line 95
    return-void
.end method
