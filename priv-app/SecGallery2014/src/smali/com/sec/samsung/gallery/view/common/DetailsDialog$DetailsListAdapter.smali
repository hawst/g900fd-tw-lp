.class Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;
.super Landroid/widget/BaseAdapter;
.source "DetailsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/DetailsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DetailsListAdapter"
.end annotation


# instance fields
.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 183
    .local p3, "contentsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 184
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->mItems:Ljava/util/ArrayList;

    .line 185
    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;Ljava/util/ArrayList;Lcom/sec/samsung/gallery/view/common/DetailsDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # Ljava/util/ArrayList;
    .param p4, "x3"    # Lcom/sec/samsung/gallery/view/common/DetailsDialog$1;

    .prologue
    .line 180
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;-><init>(Lcom/sec/samsung/gallery/view/common/DetailsDialog;Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 223
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 189
    if-nez p2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mCtx:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 192
    .local v0, "inflater":Landroid/view/LayoutInflater;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->this$0:Lcom/sec/samsung/gallery/view/common/DetailsDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/DetailsDialog;->mDetailItemLayout:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/DetailsDialog;->access$500(Lcom/sec/samsung/gallery/view/common/DetailsDialog;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 195
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v2, 0x7f0f00de

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 196
    .local v1, "textView":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/common/DetailsDialog$DetailsListAdapter;->getItem(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 198
    return-object p2
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method
