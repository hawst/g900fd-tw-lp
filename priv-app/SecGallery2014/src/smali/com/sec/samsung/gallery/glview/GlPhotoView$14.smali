.class Lcom/sec/samsung/gallery/glview/GlPhotoView$14;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1988
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 9
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v8, 0x4

    const/4 v4, 0x1

    const v7, 0x7f0e0107

    const/4 v5, 0x0

    .line 1990
    move-object v3, p1

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1992
    .local v1, "index":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    .line 2038
    :goto_0
    return v3

    .line 1994
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1700(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 1995
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mMode:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-ne v3, v6, :cond_2

    .line 1996
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v3

    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2001
    :cond_1
    :goto_1
    const/4 v0, 0x0

    .line 2002
    .local v0, "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v3, :cond_a

    .line 2003
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .end local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .line 2004
    .restart local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 2005
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_3

    move v3, v5

    .line 2006
    goto :goto_0

    .line 1998
    .end local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1700(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1

    .line 2009
    .restart local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    instance-of v3, v2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_4
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFilteredItems:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1800()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2010
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v3, v5

    .line 2011
    goto :goto_0

    .line 2014
    :cond_5
    instance-of v3, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_6
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForVideoAlbum:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1900()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2015
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v3, v5

    .line 2016
    goto :goto_0

    .line 2019
    :cond_7
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v3

    if-eq v3, v8, :cond_8

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_8
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForStoryAlbum:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2000()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2020
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v3, v5

    .line 2021
    goto/16 :goto_0

    .line 2024
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v6, :cond_a

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2025
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v3, v5

    .line 2026
    goto/16 :goto_0

    .line 2030
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_a
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2031
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v6, 0x52d0

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 2033
    :cond_b
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    if-eqz v3, :cond_c

    .line 2034
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-interface {v3, v6, v7, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 2036
    :cond_c
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int/2addr v6, v7

    iput v6, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 2037
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$14;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v3, v8, v1, v5, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    move v3, v4

    .line 2038
    goto/16 :goto_0
.end method
