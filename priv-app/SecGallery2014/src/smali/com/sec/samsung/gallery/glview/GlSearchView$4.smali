.class Lcom/sec/samsung/gallery/glview/GlSearchView$4;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 425
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 433
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->onMoved(II)Z

    move-result v0

    return v0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 428
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$702(Lcom/sec/samsung/gallery/glview/GlSearchView;Z)Z

    .line 429
    const/4 v0, 0x0

    return v0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->onReleased(IIII)Z

    move-result v0

    return v0
.end method
