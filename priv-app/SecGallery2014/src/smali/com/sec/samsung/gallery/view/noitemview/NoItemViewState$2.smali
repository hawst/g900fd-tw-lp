.class Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;
.super Ljava/lang/Object;
.source "NoItemViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V
    .locals 0

    .prologue
    .line 122
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 173
    const/4 v0, 0x0

    return-object v0
.end method

.method public onSizeChanged(I)V
    .locals 8
    .param p1, "size"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 125
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$800(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 126
    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$600()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mNeedsCrop is true. Skip startPreviousViewState()"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # setter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mNeedsCrop:Z
    invoke-static {v4, v7}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$802(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;Z)Z

    .line 153
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->getCount()I

    move-result v4

    if-eqz v4, :cond_3

    .line 131
    const/4 v3, 0x1

    .line 133
    .local v3, "toPreviousView":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$1000(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    .line 134
    .local v0, "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v4

    if-ne v4, v6, :cond_2

    .line 135
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    move-result-object v4

    instance-of v4, v4, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v4, :cond_2

    .line 136
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$900(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 137
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_2

    .line 138
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    if-gtz v4, :cond_2

    .line 139
    const/4 v3, 0x0

    .line 144
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    if-ne v3, v6, :cond_0

    .line 145
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # invokes: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->startPreviousViewState()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$1100(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)V

    goto :goto_0

    .line 147
    .end local v0    # "app":Lcom/sec/android/gallery3d/app/GalleryApp;
    .end local v3    # "toPreviousView":Z
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # invokes: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->getPreviousViewState()Ljava/lang/Class;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$1200(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Ljava/lang/Class;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-ne v4, v5, :cond_0

    .line 148
    new-array v2, v6, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$1300(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v4

    aput-object v4, v2, v7

    .line 151
    .local v2, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState$2;->this$0:Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    # getter for: Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;->access$1400(Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "START_IDLE_PROCESS"

    invoke-virtual {v4, v5, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public onWindowContentChanged(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 169
    return-void
.end method

.method public resetAllThumbnails()V
    .locals 0

    .prologue
    .line 165
    return-void
.end method

.method public resetContentWindow()V
    .locals 0

    .prologue
    .line 161
    return-void
.end method

.method public resetOutOfActivieThumbnail(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 178
    return-void
.end method

.method public setActiveWindow(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 157
    return-void
.end method
