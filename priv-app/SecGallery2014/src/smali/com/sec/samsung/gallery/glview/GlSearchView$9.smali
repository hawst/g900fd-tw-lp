.class Lcom/sec/samsung/gallery/glview/GlSearchView$9;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 527
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 6
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 532
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 533
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setAlpha(F)V

    .line 535
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->remove(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 536
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1300(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v1

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollRange:F

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-static {v0, v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1202(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlThumbObject;)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 538
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1102(Lcom/sec/samsung/gallery/glview/GlSearchView;I)I

    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1400(Lcom/sec/samsung/gallery/glview/GlSearchView;)[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->startAnimation([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 540
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->applyTransform(F)V

    .line 548
    :cond_0
    :goto_0
    return-void

    .line 541
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 542
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1102(Lcom/sec/samsung/gallery/glview/GlSearchView;I)I

    .line 543
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeletingIndex:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I

    move-result v2

    invoke-interface {v0, v1, v5, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;->onItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;II)V

    .line 544
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlSearchView;->loadKeywords()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1600(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    .line 545
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadItems()V

    .line 546
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlSearchView;->setClipRect()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$1700(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 529
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 528
    return-void
.end method
