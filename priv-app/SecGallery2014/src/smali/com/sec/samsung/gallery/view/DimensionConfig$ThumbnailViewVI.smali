.class public Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ThumbnailViewVI"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;


# instance fields
.field public mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 474
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 475
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 476
    .local v0, "r":Landroid/content/res/Resources;
    new-instance v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    .line 477
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    const v2, 0x7f0c0077

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree_count:I

    .line 478
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget v2, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree_count:I

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree:[I

    .line 479
    const v1, 0x7f090087

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree:[I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->mThumbnailViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;

    iget v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI$Spec;->thumbnail_view_end_affordance_row_degree_count:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy([II[III)V

    .line 482
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 468
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    if-nez v0, :cond_0

    .line 469
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;

    .line 471
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
