.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;
.super Ljava/lang/Object;
.source "MoreInfoLocationEditActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    .line 233
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 6
    .param p1, "arg0"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 220
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    const v3, 0x7f0f002c

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    .line 221
    .local v0, "etLocation":Landroid/widget/SearchView;
    invoke-virtual {v0}, Landroid/widget/SearchView;->getQuery()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "location":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 223
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$GeocoderTask;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$GeocoderTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$1;)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object v1, v3, v5

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$GeocoderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 225
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->mImr:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 226
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->mImr:Landroid/view/inputmethod/InputMethodManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;)Landroid/widget/SearchView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 228
    :cond_1
    return v5
.end method
