.class public final enum Lcom/sec/samsung/gallery/core/LaunchModeType;
.super Ljava/lang/Enum;
.source "LaunchModeType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/core/LaunchModeType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/core/LaunchModeType;

.field public static final enum ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field public static final enum ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field public static final enum ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field public static final enum ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    const-string v1, "ACTION_NORMAL"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/core/LaunchModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 18
    new-instance v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    const-string v1, "ACTION_PICK"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/core/LaunchModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 19
    new-instance v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    const-string v1, "ACTION_MULTIPLE_PICK"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/core/LaunchModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 20
    new-instance v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    const-string v1, "ACTION_PERSON_PICK"

    invoke-direct {v0, v1, v5}, Lcom/sec/samsung/gallery/core/LaunchModeType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 16
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->$VALUES:[Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/LaunchModeType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/core/LaunchModeType;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/samsung/gallery/core/LaunchModeType;->$VALUES:[Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/core/LaunchModeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-object v0
.end method
