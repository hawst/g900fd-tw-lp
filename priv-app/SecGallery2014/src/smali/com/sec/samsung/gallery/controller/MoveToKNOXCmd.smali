.class public Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "MoveToKNOXCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final ACTION_MEDIA_SCAN:Ljava/lang/String; = "android.intent.action.MEDIA_SCAN"

.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field private static final FAIL_ERROR_CODE_CANCELED:I = 0x2

.field private static final FAIL_ERROR_CODE_CONTAINER_STATE_PROBLEM:I = 0x1

.field private static final FAIL_ERROR_CODE_KNOX_NOT_READY:I = -0x1

.field private static final FAIL_ERROR_CODE_NOT_ERROR:I = 0x0

.field private static final FAIL_ERROR_CODE_STORAGE_FULL:I = 0x7

.field private static final MSG_EXIT_SELECTION_MODE:I = 0x0

.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MoveToKNOXCmd"


# instance fields
.field private mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mKnox2DestinationId:I

.field private mKnoxSelectionDialog:Landroid/app/AlertDialog;

.field private mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

.field private mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

.field private mOperationId:I

.field private mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mSelectedAlbumCount:I

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mThreadId:J

.field private mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private previousPercentage:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 70
    iput v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 87
    iput v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    .line 92
    new-instance v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;III)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showProgressivePopup(III)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;Ljava/util/ArrayList;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # Ljava/util/ArrayList;
    .param p2, "x2"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->moveToKnoxAPI2(Ljava/util/ArrayList;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mThreadId:J

    return-wide v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->dismissDialog()V

    return-void
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnox2DestinationId:I

    return p1
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->startMove()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnoxSelectionDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;IIII)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->showToast(IIII)V

    return-void
.end method

.method static synthetic access$808(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    return v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)Lcom/sec/knox/containeragent/ContainerInstallerManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-object v0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 376
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 377
    return-void
.end method

.method private handleOperation(II)V
    .locals 4
    .param p1, "operationId"    # I
    .param p2, "count"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 231
    iput v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    .line 232
    iput v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->previousPercentage:I

    .line 233
    new-instance v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;

    invoke-direct {v0, p0, p1, p2}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$5;-><init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;II)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/KNOXOperations;->cancel(Z)Z

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 352
    new-instance v0, Lcom/sec/samsung/gallery/util/KNOXOperations;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/samsung/gallery/util/KNOXOperations;-><init>(ILcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/KNOXOperations;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 354
    return-void
.end method

.method private moveToKnoxAPI2(Ljava/util/ArrayList;I)V
    .locals 4
    .param p2, "operationId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 361
    .local p1, "paths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    const-string v3, "rcp"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/RCPManager;

    .line 363
    .local v1, "mRcpm":Landroid/os/RCPManager;
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {v1, v2, p1, p1}, Landroid/os/RCPManager;->moveFilesForApp(ILjava/util/List;Ljava/util/List;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mThreadId:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 364
    :catch_0
    move-exception v0

    .line 365
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MoveToKNOXCmd"

    const-string v3, "failed to move files to KNOX API 2"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 366
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    if-eqz v2, :cond_0

    .line 367
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    const/4 v3, -0x1

    invoke-interface {v2, v3}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->failed(I)V

    goto :goto_0
.end method

.method private setKnoxDestinationIdAndStart(Landroid/content/Context;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p1}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 144
    const-string v6, "KnoxIdNamePair"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/HashMap;

    .line 145
    .local v4, "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 170
    :goto_0
    return-void

    .line 147
    :cond_0
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 149
    .local v2, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    const/4 v1, 0x0

    .line 150
    .local v1, "count":I
    const/4 v3, 0x0

    .line 151
    .local v3, "knoxId":I
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 152
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 153
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 154
    .local v5, "knoxName":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 155
    add-int/lit8 v1, v1, 0x1

    .line 159
    goto :goto_1

    .line 168
    .end local v5    # "knoxName":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->startMove()V

    goto :goto_0
.end method

.method private showKnoxChoicePoppup(Ljava/util/HashMap;)Z
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 173
    .local p1, "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    const/4 v1, 0x0

    .line 174
    .local v1, "activity":Landroid/app/Activity;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    instance-of v15, v15, Landroid/app/Activity;

    if-eqz v15, :cond_0

    .line 175
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    .end local v1    # "activity":Landroid/app/Activity;
    check-cast v1, Landroid/app/Activity;

    .line 178
    .restart local v1    # "activity":Landroid/app/Activity;
    :cond_0
    if-eqz v1, :cond_3

    .line 179
    invoke-virtual/range {p1 .. p1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .line 180
    .local v6, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 181
    .local v9, "knoxFirstId":I
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-virtual {v15}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 182
    .local v10, "knoxSecondId":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 183
    .local v5, "firstName":Ljava/lang/String;
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 185
    .local v13, "secondName":Ljava/lang/String;
    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v15

    const v16, 0x7f0300a9

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v15 .. v18}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v14

    .line 187
    .local v14, "view":Landroid/view/View;
    const v15, 0x7f0f01dd

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 188
    .local v3, "firstKnox":Landroid/widget/TextView;
    const v15, 0x7f0f01de

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    .line 190
    .local v11, "secondKnox":Landroid/widget/TextView;
    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-virtual {v11, v13}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    const v15, 0x7f0f01db

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 194
    .local v4, "firstKnoxImg":Landroid/widget/ImageView;
    const v15, 0x7f0f01dc

    invoke-virtual {v14, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 196
    .local v12, "secondKnoxImg":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f02043c

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v7

    .line 197
    .local v7, "knox1":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v15}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f02043d

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 199
    .local v8, "knox2":Landroid/graphics/drawable/Drawable;
    const-string v15, "KNOX"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    move-object v15, v7

    :goto_0
    invoke-virtual {v4, v15}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 200
    const-string v15, "KNOX"

    invoke-virtual {v15, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_2

    .end local v8    # "knox2":Landroid/graphics/drawable/Drawable;
    :goto_1
    invoke-virtual {v12, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 202
    new-instance v15, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v9}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;I)V

    invoke-virtual {v4, v15}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 211
    new-instance v15, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$4;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v10}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$4;-><init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;I)V

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    new-instance v15, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-direct/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v16, 0x7f0e0346

    invoke-virtual/range {v15 .. v16}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v15

    invoke-virtual {v15, v14}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 222
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v15

    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnoxSelectionDialog:Landroid/app/AlertDialog;

    .line 223
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnoxSelectionDialog:Landroid/app/AlertDialog;

    invoke-virtual {v15}, Landroid/app/AlertDialog;->show()V

    .line 224
    const/4 v15, 0x1

    .line 226
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "firstKnox":Landroid/widget/TextView;
    .end local v4    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v5    # "firstName":Ljava/lang/String;
    .end local v6    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v7    # "knox1":Landroid/graphics/drawable/Drawable;
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxSecondId":I
    .end local v11    # "secondKnox":Landroid/widget/TextView;
    .end local v12    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v13    # "secondName":Ljava/lang/String;
    .end local v14    # "view":Landroid/view/View;
    :goto_2
    return v15

    .restart local v3    # "firstKnox":Landroid/widget/TextView;
    .restart local v4    # "firstKnoxImg":Landroid/widget/ImageView;
    .restart local v5    # "firstName":Ljava/lang/String;
    .restart local v6    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .restart local v7    # "knox1":Landroid/graphics/drawable/Drawable;
    .restart local v8    # "knox2":Landroid/graphics/drawable/Drawable;
    .restart local v9    # "knoxFirstId":I
    .restart local v10    # "knoxSecondId":I
    .restart local v11    # "secondKnox":Landroid/widget/TextView;
    .restart local v12    # "secondKnoxImg":Landroid/widget/ImageView;
    .restart local v13    # "secondName":Ljava/lang/String;
    .restart local v14    # "view":Landroid/view/View;
    :cond_1
    move-object v15, v8

    .line 199
    goto :goto_0

    :cond_2
    move-object v8, v7

    .line 200
    goto :goto_1

    .line 226
    .end local v3    # "firstKnox":Landroid/widget/TextView;
    .end local v4    # "firstKnoxImg":Landroid/widget/ImageView;
    .end local v5    # "firstName":Ljava/lang/String;
    .end local v6    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v7    # "knox1":Landroid/graphics/drawable/Drawable;
    .end local v8    # "knox2":Landroid/graphics/drawable/Drawable;
    .end local v9    # "knoxFirstId":I
    .end local v10    # "knoxSecondId":I
    .end local v11    # "secondKnox":Landroid/widget/TextView;
    .end local v12    # "secondKnoxImg":Landroid/widget/ImageView;
    .end local v13    # "secondName":Ljava/lang/String;
    .end local v14    # "view":Landroid/view/View;
    :cond_3
    const/4 v15, 0x0

    goto :goto_2
.end method

.method private showProgressivePopup(III)V
    .locals 6
    .param p1, "operationId"    # I
    .param p2, "count"    # I
    .param p3, "percentage"    # I

    .prologue
    const/4 v4, 0x1

    .line 380
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-nez v2, :cond_0

    .line 382
    if-nez p1, :cond_1

    .line 383
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0346

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 388
    .local v1, "title":Ljava/lang/String;
    :goto_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    if-eqz v2, :cond_2

    .line 389
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e01aa

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "message":Ljava/lang/String;
    new-instance v2, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 391
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v2, p2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 392
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v2, v1, v4, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 393
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    int-to-long v4, p3

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 401
    .end local v0    # "message":Ljava/lang/String;
    .end local v1    # "title":Ljava/lang/String;
    :cond_0
    :goto_1
    return-void

    .line 385
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0347

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_0

    .line 395
    :cond_2
    new-instance v2, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 396
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v2, p2}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 397
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v4, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 398
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    int-to-long v4, p3

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    goto :goto_1
.end method

.method private showToast(IIII)V
    .locals 11
    .param p1, "operationId"    # I
    .param p2, "failedCount"    # I
    .param p3, "count"    # I
    .param p4, "errorCode"    # I

    .prologue
    .line 404
    const/4 v4, 0x0

    .line 405
    .local v4, "msg":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 407
    .local v1, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 487
    :goto_0
    :sswitch_0
    return-void

    .line 411
    :cond_0
    if-eqz p4, :cond_2

    .line 412
    if-nez v4, :cond_1

    .line 413
    sparse-switch p4, :sswitch_data_0

    .line 421
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01c9

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 486
    :cond_1
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-static {v5, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 415
    :sswitch_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0139

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 416
    goto :goto_1

    .line 426
    :cond_2
    const/4 v5, -0x1

    if-ne p4, v5, :cond_4

    .line 427
    if-nez p1, :cond_3

    .line 428
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-static {v5}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 429
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "KnoxIdNamePair"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 431
    .local v2, "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    iget v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mKnox2DestinationId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 432
    .local v3, "knoxName":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 433
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0350

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 436
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    .end local v3    # "knoxName":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0350

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e0345

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 440
    :cond_4
    if-nez p1, :cond_a

    .line 441
    const/4 v5, 0x1

    if-ne p2, v5, :cond_5

    .line 442
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01e7

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 443
    :cond_5
    const/4 v5, 0x1

    if-le p2, v5, :cond_6

    .line 444
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01e8

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 445
    :cond_6
    instance-of v5, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v5, :cond_8

    .line 446
    const/4 v5, 0x1

    if-ne p3, v5, :cond_7

    .line 447
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 448
    :cond_7
    const/4 v5, 0x1

    if-le p3, v5, :cond_1

    .line 449
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034d

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 453
    :cond_8
    const/4 v5, 0x1

    if-ne p3, v5, :cond_9

    .line 454
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0348

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 455
    :cond_9
    const/4 v5, 0x1

    if-le p3, v5, :cond_1

    .line 456
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0349

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 459
    :cond_a
    const/4 v5, 0x1

    if-ne p1, v5, :cond_1

    .line 460
    const/4 v5, 0x1

    if-ne p2, v5, :cond_b

    .line 461
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01ef

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 462
    :cond_b
    const/4 v5, 0x1

    if-le p2, v5, :cond_c

    .line 463
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e01f0

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 464
    :cond_c
    instance-of v5, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v5, :cond_e

    .line 465
    const/4 v5, 0x1

    if-ne p3, v5, :cond_d

    .line 466
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034e

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0345

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 469
    :cond_d
    const/4 v5, 0x1

    if-le p3, v5, :cond_1

    .line 470
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034f

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectedAlbumCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0345

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 475
    :cond_e
    const/4 v5, 0x1

    if-ne p3, v5, :cond_f

    .line 476
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034a

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0345

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 479
    :cond_f
    const/4 v5, 0x1

    if-le p3, v5, :cond_1

    .line 480
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e034b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0e0345

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_1

    .line 413
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch
.end method

.method private startMove()V
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 138
    .local v0, "count":I
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->handleOperation(II)V

    .line 139
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 8
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v7, 0x1

    .line 111
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 112
    .local v2, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v4, v2, v4

    check-cast v4, Landroid/content/Context;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    .line 113
    aget-object v4, v2, v7

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 114
    .local v1, "move":Z
    const/4 v4, 0x2

    aget-object v4, v2, v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    .line 115
    .local v3, "remove":Z
    new-instance v4, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    .line 116
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 117
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 118
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 119
    new-instance v4, Lcom/sec/knox/containeragent/ContainerInstallerManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    new-instance v6, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;)V

    invoke-direct {v4, v5, v6}, Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 128
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 129
    .local v0, "count":I
    if-eqz v1, :cond_1

    .line 130
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->setKnoxDestinationIdAndStart(Landroid/content/Context;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 131
    :cond_1
    if-eqz v3, :cond_0

    .line 132
    invoke-direct {p0, v7, v0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->handleOperation(II)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 4
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v1, 0x1

    .line 491
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOperateHideAsyncTask:Lcom/sec/samsung/gallery/util/KNOXOperations;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/KNOXOperations;->cancel(Z)Z

    .line 493
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    if-eqz v0, :cond_0

    .line 494
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->cancel()V

    .line 497
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->dismissDialog()V

    .line 498
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToKNOXCmd;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 500
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 358
    return-void
.end method
