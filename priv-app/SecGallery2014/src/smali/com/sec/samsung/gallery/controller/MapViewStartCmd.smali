.class public Lcom/sec/samsung/gallery/controller/MapViewStartCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "MapViewStartCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 17
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 18
    .local v0, "activity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 19
    return-void
.end method
