.class public Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
.super Ljava/lang/Object;
.source "QuickScrollHelpView.java"


# static fields
.field public static final HELP_KEY:Ljava/lang/String; = "quick_scroll_help"

.field public static final HELP_PARAM_KEY:Ljava/lang/String; = "is_show"


# instance fields
.field private mCloseBtn:Landroid/widget/ImageButton;

.field private mCloseClickListener:Landroid/view/View$OnClickListener;

.field private mCloseKeyListener:Landroid/view/View$OnKeyListener;

.field private mConfig:Landroid/content/res/Configuration;

.field private mContext:Landroid/content/Context;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mIsDrawerEnabled:Z

.field private mIsShowingHelpView:Z

.field private mMainView:Landroid/view/ViewGroup;

.field private mOkBtn:Landroid/widget/TextView;

.field private mQuickScrollHelpView:Landroid/widget/RelativeLayout;

.field private mResource:Landroid/content/res/Resources;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z

    .line 47
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsDrawerEnabled:Z

    .line 220
    new-instance v0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$4;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    .line 227
    new-instance v0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$5;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseKeyListener:Landroid/view/View$OnKeyListener;

    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mConfig:Landroid/content/res/Configuration;

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 56
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .param p1, "x1"    # Landroid/content/res/Configuration;

    .prologue
    .line 32
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mConfig:Landroid/content/res/Configuration;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isRTL()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->initializeView()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->setHelpState(Z)V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->bringToFront()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsDrawerEnabled:Z

    return v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 32
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsDrawerEnabled:Z

    return p1
.end method

.method private bringToFront()V
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 108
    :goto_0
    return-void

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mMainView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0
.end method

.method private initializeView()V
    .locals 12

    .prologue
    const v7, 0x7f0f010e

    const v11, 0x7f0f00e3

    const v10, 0x7f0f00e2

    const v9, 0x7f0d0337

    const/4 v8, 0x6

    .line 136
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v6, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x1

    .line 137
    .local v1, "isMultiwindow":Z
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTabletPickerMode(Landroid/content/Intent;)Z

    move-result v2

    .line 139
    .local v2, "isPickMode":Z
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v6, :cond_9

    .line 141
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mOkBtn:Landroid/widget/TextView;

    .line 142
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mOkBtn:Landroid/widget/TextView;

    if-nez v6, :cond_2

    .line 218
    :cond_0
    :goto_1
    return-void

    .line 136
    .end local v1    # "isMultiwindow":Z
    .end local v2    # "isPickMode":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 144
    .restart local v1    # "isMultiwindow":Z
    .restart local v2    # "isPickMode":Z
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mOkBtn:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mOkBtn:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 147
    .local v4, "params":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v1, :cond_3

    if-eqz v2, :cond_6

    .line 148
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d033b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    .line 151
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mOkBtn:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6, v11}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    .line 155
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 159
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 160
    .local v5, "txtParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v1, :cond_4

    if-eqz v2, :cond_7

    .line 161
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0336

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 162
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0334

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 163
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0338

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 170
    :goto_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 173
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 174
    .local v3, "mBody":Landroid/widget/ImageView;
    if-eqz v3, :cond_0

    .line 176
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 177
    .local v0, "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-nez v1, :cond_5

    if-eqz v2, :cond_8

    .line 178
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ActionBar;->getHeight()I

    move-result v6

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 181
    :goto_4
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 211
    .end local v0    # "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "mBody":Landroid/widget/ImageView;
    :goto_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    new-instance v7, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$3;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$3;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 150
    .end local v5    # "txtParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d033a

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_2

    .line 166
    .restart local v5    # "txtParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_7
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0335

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    .line 167
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0333

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 168
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    goto :goto_3

    .line 180
    .restart local v0    # "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v3    # "mBody":Landroid/widget/ImageView;
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    const v7, 0x7f0d0339

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto :goto_4

    .line 184
    .end local v0    # "bodyParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v3    # "mBody":Landroid/widget/ImageView;
    .end local v4    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v5    # "txtParams":Landroid/widget/RelativeLayout$LayoutParams;
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6, v7}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageButton;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    .line 185
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    if-eqz v6, :cond_0

    .line 187
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 188
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 190
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 191
    .restart local v4    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/ActionBar;->getHeight()I

    move-result v6

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 192
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v6, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 195
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v6, Landroid/app/Activity;

    invoke-virtual {v6, v11}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    .line 196
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    if-eqz v6, :cond_0

    .line 198
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 200
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    .line 201
    .restart local v5    # "txtParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mResource:Landroid/content/res/Resources;

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 203
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mConfig:Landroid/content/res/Configuration;

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_a

    .line 204
    invoke-virtual {v5, v8, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 208
    :goto_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_5

    .line 206
    :cond_a
    invoke-virtual {v5, v8}, Landroid/widget/RelativeLayout$LayoutParams;->removeRule(I)V

    goto :goto_6
.end method

.method private isRTL()Z
    .locals 2

    .prologue
    .line 98
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 99
    .local v0, "locale":Ljava/lang/String;
    const-string v1, "ar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "fa"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "ur"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isShowHelpPopup()Z
    .locals 3

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    const-string v1, "is_show"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private setHelpState(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    const-string v1, "is_show"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 116
    return-void
.end method


# virtual methods
.method public addView()V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->isShowHelpPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z

    if-nez v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$1;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 95
    :cond_0
    return-void
.end method

.method public isShowingHelpView()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mIsShowingHelpView:Z

    return v0
.end method

.method public removeView()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_1

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$2;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mQuickScrollHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 257
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView$6;-><init>(Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
