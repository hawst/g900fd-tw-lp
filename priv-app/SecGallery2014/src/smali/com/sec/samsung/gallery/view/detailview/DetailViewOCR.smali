.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;
.super Ljava/lang/Object;
.source "DetailViewOCR.java"


# static fields
.field static final GET_OCR_TEXT_ERROR:I = 0x2

.field static final GET_OCR_TEXT_IMAGE_IS_TOO_LARGE:I = 0x4

.field static final GET_OCR_TEXT_NO_TEXT:I = 0x1

.field static final GET_OCR_TEXT_UNSUPPORTED_IMAGE_FORMAT:I = 0x3

.field static final KEY_GET_OCR_TEXT_RESULT:Ljava/lang/String; = "gettext_result"

.field private static final OCR_ACTIVITY_NAME:Ljava/lang/String; = "com.sec.android.app.ocr3.GetTextActivity"

.field private static final TAG:Ljava/lang/String;

.field private static mOCRDialog:Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static showOCRFailedDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;)V
    .locals 5
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p1, "data"    # Landroid/content/Intent;

    .prologue
    .line 69
    const/4 v0, 0x0

    .line 70
    .local v0, "msgId":I
    const-string v2, "gettext_result"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 71
    .local v1, "result":I
    packed-switch v1, :pswitch_data_0

    .line 89
    :goto_0
    return-void

    .line 73
    :pswitch_0
    const v0, 0x7f0e0248

    .line 87
    :goto_1
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0e0247

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e00db

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 76
    :pswitch_1
    const v0, 0x7f0e0249

    .line 77
    goto :goto_1

    .line 79
    :pswitch_2
    const v0, 0x7f0e0245

    .line 80
    goto :goto_1

    .line 82
    :pswitch_3
    const v0, 0x7f0e0246

    .line 83
    goto :goto_1

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static startOCRActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 65
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->startOCRActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;Z)V

    .line 66
    return-void
.end method

.method public static startOCRActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Rect;Z)V
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "returnMode"    # Z

    .prologue
    const/4 v4, 0x0

    .line 30
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 31
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.ocr3"

    const-string v3, "com.sec.android.app.ocr3.GetTextActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 32
    const-string v2, "OCR_GETTEXT_IMAGEPATH"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 33
    const-string v2, "RETURN_MODE"

    invoke-virtual {v1, v2, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 34
    if-eqz p3, :cond_0

    .line 35
    const-string v2, "CROP_LEFT"

    iget v3, p3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 36
    const-string v2, "CROP_TOP"

    iget v3, p3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 37
    const-string v2, "CROP_RIGHT"

    iget v3, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    const-string v2, "CROP_BOTTOM"

    iget v3, p3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 41
    :cond_0
    invoke-static {p0}, Lcom/dmc/ocr/OcrUtils;->isOCRAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 42
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mHasOCR:Z

    .line 43
    if-eqz p4, :cond_1

    .line 45
    const/16 v2, 0x308

    :try_start_0
    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    .line 47
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_1
    const/16 v2, 0x307

    :try_start_1
    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 53
    :catch_1
    move-exception v0

    .line 54
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 58
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_2
    sput-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mHasOCR:Z

    .line 59
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v4, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    sput-object v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->mOCRDialog:Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;

    .line 60
    sget-object v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->mOCRDialog:Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->showDialog()V

    goto :goto_0
.end method
