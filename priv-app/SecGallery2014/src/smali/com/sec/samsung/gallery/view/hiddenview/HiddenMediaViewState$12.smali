.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKeyEvent(II)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "keyAction"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 329
    const/16 v2, 0x70

    if-ne p1, v2, :cond_1

    if-nez p2, :cond_1

    .line 330
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    if-lez v2, :cond_3

    .line 332
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->showDeleteDialog()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 344
    :cond_0
    :goto_0
    return v0

    .line 335
    :cond_1
    const/16 v2, 0x42

    if-eq p1, v2, :cond_2

    const/16 v2, 0x17

    if-ne p1, v2, :cond_3

    :cond_2
    const/16 v2, 0x80

    if-ne p2, v2, :cond_3

    .line 336
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v2, v3, :cond_0

    .line 337
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->enterSelectionMode(Z)V
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;Z)V

    .line 338
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 340
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$12;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 344
    goto :goto_0
.end method
