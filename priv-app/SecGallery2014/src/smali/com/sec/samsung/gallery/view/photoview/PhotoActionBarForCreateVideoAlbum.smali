.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;
.super Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
.source "PhotoActionBarForCreateVideoAlbum.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 23
    return-void
.end method

.method private isAvalableProceedCreateVideoAlbum(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 67
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-nez v0, :cond_0

    .line 68
    const/4 v0, 0x0

    .line 70
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startCreateVideoAlbum()V
    .locals 3

    .prologue
    .line 60
    const/4 v1, 0x1

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    .line 63
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "CREATE_VIDEO_ALBUM"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 64
    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 49
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 57
    :goto_0
    :pswitch_0
    return-void

    .line 51
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;->startCreateVideoAlbum()V

    goto :goto_0

    .line 54
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->onBackPressed()V

    goto :goto_0

    .line 49
    :pswitch_data_0
    .packed-switch 0x7f0f0022
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0024

    const/4 v5, 0x0

    .line 27
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 28
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForCreateVideoAlbum;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 29
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    .line 30
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x1

    .line 31
    .local v0, "bShowDone":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 32
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 33
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_1

    .line 34
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 45
    :cond_0
    :goto_1
    return-void

    .line 38
    :cond_1
    const/4 v0, 0x1

    .line 40
    if-nez v0, :cond_2

    .line 41
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1

    .line 31
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
