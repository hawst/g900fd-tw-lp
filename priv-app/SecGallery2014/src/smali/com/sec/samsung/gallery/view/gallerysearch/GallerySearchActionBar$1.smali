.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;
.super Ljava/lang/Object;
.source "GallerySearchActionBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    const/16 v1, 0x11

    .line 78
    .local v1, "flags":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getDisplayOptions()I

    move-result v3

    or-int v0, v3, v1

    .line 79
    .local v0, "change":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 80
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030007

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 82
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 83
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 84
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 86
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$600(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 87
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 88
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 94
    return-void
.end method
