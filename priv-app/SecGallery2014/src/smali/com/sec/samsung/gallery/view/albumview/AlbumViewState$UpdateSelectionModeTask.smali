.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
.super Landroid/os/AsyncTask;
.source "AlbumViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSelectionModeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 2195
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;

    .prologue
    .line 2195
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    return-void
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 2203
    iget-object v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v12, v12, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v6

    .line 2205
    .local v6, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v4, 0x0

    .line 2206
    .local v4, "isSelectedDataChanged":Z
    invoke-virtual {v6}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2207
    .local v7, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v12

    if-eqz v12, :cond_2

    .line 2244
    .end local v7    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    :goto_1
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    return-object v10

    .line 2209
    .restart local v7    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v12, v7, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v12, :cond_0

    move-object v8, v7

    .line 2210
    check-cast v8, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2211
    .local v8, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v9, 0x0

    .line 2212
    .local v9, "updateSelection":Z
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v12, :cond_3

    instance-of v12, v8, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v12, :cond_7

    :cond_3
    move v3, v11

    .line 2213
    .local v3, "isLocalAlbum":Z
    :goto_2
    const/4 v1, 0x0

    .line 2214
    .local v1, "filePathOfMediaSet":Ljava/lang/String;
    if-eqz v3, :cond_4

    .line 2215
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v1

    .line 2218
    :cond_4
    if-eqz v1, :cond_5

    .line 2219
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 2220
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v12

    if-nez v12, :cond_8

    .line 2221
    const/4 v9, 0x1

    .line 2230
    .end local v0    # "file":Ljava/io/File;
    :cond_5
    :goto_3
    if-eqz v9, :cond_0

    if-eqz v3, :cond_0

    .line 2231
    iget-object v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v12}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v12

    invoke-virtual {v12, v11}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 2232
    iget-object v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v12}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 2234
    iget-object v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z
    invoke-static {v12}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z

    move-result v12

    if-nez v12, :cond_6

    if-eqz v3, :cond_9

    .line 2235
    :cond_6
    const/4 v4, 0x1

    .line 2236
    iget-object v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    iget-object v12, v12, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0

    .end local v1    # "filePathOfMediaSet":Ljava/lang/String;
    .end local v3    # "isLocalAlbum":Z
    :cond_7
    move v3, v10

    .line 2212
    goto :goto_2

    .line 2224
    .restart local v0    # "file":Ljava/io/File;
    .restart local v1    # "filePathOfMediaSet":Ljava/lang/String;
    .restart local v3    # "isLocalAlbum":Z
    :cond_8
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v12

    invoke-virtual {v8, v10, v12}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 2225
    .local v5, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v5, :cond_5

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v12

    if-eqz v12, :cond_5

    .line 2226
    const/4 v9, 0x1

    goto :goto_3

    .line 2238
    .end local v0    # "file":Ljava/io/File;
    .end local v5    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_9
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 2199
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 2195
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 2249
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 2251
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2252
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAllPostProcess()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$6700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2258
    :cond_0
    :goto_0
    return-void

    .line 2254
    :catch_0
    move-exception v0

    .line 2255
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 2256
    const-string v1, "AlbumViewState"

    const-string v2, "onPostExecute : NullPointerException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 2195
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
