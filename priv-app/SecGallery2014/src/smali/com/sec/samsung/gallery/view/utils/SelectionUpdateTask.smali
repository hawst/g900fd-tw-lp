.class public Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;
.super Landroid/os/AsyncTask;
.source "SelectionUpdateTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field protected mContext:Landroid/content/Context;

.field private mFinishingState:Z

.field private mMaxCount:I

.field private mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mFinishingState:Z

    .line 32
    iput v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mMaxCount:I

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "maxCount"    # I

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 29
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mFinishingState:Z

    .line 32
    iput v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mMaxCount:I

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask$1;-><init>(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 56
    iput p3, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mMaxCount:I

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;

    .prologue
    .line 25
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mFinishingState:Z

    return v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)Lcom/sec/android/gallery3d/data/OnProgressListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    return-object v0
.end method

.method private addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p3, "tempMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :cond_0
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 147
    return-void
.end method

.method private checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 150
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private selectItems(Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V
    .locals 12
    .param p1, "selectionProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/ui/SelectionManager;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-wide/16 v10, 0x1

    const v8, 0x7f0e0107

    .line 91
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 92
    .local v4, "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 93
    .local v0, "activity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiPickMode(Landroid/content/Context;)Z

    move-result v2

    .line 95
    .local v2, "isMultiPick":Z
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-direct {p0, p2}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 96
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 101
    .local v6, "tempMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    const/4 v5, 0x1

    .line 102
    .local v5, "showUnsupportedFileToast":Z
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 103
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_0

    .line 105
    const/4 v7, 0x0

    invoke-virtual {p0, v10, v11, v7}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 106
    const/4 v7, 0x1

    invoke-virtual {p0, v10, v11, v7}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 108
    instance-of v7, v3, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v7, :cond_2

    .line 110
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 112
    :cond_3
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 113
    if-eqz v2, :cond_6

    .line 114
    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v7

    if-nez v7, :cond_5

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 116
    :cond_4
    if-eqz v5, :cond_2

    .line 117
    invoke-static {v0, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 118
    const/4 v5, 0x0

    goto :goto_1

    .line 122
    :cond_5
    invoke-direct {p0, p2, v3, v6}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    goto :goto_1

    .line 124
    :cond_6
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_7

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_7

    .line 125
    if-eqz v5, :cond_2

    .line 126
    invoke-static {v0, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 127
    const/4 v5, 0x0

    goto :goto_1

    .line 131
    :cond_7
    invoke-direct {p0, p2, v3, v6}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    goto :goto_1

    .line 135
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_8
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->setFinishingState()V

    .line 136
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/Map;)V

    .line 137
    if-nez v2, :cond_0

    .line 138
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/Map;)V

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
    .locals 14
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const-wide/16 v12, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 61
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 62
    .local v5, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    aget-object v4, p1, v9

    .line 63
    .local v4, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 64
    .local v0, "albumCount":I
    if-lez v0, :cond_4

    .line 65
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 66
    .local v6, "tempSelectionMap":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 67
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->isCancelled()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 87
    .end local v1    # "i":I
    .end local v6    # "tempSelectionMap":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :goto_1
    return-object v10

    .line 69
    .restart local v1    # "i":I
    .restart local v6    # "tempSelectionMap":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :cond_0
    invoke-virtual {p0, v12, v13, v9}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 70
    const/4 v7, 0x1

    invoke-virtual {p0, v12, v13, v7}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 72
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 73
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 74
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    const v8, 0x7f0e0107

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 66
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 77
    :cond_2
    invoke-interface {v6, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 78
    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 81
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->setFinishingState()V

    .line 82
    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    goto :goto_1

    .line 84
    .end local v1    # "i":I
    .end local v6    # "tempSelectionMap":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    :cond_4
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    invoke-virtual {v4, v9, v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 85
    .local v2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-direct {p0, v5, v4, v2}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->selectItems(Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    goto :goto_1
.end method

.method public increaseProgress(JZ)V
    .locals 3
    .param p1, "step"    # J
    .param p3, "isFileSize"    # Z

    .prologue
    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 184
    return-void
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 156
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 157
    return-void
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->onCancelled(Ljava/lang/Void;)V

    return-void
.end method

.method protected onCancelled(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 162
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 25
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 169
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 170
    return-void
.end method

.method protected onPreExecute()V
    .locals 5

    .prologue
    .line 174
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mMaxCount:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget v1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mMaxCount:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mContext:Landroid/content/Context;

    const v2, 0x7f0e0032

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mOnCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 179
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 180
    return-void
.end method

.method public setFinishingState()V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->mFinishingState:Z

    .line 46
    return-void
.end method
