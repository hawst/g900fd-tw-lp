.class Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;
.super Ljava/lang/Object;
.source "AndroidBeamCmd.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "BeamPushCallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$1;

    .prologue
    .line 204
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;-><init>(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)V

    return-void
.end method

.method private createRecord()Landroid/nfc/NdefRecord;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 230
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "create record"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    const/4 v3, 0x0

    .line 234
    .local v3, "payload":[B
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getFiles()[Ljava/io/File;

    move-result-object v1

    .line 236
    .local v1, "files":[Ljava/io/File;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I

    move-result v5

    if-ne v8, v5, :cond_1

    .line 237
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getBeamIntent()Landroid/content/Intent;

    move-result-object v2

    .line 238
    .local v2, "i":Landroid/content/Intent;
    const-string v5, "POPUP_MODE"

    const-string v6, "no_file_selected"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 240
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 246
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 282
    .end local v2    # "i":Landroid/content/Intent;
    :cond_0
    :goto_1
    return-object v4

    .line 241
    .restart local v2    # "i":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 242
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Activity Not Found"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 248
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_1
    const/4 v5, 0x3

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I

    move-result v6

    if-ne v5, v6, :cond_2

    .line 249
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "is cloud file"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getBeamIntent()Landroid/content/Intent;

    move-result-object v2

    .line 251
    .restart local v2    # "i":Landroid/content/Intent;
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "set intent"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    const-string v5, "POPUP_MODE"

    const-string v6, "from_cloud_file"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 253
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "try to start"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    :try_start_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 260
    :goto_2
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "start activity! cloud file"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    goto :goto_1

    .line 256
    :catch_1
    move-exception v0

    .line 257
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Activity Not Found"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_2

    .line 263
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_2
    const/4 v5, 0x4

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I

    move-result v6

    if-ne v5, v6, :cond_3

    .line 264
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getBeamIntent()Landroid/content/Intent;

    move-result-object v2

    .line 265
    .restart local v2    # "i":Landroid/content/Intent;
    const-string v5, "POPUP_MODE"

    const-string v6, "from_drm_file"

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    :try_start_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    .line 272
    :goto_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v5, v7}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    goto/16 :goto_1

    .line 268
    :catch_2
    move-exception v0

    .line 269
    .restart local v0    # "e":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v5

    const-string v6, "Activity Not Found"

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_3

    .line 275
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "i":Landroid/content/Intent;
    :cond_3
    if-eqz v1, :cond_0

    .line 276
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getJSONObject([Ljava/io/File;)[B

    move-result-object v3

    .line 282
    new-instance v4, Landroid/nfc/NdefRecord;

    const-string/jumbo v5, "text/DirectShareGallery"

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    new-array v6, v7, [B

    invoke-direct {v4, v8, v5, v6, v3}, Landroid/nfc/NdefRecord;-><init>(S[B[B[B)V

    goto/16 :goto_1
.end method

.method private getBeamIntent()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSbeamOn()Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$500(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DirectSharePopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 290
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.nfc.AndroidBeamPopUp"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getFileList([Ljava/io/File;)Lorg/json/JSONArray;
    .locals 12
    .param p1, "files"    # [Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 363
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v7

    .line 364
    .local v7, "rootPath":Ljava/lang/String;
    new-instance v6, Lorg/json/JSONArray;

    invoke-direct {v6}, Lorg/json/JSONArray;-><init>()V

    .line 366
    .local v6, "retList":Lorg/json/JSONArray;
    move-object v1, p1

    .local v1, "arr$":[Ljava/io/File;
    array-length v4, v1

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v2, v1, v3

    .line 367
    .local v2, "file":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "absolutePath":Ljava/lang/String;
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5}, Lorg/json/JSONObject;-><init>()V

    .line 369
    .local v5, "obj":Lorg/json/JSONObject;
    const-string v8, "fileName"

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 370
    const-string v8, "fileLen"

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v10

    invoke-virtual {v5, v8, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 371
    const-string v8, "filepath"

    invoke-virtual {v5, v8, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 372
    const-string/jumbo v8, "subPath"

    const-string v9, ""

    invoke-virtual {v0, v7, v9}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 373
    invoke-virtual {v6, v5}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 366
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 376
    .end local v0    # "absolutePath":Ljava/lang/String;
    .end local v2    # "file":Ljava/io/File;
    .end local v5    # "obj":Lorg/json/JSONObject;
    :cond_0
    return-object v6
.end method

.method private getFiles()[Ljava/io/File;
    .locals 15

    .prologue
    .line 296
    const/4 v5, 0x1

    .line 298
    .local v5, "isLocalAlbum":Z
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v13}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v13

    instance-of v13, v13, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v13, :cond_2

    .line 299
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v13}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v10

    .line 300
    .local v10, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v8

    .line 301
    .local v8, "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 302
    .local v7, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v13, :cond_1

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v13, :cond_1

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-eqz v13, :cond_4

    .line 303
    :cond_1
    const/4 v5, 0x0

    .line 313
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v8    # "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_2
    :goto_0
    const/4 v12, 0x0

    .line 314
    .local v12, "uris":[Landroid/net/Uri;
    if-eqz v5, :cond_6

    .line 315
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mListener:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;
    invoke-static {v13}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$400(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;

    move-result-object v13

    invoke-interface {v13}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;->onGetFilePath()[Landroid/net/Uri;

    move-result-object v12

    .line 316
    if-nez v12, :cond_7

    .line 317
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    const/4 v14, 0x2

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 318
    const/4 v1, 0x0

    .line 342
    :cond_3
    :goto_1
    return-object v1

    .line 305
    .end local v12    # "uris":[Landroid/net/Uri;
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v7    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v8    # "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .restart local v10    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_4
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v13, :cond_5

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v13, :cond_5

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v13, :cond_5

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v13, :cond_0

    .line 307
    :cond_5
    const/4 v5, 0x0

    .line 308
    goto :goto_0

    .line 321
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v7    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v8    # "mediaSetList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    .restart local v12    # "uris":[Landroid/net/Uri;
    :cond_6
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    const/4 v14, 0x3

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 322
    const/4 v1, 0x0

    goto :goto_1

    .line 325
    :cond_7
    array-length v13, v12

    new-array v1, v13, [Ljava/io/File;

    .line 326
    .local v1, "files":[Ljava/io/File;
    const/4 v2, 0x0

    .line 328
    .local v2, "i":I
    move-object v0, v12

    .local v0, "arr$":[Landroid/net/Uri;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    move v3, v2

    .end local v2    # "i":I
    .local v3, "i":I
    :goto_2
    if-ge v4, v6, :cond_3

    aget-object v11, v0, v4

    .line 329
    .local v11, "uri":Landroid/net/Uri;
    invoke-virtual {v11}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v9

    .line 330
    .local v9, "scheme":Ljava/lang/String;
    const-string v13, "file"

    invoke-virtual {v9, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 331
    invoke-virtual {v11}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->isDrmFile(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_9

    .line 332
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    const/4 v14, 0x4

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 333
    const/4 v1, 0x0

    goto :goto_1

    .line 336
    :cond_8
    iget-object v13, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    const/4 v14, 0x3

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 337
    const/4 v1, 0x0

    goto :goto_1

    .line 339
    :cond_9
    add-int/lit8 v2, v3, 0x1

    .end local v3    # "i":I
    .restart local v2    # "i":I
    new-instance v13, Ljava/io/File;

    invoke-virtual {v11}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    aput-object v13, v1, v3

    .line 328
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    .end local v2    # "i":I
    .restart local v3    # "i":I
    goto :goto_2
.end method

.method private getJSONObject([Ljava/io/File;)[B
    .locals 5
    .param p1, "files"    # [Ljava/io/File;

    .prologue
    .line 350
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 352
    .local v1, "obj":Lorg/json/JSONObject;
    :try_start_0
    const-string v2, "mac"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getMacAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 353
    const-string v2, "mimeType"

    const-string/jumbo v3, "text/DirectShareGallery"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 354
    const-string v2, "list"

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->getFileList([Ljava/io/File;)Lorg/json/JSONArray;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    return-object v2

    .line 355
    :catch_0
    move-exception v0

    .line 356
    .local v0, "e":Lorg/json/JSONException;
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SbeamHelperSbeamPushCallback.getJSONObject : text/DirectShareGallery/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private getMacAddress()Ljava/lang/String;
    .locals 9

    .prologue
    const/16 v8, 0x10

    const/4 v7, 0x2

    const/4 v6, 0x0

    .line 380
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v4

    const-string/jumbo v5, "wifi"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/wifi/WifiManager;

    .line 382
    .local v3, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v3}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    .line 383
    .local v2, "wifiInfo":Landroid/net/wifi/WifiInfo;
    if-nez v2, :cond_0

    .line 384
    const/4 v4, 0x0

    .line 394
    :goto_0
    return-object v4

    .line 385
    :cond_0
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 386
    .local v0, "firstMac":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v4, v7, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 389
    .local v1, "lastMac":Ljava/lang/String;
    const/4 v4, 0x1

    invoke-virtual {v0, v6, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 390
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "0"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 394
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v0, v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v5

    or-int/lit8 v5, v5, 0x2

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method private isDrmFile(Ljava/lang/String;)Z
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$700(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isDrm(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 5
    .param p1, "arg0"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v4, 0x1

    .line 209
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "createNdefMessage"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->createRecord()Landroid/nfc/NdefRecord;

    move-result-object v1

    .line 212
    .local v1, "record":Landroid/nfc/NdefRecord;
    if-nez v1, :cond_1

    .line 213
    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$300()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "BeamPushCallback.createNdefMessage : failed ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$600(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x0

    .line 226
    :cond_0
    :goto_0
    return-object v0

    .line 217
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->mNdefStatus:I
    invoke-static {v2, v4}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$602(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;I)I

    .line 219
    const/4 v0, 0x0

    .line 220
    .local v0, "ndefMessage":Landroid/nfc/NdefMessage;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$BeamPushCallback;->this$0:Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->isSbeamOn()Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;->access$500(Lcom/sec/samsung/gallery/controller/AndroidBeamCmd;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 221
    new-instance v0, Landroid/nfc/NdefMessage;

    .end local v0    # "ndefMessage":Landroid/nfc/NdefMessage;
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/nfc/NdefRecord;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const-string v3, "com.sec.android.directshare"

    invoke-static {v3}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-direct {v0, v2}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    .restart local v0    # "ndefMessage":Landroid/nfc/NdefMessage;
    goto :goto_0
.end method
