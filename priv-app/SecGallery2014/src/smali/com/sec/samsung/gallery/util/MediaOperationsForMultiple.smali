.class public Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;
.super Landroid/os/AsyncTask;
.source "MediaOperationsForMultiple.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mTotal:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;Landroid/content/Context;)V
    .locals 1
    .param p1, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 29
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mContext:Landroid/content/Context;

    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 32
    return-void
.end method

.method private getProgressCnt(I)I
    .locals 2
    .param p1, "totalCnt"    # I

    .prologue
    .line 97
    const/4 v0, 0x0

    .line 99
    .local v0, "progressCnt":I
    const/16 v1, 0x190

    if-le p1, v1, :cond_0

    .line 100
    const/16 v0, 0x64

    .line 110
    :goto_0
    return v0

    .line 101
    :cond_0
    const/16 v1, 0xc8

    if-le p1, v1, :cond_1

    .line 102
    const/16 v0, 0x32

    goto :goto_0

    .line 103
    :cond_1
    const/16 v1, 0x14

    if-le p1, v1, :cond_2

    .line 104
    div-int/lit8 v0, p1, 0x4

    goto :goto_0

    .line 105
    :cond_2
    const/4 v1, 0x6

    if-le p1, v1, :cond_3

    .line 106
    div-int/lit8 v0, p1, 0x2

    goto :goto_0

    .line 108
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 10
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v9, 0x0

    .line 42
    const/4 v4, 0x1

    .line 43
    .local v4, "result":Z
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 44
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-nez v5, :cond_0

    .line 45
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 70
    :goto_0
    return-object v7

    .line 46
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-direct {v1, v7}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 47
    .local v1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 48
    .local v6, "tempList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    .line 49
    iget v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->getProgressCnt(I)I

    move-result v3

    .line 52
    .local v3, "progressCnt":I
    const/4 v0, 0x0

    .local v0, "index":I
    const/4 v2, 0x0

    .line 53
    .local v2, "progress":I
    :goto_1
    iget v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    if-ge v0, v7, :cond_5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->isCancelled()Z

    move-result v7

    if-nez v7, :cond_5

    .line 54
    if-ltz v0, :cond_1

    iget v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    if-le v0, v7, :cond_2

    .line 55
    :cond_1
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_0

    .line 58
    :cond_2
    add-int v7, v0, v3

    iget v8, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    if-ge v7, v8, :cond_3

    add-int v2, v0, v3

    .line 60
    :goto_2
    invoke-interface {v1, v0, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v6

    .line 62
    move v0, v2

    .line 64
    iget-object v7, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-interface {v7, v6}, Lcom/sec/android/gallery3d/data/OnProgressListener;->handleOperation(Ljava/util/List;)Z

    move-result v4

    .line 65
    if-nez v4, :cond_4

    .line 66
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_0

    .line 58
    :cond_3
    iget v2, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    goto :goto_2

    .line 68
    :cond_4
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->publishProgress([Ljava/lang/Object;)V

    goto :goto_1

    .line 70
    :cond_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 94
    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 86
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 38
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1, "progress"    # [Ljava/lang/Integer;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->mTotal:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onProgress(II)V

    .line 78
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperationsForMultiple;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
