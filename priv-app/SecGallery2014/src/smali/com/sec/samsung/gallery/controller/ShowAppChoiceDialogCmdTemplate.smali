.class abstract Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowAppChoiceDialogCmdTemplate.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final MSG_DIALOG_CANCEL_SELECTED:I = 0x1

.field private static final MSG_DIALOG_SHOW_REFRESH:I = 0x2

.field private static final MSG_DIALOG_SHOW_SELECTED:I


# instance fields
.field private mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

.field protected mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mIsAirButton:Z

.field protected mMimeType:Ljava/lang/String;

.field protected mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

.field protected mTitleOfDialog:I

.field protected mUriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 37
    iput v1, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mTitleOfDialog:I

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .line 42
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mIsAirButton:Z

    .line 80
    new-instance v0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->handleMessageOnUiThread(I)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;)Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;Ljava/util/List;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
    .param p1, "x1"    # Ljava/util/List;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->showAppChoiceDialog(Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mIsAirButton:Z

    return v0
.end method

.method private handleMessageOnUiThread(I)V
    .locals 2
    .param p1, "msg"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$2;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 114
    return-void
.end method

.method private showAppChoiceDialog(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "shareAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v4, 0x1

    .line 120
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mTitleOfDialog:I

    if-nez v2, :cond_0

    .line 121
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v2

    .line 123
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 172
    :cond_1
    :goto_0
    return-void

    .line 127
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    const-string v3, "enterprise_policy"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 128
    .local v0, "edm":Landroid/app/enterprise/EnterpriseDeviceManager;
    invoke-virtual {v0}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRestrictionPolicy()Landroid/app/enterprise/RestrictionPolicy;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/enterprise/RestrictionPolicy;->isShareListAllowed()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 131
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    .line 132
    .local v1, "event":Lcom/sec/samsung/gallery/core/Event;
    new-instance v2, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, p1, v1, v4}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;-><init>(Landroid/content/Context;Ljava/util/List;Lcom/sec/samsung/gallery/core/Event;Z)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    .line 133
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    iget v3, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mTitleOfDialog:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setTitle(I)V

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v2, p0}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->addObserver(Ljava/util/Observer;)V

    .line 135
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$3;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$3;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setOnDialogOrientationListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnOrientationChangedListener;)V

    .line 143
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$4;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$4;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setPrivateModeChangeListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$PrivateModeChangeListener;)V

    .line 150
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$5;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$5;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setOnDialogDismissListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnDialogDismissListener;)V

    .line 159
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    new-instance v3, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$6;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate$6;-><init>(Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->setOnResumeListener(Lcom/sec/samsung/gallery/view/common/AppChoiceDialog$OnResumeListener;)V

    .line 166
    if-eqz p1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 167
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->startSingleShareApp()V

    .line 168
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    goto :goto_0

    .line 170
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->showDialog()V

    goto :goto_0
.end method


# virtual methods
.method protected abstract createAppList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 50
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 51
    .local v0, "params":[Ljava/lang/Object;
    aget-object v2, v0, v5

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    .line 52
    aget-object v2, v0, v4

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 53
    .local v1, "show":Z
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mIsAirButton:Z

    .line 54
    new-instance v2, Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    .line 55
    array-length v2, v0

    if-le v2, v6, :cond_0

    .line 56
    aget-object v2, v0, v6

    check-cast v2, Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mUriList:Ljava/util/ArrayList;

    .line 60
    :goto_0
    if-nez v1, :cond_1

    .line 61
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->handleMessageOnUiThread(I)V

    .line 78
    :goto_1
    return-void

    .line 58
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getUriList()Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mUriList:Ljava/util/ArrayList;

    goto :goto_0

    .line 65
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->getMimeType()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mMimeType:Ljava/lang/String;

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mUriList:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 67
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->existImageNotCached()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    const v3, 0x7f0e04bb

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_1

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    const v3, 0x7f0e0113

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    goto :goto_1

    .line 73
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mShareAppUtil:Lcom/sec/samsung/gallery/controller/ShareAppUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/controller/ShareAppUtil;->existImageNotCached()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->mContext:Landroid/content/Context;

    const v3, 0x7f0e04ba

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 76
    :cond_5
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->handleMessageOnUiThread(I)V

    goto :goto_1
.end method

.method public abstract update(Ljava/util/Observable;Ljava/lang/Object;)V
.end method
