.class public Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "DownloadCloudCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;

    .line 57
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->getCloudItems(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private getCloudItems(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/remote/cloud/CloudImage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 45
    .local v0, "items":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/remote/cloud/CloudImage;>;"
    new-instance v1, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$1;

    invoke-direct {v1, p0, v0}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;Ljava/util/List;)V

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->enumerateTotalMediaItems(Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;)V

    .line 54
    return-object v0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v4, 0x0

    .line 37
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v1, v2

    check-cast v1, [Ljava/lang/Object;

    .line 38
    .local v1, "params":[Ljava/lang/Object;
    aget-object v0, v1, v4

    check-cast v0, Landroid/content/Context;

    .line 39
    .local v0, "context":Landroid/content/Context;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;->mContext:Landroid/content/Context;

    .line 40
    new-instance v2, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;-><init>(Lcom/sec/samsung/gallery/controller/DownloadCloudCmd;Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$1;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/controller/DownloadCloudCmd$DownloadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 41
    return-void
.end method
