.class Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;
.super Ljava/lang/Object;
.source "EventViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0

    .prologue
    .line 328
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 4
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    const/4 v3, 0x1

    .line 331
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 332
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAlbum(I)V
    invoke-static {v1, p3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V

    .line 347
    :cond_0
    :goto_0
    return v3

    .line 335
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 336
    .local v0, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    .line 338
    const/4 v1, -0x1

    if-ne p4, v1, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->hasLocation()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 339
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startMapView(I)V
    invoke-static {v1, p3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$1000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V

    goto :goto_0

    .line 341
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startPhotoView(I)V
    invoke-static {v1, p3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V

    .line 342
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->access$2300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->removeSuggestionMark(I)V

    .line 343
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, p3}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->setSuggestionEvent(I)V

    goto :goto_0
.end method
