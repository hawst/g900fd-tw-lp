.class Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;
.super Ljava/lang/Object;
.source "SelectionModeBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/SelectionModeBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/ViewObservable;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;->this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "paramView"    # Landroid/view/View;

    .prologue
    .line 69
    const v1, 0x7f0f020c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 70
    .local v0, "twCheckBox":Landroid/widget/CheckBox;
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 73
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;->this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    # getter for: Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->access$000(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)Lcom/sec/samsung/gallery/view/ViewObservable;

    move-result-object v1

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ViewObservable;->notifyObservers(Ljava/lang/Object;)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 76
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/SelectionModeBar$1;->this$0:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    # getter for: Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->mViewObservable:Lcom/sec/samsung/gallery/view/ViewObservable;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->access$000(Lcom/sec/samsung/gallery/view/common/SelectionModeBar;)Lcom/sec/samsung/gallery/view/ViewObservable;

    move-result-object v1

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ViewObservable;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0
.end method
