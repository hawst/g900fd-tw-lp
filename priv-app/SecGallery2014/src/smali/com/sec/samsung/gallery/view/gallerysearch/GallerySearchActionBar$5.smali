.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;
.super Ljava/lang/Object;
.source "GallerySearchActionBar.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnSuggestionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->initializeSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionClick(I)Z
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/database/MatrixCursor;->moveToPosition(I)Z

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 279
    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 284
    const/4 v0, 0x0

    return v0
.end method
