.class public Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;
.super Ljava/lang/Object;
.source "ConfigPhoto.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PhotoView"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;


# instance fields
.field public final edit_mode_checkbox_right_margine:I

.field public final edit_mode_checkbox_size_factor:F

.field public final edit_mode_checkbox_top_margine:I

.field public final newalbum_bitmap_bottom_margin:I

.field public final newalbum_bitmap_right_margin:I

.field public final newalbum_frame_bottom_margin:I

.field public final photoview_bottom_margin:I

.field public final photoview_checkbox_land_multiframe_offset_right_2:I

.field public final photoview_checkbox_land_multiframe_offset_right_3:I

.field public final photoview_checkbox_land_multiframe_offset_right_4:I

.field public final photoview_checkbox_land_right_margin_2:I

.field public final photoview_checkbox_land_right_margin_3:I

.field public final photoview_checkbox_land_right_margin_4:I

.field public final photoview_checkbox_land_size_2:I

.field public final photoview_checkbox_land_size_3:I

.field public final photoview_checkbox_land_size_4:I

.field public final photoview_checkbox_land_top_margin_2:I

.field public final photoview_checkbox_land_top_margin_3:I

.field public final photoview_checkbox_land_top_margin_4:I

.field public final photoview_checkbox_port_multiframe_offset_right_1:I

.field public final photoview_checkbox_port_multiframe_offset_right_2:I

.field public final photoview_checkbox_port_right_margin_1:I

.field public final photoview_checkbox_port_right_margin_2:I

.field public final photoview_checkbox_port_size_1:I

.field public final photoview_checkbox_port_size_2:I

.field public final photoview_checkbox_port_top_margin_1:I

.field public final photoview_checkbox_port_top_margin_2:I

.field public final photoview_focus_padding_bottom:I

.field public final photoview_focus_padding_right:I

.field public final photoview_icon_bottom_margin:I

.field public final photoview_icon_gap:I

.field public final photoview_icon_hight:I

.field public final photoview_icon_left_margin:I

.field public final photoview_icon_width:I

.field public final photoview_left_margin:I

.field public final photoview_multishot_bottom_margin:I

.field public final photoview_multishot_left_margin:I

.field public final photoview_multishot_right_margin:I

.field public final photoview_multishot_top_margin:I

.field public final photoview_right_margin:I

.field public final photoview_top_margin:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 93
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d00f9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_left_margin:I

    .line 94
    const v1, 0x7f0d00fa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_top_margin:I

    .line 95
    const v1, 0x7f0d00fb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_right_margin:I

    .line 96
    const v1, 0x7f0d00fc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_bottom_margin:I

    .line 99
    const v1, 0x7f0d00fd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_left_margin:I

    .line 100
    const v1, 0x7f0d00fe

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_top_margin:I

    .line 101
    const v1, 0x7f0d00ff

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_right_margin:I

    .line 102
    const v1, 0x7f0d0100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_multishot_bottom_margin:I

    .line 105
    const v1, 0x7f0d0101

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_left_margin:I

    .line 106
    const v1, 0x7f0d0104

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_bottom_margin:I

    .line 107
    const v1, 0x7f0d0105

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_gap:I

    .line 108
    const v1, 0x7f0d0106

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_width:I

    .line 109
    const v1, 0x7f0d0107

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_icon_hight:I

    .line 111
    const v1, 0x7f0c0022

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->newalbum_frame_bottom_margin:I

    .line 112
    const v1, 0x7f0c0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->newalbum_bitmap_right_margin:I

    .line 113
    const v1, 0x7f0c0024

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->newalbum_bitmap_bottom_margin:I

    .line 116
    const v1, 0x7f0c0025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_right_margin_2:I

    .line 117
    const v1, 0x7f0c0026

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_top_margin_2:I

    .line 118
    const v1, 0x7f0c0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_size_2:I

    .line 120
    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_right_margin_3:I

    .line 121
    const v1, 0x7f0c0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_top_margin_3:I

    .line 122
    const v1, 0x7f0c002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_size_3:I

    .line 124
    const v1, 0x7f0c002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_right_margin_4:I

    .line 125
    const v1, 0x7f0c002c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_top_margin_4:I

    .line 126
    const v1, 0x7f0c002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_size_4:I

    .line 128
    const v1, 0x7f0c002e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_right_margin_1:I

    .line 129
    const v1, 0x7f0c002f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_top_margin_1:I

    .line 130
    const v1, 0x7f0c0030

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_size_1:I

    .line 132
    const v1, 0x7f0c0031

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_right_margin_2:I

    .line 133
    const v1, 0x7f0c0032

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_top_margin_2:I

    .line 134
    const v1, 0x7f0c0033

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_size_2:I

    .line 136
    const v1, 0x7f0c0034

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_multiframe_offset_right_2:I

    .line 137
    const v1, 0x7f0c0035

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_multiframe_offset_right_3:I

    .line 138
    const v1, 0x7f0c0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_land_multiframe_offset_right_4:I

    .line 139
    const v1, 0x7f0c0037

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_multiframe_offset_right_1:I

    .line 140
    const v1, 0x7f0c0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_checkbox_port_multiframe_offset_right_2:I

    .line 142
    const v1, 0x7f0c0039

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_focus_padding_right:I

    .line 143
    const v1, 0x7f0c003a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_focus_padding_bottom:I

    .line 145
    const v1, 0x7f0d0153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_top_margine:I

    .line 146
    const v1, 0x7f0d0154

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_right_margine:I

    .line 147
    const v1, 0x7f0e001f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_size_factor:F

    .line 148
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 83
    const-class v1, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->sInstance:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->sInstance:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    .line 86
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->sInstance:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 83
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
