.class public Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowEditBurstShotDialogCmd.java"


# static fields
.field private static final ARCSOFT_PICLEAR_PICTURE_SIZE_KEY:Ljava/lang/String; = "size_key"

.field private static final BURSTSHOT_BESTPHOTO_ACTIVITY:Ljava/lang/String; = "com.arcsoft.burstshoteditor.BestFace"

.field private static final BURSTSHOT_ERASER_ACTIVITY:Ljava/lang/String; = "com.arcsoft.burstshoteditor.Eraser"

.field private static final BURSTSHOT_PACKAGE:Ljava/lang/String; = "com.arcsoft.burstshoteditor"

.field private static final PICMOVIE_ACTIVITY:Ljava/lang/String; = "com.arcsoft.burstshoteditor.animatedphoto.Launch"

.field private static final PICMOVIE_PACKAGE:Ljava/lang/String; = "com.arcsoft.burstshoteditor.animatedphoto"

.field private static final SELECTED_ITEMS_KEY:Ljava/lang/String; = "selectedItems"

.field private static final TAG:Ljava/lang/String; = "ShowEditBurstShotDialogCmd"


# instance fields
.field burstShotList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mEditBusrtShotDialog:Landroid/app/AlertDialog;

.field private mMenuList:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mEditBusrtShotDialog:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startPicMovie(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startBestPhotos(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startClear(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-void
.end method

.method private showPopup(Ljava/lang/String;)V
    .locals 4
    .param p1, "packagename"    # Ljava/lang/String;

    .prologue
    .line 125
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 126
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.popupuireceiver"

    const-string v3, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    const-string v2, "app_package_name"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 135
    :goto_0
    return-void

    .line 131
    :catch_0
    move-exception v0

    .line 132
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "ShowEditBurstShotDialogCmd"

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startBestPhotos(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "iFolderNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 94
    .local v3, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v4, "com.arcsoft.burstshoteditor"

    const-string v5, "com.arcsoft.burstshoteditor.BestFace"

    invoke-direct {v1, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    .local v1, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 97
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "selectedItems"

    invoke-virtual {v0, v4, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 99
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 102
    :try_start_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-void

    .line 103
    :catch_0
    move-exception v2

    .line 104
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "com.arcsoft.burstshoteditor"

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->showPopup(Ljava/lang/String;)V

    .line 105
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startClear(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    .local p2, "iFolderNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 73
    .local v3, "intent":Landroid/content/Intent;
    const/4 v5, 0x2

    new-array v4, v5, [I

    .line 76
    .local v4, "pictureSize":[I
    new-instance v1, Landroid/content/ComponentName;

    const-string v5, "com.arcsoft.burstshoteditor"

    const-string v6, "com.arcsoft.burstshoteditor.Eraser"

    invoke-direct {v1, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    .local v1, "component":Landroid/content/ComponentName;
    invoke-virtual {v3, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "selectedItems"

    invoke-virtual {v0, v5, p2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 81
    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 82
    const-string/jumbo v5, "size_key"

    invoke-virtual {v3, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 85
    :try_start_0
    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :goto_0
    return-void

    .line 86
    :catch_0
    move-exception v2

    .line 87
    .local v2, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "com.arcsoft.burstshoteditor.animatedphoto"

    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->showPopup(Ljava/lang/String;)V

    .line 88
    invoke-virtual {v2}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private startPicMovie(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "iFolderNames":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 111
    .local v2, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/content/ComponentName;

    const-string v3, "com.arcsoft.burstshoteditor.animatedphoto"

    const-string v4, "com.arcsoft.burstshoteditor.animatedphoto.Launch"

    invoke-direct {v0, v3, v4}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    .local v0, "comp":Landroid/content/ComponentName;
    invoke-virtual {v2, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 113
    const-string v3, "android.intent.action.MAIN"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 114
    const-string v3, "selectedItems"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 117
    :try_start_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v1

    .line 119
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "com.arcsoft.burstshoteditor.animatedphoto"

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->showPopup(Ljava/lang/String;)V

    .line 120
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "notification"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 29
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 30
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;

    .line 31
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->burstShotList:Ljava/util/ArrayList;

    .line 32
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->showSubOptions()V

    .line 33
    return-void
.end method

.method protected showSubOptions()V
    .locals 3

    .prologue
    .line 35
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f090043

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mMenuList:[Ljava/lang/String;

    .line 37
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mEditBusrtShotDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mEditBusrtShotDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_1

    .line 38
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 39
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0e02f0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 40
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mMenuList:[Ljava/lang/String;

    new-instance v2, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 58
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mEditBusrtShotDialog:Landroid/app/AlertDialog;

    .line 60
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_1
    return-void
.end method
