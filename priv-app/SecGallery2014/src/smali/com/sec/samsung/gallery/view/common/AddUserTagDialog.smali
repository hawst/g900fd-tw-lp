.class public Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "AddUserTagDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;
    }
.end annotation


# static fields
.field private static final HIDE_SOFT_INPUT:I = 0x0

.field public static final INVALID_CHAR:[Ljava/lang/String;

.field private static final IS_VISIBLE_WINDOW:Ljava/lang/String; = "AxT9IME.isVisibleWindow"

.field private static final RESPONSE_AXT9INFO:Ljava/lang/String; = "ResponseAxT9Info"


# instance fields
.field private isSipVisible:Z

.field private final mContext:Landroid/content/Context;

.field private mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

.field private mHandler:Landroid/os/Handler;

.field private mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

.field private mReceiveTime:J

.field private mRequestCode:I

.field private mSipIntentFilter:Landroid/content/IntentFilter;

.field private mSipReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 62
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->INVALID_CHAR:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 67
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mHandler:Landroid/os/Handler;

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mReceiveTime:J

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mRequestCode:I

    .line 80
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    .line 81
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    .line 82
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mHandler:Landroid/os/Handler;

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    .line 96
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->hideSoftInput(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->isSipVisible:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 59
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->isSipVisible:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    iget-wide v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mReceiveTime:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .param p1, "x1"    # J

    .prologue
    .line 59
    iput-wide p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/util/ArrayList;

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->startAddTagListActivity(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mInputMethodManager:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->setSipBroadcastReceiver()V

    return-void
.end method

.method private hideSoftInput(Landroid/os/IBinder;)V
    .locals 4
    .param p1, "windowToken"    # Landroid/os/IBinder;

    .prologue
    .line 471
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 473
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 475
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 479
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 476
    :catch_0
    move-exception v0

    .line 477
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private setSipBroadcastReceiver()V
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    if-nez v0, :cond_0

    .line 422
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    const-string v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_1

    .line 426
    new-instance v0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    .line 434
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mSipIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 435
    return-void
.end method

.method private startAddTagListActivity(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 7
    .param p1, "inputTagText"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v4, 0x1

    .line 484
    new-instance v0, Landroid/content/Intent;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    const-class v5, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-direct {v0, v3, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 485
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 486
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 487
    .local v1, "sm":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-eqz v1, :cond_4

    .line 488
    const-string v5, "isDetailView"

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v3

    const/4 v6, 0x2

    if-ge v3, v6, :cond_3

    move v3, v4

    :goto_0
    invoke-virtual {v0, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 492
    .end local v1    # "sm":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_0
    :goto_1
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    .line 493
    const-string v3, "inputTagText"

    invoke-virtual {v0, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 495
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 496
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 497
    .local v2, "uriArray":[Ljava/lang/String;
    const-string/jumbo v3, "uriList"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 499
    .end local v2    # "uriArray":[Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    iget v4, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mRequestCode:I

    invoke-virtual {v3, v0, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 500
    return-void

    .line 488
    .restart local v1    # "sm":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 490
    :cond_4
    const-string v3, "isDetailView"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1
.end method


# virtual methods
.method public dismissAddTagDialog()V
    .locals 4

    .prologue
    .line 454
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 455
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 456
    .local v0, "dlg":Landroid/app/Dialog;
    if-eqz v0, :cond_1

    .line 457
    const v3, 0x7f0f0052

    invoke-virtual {v0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 458
    .local v1, "edit":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 459
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 460
    .local v2, "msg":Landroid/os/Message;
    const/4 v3, 0x0

    iput v3, v2, Landroid/os/Message;->what:I

    .line 461
    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    iput-object v3, v2, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 462
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mHandler:Landroid/os/Handler;

    invoke-virtual {v3, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 464
    .end local v2    # "msg":Landroid/os/Message;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->dismiss()V

    .line 467
    .end local v0    # "dlg":Landroid/app/Dialog;
    .end local v1    # "edit":Landroid/view/View;
    :cond_1
    return-void
.end method

.method public setRequestCode(I)V
    .locals 0
    .param p1, "requestCode"    # I

    .prologue
    .line 450
    iput p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mRequestCode:I

    .line 451
    return-void
.end method

.method public setSelectedItemUriList(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 446
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->setSelectedItemUriList(Ljava/util/ArrayList;)V

    .line 447
    return-void
.end method

.method public showAddTagDialog()V
    .locals 4

    .prologue
    .line 439
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mDialogFragment:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "dialog"

    invoke-virtual {v2, v1, v3}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 443
    :goto_0
    return-void

    .line 440
    :catch_0
    move-exception v0

    .line 441
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method
