.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2782
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 6
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2784
    const/4 v0, 0x0

    .line 2785
    .local v0, "handled":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v4, v5, :cond_0

    .line 2795
    :goto_0
    return v3

    .line 2787
    :cond_0
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLongPressAlbums:Z

    if-eqz v4, :cond_2

    .line 2788
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-nez v4, :cond_1

    .line 2789
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4, v3, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 2790
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2792
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$19;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v4, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectAlbum(I)I

    move-result v1

    .line 2793
    .local v1, "selectAlbumResult":I
    if-lez v1, :cond_3

    move v0, v2

    .end local v1    # "selectAlbumResult":I
    :cond_2
    :goto_1
    move v3, v0

    .line 2795
    goto :goto_0

    .restart local v1    # "selectAlbumResult":I
    :cond_3
    move v0, v3

    .line 2793
    goto :goto_1
.end method
