.class public abstract Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "AbstractActionBarViewForSelection.java"


# static fields
.field protected static final MAX_MAKE_COLLAGE_SELECTION_NUMBER:I = 0x6

.field protected static final MIN_MAKE_COLLAGE_SELECTION_NUMBER:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field public static final USE_ANIMATION_FOR_SELECT_ALL:Z


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mIsPopupShow:Z

.field private mIsSelectAll:Z

.field private mNumberOfItemsSelected:I

.field protected mPopupMenu:Landroid/widget/ListPopupWindow;

.field private mPopupMenuItemWidth:I

.field private mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

.field protected mSelectionActionBarView:Landroid/view/View;

.field private mSelectionButton:Landroid/widget/TextView;

.field protected mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

.field private mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

.field private mSelectionText:Landroid/widget/TextView;

.field private mTotalSelectedItemsCount:I

.field private mTransParentSelectionBar:Z

.field protected mUsingSelectionModeBar:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsPopupShow:Z

    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTransParentSelectionBar:Z

    .line 69
    iput v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 72
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->initialize()V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "style"    # I

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsPopupShow:Z

    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTransParentSelectionBar:Z

    .line 69
    iput v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 77
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->initialize()V

    .line 78
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "transparent"    # Z

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 57
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    .line 58
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsPopupShow:Z

    .line 63
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 67
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTransParentSelectionBar:Z

    .line 69
    iput v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 82
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTransParentSelectionBar:Z

    .line 83
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->initialize()V

    .line 85
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setActionBarView()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;Landroid/widget/TextView;)Landroid/widget/TextView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
    .param p1, "x1"    # Landroid/widget/TextView;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    return-object p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    return v0
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)Landroid/widget/LinearLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private initialize()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$1;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 95
    return-void
.end method

.method private setActionBarView()V
    .locals 5

    .prologue
    const/4 v2, -0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 158
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v1, :cond_2

    .line 159
    new-instance v0, Landroid/app/ActionBar$LayoutParams;

    const/16 v1, 0x15

    invoke-direct {v0, v2, v2, v1}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    .line 160
    .local v0, "layoutParam":Landroid/app/ActionBar$LayoutParams;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 161
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 162
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 163
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 176
    .end local v0    # "layoutParam":Landroid/app/ActionBar$LayoutParams;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 177
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 180
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseBackIconInSelection:Z

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 182
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 185
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 187
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-nez v1, :cond_1

    .line 188
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200f3

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 189
    :cond_1
    return-void

    .line 165
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 166
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 167
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_3

    .line 168
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 169
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0

    .line 171
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v4}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method


# virtual methods
.method public disablePopupMenu()V
    .locals 0

    .prologue
    .line 371
    return-void
.end method

.method protected dismissPopupMenu()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 381
    :cond_0
    return-void
.end method

.method public getMaxCount()I
    .locals 3

    .prologue
    .line 231
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 232
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "pick-max-item"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public getPopUpMenu()Landroid/widget/ListPopupWindow;
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    return-object v0
.end method

.method public getSelectedItemsCount()I
    .locals 1

    .prologue
    .line 356
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    return v0
.end method

.method public getSelectionModeBar()Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1

    .prologue
    .line 707
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method public getSelectionModeBarHeight()I
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->getSelectionModeBarHeight()I

    move-result v0

    .line 703
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTotalSelectedItemsCount()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    return v0
.end method

.method protected isMultiPickMode()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 212
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 213
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "multi-pick"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method protected isPersonPickMode()Z
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isPickMode()Z
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isPopupMenuVisible()Z
    .locals 1

    .prologue
    .line 374
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsPopupShow:Z

    return v0
.end method

.method protected isSinglePickMode()Z
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 675
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 676
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-nez v0, :cond_0

    .line 684
    :goto_0
    return-void

    .line 679
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    if-nez v0, :cond_1

    .line 680
    sget-object v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    const-string v1, "mSelectionModeBar is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 683
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->onConfigChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 653
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    .line 654
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$15;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$15;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 671
    return-void
.end method

.method public onPause(Z)V
    .locals 2
    .param p1, "useAnim"    # Z

    .prologue
    .line 631
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onPause()V

    .line 632
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 649
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 611
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->onResume()V

    .line 612
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$13;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 628
    return-void
.end method

.method protected processSelectAllMenuItemClick(II)V
    .locals 2
    .param p1, "itemId"    # I
    .param p2, "quantitySelectedAlbum"    # I

    .prologue
    .line 522
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V

    .line 524
    packed-switch p1, :pswitch_data_0

    .line 536
    :goto_0
    return-void

    .line 526
    :pswitch_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 527
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    invoke-virtual {p0, v0, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuItemsVisibility(ZI)V

    .line 528
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 531
    :pswitch_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 532
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    invoke-virtual {p0, v0, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuItemsVisibility(ZI)V

    .line 533
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 524
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public selectionModeBarVisibiltiy(IZ)V
    .locals 2
    .param p1, "visible"    # I
    .param p2, "enableAnim"    # Z

    .prologue
    .line 688
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$16;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$16;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;IZ)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 696
    return-void
.end method

.method public setCheckboxState(Z)V
    .locals 1
    .param p1, "checked"    # Z

    .prologue
    .line 605
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 606
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 607
    :cond_0
    return-void
.end method

.method public setEnableSelectionOnActionBar(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 589
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 591
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 592
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 593
    if-eqz p1, :cond_3

    .line 594
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x3f4ccccd    # 0.8f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 598
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 599
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_2

    .line 601
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 602
    :cond_2
    return-void

    .line 596
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setAlpha(F)V

    goto :goto_0
.end method

.method protected setSelectAllButtonTTS(Ljava/lang/String;)V
    .locals 2
    .param p1, "contentDescrption"    # Ljava/lang/String;

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f001e

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionText:Landroid/widget/TextView;

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionText:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 312
    :cond_0
    return-void
.end method

.method protected setSelectAllButtonTitle(IILjava/lang/String;)V
    .locals 3
    .param p1, "numberOfItemsSelected"    # I
    .param p2, "countOfMediaItem"    # I
    .param p3, "numberOfSelectedItems"    # Ljava/lang/String;

    .prologue
    .line 277
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 278
    sget-object v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    const-string v2, "setSelectAllButtonTitle() mSelectionActionBarView is null."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    .line 282
    iput p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    .line 284
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v1, :cond_2

    .line 285
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, p3}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 286
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_3

    .line 287
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0021

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    .line 288
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 290
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0018

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    .line 291
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 293
    .local v0, "viewObserver":Landroid/view/ViewTreeObserver;
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$5;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 301
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setSelectAllButtonTitle(ILjava/lang/String;)V
    .locals 3
    .param p1, "numberOfItemsSelected"    # I
    .param p2, "numberOfSelectedItems"    # Ljava/lang/String;

    .prologue
    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    if-nez v1, :cond_1

    .line 243
    sget-object v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    const-string v2, "setSelectAllButtonTitle() mSelectionActionBarView is null."

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    .line 247
    iput p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTotalSelectedItemsCount:I

    .line 249
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v1, :cond_2

    .line 250
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, p2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 251
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_3

    .line 252
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0021

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    .line 253
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 256
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0018

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    .line 257
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 259
    .local v0, "viewObserver":Landroid/view/ViewTreeObserver;
    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$4;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 267
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 269
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfSelectedItems"    # I

    .prologue
    .line 366
    iput p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    .line 367
    return-void
.end method

.method protected setSelectionModeBar()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 98
    sget-object v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mUsingSelectionModeBar : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v1, :cond_2

    .line 101
    new-instance v1, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/ViewObservable;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mTransParentSelectionBar:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setTransparentBackground(Z)V

    .line 106
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(I)V

    .line 108
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 110
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setTextColor(I)V

    .line 116
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030010

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    .line 153
    :goto_1
    return-void

    .line 113
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setTextColor(I)V

    goto :goto_0

    .line 117
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_2
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_3

    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030011

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    .line 119
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f001f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    .line 120
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f0020

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    const v2, 0x7f0f0021

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionButton:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeOnActionBarLayout:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$2;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v2, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$3;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 151
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f03000f

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionActionBarView:Landroid/view/View;

    goto :goto_1
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 385
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 397
    return-void
.end method

.method protected setupPopupMenu(Ljava/lang/String;)V
    .locals 21
    .param p1, "buttonTitle"    # Ljava/lang/String;

    .prologue
    .line 400
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0f0018

    invoke-virtual/range {v17 .. v18}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/Button;

    .line 401
    .local v14, "selectAllButton":Landroid/widget/Button;
    if-eqz v14, :cond_5

    .line 402
    sget v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSystemUiVisibility:I

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setSystemUiVisibility(I)V

    .line 403
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 405
    new-instance v17, Landroid/widget/ListPopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-direct/range {v17 .. v18}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 407
    .local v13, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    const v18, 0x7f0d0006

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v0, v0

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 409
    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v15, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    const v18, 0x7f0e0048

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    const/16 v17, 0x1

    const v18, 0x7f0e0049

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v17

    .line 410
    .local v15, "strs":[Ljava/lang/String;
    invoke-virtual {v14}, Landroid/widget/Button;->getPaint()Landroid/text/TextPaint;

    move-result-object v8

    .line 411
    .local v8, "paint":Landroid/text/TextPaint;
    const/4 v3, 0x0

    .line 412
    .local v3, "contentWidth":F
    move-object v2, v15

    .local v2, "arr$":[Ljava/lang/String;
    array-length v6, v2

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v7, v2, v5

    .line 413
    .local v7, "menuOption":Ljava/lang/String;
    invoke-virtual {v8, v7}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v17

    move/from16 v0, v17

    invoke-static {v3, v0}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 412
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 415
    .end local v7    # "menuOption":Ljava/lang/String;
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v17

    if-eqz v17, :cond_6

    .line 416
    const v17, 0x7f0d0006

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v10, v0

    .line 418
    .local v10, "popupMenuWidth":I
    const v17, 0x7f0d025c

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 421
    .local v9, "popupMenuItemPaddings":I
    float-to-int v0, v3

    move/from16 v17, v0

    mul-int/lit8 v18, v9, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 423
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v10, :cond_1

    .line 424
    move-object/from16 v0, p0

    iput v10, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 444
    .end local v10    # "popupMenuWidth":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    move/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 446
    if-eqz p1, :cond_2

    .line 447
    move-object/from16 v0, p1

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 449
    :cond_2
    new-instance v17, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$7;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    move-object/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 466
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 467
    const v17, 0x7f0d0259

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 468
    .local v4, "horizontalOffset":I
    const v17, 0x7f0d025a

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/ActionBar;->getHeight()I

    move-result v18

    sub-int v16, v17, v18

    .line 469
    .local v16, "verticalOffset":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 470
    if-eqz v4, :cond_3

    .line 471
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 473
    :cond_3
    new-instance v17, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const v19, 0x7f0300cc

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    move/from16 v20, v0

    invoke-direct/range {v17 .. v20}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    .line 474
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->isPersonPickMode()Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->getCount()I

    move-result v17

    if-nez v17, :cond_4

    .line 475
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updateButton(Z)V

    .line 476
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 477
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 479
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$8;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$8;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual/range {v17 .. v18}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setSelectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$9;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$9;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual/range {v17 .. v18}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 495
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$10;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$10;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual/range {v17 .. v18}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setSelectedItemCountTextClickListener(Landroid/view/View$OnClickListener;)V

    .line 503
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$11;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V

    invoke-virtual/range {v17 .. v18}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 518
    .end local v2    # "arr$":[Ljava/lang/String;
    .end local v3    # "contentWidth":F
    .end local v4    # "horizontalOffset":I
    .end local v5    # "i$":I
    .end local v6    # "len$":I
    .end local v8    # "paint":Landroid/text/TextPaint;
    .end local v9    # "popupMenuItemPaddings":I
    .end local v13    # "res":Landroid/content/res/Resources;
    .end local v15    # "strs":[Ljava/lang/String;
    .end local v16    # "verticalOffset":I
    :cond_5
    return-void

    .line 427
    .restart local v2    # "arr$":[Ljava/lang/String;
    .restart local v3    # "contentWidth":F
    .restart local v5    # "i$":I
    .restart local v6    # "len$":I
    .restart local v8    # "paint":Landroid/text/TextPaint;
    .restart local v13    # "res":Landroid/content/res/Resources;
    .restart local v15    # "strs":[Ljava/lang/String;
    :cond_6
    const v17, 0x7f0d0006

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v12, v0

    .line 429
    .local v12, "popupMenuWidthMin":I
    const v17, 0x7f0d0257

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v11, v0

    .line 431
    .local v11, "popupMenuWidthMax":I
    const v17, 0x7f0d025c

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v17

    move/from16 v0, v17

    float-to-int v9, v0

    .line 434
    .restart local v9    # "popupMenuItemPaddings":I
    mul-int/lit8 v17, v9, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    add-float v17, v17, v3

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 436
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v0, v12, :cond_7

    .line 437
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    .line 439
    :cond_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-le v0, v11, :cond_1

    .line 440
    move-object/from16 v0, p0

    iput v11, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenuItemWidth:I

    goto/16 :goto_1
.end method

.method protected updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 2
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 540
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 541
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$12;-><init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;ILcom/sec/samsung/gallery/util/Consts$ButtonType;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 586
    return-void
.end method

.method public updateButton(Z)V
    .locals 1
    .param p1, "toggleImage"    # Z

    .prologue
    .line 351
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 352
    iget v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mNumberOfItemsSelected:I

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuItemsVisibility(ZI)V

    .line 353
    return-void
.end method

.method public updateFuntionSimplificationMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 713
    const/4 v0, 0x1

    return v0
.end method

.method protected updatePopupMenuButtonState()Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x1

    .line 192
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 193
    .local v0, "itemCount":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->getMaxCount()I

    move-result v1

    .line 194
    .local v1, "pickMaxCount":I
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    invoke-virtual {p0, v3, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuItemsVisibility(ZI)V

    .line 195
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->isMultiPickMode()Z

    move-result v3

    if-eqz v3, :cond_1

    if-lez v1, :cond_1

    if-ge v0, v1, :cond_1

    .line 208
    :cond_0
    :goto_0
    return v2

    .line 197
    :cond_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGalleryWidget(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    if-ne v1, v4, :cond_3

    .line 198
    const/16 v1, 0x3e8

    .line 199
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->isMultiPickMode()Z

    move-result v3

    if-eqz v3, :cond_2

    if-lez v1, :cond_2

    if-lt v0, v1, :cond_0

    .line 208
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 202
    :cond_3
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromInsideGallery(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2

    if-ne v1, v4, :cond_2

    .line 203
    sget v1, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    .line 204
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->isMultiPickMode()Z

    move-result v3

    if-eqz v3, :cond_2

    if-lez v1, :cond_2

    if-ge v0, v1, :cond_2

    goto :goto_0
.end method

.method protected updatePopupMenuItemsVisibility(ZI)V
    .locals 9
    .param p1, "isSelectAll"    # Z
    .param p2, "quantitySelectedAlbum"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 315
    sget-object v6, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "updatePopupMenuItemsVisibility isSelectAll : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v6, :cond_2

    .line 317
    if-lez p2, :cond_3

    move v1, v4

    .line 318
    .local v1, "isUnsellectAll":Z
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v6, v7, :cond_4

    move v3, v4

    .line 319
    .local v3, "tabTagAlbum":Z
    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCurrentMediaSetSelectedCount()I

    move-result v6

    if-lez v6, :cond_5

    move v0, v4

    .line 320
    .local v0, "currentAlbumselectedCount":Z
    :goto_2
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 321
    const/4 p1, 0x0

    .line 323
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->isMultiPickMode()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 324
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->getMaxCount()I

    move-result v2

    .line 325
    .local v2, "maxPickCount":I
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-eqz v6, :cond_8

    .line 326
    if-gez v2, :cond_6

    .line 327
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 346
    .end local v2    # "maxPickCount":I
    :cond_1
    :goto_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    if-nez p1, :cond_b

    :goto_4
    invoke-virtual {v6, v4, v1}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setMenuEnable(ZZ)V

    .line 348
    .end local v0    # "currentAlbumselectedCount":Z
    .end local v1    # "isUnsellectAll":Z
    .end local v3    # "tabTagAlbum":Z
    :cond_2
    return-void

    :cond_3
    move v1, v5

    .line 317
    goto :goto_0

    .restart local v1    # "isUnsellectAll":Z
    :cond_4
    move v3, v5

    .line 318
    goto :goto_1

    .restart local v3    # "tabTagAlbum":Z
    :cond_5
    move v0, v5

    .line 319
    goto :goto_2

    .line 329
    .restart local v0    # "currentAlbumselectedCount":Z
    .restart local v2    # "maxPickCount":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->updatePopupMenuButtonState()Z

    move-result v6

    if-eqz v6, :cond_7

    .line 330
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    goto :goto_3

    .line 332
    :cond_7
    const/4 p1, 0x1

    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    goto :goto_3

    .line 336
    :cond_8
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 337
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->getMaxCount()I

    move-result v6

    if-ne p2, v6, :cond_9

    if-nez v0, :cond_9

    if-eqz v3, :cond_9

    .line 338
    const/4 p1, 0x0

    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mIsSelectAll:Z

    .line 340
    :cond_9
    move v1, v0

    .line 341
    if-nez v3, :cond_1

    .line 342
    if-lez p2, :cond_a

    move v1, v4

    :goto_5
    goto :goto_3

    :cond_a
    move v1, v5

    goto :goto_5

    .end local v2    # "maxPickCount":I
    :cond_b
    move v4, v5

    .line 346
    goto :goto_4
.end method
