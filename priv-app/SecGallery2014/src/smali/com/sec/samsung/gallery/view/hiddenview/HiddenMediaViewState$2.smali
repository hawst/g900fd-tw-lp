.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMediaList(Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p1, "list":Ljava/util/Set;, "Ljava/util/Set<Lcom/sec/android/gallery3d/data/Path;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    if-lez v2, :cond_1

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 133
    .local v1, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    new-instance v3, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;)V

    invoke-virtual {v2, v3}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 148
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    return-void
.end method
