.class final enum Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
.super Ljava/lang/Enum;
.source "GallerySearchViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "TagCloudViewMode"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

.field public static final enum LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

.field public static final enum MULTILINE:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

.field public static final enum REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 182
    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    const-string v1, "REGULAR"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    const-string v1, "MULTILINE"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->MULTILINE:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    new-instance v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    const-string v1, "LIST"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->REGULAR:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->MULTILINE:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->LIST:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    aput-object v1, v0, v4

    sput-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->$VALUES:[Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 182
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 182
    const-class v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->$VALUES:[Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$TagCloudViewMode;

    return-object v0
.end method
