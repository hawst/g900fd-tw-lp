.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$2;
.super Ljava/lang/Object;
.source "MoreInfoUserTagEditView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->initView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l_position"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v2, 0x7f0f01ab

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 137
    .local v0, "checkbox":Landroid/widget/CheckBox;
    if-eqz v0, :cond_0

    .line 138
    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x1

    .line 139
    .local v1, "checked":Z
    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 140
    if-nez v1, :cond_2

    .line 141
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->removeTag(I)V

    .line 146
    .end local v1    # "checked":Z
    :cond_0
    :goto_1
    return-void

    .line 138
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 143
    .restart local v1    # "checked":Z
    :cond_2
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->addTag(I)V

    goto :goto_1
.end method
