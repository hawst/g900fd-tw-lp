.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "DetailViewOCRDialog.java"


# instance fields
.field private final mCtx:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "t"    # I
    .param p3, "msg"    # Ljava/lang/String;

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mCtx:Landroid/content/Context;

    .line 21
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->createDialog()V

    .line 22
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->startMoreService()V

    return-void
.end method

.method private startMoreService()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/high16 v2, 0x14000000

    .line 65
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mCtx:Landroid/content/Context;

    invoke-static {v1}, Lcom/dmc/ocr/OcrUtils;->isSamsungAppsAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 67
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "samsungapps://ProductDetail/com.sec.android.app.ocr3"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 68
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 69
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 76
    :goto_0
    return-void

    .line 71
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 72
    .restart local v0    # "intent":Landroid/content/Intent;
    const-string v1, "market://details?id=com.sec.android.app.ocr3"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 73
    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 74
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mCtx:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method public createDialog()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mCtx:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 38
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v1, 0x7f0e01fd

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 39
    const v1, 0x7f0e019c

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 40
    const v1, 0x7f0e010d

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    const v1, 0x7f0e0046

    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 59
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 62
    return-void
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 34
    :cond_0
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCRDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 28
    :cond_0
    return-void
.end method
