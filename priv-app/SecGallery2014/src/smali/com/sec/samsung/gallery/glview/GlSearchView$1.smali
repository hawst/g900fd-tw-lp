.class Lcom/sec/samsung/gallery/glview/GlSearchView$1;
.super Ljava/lang/Object;
.source "GlSearchView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlSearchView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 143
    const-string v0, "GlSearchView"

    const-string v1, "CMD_UPDATE_ITEM"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    .line 145
    return-void
.end method

.method public onChanged(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 149
    if-ne p2, v2, :cond_1

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mVisibleFirst:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mVisibleLast:I

    if-gt p1, v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0, v3, p1, v2, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    .line 158
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    if-gt v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    if-gt p1, v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlSearchView;

    invoke-virtual {v0, v3, p1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    goto :goto_0
.end method
