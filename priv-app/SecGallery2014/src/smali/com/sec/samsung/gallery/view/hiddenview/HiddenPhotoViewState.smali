.class public Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "HiddenPhotoViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1

.field private static final TAG:Ljava/lang/String; = "HiddenPhotoViewState"

.field private static final THUMB_CACHE_SIZE:I = 0x30


# instance fields
.field private mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mBgImageIndex:I

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

.field private mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mIsDeleteMode:Z

.field private mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

.field private mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

.field private mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

.field private mMediaSetPath:Ljava/lang/String;

.field private mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mScrolledPosition:I

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSplitScroll:F

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mThumbScroll:F

.field private mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 69
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 73
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 77
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    .line 82
    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I

    .line 83
    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I

    .line 86
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSetPath:Ljava/lang/String;

    .line 88
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mIsDeleteMode:Z

    .line 92
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;

    const-string v1, "HIDDEN_ITEM_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$1;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 120
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$2;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    .line 140
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 141
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$3;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 155
    new-instance v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$4;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->exitSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mBgImageIndex:I

    return p1
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # I

    .prologue
    .line 69
    iput p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->selectMedia(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->handleHiddenItemsLaunch(I)V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;
    .param p1, "x1"    # I

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->genericMotionEnter(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->genericMotionCancel()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/core/LaunchModeType;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    return-object v0
.end method

.method private checkMediaAvailability()V
    .locals 1

    .prologue
    .line 542
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 543
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->exitViewState()V

    .line 545
    :cond_0
    return-void
.end method

.method private enterSelectionMode(Z)V
    .locals 6
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 476
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 477
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 478
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 481
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    invoke-virtual {v0, v1, v5, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 482
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V

    .line 483
    return-void
.end method

.method private exitSelectionMode(Z)V
    .locals 5
    .param p1, "doRefresh"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 486
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setMediaSelectionCallback(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$MediaSelectionCallback;)V

    .line 487
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 488
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 489
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 490
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 491
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0e01d9

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 492
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    if-eqz v0, :cond_0

    .line 493
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 494
    :cond_0
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mIsDeleteMode:Z

    .line 495
    return-void
.end method

.method private exitViewState()V
    .locals 1

    .prologue
    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 538
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 539
    return-void
.end method

.method private genericMotionCancel()V
    .locals 3

    .prologue
    .line 298
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 299
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setGenericMotionFocus(I)V

    .line 300
    if-ltz v0, :cond_0

    .line 301
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 303
    :cond_0
    return-void
.end method

.method private genericMotionEnter(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 289
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 290
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setGenericMotionFocus(I)V

    .line 291
    if-ltz v0, :cond_0

    .line 292
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 294
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->notifyDataSetChanged(II)V

    .line 295
    return-void
.end method

.method private handleHiddenItemsLaunch(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v6, 0x1

    .line 548
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 549
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v0, 0x0

    .line 551
    .local v0, "body":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    .line 552
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v2, :cond_0

    .line 568
    :goto_0
    return-void

    .line 555
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v4, v5, :cond_1

    .line 556
    const/4 v4, 0x2

    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v3, v4

    aput-object v2, v3, v6

    .line 559
    .local v3, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "PICKER_ITEM_SELECTED"

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 563
    .end local v3    # "params":[Ljava/lang/Object;
    :cond_1
    const-string v4, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 564
    const-string v4, "KEY_MEDIA_SET_PATH"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSetPath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    const-string v4, "KEY_ITEM_POSITION"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 566
    const-string v4, "KEY_ITEM_IS_HIDDEN"

    invoke-virtual {v1, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 567
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v4, v5, v1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private handleResultWallpaper(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "result"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 599
    if-eq p2, v6, :cond_0

    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v0, v4

    .line 601
    .local v0, "finishActivity":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 602
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_2

    .end local p1    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v3, p2, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 603
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 608
    :goto_2
    return-void

    .end local v0    # "finishActivity":Z
    .restart local p1    # "data":Landroid/content/Intent;
    :cond_1
    move v0, v3

    .line 599
    goto :goto_0

    .restart local v0    # "finishActivity":Z
    :cond_2
    move-object p1, v2

    .line 602
    goto :goto_1

    .line 605
    :cond_3
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v3

    aput-object v2, v1, v4

    .line 606
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private isAvailableCount(IZ)Z
    .locals 9
    .param p1, "selected"    # I
    .param p2, "showToast"    # Z

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 621
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v6, :cond_1

    .line 622
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 623
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v6, :cond_1

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v6, :cond_1

    .line 637
    .end local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :cond_0
    :goto_0
    return v4

    .line 628
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 629
    .local v1, "intent":Landroid/content/Intent;
    const-string v6, "pick-max-item"

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v1, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 630
    .local v2, "maxPickCount":I
    if-le p1, v2, :cond_0

    .line 631
    if-eqz p2, :cond_2

    .line 632
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0117

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 633
    .local v3, "text":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .end local v3    # "text":Ljava/lang/CharSequence;
    :cond_2
    move v4, v5

    .line 635
    goto :goto_0
.end method

.method private selectAll()V
    .locals 5

    .prologue
    .line 519
    const/4 v1, 0x0

    .local v1, "position":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v2

    .local v2, "size":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 520
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 521
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 519
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 523
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 524
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 525
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 526
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    .line 527
    return-void
.end method

.method private selectMedia(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 498
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getMedia(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 500
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_0

    .line 501
    const-string v2, "HiddenPhotoViewState"

    const-string v3, "media is null at selectMedia"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :goto_0
    return-void

    .line 505
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 506
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e029e

    new-array v6, v1, [Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 510
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->toggle(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 511
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v4

    if-ne v2, v4, :cond_2

    .line 512
    .local v1, "selected":Z
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v2, v4, v3, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 513
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 514
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 515
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_0

    .line 508
    .end local v1    # "selected":Z
    :cond_1
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v4

    const v5, 0x7f0e029d

    new-array v6, v1, [Ljava/lang/Object;

    move-object v2, v0

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v3

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 511
    goto :goto_2
.end method

.method private setHiddenAlbumAdapter()V
    .locals 6

    .prologue
    .line 466
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-nez v2, :cond_0

    .line 467
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 468
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSetPath:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 469
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    new-instance v2, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ZLcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    .line 473
    .end local v0    # "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    return-void
.end method

.method private showDeleteDialog()V
    .locals 3

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 307
    return-void
.end method

.method private unselectAll()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 530
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 531
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 532
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 533
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    .line 534
    return-void
.end method

.method private updateSelectAllActionBar()V
    .locals 4

    .prologue
    .line 610
    const/4 v0, 0x0

    .line 611
    .local v0, "selected":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getCount()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 612
    const/4 v0, 0x1

    .line 614
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v2, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 615
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    const/4 v3, 0x1

    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->isAvailableCount(IZ)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 616
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 617
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 618
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 424
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->exitSelectionMode(Z)V

    .line 428
    :goto_0
    return-void

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 461
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 462
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 463
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 171
    const-string v0, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaSetPath:Ljava/lang/String;

    .line 173
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f00bf

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 177
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 180
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 183
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirJump:Z

    if-eqz v0, :cond_1

    .line 184
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;-><init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 188
    :cond_1
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 389
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 378
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onStop()V

    .line 381
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->destoryAirMotionDetector()V

    .line 384
    :cond_1
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 414
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->exitSelectionMode(Z)V

    .line 419
    :goto_0
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onPause()V

    .line 360
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 364
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause()V

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getSplitScroll()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSplitScroll:F

    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getThumbScroll()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbScroll:F

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I

    .line 369
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->pause()V

    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "HIDDEN_ITEM_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 374
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 393
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 394
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mIsDeleteMode:Z

    if-eqz v4, :cond_2

    .line 395
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 396
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 397
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 398
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f027b

    if-ne v4, v5, :cond_1

    .line 399
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 400
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 401
    if-eqz v1, :cond_0

    .line 402
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 396
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 404
    :cond_0
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 406
    :cond_1
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 410
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mMediaFilterType:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 312
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->setHiddenAlbumAdapter()V

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$11;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->onResume()V

    .line 329
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mScrolledPosition:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSplitScroll:F

    iget v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mThumbScroll:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;-><init>(Landroid/content/Context;IIFF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpandable(Z)V

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v6, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v6, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const-string v1, "hiddenPhotoMode"

    iput-object v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSPKeyString:Ljava/lang/String;

    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v3, v2}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0e01d9

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 347
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 352
    :cond_0
    return-void

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 342
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v1, v2, v3}, Lcom/sec/samsung/gallery/view/hiddenview/HideActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 344
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->updateSelectAllActionBar()V

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_0
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 0
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 590
    packed-switch p1, :pswitch_data_0

    .line 596
    :goto_0
    return-void

    .line 593
    :pswitch_0
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->handleResultWallpaper(Landroid/content/Intent;I)V

    goto :goto_0

    .line 590
    :pswitch_data_0
    .packed-switch 0x403
        :pswitch_0
    .end packed-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$5;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V

    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$6;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mLaunchMode:Lcom/sec/samsung/gallery/core/LaunchModeType;

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$7;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;)V

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$8;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V

    .line 250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$9;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 264
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState$10;-><init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V

    .line 286
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 572
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 574
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;

    if-eqz v1, :cond_0

    .line 576
    if-eqz p1, :cond_1

    .line 577
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 578
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 586
    :cond_0
    :goto_0
    return-void

    .line 581
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 8
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 432
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 433
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 435
    .local v1, "eventType":I
    const-string v3, "HiddenPhotoViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Event update ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v1, v3, :cond_1

    .line 438
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->selectAll()V

    .line 457
    :cond_0
    :goto_0
    return-void

    .line 439
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v1, v3, :cond_2

    .line 440
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->unselectAll()V

    goto :goto_0

    .line 441
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v1, v3, :cond_3

    .line 442
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->enterSelectionMode(Z)V

    .line 443
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 444
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0

    .line 445
    :cond_3
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v1, v3, :cond_4

    .line 446
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mIsDeleteMode:Z

    .line 447
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->enterSelectionMode(Z)V

    .line 448
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 449
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto :goto_0

    .line 450
    :cond_4
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v1, v3, :cond_0

    .line 451
    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v2, v6

    .line 454
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenPhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "LAST_SHARE_APP"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
