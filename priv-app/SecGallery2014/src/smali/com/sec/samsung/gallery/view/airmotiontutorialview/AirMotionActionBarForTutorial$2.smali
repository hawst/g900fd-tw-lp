.class Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;
.super Ljava/lang/Object;
.source "AirMotionActionBarForTutorial.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->setTitle()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 39
    const-string v2, "ro.csc.country_code"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "JP"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 40
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->access$200(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00dd

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 45
    .local v1, "title":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->access$400(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    const v3, 0x7f0f0014

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 46
    .local v0, "t":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v2, 0x1

    const/high16 v3, 0x41980000    # 19.0f

    invoke-virtual {v0, v2, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 50
    :cond_0
    return-void

    .line 42
    .end local v0    # "t":Landroid/widget/TextView;
    .end local v1    # "title":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial$2;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;->access$300(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionActionBarForTutorial;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e00dc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "title":Ljava/lang/String;
    goto :goto_0
.end method
