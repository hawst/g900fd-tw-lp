.class Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;
.super Ljava/lang/Object;
.source "HiddenMediaAdapter.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetUpdateInfo"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;",
        ">;"
    }
.end annotation


# instance fields
.field private final mVersion:J

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;J)V
    .locals 0
    .param p2, "version"    # J

    .prologue
    .line 537
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 538
    iput-wide p2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->mVersion:J

    .line 539
    return-void
.end method


# virtual methods
.method public call()Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 542
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceVersion:J
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$000(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->mVersion:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move-object v0, v1

    .line 545
    :goto_0
    return-object v0

    .line 543
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;-><init>(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$1;)V

    .line 544
    .local v0, "info":Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;
    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->mVersion:J

    iput-wide v2, v0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->version:J

    goto :goto_0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$GetUpdateInfo;->call()Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    move-result-object v0

    return-object v0
.end method
