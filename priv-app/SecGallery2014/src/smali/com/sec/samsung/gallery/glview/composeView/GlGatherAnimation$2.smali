.class Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;
.super Ljava/lang/Object;
.source "GlGatherAnimation.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)V
    .locals 0

    .prologue
    .line 350
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 5
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 358
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->releaseDrag()V

    .line 359
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$202(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z

    .line 360
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    sget v2, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->DRAG_ANIM_NONE:I

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mCurrentAnim:I
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$002(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;I)I

    .line 361
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragAndDropInExpandSplitView:Z

    if-eqz v1, :cond_0

    .line 362
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$300(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    .line 363
    .local v0, "mPhotoView":Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->isOriginExpanded()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 364
    invoke-virtual {v0, v4, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setExpanded(ZZ)V

    .line 365
    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setOriginExpanded(Z)V

    .line 368
    .end local v0    # "mPhotoView":Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 370
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->mIsInverseDragAnimationRunning:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;->access$202(Lcom/sec/samsung/gallery/glview/composeView/GlGatherAnimation;Z)Z

    .line 354
    return-void
.end method
