.class Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;
.super Ljava/lang/Object;
.source "SLinkActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;I)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    iput p2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 136
    const/4 v0, 0x0

    .line 137
    .local v0, "numberOfSelectedItems":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->access$100(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 140
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-ltz v2, :cond_1

    .line 141
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 142
    .local v1, "res":Landroid/content/res/Resources;
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 145
    .end local v1    # "res":Landroid/content/res/Resources;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    iget v3, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->access$300(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;ILjava/lang/String;)V

    .line 147
    iget v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-lez v2, :cond_2

    .line 148
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0f02a2

    invoke-static {v2, v3, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 150
    :cond_2
    return-void
.end method
