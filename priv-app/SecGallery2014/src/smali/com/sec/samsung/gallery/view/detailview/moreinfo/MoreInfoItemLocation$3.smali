.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;
.super Ljava/lang/Object;
.source "MoreInfoItemLocation.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment$MapViewListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMapCreated()V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMapFragment:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoMapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$102(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;Lcom/google/android/gms/maps/GoogleMap;)Lcom/google/android/gms/maps/GoogleMap;

    .line 107
    const-string v0, "MoreInfoItemLocation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Map created : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$3;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->invalidate()V

    .line 111
    :cond_0
    return-void
.end method
