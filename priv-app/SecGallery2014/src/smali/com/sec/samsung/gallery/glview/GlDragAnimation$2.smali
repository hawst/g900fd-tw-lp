.class Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;
.super Ljava/lang/Object;
.source "GlDragAnimation.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startInverseDragAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->releaseDrag()V

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mIsInverseDragAnimationRunning:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->access$202(Lcom/sec/samsung/gallery/glview/GlDragAnimation;Z)Z

    .line 195
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 197
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;->this$0:Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mIsInverseDragAnimationRunning:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->access$202(Lcom/sec/samsung/gallery/glview/GlDragAnimation;Z)Z

    .line 189
    return-void
.end method
