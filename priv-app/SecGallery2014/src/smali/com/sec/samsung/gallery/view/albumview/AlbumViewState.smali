.class public Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$19;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionTask;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;
    }
.end annotation


# static fields
.field private static final COLCNT_LEVEL_DEFAULT:I = 0x1

.field private static final COLCNT_LEVEL_SELECT:I = 0x1

.field private static final DEBUG:Z = false

.field public static final KEY_MEDIA_PATH:Ljava/lang/String; = "media-path"

.field public static final KEY_SELECTED_CLUSTER_TYPE:Ljava/lang/String; = "selected-cluster"

.field public static final KEY_SET_SUBTITLE:Ljava/lang/String; = "set-subtitle"

.field public static final KEY_SET_TITLE:Ljava/lang/String; = "set-title"

.field public static final REQUEST_ADD_TAG:I = 0x905

.field public static final REQUEST_ASSIGN_NAME:I = 0x904

.field public static final REQUEST_CAMERA_LAUNCH_FOR_SINGLE_IMAGE_PICK:I = 0x902

.field public static final REQUEST_CAMERA_LAUNCH_FOR_SINGLE_VIDEO_PICK:I = 0x903

.field private static final REQUEST_HIDDEN_ALBUM_CREATION:I = 0x901

.field private static final TAG:Ljava/lang/String; = "AlbumViewState"


# instance fields
.field private isfromEditmode:Z

.field private mAdapterConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;

.field private mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

.field private mAlbumIndex:I

.field private mAlbumItemIndex:I

.field private mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mBackupMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mChangeViewMode:I

.field public mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

.field private mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

.field private mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurrentMediaSetIndex:I

.field private mCurrentTopSetPath:Ljava/lang/String;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mFirstMediaCheck:Z

.field private mFromGifMaker:Z

.field private mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

.field private mIsAlbumPick:Z

.field private mIsDeleteMode:Z

.field private mIsEasyMode:Z

.field private mIsHelpMode:Z

.field private mIsPickerMode:Z

.field private mIsReOrderState:Z

.field private mKeepThumbnailForPhotoView:Z

.field public mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

.field private mMenu:Landroid/view/Menu;

.field mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field private mNeedIdleProcess:Z

.field private mRefreshView:Z

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field protected mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShowContentToDisplay:Z

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mShrinkOption:I

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

.field private mUpdatePath:Z

.field private mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

.field private mViewMode:I

.field private tempTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 119
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 145
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    .line 147
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 148
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 149
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaSetIndex:I

    .line 154
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRefreshView:Z

    .line 161
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 162
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 165
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    invoke-direct {v0, p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->tempTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    .line 171
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z

    .line 172
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowContentToDisplay:Z

    .line 173
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    .line 174
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mKeepThumbnailForPhotoView:Z

    .line 175
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z

    .line 177
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 178
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    .line 180
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->isfromEditmode:Z

    .line 183
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z

    .line 185
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mChangeViewMode:I

    .line 186
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    .line 189
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShrinkOption:I

    .line 191
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdatePath:Z

    .line 192
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsDeleteMode:Z

    .line 194
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumIndex:I

    .line 195
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumItemIndex:I

    .line 197
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;

    const-string v1, "ALBUM_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 263
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$2;

    const-string v1, "ALBUM_VIEW_MEDIA_EJECT"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$2;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 289
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$3;

    const-string v1, "ALBUM_VIEW_EXIT_SELECTION"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$3;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 311
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$4;

    const-string v1, "SLIDESHOW_SETTINGS"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$4;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 368
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$5;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .line 2195
    return-void
.end method

.method private ResetReorderAlbums()V
    .locals 4

    .prologue
    .line 1965
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v0

    .line 1967
    .local v0, "manager":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "album_order_backup"

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1968
    .local v1, "originalOrder":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "album_order_backup"

    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->removeKey(Landroid/content/Context;Ljava/lang/String;)V

    .line 1969
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addAlbumDisplayInfo(Ljava/lang/String;)Z

    .line 1970
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$1802(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    return-object v0
.end method

.method static synthetic access$2102(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    return-object p1
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->tempTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z

    return v0
.end method

.method static synthetic access$2302(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z

    return p1
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateConfirmMenu()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startPhotoView(I)V

    return-void
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->latestSelectedAlbuminfo(I)V

    return-void
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addToSlideshowProxy()V

    return-void
.end method

.method static synthetic access$4002(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/android/gallery3d/data/MediaSet;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 119
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mBackupMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object p1
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterReorderAlbums(Z)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z

    return v0
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleReorderAlbums(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$5200(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;IILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 119
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startDetailView(IILcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$5400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startSlideShow()V

    return-void
.end method

.method static synthetic access$5800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$5900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lorg/puremvc/java/interfaces/INotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .param p1, "x1"    # Lorg/puremvc/java/interfaces/INotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 119
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6100(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 119
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->toggleSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    return-void
.end method

.method static synthetic access$6200(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 119
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    return-void
.end method

.method static synthetic access$6300(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 119
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->removeSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    return-void
.end method

.method static synthetic access$6400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6600(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    return v0
.end method

.method static synthetic access$6700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAllPostProcess()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->ResetReorderAlbums()V

    return-void
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitReorderAlbums()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private static addSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 2132
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2133
    invoke-static {p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2134
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2135
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2143
    :goto_0
    return-void

    .line 2137
    :cond_0
    const v0, 0x7f0e0107

    invoke-static {p0, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 2140
    :cond_1
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2141
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method

.method private addToSlideshowProxy()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 1503
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 1505
    .local v0, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1506
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->isfromEditmode:Z

    .line 1507
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$15;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$15;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    invoke-direct {v1, v2, v3, v5, v4}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZI)V

    new-array v2, v5, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1552
    :goto_0
    return-void

    .line 1529
    :cond_0
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->isfromEditmode:Z

    .line 1530
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$16;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$16;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    new-array v2, v5, [Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private cancelSwitchFilterTask()V
    .locals 2

    .prologue
    .line 1376
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    if-eqz v0, :cond_0

    .line 1377
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->cancel(Z)Z

    .line 1378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 1380
    :cond_0
    return-void
.end method

.method private static checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p0, "mediaObj"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 2151
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkMediaAvailability()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1555
    const/4 v3, 0x0

    .line 1557
    .local v3, "newEmptyViewMode":Z
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->isLoading()Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_1

    move v3, v4

    .line 1562
    :goto_0
    if-nez v3, :cond_0

    .line 1563
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 1566
    :cond_0
    if-eqz v3, :cond_4

    .line 1568
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1569
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v2

    .line 1570
    .local v2, "mimeType":Ljava/lang/String;
    const-string v4, "AlbumViewState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "MIMETYPE : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 1572
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V

    .line 1573
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startNoItemViewState(Ljava/lang/String;)V

    .line 1574
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMenu:Landroid/view/Menu;

    if-nez v4, :cond_2

    .line 1595
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "mimeType":Ljava/lang/String;
    :goto_1
    return-void

    :cond_1
    move v3, v5

    .line 1557
    goto :goto_0

    .line 1558
    :catch_0
    move-exception v0

    .line 1559
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_0

    .line 1576
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "mimeType":Ljava/lang/String;
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1594
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v2    # "mimeType":Ljava/lang/String;
    :cond_3
    :goto_2
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mFirstMediaCheck:Z

    goto :goto_1

    .line 1577
    :cond_4
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mFirstMediaCheck:Z

    if-eqz v6, :cond_7

    .line 1583
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v6, :cond_6

    .line 1584
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v6

    if-nez v6, :cond_5

    .line 1585
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v6, v7, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 1586
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshView(Z)V

    .line 1588
    :cond_6
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1589
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v6, :cond_3

    .line 1590
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    goto :goto_2

    .line 1592
    :cond_7
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto :goto_2
.end method

.method private checkReorderAlbums(II)Z
    .locals 1
    .param p1, "target"    # I
    .param p2, "source"    # I

    .prologue
    .line 1973
    if-ne p1, p2, :cond_0

    .line 1974
    const/4 v0, 0x0

    .line 1976
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 9
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 1852
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 1853
    .local v0, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v7, "nearby"

    invoke-virtual {v0, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 1854
    .local v3, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-nez v3, :cond_0

    .line 1855
    new-instance v7, Ljava/lang/NullPointerException;

    const-string v8, "downloadNearby() : NearbySource is null"

    invoke-direct {v7, v8}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v7

    .line 1856
    :cond_0
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v2

    .line 1857
    .local v2, "nearbyClient":Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    const-string v6, ""

    .line 1858
    .local v6, "serverName":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaSetIndex:I

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1859
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_1

    .line 1860
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v6

    .line 1863
    :cond_1
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/Object;

    move-object v5, v7

    check-cast v5, [Ljava/lang/Object;

    .line 1865
    .local v5, "params":[Ljava/lang/Object;
    const/4 v7, 0x0

    aget-object v4, v5, v7

    check-cast v4, Ljava/util/ArrayList;

    .line 1866
    .local v4, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v2, v6, v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->download(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1867
    return-void
.end method

.method private enterReorderAlbums(Z)V
    .locals 13
    .param p1, "isFirstTime"    # Z

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 1876
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z

    .line 1877
    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v8

    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 1878
    .local v7, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8, v7, v11, v11}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z

    .line 1879
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    const/4 v9, 0x3

    const/4 v10, 0x0

    invoke-virtual {v8, v9, v11, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setMode(IILjava/lang/Object;)V

    .line 1880
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v8, v11}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1881
    if-eqz p1, :cond_1

    .line 1882
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v8, :cond_0

    .line 1883
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v8, v12}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1884
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1885
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1886
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1891
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->initReorderAlbums()V

    .line 1929
    :goto_1
    return-void

    .line 1888
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1889
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10, v11}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 1893
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "album_order_backup"

    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1896
    .local v0, "backupOrder":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v3

    .line 1898
    .local v3, "count":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1899
    .local v2, "bucketIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v3, :cond_3

    .line 1900
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 1901
    .local v6, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v6, :cond_2

    .line 1899
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1902
    :cond_2
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    .line 1904
    .local v1, "bucketId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1907
    .end local v1    # "bucketId":I
    .end local v6    # "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    const-string v4, ""

    .line 1908
    .local v4, "currentOrder":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1909
    const/4 v5, 0x0

    :goto_4
    if-ge v5, v3, :cond_4

    .line 1910
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ";"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1909
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 1913
    :cond_4
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-eqz v8, :cond_5

    .line 1914
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v8, v12}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1915
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1916
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1917
    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto/16 :goto_1

    .line 1920
    :cond_5
    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 1921
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1922
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10, v11}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto/16 :goto_1

    .line 1924
    :cond_6
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1925
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v9, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v9, v10, v12}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto/16 :goto_1
.end method

.method private enterSelectionMode(Z)V
    .locals 5
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1166
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 1167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 1168
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsDeleteMode:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setDeleteSelectMode(Z)V

    .line 1169
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1170
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1171
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1172
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_0

    .line 1173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setMode(IILjava/lang/Object;)V

    .line 1174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshSelectionBarState(Z)V

    .line 1176
    :cond_0
    return-void
.end method

.method private exitReorderAlbums()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1932
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z

    .line 1933
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setMode(IILjava/lang/Object;)V

    .line 1934
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1935
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1936
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1937
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1938
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mBackupMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, v1, v3, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z

    .line 1939
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v1, "album_order_backup"

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->removeKey(Landroid/content/Context;Ljava/lang/String;)V

    .line 1940
    return-void
.end method

.method private exitSelectionMode()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1179
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 1180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v1, 0x7f0e00a4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->exitSelectionMode(I)V

    .line 1181
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 1182
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1183
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setDownloadMode(Z)V

    .line 1184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeFilterAlbums(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 1195
    :cond_0
    :goto_0
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsDeleteMode:Z

    .line 1196
    return-void

    .line 1186
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1187
    const-string v0, "AlbumViewState"

    const-string v1, "exitSelectionMode.setAction"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1188
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1189
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1190
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setMode(IILjava/lang/Object;)V

    .line 1192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshSelectionBarState(Z)V

    goto :goto_0
.end method

.method private finishCurrentViewState()V
    .locals 1

    .prologue
    .line 1737
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 1738
    return-void
.end method

.method private getDefaultPath()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 710
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 712
    .local v0, "intent":Landroid/content/Intent;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mData:Landroid/os/Bundle;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mData:Landroid/os/Bundle;

    const-string v6, "KEY_MEDIA_SET_PATH"

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    .line 713
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mFromGifMaker:Z

    if-nez v5, :cond_3

    .line 714
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mData:Landroid/os/Bundle;

    const-string v6, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 718
    .local v2, "newPath":Ljava/lang/String;
    :goto_0
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    if-eqz v5, :cond_4

    const-string v5, "combo"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, "cloud"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "sns"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string v5, "picasa"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    const-string/jumbo v5, "tCloud"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_0
    :goto_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowContentToDisplay:Z

    .line 734
    :goto_2
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 735
    const-string v3, "/combo/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 736
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/combo/{"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, "}"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 740
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v5, "mtpMode"

    invoke-static {v3, v5, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 741
    .local v1, "isMtpMode":Z
    if-eqz v1, :cond_2

    .line 742
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getAddMtpTopPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 744
    :cond_2
    return-object v2

    .line 716
    .end local v1    # "isMtpMode":Z
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "newPath":Ljava/lang/String;
    goto/16 :goto_0

    :cond_4
    move v3, v4

    .line 718
    goto :goto_1

    .line 721
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_5
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    if-eqz v5, :cond_6

    .line 722
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowContentToDisplay:Z

    .line 723
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v6

    invoke-static {v3, v5, v6}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "newPath":Ljava/lang/String;
    goto :goto_2

    .line 725
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_6
    if-eqz v0, :cond_7

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mData:Landroid/os/Bundle;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mData:Landroid/os/Bundle;

    const-string v5, "KEY_VIEW_FACE"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 726
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 727
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "newPath":Ljava/lang/String;
    goto/16 :goto_2

    .line 729
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_7
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "newPath":Ljava/lang/String;
    goto/16 :goto_2
.end method

.method private getFileFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1363
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRefreshState(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 1471
    const/4 v0, 0x0

    .line 1473
    .local v0, "refreshView":Z
    if-eqz p1, :cond_0

    .line 1474
    const-string v1, "KEY_VIEW_REDRAW"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 1476
    :cond_0
    return v0
.end method

.method private getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 5
    .param p1, "viewByType"    # I

    .prologue
    .line 1427
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 1428
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    .line 1429
    .local v1, "tabType":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v4

    invoke-static {v3, p1, v4}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v2

    .line 1430
    .local v2, "topPath":Ljava/lang/String;
    invoke-static {v1}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 1431
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    return-object v3
.end method

.method private getTopSetPathByFilter()Ljava/lang/String;
    .locals 9

    .prologue
    .line 1439
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 1440
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 1441
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v8, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 1466
    :goto_0
    return-object v1

    .line 1442
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v7, v8, :cond_1

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v7, v8, :cond_3

    .line 1443
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    .line 1444
    .local v3, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1445
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const-string v0, ""

    .line 1446
    .local v0, "basePath":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 1447
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1449
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v7

    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    .local v5, "newPath":Ljava/lang/String;
    move-object v1, v5

    .line 1451
    goto :goto_0

    .line 1454
    .end local v0    # "basePath":Ljava/lang/String;
    .end local v3    # "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "newPath":Ljava/lang/String;
    :cond_3
    const/4 v1, 0x0

    .line 1456
    .local v1, "clusterPath":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    .line 1458
    .local v6, "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v8

    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 1459
    .restart local v0    # "basePath":Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v2

    .line 1460
    .local v2, "clusterType":I
    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v6, v7, :cond_4

    .line 1461
    sget-object v7, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1465
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    goto :goto_0

    .line 1463
    :cond_4
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private handleHiddenAlbumsLaunch()V
    .locals 4

    .prologue
    .line 1435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/16 v2, 0x901

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->startStateForResult(Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 1436
    return-void
.end method

.method private handleNewAlbumLaunch()V
    .locals 2

    .prologue
    .line 1367
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->hasLocalMediaSet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1368
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->startPhotoView(I)V

    .line 1369
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 1373
    :goto_0
    return-void

    .line 1371
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e02eb

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method private handleReorderAlbums(II)Z
    .locals 11
    .param p1, "target"    # I
    .param p2, "source"    # I

    .prologue
    const/4 v9, 0x0

    .line 1980
    if-ne p1, p2, :cond_0

    move v8, v9

    .line 2011
    :goto_0
    return v8

    .line 1984
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-static {v8}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v5

    .line 1985
    .local v5, "manager":Lcom/sec/samsung/gallery/util/LocalDatabaseManager;
    const-string v7, ""

    .line 1986
    .local v7, "order":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v3

    .line 1987
    .local v3, "count":I
    if-ltz p1, :cond_1

    if-gt p1, v3, :cond_1

    if-ltz p2, :cond_1

    if-lt p2, v3, :cond_2

    :cond_1
    move v8, v9

    .line 1988
    goto :goto_0

    .line 1990
    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1991
    .local v2, "bucketIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v3, :cond_4

    .line 1992
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    .line 1993
    .local v6, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v6, :cond_3

    .line 1994
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v1

    .line 1995
    .local v1, "bucketId":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1991
    .end local v1    # "bucketId":I
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 1998
    .end local v6    # "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v2, p1, v8}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 1999
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 2000
    const/4 v4, 0x0

    :goto_2
    if-ge v4, v3, :cond_5

    .line 2001
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ";"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2000
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2004
    :cond_5
    invoke-virtual {v5, v7}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->addAlbumDisplayInfo(Ljava/lang/String;)Z

    .line 2006
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v10, "album_order_backup"

    invoke-static {v8, v10}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadStringKey(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2008
    .local v0, "backupOrder":Ljava/lang/String;
    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_6

    move v8, v9

    .line 2009
    goto :goto_0

    .line 2011
    :cond_6
    const/4 v8, 0x1

    goto :goto_0
.end method

.method private handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V
    .locals 8
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "request"    # I

    .prologue
    .line 1797
    if-nez p1, :cond_1

    .line 1823
    :cond_0
    :goto_0
    return-void

    .line 1801
    :cond_1
    const/16 v5, 0x902

    if-ne p2, v5, :cond_2

    .line 1802
    const-string/jumbo v5, "uri-data"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1803
    .local v0, "fileString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1805
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1810
    .end local v0    # "fileString":Ljava/lang/String;
    .local v4, "uri":Landroid/net/Uri;
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 1811
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v3, :cond_3

    .line 1812
    const-string v5, "AlbumViewState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cannot find file : path is null : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1807
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .restart local v4    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 1816
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1817
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 1819
    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v6, v2, v5

    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 1822
    .local v2, "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "PICKER_ITEM_SELECTED"

    invoke-virtual {v5, v6, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleResultWallpaper(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "result"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 1783
    if-eq p2, v6, :cond_0

    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v0, v4

    .line 1785
    .local v0, "finishActivity":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 1786
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_2

    .end local p1    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v3, p2, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 1787
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 1794
    :goto_2
    return-void

    .end local v0    # "finishActivity":Z
    .restart local p1    # "data":Landroid/content/Intent;
    :cond_1
    move v0, v3

    .line 1783
    goto :goto_0

    .restart local v0    # "finishActivity":Z
    :cond_2
    move-object p1, v2

    .line 1786
    goto :goto_1

    .line 1789
    :cond_3
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v3

    aput-object v2, v1, v4

    .line 1792
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private initReorderAlbums()V
    .locals 8

    .prologue
    .line 1944
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v2

    .line 1946
    .local v2, "count":I
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1947
    .local v1, "bucketIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 1948
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v6, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1949
    .local v4, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v4, :cond_0

    .line 1947
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1950
    :cond_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    .line 1952
    .local v0, "bucketId":I
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1955
    .end local v0    # "bucketId":I
    .end local v4    # "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    const-string v5, ""

    .line 1956
    .local v5, "originalOrder":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1957
    const/4 v3, 0x0

    :goto_2
    if-ge v3, v2, :cond_2

    .line 1958
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ";"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1957
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1960
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "album_order_backup"

    invoke-static {v6, v7, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 1962
    return-void
.end method

.method private isLaunchedFromMainMenu()Z
    .locals 2

    .prologue
    .line 1733
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private latestSelectedAlbuminfo(I)V
    .locals 9
    .param p1, "position"    # I

    .prologue
    .line 2015
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v5, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 2016
    .local v3, "mediaset":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v3, :cond_1

    .line 2031
    :cond_0
    :goto_0
    return-void

    .line 2018
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getLatestAlbumInfo()I

    move-result v4

    .line 2019
    .local v4, "saveInfo":I
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v0

    .line 2021
    .local v0, "bucketId":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    const/4 v8, 0x0

    invoke-virtual {v5, p1, v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 2022
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    .line 2026
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v1

    .line 2027
    .local v1, "itemId":I
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    .line 2028
    .local v6, "takenTime":J
    if-ne v0, v4, :cond_0

    .line 2029
    invoke-direct {p0, v0, v1, v6, v7}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setSelectedAlbum(IIJ)V

    goto :goto_0
.end method

.method private static removeSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 2146
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2147
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2148
    return-void
.end method

.method private restorePreviousState()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 1051
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_2

    .line 1052
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1053
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setSlideShowMode(Z)V

    .line 1055
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_1

    .line 1059
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1060
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 1061
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 1062
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v1, 0x7f0e00a4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 1063
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1065
    const-string v0, "AlbumViewState"

    const-string v1, "restorePreviousState.setAction"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1066
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1068
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1078
    :cond_1
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 1081
    :cond_2
    return-void

    .line 1070
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1071
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1072
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1073
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    goto :goto_0
.end method

.method private runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "type"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    .prologue
    .line 2053
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;

    invoke-direct {v1, p0, p2, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$18;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;Lcom/sec/android/gallery3d/data/MediaSet;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2078
    return-void
.end method

.method private selectAll()V
    .locals 3

    .prologue
    .line 1199
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1200
    .local v0, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v1

    .line 1202
    .local v1, "totalCount":I
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v1, v2, :cond_0

    .line 1203
    sget-object v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;->ADD_TOP_SET:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;)V

    .line 1208
    :goto_0
    return-void

    .line 1205
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v2, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1206
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAllPostProcess()V

    goto :goto_0
.end method

.method private selectAllPostProcess()V
    .locals 1

    .prologue
    .line 1211
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    .line 1212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshCheckState()V

    .line 1213
    return-void
.end method

.method private selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1218
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 1219
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 1220
    invoke-virtual {p2, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1221
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_0

    .line 1219
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1224
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1225
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 1227
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return-void
.end method

.method private setAlbumEventHandler()V
    .locals 3

    .prologue
    .line 1606
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    .line 1607
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 1608
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 1611
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 1615
    :goto_0
    return-void

    .line 1613
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private setDisplaySearchIcon(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 1598
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 1599
    .local v0, "actionbar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    if-eqz v1, :cond_0

    .line 1600
    check-cast v0, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    .end local v0    # "actionbar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;->setNoItemMode(Z)V

    .line 1602
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1603
    return-void

    .line 1600
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setSelectedAlbum(IIJ)V
    .locals 3
    .param p1, "bucketId"    # I
    .param p2, "itemId"    # I
    .param p3, "takenTime"    # J

    .prologue
    .line 2040
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2041
    .local v0, "albuminfo":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "user_selected_album"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    return-void
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1722
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 1724
    .local v0, "isSelectAll":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 1725
    return-void

    .line 1722
    .end local v0    # "isSelectAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startDetailView(IILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 5
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1343
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1344
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    if-gtz v3, :cond_1

    .line 1360
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return-void

    .line 1347
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const/4 v2, 0x0

    .line 1348
    .local v2, "path":Ljava/lang/String;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1349
    .local v0, "bundle":Landroid/os/Bundle;
    instance-of v3, p3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v3, :cond_2

    .line 1350
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/LocalImage;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1354
    :goto_1
    const-string v3, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    const-string v3, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1357
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1358
    const-string v3, "add_camera_shortcut"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1359
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1352
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private startDetailView(ILandroid/net/Uri;)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v10, 0x0

    .line 1313
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 1314
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v8

    if-gtz v8, :cond_1

    .line 1340
    :cond_0
    :goto_0
    return-void

    .line 1316
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v9, 0x0

    invoke-virtual {v8, p2, v9}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 1317
    .local v6, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1318
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v4, :cond_0

    .line 1320
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    .line 1321
    .local v7, "srcPath":Ljava/lang/String;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 1322
    .local v3, "dstPath":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-direct {p0, v9}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getFileFullName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1323
    .local v2, "dstItemPath":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 1324
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1325
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 1326
    const-string v8, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1327
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1328
    const-string v8, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1329
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1339
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v8

    const-class v9, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v8, v9, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 1330
    :cond_2
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v1, v4, v3, v8}, Lcom/sec/android/gallery3d/data/DataManager;->copyItem(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 1331
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1332
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 1334
    :cond_3
    const-string v8, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1335
    const-string v8, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1336
    const-string v8, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1337
    const-string v8, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v8, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method private startDetailView(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1293
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 1294
    .local v2, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v2, :cond_0

    .line 1295
    const-string v3, "AlbumViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot start detailview : path is null : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1310
    :goto_0
    return-void

    .line 1299
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1300
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_1

    .line 1301
    const-string v3, "AlbumViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cannot start detailview : mediaitem is null : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1304
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1306
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1308
    const-string v3, "KEY_ITEM_POSITION"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1309
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startNoItemViewState(Ljava/lang/String;)V
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 1826
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 1828
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1829
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    const-string v1, "KEY_NOITEMSVIEW_MIME_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1831
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z

    if-eqz v1, :cond_0

    .line 1832
    const-string v1, "album-pick"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1834
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$17;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$17;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1842
    return-void
.end method

.method private startPhotoView(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v5, 0x1

    .line 1273
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 1274
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    if-gtz v3, :cond_1

    .line 1290
    :cond_0
    :goto_0
    return-void

    .line 1279
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1280
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "KEY_MEDIA_SET_PATH"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1281
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1282
    const-string v3, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1283
    const-string v3, "KEY_FROM_ALBUM_VIEW_STATE"

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1285
    new-instance v1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    invoke-direct {v1}, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;-><init>()V

    .line 1286
    .local v1, "interInfo":Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V

    .line 1287
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/gallery3d/app/StateManager;->pushPreviousInfo(ILjava/lang/Object;)V

    .line 1288
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mKeepThumbnailForPhotoView:Z

    .line 1289
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startSlideShow()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 1496
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->isfromEditmode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1499
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1500
    return-void
.end method

.method private static toggleSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "selectionManager"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 2124
    invoke-virtual {p1, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2125
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->removeSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 2129
    :goto_0
    return-void

    .line 2127
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->addSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0
.end method

.method private unselectAll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1230
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 1231
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1232
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1233
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshCheckState()V

    .line 1234
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1235
    return-void
.end method

.method private updateConfirmMenu()V
    .locals 5

    .prologue
    .line 1629
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 1630
    .local v2, "selected":I
    const/4 v1, 0x0

    .line 1632
    .local v1, "name":Ljava/lang/String;
    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 1633
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCurrentIndex()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 1634
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v3, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v3, :cond_1

    move-object v3, v0

    .line 1635
    check-cast v3, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getMediaSetType()I

    move-result v3

    if-nez v3, :cond_1

    .line 1636
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getName()Ljava/lang/String;

    move-result-object v1

    .line 1637
    if-eqz v1, :cond_0

    .line 1638
    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 1640
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateConfirm(Ljava/lang/String;)V

    .line 1644
    :cond_1
    return-void
.end method

.method private updateCountOnActionBar()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1265
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1266
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1267
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    .line 1269
    .local v0, "isAllSelected":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v3, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v2, v3, v1, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1270
    return-void

    .end local v0    # "isAllSelected":Z
    :cond_0
    move v0, v1

    .line 1267
    goto :goto_0
.end method

.method private updateLastSharedIconAndTitle()V
    .locals 1

    .prologue
    .line 1869
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppExisted(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1870
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1872
    :cond_0
    return-void
.end method

.method private updateNewAlbumMenuOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1022
    const v1, 0x7f0f028d

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1023
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/view/MenuItem;->isVisible()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1030
    :cond_0
    :goto_0
    return-void

    .line 1027
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->hasLocalMediaSet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1028
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private updateReOrderMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1008
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1018
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbumsSlectionMode:Z

    if-nez v2, :cond_0

    .line 1012
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v0

    .line 1013
    .local v0, "count":I
    const/4 v1, 0x0

    .line 1014
    .local v1, "isReorderSupported":Z
    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    .line 1015
    const/4 v1, 0x1

    .line 1016
    :cond_2
    const v2, 0x7f0f0292

    invoke-static {p1, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method private updateShowContentToDisplayMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 998
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 1000
    :cond_0
    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1001
    const v1, 0x7f0f0289

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowContentToDisplay:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v1, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1005
    :cond_1
    return-void

    .line 1001
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getContentsForFaceTag()Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1619
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1620
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1621
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1622
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1625
    :cond_0
    return-object v0
.end method

.method public getLatestAlbumInfo()I
    .locals 3

    .prologue
    .line 2034
    const/4 v0, 0x0

    .line 2035
    .local v0, "id":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "latest_update_album"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 2036
    return v0
.end method

.method public getMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V
    .locals 1
    .param p1, "interInfo"    # Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .prologue
    .line 2045
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCurrentInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V

    .line 2046
    return-void
.end method

.method protected handleFilter()V
    .locals 5

    .prologue
    .line 1383
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->cancelSwitchFilterTask()V

    .line 1384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v0

    .line 1385
    .local v0, "viewByType":I
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$14;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$14;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 1423
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v3, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1424
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 1085
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    .line 1086
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->onBackPressed()V

    .line 1100
    :goto_0
    return-void

    .line 1087
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1088
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1089
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->restorePreviousState()V

    goto :goto_0

    .line 1091
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1092
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->getMode()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 1093
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->ResetReorderAlbums()V

    .line 1094
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitReorderAlbums()V

    goto :goto_0

    .line 1095
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1096
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->restorePreviousState()V

    goto :goto_0

    .line 1098
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->finishCurrentViewState()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 1648
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1649
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0288

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 1650
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1651
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_0

    .line 1652
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 1653
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1655
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    .line 1656
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 1658
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v0, :cond_2

    .line 1659
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->notifyLayoutChanged()V

    .line 1661
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 413
    const-string v3, "Gallery_Performance"

    const-string v6, "AlbumViewState onCreate Start"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    const-string v3, "AlbumViewState"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate  = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v3, :cond_0

    .line 417
    const-string v3, "AlbumViewState"

    const-string v4, "mActivity is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    :goto_0
    return-void

    .line 420
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hidePreDisplayScreen()V

    .line 421
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 422
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 423
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 424
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 425
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGifMaker(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mFromGifMaker:Z

    .line 427
    new-instance v3, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v6, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 428
    new-instance v3, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 429
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getRefreshState(Landroid/os/Bundle;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRefreshView:Z

    .line 430
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v6, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 431
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setAlbumEventHandler()V

    .line 432
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 433
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0f00bf

    invoke-virtual {v3, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 434
    iput-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .line 435
    iput-object v8, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 436
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_1
    move v3, v5

    :goto_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    .line 437
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 438
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 439
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v6, 0x7f0e00a4

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 440
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 441
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 442
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 443
    .local v0, "intent":Landroid/content/Intent;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v6, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v6, :cond_3

    .line 445
    :cond_2
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    .line 448
    :cond_3
    if-eqz p1, :cond_4

    .line 449
    const-string v3, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaSetIndex:I

    .line 452
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getDefaultPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 453
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdatePath:Z

    .line 458
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-nez v3, :cond_8

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 461
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_2
    new-instance v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;

    .line 462
    new-instance v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    .line 463
    new-instance v3, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$DataLoaderConfig;

    invoke-direct {v3, v6, v1, v7, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    .line 464
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 465
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setEasyMode(Z)V

    .line 467
    if-eqz p1, :cond_5

    .line 468
    const-string v3, "album-pick"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsAlbumPick:Z

    .line 471
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 472
    iput v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    .line 477
    :goto_3
    const-string v3, "help_mode"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    .line 478
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    if-eqz v3, :cond_6

    .line 479
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "helpPickerViewPreference"

    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v2

    .line 481
    .local v2, "viewMode":I
    const/4 v3, 0x2

    if-eq v2, v3, :cond_a

    .line 482
    new-instance v3, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;-><init>(Landroid/content/Context;I)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 483
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->show(Z)V

    .line 491
    .end local v2    # "viewMode":I
    :cond_6
    :goto_4
    const-string v3, "Gallery_Performance"

    const-string v4, "AlbumViewState onCreate End"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_7
    move v3, v4

    .line 436
    goto/16 :goto_1

    .line 458
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    goto :goto_2

    .line 474
    .restart local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_9
    iput v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    goto :goto_3

    .line 486
    .restart local v2    # "viewMode":I
    :cond_a
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v4, "helpPickerViewPreference"

    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 968
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMenu:Landroid/view/Menu;

    .line 969
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 970
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 948
    const-string v0, "Gallery_Performance"

    const-string v1, "AlbumViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 949
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v0, :cond_0

    .line 950
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->onStop()V

    .line 957
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    .line 962
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "ALBUM_VIEW_EXIT_SELECTION"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 963
    const-string v0, "Gallery_Performance"

    const-string v1, "AlbumViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    return-void

    .line 960
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    goto :goto_0
.end method

.method public onDirty()V
    .locals 2

    .prologue
    .line 1665
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-nez v1, :cond_0

    .line 1679
    :goto_0
    return-void

    .line 1668
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1669
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 1670
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isFaceCluster()Z

    move-result v1

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 1671
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 1674
    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    if-eqz v1, :cond_2

    .line 1675
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 1677
    :cond_2
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public onMWLayoutChanged()V
    .locals 2

    .prologue
    .line 1729
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    .line 1730
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 1035
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1048
    :cond_0
    :goto_0
    return-void

    .line 1038
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 1039
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1040
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onBackPressed()V

    goto :goto_0

    .line 1042
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1044
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onBackPressed()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v4, -0x1

    .line 909
    const-string v0, "Gallery_Performance"

    const-string v3, "AlbumViewState onPause Start"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 911
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 913
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v0, :cond_0

    .line 914
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mKeepThumbnailForPhotoView:Z

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->onPause(Z)V

    .line 920
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 921
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_1

    .line 922
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->saveCurrentScrollInfo()V

    .line 923
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->pause()V

    .line 927
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 928
    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    .line 932
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "ALBUM_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 933
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "ALBUM_VIEW_MEDIA_EJECT"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 934
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    if-eqz v0, :cond_2

    .line 935
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 936
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setGenericMotionTitleFocus(I)V

    .line 938
    :cond_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v0, :cond_3

    .line 939
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 942
    :cond_3
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 943
    const-string v0, "Gallery_Performance"

    const-string v1, "AlbumViewState onPause End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 944
    return-void

    :cond_4
    move v0, v2

    .line 914
    goto :goto_0

    .line 930
    :cond_5
    iput v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mViewMode:I

    goto :goto_1
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 974
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 977
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v4, :cond_0

    .line 978
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateReOrderMenu(Landroid/view/Menu;)V

    .line 980
    :cond_0
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsDeleteMode:Z

    if-eqz v4, :cond_3

    .line 981
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 982
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_3

    .line 983
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 984
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v7, 0x7f0f027b

    if-ne v4, v7, :cond_2

    .line 985
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 986
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 987
    if-nez v1, :cond_1

    move v4, v5

    :goto_1
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 982
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v4, v6

    .line 987
    goto :goto_1

    .line 989
    :cond_2
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 993
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateNewAlbumMenuOption(Landroid/view/Menu;)V

    .line 994
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 995
    return-void
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 1742
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_0

    .line 1743
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->refreshView(Z)V

    .line 1744
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 15

    .prologue
    const/4 v2, -0x1

    const/4 v14, 0x0

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 750
    const/4 v6, -0x1

    .line 753
    .local v6, "initialCode":I
    const-string v0, "Gallery_Performance"

    const-string v1, "AlbumViewState onResume Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 754
    const-string v0, "AlbumViewState"

    const-string v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    .line 756
    iput-boolean v13, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mFirstMediaCheck:Z

    .line 757
    iput-boolean v13, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mNeedIdleProcess:Z

    .line 759
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdatePath:Z

    if-eqz v0, :cond_e

    .line 760
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getDefaultPath()Ljava/lang/String;

    move-result-object v10

    .line 761
    .local v10, "newPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 762
    :cond_0
    iput-object v10, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 763
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    .line 765
    .local v9, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    .line 771
    .end local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v10    # "newPath":Ljava/lang/String;
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_2
    move v0, v13

    :goto_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    .line 772
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setEasyMode(Z)V

    .line 773
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isLocaleRTL()Z

    move-result v7

    .line 774
    .local v7, "isRTL":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mIsRTL:Z

    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setRTL(Z)V

    .line 776
    const/4 v8, 0x1

    .line 777
    .local v8, "level":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->setInitalLevel(ZI)V
    invoke-static {v0, v1, v8}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->access$5300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;ZI)V

    .line 779
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerAlbum;->getThumbSizeType(I)I

    move-result v11

    .line 780
    .local v11, "type":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setInitThumbType(I)V

    .line 782
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 783
    const/4 v4, 0x1

    .line 784
    .local v4, "option":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_3

    .line 785
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->tempTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    monitor-enter v1

    .line 786
    :try_start_0
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    const/4 v3, 0x0

    invoke-direct {v0, p0, v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    .line 787
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 788
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 794
    :cond_3
    :goto_3
    or-int/lit8 v4, v4, 0x40

    .line 795
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_11

    .line 796
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v6

    .line 797
    if-ne v6, v2, :cond_4

    .line 798
    const-string v0, "AlbumViewState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialCode = -1!!, re-calculate initialCode with albumIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", itemIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumItemIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 799
    iget v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumIndex:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumItemIndex:I

    or-int v6, v0, v1

    .line 801
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShrinkOption:I

    or-int/2addr v4, v0

    .line 805
    :goto_4
    if-ltz v6, :cond_5

    .line 806
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setFirstIndex(I)V

    .line 809
    :cond_5
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsEasyMode:Z

    if-eqz v0, :cond_6

    .line 810
    or-int/lit16 v4, v4, 0x100

    .line 813
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    const/16 v1, 0x15

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setFirstLoadingCount(I)V

    .line 814
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .line 815
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 817
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 819
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->onResume()V

    .line 821
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "SLIDESHOW_SETTINGS"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 822
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 824
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 825
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setSlideShowMode(Z)V

    .line 828
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 829
    iput v13, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mChangeViewMode:I

    .line 838
    :goto_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsPickerMode:Z

    if-nez v0, :cond_13

    .line 839
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v14}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 840
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v0, v13, v12, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setMode(IILjava/lang/Object;)V

    .line 847
    :goto_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_8

    .line 850
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_17

    .line 851
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_15

    .line 852
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 853
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 886
    :cond_8
    :goto_7
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsHelpMode:Z

    if-eqz v0, :cond_a

    .line 887
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    if-eqz v0, :cond_9

    .line 888
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->hide()V

    .line 889
    iput-object v14, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 891
    :cond_9
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, v13}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mHelpView:Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    .line 893
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_b

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_c

    .line 896
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mAlbumEventHandle:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->initializeView()V

    .line 900
    :cond_c
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onDirty()V

    .line 901
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 902
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mKeepThumbnailForPhotoView:Z

    .line 903
    const-string v0, "Gallery_Performance"

    const-string v1, "AlbumViewState onResume End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 904
    return-void

    .line 763
    .end local v4    # "option":I
    .end local v7    # "isRTL":Z
    .end local v8    # "level":I
    .end local v11    # "type":I
    .restart local v10    # "newPath":Ljava/lang/String;
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    goto/16 :goto_0

    .line 768
    .end local v10    # "newPath":Ljava/lang/String;
    :cond_e
    iput-boolean v13, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mUpdatePath:Z

    goto/16 :goto_1

    :cond_f
    move v0, v12

    .line 771
    goto/16 :goto_2

    .line 788
    .restart local v4    # "option":I
    .restart local v7    # "isRTL":Z
    .restart local v8    # "level":I
    .restart local v11    # "type":I
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 792
    .end local v4    # "option":I
    :cond_10
    const/4 v4, 0x0

    .restart local v4    # "option":I
    goto/16 :goto_3

    .line 803
    :cond_11
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;

    iget v6, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$ComposeViewConfig;->mPrevCenterObject:I

    goto/16 :goto_4

    .line 831
    :cond_12
    iput v12, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mChangeViewMode:I

    goto/16 :goto_5

    .line 841
    :cond_13
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_14

    .line 842
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto/16 :goto_6

    .line 844
    :cond_14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v1, 0x7f0e00a4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(I)V

    goto/16 :goto_6

    .line 854
    :cond_15
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsReOrderState:Z

    if-eqz v0, :cond_16

    .line 855
    invoke-direct {p0, v12}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterReorderAlbums(Z)V

    goto/16 :goto_7

    .line 857
    :cond_16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 858
    const-string v0, "AlbumViewState"

    const-string v1, "onResume.setAction"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 860
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v13}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto/16 :goto_7

    .line 863
    :cond_17
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 864
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_18

    .line 865
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 866
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto/16 :goto_7

    .line 868
    :cond_18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 869
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 870
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    goto/16 :goto_7

    .line 873
    :cond_19
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 874
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 875
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForDownload;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForDownload;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 876
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 877
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 878
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto/16 :goto_7
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1683
    sparse-switch p1, :sswitch_data_0

    .line 1719
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 1690
    :sswitch_1
    invoke-direct {p0, p3, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1694
    :sswitch_2
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleResultWallpaper(Landroid/content/Intent;I)V

    goto :goto_0

    .line 1697
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 1698
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p2, :cond_1

    .line 1699
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v2, v5

    aput-object v0, v2, v4

    sget-object v3, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->ASSIGN_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    aput-object v3, v2, v6

    aput-object p3, v2, v7

    .line 1702
    .local v2, "sendParams":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "START_FACE_TAG"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1704
    .end local v2    # "sendParams":[Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "EXIT_SELECTION_MODE"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 1707
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_4
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    .line 1708
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v4, 0x905

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    .line 1709
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1713
    :sswitch_5
    new-array v1, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v4

    aput-object p3, v1, v6

    .line 1716
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 1683
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0x403 -> :sswitch_2
        0x901 -> :sswitch_0
        0x902 -> :sswitch_1
        0x903 -> :sswitch_1
        0x904 -> :sswitch_3
        0x905 -> :sswitch_4
    .end sparse-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 4
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    .line 497
    .local v0, "glComposeView":Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    const-string v1, "AlbumViewState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialzieView = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 500
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$6;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V

    .line 545
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$7;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V

    .line 584
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V

    .line 603
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$9;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 624
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$10;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;)V

    .line 647
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$11;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnHoverListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;)V

    .line 661
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$12;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V

    .line 686
    new-instance v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$13;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnExtendListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;)V

    .line 693
    return-void
.end method

.method protected selectAlbum(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1238
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1239
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_1

    .line 1240
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1241
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    const v2, 0x7f0e029e

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1242
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1243
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x4e20

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1250
    :cond_0
    :goto_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v1

    sget v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMaxCountForSelectionLoadingTask:I

    if-lt v1, v2, :cond_4

    .line 1251
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1252
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->toggleSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1253
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "REFRESH_SELECTION"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 1262
    :cond_1
    :goto_1
    return-void

    .line 1245
    :cond_2
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    const v2, 0x7f0e029d

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1246
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1247
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v2, 0x4e2a

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    goto :goto_0

    .line 1255
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;->ADD:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$SelectionType;)V

    goto :goto_1

    .line 1258
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->toggleSelectProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1259
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "REFRESH_SELECTION"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public setAlbumFocus(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 697
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->setGenericMotionFocus(I)V

    .line 702
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 9
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 1104
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 1105
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    .line 1107
    .local v1, "eventType":I
    const-string v3, "AlbumViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Event update ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1109
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v1, v3, :cond_1

    .line 1110
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mIsDeleteMode:Z

    .line 1111
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterSelectionMode(Z)V

    .line 1112
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    .line 1163
    :cond_0
    :goto_0
    return-void

    .line 1113
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    if-ne v1, v3, :cond_2

    .line 1114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->showDeleteDialog()V

    goto :goto_0

    .line 1115
    :cond_2
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v1, v3, :cond_3

    .line 1116
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->selectAll()V

    goto :goto_0

    .line 1117
    :cond_3
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v1, v3, :cond_4

    .line 1118
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->unselectAll()V

    goto :goto_0

    .line 1119
    :cond_4
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    if-ne v1, v3, :cond_5

    .line 1120
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1121
    :cond_5
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    if-ne v1, v3, :cond_9

    .line 1122
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1123
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e0066

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1126
    :cond_6
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 1127
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1128
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1129
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1130
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1131
    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v4, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 1132
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1137
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;->setSlideShowMode(Z)V

    goto :goto_0

    .line 1134
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 1138
    :cond_9
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REORDER_ALBUMS:I

    if-ne v1, v3, :cond_a

    .line 1139
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mMediaAlbumSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeAlbumSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mBackupMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1141
    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterReorderAlbums(Z)V

    goto/16 :goto_0

    .line 1142
    :cond_a
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NEW_ALBUM:I

    if-ne v1, v3, :cond_b

    .line 1143
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleNewAlbumLaunch()V

    goto/16 :goto_0

    .line 1144
    :cond_b
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_CAMERA_MODE:I

    if-ne v1, v3, :cond_c

    .line 1145
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "START_CAMERA"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1146
    :cond_c
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v1, v3, :cond_d

    .line 1147
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->enterSelectionMode(Z)V

    .line 1148
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->updateCountOnActionBar()V

    goto/16 :goto_0

    .line 1149
    :cond_d
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    if-ne v1, v3, :cond_e

    .line 1150
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleHiddenAlbumsLaunch()V

    goto/16 :goto_0

    .line 1151
    :cond_e
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    if-ne v1, v3, :cond_f

    .line 1152
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1153
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRefreshView:Z

    goto/16 :goto_0

    .line 1154
    :cond_f
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_GALLERY_SEARCH_MODE:I

    if-ne v1, v3, :cond_10

    .line 1155
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v3, v4, v8}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1156
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mRefreshView:Z

    goto/16 :goto_0

    .line 1157
    :cond_10
    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v1, v3, :cond_0

    .line 1158
    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v2, v7

    .line 1161
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "LAST_SHARE_APP"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
