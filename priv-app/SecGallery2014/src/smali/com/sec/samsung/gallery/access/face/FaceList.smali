.class public Lcom/sec/samsung/gallery/access/face/FaceList;
.super Lcom/sec/samsung/gallery/access/face/List;
.source "FaceList.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    }
.end annotation


# static fields
.field private static mFaceDir:Ljava/lang/String;


# instance fields
.field private mList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/access/face/FaceList;->mFaceDir:Ljava/lang/String;

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/.face"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/access/face/FaceList;->mFaceDir:Ljava/lang/String;

    .line 38
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/sec/samsung/gallery/access/face/List;-><init>()V

    .line 46
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mContext:Landroid/content/Context;

    .line 49
    return-void
.end method

.method private close()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 105
    return-void
.end method

.method public static close(Lcom/sec/samsung/gallery/access/face/FaceList;)V
    .locals 0
    .param p0, "faceList"    # Lcom/sec/samsung/gallery/access/face/FaceList;

    .prologue
    .line 116
    if-eqz p0, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/sec/samsung/gallery/access/face/FaceList;->close()V

    .line 118
    :cond_0
    return-void
.end method

.method public static getFaceData(Landroid/content/Context;I)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 245
    if-nez p0, :cond_0

    .line 246
    const/4 v2, -0x1

    .line 249
    :goto_0
    return v2

    .line 247
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 248
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 249
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "face_data"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/access/face/FaceList;->getInt(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)I

    move-result v2

    goto :goto_0
.end method

.method public static getFaceFile(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 253
    const/4 v1, 0x0

    .line 254
    .local v1, "faceFile":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceList;->mFaceDir:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 255
    const/4 v2, 0x0

    .line 260
    :goto_0
    return-object v2

    .line 256
    :cond_0
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceData(Landroid/content/Context;I)I

    move-result v0

    .line 257
    .local v0, "faceData":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 258
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceList;->mFaceDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_1
    move-object v2, v1

    .line 260
    goto :goto_0
.end method

.method public static getFaceRect(Landroid/content/Context;I)Landroid/graphics/Rect;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 172
    const/4 v7, 0x0

    .line 173
    .local v7, "rt":Landroid/graphics/Rect;
    if-nez p0, :cond_0

    move-object v8, v7

    .line 194
    .end local v7    # "rt":Landroid/graphics/Rect;
    .local v8, "rt":Landroid/graphics/Rect;
    :goto_0
    return-object v8

    .line 175
    .end local v8    # "rt":Landroid/graphics/Rect;
    .restart local v7    # "rt":Landroid/graphics/Rect;
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 176
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 177
    .local v1, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 180
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x4

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "pos_left"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "pos_top"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "pos_right"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "pos_bottom"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 184
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 185
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 186
    .end local v7    # "rt":Landroid/graphics/Rect;
    .restart local v8    # "rt":Landroid/graphics/Rect;
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v8, Landroid/graphics/Rect;->left:I

    .line 187
    const/4 v2, 0x1

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v8, Landroid/graphics/Rect;->top:I

    .line 188
    const/4 v2, 0x2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v8, Landroid/graphics/Rect;->right:I

    .line 189
    const/4 v2, 0x3

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v8, Landroid/graphics/Rect;->bottom:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v7, v8

    .line 192
    .end local v8    # "rt":Landroid/graphics/Rect;
    .restart local v7    # "rt":Landroid/graphics/Rect;
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move-object v8, v7

    .line 194
    .end local v7    # "rt":Landroid/graphics/Rect;
    .restart local v8    # "rt":Landroid/graphics/Rect;
    goto :goto_0

    .line 192
    .end local v8    # "rt":Landroid/graphics/Rect;
    .restart local v7    # "rt":Landroid/graphics/Rect;
    :catchall_0
    move-exception v2

    :goto_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .end local v7    # "rt":Landroid/graphics/Rect;
    .restart local v8    # "rt":Landroid/graphics/Rect;
    :catchall_1
    move-exception v2

    move-object v7, v8

    .end local v8    # "rt":Landroid/graphics/Rect;
    .restart local v7    # "rt":Landroid/graphics/Rect;
    goto :goto_1
.end method

.method public static getNeedScannedImageCount(Landroid/content/Context;)I
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 301
    const/4 v6, 0x0

    .line 302
    .local v6, "count":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 303
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v9, "select count(_id) from files where media_type=1 and face_count=-1"

    .line 304
    .local v9, "sql":Ljava/lang/String;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceList;->RAW_SQL_MAIN_DB:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 305
    .local v1, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 307
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 308
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 310
    :cond_0
    const/4 v2, 0x0

    invoke-interface {v7, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 311
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 316
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 318
    :goto_0
    return v6

    .line 313
    :catch_0
    move-exception v8

    .line 314
    .local v8, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 316
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/UnsupportedOperationException;
    :catchall_0
    move-exception v2

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public static getPersonId(Landroid/content/Context;I)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 237
    if-nez p0, :cond_0

    .line 238
    const/4 v2, 0x1

    .line 241
    :goto_0
    return v2

    .line 239
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 240
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 241
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "person_id"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/access/face/FaceList;->getInt(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)I

    move-result v2

    goto :goto_0
.end method

.method public static getRecommendedId(Landroid/content/Context;I)I
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    .line 229
    if-nez p0, :cond_0

    .line 230
    const/4 v2, 0x1

    .line 233
    :goto_0
    return v2

    .line 231
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 232
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 233
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "recommended_id"

    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/access/face/FaceList;->getInt(Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentResolver;)I

    move-result v2

    goto :goto_0
.end method

.method public static open(Landroid/content/Context;II)Lcom/sec/samsung/gallery/access/face/FaceList;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "personId"    # I
    .param p2, "imageId"    # I

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    if-nez p0, :cond_0

    move-object v1, v0

    .line 112
    .end local v0    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    .local v1, "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    :goto_0
    return-object v1

    .line 110
    .end local v1    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    .restart local v0    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/access/face/FaceList;

    .end local v0    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/access/face/FaceList;-><init>(Landroid/content/Context;)V

    .line 111
    .restart local v0    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    invoke-direct {v0, p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceList;->open(II)V

    move-object v1, v0

    .line 112
    .end local v0    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    .restart local v1    # "faceList":Lcom/sec/samsung/gallery/access/face/FaceList;
    goto :goto_0
.end method

.method private open(II)V
    .locals 13
    .param p1, "personId"    # I
    .param p2, "imageId"    # I

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 51
    const/4 v7, 0x0

    .line 52
    .local v7, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    const/4 v9, 0x0

    .line 53
    .local v9, "selectionBuilder":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 54
    .local v4, "selectionArgs":[Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 55
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v1, 0x5

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v3

    const-string v1, "image_id"

    aput-object v1, v2, v5

    const-string v1, "person_id"

    aput-object v1, v2, v10

    const-string v1, "recommended_id"

    aput-object v1, v2, v11

    const-string v1, "face_data"

    aput-object v1, v2, v12

    .line 59
    .local v2, "PROJECTION_FACES":[Ljava/lang/String;
    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    .line 60
    new-instance v9, Ljava/lang/StringBuilder;

    .end local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "recommended_id"

    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 61
    .restart local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "=? and "

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 62
    const-string v1, "image_id"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 63
    const-string v1, "=?"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    new-array v4, v10, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    .line 82
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    :goto_0
    const/4 v6, 0x0

    .line 84
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    if-nez v9, :cond_6

    const/4 v3, 0x0

    :goto_1
    const-string v5, "person_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 86
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    move-object v8, v7

    .line 88
    .end local v7    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    .local v8, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    :try_start_1
    new-instance v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    invoke-direct {v7}, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 89
    .end local v8    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    .restart local v7    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    const/4 v1, 0x0

    :try_start_2
    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mId:I

    .line 90
    const/4 v1, 0x1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mFileId:I

    .line 91
    const/4 v1, 0x2

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mPersonId:I

    .line 92
    const/4 v1, 0x3

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mRecommendedId:I

    .line 93
    const/4 v1, 0x4

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v7, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mFaceData:I

    .line 94
    iget-object v1, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 99
    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 102
    return-void

    .line 65
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    if-eqz p1, :cond_5

    .line 66
    const v1, 0xf4240

    if-lt p1, v1, :cond_4

    .line 67
    const v1, 0xf4240

    sub-int/2addr p1, v1

    .line 68
    new-instance v9, Ljava/lang/StringBuilder;

    .end local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "group_id"

    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 69
    .restart local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "=? and recommended_id=1"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 70
    new-array v4, v5, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 72
    :cond_4
    new-instance v9, Ljava/lang/StringBuilder;

    .end local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "recommended_id"

    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 73
    .restart local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "=?"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    new-array v4, v5, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto :goto_0

    .line 77
    :cond_5
    if-eqz p2, :cond_0

    .line 78
    new-instance v9, Ljava/lang/StringBuilder;

    .end local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "image_id"

    invoke-direct {v9, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 79
    .restart local v9    # "selectionBuilder":Ljava/lang/StringBuilder;
    const-string v1, "=?"

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 80
    new-array v4, v5, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v3

    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    goto/16 :goto_0

    .line 84
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_6
    :try_start_3
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    goto/16 :goto_1

    .line 99
    :catchall_0
    move-exception v1

    :goto_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    .end local v7    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    .restart local v8    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    :catchall_1
    move-exception v1

    move-object v7, v8

    .end local v8    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    .restart local v7    # "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    goto :goto_2
.end method

.method public static recommendFaces(Landroid/content/Context;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "faceId"    # I
    .param p3, "newPersonId"    # I

    .prologue
    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://media/external/recommend_person/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 347
    .local v1, "uri":Landroid/net/Uri;
    if-eqz p1, :cond_0

    .line 348
    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->registerFaceRecommendationObserver(Landroid/net/Uri;Z)V

    .line 351
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "content://media/external/recommend_person/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 352
    const/4 v6, 0x0

    .line 354
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 356
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 358
    return-void

    .line 356
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static remove(Landroid/content/Context;I)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x0

    .line 220
    if-nez p0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 223
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 224
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v0, v1, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 225
    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static removeFaceByFileId(Landroid/content/Context;I)V
    .locals 14
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fileId"    # I

    .prologue
    .line 264
    if-nez p0, :cond_0

    .line 298
    :goto_0
    return-void

    .line 266
    :cond_0
    const/4 v8, 0x0

    .line 267
    .local v8, "faceData":[I
    const/4 v9, 0x0

    .line 268
    .local v9, "faceFile":Ljava/lang/String;
    const/4 v11, 0x0

    .line 269
    .local v11, "i":I
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 270
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v7, 0x0

    .line 272
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "face_data"

    aput-object v4, v2, v3

    const-string v3, "image_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 277
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 278
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 279
    .local v6, "count":I
    new-array v8, v6, [I

    move v12, v11

    .line 281
    .end local v11    # "i":I
    .local v12, "i":I
    :goto_1
    add-int/lit8 v11, v12, 0x1

    .end local v12    # "i":I
    .restart local v11    # "i":I
    const/4 v1, 0x0

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    aput v1, v8, v12

    .line 282
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_4

    .line 286
    .end local v6    # "count":I
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 288
    if-eqz v8, :cond_3

    .line 289
    const/4 v10, 0x0

    .line 290
    .local v10, "file":Ljava/io/File;
    const/4 v11, 0x0

    :goto_2
    array-length v1, v8

    if-ge v11, v1, :cond_3

    .line 291
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/sec/samsung/gallery/access/face/FaceList;->mFaceDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, v8, v11

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 292
    new-instance v10, Ljava/io/File;

    .end local v10    # "file":Ljava/io/File;
    invoke-direct {v10, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 293
    .restart local v10    # "file":Ljava/io/File;
    invoke-virtual {v10}, Ljava/io/File;->isFile()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 294
    invoke-virtual {v10}, Ljava/io/File;->delete()Z

    .line 290
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 286
    .end local v10    # "file":Ljava/io/File;
    :catchall_0
    move-exception v1

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1

    .line 297
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const-string v2, "image_id=?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_0

    .restart local v6    # "count":I
    :cond_4
    move v12, v11

    .end local v11    # "i":I
    .restart local v12    # "i":I
    goto :goto_1
.end method

.method public static setFaceUnknown(Landroid/content/Context;I)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 322
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 323
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 324
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 325
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "recommended_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 326
    const-string v3, "person_id"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 327
    const-string v3, "group_id"

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 328
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 329
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 331
    return-void
.end method

.method public static setFaceUnknownIgnoreAutoGroup(Landroid/content/Context;I)V
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 334
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 335
    .local v1, "cr":Landroid/content/ContentResolver;
    sget-object v4, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v6, p1

    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    .line 336
    .local v2, "uri":Landroid/net/Uri;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 337
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "recommended_id"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 338
    const-string v4, "person_id"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 341
    invoke-virtual {v1, v2, v3, v9, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 342
    .local v0, "cnt":I
    const-string v4, "FaceList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setFaceUnknown update cnt = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void
.end method

.method public static setPerson(Landroid/content/Context;II)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "personId"    # I

    .prologue
    const/4 v6, 0x0

    .line 198
    if-nez p0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 200
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 201
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 202
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 203
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "person_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 204
    const-string v3, "recommended_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 205
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 206
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method public static setPersonId(Landroid/content/Context;II)V
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "personId"    # I

    .prologue
    const/4 v6, 0x0

    .line 210
    if-nez p0, :cond_0

    .line 217
    :goto_0
    return-void

    .line 212
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 213
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    int-to-long v4, p1

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 214
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 215
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "person_id"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 216
    invoke-virtual {v0, v1, v2, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getFaceData(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    move-result-object v0

    .line 166
    .local v0, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    if-eqz v0, :cond_0

    .line 167
    iget v1, v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mFaceData:I

    .line 168
    :goto_0
    return v1

    :cond_0
    const/4 v1, -0x1

    goto :goto_0
.end method

.method public getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/face/FaceList;->mList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    .line 123
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFileId(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 158
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    move-result-object v0

    .line 159
    .local v0, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    if-eqz v0, :cond_0

    .line 160
    iget v1, v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mFileId:I

    .line 161
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getId(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 132
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    move-result-object v0

    .line 133
    .local v0, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    if-eqz v0, :cond_0

    .line 134
    iget v1, v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mId:I

    .line 135
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getPersonId(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    move-result-object v0

    .line 152
    .local v0, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    if-eqz v0, :cond_0

    .line 153
    iget v1, v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mPersonId:I

    .line 154
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public getRecommendedId(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 144
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceList;->getFaceItem(I)Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;

    move-result-object v0

    .line 145
    .local v0, "faceItem":Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;
    if-eqz v0, :cond_0

    .line 146
    iget v1, v0, Lcom/sec/samsung/gallery/access/face/FaceList$FaceItem;->mRecommendedId:I

    .line 147
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public remove(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 141
    return-void
.end method
