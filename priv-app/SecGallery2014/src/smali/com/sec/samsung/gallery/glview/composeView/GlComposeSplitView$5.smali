.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;
.super Ljava/lang/Object;
.source "GlComposeSplitView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 543
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    return-object v0
.end method

.method public update(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 533
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v2

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    if-ge v1, v2, :cond_1

    .line 534
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    .line 535
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 536
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeSplitView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {p1, v2, v3}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 533
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 539
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    return-void
.end method
