.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "TimeViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewState$SelectionTask;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;
    }
.end annotation


# static fields
.field private static final CHECK_ALL_ALBUM_COUNT:I = -0x1

.field private static final COLCNT_LEVEL_DEFAULT:I = 0x1

.field private static final COLCNT_LEVEL_DEFAULT_FOR_TABLET:I = 0x2

.field private static final COLCNT_LEVEL_SELECT:I = 0x1

.field private static final KEY_VIDEO_CLIP:Ljava/lang/String; = "VIDEO_CLIP_MULTIPIC"

.field private static final MSG_UPDATE_ACTIONBAR:I = 0x0

.field public static final REQUEST_ADD_TAG:I = 0x905

.field public static final REQUEST_ASSIGN_NAME:I = 0x904

.field public static final REQUEST_CAMERA_LAUNCH_FOR_SINGLE_IMAGE_PICK:I = 0x902

.field public static final REQUEST_CAMERA_LAUNCH_FOR_SINGLE_VIDEO_PICK:I = 0x903

.field private static final REQUEST_HIDDEN_ALBUM_CREATION:I = 0x901

.field private static final TAG:Ljava/lang/String;

.field private static final VIDEO_CLIP_MAX_ITEM:I = 0xf


# instance fields
.field private isfromEditmode:Z

.field private mAdapterConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;

.field private mAlbumIndex:I

.field private mAlbumItemIndex:I

.field private mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mBgImageIndex:I

.field private mChangeViewMode:I

.field private final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field public mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field private mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

.field private mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurrentMediaSetIndex:I

.field private mCurrentTopSetPath:Ljava/lang/String;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEAMReceiver:Landroid/content/BroadcastReceiver;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

.field private mFirstMediaCheck:Z

.field private mFromGalleryWidget:Z

.field private mFromGifMaker:Z

.field private mFromInsideGallery:Z

.field private mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIsAlbumPick:Z

.field private mIsDeleteMode:Z

.field private mIsEasyMode:Z

.field private mIsGifMode:Z

.field private mIsPickerMode:Z

.field private final mMainHandler:Landroid/os/Handler;

.field public mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

.field private mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter$MediaSelectionCallback;

.field private mMenu:Landroid/view/Menu;

.field mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field private mNeedIdleProcess:Z

.field private mRefreshView:Z

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShowContentToDisplay:Z

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mShrinkOption:I

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

.field private mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

.field private mUpdatePath:Z

.field private mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

.field private mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 145
    const-class v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 144
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 159
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .line 161
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 162
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    .line 163
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 164
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaSetIndex:I

    .line 169
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRefreshView:Z

    .line 171
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mBgImageIndex:I

    .line 174
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 175
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 183
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z

    .line 188
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsAlbumPick:Z

    .line 189
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowContentToDisplay:Z

    .line 190
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isfromEditmode:Z

    .line 191
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShrinkOption:I

    .line 192
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mChangeViewMode:I

    .line 193
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    .line 195
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsDeleteMode:Z

    .line 196
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    .line 198
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumIndex:I

    .line 199
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumItemIndex:I

    .line 200
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdatePath:Z

    .line 205
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMainHandler:Landroid/os/Handler;

    .line 219
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;

    const-string v1, "ALBUM_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$2;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 275
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$3;

    const-string v1, "ALBUM_VIEW_EXIT_SELECTION"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$3;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 297
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$4;

    const-string v1, "ALBUM_VIEW_MEDIA_EJECT"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$4;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 321
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$5;

    const-string v1, "SLIDESHOW_SETTINGS"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$5;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 376
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$6;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaSelectionCallback:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter$MediaSelectionCallback;

    .line 405
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$7;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .line 2189
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$19;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComparator:Ljava/util/Comparator;

    .line 2894
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$23;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$23;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    .line 3054
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateConfirmMenu()V

    return-void
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$UpdateSelectionModeTask;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->postUpdateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addToSildeProxy()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbum(I)V

    return-void
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startDetailViewInUIThread(II)V

    return-void
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$4700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$5000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    return v0
.end method

.method static synthetic access$5500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$5700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$5800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startDetailView(II)V

    return-void
.end method

.method static synthetic access$5900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateSelectionMode()V

    return-void
.end method

.method static synthetic access$6000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startSlideShow()V

    return-void
.end method

.method static synthetic access$6300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    return v0
.end method

.method static synthetic access$6600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    return-object v0
.end method

.method static synthetic access$6700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$6900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startSlideshowSettings()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onTabChanged()V

    return-void
.end method

.method static synthetic access$7000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7200(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    return-void
.end method

.method static synthetic access$7300(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7400(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7500(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumPostProcess()V

    return-void
.end method

.method static synthetic access$7600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "x3"    # I
    .param p4, "x4"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;

    .prologue
    .line 144
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    return-void
.end method

.method static synthetic access$7700(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$7900(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lorg/puremvc/java/interfaces/INotification;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .param p1, "x1"    # Lorg/puremvc/java/interfaces/INotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V

    return-void
.end method

.method static synthetic access$8000(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$8100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$900()Ljava/lang/String;
    .locals 1

    .prologue
    .line 144
    sget-object v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            "Ljava/util/Map",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 1604
    .local p3, "tempMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-interface {p3, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1605
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p3, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1607
    :cond_0
    invoke-interface {p3, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1608
    return-void
.end method

.method private addToSildeProxy()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v3, 0x1

    .line 2198
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSlideShowSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v10

    .line 2202
    .local v10, "slideShowProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2203
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isfromEditmode:Z

    .line 2204
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "sort_by_type_time"

    invoke-static {v0, v1, v12}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 2206
    .local v11, "type":I
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$20;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$20;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    if-ne v11, v3, :cond_0

    move v4, v3

    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZZI)V

    new-array v1, v3, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v1, v12

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2256
    .end local v11    # "type":I
    :goto_1
    return-void

    .restart local v11    # "type":I
    :cond_0
    move v4, v12

    .line 2206
    goto :goto_0

    .line 2228
    .end local v11    # "type":I
    :cond_1
    const/4 v9, 0x0

    .line 2229
    .local v9, "resultItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "resultItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 2231
    .restart local v9    # "resultItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isfromEditmode:Z

    .line 2232
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v6

    .line 2233
    .local v6, "count":I
    const/4 v8, 0x0

    .local v8, "position":I
    :goto_2
    if-ge v8, v6, :cond_6

    .line 2235
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 2236
    .local v7, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v0, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v0, :cond_3

    instance-of v0, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    :cond_2
    instance-of v0, v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v0, :cond_3

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v0, :cond_4

    instance-of v0, v7, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v0, :cond_3

    instance-of v0, v7, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v0, :cond_4

    .line 2233
    :cond_3
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_2

    .line 2242
    :cond_4
    instance-of v0, v7, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v0, :cond_5

    .line 2243
    check-cast v7, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v7    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v7

    .line 2245
    .restart local v7    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    if-eqz v7, :cond_3

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v0

    if-nez v0, :cond_3

    .line 2247
    if-eqz v9, :cond_3

    .line 2248
    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 2250
    .end local v7    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v0

    if-nez v0, :cond_7

    .line 2251
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComparator:Ljava/util/Comparator;

    invoke-static {v9, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 2252
    invoke-virtual {v10, v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 2254
    :cond_7
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startSlideShow()V

    goto :goto_1
.end method

.method private cancelSwitchFilterTask()V
    .locals 2

    .prologue
    .line 2071
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    if-eqz v0, :cond_0

    .line 2072
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->cancel(Z)Z

    .line 2073
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 2075
    :cond_0
    return-void
.end method

.method private checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z
    .locals 4
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 3161
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private checkMediaAvailability()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 2266
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v7

    if-nez v7, :cond_3

    move v4, v5

    .line 2267
    .local v4, "newEmptyViewMode":Z
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2268
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 2269
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 2270
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v7

    if-gtz v7, :cond_0

    .line 2271
    const/4 v4, 0x1

    .line 2276
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    if-eqz v4, :cond_5

    .line 2277
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v5

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v5, v7, :cond_4

    .line 2278
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 2279
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$21;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$21;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v5, v7}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2329
    :cond_1
    :goto_1
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFirstMediaCheck:Z

    .line 2330
    :cond_2
    return-void

    .end local v4    # "newEmptyViewMode":Z
    :cond_3
    move v4, v6

    .line 2266
    goto :goto_0

    .line 2286
    .restart local v4    # "newEmptyViewMode":Z
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 2287
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v3

    .line 2288
    .local v3, "mimeType":Ljava/lang/String;
    sget-object v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "MIMETYPE : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 2289
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    .line 2290
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startNoItemViewState(Ljava/lang/String;)V

    .line 2291
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMenu:Landroid/view/Menu;

    if-eqz v5, :cond_2

    .line 2293
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_1

    .line 2295
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "mimeType":Ljava/lang/String;
    :cond_5
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFirstMediaCheck:Z

    if-eqz v7, :cond_7

    .line 2296
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 2297
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 2299
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 2300
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v7, :cond_6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v7, :cond_6

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v7, :cond_1

    .line 2303
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->initializeView()V

    .line 2304
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 2306
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v7, :cond_1

    .line 2307
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v7, v5, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setMode(IILjava/lang/Object;)V

    .line 2308
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v5, :cond_1

    .line 2309
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshSelectionBarState(Z)V

    goto :goto_1

    .line 2313
    .end local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    :cond_7
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 2315
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 2316
    .restart local v0    # "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v7, :cond_8

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v7, :cond_8

    sget-object v7, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v7, :cond_1

    .line 2319
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->initializeView()V

    .line 2320
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 2322
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v7, :cond_1

    .line 2323
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v7, v5, v6, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setMode(IILjava/lang/Object;)V

    .line 2324
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v5, :cond_1

    .line 2325
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshSelectionBarState(Z)V

    goto/16 :goto_1
.end method

.method private checkPickMaxCount(I)Ljava/lang/Boolean;
    .locals 11
    .param p1, "albumIndex"    # I

    .prologue
    const/4 v10, -0x1

    const/4 v9, 0x1

    .line 1624
    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 1625
    .local v6, "result":Ljava/lang/Boolean;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1626
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "pick-max-item"

    invoke-virtual {v3, v8, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 1627
    .local v4, "maxPickCount":I
    if-eq v4, v10, :cond_3

    .line 1628
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_1

    .line 1629
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1630
    .local v0, "allSelectCount":I
    sub-int v8, v4, v0

    if-nez v8, :cond_0

    .line 1631
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    .line 1632
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 1687
    .end local v0    # "allSelectCount":I
    :cond_0
    :goto_0
    return-object v6

    .line 1635
    :cond_1
    const/4 v7, 0x0

    .line 1636
    .local v7, "total":I
    if-ne p1, v10, :cond_2

    .line 1637
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v7

    .line 1650
    :goto_1
    if-le v7, v4, :cond_0

    .line 1651
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    .line 1652
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 1639
    :cond_2
    const/4 v0, 0x0

    .line 1640
    .restart local v0    # "allSelectCount":I
    const/4 v2, 0x0

    .line 1641
    .local v2, "curAlbumSelectCount":I
    const/4 v1, 0x0

    .line 1643
    .local v1, "curAlbumAllCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1644
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 1645
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 1646
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount(I)I

    move-result v1

    .line 1648
    sub-int v8, v0, v2

    add-int v7, v8, v1

    goto :goto_1

    .line 1655
    .end local v0    # "allSelectCount":I
    .end local v1    # "curAlbumAllCount":I
    .end local v2    # "curAlbumSelectCount":I
    .end local v5    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v7    # "total":I
    :cond_3
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_4

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-eqz v8, :cond_4

    .line 1656
    const/16 v4, 0x3e8

    .line 1657
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1658
    .restart local v0    # "allSelectCount":I
    sub-int v8, v4, v0

    if-nez v8, :cond_0

    .line 1659
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    .line 1660
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 1679
    .end local v0    # "allSelectCount":I
    :cond_4
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v8, :cond_0

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromInsideGallery:Z

    if-nez v8, :cond_0

    .line 1680
    sget v4, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    .line 1681
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1682
    .restart local v0    # "allSelectCount":I
    sub-int v8, v4, v0

    if-nez v8, :cond_0

    .line 1683
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    .line 1684
    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0
.end method

.method private checkVideoClipMaxCount(I)Ljava/lang/Boolean;
    .locals 12
    .param p1, "albumIndex"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/16 v9, 0xf

    .line 1691
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 1692
    .local v5, "result":Ljava/lang/Boolean;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1693
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "VIDEO_CLIP_MULTIPIC"

    invoke-virtual {v3, v8, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 1694
    .local v7, "videoClip":Z
    if-eqz v7, :cond_0

    .line 1695
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v8, :cond_1

    .line 1696
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1697
    .local v0, "allSelectCount":I
    rsub-int/lit8 v8, v0, 0xf

    if-nez v8, :cond_0

    .line 1698
    invoke-direct {p0, v9}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showExceedMaxItemsVideoEdit(I)V

    .line 1699
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 1723
    .end local v0    # "allSelectCount":I
    :cond_0
    :goto_0
    return-object v5

    .line 1702
    :cond_1
    const/4 v6, 0x0

    .line 1703
    .local v6, "total":I
    const/4 v8, -0x1

    if-ne p1, v8, :cond_2

    .line 1704
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v6

    .line 1717
    :goto_1
    if-le v6, v9, :cond_0

    .line 1718
    invoke-direct {p0, v9}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showExceedMaxItemsVideoEdit(I)V

    .line 1719
    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    goto :goto_0

    .line 1706
    :cond_2
    const/4 v0, 0x0

    .line 1707
    .restart local v0    # "allSelectCount":I
    const/4 v2, 0x0

    .line 1708
    .local v2, "curAlbumSelectCount":I
    const/4 v1, 0x0

    .line 1710
    .local v1, "curAlbumAllCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    .line 1711
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1712
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v2

    .line 1713
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount(I)I

    move-result v1

    .line 1715
    sub-int v8, v0, v2

    add-int v6, v8, v1

    goto :goto_1
.end method

.method private downloadNearby(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 0
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/NullPointerException;
        }
    .end annotation

    .prologue
    .line 2791
    return-void
.end method

.method private enterSelectionMode(Z)V
    .locals 5
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 1328
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 1329
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-nez v0, :cond_2

    .line 1330
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 1333
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsDeleteMode:Z

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setDeleteSelectMode(Z)V

    .line 1334
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v1, :cond_3

    .line 1335
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1336
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1344
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_1

    .line 1345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v4, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setMode(IILjava/lang/Object;)V

    .line 1346
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshSelectionBarState(Z)V

    .line 1348
    :cond_1
    return-void

    .line 1331
    :cond_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v0, :cond_0

    .line 1332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    goto :goto_0

    .line 1338
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1339
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-nez v0, :cond_4

    .line 1340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_1

    .line 1342
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_1
.end method

.method private exitSelectionMode()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1351
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 1352
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->exitSelectionMode()V

    .line 1353
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 1354
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1355
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setDownloadMode(Z)V

    .line 1356
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeFilterAlbums(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 1370
    :cond_0
    :goto_0
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsDeleteMode:Z

    .line 1371
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v1, :cond_1

    .line 1372
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    .line 1373
    :cond_1
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    .line 1374
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setGifMode(Z)V

    .line 1375
    return-void

    .line 1358
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v0

    .line 1359
    .local v0, "menu":Landroid/widget/ListPopupWindow;
    if-eqz v0, :cond_3

    .line 1360
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 1362
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1363
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1364
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 1365
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v1, :cond_0

    .line 1366
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setMode(IILjava/lang/Object;)V

    .line 1367
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshSelectionBarState(Z)V

    goto :goto_0
.end method

.method private finishCurrentViewState()V
    .locals 1

    .prologue
    .line 2670
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 2671
    return-void
.end method

.method private getDefaultPath()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 553
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 554
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v4

    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 555
    .local v0, "basePath":Ljava/lang/String;
    const/high16 v3, 0x100000

    invoke-static {v0, v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    .line 585
    .end local v0    # "basePath":Ljava/lang/String;
    .local v2, "newPath":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v2

    .line 557
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v5, "mtpMode"

    invoke-static {v4, v5, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    .line 558
    .local v1, "isMtpMode":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mData:Landroid/os/Bundle;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mData:Landroid/os/Bundle;

    const-string v5, "KEY_MEDIA_SET_PATH"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    .line 559
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-eqz v4, :cond_3

    .line 560
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowContentToDisplay:Z

    .line 561
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v2

    .line 575
    .restart local v2    # "newPath":Ljava/lang/String;
    :goto_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAlbumGrouping:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    .line 576
    const-string v3, "/combo/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 577
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/combo/{"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "}"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 581
    :cond_2
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 582
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getAddMtpTopPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 563
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mData:Landroid/os/Bundle;

    const-string v5, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 564
    .restart local v2    # "newPath":Ljava/lang/String;
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v4, :cond_5

    const-string v4, "combo"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "cloud"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "sns"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string v4, "picasa"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    const-string/jumbo v4, "tCloud"

    invoke-virtual {v2, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_4
    const/4 v3, 0x1

    :cond_5
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowContentToDisplay:Z

    goto :goto_1

    .line 569
    .end local v2    # "newPath":Ljava/lang/String;
    :cond_6
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowContentToDisplay:Z

    .line 570
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getTopSetPathByFilter()Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "newPath":Ljava/lang/String;
    goto :goto_1
.end method

.method private getFileFullName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2067
    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getRefreshState(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 2181
    const/4 v0, 0x0

    .line 2183
    .local v0, "refreshView":Z
    if-eqz p1, :cond_0

    .line 2184
    const-string v1, "KEY_VIEW_REDRAW"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 2186
    :cond_0
    return v0
.end method

.method private getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 3
    .param p1, "viewByType"    # I

    .prologue
    .line 2122
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    .line 2123
    .local v0, "tabType":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, p1}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 2124
    .local v1, "topPath":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 2125
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    return-object v2
.end method

.method private getTopSetPathByFilter()Ljava/lang/String;
    .locals 7

    .prologue
    .line 2148
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2149
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 2150
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 2176
    :cond_0
    :goto_0
    return-object v1

    .line 2151
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v4, v5, :cond_3

    .line 2153
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;ILcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Ljava/lang/String;

    move-result-object v0

    .line 2155
    .local v0, "basePath":Ljava/lang/String;
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 2156
    .local v3, "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v2

    .line 2157
    .local v2, "clusterType":I
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 2158
    .local v1, "clusterPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    goto :goto_0

    .line 2162
    .end local v0    # "basePath":Ljava/lang/String;
    .end local v1    # "clusterPath":Ljava/lang/String;
    .end local v2    # "clusterType":I
    .end local v3    # "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_3
    const/4 v1, 0x0

    .line 2164
    .restart local v1    # "clusterPath":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    .line 2166
    .restart local v3    # "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getViewByTopSetPath(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 2167
    .restart local v0    # "basePath":Ljava/lang/String;
    invoke-static {v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v2

    .line 2168
    .restart local v2    # "clusterType":I
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SECRET_BOX:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v4, :cond_4

    .line 2169
    sget-object v4, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2173
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2174
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    goto :goto_0

    .line 2171
    :cond_4
    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method private handleHiddenAlbumsLaunch()V
    .locals 4

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    const/16 v2, 0x901

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->startStateForResult(Ljava/lang/Class;ILandroid/os/Bundle;)V

    .line 2130
    return-void
.end method

.method private handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V
    .locals 8
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "request"    # I

    .prologue
    .line 2710
    if-nez p1, :cond_1

    .line 2736
    :cond_0
    :goto_0
    return-void

    .line 2714
    :cond_1
    const/16 v5, 0x902

    if-ne p2, v5, :cond_2

    .line 2715
    const-string/jumbo v5, "uri-data"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2716
    .local v0, "fileString":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2718
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 2723
    .end local v0    # "fileString":Ljava/lang/String;
    .local v4, "uri":Landroid/net/Uri;
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v6, 0x0

    invoke-virtual {v5, v4, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    .line 2724
    .local v3, "path":Lcom/sec/android/gallery3d/data/Path;
    if-nez v3, :cond_3

    .line 2725
    sget-object v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "cannot find file : path is null : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2720
    .end local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_2
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .restart local v4    # "uri":Landroid/net/Uri;
    goto :goto_1

    .line 2729
    .restart local v3    # "path":Lcom/sec/android/gallery3d/data/Path;
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v5, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 2730
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 2732
    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v6, v2, v5

    const/4 v5, 0x1

    aput-object v1, v2, v5

    .line 2735
    .local v2, "params":[Ljava/lang/Object;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v5

    const-string v6, "PICKER_ITEM_SELECTED"

    invoke-virtual {v5, v6, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private handleResultWallpaper(Landroid/content/Intent;I)V
    .locals 7
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "result"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v6, -0x1

    const/4 v3, 0x0

    .line 2696
    if-eq p2, v6, :cond_0

    if-nez p2, :cond_1

    if-eqz p1, :cond_1

    const-string v5, "is_pressed_cancel"

    invoke-virtual {p1, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_1

    :cond_0
    move v0, v4

    .line 2698
    .local v0, "finishActivity":Z
    :goto_0
    if-eqz v0, :cond_3

    .line 2699
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-ne p2, v6, :cond_2

    .end local p1    # "data":Landroid/content/Intent;
    :goto_1
    invoke-virtual {v3, p2, p1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 2700
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 2707
    :goto_2
    return-void

    .end local v0    # "finishActivity":Z
    .restart local p1    # "data":Landroid/content/Intent;
    :cond_1
    move v0, v3

    .line 2696
    goto :goto_0

    .restart local v0    # "finishActivity":Z
    :cond_2
    move-object p1, v2

    .line 2699
    goto :goto_1

    .line 2702
    :cond_3
    const/4 v5, 0x2

    new-array v1, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v5, v1, v3

    aput-object v2, v1, v4

    .line 2705
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "PICKER_ITEM_SELECTED"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method private handleSortByAction(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    const/4 v3, 0x1

    .line 2135
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v2, "sort_by_type_time"

    invoke-static {v1, v2, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2137
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2138
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->setSortByType(I)V

    .line 2139
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->setForceReload(Z)V

    .line 2140
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->reloadData()V

    .line 2142
    if-ne p1, v3, :cond_0

    .line 2143
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "SORT"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2145
    :cond_0
    return-void
.end method

.method private initTimeViewEventHandler()V
    .locals 3

    .prologue
    .line 2341
    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    .line 2343
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    .line 2344
    .local v0, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 2347
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V

    .line 2351
    :goto_0
    return-void

    .line 2349
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    goto :goto_0
.end method

.method private isAvailableEAM()Z
    .locals 1

    .prologue
    .line 3174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3175
    :cond_1
    const/4 v0, 0x0

    .line 3177
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isLaunchedFromMainMenu()Z
    .locals 2

    .prologue
    .line 2666
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onTabChanged()V
    .locals 0

    .prologue
    .line 2773
    return-void
.end method

.method private postUpdateCountOnActionBar()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMainHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMainHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x50

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3158
    return-void
.end method

.method private runGroupSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "position"    # I

    .prologue
    .line 2999
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$25;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$25;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 3024
    return-void
.end method

.method private runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 2963
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$24;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Lcom/sec/android/gallery3d/data/MediaSet;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2996
    return-void
.end method

.method private selectAlbum(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    const/4 v6, -0x1

    .line 1805
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1806
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v2, 0x0

    .line 1808
    .local v2, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v3, :cond_0

    .line 1860
    :goto_0
    return-void

    .line 1811
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    if-ne v4, v5, :cond_1

    .line 1812
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1813
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 1814
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumPostProcess()V

    goto :goto_0

    .line 1816
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkPickMaxCount(I)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1817
    sget-object v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v5, "Limit Max Pick count"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1821
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkVideoClipMaxCount(I)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1822
    sget-object v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v5, "Limit Video Clip Max Pick count"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1824
    :cond_3
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v4, :cond_9

    .line 1825
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1826
    .local v0, "intent":Landroid/content/Intent;
    const-string v4, "pick-max-item"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1827
    .local v1, "maxPickCount":I
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v4, :cond_4

    if-eq v1, v6, :cond_4

    .line 1828
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    sub-int v4, v1, v4

    invoke-direct {p0, v3, v4, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectLimitedAlbumProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V

    .line 1849
    :goto_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumPostProcess()V

    goto :goto_0

    .line 1829
    :cond_4
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v4, :cond_5

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-eqz v4, :cond_5

    .line 1830
    const/16 v1, 0x3e8

    .line 1831
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1832
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    sub-int v4, v1, v4

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;II)V

    goto :goto_1

    .line 1833
    :cond_5
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v4, :cond_7

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromInsideGallery:Z

    if-nez v4, :cond_7

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-nez v4, :cond_7

    .line 1834
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v4, :cond_6

    .line 1835
    const/16 v1, 0x14

    .line 1838
    :goto_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1839
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    sub-int v4, v1, v4

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;II)V

    goto :goto_1

    .line 1837
    :cond_6
    sget v1, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    goto :goto_2

    .line 1840
    :cond_7
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v4, :cond_8

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-eqz v4, :cond_8

    .line 1841
    const-string v4, "select_num_key"

    invoke-virtual {v0, v4, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1842
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1843
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    sub-int v4, v1, v4

    invoke-direct {p0, v2, v3, v4, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;II)V

    goto :goto_1

    .line 1845
    :cond_8
    sget-object v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v5, "This should not have happened!!"

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1846
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1847
    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1

    .line 1851
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "maxPickCount":I
    :cond_9
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItemCount(I)I

    move-result v4

    sget v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMaxCountForSelectionLoadingTask:I

    if-le v4, v5, :cond_a

    .line 1852
    invoke-direct {p0, v3, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->runGroupSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;I)V

    goto/16 :goto_0

    .line 1854
    :cond_a
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v2

    .line 1855
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4, v3, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 1856
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumPostProcess()V

    goto/16 :goto_0
.end method

.method private selectAlbumPostProcess()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 1754
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 1755
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    const/4 v5, 0x1

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    invoke-virtual {p0, v4, v5, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableCount(IZZ)Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 1756
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshCheckState()V

    .line 1758
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v3, :cond_0

    .line 1759
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1760
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "pick-max-item"

    invoke-virtual {v0, v3, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1761
    .local v1, "pickItemCount":I
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v3, :cond_0

    if-eq v1, v7, :cond_0

    .line 1762
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 1763
    .local v2, "totalSelectedCount":I
    if-lt v2, v1, :cond_0

    .line 1764
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    .line 1768
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pickItemCount":I
    .end local v2    # "totalSelectedCount":I
    :cond_0
    return-void
.end method

.method private selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 1
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v0, -0x1

    .line 1727
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;II)V

    .line 1728
    return-void
.end method

.method private selectAlbumProcess(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;II)V
    .locals 9
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "maxAddCnt"    # I
    .param p4, "maxPickCount"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "II)V"
        }
    .end annotation

    .prologue
    .local p1, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1731
    const/4 v6, -0x1

    if-eq p3, v6, :cond_1

    move v1, v4

    .line 1732
    .local v1, "isLimited":Z
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 1733
    if-eqz v1, :cond_2

    if-gtz p3, :cond_2

    .line 1734
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0117

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-virtual {v6, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1736
    .local v3, "text":Ljava/lang/CharSequence;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1751
    .end local v3    # "text":Ljava/lang/CharSequence;
    :cond_0
    return-void

    .end local v0    # "i":I
    .end local v1    # "isLimited":Z
    :cond_1
    move v1, v5

    .line 1731
    goto :goto_0

    .line 1739
    .restart local v0    # "i":I
    .restart local v1    # "isLimited":Z
    :cond_2
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1740
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v6

    if-eqz v6, :cond_5

    .line 1741
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0107

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1732
    :cond_4
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1744
    :cond_5
    if-eqz v1, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 1746
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, p2, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1747
    if-eqz v1, :cond_4

    .line 1748
    add-int/lit8 p3, p3, -0x1

    goto :goto_2
.end method

.method private selectAll()V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 1381
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkPickMaxCount(I)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1382
    sget-object v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v7, "Limit Max Pick count"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1383
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 1432
    :cond_0
    :goto_0
    return-void

    .line 1387
    :cond_1
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkVideoClipMaxCount(I)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 1388
    sget-object v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v7, "Limit Video Clip Max Pick count"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 1393
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 1394
    .local v3, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v4

    .line 1395
    .local v4, "totalCount":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1396
    .local v0, "intent":Landroid/content/Intent;
    const-string v6, "pick-max-item"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1397
    .local v1, "maxPickCount":I
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_3

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_3

    if-eq v1, v7, :cond_3

    .line 1398
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectLimitedAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 1399
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    .line 1423
    :goto_1
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_0

    .line 1424
    const-string v6, "pick-max-item"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1425
    .local v2, "pickItemCount":I
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_0

    if-eq v2, v7, :cond_0

    .line 1426
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    .line 1427
    .local v5, "totalSelectedCount":I
    if-lt v5, v2, :cond_0

    .line 1428
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showMaximumSelectionNumberExceeded(I)V

    goto :goto_0

    .line 1400
    .end local v2    # "pickItemCount":I
    .end local v5    # "totalSelectedCount":I
    :cond_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_4

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-eqz v6, :cond_4

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_4

    .line 1401
    const/16 v1, 0x3e8

    .line 1402
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 1403
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    goto :goto_1

    .line 1404
    :cond_4
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v6, :cond_5

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_5

    .line 1405
    const/16 v1, 0x14

    .line 1406
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 1407
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    goto :goto_1

    .line 1408
    :cond_5
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-eqz v6, :cond_6

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_6

    .line 1409
    const-string v6, "select_num_key"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1410
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 1411
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    goto :goto_1

    .line 1412
    :cond_6
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v6, :cond_7

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v6, :cond_7

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromInsideGallery:Z

    if-nez v6, :cond_7

    .line 1413
    sget v1, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    .line 1414
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V

    .line 1415
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    goto :goto_1

    .line 1416
    :cond_7
    sget v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mMaxCountForSelectionLoadingTask:I

    if-le v4, v6, :cond_8

    .line 1417
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto/16 :goto_1

    .line 1419
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0, v6, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 1420
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllPostProcess()V

    goto/16 :goto_1
.end method

.method private selectAllPostProcess()V
    .locals 4

    .prologue
    .line 1435
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 1436
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    invoke-virtual {p0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableCount(IZZ)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 1438
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshCheckState()V

    .line 1439
    return-void
.end method

.method private selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1446
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    .line 1447
    return-void
.end method

.method private selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "maxCnt"    # I

    .prologue
    .line 1442
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p2, p3, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V

    .line 1443
    return-void
.end method

.method private selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "maxCnt"    # I
    .param p4, "task"    # Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;

    .prologue
    .line 1452
    new-instance v14, Ljava/util/HashMap;

    invoke-direct {v14}, Ljava/util/HashMap;-><init>()V

    .line 1453
    .local v14, "tempSelectionMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    const/16 v17, -0x1

    move/from16 v0, p3

    move/from16 v1, v17

    if-eq v0, v1, :cond_3

    const/4 v5, 0x1

    .line 1454
    .local v5, "isLimited":Z
    :goto_0
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v4

    .line 1455
    .local v4, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v16

    .line 1456
    .local v16, "totalSelectedCount":I
    sub-int v7, p3, v16

    .line 1458
    .local v7, "maxAddCnt":I
    const/4 v11, 0x0

    .local v11, "position":I
    :goto_1
    if-ge v11, v4, :cond_0

    .line 1459
    if-eqz v5, :cond_4

    if-gtz v7, :cond_4

    .line 1521
    :cond_0
    if-eqz p4, :cond_1

    .line 1522
    invoke-virtual/range {p4 .. p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->setFinishingState()V

    .line 1523
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/Map;)V

    .line 1524
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_2

    .line 1525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/Map;)V

    .line 1527
    :cond_2
    :goto_2
    return-void

    .line 1453
    .end local v4    # "count":I
    .end local v5    # "isLimited":Z
    .end local v7    # "maxAddCnt":I
    .end local v11    # "position":I
    .end local v16    # "totalSelectedCount":I
    :cond_3
    const/4 v5, 0x0

    goto :goto_0

    .line 1461
    .restart local v4    # "count":I
    .restart local v5    # "isLimited":Z
    .restart local v7    # "maxAddCnt":I
    .restart local v11    # "position":I
    .restart local v16    # "totalSelectedCount":I
    :cond_4
    if-eqz p4, :cond_5

    invoke-virtual/range {p4 .. p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->isCancelled()Z

    move-result v17

    if-nez v17, :cond_2

    .line 1464
    :cond_5
    move-object/from16 v0, p2

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    .line 1465
    .local v10, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v10, :cond_7

    .line 1458
    :cond_6
    :goto_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1468
    :cond_7
    const/16 v17, 0x0

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v10, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v9

    .line 1469
    .local v9, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v9, :cond_6

    .line 1472
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v17

    if-eqz v17, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_8

    .line 1473
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_2

    .line 1477
    :cond_8
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 1478
    .local v13, "size":I
    const/4 v12, 0x1

    .line 1479
    .local v12, "showUnsupportedFileToast":Z
    const/4 v6, 0x0

    .local v6, "ix":I
    :goto_4
    if-ge v6, v13, :cond_6

    .line 1480
    if-eqz p4, :cond_9

    invoke-virtual/range {p4 .. p4}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->isCancelled()Z

    move-result v17

    if-nez v17, :cond_2

    .line 1483
    :cond_9
    if-eqz p4, :cond_a

    .line 1484
    const-wide/16 v18, 0x1

    const/16 v17, 0x0

    move-object/from16 v0, p4

    move-wide/from16 v1, v18

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 1485
    const-wide/16 v18, 0x1

    const/16 v17, 0x1

    move-object/from16 v0, p4

    move-wide/from16 v1, v18

    move/from16 v3, v17

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTask;->increaseProgress(JZ)V

    .line 1488
    :cond_a
    if-eqz v5, :cond_b

    if-gtz v7, :cond_b

    if-lez p3, :cond_b

    .line 1489
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0e0117

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-virtual/range {v17 .. v19}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 1491
    .local v15, "text":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    invoke-interface {v15}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_3

    .line 1494
    .end local v15    # "text":Ljava/lang/CharSequence;
    :cond_b
    invoke-virtual {v9, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1495
    .local v8, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_10

    .line 1496
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v17

    if-eqz v17, :cond_c

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v17

    if-nez v17, :cond_e

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v17

    if-eqz v17, :cond_e

    .line 1498
    :cond_c
    if-eqz v12, :cond_d

    .line 1499
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1500
    const/4 v12, 0x0

    .line 1479
    :cond_d
    :goto_5
    add-int/lit8 v6, v6, 0x1

    goto/16 :goto_4

    .line 1504
    :cond_e
    if-eqz v5, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_d

    .line 1506
    :cond_f
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v8, v14}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    .line 1507
    if-eqz v5, :cond_d

    .line 1508
    add-int/lit8 v7, v7, -0x1

    goto :goto_5

    .line 1510
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v17

    if-eqz v17, :cond_11

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_11

    .line 1511
    if-eqz v12, :cond_d

    .line 1512
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1513
    const/4 v12, 0x0

    goto :goto_5

    .line 1517
    :cond_11
    move-object/from16 v0, p0

    invoke-direct {v0, v10, v8, v14}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    goto :goto_5
.end method

.method private selectLimitedAlbumProcess(Lcom/sec/android/gallery3d/data/MediaSet;II)V
    .locals 12
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "maxAddCnt"    # I
    .param p3, "maxPickCount"    # I

    .prologue
    .line 1771
    const/4 v2, 0x0

    .line 1773
    .local v2, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v6

    .line 1774
    .local v6, "totalCount":I
    div-int v0, v6, p3

    .line 1776
    .local v0, "fullStep":I
    const/4 v4, 0x0

    .local v4, "step":I
    :goto_0
    if-gt v4, v0, :cond_0

    .line 1777
    if-gtz p2, :cond_1

    .line 1802
    :cond_0
    return-void

    .line 1779
    :cond_1
    mul-int v7, v4, p3

    invoke-virtual {p1, v7, p3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v2

    .line 1781
    const/4 v3, 0x0

    .local v3, "pos":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_3

    .line 1782
    if-gtz p2, :cond_4

    .line 1783
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectAllForLimitationPickMode:Z

    if-eqz v7, :cond_3

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-nez v7, :cond_2

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-eqz v7, :cond_3

    .line 1784
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0e0117

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/16 v11, 0x3e8

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1786
    .local v5, "text":Ljava/lang/CharSequence;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1776
    .end local v5    # "text":Ljava/lang/CharSequence;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1790
    :cond_4
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1791
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v7

    if-nez v7, :cond_7

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 1792
    :cond_5
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v8, 0x7f0e0107

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1781
    :cond_6
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1795
    :cond_7
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-nez v7, :cond_6

    .line 1797
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, p1, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1798
    add-int/lit8 p2, p2, -0x1

    goto :goto_2
.end method

.method private selectLimitedAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;I)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "maxCnt"    # I

    .prologue
    .line 1532
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    .line 1534
    .local v13, "tempSelectionMap":Ljava/util/Map;, "Ljava/util/Map<Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;>;"
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v2

    .line 1535
    .local v2, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v16

    .line 1536
    .local v16, "totalSelectedCount":I
    sub-int v5, p3, v16

    .line 1538
    .local v5, "maxAddCnt":I
    const/4 v9, 0x0

    .local v9, "position":I
    :goto_0
    if-ge v9, v2, :cond_0

    .line 1539
    if-gtz v5, :cond_2

    .line 1597
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/Map;)V

    .line 1598
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 1599
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/Map;)V

    .line 1601
    :cond_1
    :goto_1
    return-void

    .line 1541
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 1542
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v8, :cond_4

    .line 1538
    :cond_3
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 1545
    :cond_4
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v15

    .line 1546
    .local v15, "totalCount":I
    div-int v12, v15, p3

    .line 1547
    .local v12, "step":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-gt v3, v12, :cond_3

    .line 1548
    if-lez v5, :cond_3

    .line 1550
    mul-int v17, v3, p3

    move/from16 v0, v17

    move/from16 v1, p3

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v7

    .line 1551
    .local v7, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez v7, :cond_6

    .line 1547
    :cond_5
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 1554
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v17

    if-eqz v17, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_7

    .line 1555
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_1

    .line 1559
    :cond_7
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1560
    .local v11, "size":I
    const/4 v10, 0x1

    .line 1561
    .local v10, "showUnsupportedFileToast":Z
    const/4 v4, 0x0

    .local v4, "ix":I
    :goto_4
    if-ge v4, v11, :cond_5

    .line 1562
    if-gtz v5, :cond_8

    .line 1563
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    move/from16 v17, v0

    if-eqz v17, :cond_5

    .line 1564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f0e0117

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    aput-object v21, v19, v20

    invoke-virtual/range {v17 .. v19}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 1566
    .local v14, "text":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    invoke-interface {v14}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_3

    .line 1570
    .end local v14    # "text":Ljava/lang/CharSequence;
    :cond_8
    invoke-virtual {v7, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1571
    .local v6, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    move/from16 v17, v0

    if-eqz v17, :cond_c

    .line 1572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-static {v0, v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v17

    if-eqz v17, :cond_9

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v17

    if-nez v17, :cond_b

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 1574
    :cond_9
    if-eqz v10, :cond_a

    .line 1575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1576
    const/4 v10, 0x0

    .line 1561
    :cond_a
    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1580
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_a

    .line 1582
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v6, v13}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    .line 1583
    add-int/lit8 v5, v5, -0x1

    goto :goto_5

    .line 1585
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v17

    if-eqz v17, :cond_d

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v17

    if-nez v17, :cond_d

    .line 1586
    if-eqz v10, :cond_a

    .line 1587
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v17, v0

    const v18, 0x7f0e0107

    invoke-static/range {v17 .. v18}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1588
    const/4 v10, 0x0

    goto :goto_5

    .line 1592
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v6, v13}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->addItemtoTempMap(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/util/Map;)V

    goto :goto_5
.end method

.method private setDisplaySearchIcon(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "isDisplay"    # Ljava/lang/Boolean;

    .prologue
    .line 2333
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 2334
    .local v0, "actionbar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    if-eqz v1, :cond_0

    .line 2335
    check-cast v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    .end local v0    # "actionbar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;->setNoItemMode(Z)V

    .line 2337
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 2338
    return-void

    .line 2335
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private setFirstLoding(I)V
    .locals 6
    .param p1, "level"    # I

    .prologue
    .line 1000
    const/4 v3, 0x0

    .line 1001
    .local v3, "row_count":I
    const/4 v0, 0x0

    .line 1002
    .local v0, "column_count":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v1, v4, Landroid/content/res/Configuration;->orientation:I

    .line 1003
    .local v1, "orientation":I
    const/4 v2, 0x1

    .line 1004
    .local v2, "portraitMode":Z
    const/4 v4, 0x1

    if-ne v1, v4, :cond_0

    .line 1005
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_FIRSTLOAD_ROWCNT_PORT:[I

    aget v3, v4, p1

    .line 1010
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->getGridColumnsCount(Landroid/content/res/Resources;Z)[I

    move-result-object v4

    aget v0, v4, p1

    .line 1011
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, v3, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingValues(II)V

    .line 1012
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    mul-int v5, v3, v0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstLoadingCount(I)V

    .line 1013
    return-void

    .line 1007
    :cond_0
    sget-object v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->GRID_FIRSTLOAD_ROWCNT_LAND:[I

    aget v3, v4, p1

    .line 1008
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private setLaunchMode()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2354
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v1

    .line 2356
    .local v1, "currentMode":Lcom/sec/samsung/gallery/core/LaunchModeType;
    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v1, v3, :cond_0

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v1, v3, :cond_3

    .line 2360
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v0

    .line 2362
    .local v0, "allItemsCount":I
    if-nez v0, :cond_1

    .line 2363
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 2365
    .local v2, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_1

    .line 2366
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    .line 2369
    .end local v2    # "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    if-eqz v0, :cond_2

    .line 2370
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->initializeView()V

    .line 2371
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 2406
    .end local v0    # "allItemsCount":I
    :cond_2
    :goto_0
    return-void

    .line 2373
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 2374
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2375
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2376
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 2378
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v4, :cond_5

    .line 2379
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2380
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewPeopleActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2381
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 2384
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2385
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2386
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 2390
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v3, :cond_7

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2391
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2392
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 2393
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2394
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2395
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForDownload;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForDownload;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2396
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 2397
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 2398
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    goto/16 :goto_0

    .line 2399
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2400
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2401
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2402
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 2403
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto/16 :goto_0
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2579
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 2581
    .local v0, "isSelectAll":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 2582
    return-void

    .line 2579
    .end local v0    # "isSelectAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showExceedMaxItemsVideoEdit(I)V
    .locals 6
    .param p1, "maxCount"    # I

    .prologue
    .line 1617
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e02ef

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1620
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1621
    return-void
.end method

.method private showMaximumSelectionNumberExceeded(I)V
    .locals 6
    .param p1, "maxCount"    # I

    .prologue
    .line 1611
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0117

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1613
    .local v0, "text":Ljava/lang/CharSequence;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1614
    return-void
.end method

.method private startDetailView(II)V
    .locals 13
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 2010
    sget-boolean v10, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTimeAllSelectMode:Z

    if-nez v10, :cond_0

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v10

    if-nez v10, :cond_2

    :cond_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v10

    sget-object v11, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v10, v11, :cond_2

    const/4 v0, 0x1

    .line 2012
    .local v0, "bEnableTimeAll":Z
    :goto_0
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v10, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 2013
    .local v5, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v10

    if-gtz v10, :cond_3

    .line 2064
    :cond_1
    :goto_1
    return-void

    .line 2010
    .end local v0    # "bEnableTimeAll":Z
    .end local v5    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2017
    .restart local v0    # "bEnableTimeAll":Z
    .restart local v5    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_3
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v10, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 2018
    .local v4, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v4, :cond_4

    .line 2019
    sget-object v10, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v11, "cannot start DetailView. mediaItem is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 2022
    :cond_4
    instance-of v10, v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v10, :cond_5

    .line 2023
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v4    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    .line 2025
    .restart local v4    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getScreenNailImage()Landroid/graphics/Bitmap;

    move-result-object v3

    .line 2026
    .local v3, "itemBitmap":Landroid/graphics/Bitmap;
    const/4 v6, 0x0

    .line 2027
    .local v6, "newBitmap":Landroid/graphics/Bitmap;
    if-nez v3, :cond_7

    .line 2028
    sget-object v10, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v11, "cannot start anmation to DetailView. bitmap is null"

    invoke-static {v10, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2032
    :goto_2
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v10, p0}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousActivityState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 2033
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v11

    invoke-virtual {v10, v6, v11}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousBitmap(Landroid/graphics/Bitmap;I)V

    .line 2034
    const/4 v7, 0x0

    .line 2035
    .local v7, "path":Ljava/lang/String;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2036
    .local v1, "bundle":Landroid/os/Bundle;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2037
    if-eqz v0, :cond_8

    .line 2038
    const-string v10, "KEY_MEDIA_SET_PATH"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "/timeall"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2041
    :goto_3
    const-string v10, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1, v10, v7}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2042
    const-string v10, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v1, v10, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2043
    if-eqz v0, :cond_9

    .line 2044
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v10}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v9

    .line 2045
    .local v9, "subSetSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_4
    if-ge v2, p1, :cond_9

    if-ge v2, v9, :cond_9

    .line 2046
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v10, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 2047
    .local v8, "subMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v8, :cond_6

    .line 2048
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v10

    add-int/2addr p2, v10

    .line 2045
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2030
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v2    # "i":I
    .end local v7    # "path":Ljava/lang/String;
    .end local v8    # "subMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v9    # "subSetSize":I
    :cond_7
    invoke-static {v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_2

    .line 2040
    .restart local v1    # "bundle":Landroid/os/Bundle;
    .restart local v7    # "path":Ljava/lang/String;
    :cond_8
    const-string v10, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 2051
    :cond_9
    const-string v10, "KEY_ITEM_POSITION"

    invoke-virtual {v1, v10, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2053
    if-nez p1, :cond_a

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v10

    sget-object v11, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v10, v11, :cond_a

    .line 2054
    const-string v10, "add_camera_shortcut"

    const/4 v11, 0x1

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2057
    :cond_a
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inExpansionMode()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 2058
    const-string v10, "add_camera_shortcut"

    const/4 v11, 0x0

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2059
    const-string v10, "expansion_detail_view"

    const/4 v11, 0x1

    invoke-virtual {v1, v10, v11}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2060
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v10, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addExpMediaItem(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 2063
    :cond_b
    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v10

    const-class v11, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v10, v11, v1}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_1
.end method

.method private startDetailViewInUIThread(II)V
    .locals 2
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 1998
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setClickEnabled(Z)V

    .line 1999
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$17;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;II)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2007
    return-void
.end method

.method private startNoItemViewState(Ljava/lang/String;)V
    .locals 3
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 2739
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 2741
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 2742
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2743
    const-string v1, "KEY_NOITEMSVIEW_MIME_TYPE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2744
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    .line 2745
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsAlbumPick:Z

    if-eqz v1, :cond_0

    .line 2746
    const-string v1, "album-pick"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 2748
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$22;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$22;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 2756
    return-void
.end method

.method private startSlideShow()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 2259
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isfromEditmode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 2262
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2263
    return-void
.end method

.method private startSlideshowSettings()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 2794
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2795
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 2796
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 2797
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 2798
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 2799
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v1, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 2800
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 2805
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_1

    .line 2806
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSlideShowMode(Z)V

    .line 2808
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 2809
    return-void

    .line 2802
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private unselectAll()V
    .locals 8

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 1863
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 1864
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1865
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1866
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v4, v7, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1867
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshCheckState()V

    .line 1869
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 1870
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "pick-min-item"

    const/4 v4, -0x1

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1871
    .local v1, "pickCount":I
    if-le v1, v5, :cond_0

    .line 1872
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0116

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1873
    .local v2, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1875
    .end local v2    # "text":Ljava/lang/CharSequence;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 1876
    return-void
.end method

.method private updateConfirmMenu()V
    .locals 10

    .prologue
    .line 2432
    const/4 v5, 0x0

    .line 2433
    .local v5, "name":Ljava/lang/String;
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 2434
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedMediaSetMap()Ljava/util/HashMap;

    move-result-object v8

    .line 2436
    .local v8, "selectedSetMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    const/4 v4, 0x0

    .line 2437
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v7, 0x0

    .line 2439
    .local v7, "selectedMediaSetSize":I
    if-nez v8, :cond_1

    .line 2470
    :cond_0
    :goto_0
    return-void

    .line 2442
    :cond_1
    invoke-virtual {v8}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v9

    invoke-interface {v9}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2444
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-eqz v9, :cond_2

    .line 2445
    add-int/lit8 v7, v7, 0x1

    .line 2446
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2447
    .local v6, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v9, v6, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v9, :cond_2

    move-object v4, v6

    .line 2448
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    goto :goto_1

    .line 2453
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    .end local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    const/4 v9, 0x1

    if-gt v7, v9, :cond_0

    .line 2456
    if-eqz v3, :cond_0

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v9

    if-nez v9, :cond_0

    .line 2457
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2458
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v2, :cond_4

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v2    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v9

    if-nez v9, :cond_4

    .line 2459
    if-eqz v4, :cond_5

    .line 2460
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v5

    .line 2462
    :cond_5
    if-eqz v5, :cond_6

    .line 2463
    const-string v9, "/"

    invoke-virtual {v5, v9}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-virtual {v5, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 2465
    :cond_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v9, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateConfirm(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateCountOnActionBar()V
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1964
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    .line 1965
    .local v4, "selectedCount":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllCount()I

    move-result v0

    .line 1967
    .local v0, "allItemsCount":I
    if-nez v0, :cond_0

    .line 1968
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    .line 1970
    .local v5, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v5, :cond_0

    .line 1971
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v0

    .line 1974
    .end local v5    # "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1975
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v8, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1977
    if-gtz v0, :cond_4

    move v2, v6

    .line 1979
    .local v2, "isAllSelected":Z
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v8

    sget-object v9, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v8, v9, :cond_1

    .line 1980
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1981
    .local v1, "intent":Landroid/content/Intent;
    const-string v8, "pick-max-item"

    const/4 v9, -0x1

    invoke-virtual {v1, v8, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1982
    .local v3, "pickMaxCount":I
    if-le v0, v3, :cond_1

    if-lez v3, :cond_1

    .line 1983
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    if-ne v8, v3, :cond_6

    move v2, v7

    .line 1986
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "pickMaxCount":I
    :cond_1
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v9, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v8, v9, v6, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1988
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableEAM()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1989
    if-eq v4, v7, :cond_2

    if-ne v4, v0, :cond_7

    .line 1990
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 1995
    :cond_3
    :goto_2
    return-void

    .line 1977
    .end local v2    # "isAllSelected":Z
    :cond_4
    if-ne v4, v0, :cond_5

    move v2, v7

    goto :goto_0

    :cond_5
    move v2, v6

    goto :goto_0

    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local v2    # "isAllSelected":Z
    .restart local v3    # "pickMaxCount":I
    :cond_6
    move v2, v6

    .line 1983
    goto :goto_1

    .line 1991
    .end local v1    # "intent":Landroid/content/Intent;
    .end local v3    # "pickMaxCount":I
    :cond_7
    if-nez v4, :cond_3

    .line 1992
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    goto :goto_2
.end method

.method private updateSelectionMode()V
    .locals 2

    .prologue
    .line 2759
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_0

    .line 2760
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2761
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 2762
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v1, :cond_0

    .line 2763
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    .line 2766
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    return-void
.end method

.method private updateShowContentToDisplayMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    .line 1131
    :cond_0
    if-eqz p1, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v0, :cond_1

    .line 1132
    const v1, 0x7f0f0289

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowContentToDisplay:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v1, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1136
    :cond_1
    return-void

    .line 1132
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateSortByOptionMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0291

    const/4 v5, 0x1

    const v4, 0x7f0f0290

    const/4 v3, 0x0

    .line 1139
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFunctionSimplification:Z

    if-eqz v1, :cond_0

    .line 1153
    :goto_0
    return-void

    .line 1140
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_1

    .line 1141
    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 1144
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v2, "sort_by_type_time"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 1146
    .local v0, "type":I
    if-nez v0, :cond_2

    .line 1147
    invoke-static {p1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1148
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 1150
    :cond_2
    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1151
    invoke-static {p1, v6, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method


# virtual methods
.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2909
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2910
    .local v2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v11, :cond_0

    .line 2911
    sget-object v11, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v16, "mSelectionModeProxy is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2959
    .end local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v3, "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 2915
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    .line 2916
    .local v10, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez v10, :cond_1

    .line 2917
    sget-object v11, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v16, "mediaList is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 2918
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 2921
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 2924
    .local v9, "mediaCount":I
    const/4 v11, 0x1

    if-ne v9, v11, :cond_4

    .line 2925
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2926
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v11, v6, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v11, :cond_2

    .line 2927
    check-cast v6, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v6

    .line 2929
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    if-eqz v6, :cond_3

    .line 2930
    new-instance v11, Lcom/sec/samsung/gallery/controller/DetailModel;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v2

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    :goto_1
    move-object v3, v2

    .line 2959
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 2933
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const-string v7, ""

    .line 2934
    .local v7, "line1":Ljava/lang/String;
    const-string v8, ""

    .line 2935
    .local v8, "line2":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 2936
    .local v14, "sizeSummary":J
    const-wide/16 v12, 0x0

    .line 2939
    .local v12, "size":D
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v9, :cond_6

    .line 2940
    :try_start_0
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2941
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v11, v16, v18

    if-lez v11, :cond_5

    .line 2942
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    add-long v14, v14, v16

    .line 2939
    :cond_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 2945
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v4

    .line 2946
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 2949
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_6
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v12

    .line 2950
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v16, 0x7f0e01d0

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 2952
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e01d1

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, ": "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatItemsSelectedSize(Landroid/content/Context;D)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 2955
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2956
    const-wide/16 v16, 0x0

    cmpl-double v11, v12, v16

    if-lez v11, :cond_3

    .line 2957
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method public getContentsForFaceTag()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2410
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2412
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    .line 2413
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    .line 2414
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2415
    .local v3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSelectedMediaSetMap()Ljava/util/HashMap;

    move-result-object v5

    .line 2416
    .local v5, "selectedSetMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-virtual {v5}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 2417
    .local v1, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 2418
    .local v4, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v6, v4, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v6, :cond_0

    .line 2419
    check-cast v4, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .end local v4    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 2422
    .end local v1    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_2

    .line 2423
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2425
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2428
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/ClusterAlbum;>;"
    .end local v5    # "selectedSetMap":Ljava/util/HashMap;, "Ljava/util/HashMap<Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/Integer;>;"
    :cond_3
    return-object v0
.end method

.method public getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 2591
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 2592
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 2593
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_1

    .line 2594
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v1, :cond_0

    .line 2595
    check-cast v0, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v0

    .line 2602
    :goto_0
    return-object v0

    .line 2597
    .restart local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_0

    .line 2602
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1

    .prologue
    .line 2674
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method protected handleFilter()V
    .locals 5

    .prologue
    .line 2078
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->cancelSwitchFilterTask()V

    .line 2079
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v0

    .line 2080
    .local v0, "viewByType":I
    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$18;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$18;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 2118
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v3, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getTopFilterMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2119
    return-void
.end method

.method protected isAvailableCount(IZZ)Z
    .locals 10
    .param p1, "selected"    # I
    .param p2, "checkMin"    # Z
    .param p3, "showToast"    # Z

    .prologue
    const v9, 0x7f0e0117

    const/4 v7, -0x1

    const/4 v8, 0x0

    const/4 v4, 0x1

    .line 2624
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2625
    .local v0, "intent":Landroid/content/Intent;
    const/4 v3, 0x0

    .line 2627
    .local v3, "text":Ljava/lang/CharSequence;
    const/4 v1, 0x1

    .line 2629
    .local v1, "isAvailable":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v5

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v5, v6, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-nez v5, :cond_0

    .line 2662
    :goto_0
    return v4

    .line 2632
    :cond_0
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromInsideGallery:Z

    if-eqz v5, :cond_3

    .line 2633
    const/4 v1, 0x1

    .line 2659
    :cond_1
    :goto_1
    if-nez v1, :cond_2

    if-eqz p3, :cond_2

    .line 2660
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    :cond_2
    move v4, v1

    .line 2662
    goto :goto_0

    .line 2635
    :cond_3
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-nez v5, :cond_6

    .line 2636
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    if-eqz v5, :cond_5

    .line 2637
    const-string v5, "select_num_key"

    invoke-virtual {v0, v5, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2640
    .local v2, "pickCount":I
    :goto_2
    if-eq v2, v7, :cond_4

    if-le p1, v2, :cond_4

    .line 2641
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v5, v9, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2642
    const/4 v1, 0x0

    .line 2644
    :cond_4
    if-eqz p2, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isPenSelectionMode()Z

    move-result v5

    if-nez v5, :cond_1

    .line 2645
    const-string v5, "pick-min-item"

    invoke-virtual {v0, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2646
    if-ge p1, v2, :cond_1

    if-le v2, v4, :cond_1

    .line 2647
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0116

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v4, v8

    invoke-virtual {v5, v6, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2648
    const/4 v1, 0x0

    goto :goto_1

    .line 2639
    .end local v2    # "pickCount":I
    :cond_5
    const-string v5, "pick-max-item"

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .restart local v2    # "pickCount":I
    goto :goto_2

    .line 2652
    .end local v2    # "pickCount":I
    :cond_6
    const/16 v5, 0x3e8

    if-le p1, v5, :cond_1

    .line 2653
    const/16 v2, 0x3e8

    .line 2654
    .restart local v2    # "pickCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v8

    invoke-virtual {v5, v9, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 2655
    const/4 v1, 0x0

    goto/16 :goto_1
.end method

.method protected isAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2607
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-nez v3, :cond_1

    .line 2620
    :cond_0
    :goto_0
    return v1

    .line 2611
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2612
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0107

    invoke-static {v1, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v1, v2

    .line 2613
    goto :goto_0

    .line 2616
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    add-int/lit8 v0, v3, 0x1

    .line 2617
    .local v0, "count":I
    invoke-virtual {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableCount(IZZ)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 2618
    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1173
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isShowingHelpView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1174
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->removeQuickHelpView()V

    .line 1217
    :goto_0
    return-void

    .line 1178
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_1

    .line 1179
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mTimeEventHandle:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->onBackPressed()V

    goto :goto_0

    .line 1180
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1181
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 1182
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1184
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1185
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSlideShowMode(Z)V

    .line 1186
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setLaunchMode()V

    goto :goto_0

    .line 1188
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1189
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v0, v1, :cond_7

    .line 1190
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1191
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 1193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSlideShowMode(Z)V

    .line 1194
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1195
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setLaunchMode()V

    .line 1196
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    goto :goto_0

    .line 1199
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->finishCurrentViewState()V

    goto :goto_0

    .line 1201
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 1202
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSlideShowMode(Z)V

    .line 1204
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 1205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1206
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setLaunchMode()V

    .line 1207
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v0, v1, :cond_9

    .line 1208
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 1209
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 1210
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v1, 0x7f0e0073

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 1212
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1213
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    goto/16 :goto_0

    .line 1215
    :cond_a
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->finishCurrentViewState()V

    goto/16 :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    const/4 v2, 0x0

    .line 2474
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2476
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f0288

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 2485
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2486
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mShrinkAnim:Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->stop()V

    .line 2487
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshSelectionBarState(Z)V

    .line 2490
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_1

    .line 2491
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 2493
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_2

    .line 2494
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 2495
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 2499
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_3

    .line 2500
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 2502
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isShowingHelpView()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2503
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetQuickHelpView()V

    .line 2506
    :cond_4
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 464
    const-string v3, "Gallery_Performance"

    const-string v6, "TimeViewState onCreate Start"

    invoke-static {v3, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    sget-object v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "onCreate  = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 467
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v3, :cond_0

    .line 468
    sget-object v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v4, "mActivity is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    :goto_0
    return-void

    .line 471
    :cond_0
    new-instance v6, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v6, v3}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    .line 472
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hidePreDisplayScreen()V

    .line 473
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 474
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 475
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 476
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 477
    new-instance v3, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v6, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 478
    new-instance v3, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v3, v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 479
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getRefreshState(Landroid/os/Bundle;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRefreshView:Z

    .line 480
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->initTimeViewEventHandler()V

    .line 481
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGalleryWidget(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    .line 482
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromInsideGallery(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromInsideGallery:Z

    .line 483
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFromGifMaker(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGifMaker:Z

    .line 484
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 485
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v6, 0x7f0f00bf

    invoke-virtual {v3, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 486
    iput-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 487
    iput-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 488
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_1
    move v3, v5

    :goto_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    .line 489
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 490
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 491
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle()V

    .line 492
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewExitSelectionMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 494
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v6, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v3, v6, :cond_3

    .line 496
    :cond_2
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    .line 497
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 498
    .local v0, "intent":Landroid/content/Intent;
    const-string v3, "pick-max-item"

    const/4 v6, -0x1

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 499
    .local v2, "pickMaxCount":I
    if-gtz v2, :cond_3

    .line 500
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFromGalleryWidget:Z

    if-eqz v3, :cond_8

    .line 501
    const-string v3, "pick-max-item"

    const/16 v6, 0x3e8

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 507
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "pickMaxCount":I
    :cond_3
    :goto_2
    if-eqz p1, :cond_4

    .line 508
    const-string v3, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaSetIndex:I

    .line 511
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getDefaultPath()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 513
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v3

    if-ne v3, v5, :cond_5

    .line 514
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    const/high16 v6, 0x100000

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    .line 517
    :cond_5
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdatePath:Z

    .line 518
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-nez v3, :cond_9

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 521
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_3
    new-instance v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;

    .line 522
    new-instance v3, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$DataLoaderConfig;

    const/4 v8, 0x2

    invoke-direct {v3, v6, v1, v7, v8}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .line 523
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 524
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    invoke-virtual {v3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setEasyMode(Z)V

    .line 526
    new-instance v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    .line 527
    if-eqz p1, :cond_6

    .line 528
    const-string v3, "album-pick"

    invoke-virtual {p1, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsAlbumPick:Z

    .line 531
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v3, v6, :cond_a

    .line 532
    iput v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    .line 543
    :goto_4
    const-string v3, "Gallery_Performance"

    const-string v4, "TimeViewState onCreate End"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_7
    move v3, v4

    .line 488
    goto/16 :goto_1

    .line 503
    .restart local v0    # "intent":Landroid/content/Intent;
    .restart local v2    # "pickMaxCount":I
    :cond_8
    const-string v3, "pick-max-item"

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_2

    .line 518
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v2    # "pickMaxCount":I
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    goto :goto_3

    .line 534
    .restart local v1    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_a
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    goto :goto_4
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1082
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMenu:Landroid/view/Menu;

    .line 1083
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 1084
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1062
    const-string v0, "Gallery_Performance"

    const-string v1, "TimeViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1063
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->onStop()V

    .line 1071
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v0, v1, :cond_1

    .line 1072
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    .line 1076
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "ALBUM_VIEW_EXIT_SELECTION"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1077
    const-string v0, "Gallery_Performance"

    const-string v1, "TimeViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    return-void

    .line 1074
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    goto :goto_0
.end method

.method public onDirty()V
    .locals 1

    .prologue
    .line 2510
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onDirty(Z)V

    .line 2511
    return-void
.end method

.method public onDirty(Z)V
    .locals 2
    .param p1, "isExitSelectionMode"    # Z

    .prologue
    .line 2516
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-nez v1, :cond_0

    .line 2535
    :goto_0
    return-void

    .line 2520
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 2521
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 2522
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isNeedUpdateCluster()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2523
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz p1, :cond_1

    .line 2524
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    :cond_1
    move-object v1, v0

    .line 2526
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    .line 2530
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    if-eqz v1, :cond_3

    .line 2531
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2533
    :cond_3
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setDisplaySearchIcon(Ljava/lang/Boolean;)V

    goto :goto_0
.end method

.method public onMWLayoutChanged()V
    .locals 2

    .prologue
    .line 2586
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    .line 2587
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 1158
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 1159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 1160
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_3

    .line 1161
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v0, :cond_4

    .line 1162
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onBackPressed()V

    .line 1169
    :cond_3
    :goto_0
    return-void

    .line 1164
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 1017
    const-string v1, "Gallery_Performance"

    const-string v2, "TimeViewState onPause Start"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 1020
    .local v0, "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    if-eqz v1, :cond_5

    .line 1021
    check-cast v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    .end local v0    # "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause(Z)V

    .line 1026
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v1, :cond_0

    .line 1027
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->onPause()V

    .line 1033
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1034
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOverlayHelp:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->isShowingHelpView()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1035
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->removeQuickHelpView()V

    .line 1037
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v1, :cond_2

    .line 1038
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->saveCurrentScrollInfo()V

    .line 1039
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->pause()V

    .line 1042
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v1, v2, :cond_6

    .line 1043
    const/4 v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    .line 1047
    :goto_1
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShrinkOption:I

    .line 1048
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "ALBUM_VIEW"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1049
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "ALBUM_VIEW_MEDIA_EJECT"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1050
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    if-eqz v1, :cond_3

    .line 1051
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setGenericMotionFocus(I)V

    .line 1052
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setGenericMotionTitleFocus(I)V

    .line 1054
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableEAM()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1055
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 1056
    :cond_4
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 1057
    const-string v1, "Gallery_Performance"

    const-string v2, "TimeViewState onPause End"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1058
    return-void

    .line 1023
    .restart local v0    # "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    goto/16 :goto_0

    .line 1045
    .end local v0    # "actionbarView":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    :cond_6
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mViewMode:I

    goto :goto_1
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1088
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1090
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateSortByOptionMenu(Landroid/view/Menu;)V

    .line 1091
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsDeleteMode:Z

    if-eqz v4, :cond_2

    .line 1092
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 1093
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_5

    .line 1094
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1095
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f027b

    if-ne v4, v5, :cond_1

    .line 1096
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1097
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1098
    if-eqz v1, :cond_0

    .line 1099
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1093
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1101
    :cond_0
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1103
    :cond_1
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 1106
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_2
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v4, :cond_5

    .line 1107
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 1108
    .restart local v1    # "isSelected":Z
    const/4 v0, 0x0

    .restart local v0    # "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .restart local v3    # "n":I
    :goto_2
    if-ge v0, v3, :cond_5

    .line 1109
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 1110
    .restart local v2    # "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f02ce

    if-ne v4, v5, :cond_4

    .line 1111
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1112
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 1113
    if-eqz v1, :cond_3

    .line 1114
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 1108
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1116
    :cond_3
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1118
    :cond_4
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_3

    .line 1123
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v4, :cond_6

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v4, v5, :cond_6

    .line 1124
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 1126
    :cond_6
    return-void
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 2691
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-eqz v0, :cond_0

    .line 2692
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->refreshView(Z)V

    .line 2693
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 13

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v12, 0x1

    .line 892
    const/4 v6, -0x1

    .line 896
    .local v6, "initialCode":I
    const-string v0, "Gallery_Performance"

    const-string v3, "TimeViewState onResume Start"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 898
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mNeedIdleProcess:Z

    .line 899
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isFestivalMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 900
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->setCurrentClusterType(I)V

    .line 903
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableEAM()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 904
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    .line 905
    :cond_1
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mFirstMediaCheck:Z

    .line 906
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v3, "sort_by_type_time"

    invoke-static {v0, v3, v1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v11

    .line 909
    .local v11, "type":I
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdatePath:Z

    if-eqz v0, :cond_c

    .line 910
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getDefaultPath()Ljava/lang/String;

    move-result-object v10

    .line 911
    .local v10, "newPath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 912
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    .line 925
    .end local v10    # "newPath":Ljava/lang/String;
    .local v9, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    if-eqz v9, :cond_2

    .line 926
    invoke-virtual {v9, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->setSortByType(I)V

    .line 928
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_3
    move v0, v12

    :goto_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    .line 929
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setEasyMode(Z)V

    .line 930
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isLocaleRTL()Z

    move-result v7

    .line 931
    .local v7, "isRTL":Z
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mIsRTL:Z

    .line 932
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setRTL(Z)V

    .line 933
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v0, :cond_e

    move v8, v12

    .line 937
    .local v8, "level":I
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsEasyMode:Z

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->setInitalLevel(ZI)V
    invoke-static {v0, v3, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->access$5600(Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;ZI)V

    .line 939
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerGrid;->getThumbSizeType(I)I

    move-result v11

    .line 940
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setInitThumbType(I)V

    .line 942
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->setIsPhotoPage(Z)V

    .line 945
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_CATEGORY:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v0, v3, :cond_10

    .line 946
    iput v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mChangeViewMode:I

    .line 950
    :goto_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "SLIDESHOW_SETTINGS"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 951
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 953
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    if-eqz v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_6

    .line 954
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_6

    .line 955
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 959
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setLaunchMode()V

    .line 960
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_11

    .line 961
    const/4 v4, 0x1

    .line 966
    .local v4, "option":I
    :goto_4
    or-int/lit16 v4, v4, 0x80

    .line 967
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_12

    .line 968
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v6

    .line 969
    if-ne v6, v2, :cond_7

    .line 970
    sget-object v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialCode = -1!!, re-calculate initialCode with albumIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", itemIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumItemIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumIndex:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumItemIndex:I

    or-int v6, v0, v1

    .line 973
    :cond_7
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShrinkOption:I

    or-int/2addr v4, v0

    .line 977
    :goto_5
    if-ltz v6, :cond_8

    .line 978
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setFirstIndex(I)V

    .line 980
    :cond_8
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->setFirstLoding(I)V

    .line 981
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 982
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 983
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->onResume()V

    .line 984
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 985
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumViewMediaEjectMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 987
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 988
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSlideShowMode(Z)V

    .line 990
    :cond_9
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->onDirty()V

    .line 992
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 994
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkViewStateByFilterBySetting(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 996
    const-string v0, "Gallery_Performance"

    const-string v1, "TimeViewState onResume End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 997
    return-void

    .line 914
    .end local v4    # "option":I
    .end local v7    # "isRTL":Z
    .end local v8    # "level":I
    .end local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v10    # "newPath":Ljava/lang/String;
    :cond_a
    iput-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 915
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    .line 917
    .restart local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    goto/16 :goto_0

    .line 915
    .end local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    goto :goto_6

    .line 921
    .end local v10    # "newPath":Ljava/lang/String;
    :cond_c
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    .line 922
    .restart local v9    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mUpdatePath:Z

    goto/16 :goto_0

    :cond_d
    move v0, v1

    .line 928
    goto/16 :goto_1

    .line 933
    .restart local v7    # "isRTL":Z
    :cond_e
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v5, "timeViewColCnt"

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x2

    :goto_7
    invoke-static {v3, v5, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v8

    goto/16 :goto_2

    :cond_f
    move v0, v12

    goto :goto_7

    .line 948
    .restart local v8    # "level":I
    :cond_10
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mChangeViewMode:I

    goto/16 :goto_3

    .line 964
    :cond_11
    const/4 v4, 0x0

    .restart local v4    # "option":I
    goto/16 :goto_4

    .line 975
    :cond_12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;

    iget v6, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$ComposeViewConfig;->mPrevCenterObject:I

    goto/16 :goto_5
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2539
    sparse-switch p1, :sswitch_data_0

    .line 2576
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 2546
    :sswitch_1
    invoke-direct {p0, p3, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleResultCameraLaunchForSinglePick(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2550
    :sswitch_2
    invoke-direct {p0, p3, p2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleResultWallpaper(Landroid/content/Intent;I)V

    goto :goto_0

    .line 2553
    :sswitch_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 2554
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    if-eqz p2, :cond_1

    .line 2555
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v2, v5

    aput-object v0, v2, v4

    sget-object v3, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->ASSIGN_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    aput-object v3, v2, v6

    aput-object p3, v2, v7

    .line 2558
    .local v2, "sendParams":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v3

    const-string v4, "START_FACE_TAG"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2561
    .end local v2    # "sendParams":[Ljava/lang/Object;
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    goto :goto_0

    .line 2564
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :sswitch_4
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    .line 2565
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v4, 0x905

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    .line 2566
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    goto :goto_0

    .line 2570
    :sswitch_5
    new-array v1, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v5

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v4

    aput-object p3, v1, v6

    .line 2573
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 2539
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_5
        0x403 -> :sswitch_2
        0x901 -> :sswitch_0
        0x902 -> :sswitch_1
        0x903 -> :sswitch_1
        0x904 -> :sswitch_3
        0x905 -> :sswitch_4
    .end sparse-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 5
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v1, 0x0

    .line 590
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 591
    .local v0, "glComposeView":Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    sget-object v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "initialzieView = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 595
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$8;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$8;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V

    .line 628
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$9;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$9;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V

    .line 656
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$10;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V

    .line 673
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$11;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$11;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 699
    const/4 v2, 0x2

    new-instance v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$12;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$12;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 738
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$13;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$13;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;)V

    .line 759
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$14;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$14;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnHoverListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;)V

    .line 771
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$15;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$15;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnSwitchViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;)V

    .line 794
    new-instance v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$16;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$16;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V

    .line 851
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v2, v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 853
    .local v1, "isPickerMode":Z
    :cond_1
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 2
    .param p1, "hasFocus"    # Z

    .prologue
    .line 2679
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableEAM()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2687
    :goto_0
    return-void

    .line 2682
    :cond_0
    if-eqz p1, :cond_1

    .line 2683
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->registerEAM(Landroid/content/BroadcastReceiver;)V

    goto :goto_0

    .line 2685
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    goto :goto_0
.end method

.method protected prePenSelectionRemoveItem(II)Z
    .locals 10
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v5, 0x0

    .line 1941
    const/4 v2, 0x0

    .line 1942
    .local v2, "remove":Z
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1943
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    invoke-virtual {v4, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1944
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 1946
    .local v3, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    move v4, v5

    .line 1960
    :goto_0
    return v4

    .line 1950
    :cond_1
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1951
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v6

    const v7, 0x7f0e029e

    new-array v8, v9, [Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v8, v5

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1952
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1953
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v5, 0x4e20

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1954
    :cond_2
    invoke-virtual {v3, v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1955
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1956
    const/4 v2, 0x1

    .line 1958
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    invoke-virtual {p0, v5, v9, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableCount(IZZ)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 1959
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    move v4, v2

    .line 1960
    goto :goto_0
.end method

.method protected selectItem(II)V
    .locals 16
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 1879
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move/from16 v0, p1

    invoke-virtual {v11, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v7

    .line 1880
    .local v7, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v11, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 1881
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 1883
    .local v8, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-eqz v7, :cond_0

    if-nez v6, :cond_1

    .line 1938
    :cond_0
    :goto_0
    return-void

    .line 1887
    :cond_1
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 1888
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v12

    const v13, 0x7f0e029e

    const/4 v11, 0x1

    new-array v14, v11, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object v11, v6

    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1889
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 1890
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v12, 0x4e20

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1891
    :cond_2
    invoke-virtual {v8, v7, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1892
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1936
    :goto_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v12

    const/4 v13, 0x1

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v14}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableCount(IZZ)Z

    move-result v12

    invoke-virtual {v11, v12}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    .line 1937
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 1894
    :cond_3
    instance-of v11, v6, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v11, :cond_4

    instance-of v11, v6, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v11, :cond_4

    instance-of v11, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v11, :cond_4

    instance-of v11, v6, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v11, :cond_7

    :cond_4
    const/4 v4, 0x1

    .line 1896
    .local v4, "isHide":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isHideItemInSelectionMode()Z

    move-result v11

    if-eqz v11, :cond_8

    instance-of v11, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v11, :cond_5

    move-object v11, v6

    check-cast v11, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isHideBlockedItem()Z

    move-result v11

    if-nez v11, :cond_6

    :cond_5
    if-eqz v4, :cond_8

    :cond_6
    const/4 v5, 0x1

    .local v5, "isHideBlockedItem":Z
    :goto_3
    move-object v11, v6

    .line 1899
    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->isAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v11

    if-eqz v11, :cond_0

    if-nez v5, :cond_0

    .line 1903
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 1904
    .local v3, "intent":Landroid/content/Intent;
    const-string v11, "VIDEO_CLIP_MULTIPIC"

    const/4 v12, 0x0

    invoke-virtual {v3, v11, v12}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 1905
    .local v10, "videoClip":Z
    if-eqz v10, :cond_9

    .line 1906
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 1907
    .local v2, "currentSelectedCount":I
    const/16 v11, 0xf

    if-lt v2, v11, :cond_a

    .line 1908
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e02ef

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/16 v15, 0xf

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 1911
    .local v9, "text":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1894
    .end local v2    # "currentSelectedCount":I
    .end local v3    # "intent":Landroid/content/Intent;
    .end local v4    # "isHide":Z
    .end local v5    # "isHideBlockedItem":Z
    .end local v9    # "text":Ljava/lang/CharSequence;
    .end local v10    # "videoClip":Z
    :cond_7
    const/4 v4, 0x0

    goto :goto_2

    .line 1896
    .restart local v4    # "isHide":Z
    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    .line 1914
    .restart local v3    # "intent":Landroid/content/Intent;
    .restart local v5    # "isHideBlockedItem":Z
    .restart local v10    # "videoClip":Z
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    if-eqz v11, :cond_a

    .line 1915
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 1916
    .restart local v2    # "currentSelectedCount":I
    const/16 v11, 0x14

    if-lt v2, v11, :cond_a

    .line 1917
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0e0117

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    const/16 v15, 0x14

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 1920
    .restart local v9    # "text":Ljava/lang/CharSequence;
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v9}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1925
    .end local v2    # "currentSelectedCount":I
    .end local v9    # "text":Ljava/lang/CharSequence;
    :cond_a
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inDeleteSelectionMode()Z

    move-result v11

    if-eqz v11, :cond_b

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->checkDeletableMediaObject(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-nez v11, :cond_b

    .line 1926
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v12, 0x7f0e0107

    invoke-static {v11, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto/16 :goto_0

    .line 1930
    :cond_b
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v12

    const v13, 0x7f0e029d

    const/4 v11, 0x1

    new-array v14, v11, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object v11, v6

    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v14, v15

    invoke-virtual {v12, v13, v14}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1931
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v11

    if-eqz v11, :cond_c

    .line 1932
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v12, 0x4e2a

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1933
    :cond_c
    invoke-virtual {v8, v7, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1934
    invoke-virtual {v8, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto/16 :goto_1
.end method

.method public setPromptRequest(IILjava/lang/Object;)V
    .locals 5
    .param p1, "code"    # I
    .param p2, "parm"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    .line 857
    move-object v0, p3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 858
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v1, 0x0

    .line 859
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v3, :cond_1

    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 861
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceImagePath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 862
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    :goto_0
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 866
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z

    .line 867
    const/4 v2, 0x1

    if-ne p2, v2, :cond_2

    const/16 v2, 0x20

    :goto_2
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShrinkOption:I

    .line 869
    return-void

    :cond_0
    move-object v2, v0

    .line 862
    goto :goto_0

    .line 864
    :cond_1
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_1

    .line 867
    :cond_2
    const/4 v2, 0x2

    if-ne p2, v2, :cond_3

    const/16 v2, 0x10

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public setPromptRequest(IILjava/lang/Object;II)V
    .locals 5
    .param p1, "code"    # I
    .param p2, "parm"    # I
    .param p3, "data"    # Ljava/lang/Object;
    .param p4, "albumSetIndex"    # I
    .param p5, "albumItemIndex"    # I

    .prologue
    .line 872
    move-object v0, p3

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 873
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v1, 0x0

    .line 874
    .local v1, "path":Lcom/sec/android/gallery3d/data/Path;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v2, v3, :cond_1

    if-eqz v0, :cond_1

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_1

    move-object v2, v0

    .line 876
    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/LocalImage;->getFaceImagePath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 877
    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    :goto_0
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 881
    :goto_1
    iput p4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumIndex:I

    .line 882
    iput p5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mAlbumItemIndex:I

    .line 884
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z

    .line 885
    const/4 v2, 0x1

    if-ne p2, v2, :cond_2

    const/16 v2, 0x20

    :goto_2
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mShrinkOption:I

    .line 887
    return-void

    :cond_0
    move-object v2, v0

    .line 877
    goto :goto_0

    .line 879
    :cond_1
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    goto :goto_1

    .line 885
    :cond_2
    const/4 v2, 0x2

    if-ne p2, v2, :cond_3

    const/16 v2, 0x10

    goto :goto_2

    :cond_3
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 11
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 1221
    move-object v2, p2

    check-cast v2, Lcom/sec/samsung/gallery/core/Event;

    .line 1222
    .local v2, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v3

    .line 1224
    .local v3, "eventType":I
    sget-object v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Event update ["

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "]"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v3, v6, :cond_1

    .line 1227
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsDeleteMode:Z

    .line 1228
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode()V

    .line 1229
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    .line 1230
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 1231
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEAMReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;->unregisterEAM(Landroid/content/BroadcastReceiver;)V

    .line 1325
    :cond_0
    :goto_0
    return-void

    .line 1232
    :cond_1
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    if-ne v3, v6, :cond_2

    .line 1233
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->showDeleteDialog()V

    goto :goto_0

    .line 1234
    :cond_2
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v3, v6, :cond_3

    .line 1235
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->selectAll()V

    goto :goto_0

    .line 1236
    :cond_3
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v3, v6, :cond_4

    .line 1237
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->unselectAll()V

    goto :goto_0

    .line 1238
    :cond_4
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    if-ne v3, v6, :cond_5

    .line 1239
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->exitSelectionMode()V

    goto :goto_0

    .line 1240
    :cond_5
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    if-ne v3, v6, :cond_6

    .line 1241
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "SHOW_IMPORT_DIALOG"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const/4 v10, 0x0

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 1242
    :cond_6
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    if-ne v3, v6, :cond_8

    .line 1243
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1244
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0066

    invoke-static {v6, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 1247
    :cond_7
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->startSlideshowSettings()V

    goto :goto_0

    .line 1248
    :cond_8
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v3, v6, :cond_9

    .line 1249
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1251
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 1252
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto :goto_0

    .line 1254
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_9
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-ne v3, v6, :cond_a

    .line 1255
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1257
    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 1258
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x1

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 1260
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_a
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_CAMERA_MODE:I

    if-ne v3, v6, :cond_b

    .line 1261
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "START_CAMERA"

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v7, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1262
    :cond_b
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v3, v6, :cond_c

    .line 1263
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode()V

    .line 1264
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    .line 1265
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    goto/16 :goto_0

    .line 1266
    :cond_c
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    if-ne v3, v6, :cond_d

    .line 1267
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleHiddenAlbumsLaunch()V

    goto/16 :goto_0

    .line 1268
    :cond_d
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_OLDEST:I

    if-ne v3, v6, :cond_e

    .line 1269
    const/4 v6, 0x1

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleSortByAction(I)V

    goto/16 :goto_0

    .line 1270
    :cond_e
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_LATEST:I

    if-ne v3, v6, :cond_f

    .line 1271
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->handleSortByAction(I)V

    goto/16 :goto_0

    .line 1272
    :cond_f
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_STORY_ALBUM:I

    if-ne v3, v6, :cond_10

    .line 1273
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode()V

    .line 1274
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    .line 1275
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 1276
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1277
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForStoryAlbum;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForStoryAlbum;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1278
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 1279
    :cond_10
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_MAKE_COLLAGE:I

    if-ne v3, v6, :cond_11

    .line 1280
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode()V

    .line 1281
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    .line 1282
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterEventSelectionMode()V

    .line 1283
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1284
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v7, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForCollage;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1285
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto/16 :goto_0

    .line 1286
    :cond_11
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    if-ne v3, v6, :cond_12

    .line 1287
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1288
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRefreshView:Z

    goto/16 :goto_0

    .line 1289
    :cond_12
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_GALLERY_SEARCH_MODE:I

    if-ne v3, v6, :cond_13

    .line 1290
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    const-class v7, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    const/4 v8, 0x0

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1291
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mRefreshView:Z

    goto/16 :goto_0

    .line 1292
    :cond_13
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_SCAN_NEARBY_DEVICES:I

    if-ne v3, v6, :cond_14

    .line 1294
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->scanForNearbyProviderDevices(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1295
    :catch_0
    move-exception v1

    .line 1296
    .local v1, "e":Ljava/lang/NullPointerException;
    sget-object v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->TAG:Ljava/lang/String;

    const-string v7, "NullPointer Exception, maybe nearby device is not initiated yet"

    invoke-static {v6, v7, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 1298
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :cond_14
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v3, v6, :cond_15

    .line 1299
    const/4 v6, 0x1

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    .line 1300
    .local v5, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "LAST_SHARE_APP"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1301
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_15
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    if-ne v3, v6, :cond_16

    .line 1302
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 1303
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 1305
    .restart local v5    # "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "START_CATEGORY_TAG"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1306
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_16
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_EDIT_CATEGORY:I

    if-ne v3, v6, :cond_17

    .line 1307
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 1308
    .restart local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v6, 0x5

    new-array v5, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->EDIT_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    aput-object v7, v5, v6

    const/4 v6, 0x4

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    .line 1310
    .restart local v5    # "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "START_CATEGORY_TAG"

    invoke-virtual {v6, v7, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 1311
    .end local v0    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    .end local v5    # "params":[Ljava/lang/Object;
    :cond_17
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v3, v6, :cond_18

    .line 1312
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    const/4 v8, 0x7

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 1314
    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_0

    .line 1315
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mCurrentMediaSetIndex:I

    const/4 v8, 0x1

    const/4 v9, 0x3

    invoke-virtual {v6, v4, v7, v8, v9}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 1317
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_18
    sget v6, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_GIF:I

    if-ne v3, v6, :cond_0

    .line 1318
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    .line 1319
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsPickerMode:Z

    .line 1320
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->scaleAnimationForSelectionMode()V

    .line 1321
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->enterSelectionMode(Z)V

    .line 1322
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->updateCountOnActionBar()V

    .line 1323
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mIsGifMode:Z

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setGifMode(Z)V

    goto/16 :goto_0
.end method
