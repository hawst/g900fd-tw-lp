.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;
.super Ljava/lang/Object;
.source "HelpDetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFilmIndexChanged(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$102(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)I

    .line 145
    return-void
.end method

.method public onLoadingFinished(Z)V
    .locals 2
    .param p1, "loadingFailed"    # Z

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$400(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->hideAndReleaseSpinner(Landroid/app/Activity;)V

    .line 114
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 116
    .local v0, "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 124
    .end local v0    # "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    :goto_0
    return-void

    .line 121
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->finishDetailView()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$600(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    goto :goto_0
.end method

.method public onLoadingStarted()V
    .locals 3

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v2

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetAgifMode(ZLcom/sec/android/gallery3d/data/MediaItem;)V

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setSpinnerVisibility(Landroid/app/Activity;Z)V

    .line 130
    return-void
.end method

.method public onPhotoAvailable(Lcom/sec/android/gallery3d/data/Path;Z)V
    .locals 4
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "fullImage"    # Z

    .prologue
    .line 134
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v0

    const-wide/16 v2, 0x40

    and-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScalePrepared(Z)V

    .line 137
    :cond_1
    return-void
.end method

.method public onPhotoChanged(ILcom/sec/android/gallery3d/data/Path;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/Path;

    .prologue
    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 98
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$102(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)I

    .line 99
    if-eqz p2, :cond_1

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 101
    .local v0, "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_1

    .line 102
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 105
    .end local v0    # "photo":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    return-void
.end method

.method public onPhotoFailedToLoad()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method

.method public onPhotoRquestDone()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    sget-object v1, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;->RUN:Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    .line 150
    return-void
.end method
