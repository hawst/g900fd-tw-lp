.class final Lcom/sec/samsung/gallery/access/face/FaceInterface$1;
.super Ljava/lang/Thread;
.source "FaceInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;ZI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$cr:Landroid/content/ContentResolver;

.field final synthetic val$mediaList:Ljava/util/List;

.field final synthetic val$type:I


# direct methods
.method constructor <init>(Ljava/util/List;Landroid/content/ContentResolver;I)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$mediaList:Ljava/util/List;

    iput-object p2, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    iput p3, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$type:I

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 60
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$mediaList:Ljava/util/List;

    if-nez v7, :cond_1

    .line 64
    :try_start_0
    # getter for: Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "startFaceScan() : waiting 5 sec"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    const-wide/16 v8, 0x1388

    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->sleep(J)V

    .line 66
    # getter for: Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$000()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "startFaceScan() : going on"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    iget v8, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$type:I

    # invokes: Lcom/sec/samsung/gallery/access/face/FaceInterface;->startFaceScan(Landroid/content/ContentResolver;I)V
    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$100(Landroid/content/ContentResolver;I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 68
    :catch_0
    move-exception v0

    .line 69
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 73
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$mediaList:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 74
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$mediaList:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 75
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v7, v4, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v7, :cond_2

    .line 76
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    .line 77
    .local v5, "itemUri":Landroid/net/Uri;
    const-wide/16 v2, 0x0

    .line 79
    .local v2, "id":J
    :try_start_1
    invoke-static {v5}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-wide v2

    .line 82
    :goto_2
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-gtz v7, :cond_3

    .line 73
    .end local v2    # "id":J
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v5    # "itemUri":Landroid/net/Uri;
    :cond_2
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 85
    .restart local v2    # "id":J
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v5    # "itemUri":Landroid/net/Uri;
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    # invokes: Lcom/sec/samsung/gallery/access/face/FaceInterface;->checkFaceScanned(Landroid/content/ContentResolver;J)I
    invoke-static {v7, v2, v3}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$200(Landroid/content/ContentResolver;J)I

    move-result v6

    .line 87
    .local v6, "notScanned":I
    const/4 v7, 0x2

    if-ne v6, v7, :cond_4

    .line 89
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    # invokes: Lcom/sec/samsung/gallery/access/face/FaceInterface;->setScanPriority(Landroid/content/ContentResolver;J)V
    invoke-static {v7, v2, v3}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$300(Landroid/content/ContentResolver;J)V

    .line 91
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    long-to-int v8, v2

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalImage;->getFilePath()Ljava/lang/String;

    move-result-object v9

    # invokes: Lcom/sec/samsung/gallery/access/face/FaceInterface;->singleFaceScan(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v7, v8, v9}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$400(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 92
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    const/4 v7, 0x3

    if-ne v6, v7, :cond_2

    .line 93
    iget-object v7, p0, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;->val$cr:Landroid/content/ContentResolver;

    long-to-int v8, v2

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    # invokes: Lcom/sec/samsung/gallery/access/face/FaceInterface;->restoreSingleFaceData(Landroid/content/ContentResolver;Ljava/lang/String;)V
    invoke-static {v7, v8}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->access$500(Landroid/content/ContentResolver;Ljava/lang/String;)V

    goto :goto_3

    .line 80
    .end local v6    # "notScanned":I
    :catch_1
    move-exception v7

    goto :goto_2
.end method
