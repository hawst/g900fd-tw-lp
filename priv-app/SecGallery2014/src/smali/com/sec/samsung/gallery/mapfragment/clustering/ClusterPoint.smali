.class public Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;
.source "ClusterPoint.java"


# instance fields
.field private boundsOfInputPoints:Lcom/google/android/gms/maps/model/LatLngBounds;

.field private final pointsInClusterList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final pointsInClusterSet:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final transition:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;Z)V
    .locals 1
    .param p1, "initialPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .param p2, "projection"    # Lcom/google/android/gms/maps/Projection;
    .param p3, "transition"    # Z

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;-><init>()V

    .line 16
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    .line 17
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterSet:Ljava/util/HashSet;

    .line 24
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 25
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->transition:Z

    .line 26
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->add(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V

    .line 27
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->buildScreenPosition(Lcom/google/android/gms/maps/Projection;)V

    .line 28
    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;ZLcom/google/android/gms/maps/model/LatLng;)V
    .locals 0
    .param p1, "initialPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .param p2, "projection"    # Lcom/google/android/gms/maps/Projection;
    .param p3, "transition"    # Z
    .param p4, "overridePosition"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 31
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/google/android/gms/maps/Projection;Z)V

    .line 32
    iput-object p4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    .line 33
    return-void
.end method


# virtual methods
.method add(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V
    .locals 1
    .param p1, "point"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->boundsOfInputPoints:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 40
    return-void
.end method

.method clearScreenPosition()V
    .locals 3

    .prologue
    .line 71
    invoke-super {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->clearScreenPosition()V

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 73
    .local v1, "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->clearScreenPosition()V

    goto :goto_0

    .line 75
    .end local v1    # "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    :cond_0
    return-void
.end method

.method public containsInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z
    .locals 1
    .param p1, "point"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterSet:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method getBoundsOfInputPoints()Lcom/google/android/gms/maps/model/LatLngBounds;
    .locals 4

    .prologue
    .line 85
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->boundsOfInputPoints:Lcom/google/android/gms/maps/model/LatLngBounds;

    if-nez v3, :cond_1

    .line 86
    invoke-static {}, Lcom/google/android/gms/maps/model/LatLngBounds;->builder()Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    move-result-object v0

    .line 87
    .local v0, "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 88
    .local v2, "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->include(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/LatLngBounds$Builder;

    goto :goto_0

    .line 90
    .end local v2    # "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/LatLngBounds$Builder;->build()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->boundsOfInputPoints:Lcom/google/android/gms/maps/model/LatLngBounds;

    .line 92
    .end local v0    # "builder":Lcom/google/android/gms/maps/model/LatLngBounds$Builder;
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->boundsOfInputPoints:Lcom/google/android/gms/maps/model/LatLngBounds;

    return-object v3
.end method

.method public bridge synthetic getMapPosition()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 14
    invoke-super {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method public getPointAtOffset(I)Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    return-object v0
.end method

.method public getPointsInCluster()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public isTransition()Z
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->transition:Z

    return v0
.end method

.method public size()I
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->pointsInClusterList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method
