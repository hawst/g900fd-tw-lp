.class public Lcom/sec/samsung/gallery/access/face/FaceInterface;
.super Ljava/lang/Object;
.source "FaceInterface.java"


# static fields
.field public static final FINISHED_TYPE_NORMAL:I = 0x3

.field public static final FINISHED_TYPE_REMOVE_MMC:I = 0x2

.field public static final FINISHED_TYPE_RESTORE_MMC:I = 0x1

.field public static final SCANNED_FALSE:I = 0x2

.field public static final SCANNED_RESTORE:I = 0x3

.field public static final SCANNED_TRUE:I = 0x1

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Landroid/content/ContentResolver;I)V
    .locals 0
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # I

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->startFaceScan(Landroid/content/ContentResolver;I)V

    return-void
.end method

.method static synthetic access$200(Landroid/content/ContentResolver;J)I
    .locals 1
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # J

    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->checkFaceScanned(Landroid/content/ContentResolver;J)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Landroid/content/ContentResolver;J)V
    .locals 1
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # J

    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->setScanPriority(Landroid/content/ContentResolver;J)V

    return-void
.end method

.method static synthetic access$400(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->singleFaceScan(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Landroid/content/ContentResolver;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0, p1}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->restoreSingleFaceData(Landroid/content/ContentResolver;Ljava/lang/String;)V

    return-void
.end method

.method private static checkFaceScanned(Landroid/content/ContentResolver;J)I
    .locals 13
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    .line 106
    const/4 v7, 0x0

    .line 107
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x1

    .line 108
    .local v11, "scanned":I
    const/4 v9, -0x1

    .line 109
    .local v9, "face_count":I
    const/4 v10, 0x0

    .line 110
    .local v10, "reusable":I
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "main"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 111
    .local v6, "RAW_SQL_MAIN":Landroid/net/Uri;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "select face_count, reusable from files where _id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    .line 112
    .local v12, "sqlString":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v12}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 115
    .local v1, "sqlUri":Landroid/net/Uri;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 116
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 118
    const/4 v0, 0x1

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 120
    const/4 v0, -0x1

    if-ne v9, v0, :cond_1

    .line 121
    const/4 v11, 0x2

    .line 134
    :cond_0
    :goto_0
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 136
    :goto_1
    return v11

    .line 122
    :cond_1
    const/4 v0, 0x1

    if-ne v10, v0, :cond_2

    .line 123
    :try_start_1
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->checkRestore(Landroid/content/ContentResolver;J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string v2, "face_restore checkFaceScanned SCANNED_RESTORE"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 125
    const/4 v11, 0x3

    goto :goto_0

    .line 128
    :cond_2
    const/4 v11, 0x1

    goto :goto_0

    .line 131
    :catch_0
    move-exception v8

    .line 132
    .local v8, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v8}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 134
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static checkRestore(Landroid/content/ContentResolver;J)Z
    .locals 13
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "files_id"    # J

    .prologue
    const/4 v5, 0x0

    .line 140
    const/4 v8, 0x0

    .line 141
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 142
    .local v7, "check":Z
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "select A.image_id from (select * from faces ) A inner join (select * from files where reusable = 1 AND _id =%d) B on A.data = B._data"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v0, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 146
    .local v11, "sql":Ljava/lang/String;
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "main"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 147
    .local v6, "RAW_SQL_MAIN":Landroid/net/Uri;
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v11}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 150
    .local v1, "uri":Landroid/net/Uri;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 151
    if-eqz v8, :cond_0

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v10

    .line 153
    .local v10, "faces_image_id":I
    int-to-long v2, v10

    cmp-long v0, v2, p1

    if-eqz v0, :cond_0

    .line 154
    const/4 v7, 0x1

    .line 159
    .end local v10    # "faces_image_id":I
    :cond_0
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 161
    :goto_0
    return v7

    .line 156
    :catch_0
    move-exception v9

    .line 157
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 159
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static requestFaceScan(Landroid/content/Context;Ljava/util/List;Z)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "bForce"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 47
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x3

    invoke-static {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/access/face/FaceInterface;->requestFaceScan(Landroid/content/Context;Ljava/util/List;ZI)V

    .line 48
    return-void
.end method

.method public static requestFaceScan(Landroid/content/Context;Ljava/util/List;ZI)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "bForce"    # Z
    .param p3, "type"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;ZI)V"
        }
    .end annotation

    .prologue
    .line 51
    .local p1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v3, :cond_1

    .line 52
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz p2, :cond_1

    .line 53
    :cond_0
    sget-object v3, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string v4, "requestFaceScan() is called."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 55
    .local v0, "cr":Landroid/content/ContentResolver;
    move-object v2, p1

    .line 56
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v1, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;

    invoke-direct {v1, v2, v0, p3}, Lcom/sec/samsung/gallery/access/face/FaceInterface$1;-><init>(Ljava/util/List;Landroid/content/ContentResolver;I)V

    .line 100
    .local v1, "faceScanThread":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 103
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v1    # "faceScanThread":Ljava/lang/Thread;
    .end local v2    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_1
    return-void
.end method

.method private static restoreSingleFaceData(Landroid/content/ContentResolver;Ljava/lang/String;)V
    .locals 7
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 234
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string v3, "restoreSingleFaceData() is called+"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    if-nez p1, :cond_0

    .line 236
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string v3, "restoreSingleFaceData() : id or data is null"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    return-void

    .line 239
    :cond_0
    const/4 v6, 0x0

    .line 241
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v3, "single_restore"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 242
    .local v1, "faceUri":Landroid/net/Uri;
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v2, v0

    .line 243
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 245
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v1    # "faceUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static setScanPriority(Landroid/content/ContentResolver;J)V
    .locals 11
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "id"    # J

    .prologue
    .line 165
    const/4 v8, 0x0

    .line 166
    .local v8, "max_pri":I
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->RAW_SQL_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "main"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 167
    .local v6, "RAW_SQL_MAIN_DB":Landroid/net/Uri;
    const-string v9, "select max(scan_pri) from files where media_type=1 "

    .line 168
    .local v9, "sql":Ljava/lang/String;
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 169
    .local v1, "uri":Landroid/net/Uri;
    const/4 v7, 0x0

    .line 171
    .local v7, "cursor":Landroid/database/Cursor;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 172
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    add-int/lit8 v8, v0, 0x1

    .line 175
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 178
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 181
    const/4 v7, 0x0

    .line 183
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "update files set scan_pri= "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " where media_type=1 and face_count=-1 and _id="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 184
    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 185
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 187
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 189
    return-void

    .line 178
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 187
    :catchall_1
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static singleFaceScan(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "data"    # Ljava/lang/String;

    .prologue
    .line 215
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "singleFaceScan() is called+"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 217
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "singleFaceScan() : id or data is null"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :goto_0
    return-void

    .line 220
    :cond_1
    const/4 v6, 0x0

    .line 222
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v3, "single_scan"

    const-string v4, "1"

    invoke-virtual {v0, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 223
    .local v1, "faceUri":Landroid/net/Uri;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v2, v0

    const/4 v0, 0x1

    aput-object p2, v2, v0

    .line 224
    .local v2, "projection":[Ljava/lang/String;
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 228
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 225
    .end local v1    # "faceUri":Landroid/net/Uri;
    .end local v2    # "projection":[Ljava/lang/String;
    :catch_0
    move-exception v7

    .line 226
    .local v7, "e":Ljava/lang/UnsupportedOperationException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/UnsupportedOperationException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/UnsupportedOperationException;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private static startFaceScan(Landroid/content/ContentResolver;I)V
    .locals 8
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "type"    # I

    .prologue
    .line 192
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceInterface;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "startFaceScan() is called."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    const/4 v6, 0x0

    .line 195
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    .line 197
    .local v1, "faceUri":Landroid/net/Uri;
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 198
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "CleanBuffer"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 204
    :cond_0
    :goto_0
    if-eqz p0, :cond_1

    .line 205
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 210
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 212
    .end local v1    # "faceUri":Landroid/net/Uri;
    :goto_1
    return-void

    .line 199
    .restart local v1    # "faceUri":Landroid/net/Uri;
    :cond_2
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 200
    :try_start_1
    sget-object v0, Lcom/sec/samsung/gallery/access/face/FaceData;->FACE_SCANNER_REQUEST_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "BackupTable"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    goto :goto_0

    .line 207
    .end local v1    # "faceUri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 208
    .local v7, "e":Ljava/lang/Exception;
    :try_start_2
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 210
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method
