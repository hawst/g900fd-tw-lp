.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;
.super Ljava/lang/Object;
.source "GallerySearchView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->setImeVisibility(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

.field final synthetic val$imm:Landroid/view/inputmethod/InputMethodManager;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;ZLandroid/view/inputmethod/InputMethodManager;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$visible:Z

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 268
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$visible:Z

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$imm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$imm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v0

    if-nez v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowImeRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mShowImeRunnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$imm:Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$SearchAutoComplete;->setCursorVisible(Z)V

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->val$imm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$3;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method
