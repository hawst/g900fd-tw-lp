.class public Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;
.super Ljava/lang/Object;
.source "ConfigAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumView"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;


# instance fields
.field public final dragndrop_bottom_margin:I

.field public final dragndrop_left_margin:I

.field public final dragndrop_right_margin:I

.field public final dragndrop_top_margin:I

.field public final drawframe_bottom_padding:I

.field public final drawframe_left_padding:I

.field public final drawframe_right_padding:I

.field public final drawframe_top_padding:I

.field public final drawframes_2_bottom_padding:I

.field public final drawframes_2_left_padding:I

.field public final drawframes_2_right_padding:I

.field public final drawframes_2_top_padding:I

.field public final drawframes_3_bottom_padding:I

.field public final drawframes_3_left_padding:I

.field public final drawframes_3_right_padding:I

.field public final drawframes_3_top_padding:I

.field public final drawlabel_bottom_margin:I

.field public final drawlabel_count_font:I

.field public final drawlabel_font:I

.field public final drawlabel_left_margin:I

.field public final drawlabel_top_margin:I

.field public final edit_mode_checkbox_right_margine:I

.field public final edit_mode_checkbox_top_margine:I

.field public final frameTypeCount:I

.field public final imageview_2_bottom_margin:I

.field public final imageview_2_left_margin:I

.field public final imageview_2_right_margin:I

.field public final imageview_2_top_margin:I

.field public final imageview_3_bottom_margin:I

.field public final imageview_3_left_margin:I

.field public final imageview_3_right_margin:I

.field public final imageview_3_top_margin:I

.field public final imageview_bottom_margin:I

.field public final imageview_left_margin:I

.field public final imageview_right_margin:I

.field public final imageview_top_margin:I

.field public final photoview_image_bottom_margine:I

.field public final photoview_typeicon_bottom_margine:I

.field public final select_mode_checkbox_left_margin:I

.field public final select_mode_checkbox_top_margin:I

.field public final typeicon_bottom_margin:I

.field public final typeicon_scale:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const/16 v1, 0xf

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->frameTypeCount:I

    .line 92
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 94
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d00e1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->typeicon_bottom_margin:I

    .line 95
    const v1, 0x7f0e0012

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->typeicon_scale:F

    .line 96
    const v1, 0x7f0d0313

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_font:I

    .line 97
    const v1, 0x7f0d00e2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_count_font:I

    .line 98
    const v1, 0x7f0d00e3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_bottom_margin:I

    .line 99
    const v1, 0x7f0d00e4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_left_margin:I

    .line 100
    const v1, 0x7f0d00e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawlabel_top_margin:I

    .line 102
    const v1, 0x7f0d00f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->photoview_image_bottom_margine:I

    .line 103
    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->photoview_typeicon_bottom_margine:I

    .line 106
    const v1, 0x7f0d00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_left_margin:I

    .line 107
    const v1, 0x7f0d00ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_top_margin:I

    .line 108
    const v1, 0x7f0d00cb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_right_margin:I

    .line 109
    const v1, 0x7f0d00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_bottom_margin:I

    .line 111
    const v1, 0x7f0d00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_left_margin:I

    .line 112
    const v1, 0x7f0d00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_top_margin:I

    .line 113
    const v1, 0x7f0d00cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_right_margin:I

    .line 114
    const v1, 0x7f0d00d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_2_bottom_margin:I

    .line 116
    const v1, 0x7f0d00d1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_left_margin:I

    .line 117
    const v1, 0x7f0d00d2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_top_margin:I

    .line 118
    const v1, 0x7f0d00d3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_right_margin:I

    .line 119
    const v1, 0x7f0d00d4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->imageview_3_bottom_margin:I

    .line 122
    const v1, 0x7f0d00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_left_padding:I

    .line 123
    const v1, 0x7f0d00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_top_padding:I

    .line 124
    const v1, 0x7f0d00d7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_right_padding:I

    .line 125
    const v1, 0x7f0d00d8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframe_bottom_padding:I

    .line 127
    const v1, 0x7f0d00d9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_left_padding:I

    .line 128
    const v1, 0x7f0d00da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_top_padding:I

    .line 129
    const v1, 0x7f0d00db

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_right_padding:I

    .line 130
    const v1, 0x7f0d00dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_2_bottom_padding:I

    .line 132
    const v1, 0x7f0d00dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_left_padding:I

    .line 133
    const v1, 0x7f0d00de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_top_padding:I

    .line 134
    const v1, 0x7f0d00df

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_right_padding:I

    .line 135
    const v1, 0x7f0d00e0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->drawframes_3_bottom_padding:I

    .line 137
    const v1, 0x7f0c001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->dragndrop_left_margin:I

    .line 138
    const v1, 0x7f0c001c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->dragndrop_top_margin:I

    .line 139
    const v1, 0x7f0c001d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->dragndrop_right_margin:I

    .line 140
    const v1, 0x7f0c001e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->dragndrop_bottom_margin:I

    .line 142
    const v1, 0x7f0d0153

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->edit_mode_checkbox_top_margine:I

    .line 143
    const v1, 0x7f0d0154

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->edit_mode_checkbox_right_margine:I

    .line 145
    const v1, 0x7f0d0155

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->select_mode_checkbox_top_margin:I

    .line 146
    const v1, 0x7f0d0156

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->select_mode_checkbox_left_margin:I

    .line 147
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 85
    const-class v1, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->sInstance:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->sInstance:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;

    .line 88
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;->sInstance:Lcom/sec/samsung/gallery/view/albumview/ConfigAlbum$AlbumView;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
