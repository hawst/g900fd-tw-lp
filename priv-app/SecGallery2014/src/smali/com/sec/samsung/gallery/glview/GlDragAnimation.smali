.class public Lcom/sec/samsung/gallery/glview/GlDragAnimation;
.super Ljava/lang/Object;
.source "GlDragAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;
    }
.end annotation


# static fields
.field public static DRAG_ANIM_FIRST:I = 0x0

.field public static DRAG_ANIM_NONE:I = 0x0

.field public static DRAG_ANIM_SEC:I = 0x0

.field public static DRAG_ANIM_THIRD:I = 0x0

.field private static final MAX_DIM_VALUE:F = 1.0f

.field private static final MIN_DIM_VALUE:F = 0.2f

.field private static final TAG:Ljava/lang/String; = "GlDragAnimation"

.field public static posLatency:I

.field public static stackZ:F


# instance fields
.field private mCount:I

.field private mCurrentAnim:I

.field private mDstHeight:F

.field private mDstWidth:F

.field private mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

.field private mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

.field private mIsInverseDragAnimationRunning:Z

.field private mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

.field private mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field private mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field private mThumbDragSet:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/GlSnakeObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 19
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_NONE:I

    .line 20
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_FIRST:I

    .line 21
    sput v1, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_SEC:I

    .line 22
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_THIRD:I

    .line 27
    const/high16 v0, 0x41200000    # 10.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->stackZ:F

    .line 28
    sput v1, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->posLatency:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 31
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr2;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    .line 33
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    .line 37
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    .line 39
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mIsInverseDragAnimationRunning:Z

    .line 278
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlDragAnimation$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation$4;-><init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .line 288
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/glview/GlDragAnimation;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation;
    .param p1, "x1"    # I

    .prologue
    .line 16
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->startFollowingAnim()V

    return-void
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/glview/GlDragAnimation;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation;
    .param p1, "x1"    # Z

    .prologue
    .line 16
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mIsInverseDragAnimationRunning:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    return-object v0
.end method

.method private startFollowingAnim()V
    .locals 6

    .prologue
    .line 266
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 267
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 268
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getX()F

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLx:F

    .line 269
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getY()F

    move-result v3

    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mLy:F

    .line 270
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->updateInitPosition()V

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 272
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 273
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    const-wide/32 v4, 0x989680

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setDuration(J)V

    .line 274
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V

    .line 275
    sget v3, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_SEC:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    .line 276
    return-void
.end method


# virtual methods
.method public getHeaderObject()Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    return-object v0
.end method

.method public releaseDrag()V
    .locals 4

    .prologue
    .line 243
    const/4 v0, 0x0

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 244
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 245
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->remove()V

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 248
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 249
    return-void
.end method

.method public resetDrag()V
    .locals 4

    .prologue
    .line 252
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    if-nez v2, :cond_0

    .line 261
    :goto_0
    return-void

    .line 254
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_2

    .line 255
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v2, v0

    if-eqz v2, :cond_1

    .line 256
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->stopDim()V

    .line 257
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v2, v2, v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setDim(F)V

    .line 254
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 260
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->releaseDrag()V

    goto :goto_0
.end method

.method public setObjectSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 42
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstWidth:F

    .line 43
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstHeight:F

    .line 44
    return-void
.end method

.method public setOnDragAnimListener(Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;)V
    .locals 0
    .param p1, "onDragListener"    # Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    .prologue
    .line 285
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mOnDragListener:Lcom/sec/samsung/gallery/glview/GlDragAnimation$OnDragAnimListener;

    .line 286
    return-void
.end method

.method public setTargetPos(Lcom/sec/samsung/gallery/glview/GlThumbObject;)V
    .locals 6
    .param p1, "dstObj"    # Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .prologue
    .line 104
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    sget v4, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_SEC:I

    if-ne v3, v4, :cond_1

    .line 111
    :cond_0
    return-void

    .line 107
    :cond_1
    const/4 v0, 0x1

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 108
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 109
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v3

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v4

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetPos(FFF)V

    .line 107
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setThumb(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/samsung/gallery/view/utils/SoundUtils;[Lcom/sec/samsung/gallery/glview/GlThumbObject;[Lcom/sec/android/gallery3d/glcore/GlView;ILcom/sec/android/gallery3d/glcore/GlObject;FF)V
    .locals 13
    .param p1, "layer"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "sound"    # Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    .param p3, "srcObj"    # [Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .param p4, "srcView"    # [Lcom/sec/android/gallery3d/glcore/GlView;
    .param p5, "count"    # I
    .param p6, "dstObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p7, "x"    # F
    .param p8, "y"    # F

    .prologue
    .line 50
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 51
    move/from16 v0, p5

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    .line 54
    move-object/from16 v0, p6

    iget v3, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v3, v3, p7

    neg-float v3, v3

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstWidth:F

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    sub-float v11, v3, v4

    .line 55
    .local v11, "offsetX":F
    move-object/from16 v0, p6

    iget v3, v0, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sub-float v3, v3, p8

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstHeight:F

    const/high16 v5, 0x40800000    # 4.0f

    div-float/2addr v4, v5

    add-float v12, v3, v4

    .line 57
    .local v12, "offsetY":F
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 58
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v6, 0x140

    const/16 v7, 0x140

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 59
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    const/4 v3, 0x0

    aget-object v3, p3, v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mIndex:I

    .line 60
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 61
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstWidth:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSize(FF)V

    .line 62
    const/4 v3, 0x0

    aget-object v3, p3, v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v3

    const/4 v4, 0x0

    aget-object v4, p3, v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v4

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v5

    const/high16 v6, 0x3f800000    # 1.0f

    add-float/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPos(FFF)V

    .line 67
    :goto_0
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPosAsSource()V

    .line 68
    const/4 v3, 0x0

    aget-object v3, p3, v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    const/4 v4, 0x0

    aget-object v4, p3, v4

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSourceSize(FF)V

    .line 69
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstWidth:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setTargetSize(FF)V

    .line 70
    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v3

    add-float/2addr v3, v11

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v4

    add-float/2addr v4, v12

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v5

    sget v6, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->stackZ:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setTargetPos(FFF)V

    .line 71
    sget v3, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->posLatency:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSnakeLatency(I)V

    .line 72
    const/4 v3, 0x0

    aget-object v3, p4, v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 73
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 76
    const/4 v9, 0x1

    .local v9, "i":I
    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    .local v10, "n":I
    :goto_1
    if-ge v9, v10, :cond_1

    .line 77
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x3f800000    # 1.0f

    const/16 v6, 0x140

    const/16 v7, 0x140

    move-object v3, p1

    invoke-direct/range {v2 .. v7}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 78
    .restart local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlSnakeObject;
    aget-object v3, p3, v9

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iput v3, v2, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->mIndex:I

    .line 79
    aget-object v3, p3, v9

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    aget-object v4, p3, v9

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSize(FF)V

    .line 80
    aget-object v3, p3, v9

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v3

    aget-object v4, p3, v9

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v4

    aget-object v5, p3, v9

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPos(FFF)V

    .line 81
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPosAsSource()V

    .line 82
    aget-object v3, p3, v9

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    aget-object v4, p3, v9

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSourceSize(FF)V

    .line 83
    sget v3, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->posLatency:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSnakeLatency(I)V

    .line 84
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstWidth:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mDstHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setTargetSize(FF)V

    .line 85
    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v3

    add-float/2addr v3, v11

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v4

    add-float/2addr v4, v12

    invoke-virtual/range {p6 .. p6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v5

    sget v6, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->stackZ:F

    sub-int v7, v10, v9

    int-to-float v7, v7

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setTargetPos(FFF)V

    .line 86
    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v4

    double-to-float v3, v4

    const/high16 v4, 0x41f00000    # 30.0f

    mul-float/2addr v3, v4

    const/high16 v4, 0x41700000    # 15.0f

    sub-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setTargetRoll(F)V

    .line 87
    aget-object v3, p4, v9

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 89
    const v3, 0x3f4ccccd    # 0.8f

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    sub-int/2addr v4, v9

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    const v4, 0x3e4ccccd    # 0.2f

    add-float v8, v3, v4

    .line 90
    .local v8, "dim":F
    invoke-virtual {v2, v8}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setDim(F)V

    .line 91
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    .line 64
    .end local v8    # "dim":F
    .end local v9    # "i":I
    .end local v10    # "n":I
    :cond_0
    const/4 v3, 0x0

    aget-object v3, p3, v3

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    const/4 v4, 0x0

    aget-object v4, p3, v4

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setSize(FF)V

    .line 65
    const/4 v3, 0x0

    aget-object v3, p3, v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v3

    const/4 v4, 0x0

    aget-object v4, p3, v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v4

    const/4 v5, 0x0

    aget-object v5, p3, v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->setPos(FFF)V

    goto/16 :goto_0

    .line 94
    .restart local v9    # "i":I
    .restart local v10    # "n":I
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->linkSnake(Ljava/util/ArrayList;)V

    .line 95
    return-void
.end method

.method public startDragAnimation()V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 113
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mIsInverseDragAnimationRunning:Z

    if-eqz v4, :cond_0

    .line 150
    :goto_0
    return-void

    .line 117
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 119
    .local v2, "n":I
    if-ne v2, v6, :cond_1

    .line 120
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    .line 121
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 122
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 123
    .local v0, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 124
    sget v4, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_NONE:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    goto :goto_0

    .line 127
    .end local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCount:I

    if-ge v1, v4, :cond_2

    .line 128
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 129
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 130
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 131
    .restart local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 127
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 133
    .end local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v4, :cond_3

    .line 134
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 135
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 136
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 137
    .restart local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    new-instance v4, Lcom/sec/samsung/gallery/glview/GlDragAnimation$1;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation$1;-><init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_0
.end method

.method public startInverseDragAnimation()V
    .locals 9

    .prologue
    .line 155
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 199
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 158
    sget v6, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_THIRD:I

    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    .line 159
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_1
    if-ge v2, v3, :cond_1

    .line 160
    const/4 v5, 0x0

    .line 162
    .local v5, "srcObj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    aget-object v5, v6, v2

    .line 163
    if-nez v5, :cond_2

    .line 164
    const-string v6, "GlDragAnimation"

    const-string/jumbo v7, "srcObj is null"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 159
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    const-string v6, "GlDragAnimation"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "ArrayIndexOutOfBoundsException..  index = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "  size of mSrcObj = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSrcObj:[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    .end local v1    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    .end local v5    # "srcObj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 185
    .local v0, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    new-instance v6, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation$2;-><init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_0

    .line 171
    .end local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    .restart local v5    # "srcObj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 172
    .local v4, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 173
    .restart local v0    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 174
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPosAsSource()V

    .line 175
    iget v6, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v4, v6, v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceSize(FF)V

    .line 176
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getRoll()F

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceRoll(F)V

    .line 177
    iget v6, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    iget v7, v5, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v4, v6, v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetSize(FF)V

    .line 178
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getX()F

    move-result v6

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getY()F

    move-result v7

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getZ()F

    move-result v8

    invoke-virtual {v4, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetPos(FFF)V

    .line 179
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetRoll(F)V

    .line 180
    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 181
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v0

    .line 182
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/GlInterpolator;

    invoke-virtual {v0, v6}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    goto :goto_2
.end method

.method public startMoveToAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 9
    .param p1, "dstObj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 204
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    :goto_0
    return-void

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mFollowAnim:Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 207
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v1, :cond_1

    .line 208
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 209
    :cond_1
    sget v1, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->DRAG_ANIM_THIRD:I

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mCurrentAnim:I

    .line 210
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 211
    .local v8, "totalCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v8, :cond_2

    .line 212
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mThumbDragSet:Ljava/util/ArrayList;

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 213
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v6

    .line 214
    .local v6, "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->stop()V

    .line 215
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setPosAsSource()V

    .line 216
    iget v1, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mWidth:F

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceSize(FF)V

    .line 217
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->getRoll()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setSourceRoll(F)V

    .line 218
    iget v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mWidth:F

    iget v2, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetSize(FF)V

    .line 219
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getX()F

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getY()F

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetPos(FFF)V

    .line 220
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setTargetRoll(F)V

    .line 221
    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    sub-int v4, v8, v7

    add-int/lit8 v4, v4, -0x1

    mul-int/lit8 v4, v4, 0x28

    int-to-long v4, v4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->startDefTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJ)V

    .line 211
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 223
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v6    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlDragAnimation;->mHeadObj:Lcom/sec/samsung/gallery/glview/GlSnakeObject;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlSnakeObject;->getDefTransAnim()Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    move-result-object v6

    .line 224
    .restart local v6    # "animation":Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/GlDragAnimation$3;-><init>(Lcom/sec/samsung/gallery/glview/GlDragAnimation;)V

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    goto :goto_0
.end method
