.class final Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;
.super Landroid/widget/BaseAdapter;
.source "MoreInfoItemUserTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "FaceAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mFaceNames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 438
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 433
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    .line 439
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mContext:Landroid/content/Context;

    .line 440
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 441
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->startLoadFace()V

    .line 442
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    .prologue
    .line 432
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    return-object v0
.end method

.method private startLoadFace()V
    .locals 4

    .prologue
    .line 445
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mContext:Landroid/content/Context;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 446
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 455
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 460
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getTags()Ljava/lang/String;
    .locals 4

    .prologue
    .line 480
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 481
    .local v2, "size":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 482
    .local v0, "builder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 483
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 484
    add-int/lit8 v3, v2, -0x1

    if-ge v1, v3, :cond_0

    .line 485
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 482
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 488
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 465
    if-nez p2, :cond_0

    .line 466
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03008c

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 469
    :cond_0
    const v2, 0x7f0f0188

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 470
    .local v1, "textView":Landroid/widget/TextView;
    if-eqz v1, :cond_1

    .line 471
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 474
    :cond_1
    const v2, 0x7f0f0189

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 475
    .local v0, "deleteIcon":Landroid/widget/ImageView;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 476
    return-object p2
.end method

.method public onDataSetChanged()V
    .locals 1

    .prologue
    .line 496
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;->dataChanged()V

    .line 497
    return-void
.end method

.method public registerDataSetChangeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    .prologue
    .line 492
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    .line 493
    return-void
.end method
