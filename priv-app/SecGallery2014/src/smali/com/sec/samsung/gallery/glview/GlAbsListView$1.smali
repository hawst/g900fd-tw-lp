.class Lcom/sec/samsung/gallery/glview/GlAbsListView$1;
.super Ljava/lang/Object;
.source "GlAbsListView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlAbsListView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlAbsListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlAbsListView;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 143
    const-string v0, "GlAbsListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CMD_UPDATE_ITEM. Size = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setCommand(IIII)V

    .line 145
    return-void
.end method

.method public onChanged(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v3, 0x0

    .line 149
    const-string v0, "GlAbsListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CMD_UPDATE_ITEM index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;->this$0:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, p1, v3, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setCommand(IIII)V

    .line 151
    return-void
.end method
