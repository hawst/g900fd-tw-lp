.class Lcom/sec/samsung/gallery/glview/GlPhotoView$12;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1911
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFlingEnd(F)V
    .locals 3
    .param p1, "delta"    # F

    .prologue
    const/4 v2, 0x0

    .line 1931
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1502(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1932
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1933
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setScroll(F)V

    .line 1934
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setAlbumPitchRate(F)V

    .line 1935
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->hideScrollBar()V

    .line 1937
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 1938
    return-void
.end method

.method public onFlingProcess(FF)V
    .locals 2
    .param p1, "delta"    # F
    .param p2, "elastic"    # F

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1502(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 1914
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1915
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setScroll(F)V

    .line 1916
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    neg-float v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->setAlbumPitchRate(F)V

    .line 1924
    :cond_0
    return-void
.end method
