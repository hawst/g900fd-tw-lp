.class public Lcom/sec/samsung/gallery/view/ViewObservable;
.super Ljava/lang/Object;
.source "ViewObservable.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/ViewObservable$1;,
        Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;
    }
.end annotation


# instance fields
.field private final mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;-><init>(Lcom/sec/samsung/gallery/view/ViewObservable$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    .line 41
    return-void
.end method


# virtual methods
.method public addObserver(Ljava/util/Observer;)V
    .locals 1
    .param p1, "obs"    # Ljava/util/Observer;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;->addObserver(Ljava/util/Observer;)V

    .line 31
    return-void
.end method

.method public notifyObservers(Ljava/lang/Object;)V
    .locals 1
    .param p1, "data"    # Ljava/lang/Object;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;->setChanged()V

    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;->notifyObservers(Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method public removeObservers()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;->deleteObservers()V

    .line 39
    return-void
.end method

.method public removeObsever(Ljava/util/Observer;)V
    .locals 1
    .param p1, "obs"    # Ljava/util/Observer;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/ViewObservable;->mObservable:Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ViewObservable$LocalObservable;->deleteObserver(Ljava/util/Observer;)V

    .line 35
    return-void
.end method
