.class public Lcom/sec/samsung/gallery/glview/GlTouchAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlTouchAnimation.java"


# static fields
.field public static final DEF_DURATION:J = 0x64L

.field public static final DEF_FOCUS_SCALE:F = 1.3f

.field public static final DEF_PRESS_SCALE:F = 0.9f

.field public static final DEF_SELECT_SCALE:F = 1.2f


# instance fields
.field private mDefSaved:Z

.field private mFocusScale:F

.field private mFromA:F

.field private mFromScale:F

.field private mFromYaw:F

.field private mLScale:F

.field private mLYaw:F

.field private mLa:F

.field private mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mOrgAlpha:F

.field private mOrgScale:F

.field private mOrgYaw:F

.field private mPressScale:F

.field private mSelectScale:F

.field private mToA:F

.field private mToScale:F

.field private mToYaw:F


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 24
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mPressScale:F

    .line 25
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    .line 26
    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFocusScale:F

    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/glcore/GlObject;FFF)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "pressScale"    # F
    .param p3, "selectScale"    # F
    .param p4, "focusScale"    # F

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 15
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 24
    const v0, 0x3f666666    # 0.9f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mPressScale:F

    .line 25
    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    .line 26
    const v0, 0x3fa66666    # 1.3f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFocusScale:F

    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 34
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mPressScale:F

    .line 35
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    .line 36
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFocusScale:F

    .line 37
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 6
    .param p1, "ratio"    # F

    .prologue
    .line 169
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float v1, v3, v4

    .line 170
    .local v1, "dScale":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    invoke-static {v3, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_0

    .line 171
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v1, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 172
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 175
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float v0, v3, v4

    .line 176
    .local v0, "dAlpha":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    invoke-static {v3, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_1

    .line 177
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 178
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    .line 181
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    sub-float/2addr v4, v5

    mul-float/2addr v4, p1

    add-float v2, v3, v4

    .line 182
    .local v2, "dYaw":F
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    invoke-static {v3, v2}, Ljava/lang/Float;->compare(FF)I

    move-result v3

    if-eqz v3, :cond_2

    .line 183
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    .line 184
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    .line 186
    :cond_2
    return-void
.end method

.method public cancelPressAnim()V
    .locals 2

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 79
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 80
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 81
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 83
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 84
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 85
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 86
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 87
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 88
    return-void
.end method

.method public saveDefaultState()V
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mDefSaved:Z

    if-nez v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getScaleX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgScale:F

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAlpha()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getYaw()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    .line 56
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 57
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    .line 58
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    .line 59
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mDefSaved:Z

    .line 61
    :cond_0
    return-void
.end method

.method public setFocusScale(F)V
    .locals 0
    .param p1, "focusScale"    # F

    .prologue
    .line 48
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFocusScale:F

    .line 49
    return-void
.end method

.method public setPressScale(F)V
    .locals 0
    .param p1, "pressScale"    # F

    .prologue
    .line 40
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mPressScale:F

    .line 41
    return-void
.end method

.method public setSelectScale(F)V
    .locals 0
    .param p1, "selectScale"    # F

    .prologue
    .line 44
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    .line 45
    return-void
.end method

.method public setSelected(Z)V
    .locals 3
    .param p1, "selected"    # Z

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->saveDefaultState()V

    .line 137
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 138
    if-eqz p1, :cond_0

    .line 139
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 140
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    .line 147
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setScale(FF)V

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    .line 150
    return-void

    .line 143
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    .line 144
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    .line 145
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    goto :goto_0
.end method

.method public startAppearAnim(J)V
    .locals 3
    .param p1, "duration"    # J

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 153
    const v0, 0x3dcccccd    # 0.1f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 154
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 155
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 157
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 158
    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 159
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 160
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 161
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 162
    return-void
.end method

.method public startFocusAnim()V
    .locals 2

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->saveDefaultState()V

    .line 105
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 106
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 107
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 108
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 110
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFocusScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 111
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 112
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 113
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 114
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 115
    return-void
.end method

.method public startPressAnim()V
    .locals 2

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->saveDefaultState()V

    .line 65
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 66
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 67
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 68
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 70
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mPressScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 71
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 72
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 73
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 74
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 75
    return-void
.end method

.method public startSelectAnim()V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 92
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 93
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 94
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 96
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 97
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 99
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 100
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 101
    return-void
.end method

.method public startUnfocusAnim(Z)V
    .locals 2
    .param p1, "selected"    # Z

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->stop()V

    .line 119
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromScale:F

    .line 120
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLa:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromA:F

    .line 121
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mLYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mFromYaw:F

    .line 122
    if-eqz p1, :cond_0

    .line 123
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mSelectScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 124
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    .line 131
    :goto_0
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->setDuration(J)V

    .line 132
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->start()V

    .line 133
    return-void

    .line 127
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgScale:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToScale:F

    .line 128
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgAlpha:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToA:F

    .line 129
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mOrgYaw:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlTouchAnimation;->mToYaw:F

    goto :goto_0
.end method
