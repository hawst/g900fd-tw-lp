.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;
.super Ljava/lang/Object;
.source "TimeViewActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;I)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iput p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 291
    const/4 v0, 0x0

    .line 292
    .local v0, "numberOfSelectedAlbums":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 293
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 295
    .local v1, "res":Landroid/content/res/Resources;
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 296
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    iput v3, v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 297
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-ne v2, v4, :cond_1

    .line 298
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 308
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;ILjava/lang/String;)V

    .line 309
    return-void

    .line 302
    :cond_1
    const v2, 0x7f0e0053

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
