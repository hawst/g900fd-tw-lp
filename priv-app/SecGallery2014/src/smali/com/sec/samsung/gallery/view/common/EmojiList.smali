.class public Lcom/sec/samsung/gallery/view/common/EmojiList;
.super Ljava/lang/Object;
.source "EmojiList.java"


# static fields
.field private static unicodeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    .line 27
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x263a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 29
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f602"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f603"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 39
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f604"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 41
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f605"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 43
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f606"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f607"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 47
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f608"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f609"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 53
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f610"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 59
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f620"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 61
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 63
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f621"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f622"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 67
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f634"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f623"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f624"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f625"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f626"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f627"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f628"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f629"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f630"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f631"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 91
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f632"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 93
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f633"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f635"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f636"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f637"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f612"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 109
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 115
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f617"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 117
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f619"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 119
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f618"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 121
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f61a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 123
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 125
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f616"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 131
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 133
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 135
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f60f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 137
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f613"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 139
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f62b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 147
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f645"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f646"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 151
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f647"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 155
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 159
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 161
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f638"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f639"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 167
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 169
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 171
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f63e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 173
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f640"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 175
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 177
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 179
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f64a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 181
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f476"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 185
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f466"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f467"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 189
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f468"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 191
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f469"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 193
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f474"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 195
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f475"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f491"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 201
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 203
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 205
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 209
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f464"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 211
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f465"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 215
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f477"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f481"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f482"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 221
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f46f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 223
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f470"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f478"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 227
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f385"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 229
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f471"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f472"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 235
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f473"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f483"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f486"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 241
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f487"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 243
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f485"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 245
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 247
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f479"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 255
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f47f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f480"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 259
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 261
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f440"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 263
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f442"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 265
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f443"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 267
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f463"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 269
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f444"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f445"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 273
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2764"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f499"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 279
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 281
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 283
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 285
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f493"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 287
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f494"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 289
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f495"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 291
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f496"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 293
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f497"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 295
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f498"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 299
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 301
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f49f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 303
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 305
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 307
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 313
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 317
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x261d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f446"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f447"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f448"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 325
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f449"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 327
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 329
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f44f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 331
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f450"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 335
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f530"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f484"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 339
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 341
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f451"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f452"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 347
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f393"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 351
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f453"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 353
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 355
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f454"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f455"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 359
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f456"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f457"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 363
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f458"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 365
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f459"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 367
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f460"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 369
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f461"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f462"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 373
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f392"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f45b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 387
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 389
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 395
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 397
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 399
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 405
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 409
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f489"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 411
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f514"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f515"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 423
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 425
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f526"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 431
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 437
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 439
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 441
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4da"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 443
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 445
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 451
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 453
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 455
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 457
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4db"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f383"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 461
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f384"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 463
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f380"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f381"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 467
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f382"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 469
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f388"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f386"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f387"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 475
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f389"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 477
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 479
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f38b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 483
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4df"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 489
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x260e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 491
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4de"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 493
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 497
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2709"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 499
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 503
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 505
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 509
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ee"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 513
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 515
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 517
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ef"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 519
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 523
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 525
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 529
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2712"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x270f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 533
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4dd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 537
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4d0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 541
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 543
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 545
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2702"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 549
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 551
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 553
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 557
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 559
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 561
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 563
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4cb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 565
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 567
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 571
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 573
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 575
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 579
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 581
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 583
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 585
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 587
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 589
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ac"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 595
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 597
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 599
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 603
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 605
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 607
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 609
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f0cf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 615
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f004"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 617
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 621
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 623
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 625
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 627
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 633
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 639
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 645
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x303d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 651
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f415"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f436"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 655
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f429"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 657
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f408"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 659
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f431"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 661
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f400"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 663
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f401"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 665
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 667
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f439"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 669
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f422"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 671
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f407"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 673
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f430"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 675
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f413"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 677
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f414"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 679
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f423"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 681
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f424"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 683
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f425"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 685
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f426"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 689
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f411"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 691
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f410"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 693
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 695
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f403"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 697
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f402"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 699
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f404"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f434"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 705
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f417"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 707
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f416"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f437"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 711
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 713
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f438"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 715
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 717
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 719
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f427"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 721
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f418"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 723
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f428"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f412"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 727
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f435"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 729
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f406"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 731
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 733
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 735
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 737
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 739
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 741
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f433"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 743
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 745
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 747
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f420"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 749
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f421"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 751
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f419"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 753
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f42c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 757
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 759
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 761
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 763
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 765
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f41e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 767
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f432"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 769
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f409"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 771
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f43e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f378"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 775
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 777
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 779
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f377"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 781
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f379"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 783
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f376"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 785
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2615"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 787
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f375"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 789
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f37c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 791
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f374"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 793
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f368"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 795
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f367"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 797
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f366"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 799
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f369"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 801
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f370"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 803
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 807
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 809
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 811
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 813
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f36f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 815
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f373"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 817
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f354"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 819
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 821
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 823
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f355"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 825
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f356"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 827
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f357"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 829
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f364"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 831
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f363"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 833
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f371"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 835
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 837
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 839
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f359"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 841
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 843
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f35b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 845
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f372"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 847
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f365"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f362"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 851
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f361"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 853
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f358"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 855
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f360"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 857
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 859
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 861
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 863
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 865
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 867
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f344"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 869
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f345"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 871
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f346"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 873
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f347"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f348"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 877
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f349"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 879
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f350"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 881
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f351"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 883
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f352"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 885
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f353"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 887
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f34d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 889
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f330"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 891
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f331"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 893
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f332"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 895
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f333"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 897
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f334"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 899
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f335"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 901
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f337"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 903
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f338"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 905
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f339"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 907
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f340"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 909
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f341"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 911
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f342"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 913
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f343"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 915
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 917
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 919
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 921
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 923
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 925
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f33f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 927
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2600"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 929
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f308"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 931
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 933
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2601"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 935
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f301"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 937
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f302"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 939
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2614"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 941
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 943
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 945
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f300"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 947
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2744"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 949
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 951
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f319"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 953
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 955
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 957
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 959
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 961
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 963
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f311"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 965
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f312"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 967
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f313"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 969
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f314"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 971
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f315"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 973
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f316"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 975
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f317"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 977
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f318"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 979
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f391"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 981
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f304"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 983
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f305"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 985
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f307"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f306"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 989
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f303"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 991
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f309"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 995
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1003
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f30d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1005
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f310"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1009
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1011
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1013
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1015
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1017
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1019
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1021
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1023
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1025
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1027
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1029
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1031
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1033
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1035
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1037
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1039
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1041
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ed"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1043
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1045
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1047
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1049
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f5ff"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1051
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1053
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f488"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1055
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f527"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1057
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f528"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1059
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f529"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1061
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1063
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1065
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1067
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1069
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1071
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1073
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1075
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1077
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1079
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1081
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1083
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3be"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1085
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26bd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1087
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1089
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1091
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1093
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1095
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1097
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1099
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1101
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1103
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f40e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1105
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1107
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3c9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1109
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f3ca"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1111
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f682"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1113
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f683"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1115
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f684"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1117
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f685"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1119
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f686"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1121
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f687"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1123
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x24c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1125
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f688"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1127
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1129
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1131
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1133
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1135
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1137
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f68f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1139
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f690"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1141
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f691"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1143
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f692"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1145
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f693"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1147
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f694"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1149
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f695"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1151
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f696"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1153
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f697"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1155
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f698"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1157
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f699"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1159
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1161
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1163
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1165
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1167
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1169
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f69f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1171
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1173
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1175
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1177
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1179
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f681"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1181
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2708"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1183
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1185
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1187
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1189
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6c5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1191
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1193
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1195
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1197
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1199
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1201
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1203
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1205
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f689"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1207
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f680"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1209
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1211
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1213
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1215
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1217
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1219
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1221
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1223
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1225
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2668"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1227
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1229
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1231
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f48e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1233
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f490"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1235
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f492"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1239
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1241
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f519"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1243
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1245
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1247
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1249
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x231b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1253
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23f0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1255
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2648"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1257
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2649"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1259
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1261
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1263
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1265
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1267
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1269
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x264f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1271
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2650"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1273
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2651"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1275
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2652"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1277
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2653"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1279
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ce"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1281
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f531"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1283
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f52f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1285
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1287
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1289
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1291
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1293
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1295
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f170"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1297
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f171"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1299
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f18e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1301
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f17e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1303
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ae"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1305
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4af"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1307
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f520"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1309
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f521"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1311
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f522"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1313
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f523"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1315
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f524"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1317
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27bf"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1319
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1321
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f3"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1323
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1325
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4f5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1327
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6b9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1329
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ba"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1331
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6bc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1333
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1335
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x267b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1337
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ad"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1339
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1341
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1343
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f201"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1345
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f51e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1347
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26d4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1349
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f192"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1351
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1353
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1355
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1357
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1359
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f193"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1361
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1363
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f19a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1365
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f232"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1367
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f233"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1369
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f234"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1371
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f235"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1373
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f236"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1375
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f237"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1377
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f238"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1379
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f239"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1381
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f202"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1383
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f23a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1385
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f250"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1387
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f251"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1389
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3299"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1397
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f21a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1399
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f22f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1401
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3297"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1403
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b55"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1405
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1407
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x274e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1409
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2139"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1411
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f6ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1413
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2705"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1415
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2714"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1417
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f517"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1419
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2734"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1421
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2733"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1423
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2795"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1425
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2796"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1427
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2716"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1429
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2797"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1431
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1433
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1435
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a4"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1437
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a2"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1439
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f525"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1441
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a5"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1443
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a8"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1445
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4a6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1447
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1449
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1451
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f567"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1453
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f550"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1455
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1457
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f551"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1459
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1461
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f552"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1463
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1465
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f553"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1467
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1469
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f554"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1471
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f560"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1473
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f555"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1475
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f561"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1477
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f556"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1479
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f562"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1481
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f557"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1483
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f563"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1485
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f558"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1487
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f564"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1489
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f559"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1491
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f565"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1493
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f55a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1495
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f566"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1497
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2195"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1499
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b06"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1501
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2197"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1503
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27a1"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1505
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2198"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1507
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b07"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1509
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2199"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1511
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b05"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1513
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2196"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1515
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1517
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2934"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1519
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2935"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1521
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ea"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1523
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23eb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1525
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23ec"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1527
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x23e9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1529
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25c0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1531
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25b6"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1533
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1535
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1537
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2747"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1539
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2728"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1541
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f534"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1543
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f535"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1545
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1547
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x26ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1549
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f533"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1551
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f532"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1553
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b50"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1555
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f31f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1557
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f320"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1559
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25ab"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1563
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fd"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1565
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fe"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1567
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fb"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1569
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x25fc"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1571
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1c"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1573
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2b1b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1575
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f538"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1577
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f539"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1579
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f536"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1581
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f537"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1583
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1585
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f53b"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1587
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2754"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1589
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2753"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1591
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2755"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1593
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2757"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1597
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2049"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1599
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x3030"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1601
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x27b0"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1603
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2660"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1607
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2663"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1609
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2666"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1611
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f194"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1613
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f511"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1615
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21a9"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1617
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f191"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1619
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50d"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1621
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f512"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1623
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f513"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1625
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x21aa"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1627
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f510"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1629
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x2611"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1631
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f518"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1633
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50e"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1635
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f516"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1637
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50f"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1639
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f503"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1641
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f500"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1643
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f501"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1645
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f502"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1647
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f504"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1649
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f4e7"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1651
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f505"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1653
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f506"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1655
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f507"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1657
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f508"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1659
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f509"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1661
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    const-string v1, "0x1f50a"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1663
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPattern()Ljava/util/regex/Pattern;
    .locals 8

    .prologue
    .line 1679
    const/4 v0, 0x0

    .line 1680
    .local v0, "EMOJI_PATTERN":Ljava/util/regex/Pattern;
    const/4 v3, 0x0

    .line 1681
    .local v3, "patternMade":Z
    if-nez v3, :cond_1

    .line 1682
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/sec/samsung/gallery/view/common/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 1684
    .local v1, "emojiPattern":Ljava/lang/StringBuilder;
    const/16 v5, 0x28

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1686
    invoke-static {}, Lcom/sec/samsung/gallery/view/common/EmojiList;->getUnicodeList()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1687
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1688
    .local v4, "str":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1689
    const/16 v5, 0x7c

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1691
    .end local v4    # "str":Ljava/lang/String;
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    const-string v7, ")"

    invoke-virtual {v1, v5, v6, v7}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 1693
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    .line 1694
    const/4 v3, 0x1

    .line 1696
    .end local v1    # "emojiPattern":Ljava/lang/StringBuilder;
    .end local v2    # "itr":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :cond_1
    return-object v0
.end method

.method public static getUnicodeList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 15
    sget-object v0, Lcom/sec/samsung/gallery/view/common/EmojiList;->unicodeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static hasEmojiString(Ljava/lang/CharSequence;)Z
    .locals 4
    .param p0, "chars"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v3, 0x0

    .line 1667
    :try_start_0
    invoke-static {}, Lcom/sec/samsung/gallery/view/common/EmojiList;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v2

    .line 1668
    .local v2, "ptn":Ljava/util/regex/Pattern;
    if-eqz v2, :cond_0

    .line 1669
    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 1670
    .local v1, "match":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v3

    .line 1675
    .end local v1    # "match":Ljava/util/regex/Matcher;
    .end local v2    # "ptn":Ljava/util/regex/Pattern;
    :cond_0
    :goto_0
    return v3

    .line 1674
    :catch_0
    move-exception v0

    .line 1675
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static unicodeToUTF16(Ljava/lang/String;)Ljava/lang/String;
    .locals 14
    .param p0, "unicode"    # Ljava/lang/String;

    .prologue
    .line 1700
    const-string v0, ""

    .line 1702
    .local v0, "convertedUTF16Str":Ljava/lang/String;
    invoke-static {p0}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 1704
    .local v9, "unicodeInteger":I
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, -0x2

    const/4 v13, 0x4

    if-le v12, v13, :cond_0

    .line 1705
    const/high16 v12, 0x1f0000

    and-int/2addr v12, v9

    shr-int/lit8 v11, v12, 0x10

    .line 1706
    .local v11, "zzzzz":I
    add-int/lit8 v10, v11, -0x1

    .line 1708
    .local v10, "yyyy":I
    const/4 v5, 0x0

    .line 1709
    .local v5, "rear2byte":I
    const v6, 0xdc00

    .line 1710
    .local v6, "rear2byteFixed":I
    and-int/lit16 v12, v9, 0x3ff

    or-int v5, v6, v12

    .line 1711
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1713
    .local v8, "rear2byteString":Ljava/lang/String;
    const/4 v1, 0x0

    .line 1714
    .local v1, "former2byte":I
    const v2, 0xd800

    .line 1715
    .local v2, "former2byteFixed":I
    const v12, 0xfc00

    and-int/2addr v12, v9

    shr-int/lit8 v12, v12, 0xa

    or-int v1, v2, v12

    .line 1716
    shl-int/lit8 v12, v10, 0x6

    or-int/2addr v1, v12

    .line 1717
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "0x"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1719
    .local v4, "former2byteString":Ljava/lang/String;
    invoke-static {v8}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1720
    .local v7, "rear2byteInt":I
    invoke-static {v4}, Ljava/lang/Integer;->decode(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 1722
    .local v3, "former2byteInt":I
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v3

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v7

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1728
    .end local v1    # "former2byte":I
    .end local v2    # "former2byteFixed":I
    .end local v3    # "former2byteInt":I
    .end local v4    # "former2byteString":Ljava/lang/String;
    .end local v5    # "rear2byte":I
    .end local v6    # "rear2byteFixed":I
    .end local v7    # "rear2byteInt":I
    .end local v8    # "rear2byteString":Ljava/lang/String;
    .end local v10    # "yyyy":I
    .end local v11    # "zzzzz":I
    :goto_0
    return-object v0

    .line 1725
    :cond_0
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, ""

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    int-to-char v13, v9

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
