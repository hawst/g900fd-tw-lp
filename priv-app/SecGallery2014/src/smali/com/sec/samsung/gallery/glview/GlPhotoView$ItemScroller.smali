.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemScroller"
.end annotation


# instance fields
.field private mFullVisibleCnt:I

.field private mGlAbsListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

.field private paddingBottom:F

.field private paddingLeft:F

.field private paddingRight:F

.field private paddingTop:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlAbsListView;)V
    .locals 0
    .param p2, "glAbsListView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .prologue
    .line 3046
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    .line 3047
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mGlAbsListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .line 3048
    return-void
.end method


# virtual methods
.method public applyThumbPosition()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 3142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_0

    .line 3143
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 3154
    :cond_0
    return-void

    .line 3146
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 3147
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_2

    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_3

    .line 3142
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3150
    :cond_3
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDx:F

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDy:F

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDz:F

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 3151
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 3152
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_1
.end method

.method protected getItemRowCount()I
    .locals 2

    .prologue
    .line 3263
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    div-int/2addr v0, v1

    return v0
.end method

.method public getThumbnailViewRowItemCount(I)I
    .locals 5
    .param p1, "mode"    # I

    .prologue
    .line 3381
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewTopPaddingPixel(I)I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v3

    sub-int v1, v2, v3

    .line 3382
    .local v1, "viewHeight":I
    int-to-float v2, v1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWindowCoordinateValue(F)F

    move-result v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v0, v2

    .line 3383
    .local v0, "nCount":I
    if-nez v0, :cond_0

    .line 3384
    const/4 v0, 0x1

    .line 3386
    :cond_0
    return v0
.end method

.method public lastColFocused()Z
    .locals 2

    .prologue
    .line 3350
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWideMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemFocused:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeVisible(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v7, 0x0

    .line 3355
    if-nez p1, :cond_0

    .line 3378
    :goto_0
    return-void

    .line 3357
    :cond_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getY()F

    move-result v3

    .line 3358
    .local v3, "y":F
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getHeight(Z)F

    move-result v1

    .line 3359
    .local v1, "h":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    .line 3360
    .local v2, "newScroll":F
    const/4 v0, 0x0

    .line 3362
    .local v0, "deltaY":F
    cmpl-float v4, v3, v7

    if-lez v4, :cond_3

    .line 3363
    div-float v4, v1, v6

    add-float/2addr v4, v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v5, v6

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v6

    sub-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingTop:F

    sub-float/2addr v5, v6

    sub-float v0, v4, v5

    .line 3364
    cmpl-float v4, v0, v7

    if-lez v4, :cond_2

    .line 3365
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    sub-float v2, v4, v0

    .line 3376
    :cond_1
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->onMove(F)V

    .line 3377
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setScroll(F)V

    goto :goto_0

    .line 3366
    :cond_2
    invoke-static {v0, v7}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isAlmostEquals(FF)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 3367
    const/4 v2, 0x0

    goto :goto_1

    .line 3370
    :cond_3
    div-float v4, v1, v6

    sub-float v4, v3, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingBottom:F

    sub-float/2addr v5, v6

    add-float v0, v4, v5

    .line 3371
    cmpg-float v4, v0, v7

    if-gez v4, :cond_1

    .line 3372
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    sub-float v2, v4, v0

    goto :goto_1
.end method

.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 3213
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 3214
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x1

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 3216
    :cond_0
    return-void
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 3220
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1302(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 3221
    return-void
.end method

.method public onScroll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 3231
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_APPLY_POS:I

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 3232
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 3225
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 3226
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 3227
    :cond_0
    return-void
.end method

.method protected procScrollStep()V
    .locals 6

    .prologue
    .line 3309
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    .line 3310
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    .line 3311
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 3346
    :cond_0
    :goto_0
    return-void

    .line 3314
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollOffset:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v1, v4, v5

    .line 3316
    .local v1, "nScrl":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    if-ge v0, v4, :cond_2

    .line 3317
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-lt v0, v4, :cond_3

    .line 3344
    :cond_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollStep:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->onScrollStep(I)V

    .line 3345
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->onScroll()V

    goto :goto_0

    .line 3320
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v0

    .line 3321
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_4

    .line 3316
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3323
    :cond_4
    const/4 v2, 0x0

    .line 3324
    .local v2, "needUpdate":Z
    :goto_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v1, :cond_5

    .line 3325
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    add-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 3326
    const/4 v2, 0x1

    goto :goto_3

    .line 3328
    :cond_5
    :goto_4
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    add-int/2addr v5, v1

    if-lt v4, v5, :cond_6

    .line 3329
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    sub-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 3330
    const/4 v2, 0x1

    goto :goto_4

    .line 3332
    :cond_6
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v4, :cond_a

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-ge v4, v5, :cond_a

    .line 3333
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    iget v5, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v5, :cond_7

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleLast:I

    .line 3334
    :cond_7
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    iget v5, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-le v4, v5, :cond_8

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mVisibleFirst:I

    .line 3335
    :cond_8
    if-eqz v2, :cond_9

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 3336
    :cond_9
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 3337
    const/4 v4, 0x1

    sget v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 3338
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    goto :goto_2

    .line 3340
    :cond_a
    const/4 v4, 0x0

    sget v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 3341
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v5, v5, -0x1

    and-int/2addr v4, v5

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    goto :goto_2
.end method

.method public reset()V
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 3057
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewTopPadding(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingTop:F

    .line 3058
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewTopPadding(I)F

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingBottom:F

    .line 3060
    const/4 v3, 0x0

    .line 3061
    .local v3, "selectionBarSize":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mGlAbsListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mMode:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    if-eq v4, v6, :cond_0

    .line 3062
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getSelectionModeLayoutHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v6

    mul-float v3, v4, v6

    .line 3066
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidth(I)F

    move-result v0

    .line 3067
    .local v0, "mWidthL":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v6, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v4, :cond_1

    move v4, v5

    :goto_0
    sub-float v4, v6, v4

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingLeft:F

    sub-float v1, v4, v6

    .line 3069
    .local v1, "mWidthR":F
    const/4 v2, 0x0

    .line 3070
    .local v2, "scroll_bar_w":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    cmpl-float v4, v4, v6

    if-lez v4, :cond_2

    .line 3071
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const v6, 0x7f0e0010

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->getResource(I)F
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3300(Lcom/sec/samsung/gallery/glview/GlPhotoView;I)F

    move-result v2

    .line 3075
    :goto_1
    cmpl-float v4, v2, v5

    if-lez v4, :cond_3

    .line 3076
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v4

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setThinkness(F)V

    .line 3080
    :goto_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingLeft:F

    sub-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingRight:F

    sub-float/2addr v6, v7

    sub-float/2addr v6, v0

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWAvailRatio:F
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v8

    sub-float/2addr v7, v8

    mul-float/2addr v6, v7

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setRightPadding(F)V

    .line 3081
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getScreenHeight()F

    move-result v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v6

    add-float/2addr v6, v3

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingTop:F

    add-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingBottom:F

    add-float/2addr v6, v7

    sub-float/2addr v4, v6

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVisibleH:F

    .line 3084
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v6

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewVerticalGap(I)F

    move-result v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    mul-float/2addr v4, v6

    sub-float v4, v1, v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    int-to-float v6, v6

    div-float/2addr v4, v6

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    .line 3086
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewHorizontalGap(I)F

    move-result v6

    add-float/2addr v4, v6

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapW:F

    .line 3087
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewVerticalGap(I)F

    move-result v6

    add-float/2addr v4, v6

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    .line 3089
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getThumbnailViewRowItemCount(I)I

    move-result v6

    iput v6, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRowCnt:I

    .line 3091
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    .line 3092
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRowCnt:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    .line 3093
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    mul-int/2addr v4, v6

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    .line 3097
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    neg-float v4, v4

    div-float/2addr v4, v9

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v6, :cond_4

    :goto_3
    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    div-float/2addr v5, v9

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingLeft:F

    add-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartX:F

    .line 3098
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v4, v9

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    div-float/2addr v5, v9

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v6

    add-float/2addr v5, v6

    add-float/2addr v5, v3

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->paddingTop:F

    add-float/2addr v5, v6

    sub-float/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartY:F

    .line 3100
    sget v4, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v4, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartZ:F

    .line 3102
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    .line 3103
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVVisibleCnt:I

    mul-int/2addr v4, v5

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->THUMB_ITEM_MAX:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3500()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    .line 3104
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVStart:F

    .line 3105
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVisibleH:F

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVEnd:F

    .line 3106
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v5

    add-float/2addr v5, v3

    invoke-virtual {v4, p0, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->resetScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    .line 3107
    return-void

    .end local v1    # "mWidthR":F
    .end local v2    # "scroll_bar_w":F
    :cond_1
    move v4, v0

    .line 3067
    goto/16 :goto_0

    .line 3073
    .restart local v1    # "mWidthR":F
    .restart local v2    # "scroll_bar_w":F
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const v6, 0x7f0e0011

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->getResource(I)F
    invoke-static {v4, v6}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3300(Lcom/sec/samsung/gallery/glview/GlPhotoView;I)F

    move-result v2

    goto/16 :goto_1

    .line 3078
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewScrollbarWidth()I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-virtual {v4, v6}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setThinkness(F)V

    goto/16 :goto_2

    :cond_4
    move v5, v0

    .line 3097
    goto :goto_3
.end method

.method public resetPitch()V
    .locals 5

    .prologue
    .line 3113
    :try_start_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    .line 3114
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 3115
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-lt v2, v4, :cond_1

    .line 3123
    .end local v0    # "count":I
    .end local v2    # "i":I
    :cond_0
    :goto_1
    return-void

    .line 3117
    .restart local v0    # "count":I
    .restart local v2    # "i":I
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v2

    .line 3118
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPitchFact:F
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1400(Lcom/sec/samsung/gallery/glview/GlPhotoView;)F

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3114
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 3120
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :catch_0
    move-exception v1

    .line 3121
    .local v1, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public resetRoll()V
    .locals 4

    .prologue
    .line 3129
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    :try_start_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemVisibleCnt:I

    if-ge v1, v3, :cond_0

    .line 3130
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v3, v3

    if-lt v1, v3, :cond_1

    .line 3139
    :cond_0
    :goto_1
    return-void

    .line 3133
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v3, v1

    .line 3134
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setRoll(F)V
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3129
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3136
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :catch_0
    move-exception v0

    .line 3137
    .local v0, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method public resetSet(II)V
    .locals 8
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I

    .prologue
    const/4 v0, 0x1

    .line 3235
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    .line 3236
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getItemRowCount()I

    move-result v5

    iput v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemRowCount:I

    .line 3238
    const/4 v1, 0x0

    .line 3239
    .local v1, "maxRange":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVisibleH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemRowCount:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 3240
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemRowCount:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollVisibleH:F

    sub-float v1, v5, v6

    .line 3242
    :cond_0
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    div-int v3, p1, v5

    .line 3243
    .local v3, "rowIndex":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getItemRowCount()I

    move-result v2

    .line 3244
    .local v2, "rowCount":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mFullVisibleCnt:I

    add-int/2addr v5, v3

    if-le v5, v2, :cond_1

    .line 3246
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    div-float v5, v1, v5

    float-to-int v3, v5

    .line 3248
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    int-to-float v6, v3

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    mul-float/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 3249
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v4, v3, v5

    .line 3251
    .local v4, "startRowIndex":I
    invoke-super {p0, v4, p2, v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 3253
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    div-int v5, p1, v5

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->getItemRowCount()I

    move-result v6

    if-lt v5, v6, :cond_3

    .line 3254
    .local v0, "isEndRowItem":Z
    :goto_0
    if-eqz v0, :cond_2

    .line 3255
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScrollRange:F

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->setScroll(F)V

    .line 3256
    :cond_2
    return-void

    .line 3253
    .end local v0    # "isEndRowItem":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 9
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 3158
    if-nez p1, :cond_0

    .line 3159
    const-string v6, "GlPhotoView"

    const-string v7, "setThumbPosition: The obj is null."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 3177
    :goto_0
    return-void

    .line 3165
    :cond_0
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    rem-int v0, v6, v7

    .line 3166
    .local v0, "col":I
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mColCnt:I

    div-int v1, v6, v7

    .line 3167
    .local v1, "row":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    .line 3168
    .local v3, "thumbWidth":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    .line 3169
    .local v2, "thumbHeight":F
    int-to-float v6, v0

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapW:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartX:F

    add-float v4, v6, v7

    .line 3170
    .local v4, "x":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartY:F

    int-to-float v7, v1

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    mul-float/2addr v7, v8

    sub-float v5, v6, v7

    .line 3172
    .local v5, "y":F
    iput v4, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDx:F

    .line 3173
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    add-float/2addr v6, v5

    iput v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDy:F

    .line 3174
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartZ:F

    iput v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDz:F

    .line 3175
    iput v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    .line 3176
    iput v2, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    goto :goto_0
.end method

.method public setThumbnailPitchRate(F)V
    .locals 0
    .param p1, "rate"    # F

    .prologue
    .line 3303
    return-void
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 10
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    .line 3181
    if-nez p1, :cond_1

    .line 3209
    :cond_0
    :goto_0
    return-void

    .line 3185
    :cond_1
    const/high16 v6, 0x3f800000    # 1.0f

    .line 3189
    .local v6, "alphaByActive":F
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemCount:I

    if-ge v0, v1, :cond_0

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 3190
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWindowCoordinateValue(F)F

    move-result v0

    float-to-int v8, v0

    .line 3191
    .local v8, "canvasWidth":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWindowCoordinateValue(F)F

    move-result v0

    float-to-int v7, v0

    .line 3193
    .local v7, "canvasHeight":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v0

    if-ne v0, v8, :cond_2

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v0

    if-eq v0, v7, :cond_3

    .line 3194
    :cond_2
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, v8, v7}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 3196
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    if-eqz v0, :cond_5

    .line 3197
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v4, 0x0

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    .line 3202
    .local v9, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :goto_1
    invoke-virtual {p1, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 3203
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->mViewResult:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 3204
    const/high16 v6, 0x3f000000    # 0.5f

    .line 3205
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->resetDim()V

    .line 3207
    :cond_4
    invoke-virtual {p1, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setDim(F)V

    goto/16 :goto_0

    .line 3200
    .end local v9    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    .restart local v9    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    goto :goto_1
.end method
