.class public Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "EventActionBarForEdit.java"


# instance fields
.field private isNeedScanLastShareAPPSupport:Z

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mIsSelectAll:Z

.field private mMenu:Landroid/view/Menu;

.field protected mQuantitySelectedAlbum:I

.field protected mQuantitySelectedItem:I

.field private mSelectionCount:Landroid/widget/TextView;

.field private needScanLastShareAPPSupportOneMoreTime:Z

.field private outOfShareMaxItemLimit:Z


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 60
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 48
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mIsSelectAll:Z

    .line 51
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedAlbum:I

    .line 52
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedItem:I

    .line 55
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 56
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 57
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 61
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;ZI)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method private checkNeedScanLastShareAppSupport()Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 234
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v1

    .line 235
    .local v1, "numberMarkItem":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v0

    .line 236
    .local v0, "numberImageSelected":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_2

    if-le v1, v2, :cond_2

    sget v4, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-ge v0, v4, :cond_2

    .line 238
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->outOfShareMaxItemLimit:Z

    if-eqz v4, :cond_1

    .line 239
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    .line 240
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 251
    :goto_0
    sget v4, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v0, v4, :cond_0

    .line 252
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->outOfShareMaxItemLimit:Z

    .line 255
    :cond_0
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    if-eqz v4, :cond_5

    .line 261
    :goto_1
    return v2

    .line 242
    :cond_1
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 244
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v1, :cond_3

    if-ne v1, v2, :cond_4

    .line 245
    :cond_3
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    .line 246
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 248
    :cond_4
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isNeedScanLastShareAPPSupport:Z

    goto :goto_0

    .line 257
    :cond_5
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    if-eqz v4, :cond_6

    .line 258
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->needScanLastShareAPPSupportOneMoreTime:Z

    goto :goto_1

    :cond_6
    move v2, v3

    .line 261
    goto :goto_1
.end method

.method private isValidSelection()Z
    .locals 7

    .prologue
    .line 214
    const/4 v0, 0x1

    .line 216
    .local v0, "isValid":Z
    iget v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedAlbum:I

    if-nez v2, :cond_1

    .line 217
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0113

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 218
    const/4 v0, 0x0

    .line 226
    :cond_0
    :goto_0
    return v0

    .line 219
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedItem:I

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-gt v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v2

    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v2, v3, :cond_0

    .line 220
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    sget v6, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "text":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 223
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 66
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 67
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120021

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 68
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v3, 0x1

    .line 113
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 115
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 120
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 124
    :sswitch_2
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 127
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    goto :goto_0

    .line 130
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 133
    :sswitch_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_STORY_ALBUM_APP"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 136
    :sswitch_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->isValidSelection()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 137
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v1, v3, v3}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto :goto_0

    .line 141
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 146
    :sswitch_7
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x0

    aput-object v1, v0, v3

    .line 147
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "S_STUDIO"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 113
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_6
        0x7f0f026e -> :sswitch_2
        0x7f0f026f -> :sswitch_1
        0x7f0f0270 -> :sswitch_3
        0x7f0f0275 -> :sswitch_5
        0x7f0f027b -> :sswitch_2
        0x7f0f0283 -> :sswitch_7
        0x7f0f02bc -> :sswitch_4
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 9
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v8, 0x7f0f0275

    const v7, 0x7f0f026d

    const v6, 0x7f0f026c

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    .line 73
    const v2, 0x7f0f02bb

    invoke-interface {p1, v2, v5}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedAlbumItem()J

    move-result-wide v0

    .line 75
    .local v0, "supportOperation":J
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->checkNeedScanLastShareAppSupport()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 76
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v2, v3, v0, v1, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v0

    .line 80
    :goto_0
    invoke-static {p1, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 82
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-ne v2, v5, :cond_0

    .line 83
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 84
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v2, v6, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    const v3, 0x7f020030

    invoke-static {v2, v6, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    .line 89
    :cond_0
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 91
    :cond_2
    invoke-static {p1, v7, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 94
    :cond_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseStoryAlbum:Z

    if-eqz v2, :cond_a

    .line 95
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;->getStoryAlbumMaxItemCount(Landroid/content/Context;)I

    move-result v3

    if-gt v2, v3, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v2

    if-ge v2, v5, :cond_5

    .line 97
    :cond_4
    invoke-static {p1, v8, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    :cond_5
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 103
    :cond_6
    const v2, 0x7f0f0283

    invoke-static {p1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 105
    :cond_7
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v2, :cond_8

    .line 106
    invoke-static {p1, v7, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 108
    :cond_8
    const v2, 0x7f0f027b

    invoke-static {p1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 109
    return-void

    .line 78
    :cond_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;

    invoke-static {v2, v3, v0, v1, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v0

    goto/16 :goto_0

    .line 100
    :cond_a
    invoke-static {p1, v8, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1
.end method

.method public resetSelectAll()V
    .locals 1

    .prologue
    .line 230
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mIsSelectAll:Z

    .line 231
    return-void
.end method

.method protected setSelectedItemCount(I)V
    .locals 0
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 210
    iput p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedItem:I

    .line 211
    return-void
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$1;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 184
    return-void
.end method

.method protected updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
    .locals 2
    .param p1, "button"    # Lcom/sec/samsung/gallery/util/Consts$ButtonType;
    .param p2, "visible"    # I
    .param p3, "toggleImage"    # Z

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;

    invoke-direct {v1, p0, p1, p3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;Lcom/sec/samsung/gallery/util/Consts$ButtonType;Z)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 206
    return-void
.end method
