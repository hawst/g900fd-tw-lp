.class Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;
.super Ljava/lang/Object;
.source "GlLayerTransitionAnimation.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlickInAnimation"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;

    .prologue
    .line 132
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V

    return-void
.end method


# virtual methods
.method public applyTransform(F)V
    .locals 14
    .param p1, "ratio"    # F

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    .line 135
    sub-float v2, v13, p1

    .line 136
    .local v2, "inRatio":F
    mul-float v7, v2, v2

    sub-float v6, v13, v7

    .line 137
    .local v6, "proRatio":F
    const v7, 0x3e4ccccd    # 0.2f

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F

    move-result v8

    mul-float/2addr v7, v8

    mul-float v5, v7, v6

    .line 145
    .local v5, "oldDelta":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F

    move-result v7

    float-to-double v8, p1

    const-wide v10, 0x3fc99999a0000000L    # 0.20000000298023224

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    double-to-float v8, v8

    sub-float v8, v13, v8

    mul-float v3, v7, v8

    .line 151
    .local v3, "newDelta":F
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$500(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v7

    if-ge v1, v7, :cond_0

    .line 152
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$600(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 153
    .local v4, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldAlphaHnd:[I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$700(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v7

    aget v7, v7, v1

    invoke-virtual {v4, v2, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(FI)V

    .line 154
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$800(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v7

    aget v7, v7, v1

    neg-float v8, v5

    invoke-virtual {v4, v7, v8, v12, v12}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 151
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 156
    .end local v4    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    const/high16 v7, 0x41000000    # 8.0f

    mul-float/2addr v7, p1

    invoke-static {v7, v13}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 157
    .local v0, "alpha":F
    const/4 v1, 0x0

    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$900(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v7

    if-ge v1, v7, :cond_1

    .line 158
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1000(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 159
    .restart local v4    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1100(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v7

    aget v7, v7, v1

    invoke-virtual {v4, v0, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(FI)V

    .line 160
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1200(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v7

    aget v7, v7, v1

    invoke-virtual {v4, v7, v3, v12, v12}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 157
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 162
    .end local v4    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    return-void
.end method
