.class Lcom/sec/samsung/gallery/glview/GlPhotoView$16;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlDragListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2100
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDrop(Lcom/sec/android/gallery3d/glcore/GlObject;Landroid/net/Uri;)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    const v4, 0x7f0e02e4

    const/4 v3, 0x0

    .line 2103
    move-object v1, p1

    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 2104
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 2108
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    if-eqz v1, :cond_1

    if-ltz v0, :cond_1

    .line 2109
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput v0, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFocused:I

    .line 2110
    instance-of v1, p1, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v1, :cond_3

    .line 2111
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2112
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mListenerAlbumClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;->onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2113
    iget-boolean v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z

    if-eqz v1, :cond_2

    .line 2114
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->localDrag(I)V

    .line 2127
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2, v0, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2129
    :cond_1
    const/4 v1, 0x1

    return v1

    .line 2118
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 2123
    :cond_3
    iget-boolean v1, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->isLacalDrag:Z

    if-nez v1, :cond_0

    .line 2125
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$16;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method
