.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;
.super Landroid/os/Handler;
.source "MoreInfoUserTagEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 9
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 58
    iget v4, p1, Landroid/os/Message;->what:I

    packed-switch v4, :pswitch_data_0

    .line 70
    :cond_0
    :goto_0
    return-void

    .line 60
    :pswitch_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 61
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0e004e

    new-array v7, v3, [Ljava/lang/Object;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    move-result-object v8

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-virtual {v5, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 63
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getData()Ljava/util/ArrayList;

    move-result-object v1

    .line 64
    .local v1, "selectedTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUserTagList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eq v4, v5, :cond_2

    :cond_1
    move v0, v3

    .line 66
    .local v0, "chaged":Z
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v4

    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mComposeAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getCount()I

    move-result v5

    if-lez v5, :cond_3

    :goto_2
    invoke-virtual {v4, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateDoneButton(Z)V

    goto :goto_0

    .end local v0    # "chaged":Z
    :cond_2
    move v0, v2

    .line 64
    goto :goto_1

    .restart local v0    # "chaged":Z
    :cond_3
    move v3, v2

    .line 66
    goto :goto_2

    .line 58
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
