.class public Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartCategoryTagCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$4;,
        Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;,
        Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    }
.end annotation


# static fields
.field private static final BASE_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/AlertDialog;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;

    .line 50
    const-string v0, "external"

    invoke-static {v0}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 335
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private dismissRemoveTagDialog()V
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 324
    :cond_0
    return-void
.end method

.method private getSelectedList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 308
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 309
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 310
    .local v3, "selectedItems":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v4, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 312
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 313
    .local v2, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 314
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 315
    .local v1, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 311
    .end local v1    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 318
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    return-object v4
.end method

.method private requestEditCategory(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V
    .locals 15
    .param p2, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "editType"    # Ljava/lang/String;
    .param p5, "action"    # Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 205
    .local p1, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v6

    .line 209
    .local v6, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 210
    .local v12, "mediaSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    move-object/from16 v0, p2

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_2

    .line 211
    move-object/from16 v0, p2

    invoke-virtual {v12, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 214
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 215
    .local v7, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 216
    .local v13, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v13, :cond_3

    instance-of v1, v13, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_3

    move-object v10, v13

    .line 217
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 218
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v14

    .line 219
    .local v14, "uri":Ljava/lang/String;
    invoke-virtual {v7, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 223
    .end local v10    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v13    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v14    # "uri":Ljava/lang/String;
    :cond_4
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 224
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p4

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v8, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    invoke-direct {v8, v1, v11}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 226
    .local v8, "dialog":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog;
    new-instance v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;

    move-object v2, p0

    move-object/from16 v3, p4

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-direct/range {v1 .. v7}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual {v8, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog;->registerResultCallBack(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog$Result;)V

    .line 245
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCategoryPickerDialog;->show()V

    goto :goto_0
.end method

.method private requestRemoveConfirmTag(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V
    .locals 22
    .param p2, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p5, "removeType"    # Ljava/lang/String;
    .param p6, "action"    # Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 251
    .local p1, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 252
    .local v6, "mediaSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v7

    .line 253
    .local v7, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_1

    .line 254
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .local v14, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 255
    .local v17, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object/from16 v0, v17

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_0

    .line 256
    check-cast v17, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v17    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object/from16 v0, v17

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 260
    .end local v14    # "i$":Ljava/util/Iterator;
    :cond_1
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 261
    .local v8, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_3

    .line 262
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v14

    .restart local v14    # "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 263
    .restart local v17    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v17, :cond_2

    move-object/from16 v0, v17

    instance-of v2, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v2, :cond_2

    move-object/from16 v15, v17

    .line 264
    check-cast v15, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 265
    .local v15, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    .line 266
    .local v19, "uri":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 269
    .end local v14    # "i$":Ljava/util/Iterator;
    .end local v15    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v17    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v19    # "uri":Ljava/lang/String;
    :cond_3
    sget-object v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    move-object/from16 v0, p6

    if-eq v0, v2, :cond_4

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    move-object/from16 v0, p6

    if-ne v0, v2, :cond_5

    .line 270
    :cond_4
    move-object/from16 v15, p4

    .line 271
    .restart local v15    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->BASE_URI:Landroid/net/Uri;

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    .line 272
    .restart local v19    # "uri":Ljava/lang/String;
    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 275
    .end local v15    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v19    # "uri":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v18

    .line 276
    .local v18, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 277
    .local v11, "category_name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0463

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v11, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 279
    .local v13, "dialogTitle":Ljava/lang/String;
    const/16 v16, 0x1

    .line 280
    .local v16, "numSelected":I
    const/4 v12, 0x0

    .line 281
    .local v12, "dialogMessage":Ljava/lang/String;
    if-eqz p1, :cond_6

    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-eqz v2, :cond_6

    .line 282
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 285
    :cond_6
    const/4 v2, 0x1

    move/from16 v0, v16

    if-ne v0, v2, :cond_7

    .line 286
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0044

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 290
    :goto_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v13}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v12}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0e0046

    new-instance v4, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$3;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v20

    const v21, 0x7f0e0335

    new-instance v2, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$2;

    move-object/from16 v3, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v9, p5

    move-object/from16 v10, p6

    invoke-direct/range {v2 .. v10}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mDialog:Landroid/app/AlertDialog;

    .line 304
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 305
    return-void

    .line 288
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0045

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    goto :goto_2
.end method

.method public static requestUpdateScreenByCategoryChange(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 5
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 328
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->UPDATE_SCREEN_CAHNGE_CATEGORY:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 329
    .local v0, "actionType":Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    aput-object v4, v1, v3

    const/4 v2, 0x2

    aput-object v4, v1, v2

    const/4 v2, 0x3

    aput-object v0, v1, v2

    const/4 v2, 0x4

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 332
    .local v1, "params":[Ljava/lang/Object;
    invoke-static {p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_CATEGORY_TAG"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 333
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 27
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 60
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object/from16 v23, v1

    check-cast v23, [Ljava/lang/Object;

    .line 61
    .local v23, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v23, v1

    check-cast v1, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    .line 62
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 63
    const/4 v1, 0x1

    aget-object v20, v23, v1

    check-cast v20, Ljava/util/ArrayList;

    .line 64
    .local v20, "contents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v1, 0x2

    aget-object v2, v23, v1

    check-cast v2, Ljava/util/ArrayList;

    .line 65
    .local v2, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v1, 0x3

    aget-object v7, v23, v1

    check-cast v7, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 66
    .local v7, "action":Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    const/4 v1, 0x4

    aget-object v1, v23, v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v24

    .line 68
    .local v24, "show":Z
    if-nez v7, :cond_1

    .line 69
    if-nez v24, :cond_0

    .line 70
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->dismissRemoveTagDialog()V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const/16 v22, 0x0

    .line 76
    .local v22, "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v3, 0x0

    .line 77
    .local v3, "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v4, 0x0

    .line 78
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v12, 0x0

    .line 80
    .local v12, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v20, :cond_3

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 81
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    if-eq v7, v1, :cond_2

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    if-ne v7, v1, :cond_4

    .line 82
    :cond_2
    const/4 v1, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v22, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 83
    .restart local v22    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v1, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    .end local v12    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v12, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 89
    .restart local v12    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v25

    .line 90
    .local v25, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$4;->$SwitchMap$com$sec$samsung$gallery$controller$StartCategoryTagCmd$CategoryAction:[I

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->ordinal()I

    move-result v5

    aget v1, v1, v5

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 92
    :pswitch_0
    if-nez v24, :cond_5

    .line 93
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->dismissRemoveTagDialog()V

    goto :goto_0

    .line 85
    .end local v25    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_4
    const/4 v1, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v22

    .end local v22    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v22, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 86
    .restart local v22    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v1, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaSet;

    .restart local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    goto :goto_1

    .line 96
    .restart local v25    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_5
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    if-eqz v1, :cond_6

    .line 97
    move-object v3, v4

    .line 98
    :cond_6
    const/4 v4, 0x0

    .line 99
    const/4 v6, 0x0

    .line 100
    .local v6, "removeType":Ljava/lang/String;
    if-eqz v3, :cond_7

    .line 101
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 102
    :cond_7
    const/4 v5, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestRemoveConfirmTag(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 103
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto :goto_0

    .line 109
    .end local v6    # "removeType":Ljava/lang/String;
    :pswitch_1
    if-nez v24, :cond_8

    .line 110
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->dismissRemoveTagDialog()V

    goto :goto_0

    .line 113
    :cond_8
    move-object/from16 v3, v22

    .line 114
    const/16 v22, 0x0

    .line 115
    const/4 v6, 0x0

    .line 116
    .restart local v6    # "removeType":Ljava/lang/String;
    if-eqz v3, :cond_a

    .line 117
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 120
    :cond_9
    :goto_2
    const/4 v5, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestRemoveConfirmTag(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 121
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto/16 :goto_0

    .line 118
    :cond_a
    if-eqz v4, :cond_9

    .line 119
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v6

    goto :goto_2

    .line 128
    .end local v6    # "removeType":Ljava/lang/String;
    :pswitch_2
    if-nez v24, :cond_b

    .line 129
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->dismissRemoveTagDialog()V

    goto/16 :goto_0

    .line 132
    :cond_b
    move-object/from16 v3, v22

    .line 133
    const/4 v6, 0x0

    .line 134
    .restart local v6    # "removeType":Ljava/lang/String;
    const/16 v22, 0x0

    .line 135
    if-eqz v3, :cond_c

    .line 136
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v6

    .line 137
    :cond_c
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    if-ne v7, v1, :cond_d

    .line 138
    const-string v6, "People"

    .line 139
    :cond_d
    const/4 v11, 0x0

    move-object/from16 v8, p0

    move-object v9, v2

    move-object v10, v3

    move-object v13, v6

    move-object v14, v7

    invoke-direct/range {v8 .. v14}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestRemoveConfirmTag(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 140
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto/16 :goto_0

    .line 146
    .end local v6    # "removeType":Ljava/lang/String;
    :pswitch_3
    if-nez v24, :cond_e

    .line 147
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->dismissRemoveTagDialog()V

    goto/16 :goto_0

    .line 150
    :cond_e
    move-object/from16 v3, v22

    .line 151
    const/16 v22, 0x0

    .line 153
    const/16 v17, 0x0

    const-string v18, "People"

    move-object/from16 v13, p0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move-object/from16 v19, v7

    invoke-direct/range {v13 .. v19}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestRemoveConfirmTag(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 154
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto/16 :goto_0

    .line 160
    :pswitch_4
    move-object/from16 v3, v22

    .line 161
    const/16 v22, 0x0

    .line 162
    const-string v17, "People"

    move-object/from16 v13, p0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move-object/from16 v18, v7

    invoke-direct/range {v13 .. v18}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestEditCategory(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 163
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto/16 :goto_0

    .line 169
    :pswitch_5
    move-object/from16 v3, v22

    .line 170
    const/16 v22, 0x0

    .line 171
    const/16 v17, 0x0

    .line 172
    .local v17, "editType":Ljava/lang/String;
    if-eqz v3, :cond_f

    .line 173
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getKey()Ljava/lang/String;

    move-result-object v17

    :cond_f
    move-object/from16 v13, p0

    move-object v14, v2

    move-object v15, v3

    move-object/from16 v16, v4

    move-object/from16 v18, v7

    .line 174
    invoke-direct/range {v13 .. v18}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestEditCategory(Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V

    .line 175
    const/4 v1, 0x1

    move-object/from16 v0, v25

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto/16 :goto_0

    .line 181
    .end local v17    # "editType":Ljava/lang/String;
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v5, "UPDATE_CATEGORY"

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v1, v5, v8}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 182
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v21

    .line 183
    .local v21, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v26

    .line 184
    .local v26, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v26

    if-ne v0, v1, :cond_11

    move-object/from16 v0, v21

    instance-of v1, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-nez v1, :cond_10

    move-object/from16 v0, v21

    instance-of v1, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v1, :cond_11

    .line 186
    :cond_10
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;

    const-string v5, "UPDATE_SCREEN_CAHNGE_CATEGORY : CLUSTER_BY_FACE"

    invoke-static {v1, v5}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v1

    const/4 v5, 0x3

    invoke-interface {v1, v5}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    .line 189
    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty()V

    .line 193
    :goto_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v5, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    const/4 v8, 0x0

    invoke-virtual {v1, v5, v8}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto/16 :goto_0

    .line 191
    :cond_11
    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;

    const-string v5, "UPDATE_SCREEN_CAHNGE_CATEGORY : CATEGORY "

    invoke-static {v1, v5}, Lcom/sec/android/gallery3d/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
