.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mwLayoutListener(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 321
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    .prologue
    .line 324
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 342
    :goto_0
    return-void

    .line 327
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 328
    .local v0, "multiWindowSize":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mScreenSize:Landroid/graphics/Point;

    if-eqz v1, :cond_2

    .line 329
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mScreenSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    iget v2, v0, Landroid/graphics/Point;->x:I

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mScreenSize:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v2, v0, Landroid/graphics/Point;->y:I

    if-eq v1, v2, :cond_2

    .line 330
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateSCanvasViewLayout()V

    .line 334
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mIsInMultiWindow:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$100(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Z

    move-result v2

    if-eq v1, v2, :cond_3

    .line 335
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->updateSCanvasViewLayout()V

    .line 336
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->updateButtonsTextVisibiltiy()V

    .line 337
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v1

    const v2, 0x7f0f0148

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonHintPopup(I)V

    .line 338
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v1

    const v2, 0x7f0f0146

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setButtonHintPopup(I)V

    .line 340
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iput-object v0, v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mScreenSize:Landroid/graphics/Point;

    .line 341
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v2

    # setter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mIsInMultiWindow:Z
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$102(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;Z)Z

    goto :goto_0
.end method
