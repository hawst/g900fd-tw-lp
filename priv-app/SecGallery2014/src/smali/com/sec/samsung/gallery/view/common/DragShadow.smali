.class public Lcom/sec/samsung/gallery/view/common/DragShadow;
.super Landroid/view/View$DragShadowBuilder;
.source "DragShadow.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected bmpHeight:I

.field protected bmpWidth:I

.field protected mContext:Landroid/content/Context;

.field protected mHeight:I

.field protected mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field protected mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field protected mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/view/common/DragShadow;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/common/DragShadow;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 23
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    .line 24
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    .line 25
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->bmpWidth:I

    .line 26
    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->bmpHeight:I

    .line 30
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mContext:Landroid/content/Context;

    .line 31
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 32
    if-eqz p3, :cond_0

    .line 33
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    .line 34
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->bmpWidth:I

    .line 35
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->bmpHeight:I

    .line 37
    :cond_0
    return-void
.end method


# virtual methods
.method protected calculateSize()V
    .locals 2

    .prologue
    .line 64
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v1, :cond_0

    .line 65
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 67
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0d0229

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    .line 68
    const v1, 0x7f0d0228

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    .line 74
    .end local v0    # "res":Landroid/content/res/Resources;
    :cond_0
    return-void
.end method

.method public onDrawShadow(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 61
    :cond_0
    return-void
.end method

.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 8
    .param p1, "size"    # Landroid/graphics/Point;
    .param p2, "touch"    # Landroid/graphics/Point;

    .prologue
    .line 41
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    move-result-object v0

    .line 42
    .local v0, "config":Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/DragShadow;->calculateSize()V

    .line 44
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_1

    .line 45
    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/view/common/DragShadow;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mShadowBitmapDrawable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mThumbObject:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 53
    iget v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {p2, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 54
    return-void

    .line 47
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget v2, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    iget v3, v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_left_margin:I

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v3

    div-int/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    iget v4, v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_top_margin:I

    mul-int/2addr v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v4

    div-int/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mWidth:I

    iget v6, v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_right_margin:I

    mul-int/2addr v5, v6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v6

    div-int/2addr v5, v6

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    iget v6, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mHeight:I

    iget v7, v0, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->photoview_bottom_margin:I

    mul-int/2addr v6, v7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/common/DragShadow;->mThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v7

    div-int/2addr v6, v7

    sub-int/2addr v5, v6

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    goto :goto_0
.end method
