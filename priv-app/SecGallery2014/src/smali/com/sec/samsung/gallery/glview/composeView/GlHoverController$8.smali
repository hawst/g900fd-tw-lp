.class Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;
.super Ljava/lang/Object;
.source "GlHoverController.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0

    .prologue
    .line 1114
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 9
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v8, 0x1

    .line 1116
    check-cast p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local p1    # "object":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v1, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1120
    .local v1, "index":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    move-result-object v4

    if-nez v4, :cond_1

    .line 1134
    :cond_0
    :goto_0
    return v8

    .line 1122
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mDisplayItemList:[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2000(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)[Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;

    move-result-object v4

    aget-object v0, v4, v1

    .line 1123
    .local v0, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;
    if-eqz v0, :cond_0

    .line 1125
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getModeAirViewSoundAndHaptic(Landroid/content/Context;)I

    move-result v3

    .line 1126
    .local v3, "supportAirViewSound":I
    if-eq v3, v8, :cond_2

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    .line 1127
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$500(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 1129
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 1130
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIsSet:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1131
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v6

    shl-int/lit8 v6, v6, 0x10

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mFirstIndex:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2200(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v7

    add-int/2addr v7, v1

    or-int/2addr v6, v7

    invoke-interface {v4, v2, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;->hoverClick(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)Z

    goto :goto_0

    .line 1133
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1900(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;

    move-result-object v4

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/SingleImageItem;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mIndex:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$2100(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v6

    invoke-interface {v4, v2, v5, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;->hoverClick(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)Z

    goto :goto_0
.end method
