.class Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;
.super Ljava/lang/Object;
.source "DeleteHistoryForGallerySearchActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->init()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "l_position"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 136
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;

    .line 137
    .local v0, "item":Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->getChecked()Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryItem;->setChecked(Z)V

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$HistoryListAdapter;->notifyDataSetChanged()V

    .line 140
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateSelectAll()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    .line 142
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v1, :cond_1

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->isAllChecked()Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 149
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v1, v3

    .line 137
    goto :goto_0

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_1
.end method
