.class public Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartImageNoteCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 9
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 24
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-nez v4, :cond_0

    .line 52
    :goto_0
    return-void

    .line 27
    :cond_0
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 28
    .local v2, "params":[Ljava/lang/Object;
    aget-object v4, v2, v6

    check-cast v4, Landroid/app/Activity;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->mActivity:Landroid/app/Activity;

    .line 29
    aget-object v1, v2, v5

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 30
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v4, 0x2

    aget-object v4, v2, v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 32
    .local v3, "type":I
    if-nez v1, :cond_1

    .line 33
    sget-object v4, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->TAG:Ljava/lang/String;

    const-string v5, "MediaItem is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 36
    :cond_1
    instance-of v4, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v4, :cond_2

    .line 37
    sget-object v4, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "MediaItem must be a local image: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 41
    :cond_2
    const/4 v0, 0x0

    .line 42
    .local v0, "intent":Landroid/content/Intent;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_3

    .line 43
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->mActivity:Landroid/app/Activity;

    const-class v8, Lcom/sec/samsung/gallery/app/photonote/PhotoNote;

    invoke-direct {v0, v4, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 47
    .restart local v0    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 48
    const-string/jumbo v8, "view_mode"

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNoteEditor:Z

    if-nez v4, :cond_5

    move v4, v5

    :goto_1
    invoke-virtual {v0, v8, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 49
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v4, :cond_6

    const-string/jumbo v4, "type"

    :goto_2
    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 50
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->mActivity:Landroid/app/Activity;

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v5, :cond_4

    const/16 v5, 0x500

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    :cond_4
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v4, v0, v5}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/StartImageNoteCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v6, v6}, Landroid/app/Activity;->overridePendingTransition(II)V

    goto/16 :goto_0

    :cond_5
    move v4, v6

    .line 48
    goto :goto_1

    :cond_6
    move-object v4, v7

    .line 49
    goto :goto_2
.end method
