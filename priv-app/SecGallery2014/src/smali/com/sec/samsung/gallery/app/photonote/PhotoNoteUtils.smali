.class public Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;
.super Ljava/lang/Object;
.source "PhotoNoteUtils.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkEnableSpen(Landroid/content/Context;)I
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 58
    const/4 v1, 0x0

    .line 60
    .local v1, "result":I
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initializeSPenSDK(Landroid/content/Context;)I
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 64
    :goto_0
    return v1

    .line 61
    :catch_0
    move-exception v0

    .line 62
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    sget-object v2, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->TAG:Ljava/lang/String;

    const-string/jumbo v3, "spen sdk 3.0 is not supported"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static checkPhotoNote(Ljava/lang/String;)Z
    .locals 10
    .param p0, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 77
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-nez v7, :cond_1

    .line 100
    :cond_0
    :goto_0
    return v6

    .line 81
    :cond_1
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 85
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 86
    .local v0, "checkTime":J
    const/4 v5, 0x0

    .line 87
    .local v5, "note_info":I
    const/4 v3, 0x0

    .line 89
    .local v3, "file":Ljava/io/RandomAccessFile;
    :try_start_0
    new-instance v4, Ljava/io/RandomAccessFile;

    const-string v7, "r"

    invoke-direct {v4, p0, v7}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .local v4, "file":Ljava/io/RandomAccessFile;
    :try_start_1
    invoke-static {v4}, Lcom/sec/samsung/gallery/app/photonote/PhotoNoteUtils;->getPhotoNoteType(Ljava/io/RandomAccessFile;)I
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v5

    .line 94
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v3, v4

    .line 97
    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v0, v8, v0

    .line 98
    const-string v7, "Gallery_Performance"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "checkPhotoNote End spent time["

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "  msec ]"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    if-lez v5, :cond_0

    const/4 v6, 0x1

    goto :goto_0

    .line 91
    :catch_0
    move-exception v2

    .line 92
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_2
    :try_start_2
    invoke-virtual {v2}, Ljava/io/FileNotFoundException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    goto :goto_1

    .end local v2    # "e":Ljava/io/FileNotFoundException;
    :catchall_0
    move-exception v6

    :goto_3
    invoke-static {v3}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v6

    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .restart local v4    # "file":Ljava/io/RandomAccessFile;
    :catchall_1
    move-exception v6

    move-object v3, v4

    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    goto :goto_3

    .line 91
    .end local v3    # "file":Ljava/io/RandomAccessFile;
    .restart local v4    # "file":Ljava/io/RandomAccessFile;
    :catch_1
    move-exception v2

    move-object v3, v4

    .end local v4    # "file":Ljava/io/RandomAccessFile;
    .restart local v3    # "file":Ljava/io/RandomAccessFile;
    goto :goto_2
.end method

.method public static copyFile(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 11
    .param p0, "srcFilePath"    # Ljava/lang/String;
    .param p1, "destFilePath"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 23
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 24
    .local v3, "file":Ljava/io/File;
    const/4 v4, 0x0

    .line 25
    .local v4, "in":Ljava/io/FileInputStream;
    const/4 v6, 0x0

    .line 28
    .local v6, "out":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 29
    .end local v4    # "in":Ljava/io/FileInputStream;
    .local v5, "in":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v7, Ljava/io/FileOutputStream;

    const/4 v9, 0x0

    invoke-direct {v7, p1, v9}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 30
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .local v7, "out":Ljava/io/FileOutputStream;
    const/4 v1, 0x0

    .line 31
    .local v1, "bytesRead":I
    const/16 v9, 0x400

    :try_start_2
    new-array v0, v9, [B

    .line 32
    .local v0, "buffer":[B
    :goto_0
    const/4 v9, 0x0

    const/16 v10, 0x400

    invoke-virtual {v5, v0, v9, v10}, Ljava/io/FileInputStream;->read([BII)I

    move-result v1

    const/4 v9, -0x1

    if-eq v1, v9, :cond_0

    .line 33
    const/4 v9, 0x0

    invoke-virtual {v7, v0, v9, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 36
    .end local v0    # "buffer":[B
    :catch_0
    move-exception v2

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .line 37
    .end local v1    # "bytesRead":I
    .end local v5    # "in":Ljava/io/FileInputStream;
    .local v2, "e":Ljava/io/IOException;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    :goto_1
    :try_start_3
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 40
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 41
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .end local v2    # "e":Ljava/io/IOException;
    :goto_2
    return v8

    .line 35
    .end local v4    # "in":Ljava/io/FileInputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v1    # "bytesRead":I
    .restart local v5    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :cond_0
    const/4 v8, 0x1

    .line 40
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 41
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    goto :goto_2

    .line 40
    .end local v0    # "buffer":[B
    .end local v1    # "bytesRead":I
    :catchall_0
    move-exception v8

    :goto_3
    invoke-static {v4}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    .line 41
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Ljava/io/Closeable;)V

    throw v8

    .line 40
    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    :catchall_1
    move-exception v8

    move-object v4, v5

    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .end local v4    # "in":Ljava/io/FileInputStream;
    .end local v6    # "out":Ljava/io/FileOutputStream;
    .restart local v1    # "bytesRead":I
    .restart local v5    # "in":Ljava/io/FileInputStream;
    .restart local v7    # "out":Ljava/io/FileOutputStream;
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "out":Ljava/io/FileOutputStream;
    .restart local v6    # "out":Ljava/io/FileOutputStream;
    move-object v4, v5

    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    goto :goto_3

    .line 36
    .end local v1    # "bytesRead":I
    :catch_1
    move-exception v2

    goto :goto_1

    .end local v4    # "in":Ljava/io/FileInputStream;
    .restart local v5    # "in":Ljava/io/FileInputStream;
    :catch_2
    move-exception v2

    move-object v4, v5

    .end local v5    # "in":Ljava/io/FileInputStream;
    .restart local v4    # "in":Ljava/io/FileInputStream;
    goto :goto_1
.end method

.method public static getPhotoNoteType(Ljava/io/RandomAccessFile;)I
    .locals 2
    .param p0, "file"    # Ljava/io/RandomAccessFile;

    .prologue
    .line 68
    const/4 v0, 0x0

    .line 70
    .local v0, "result":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/SPDChecker;->isValid(Ljava/io/RandomAccessFile;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 71
    const/4 v0, 0x1

    .line 73
    :cond_0
    return v0
.end method

.method public static rotateNoteDoc(Landroid/content/Context;Ljava/lang/String;D)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "filePath"    # Ljava/lang/String;
    .param p2, "rotation"    # D

    .prologue
    .line 46
    const/4 v7, 0x0

    .line 48
    .local v7, "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_start_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->initializeSPenSDK(Landroid/content/Context;)I

    .line 49
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    const/4 v6, 0x1

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/lang/String;DI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 50
    .end local v7    # "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .local v1, "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_start_1
    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->attachToFile(Ljava/lang/String;)V

    .line 51
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 55
    :goto_0
    return-void

    .line 52
    .end local v1    # "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .restart local v7    # "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 53
    .end local v7    # "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .local v0, "e":Ljava/lang/Exception;
    .restart local v1    # "note":Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 52
    .end local v0    # "e":Ljava/lang/Exception;
    :catch_1
    move-exception v0

    goto :goto_1
.end method
