.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1074
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 8
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v7, 0x1

    .line 1076
    iget-object v4, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1079
    .local v1, "index":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickEnabled:Z

    if-nez v4, :cond_1

    .line 1104
    :cond_0
    :goto_0
    return v7

    .line 1080
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isIdle()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1081
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v4, :cond_0

    .line 1085
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v4

    if-eqz v4, :cond_3

    .line 1086
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->removeView()V

    .line 1089
    :cond_3
    shr-int/lit8 v0, v1, 0x10

    .line 1090
    .local v0, "albumIndex":I
    const v4, 0xffff

    and-int v2, v1, v4

    .line 1092
    .local v2, "photoIndex":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    .line 1093
    .local v3, "soundUtils":Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMode:I

    if-nez v4, :cond_6

    .line 1094
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 1099
    :cond_4
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 1100
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v4, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v5, v6, v4, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1103
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$5;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-interface {v4, v5, v6, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    goto :goto_0

    .line 1096
    :cond_6
    if-eqz v3, :cond_4

    .line 1097
    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    goto :goto_1
.end method
