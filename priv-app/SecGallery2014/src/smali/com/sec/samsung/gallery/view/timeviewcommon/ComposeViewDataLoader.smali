.class public Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;
.super Ljava/lang/Object;
.source "ComposeViewDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;,
        Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    }
.end annotation


# static fields
.field public static final ALBUM_SET_SIZE:I = 0xc8

.field public static final FIRST_CACHE_COUNT:I = 0x20

.field public static final GET_COVER_ITEM_LIMIT:I = 0x3

.field public static final INVALID_UPDATE:B = -0x1t

.field public static final INVALID_VALUE:I = -0x1

.field public static final SLIDE_ALBUM_INDEX:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field public static final THM_SIZE_MICRO:B = 0x2t

.field public static final THM_SIZE_MINI_MICRO:B = 0x1t

.field public static final THM_SIZE_THUMBNAIL:B = 0x3t

.field public static final THM_SIZE_TYPE_ALBUM:B = 0x10t

.field public static final THM_SIZE_TYPE_LEVEL:B = 0x20t

.field public static final mLOCK:Ljava/lang/Object;


# instance fields
.field public mActiveEnd:I

.field public mActiveStart:I

.field private mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field public mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

.field private mAllSet:Z

.field private mCheckSetVersion:Z

.field public mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

.field public mContentEnd:I

.field public mContentStart:I

.field protected final mContext:Landroid/content/Context;

.field private mCurrentSlide:I

.field public mFirstIndex:I

.field private mFirstItemOnly:Z

.field private mFirstLoad:Z

.field public mFirstLoadColumnCount:I

.field public mFirstLoadCount:I

.field public mFirstLoadRowCount:I

.field public mFirstReloadSkip:Z

.field private mIsActive:Z

.field private mIsActiveRange:Z

.field public mItemSize:I

.field private mLatestCount:I

.field public mLevel:B

.field private mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

.field private mMediaList:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field public mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field private mNotifyCount:I

.field private mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

.field private mReqActiveEnd:I

.field private mReqActiveStart:I

.field private mReqContentEnd:I

.field private mReqContentStart:I

.field public mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

.field private mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field public mSelectionSet:Z

.field private mSetChanged:Z

.field private mShowAllGroup:Z

.field private mShowGroupIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public mSize:I

.field private mSizeChanged:Z

.field public mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

.field public mSource:Lcom/sec/android/gallery3d/data/MediaSet;

.field protected mSourceVersion:J

.field private mSrcChanged:Z

.field private mStep:I

.field private mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

.field private mUpdContentEnd:I

.field private mUpdContentStart:I

.field private mUpdEnd:I

.field private mUpdStart:I

.field private mUpdateAll:Z

.field private mUpdatedSize:Z

.field public mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    .line 73
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLOCK:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p4, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    .prologue
    const/4 v1, -0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 98
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    const/4 v0, 0x2

    iput-byte v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 55
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    .line 64
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    .line 67
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdateAll:Z

    .line 70
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    .line 76
    const/16 v0, 0x20

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadCount:I

    .line 77
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadRowCount:I

    .line 78
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadColumnCount:I

    .line 79
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLatestCount:I

    .line 80
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    .line 84
    const/16 v0, 0xc8

    new-array v0, v0, [Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .line 85
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 86
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    .line 95
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z

    .line 96
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    .line 1187
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$1;-><init>(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    .line 99
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    .line 100
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 101
    if-nez p2, :cond_0

    .line 103
    sget-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    const-string v1, "mediaSet is Null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 108
    :goto_0
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mScanFirstOnly:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mScanAllSet:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAllSet:Z

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mCheckSetVersion:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCheckSetVersion:Z

    .line 112
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    .line 113
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 114
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mItemSize:I

    .line 115
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstIndex:I

    .line 116
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    .line 117
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    .line 118
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    .line 119
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    .line 121
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 122
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    .line 123
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 124
    check-cast p1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getThreadPool()Lcom/sec/android/gallery3d/util/ThreadPool;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mThreadPool:Lcom/sec/android/gallery3d/util/ThreadPool;

    .line 125
    return-void

    .line 106
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    invoke-static {p2}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadData()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private declared-synchronized applyViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;I)V
    .locals 3
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;
    .param p2, "count"    # I

    .prologue
    .line 205
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 206
    :try_start_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .line 208
    :cond_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    .line 209
    iput p2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLatestCount:I

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLatestCount:I

    if-eq v0, v1, :cond_2

    .line 212
    sget-object v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "applyViewListener = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", notiCount = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLatestCount:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;->onSizeChanged(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 216
    :cond_2
    monitor-exit p0

    return-void

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private cancelContents(IIZ)V
    .locals 7
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "recycle"    # Z

    .prologue
    .line 1150
    if-lt p1, p2, :cond_1

    .line 1185
    :cond_0
    return-void

    .line 1152
    :cond_1
    move v2, p1

    .line 1153
    .local v2, "index":I
    shr-int/lit8 v0, v2, 0x10

    .line 1154
    .local v0, "albIndex":I
    const v6, 0xffff

    and-int v4, v2, v6

    .line 1156
    .local v4, "thmIndex":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v1, v6, v0

    .line 1157
    .local v1, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v5, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    .line 1158
    .local v5, "updated":[B
    iget-object v3, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 1159
    .local v3, "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    :cond_2
    :goto_0
    if-ge v2, p2, :cond_0

    .line 1160
    iget v6, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-lt v4, v6, :cond_3

    .line 1161
    add-int/lit8 v0, v0, 0x1

    .line 1162
    const/4 v4, 0x0

    .line 1163
    shl-int/lit8 v2, v0, 0x10

    .line 1164
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v0, v6, :cond_0

    if-ge v2, p2, :cond_0

    .line 1166
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v1, v6, v0

    .line 1167
    iget-object v5, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    .line 1168
    iget-object v3, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 1169
    iget v6, v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eqz v6, :cond_2

    .line 1172
    :cond_3
    array-length v6, v3

    if-ge v4, v6, :cond_4

    .line 1173
    aget-object v6, v3, v4

    if-eqz v6, :cond_4

    .line 1174
    aget-object v6, v3, v4

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 1175
    if-eqz p3, :cond_4

    .line 1176
    aget-object v6, v3, v4

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 1177
    const/4 v6, 0x0

    aput-object v6, v3, v4

    .line 1178
    const/4 v6, 0x0

    aput-byte v6, v5, v4

    .line 1182
    :cond_4
    add-int/lit8 v4, v4, 0x1

    .line 1183
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private clearAlbumInfo()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 665
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 667
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 668
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 669
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    .line 670
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    .line 671
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    .line 672
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    .line 673
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 674
    return-void
.end method

.method private getFirstRangeByItemCount()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 537
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadCount:I

    div-int/lit8 v4, v7, 0x2

    .line 538
    .local v4, "halfCount":I
    move v6, v4

    .line 539
    .local v6, "remCount":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstIndex:I

    shr-int/lit8 v2, v7, 0x10

    .line 540
    .local v2, "firstAlbum":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstIndex:I

    const v8, 0xffff

    and-int v3, v7, v8

    .line 541
    .local v3, "firstIndex":I
    const/4 v1, 0x0

    .local v1, "activeStart":I
    const/4 v0, 0x0

    .line 543
    .local v0, "activeEnd":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-gtz v7, :cond_1

    .line 544
    iput v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 545
    iput v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 546
    iput v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 547
    iput v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    .line 596
    :cond_0
    :goto_0
    return-void

    .line 550
    :cond_1
    if-gt v4, v3, :cond_3

    .line 551
    sub-int v7, v3, v4

    shl-int/lit8 v8, v2, 0x10

    or-int v1, v7, v8

    .line 552
    move v6, v4

    .line 571
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v2

    if-eqz v7, :cond_2

    .line 572
    add-int v7, v3, v6

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v8, v8, v2

    iget v8, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-gt v7, v8, :cond_6

    .line 573
    add-int v7, v3, v6

    shl-int/lit8 v8, v2, 0x10

    or-int v0, v7, v8

    .line 592
    :cond_2
    :goto_2
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 593
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 594
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 595
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    goto :goto_0

    .line 554
    :cond_3
    sub-int/2addr v6, v3

    .line 555
    add-int/lit8 v5, v2, -0x1

    .line 556
    .local v5, "index":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    if-eqz v7, :cond_0

    .line 558
    :goto_3
    if-ltz v5, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-le v6, v7, :cond_4

    .line 559
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    sub-int/2addr v6, v7

    .line 560
    add-int/lit8 v5, v5, -0x1

    goto :goto_3

    .line 562
    :cond_4
    if-ltz v5, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    if-eqz v7, :cond_5

    .line 563
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    sub-int/2addr v7, v6

    shl-int/lit8 v8, v5, 0x10

    or-int v1, v7, v8

    .line 564
    move v6, v4

    goto :goto_1

    .line 566
    :cond_5
    const/4 v1, 0x0

    .line 567
    add-int/2addr v6, v4

    goto :goto_1

    .line 575
    .end local v5    # "index":I
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v2

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    sub-int/2addr v7, v3

    sub-int/2addr v6, v7

    .line 576
    add-int/lit8 v5, v2, 0x1

    .line 577
    .restart local v5    # "index":I
    :goto_4
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v5, v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-le v6, v7, :cond_7

    .line 578
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    sub-int/2addr v6, v7

    .line 579
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 581
    :cond_7
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v5, v7, :cond_8

    .line 582
    shl-int/lit8 v7, v5, 0x10

    or-int v0, v6, v7

    goto :goto_2

    .line 584
    :cond_8
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    add-int/lit8 v5, v7, -0x1

    .line 585
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    if-eqz v7, :cond_2

    .line 586
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v7, v5

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    shl-int/lit8 v8, v5, 0x10

    or-int v0, v7, v8

    goto/16 :goto_2
.end method

.method private getFirstRangeByRowCount()V
    .locals 17

    .prologue
    .line 482
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    if-nez v15, :cond_0

    .line 532
    :goto_0
    return-void

    .line 484
    :cond_0
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-gtz v15, :cond_1

    .line 485
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 486
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 487
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 488
    const/4 v15, 0x0

    move-object/from16 v0, p0

    iput v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    goto :goto_0

    .line 491
    :cond_1
    const/4 v12, 0x0

    .line 492
    .local v12, "remainCount":I
    const/4 v2, 0x0

    .local v2, "activeStart":I
    const/4 v1, 0x0

    .line 493
    .local v1, "activeEnd":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadRowCount:I

    .line 494
    .local v14, "rowCount":I
    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoadColumnCount:I

    .line 495
    .local v7, "columnCount":I
    const/4 v9, 0x0

    .line 496
    .local v9, "lineCount":I
    const/4 v4, 0x0

    .line 497
    .local v4, "albumIndex":I
    const/4 v10, 0x0

    .line 498
    .local v10, "photoIndex":I
    const/4 v5, 0x0

    .line 499
    .local v5, "checkDone":Z
    mul-int v15, v14, v7

    const/16 v16, 0x64

    move/from16 v0, v16

    if-le v15, v0, :cond_6

    const/4 v3, 0x1

    .line 501
    .local v3, "addTitleLine":Z
    :goto_1
    if-ge v9, v14, :cond_2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v15, v15, v4

    if-eqz v15, :cond_2

    .line 502
    if-eqz v3, :cond_7

    add-int/lit8 v9, v9, 0x1

    if-ne v9, v14, :cond_7

    .line 503
    const/4 v5, 0x1

    .line 519
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v15, v15, v4

    if-eqz v15, :cond_3

    if-eqz v5, :cond_5

    .line 520
    :cond_3
    if-lez v4, :cond_4

    .line 521
    add-int/lit8 v4, v4, -0x1

    .line 523
    :cond_4
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v15, v15, v4

    if-eqz v15, :cond_5

    .line 524
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v15, v15, v4

    iget v10, v15, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    .line 527
    :cond_5
    shl-int/lit8 v15, v4, 0x10

    or-int v1, v10, v15

    .line 528
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 529
    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 530
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 531
    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    goto :goto_0

    .line 499
    .end local v3    # "addTitleLine":Z
    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    .line 506
    .restart local v3    # "addTitleLine":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v15, v15, v4

    iget v8, v15, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    .line 507
    .local v8, "count":I
    div-int v11, v8, v7

    .line 508
    .local v11, "quotient":I
    rem-int v13, v8, v7

    .line 510
    .local v13, "remainder":I
    if-lez v13, :cond_8

    const/4 v15, 0x1

    :goto_3
    add-int v6, v11, v15

    .line 511
    .local v6, "checkline":I
    add-int v15, v6, v9

    if-lt v15, v14, :cond_a

    .line 512
    sub-int v15, v14, v9

    mul-int v12, v15, v7

    .line 513
    if-eqz v12, :cond_9

    if-ge v12, v8, :cond_9

    move v10, v12

    .line 514
    :goto_4
    goto :goto_2

    .line 510
    .end local v6    # "checkline":I
    :cond_8
    const/4 v15, 0x0

    goto :goto_3

    .restart local v6    # "checkline":I
    :cond_9
    move v10, v8

    .line 513
    goto :goto_4

    .line 516
    :cond_a
    add-int/2addr v9, v6

    .line 517
    add-int/lit8 v4, v4, 0x1

    .line 518
    goto :goto_1
.end method

.method private getRange(I)Z
    .locals 6
    .param p1, "type"    # I

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1041
    if-nez p1, :cond_0

    move v0, v2

    .line 1043
    .local v0, "step":I
    :goto_0
    if-nez v0, :cond_1

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    if-ge v3, v4, :cond_1

    .line 1044
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdStart:I

    .line 1045
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdEnd:I

    .line 1046
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActiveRange:Z

    .line 1047
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mStep:I

    .line 1061
    :goto_1
    return v1

    .line 1041
    .end local v0    # "step":I
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mStep:I

    goto :goto_0

    .line 1048
    .restart local v0    # "step":I
    :cond_1
    if-gt v0, v1, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    if-ge v3, v4, :cond_2

    .line 1049
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdStart:I

    .line 1050
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdEnd:I

    .line 1051
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActiveRange:Z

    .line 1052
    iput v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mStep:I

    goto :goto_1

    .line 1053
    :cond_2
    if-gt v0, v5, :cond_3

    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    if-ge v3, v4, :cond_3

    .line 1054
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdStart:I

    .line 1055
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdEnd:I

    .line 1056
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActiveRange:Z

    .line 1057
    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mStep:I

    goto :goto_1

    :cond_3
    move v1, v2

    .line 1059
    goto :goto_1
.end method

.method private loadAlbumSet()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 724
    sget-object v9, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v9

    .line 725
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v6, :cond_1

    .line 726
    monitor-exit v9

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v4

    .line 729
    .local v4, "version":J
    iget-wide v10, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    cmp-long v6, v10, v4

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    if-nez v6, :cond_2

    .line 730
    monitor-exit v9

    goto :goto_0

    .line 735
    .end local v4    # "version":J
    :catchall_0
    move-exception v6

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6

    .line 732
    .restart local v4    # "version":J
    :cond_2
    :try_start_1
    iput-wide v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 733
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 734
    .local v3, "size":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 735
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 736
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-eq v3, v6, :cond_4

    move v6, v7

    :goto_1
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    .line 737
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    .line 738
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-eqz v6, :cond_0

    .line 741
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->checkIncreaseAlbumSet(I)V

    .line 742
    const/4 v1, 0x0

    .local v1, "index":I
    :goto_2
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v6, :cond_8

    .line 743
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v6, v1

    .line 744
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_3

    .line 745
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;-><init>()V

    .line 746
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aput-object v0, v6, v1

    .line 748
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 749
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_5

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v4

    .line 750
    :goto_3
    iget-wide v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    cmp-long v6, v4, v10

    if-nez v6, :cond_6

    .line 742
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v1    # "index":I
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    move v6, v8

    .line 736
    goto :goto_1

    .line 749
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .restart local v1    # "index":I
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    const-wide/16 v4, -0x1

    goto :goto_3

    .line 753
    :cond_6
    iput-wide v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    .line 754
    iput-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 755
    const/16 v6, 0x20

    iput-byte v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 756
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget v6, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetLineCount:I

    iput v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    .line 757
    iget v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eq v6, v7, :cond_7

    .line 758
    invoke-virtual {v0, v7, v8}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->setMediaItemCount(IZ)V

    .line 759
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    .line 761
    :cond_7
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdatedSize:Z

    goto :goto_4

    .line 763
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    sget-object v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "loadSet done old = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", new = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private loadCoverItems(II)V
    .locals 18
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 865
    const/4 v9, 0x0

    .line 866
    .local v9, "updatedCount":I
    move/from16 v0, p1

    move/from16 v1, p2

    if-le v0, v1, :cond_0

    .line 901
    :goto_0
    return-void

    .line 869
    :cond_0
    sget-object v13, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v13

    .line 870
    move/from16 v5, p1

    .local v5, "index":I
    :goto_1
    move/from16 v0, p2

    if-gt v5, v0, :cond_8

    .line 871
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v2, v12, v5

    .line 872
    .local v2, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v8, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 873
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v8, :cond_1

    iget v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-nez v12, :cond_3

    .line 874
    :cond_1
    sget-object v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "loadCoveritems idx ="

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", start = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p1

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", end = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", mediaSet = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, ", count = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    iget v15, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    :cond_2
    :goto_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 877
    :cond_3
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getCoverMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    .line 878
    .local v4, "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v4, :cond_4

    .line 879
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    const/4 v14, 0x0

    const-wide/16 v16, -0x1

    aput-wide v16, v12, v14

    goto :goto_2

    .line 899
    .end local v2    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v4    # "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 882
    .restart local v2    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .restart local v4    # "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    :try_start_1
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v10

    .line 883
    .local v10, "version":J
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v6

    .line 884
    .local v6, "itemVersion":J
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    const/4 v14, 0x0

    aget-wide v14, v12, v14

    cmp-long v12, v10, v14

    if-nez v12, :cond_5

    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    const/4 v14, 0x0

    aget-wide v14, v12, v14

    cmp-long v12, v6, v14

    if-eqz v12, :cond_2

    .line 885
    :cond_5
    shl-int/lit8 v3, v5, 0x10

    .line 886
    .local v3, "code":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    if-ge v3, v12, :cond_6

    .line 887
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    .line 888
    :cond_6
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    if-le v3, v12, :cond_7

    .line 889
    move-object/from16 v0, p0

    iput v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    .line 890
    :cond_7
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    const/4 v14, 0x0

    aput-wide v6, v12, v14

    .line 891
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    const/4 v14, 0x0

    aput-wide v10, v12, v14

    .line 892
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    const/4 v14, 0x0

    aput-object v4, v12, v14

    .line 893
    iget-object v12, v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    const/4 v14, 0x0

    const/4 v15, 0x0

    aput-byte v15, v12, v14

    .line 894
    add-int/lit8 v9, v9, 0x1

    .line 895
    const/4 v12, 0x3

    if-lt v9, v12, :cond_2

    .line 899
    .end local v2    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v3    # "code":I
    .end local v4    # "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "itemVersion":J
    .end local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v10    # "version":J
    :cond_8
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 900
    sget-object v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "loadCoveritems count ="

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", updS = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    shr-int/lit8 v14, v14, 0x10

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, ", updE = "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    shr-int/lit8 v14, v14, 0x10

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private loadData()Z
    .locals 10

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 599
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    .line 600
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    .line 601
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdatedSize:Z

    .line 602
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    if-eqz v7, :cond_0

    .line 603
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    .line 604
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->clearAlbumInfo()V

    .line 606
    :cond_0
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    if-eqz v7, :cond_3

    .line 607
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadSelectedItem()V

    .line 614
    :goto_0
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    if-eqz v7, :cond_1

    .line 615
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    .line 616
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    .line 617
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-boolean v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mFirstRangeOptimization:Z

    if-eqz v7, :cond_6

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstIndex:I

    if-nez v7, :cond_6

    .line 618
    iput-boolean v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstReloadSkip:Z

    .line 619
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getFirstRangeByRowCount()V

    .line 623
    :cond_1
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v7, :cond_7

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isActive()Z

    move-result v7

    if-nez v7, :cond_7

    .line 661
    :cond_2
    :goto_2
    return v6

    .line 608
    :cond_3
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    if-eqz v7, :cond_4

    .line 609
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadAlbumSet()V

    goto :goto_0

    .line 610
    :cond_4
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAllSet:Z

    if-eqz v7, :cond_5

    .line 611
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadSet()V

    goto :goto_0

    .line 613
    :cond_5
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadItem()V

    goto :goto_0

    .line 621
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getFirstRangeByItemCount()V

    goto :goto_1

    .line 627
    :cond_7
    sget-object v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLOCK:Ljava/lang/Object;

    monitor-enter v7

    .line 628
    :try_start_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 629
    .local v3, "reqContentStart":I
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 630
    .local v2, "reqContentEnd":I
    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    iput v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    .line 631
    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    iput v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    .line 632
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    .line 634
    iput v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    .line 635
    invoke-direct {p0, v3, v2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadMediaSet(II)V

    .line 637
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isActive()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 639
    :cond_8
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    if-ne v7, v2, :cond_9

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    if-eq v7, v3, :cond_11

    :cond_9
    move v0, v6

    .line 641
    .local v0, "cntUpdated":Z
    :goto_3
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAllSet:Z

    if-nez v7, :cond_a

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    if-eqz v7, :cond_12

    :cond_a
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 642
    .local v1, "count":I
    :goto_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    if-eqz v7, :cond_d

    .line 643
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    if-nez v7, :cond_b

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdatedSize:Z

    if-eqz v7, :cond_c

    .line 644
    :cond_b
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-interface {v7, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 645
    :cond_c
    if-eqz v0, :cond_d

    .line 646
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    invoke-interface {v7, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;->onContentChanged(II)V

    .line 648
    :cond_d
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    if-eqz v7, :cond_e

    .line 649
    const/4 v7, -0x1

    iput v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mNotifyCount:I

    .line 651
    :cond_e
    const/4 v7, 0x0

    invoke-direct {p0, v7, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->applyViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;I)V

    .line 652
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .line 653
    .local v4, "viewListener":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;
    if-eqz v4, :cond_f

    if-eqz v0, :cond_f

    .line 654
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    invoke-interface {v4, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;->onContentChanged(II)V

    .line 656
    :cond_f
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v7, :cond_10

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isActive()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 659
    :cond_10
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->updateContentsRange()V

    .line 660
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdateAll:Z

    .line 661
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    iget-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    or-int/2addr v7, v8

    or-int/2addr v7, v0

    if-eqz v7, :cond_2

    move v6, v5

    goto/16 :goto_2

    .line 632
    .end local v0    # "cntUpdated":Z
    .end local v1    # "count":I
    .end local v2    # "reqContentEnd":I
    .end local v3    # "reqContentStart":I
    .end local v4    # "viewListener":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;
    :catchall_0
    move-exception v5

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v5

    .restart local v2    # "reqContentEnd":I
    .restart local v3    # "reqContentStart":I
    :cond_11
    move v0, v5

    .line 639
    goto :goto_3

    .line 641
    .restart local v0    # "cntUpdated":Z
    :cond_12
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mItemSize:I

    goto :goto_4
.end method

.method private loadItem()V
    .locals 14

    .prologue
    .line 769
    const/4 v2, 0x0

    .line 771
    .local v2, "forceReload":Z
    sget-object v8, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v8

    .line 772
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v5, :cond_1

    .line 773
    monitor-exit v8

    .line 807
    :cond_0
    :goto_0
    return-void

    .line 774
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v6

    .line 775
    .local v6, "version":J
    iget-wide v10, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    cmp-long v5, v10, v6

    if-nez v5, :cond_2

    .line 776
    monitor-exit v8

    goto :goto_0

    .line 787
    .end local v6    # "version":J
    :catchall_0
    move-exception v5

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 778
    .restart local v6    # "version":J
    :cond_2
    :try_start_1
    iget-wide v10, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    const-wide/16 v12, -0x1

    cmp-long v5, v10, v12

    if-nez v5, :cond_5

    const/4 v2, 0x1

    .line 779
    :goto_1
    iput-wide v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 780
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    .line 781
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v5, v5, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v5, :cond_6

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 782
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v5, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCacheForBurstshot()V

    .line 783
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v5, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    iget-boolean v10, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    invoke-virtual {v5, v9, v10}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemCountForBurstshot(Ljava/util/ArrayList;Z)I

    move-result v4

    .line 787
    .local v4, "newSize":I
    :goto_2
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 788
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    const/4 v8, 0x0

    aget-object v0, v5, v8

    .line 789
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_3

    .line 790
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;-><init>()V

    .line 791
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    const/4 v8, 0x0

    aput-object v0, v5, v8

    .line 793
    :cond_3
    iget-wide v8, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 795
    if-lez v4, :cond_7

    const/4 v5, 0x1

    :goto_3
    iput v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 796
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mItemSize:I

    .line 797
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 798
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iput-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 799
    iput v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 800
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    invoke-virtual {v5, v3, v4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I

    move-result v1

    .line 801
    .local v1, "count":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-byte v5, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetThmType:B

    iput-byte v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 802
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget v5, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetLineCount:I

    iput v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    .line 803
    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ne v1, v5, :cond_4

    if-eqz v2, :cond_0

    .line 804
    :cond_4
    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->setMediaItemCount(IZ)V

    .line 805
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    goto/16 :goto_0

    .line 778
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v1    # "count":I
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v4    # "newSize":I
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    .line 785
    :cond_6
    :try_start_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    .restart local v4    # "newSize":I
    goto :goto_2

    .line 795
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_7
    const/4 v5, 0x0

    goto :goto_3
.end method

.method private loadMediaSet(II)V
    .locals 12
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const v11, 0xffff

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 904
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    if-nez v7, :cond_1

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    if-ne p1, v7, :cond_1

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    if-ne p2, v7, :cond_1

    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    if-nez v7, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 906
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    if-eqz v7, :cond_0

    .line 911
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lez v7, :cond_4

    .line 912
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    add-int/lit8 v3, v7, -0x1

    .line 913
    .local v3, "index":I
    shl-int/lit8 v7, v3, 0x10

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v8, v8, v3

    iget v8, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    or-int v6, v7, v8

    .line 916
    .end local v3    # "index":I
    .local v6, "lastIndex":I
    :goto_1
    if-le p2, v6, :cond_2

    .line 917
    move p2, v6

    .line 918
    :cond_2
    if-ge p1, p2, :cond_0

    .line 922
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    if-ge p1, v7, :cond_3

    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    if-lt v7, p2, :cond_5

    .line 923
    :cond_3
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-direct {p0, v7, v8, v9}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 930
    :goto_2
    iput p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    .line 931
    iput p2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    .line 932
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    shr-int/lit8 v2, v7, 0x10

    .line 933
    .local v2, "albumStart":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    shr-int/lit8 v1, v7, 0x10

    .line 934
    .local v1, "albumEnd":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    and-int v5, v7, v11

    .line 935
    .local v5, "itemStart":I
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    and-int v4, v7, v11

    .line 938
    .local v4, "itemEnd":I
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    if-eqz v7, :cond_6

    .line 939
    invoke-direct {p0, v5, v4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadSelectionSet(II)V

    goto :goto_0

    .line 915
    .end local v1    # "albumEnd":I
    .end local v2    # "albumStart":I
    .end local v4    # "itemEnd":I
    .end local v5    # "itemStart":I
    .end local v6    # "lastIndex":I
    :cond_4
    const/4 v6, 0x0

    .restart local v6    # "lastIndex":I
    goto :goto_1

    .line 925
    :cond_5
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    invoke-direct {p0, v7, p1, v9}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 926
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-direct {p0, p2, v7, v9}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    goto :goto_2

    .line 941
    .restart local v1    # "albumEnd":I
    .restart local v2    # "albumStart":I
    .restart local v4    # "itemEnd":I
    .restart local v5    # "itemStart":I
    :cond_6
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstItemOnly:Z

    if-eqz v7, :cond_7

    .line 942
    invoke-direct {p0, v2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->loadCoverItems(II)V

    goto :goto_0

    .line 948
    :cond_7
    if-ne v2, v1, :cond_8

    .line 949
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, v2

    .line 950
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-direct {p0, v0, v2, v5, v7}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->updateRange(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;III)V

    goto :goto_0

    .line 952
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, v2

    .line 953
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-direct {p0, v0, v2, v5, v7}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->updateRange(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;III)V

    .line 954
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, v1

    .line 955
    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-static {v4, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    invoke-direct {p0, v0, v1, v10, v7}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->updateRange(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;III)V

    .line 957
    add-int/lit8 v3, v2, 0x1

    .restart local v3    # "index":I
    :goto_3
    if-ge v3, v1, :cond_0

    .line 958
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, v3

    .line 959
    if-nez v0, :cond_9

    .line 957
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 961
    :cond_9
    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-direct {p0, v0, v3, v10, v7}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->updateRange(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;III)V

    goto :goto_4
.end method

.method private loadSelectedItem()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 810
    iget-wide v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    const-wide/16 v8, -0x1

    cmp-long v2, v6, v8

    if-eqz v2, :cond_1

    .line 831
    :cond_0
    :goto_0
    return-void

    .line 812
    :cond_1
    const-wide/16 v6, 0x0

    iput-wide v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 813
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    .line 814
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mMediaList:Ljava/util/LinkedList;

    .line 815
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mMediaList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v1

    .line 816
    .local v1, "newSize":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v2, v4

    .line 817
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_2

    .line 818
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;-><init>()V

    .line 819
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aput-object v0, v2, v4

    .line 821
    :cond_2
    if-lez v1, :cond_3

    move v2, v3

    :goto_1
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 822
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mItemSize:I

    .line 823
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 824
    iput v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 825
    const/16 v2, 0x20

    iput-byte v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 826
    iput v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    .line 827
    iget v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eq v1, v2, :cond_0

    .line 828
    invoke-virtual {v0, v1, v4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->setMediaItemCount(IZ)V

    .line 829
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    goto :goto_0

    :cond_3
    move v2, v4

    .line 821
    goto :goto_1
.end method

.method private loadSelectionSet(II)V
    .locals 12
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    const/4 v8, 0x0

    .line 834
    if-ltz p1, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mMediaList:Ljava/util/LinkedList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v7

    if-le p1, v7, :cond_1

    .line 862
    :cond_0
    :goto_0
    return-void

    .line 836
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, v8

    .line 837
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    .line 839
    .local v2, "itemSetVersion":[J
    move v1, p1

    .line 841
    .local v1, "index":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v8

    .line 842
    :try_start_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mMediaList:Ljava/util/LinkedList;

    invoke-virtual {v7, p1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    .line 843
    .local v3, "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    if-ge v1, p2, :cond_6

    .line 844
    invoke-interface {v3}, Ljava/util/ListIterator;->nextIndex()I

    move-result v1

    .line 845
    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 846
    .local v6, "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v6, :cond_3

    .line 847
    const-wide/16 v10, -0x1

    aput-wide v10, v2, v1

    goto :goto_1

    .line 861
    .end local v3    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v6    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catchall_0
    move-exception v7

    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7

    .line 850
    .restart local v3    # "iterator":Ljava/util/ListIterator;, "Ljava/util/ListIterator<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .restart local v6    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    :try_start_1
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v4

    .line 851
    .local v4, "itemVersion":J
    aget-wide v10, v2, v1

    cmp-long v7, v4, v10

    if-eqz v7, :cond_2

    .line 852
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    if-ge v1, v7, :cond_4

    .line 853
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    .line 854
    :cond_4
    iget v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    if-le v1, v7, :cond_5

    .line 855
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    .line 856
    :cond_5
    aput-wide v4, v2, v1

    .line 857
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aput-object v6, v7, v1

    .line 858
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    const/4 v9, 0x0

    aput-byte v9, v7, v1

    goto :goto_1

    .line 861
    .end local v4    # "itemVersion":J
    .end local v6    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_6
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private loadSet()V
    .locals 14

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 678
    sget-object v10, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v10

    .line 679
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v5, :cond_1

    .line 680
    monitor-exit v10

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 682
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    move-result-wide v6

    .line 683
    .local v6, "version":J
    iget-wide v12, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    cmp-long v5, v12, v6

    if-nez v5, :cond_2

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    if-nez v5, :cond_2

    .line 684
    monitor-exit v10

    goto :goto_0

    .line 689
    .end local v6    # "version":J
    :catchall_0
    move-exception v5

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v5

    .line 686
    .restart local v6    # "version":J
    :cond_2
    :try_start_1
    iput-wide v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 687
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 688
    .local v4, "size":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    iput v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 689
    monitor-exit v10
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 690
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-eq v4, v5, :cond_5

    move v5, v8

    :goto_1
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    .line 691
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSetChanged:Z

    .line 692
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-eqz v5, :cond_0

    .line 695
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->checkIncreaseAlbumSet(I)V

    .line 696
    const/4 v2, 0x0

    .local v2, "index":I
    :goto_2
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v2, v5, :cond_9

    .line 697
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v5, v2

    .line 699
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_3

    .line 700
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;-><init>()V

    .line 701
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aput-object v0, v5, v2

    .line 703
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    .line 704
    .local v3, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v6

    .line 705
    :goto_3
    iget-wide v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    cmp-long v5, v6, v10

    if-nez v5, :cond_7

    .line 696
    :cond_4
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v2    # "index":I
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_5
    move v5, v9

    .line 690
    goto :goto_1

    .line 704
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .restart local v2    # "index":I
    .restart local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_6
    const-wide/16 v6, -0x1

    goto :goto_3

    .line 708
    :cond_7
    iput-wide v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    .line 709
    iput-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 710
    if-eqz v3, :cond_8

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v5

    :goto_5
    iput v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 711
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    invoke-virtual {v5, v3, v10}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I

    move-result v1

    .line 712
    .local v1, "count":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget-byte v5, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetThmType:B

    iput-byte v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 713
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mConfig:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;

    iget v5, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetLineCount:I

    iput v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    .line 714
    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eq v1, v5, :cond_4

    .line 715
    invoke-virtual {v0, v1, v9}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->setMediaItemCount(IZ)V

    .line 716
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSizeChanged:Z

    goto :goto_4

    .end local v1    # "count":I
    :cond_8
    move v5, v9

    .line 710
    goto :goto_5

    .line 719
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v3    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_9
    sget-object v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "loadSet done old = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", new = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private updateContentsRange()V
    .locals 23

    .prologue
    .line 1065
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getRange(I)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1143
    :cond_0
    return-void

    .line 1068
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "updateRange start = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", end = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", cs = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", ce = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdStart:I

    .line 1071
    .local v6, "index":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdEnd:I

    .line 1072
    .local v14, "endIndex":I
    shr-int/lit8 v11, v6, 0x10

    .line 1073
    .local v11, "albIndex":I
    const v3, 0xffff

    and-int v21, v6, v3

    .line 1075
    .local v21, "thmIndex":I
    const/4 v12, 0x0

    .line 1076
    .local v12, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    const/4 v15, 0x0

    .line 1077
    .local v15, "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    const/16 v16, 0x0

    .line 1078
    .local v16, "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    const/16 v17, 0x0

    .line 1079
    .local v17, "itemVersion":[J
    const/16 v22, 0x0

    .line 1080
    .local v22, "updated":[B
    const/4 v13, 0x0

    .line 1082
    .local v13, "cmpLevel":B
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActiveRange:Z

    .line 1083
    .local v10, "activeRange":Z
    const/16 v18, 0x1

    .line 1084
    .local v18, "needReset":Z
    const/16 v19, 0x0

    .line 1087
    .local v19, "needSkip":Z
    :cond_2
    :goto_0
    if-eqz v18, :cond_4

    .line 1088
    const/16 v18, 0x0

    .line 1089
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v12, v3, v11

    .line 1090
    iget-object v0, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    move-object/from16 v22, v0

    .line 1091
    iget-object v15, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 1092
    iget-object v0, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v16, v0

    .line 1093
    iget-object v0, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    move-object/from16 v17, v0

    .line 1094
    iget-byte v13, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 1095
    iget v3, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move/from16 v0, v21

    if-ge v0, v3, :cond_3

    if-eqz v22, :cond_3

    move-object/from16 v0, v22

    array-length v3, v0

    move/from16 v0, v21

    if-lt v0, v3, :cond_4

    .line 1096
    :cond_3
    const/16 v19, 0x1

    .line 1099
    :cond_4
    if-eqz v19, :cond_7

    .line 1100
    const/16 v19, 0x0

    .line 1126
    :cond_5
    :goto_1
    add-int/lit8 v21, v21, 0x1

    .line 1127
    add-int/lit8 v6, v6, 0x1

    .line 1128
    iget v3, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move/from16 v0, v21

    if-lt v0, v3, :cond_6

    .line 1129
    add-int/lit8 v11, v11, 0x1

    .line 1130
    const/16 v21, 0x0

    .line 1131
    shl-int/lit8 v6, v11, 0x10

    .line 1132
    const/16 v18, 0x1

    .line 1134
    :cond_6
    if-lt v6, v14, :cond_2

    .line 1135
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getRange(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1136
    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdStart:I

    .line 1137
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdEnd:I

    .line 1138
    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActiveRange:Z

    .line 1139
    shr-int/lit8 v11, v6, 0x10

    .line 1140
    const v3, 0xffff

    and-int v21, v6, v3

    .line 1141
    const/16 v18, 0x1

    goto :goto_0

    .line 1102
    :cond_7
    const/16 v3, 0x20

    if-ne v13, v3, :cond_9

    move-object/from16 v0, p0

    iget-byte v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 1103
    .local v7, "expLevel":B
    :goto_2
    aget-byte v3, v22, v21

    if-ge v3, v7, :cond_a

    .line 1104
    aput-byte v7, v22, v21

    .line 1106
    aget-object v20, v15, v21

    .line 1107
    .local v20, "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    new-instance v2, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    iget-object v4, v12, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    aget-object v5, v16, v21

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    invoke-direct/range {v2 .. v9}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;IIILcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V

    .line 1109
    .local v2, "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    aget-wide v4, v17, v21

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->setVersion(J)V

    .line 1110
    aput-object v2, v15, v21

    .line 1111
    if-eqz v20, :cond_8

    .line 1112
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 1113
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 1115
    :cond_8
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImage()V

    goto :goto_1

    .end local v2    # "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v7    # "expLevel":B
    .end local v20    # "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    :cond_9
    move v7, v13

    .line 1102
    goto :goto_2

    .line 1116
    .restart local v7    # "expLevel":B
    :cond_a
    if-nez v10, :cond_b

    .line 1117
    aget-object v20, v15, v21

    .line 1118
    .restart local v20    # "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->isValid()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1119
    invoke-virtual/range {v20 .. v20}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImageLatency()V

    goto :goto_1

    .line 1121
    .end local v20    # "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    :cond_b
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdateAll:Z

    if-eqz v3, :cond_5

    .line 1122
    aget-object v20, v15, v21

    .line 1123
    .restart local v20    # "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    move-object/from16 v0, v20

    invoke-interface {v3, v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;->onBitmapAvailable(Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;)V

    goto/16 :goto_1
.end method

.method private updateRange(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;III)V
    .locals 29
    .param p1, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .param p2, "abmIndex"    # I
    .param p3, "first"    # I
    .param p4, "last"    # I

    .prologue
    .line 967
    move/from16 v0, p3

    move/from16 v1, p4

    if-lt v0, v1, :cond_1

    .line 1034
    :cond_0
    :goto_0
    return-void

    .line 970
    :cond_1
    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 971
    .local v13, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    move-object/from16 v17, v0

    .line 972
    .local v17, "setVersion":[J
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    .line 973
    .local v12, "itemSetVersion":[J
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    move-object/from16 v20, v0

    .line 974
    .local v20, "updated":[B
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 978
    .local v11, "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v13, :cond_2

    .line 979
    sget-object v24, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    const-string v25, "mediaSet is null"

    invoke-static/range {v24 .. v25}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 983
    :cond_2
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getDataVersion()J

    move-result-wide v22

    .line 984
    .local v22, "version":J
    move/from16 v18, p4

    .line 985
    .local v18, "start":I
    move/from16 v8, p3

    .line 986
    .local v8, "end":I
    move/from16 v9, p3

    .local v9, "index":I
    :goto_1
    move/from16 v0, p4

    if-ge v9, v0, :cond_6

    .line 987
    aget-wide v24, v17, v9

    cmp-long v24, v24, v22

    if-nez v24, :cond_4

    .line 986
    :cond_3
    :goto_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 989
    :cond_4
    move/from16 v0, v18

    if-ge v9, v0, :cond_5

    move/from16 v18, v9

    .line 990
    :cond_5
    if-le v9, v8, :cond_3

    move v8, v9

    goto :goto_2

    .line 992
    :cond_6
    move/from16 v0, v18

    if-lt v8, v0, :cond_0

    .line 993
    shl-int/lit8 v6, p2, 0x10

    .line 994
    .local v6, "albIndex":I
    sget-object v25, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v25

    .line 995
    :try_start_0
    instance-of v0, v13, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move/from16 v24, v0

    if-eqz v24, :cond_b

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraRoll()Z

    move-result v24

    if-eqz v24, :cond_b

    .line 996
    move-object v0, v13

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v24, v0

    sub-int v26, v8, v18

    add-int/lit8 v26, v26, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    move-object/from16 v27, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    move/from16 v28, v0

    move-object/from16 v0, v24

    move/from16 v1, v18

    move/from16 v2, v26

    move-object/from16 v3, v27

    move/from16 v4, v28

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v10

    .line 997
    .local v10, "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v24

    if-eqz v24, :cond_8

    .line 998
    :cond_7
    move-object v0, v13

    check-cast v0, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->invalidateCacheForBurstshot()V

    .line 999
    check-cast v13, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    sub-int v24, v8, v18

    add-int/lit8 v24, v24, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    move-object/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    move/from16 v27, v0

    move/from16 v0, v18

    move/from16 v1, v24

    move-object/from16 v2, v26

    move/from16 v3, v27

    invoke-virtual {v13, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getMediaItemForBurstshot(IILjava/util/ArrayList;Z)Ljava/util/ArrayList;

    move-result-object v10

    .line 1000
    sget-object v26, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v27, "itemArray size is 0, new size is "

    move-object/from16 v0, v24

    move-object/from16 v1, v27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v27

    if-nez v10, :cond_a

    const/16 v24, 0x0

    :goto_3
    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v26

    move-object/from16 v1, v24

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    :cond_8
    :goto_4
    monitor-exit v25
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1006
    if-eqz v10, :cond_0

    .line 1008
    if-nez v10, :cond_c

    const/16 v16, 0x0

    .line 1009
    .local v16, "readCount":I
    :goto_5
    add-int v24, v18, v16

    add-int/lit8 v8, v24, -0x1

    .line 1010
    move/from16 v9, v18

    :goto_6
    if-gt v9, v8, :cond_0

    .line 1011
    aget-wide v24, v17, v9

    cmp-long v24, v24, v22

    if-eqz v24, :cond_d

    .line 1012
    aput-wide v22, v17, v9

    .line 1013
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCheckSetVersion:Z

    move/from16 v21, v0

    .line 1016
    .local v21, "verChanged":Z
    :goto_7
    aput-wide v22, v17, v9

    .line 1017
    sub-int v24, v9, v18

    move/from16 v0, v24

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1018
    .local v19, "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v19, :cond_e

    .line 1019
    const-wide/16 v24, -0x1

    aput-wide v24, v12, v9

    .line 1010
    :cond_9
    :goto_8
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 1000
    .end local v16    # "readCount":I
    .end local v19    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v21    # "verChanged":Z
    :cond_a
    :try_start_1
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v24

    goto :goto_3

    .line 1003
    .end local v10    # "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_b
    sub-int v24, v8, v18

    add-int/lit8 v24, v24, 0x1

    move/from16 v0, v18

    move/from16 v1, v24

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v10

    .restart local v10    # "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    goto :goto_4

    .line 1005
    .end local v10    # "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :catchall_0
    move-exception v24

    monitor-exit v25
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v24

    .line 1008
    .restart local v10    # "itemArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_c
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v16

    goto :goto_5

    .line 1015
    .restart local v16    # "readCount":I
    :cond_d
    const/16 v21, 0x0

    .restart local v21    # "verChanged":Z
    goto :goto_7

    .line 1022
    .restart local v19    # "updateItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_e
    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v14

    .line 1023
    .local v14, "itemVersion":J
    if-nez v21, :cond_f

    aget-wide v24, v12, v9

    cmp-long v24, v14, v24

    if-eqz v24, :cond_9

    .line 1024
    :cond_f
    aput-wide v14, v12, v9

    .line 1025
    aput-object v19, v11, v9

    .line 1026
    const/16 v24, 0x0

    aput-byte v24, v20, v9

    .line 1027
    or-int v7, v9, v6

    .line 1028
    .local v7, "cIndex":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v7, v0, :cond_10

    .line 1029
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentStart:I

    .line 1030
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-le v7, v0, :cond_9

    .line 1031
    move-object/from16 v0, p0

    iput v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdContentEnd:I

    goto :goto_8
.end method


# virtual methods
.method public addShowGroupId(Ljava/lang/Long;)Z
    .locals 2
    .param p1, "groupId"    # Ljava/lang/Long;

    .prologue
    const/4 v0, 0x0

    .line 1336
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    if-eqz v1, :cond_1

    .line 1343
    :cond_0
    :goto_0
    return v0

    .line 1339
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1342
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1343
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public applyMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V
    .locals 4
    .param p1, "interInfo"    # Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .prologue
    .line 1369
    iget v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mSize:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    .line 1370
    iget-wide v2, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mVersion:J

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 1371
    iget v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mActiveStart:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    .line 1372
    iget v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mActiveEnd:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    .line 1373
    iget v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mContentStart:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    .line 1374
    iget v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mContentEnd:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    .line 1375
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->checkIncreaseAlbumSet(I)V

    .line 1376
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v0, v1, :cond_0

    .line 1377
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    iget-object v2, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mAlbumInfo:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 1376
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1379
    :cond_0
    return-void
.end method

.method public checkIncreaseAlbumSet(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const/4 v5, 0x0

    .line 1239
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    array-length v3, v3

    if-gt p1, v3, :cond_0

    .line 1246
    :goto_0
    return-void

    .line 1241
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    array-length v0, v3

    .line 1242
    .local v0, "currentCount":I
    int-to-float v3, p1

    const v4, 0x3fb33333    # 1.4f

    mul-float/2addr v3, v4

    float-to-int v2, v3

    .line 1243
    .local v2, "newCount":I
    new-array v1, v2, [Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .line 1244
    .local v1, "newAlbum":[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    invoke-static {v3, v5, v1, v5, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1245
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    goto :goto_0
.end method

.method public clearAlbumSetInfo()V
    .locals 6

    .prologue
    .line 1251
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v2, :cond_2

    .line 1252
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v2, v1

    .line 1253
    .local v0, "albumSet":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-nez v2, :cond_1

    .line 1251
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1255
    :cond_1
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    const/4 v3, -0x1

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([BB)V

    .line 1256
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    const-wide/16 v4, -0x1

    invoke-static {v2, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    goto :goto_1

    .line 1258
    .end local v0    # "albumSet":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_2
    return-void
.end method

.method public clearShowGroupId()V
    .locals 1

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1353
    return-void
.end method

.method public forceReload()V
    .locals 2

    .prologue
    .line 1364
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 1365
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    .line 1366
    return-void
.end method

.method public getAlbumIndexFromFilePath(Ljava/lang/String;)I
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v3, -0x1

    .line 403
    if-nez p1, :cond_0

    move v1, v3

    .line 415
    :goto_0
    return v1

    .line 405
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v4, :cond_3

    .line 406
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v4, v1

    .line 407
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v0, :cond_1

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v4, :cond_2

    .line 405
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 410
    :cond_2
    iget-object v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    .line 411
    .local v2, "setPath":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_0

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v2    # "setPath":Ljava/lang/String;
    :cond_3
    move v1, v3

    .line 415
    goto :goto_0
.end method

.method public getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 5
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v2, -0x1

    .line 419
    if-nez p1, :cond_1

    move v1, v2

    .line 430
    :cond_0
    :goto_0
    return v1

    .line 421
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v3, :cond_4

    .line 422
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v3, v1

    .line 423
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v0, :cond_2

    iget-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-nez v3, :cond_3

    .line 421
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 426
    :cond_3
    iget-object v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eq p1, v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v3

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v4

    if-ne v3, v4, :cond_2

    goto :goto_0

    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_4
    move v1, v2

    .line 430
    goto :goto_0
.end method

.method public getAllCount()I
    .locals 4

    .prologue
    .line 288
    const/4 v2, 0x0

    .line 290
    .local v2, "retCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v3, :cond_1

    .line 291
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v3, v1

    .line 292
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_0

    .line 290
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 295
    :cond_0
    iget v3, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    add-int/2addr v2, v3

    goto :goto_1

    .line 297
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :cond_1
    return v2
.end method

.method public getAllItem(I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "albumIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 332
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 333
    .local v1, "mediaItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 334
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 335
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 336
    .local v0, "count":I
    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v1

    .line 338
    .end local v0    # "count":I
    :cond_0
    return-object v1
.end method

.method public getAllItemCount(I)I
    .locals 3
    .param p1, "albumIndex"    # I

    .prologue
    .line 342
    const/4 v0, 0x0

    .line 343
    .local v0, "count":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 344
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 345
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 347
    :cond_0
    return v0
.end method

.method public getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 5
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 388
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v1, v4, :cond_3

    .line 389
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v4, v1

    .line 390
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_1

    .line 388
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 393
    :cond_1
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 394
    .local v2, "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    array-length v4, v2

    if-ge v3, v4, :cond_0

    .line 395
    aget-object v4, v2, v3

    if-ne v4, p1, :cond_2

    .line 396
    shl-int/lit8 v4, v1, 0x10

    or-int/2addr v4, v3

    .line 399
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v2    # "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "j":I
    :goto_2
    return v4

    .line 394
    .restart local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .restart local v2    # "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v3    # "j":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 399
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    .end local v2    # "itemSet":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v3    # "j":I
    :cond_3
    const/4 v4, -0x1

    goto :goto_2
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 277
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    return v0
.end method

.method public getCount(I)I
    .locals 2
    .param p1, "albumIndex"    # I

    .prologue
    .line 281
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v1, p1

    .line 282
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_0

    .line 283
    const/4 v1, 0x0

    .line 284
    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    goto :goto_0
.end method

.method public getImage(II)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 351
    if-ltz p1, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v2, :cond_2

    .line 352
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getImage : index ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    :cond_1
    :goto_0
    return-object v1

    .line 356
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v2, p1

    .line 357
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-lt p2, v2, :cond_3

    .line 358
    sget-object v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getImage : photoIndex ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 361
    :cond_3
    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v2, v2, p2

    if-eqz v2, :cond_1

    .line 363
    iget-object v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v1, v1, p2

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 5
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    const/4 v3, 0x0

    .line 308
    if-ltz p1, :cond_0

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v4, :cond_1

    :cond_0
    move-object v2, v3

    .line 328
    :goto_0
    return-object v2

    .line 311
    :cond_1
    const/4 v2, 0x0

    .line 313
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v4, p1

    .line 314
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_2

    move-object v2, v3

    .line 315
    goto :goto_0

    .line 317
    :cond_2
    if-ltz p2, :cond_3

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v4, v4

    if-lt p2, v4, :cond_4

    :cond_3
    move-object v2, v3

    .line 318
    goto :goto_0

    .line 319
    :cond_4
    iget-object v4, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v2, v4, p2
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 320
    .end local v0    # "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    :catch_0
    move-exception v1

    .line 321
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    move-object v2, v3

    .line 322
    goto :goto_0

    .line 323
    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catch_1
    move-exception v1

    .line 324
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    move-object v2, v3

    .line 325
    goto :goto_0
.end method

.method public getLineCount(I)I
    .locals 2
    .param p1, "albumIndex"    # I

    .prologue
    .line 301
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v1, p1

    .line 302
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_0

    .line 303
    const/4 v1, 0x0

    .line 304
    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    goto :goto_0
.end method

.method public getMediaSetInfo(Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;)V
    .locals 4
    .param p1, "interInfo"    # Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;

    .prologue
    .line 1382
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    new-array v1, v1, [Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    iput-object v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mAlbumInfo:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .line 1383
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge v0, v1, :cond_0

    .line 1384
    iget-object v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mAlbumInfo:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v2, v2, v0

    aput-object v2, v1, v0

    .line 1383
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1386
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    iput v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mSize:I

    .line 1387
    iget-wide v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    iput-wide v2, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mVersion:J

    .line 1388
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    iput v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mActiveStart:I

    .line 1389
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    iput v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mActiveEnd:I

    .line 1390
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iput v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mContentStart:I

    .line 1391
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    iput v1, p1, Lcom/sec/android/gallery3d/app/MediaSetInterfaceInfo;->mContentEnd:I

    .line 1392
    return-void
.end method

.method public getShowGroupId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowGroupIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 5
    .param p1, "albumIndex"    # I

    .prologue
    const/4 v1, 0x0

    .line 434
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-ge p1, v2, :cond_0

    if-gez p1, :cond_1

    .line 435
    :cond_0
    sget-object v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getSubMediaSet is out of size : index ="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    :goto_0
    return-object v1

    .line 438
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v2, p1

    .line 439
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-nez v0, :cond_2

    .line 440
    sget-object v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[getSubMediaSet] mAlbumSet = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 443
    :cond_2
    iget-object v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    goto :goto_0
.end method

.method public isShowAllGroup()Z
    .locals 1

    .prologue
    .line 1360
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    return v0
.end method

.method public onPause(Z)V
    .locals 5
    .param p1, "recycleBitmap"    # Z

    .prologue
    const/4 v4, 0x0

    .line 455
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z

    .line 456
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_0

    .line 457
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->cancelReload()V

    .line 458
    :cond_0
    monitor-enter p0

    .line 459
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v1, :cond_1

    .line 460
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->terminate()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 463
    :try_start_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    const-wide/16 v2, 0x64

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->join(J)V

    .line 464
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 465
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->interrupt()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 471
    :cond_1
    :goto_0
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 472
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 473
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-direct {p0, v1, v2, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 475
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveStart:I

    .line 476
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mActiveEnd:I

    .line 477
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    .line 478
    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    .line 479
    return-void

    .line 467
    :catch_0
    move-exception v0

    .line 468
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 472
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v1
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 447
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z

    .line 448
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    .line 449
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstReloadSkip:Z

    .line 450
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .line 451
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 452
    monitor-exit p0

    return-void

    .line 447
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized reloadData()V
    .locals 3

    .prologue
    .line 1222
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mIsActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 1236
    :goto_0
    monitor-exit p0

    return-void

    .line 1224
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_1

    .line 1225
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1222
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1228
    :cond_1
    :try_start_2
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 1229
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$2;-><init>(Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setOnLoadDataListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;)V

    .line 1235
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public requestScreenNail(IIZ)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "type"    # I
    .param p3, "urgent"    # Z

    .prologue
    const/4 v1, 0x0

    .line 257
    shr-int/lit8 v8, p1, 0x10

    .line 258
    .local v8, "albIndex":I
    const v2, 0xffff

    and-int v10, p1, v2

    .line 260
    .local v10, "thmIndex":I
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt v8, v2, :cond_1

    .line 273
    :cond_0
    :goto_0
    return v1

    .line 262
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v9, v2, v8

    .line 263
    .local v9, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget v2, v9, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge v10, v2, :cond_0

    .line 266
    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    iget-object v2, v9, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v2, v2, v10

    const/4 v4, 0x3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    move v3, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIIZLcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V

    .line 267
    .local v0, "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v1, :cond_2

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 269
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 271
    :cond_2
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 272
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImage()V

    .line 273
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I
    .param p3, "urgent"    # Z

    .prologue
    .line 246
    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x3

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    move-object v2, p1

    move v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIIZLcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V

    .line 247
    .local v0, "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 249
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 251
    :cond_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 252
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImage()V

    .line 253
    const/4 v1, 0x1

    return v1
.end method

.method public requestToNext(I)Z
    .locals 9
    .param p1, "toward"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 219
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v7, v4, v1

    .line 220
    .local v7, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v7, :cond_0

    iget v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    const/4 v6, 0x2

    if-ge v4, v6, :cond_1

    :cond_0
    move v5, v1

    .line 242
    :goto_0
    return v5

    .line 223
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    add-int/2addr v4, p1

    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    .line 224
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    if-gez v4, :cond_3

    .line 225
    iget v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    add-int/lit8 v4, v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    .line 228
    :cond_2
    :goto_1
    const/4 v3, 0x0

    .line 230
    .local v3, "code":I
    iget-object v4, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    invoke-virtual {v4, v6, v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v8

    .line 231
    .local v8, "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    move v5, v1

    .line 232
    goto :goto_0

    .line 226
    .end local v3    # "code":I
    .end local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    iget v6, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    if-lt v4, v6, :cond_2

    .line 227
    iput v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mCurrentSlide:I

    goto :goto_1

    .line 234
    .restart local v3    # "code":I
    .restart local v8    # "items":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 235
    .local v2, "coverItem":Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    const/4 v4, 0x3

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V

    .line 236
    .local v0, "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v1, :cond_5

    .line 237
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 238
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 240
    :cond_5
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 241
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImage()V

    goto :goto_0
.end method

.method public requestUpdateBitmap(III)V
    .locals 11
    .param p1, "code"    # I
    .param p2, "level"    # I
    .param p3, "type"    # I

    .prologue
    .line 367
    shr-int/lit8 v7, p1, 0x10

    .line 368
    .local v7, "albIndex":I
    const v1, 0xffff

    and-int v10, p1, v1

    .line 369
    .local v10, "thmIndex":I
    iget v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt v7, v1, :cond_1

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 371
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v8, v1, v7

    .line 372
    .local v8, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    iget v1, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge v10, v1, :cond_0

    .line 375
    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContext:Landroid/content/Context;

    iget-object v2, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v2, v2, v10

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;

    move v3, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILcom/sec/samsung/gallery/view/image_manager/ComposeImageItem$OnBitmapAvailableListener;)V

    .line 376
    .local v0, "update":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget-object v1, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v9, v1, v10

    .line 377
    .local v9, "original":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget-object v1, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aput-object v0, v1, v10

    .line 378
    iget-object v1, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    int-to-byte v2, p3

    aput-byte v2, v1, v10

    .line 379
    iget-object v1, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    aget-wide v2, v1, v10

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->setVersion(J)V

    .line 380
    if-eqz v9, :cond_2

    .line 381
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->cancelImageRequest()V

    .line 382
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 384
    :cond_2
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->requestImage()V

    goto :goto_0
.end method

.method public setActiveWindow(IIII)V
    .locals 2
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "contentStart"    # I
    .param p4, "contentEnd"    # I

    .prologue
    .line 177
    sget-object v1, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 178
    :try_start_0
    iput p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveStart:I

    .line 179
    iput p2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqActiveEnd:I

    .line 181
    iput p3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentStart:I

    .line 182
    iput p4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mReqContentEnd:I

    .line 183
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstReloadSkip:Z

    if-nez v0, :cond_0

    .line 185
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    .line 187
    :cond_0
    return-void

    .line 183
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .prologue
    .line 190
    if-nez p1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 193
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    goto :goto_0
.end method

.method public setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 4
    .param p1, "selectionMgr"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    const/4 v1, 0x1

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-ne v0, p1, :cond_0

    .line 167
    :goto_0
    return-void

    .line 159
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    .line 161
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    .line 162
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    .line 163
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 164
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-direct {p0, v0, v2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 165
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->clearAlbumSetInfo()V

    .line 166
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    goto :goto_0

    .line 160
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setShowAllGroup(Z)V
    .locals 0
    .param p1, "showAll"    # Z

    .prologue
    .line 1356
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mShowAllGroup:Z

    .line 1357
    return-void
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "firstLoad"    # Z

    .prologue
    .line 128
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z

    move-result v0

    return v0
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;ZZ)Z
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "firstLoad"    # Z
    .param p3, "recycle"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 132
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v2, :cond_0

    .line 152
    :goto_0
    return v0

    .line 136
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v2, :cond_3

    .line 137
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 138
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSrcChanged:Z

    .line 142
    :cond_1
    :goto_1
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mFirstLoad:Z

    .line 143
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    .line 144
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSourceVersion:J

    .line 145
    invoke-static {p1}, Lcom/sec/android/gallery3d/common/Utils;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 147
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentStart:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mContentEnd:I

    invoke-direct {p0, v0, v2, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->cancelContents(IIZ)V

    .line 148
    if-eqz p3, :cond_2

    .line 149
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->clearAlbumSetInfo()V

    .line 151
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    move v0, v1

    .line 152
    goto :goto_0

    .line 139
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v2, :cond_1

    .line 140
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    goto :goto_1
.end method

.method public setThumbReslevel(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 170
    int-to-byte v0, p1

    .line 171
    .local v0, "bLevel":B
    iput-byte v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 172
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mUpdateAll:Z

    .line 173
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->reloadData()V

    .line 174
    return-void
.end method

.method public declared-synchronized setViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;

    .prologue
    .line 197
    monitor-enter p0

    if-nez p1, :cond_0

    .line 198
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :goto_0
    monitor-exit p0

    return-void

    .line 201
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->applyViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ViewListener;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
