.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "PhotoViewComponent.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 682
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 690
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v1

    .line 691
    .local v1, "notiName":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->unmountedStorage:Ljava/lang/String;
    invoke-static {v3, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1502(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Ljava/lang/String;)Ljava/lang/String;

    .line 692
    const-string v2, "MEDIA_EJECT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 693
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 695
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v2, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v2, :cond_0

    .line 696
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    if-lez v2, :cond_0

    .line 697
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v3

    monitor-enter v3

    .line 698
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    new-instance v4, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v2, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 699
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$4;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v2

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 700
    monitor-exit v3

    .line 704
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    return-void

    .line 700
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 685
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "MEDIA_EJECT"

    aput-object v2, v0, v1

    return-object v0
.end method
