.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlComposeObject.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ThumbFadeAnimation"
.end annotation


# instance fields
.field private final FADE_DURATION:I

.field private final FADE_START_TIME:I

.field private final REQ_BITMAP_TIME:I

.field private final TURN_DURATION:I

.field private mCallBack:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;

.field private mExcute:I

.field private mFirstFade:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;)V
    .locals 2
    .param p2, "callBack"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;

    .prologue
    const/16 v1, 0x78

    .line 515
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 506
    const/16 v0, 0x12c

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->TURN_DURATION:I

    .line 507
    const/16 v0, 0x1e

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->REQ_BITMAP_TIME:I

    .line 508
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->FADE_START_TIME:I

    .line 509
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->FADE_DURATION:I

    .line 516
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mCallBack:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;

    .line 517
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    .line 518
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z

    .line 519
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;

    .prologue
    .line 505
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z

    return v0
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/4 v2, 0x0

    .line 524
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    .line 525
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    const/16 v3, 0x1e

    if-ne v1, v3, :cond_1

    .line 526
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mCallBack:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-interface {v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$FadeAnimCallBack;->requestNext(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V

    .line 536
    :cond_0
    :goto_0
    return-void

    .line 527
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    const/16 v3, 0x78

    if-lt v1, v3, :cond_3

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    const/16 v3, 0xf0

    if-gt v1, v3, :cond_3

    .line 528
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    add-int/lit8 v1, v1, -0x78

    int-to-float v1, v1

    const/high16 v2, 0x42f00000    # 120.0f

    div-float v0, v1, v2

    .line 529
    .local v0, "fadeRatio":F
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z

    if-nez v1, :cond_2

    .line 530
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v0, v1, v0

    .line 531
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mThis:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvasMixRatio(F)V

    goto :goto_0

    .line 532
    .end local v0    # "fadeRatio":F
    :cond_3
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    const/16 v3, 0x12c

    if-ne v1, v3, :cond_0

    .line 533
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z

    if-nez v1, :cond_4

    const/4 v1, 0x1

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mFirstFade:Z

    .line 534
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$ThumbFadeAnimation;->mExcute:I

    goto :goto_0

    :cond_4
    move v1, v2

    .line 533
    goto :goto_1
.end method
