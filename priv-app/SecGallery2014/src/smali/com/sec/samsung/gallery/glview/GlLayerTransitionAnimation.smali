.class public Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;
.source "GlLayerTransitionAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;,
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;,
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$CoverAnimation;,
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;,
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;,
        Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;
    }
.end annotation


# static fields
.field public static final ANIMATION_TYPE_COVER:I = 0x2

.field public static final ANIMATION_TYPE_FLICKIN:I = 0x0

.field public static final ANIMATION_TYPE_FLICKOUT:I = 0x1

.field public static final ANIMATION_TYPE_UNCOVER:I = 0x3


# instance fields
.field private mAnimationType:I

.field private mDeltaX:F

.field private mNewAlphaHnd:[I

.field private mNewCount:I

.field private mNewObjHnd:[I

.field private mNewObjList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field private mOldAlphaHnd:[I

.field private mOldCount:I

.field private mOldObjHnd:[I

.field private mOldObjList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field private mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mAnimationType:I

    .line 28
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "animationType"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlPendingLayerAnimation;-><init>()V

    .line 31
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mAnimationType:I

    .line 32
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldAlphaHnd:[I

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    .prologue
    .line 9
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    return v0
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;->applyTransform(F)V

    .line 105
    return-void
.end method

.method protected onPrepared()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 43
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mAnimationType:I

    packed-switch v3, :pswitch_data_0

    .line 60
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    .line 61
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 62
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-lez v3, :cond_0

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    .line 64
    .local v0, "childLayer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 65
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 67
    .end local v0    # "childLayer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    .line 68
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I

    .line 69
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldAlphaHnd:[I

    .line 70
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    if-ge v1, v3, :cond_1

    .line 71
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 72
    .local v2, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPosIndex()I

    move-result v4

    aput v4, v3, v1

    .line 73
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldAlphaHnd:[I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAlphaHnd()I

    move-result v4

    aput v4, v3, v1

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 45
    .end local v1    # "i":I
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :pswitch_0
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;

    invoke-direct {v3, p0, v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickInAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;

    goto :goto_0

    .line 48
    :pswitch_1
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;

    invoke-direct {v3, p0, v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;

    goto :goto_0

    .line 51
    :pswitch_2
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$CoverAnimation;

    invoke-direct {v3, p0, v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$CoverAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;

    goto :goto_0

    .line 54
    :pswitch_3
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;

    invoke-direct {v3, p0, v4}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$UncoverAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mTransform:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;

    goto :goto_0

    .line 78
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    .line 79
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    if-eqz v3, :cond_3

    .line 80
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mChildCount:I

    if-lez v3, :cond_2

    .line 81
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v0

    .line 82
    .restart local v0    # "childLayer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 83
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getTopObjectList()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 85
    .end local v0    # "childLayer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    .line 86
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I

    .line 87
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    new-array v3, v3, [I

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I

    .line 88
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    if-ge v1, v3, :cond_3

    .line 89
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 90
    .restart local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPosIndex()I

    move-result v4

    aput v4, v3, v1

    .line 91
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAlphaHnd()I

    move-result v4

    aput v4, v3, v1

    .line 88
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 94
    .end local v1    # "i":I
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    if-nez v3, :cond_4

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    if-nez v3, :cond_4

    .line 101
    :goto_3
    return-void

    .line 96
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F

    .line 97
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 98
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->applyTransform(F)V

    .line 99
    const-wide/16 v4, 0x190

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->setDuration(J)V

    .line 100
    const-wide/16 v4, 0x20

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->startAfter(J)V

    goto :goto_3

    .line 43
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 112
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I

    if-ge v0, v2, :cond_0

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I

    if-ge v0, v2, :cond_1

    .line 116
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 117
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->releasePosIndex(I)V

    .line 118
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewAlphaHnd:[I

    aget v2, v2, v0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAlphaHnd(I)V

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 120
    .end local v1    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 121
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 122
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I

    .line 123
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I

    .line 124
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 125
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 126
    return-void
.end method

.method public setAnimationType(I)V
    .locals 0
    .param p1, "animationType"    # I

    .prologue
    .line 35
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mAnimationType:I

    .line 36
    return-void
.end method
