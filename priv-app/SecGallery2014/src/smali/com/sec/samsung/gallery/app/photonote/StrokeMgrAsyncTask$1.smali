.class Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;
.super Landroid/os/Handler;
.source "StrokeMgrAsyncTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 43
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v4, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->access$100(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->access$000(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItem(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mInsertHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->access$200(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->mIsAbleToSave:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->access$300(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;

    # invokes: Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->insertPhotoFrame()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;->access$400(Lcom/sec/samsung/gallery/app/photonote/StrokeMgrAsyncTask;)V

    goto :goto_0
.end method
