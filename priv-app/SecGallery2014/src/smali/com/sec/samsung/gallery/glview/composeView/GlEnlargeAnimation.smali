.class public Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlEnlargeAnimation.java"


# static fields
.field private static final ANIM_STATE_ACTIVE:I = 0x2

.field private static final ANIM_STATE_IDLE:I = 0x0

.field private static final ANIM_STATE_READY:I = 0x1

.field private static final ANIM_STATE_RUNNING:I = 0x3

.field private static final SCALE_DELTA:F = 0.6f

.field private static final SCALE_LIMIT:F = 10.0f

.field private static final SCALE_MAX:F = 1.6f

.field public static final SKIP_ANIMATION:Z = true

.field private static final TAG:Ljava/lang/String; = "GlEnlargeAnimation"


# instance fields
.field private inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

.field private mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field public mAnimForward:Z

.field public mAutoAnimation:Z

.field private mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field private mCropRectPro:Landroid/graphics/Rect;

.field private mCropRectSrc:Landroid/graphics/Rect;

.field private mCurrentScale:F

.field private mDstH:F

.field private mDstW:F

.field private mFadeOutObjs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;"
        }
    .end annotation
.end field

.field private mIdxAlb:I

.field private mIdxThm:I

.field private mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mManual:Z

.field private mOrgAbsX:F

.field private mOrgAbsY:F

.field private mOrgAbsZ:F

.field private mOrigX:F

.field private mOrigY:F

.field private mOrigZ:F

.field private mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mRatioRange:F

.field mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

.field private mRotation:I

.field private mRotationOnly:Z

.field private mSrcBitmap:Landroid/graphics/Bitmap;

.field private mSrcH:F

.field private mSrcW:F

.field public mState:I

.field private mTgtImgH:I

.field private mTgtImgW:I


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1
    .param p1, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 38
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    .line 43
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 417
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    .line 61
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 62
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    return-object v0
.end method

.method private applyMainAnim(F)V
    .locals 14
    .param p1, "ratio"    # F

    .prologue
    .line 314
    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, p1

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, p1

    mul-float/2addr v10, v11

    sub-float v3, v9, v10

    .line 315
    .local v3, "intRatio":F
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    if-eqz v9, :cond_1

    move v6, p1

    .line 318
    .local v6, "proRatio":F
    :goto_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v6

    mul-float/2addr v9, v10

    float-to-int v4, v9

    .line 319
    .local v4, "left":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgW:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    add-float/2addr v9, v10

    float-to-int v7, v9

    .line 320
    .local v7, "right":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v6

    mul-float/2addr v9, v10

    float-to-int v8, v9

    .line 321
    .local v8, "top":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgH:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v10, v6

    add-float/2addr v9, v10

    float-to-int v0, v9

    .line 323
    .local v0, "bottom":I
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    if-nez v9, :cond_0

    .line 324
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    const/high16 v11, 0x3f800000    # 1.0f

    sub-float/2addr v11, v3

    mul-float/2addr v10, v11

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 326
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    int-to-float v10, v4

    int-to-float v11, v8

    int-to-float v12, v7

    int-to-float v13, v0

    invoke-virtual {v9, v10, v11, v12, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTexCoords(FFFF)V

    .line 327
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsX:F

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsX:F

    mul-float/2addr v11, v6

    sub-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsY:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsY:F

    mul-float/2addr v12, v6

    sub-float/2addr v11, v12

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsZ:F

    invoke-virtual {v9, v10, v11, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 328
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcW:F

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mDstW:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcW:F

    sub-float/2addr v11, v12

    mul-float/2addr v11, v6

    add-float/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcH:F

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mDstH:F

    iget v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcH:F

    sub-float/2addr v12, v13

    mul-float/2addr v12, v6

    add-float/2addr v11, v12

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 329
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "count":I
    :goto_1
    if-ge v2, v1, :cond_4

    .line 330
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 331
    .local v5, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v5, :cond_3

    .line 329
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 315
    .end local v0    # "bottom":I
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v4    # "left":I
    .end local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    .end local v6    # "proRatio":F
    .end local v7    # "right":I
    .end local v8    # "top":I
    :cond_1
    iget-boolean v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    if-eqz v9, :cond_2

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRatioRange:F

    const/high16 v10, 0x3f800000    # 1.0f

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRatioRange:F

    sub-float/2addr v10, v11

    mul-float/2addr v10, v3

    add-float v6, v9, v10

    goto/16 :goto_0

    :cond_2
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRatioRange:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v10, v3

    mul-float v6, v9, v10

    goto/16 :goto_0

    .line 333
    .restart local v0    # "bottom":I
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v4    # "left":I
    .restart local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    .restart local v6    # "proRatio":F
    .restart local v7    # "right":I
    .restart local v8    # "top":I
    :cond_3
    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v6

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    goto :goto_2

    .line 335
    .end local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_4
    return-void
.end method

.method private applyRotateAnim(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 308
    sub-float v1, v3, p1

    sub-float v2, v3, p1

    mul-float/2addr v1, v2

    sub-float v0, v3, v1

    .line 310
    .local v0, "intRatio":F
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    sub-float/2addr v3, v0

    mul-float/2addr v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 311
    return-void
.end method

.method private requestCompleteInter()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 255
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    .line 256
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationGesture(Z)V

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;)V

    .line 260
    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setDuration(J)V

    .line 261
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->start()V

    .line 262
    return-void
.end method

.method private resetToOrigin()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 353
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    int-to-float v8, v8

    invoke-virtual {v4, v5, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTexCoords(FFFF)V

    .line 354
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcBitmap:Landroid/graphics/Bitmap;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_0

    .line 355
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v5, v6, v7}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 357
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 358
    .local v3, "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v3, v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorVisibility(Z)V

    .line 359
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 360
    invoke-direct {p0, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setOriginalPos(Z)V

    .line 361
    const/4 v1, 0x0

    .local v1, "i":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 362
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 363
    .local v2, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v2, :cond_1

    .line 361
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 365
    :cond_1
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    goto :goto_1

    .line 367
    .end local v2    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    return-void
.end method

.method private setObjectRect()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 384
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, v11, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 385
    .local v2, "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    iget v6, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    .line 386
    .local v6, "spaceW":F
    iget v5, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    .line 387
    .local v5, "spaceH":F
    iget v11, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v10, v11

    .line 388
    .local v10, "viewW":F
    iget v11, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v9, v11

    .line 393
    .local v9, "viewH":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotation:I

    if-eqz v11, :cond_0

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotation:I

    const/16 v12, 0xb4

    if-ne v11, v12, :cond_2

    .line 394
    :cond_0
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgW:I

    int-to-float v8, v11

    .line 395
    .local v8, "tgtW":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgH:I

    int-to-float v7, v11

    .line 400
    .local v7, "tgtH":F
    :goto_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v11, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v11

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcW:F

    .line 401
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v11, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v11

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcH:F

    .line 402
    mul-float v11, v10, v7

    mul-float v12, v9, v8

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    .line 404
    div-float v4, v9, v7

    .line 409
    .local v4, "scale":F
    :goto_1
    const/high16 v11, 0x41200000    # 10.0f

    cmpl-float v11, v4, v11

    if-lez v11, :cond_1

    const/high16 v4, 0x41200000    # 10.0f

    .line 410
    :cond_1
    div-float v3, v6, v10

    .line 411
    .local v3, "ratio":F
    mul-float v11, v8, v4

    mul-float v1, v11, v3

    .line 412
    .local v1, "dstW":F
    mul-float v11, v7, v4

    mul-float v0, v11, v3

    .line 413
    .local v0, "dstH":F
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mDstW:F

    .line 414
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mDstH:F

    .line 415
    return-void

    .line 397
    .end local v0    # "dstH":F
    .end local v1    # "dstW":F
    .end local v3    # "ratio":F
    .end local v4    # "scale":F
    .end local v7    # "tgtH":F
    .end local v8    # "tgtW":F
    :cond_2
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgH:I

    int-to-float v8, v11

    .line 398
    .restart local v8    # "tgtW":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgW:I

    int-to-float v7, v11

    .restart local v7    # "tgtH":F
    goto :goto_0

    .line 407
    :cond_3
    div-float v4, v10, v8

    .restart local v4    # "scale":F
    goto :goto_1
.end method

.method private setOriginalPos(Z)V
    .locals 4
    .param p1, "isSet"    # Z

    .prologue
    .line 370
    if-eqz p1, :cond_0

    .line 371
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigX:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigY:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigZ:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 372
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcW:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcH:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 381
    :goto_0
    return-void

    .line 374
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigX:F

    .line 375
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigY:F

    .line 376
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrigZ:F

    .line 377
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsX:F

    .line 378
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsY:F

    .line 379
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsZ:F

    goto :goto_0
.end method


# virtual methods
.method public addFadeOutObj(Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 1
    .param p1, "obj"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-ne v0, p1, :cond_1

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public addFadeOutObjs(Landroid/util/SparseArray;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<+",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "objList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<+Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 200
    :cond_0
    return-void

    .line 193
    :cond_1
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 195
    .local v0, "activeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 196
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 195
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 198
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public addFadeOutObjs(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<+",
            "Lcom/sec/android/gallery3d/glcore/GlObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "objList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<+Lcom/sec/android/gallery3d/glcore/GlObject;>;"
    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    if-nez v2, :cond_1

    .line 189
    :cond_0
    return-void

    .line 180
    :cond_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 182
    .local v0, "activeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 183
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v2, v3, :cond_3

    .line 182
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 185
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 187
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method protected applyTransform(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 300
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    if-eqz v0, :cond_0

    .line 301
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->applyRotateAnim(F)V

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->applyMainAnim(F)V

    goto :goto_0
.end method

.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 281
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSwitchViewState(I)V

    .line 284
    :cond_0
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimState:I

    .line 285
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 286
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    .line 287
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    .line 288
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 289
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    .line 290
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_1

    .line 291
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->resetToOrigin()V

    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationGesture(Z)V

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 296
    :cond_1
    return-void
.end method

.method public getObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    return-object v0
.end method

.method public isActive()Z
    .locals 2

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isAutoAnimation()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAutoAnimation:Z

    return v0
.end method

.method public isIdle()Z
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isReady()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 211
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isRunning()Z
    .locals 2

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onStop()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 338
    const-string v0, "GlEnlargeAnimation"

    const-string v1, "onStop !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 339
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->getLastRatio()F

    move-result v0

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 340
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->applyTransform(F)V

    .line 342
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    if-nez v0, :cond_1

    .line 343
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->resetToOrigin()V

    .line 345
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_2

    .line 346
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setSwitchViewState(I)V

    .line 348
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 349
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 350
    return-void

    .line 346
    :cond_3
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z
    .locals 12
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p2, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .param p3, "startAnim"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 65
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 66
    :cond_0
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 109
    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    :goto_0
    return v3

    .line 69
    .restart local p1    # "adapter":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    :cond_1
    check-cast p1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 70
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 71
    iput-boolean p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAutoAnimation:Z

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxAlb:I

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    const v1, 0xffff

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxThm:I

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 76
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mFadeOutObjs:Ljava/util/ArrayList;

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxAlb:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxThm:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v6, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 79
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxAlb:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxThm:I

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    .line 80
    .local v8, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v8, :cond_2

    .line 81
    const-string v0, "GlEnlargeAnimation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareAnimation is canceled : mediaItem is null!!, index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    goto :goto_0

    .line 85
    :cond_2
    if-nez v6, :cond_3

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAutoAnimation:Z

    if-nez v0, :cond_3

    .line 86
    const-string v0, "GlEnlargeAnimation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareAnimation is canceled : bitmap is null = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    goto/16 :goto_0

    .line 90
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 91
    .local v9, "rect":Landroid/graphics/Rect;
    if-eqz v9, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectSrc:Landroid/graphics/Rect;

    invoke-virtual {v0, v9}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 92
    :cond_4
    instance-of v0, v8, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v0, :cond_5

    .line 93
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v8    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v8

    .line 95
    .restart local v8    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    if-eqz v6, :cond_6

    :try_start_0
    invoke-static {v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mSrcBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;)V

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    const/4 v1, 0x2

    invoke-virtual {v0, v8, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z

    .line 102
    iput v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput v11, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mSrcRoll:F

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mRotation:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotation:I

    .line 105
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    .line 106
    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRatioRange:F

    .line 107
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    .line 108
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    move v3, v10

    .line 109
    goto/16 :goto_0

    .line 95
    :cond_6
    const/4 v0, 0x0

    goto :goto_1

    .line 96
    :catch_0
    move-exception v7

    .line 97
    .local v7, "e":Ljava/lang/OutOfMemoryError;
    const-string v0, "GlEnlargeAnimation"

    const-string v1, "OutOfMemoryError occured at prepareAnimation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    invoke-static {}, Ljava/lang/System;->gc()V

    goto :goto_2
.end method

.method public requestComplete()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 265
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    if-ne v2, v1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationGesture(Z)V

    .line 267
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;)V

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRoll(F)V

    .line 269
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 277
    :cond_0
    :goto_0
    return v0

    .line 271
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 274
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    const v3, 0x3f19999a    # 0.6f

    div-float/2addr v2, v3

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRatioRange:F

    .line 275
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    const v3, 0x3f933333    # 1.15f

    cmpl-float v2, v2, v3

    if-ltz v2, :cond_2

    move v0, v1

    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 276
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->requestCompleteInter()V

    move v0, v1

    .line 277
    goto :goto_0
.end method

.method public setScale(F)I
    .locals 7
    .param p1, "scale"    # F

    .prologue
    const/4 v0, -0x1

    const v6, 0x3fcccccd    # 1.6f

    const/4 v5, 0x1

    const/4 v1, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 227
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    if-eqz v2, :cond_0

    .line 250
    :goto_0
    return v0

    .line 229
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 230
    goto :goto_0

    .line 232
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    mul-float/2addr v2, p1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    .line 233
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mManual:Z

    .line 234
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    cmpl-float v2, v2, v6

    if-ltz v2, :cond_2

    .line 235
    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    .line 236
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->applyTransform(F)V

    .line 237
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 238
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    .line 239
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->requestCompleteInter()V

    move v0, v1

    .line 240
    goto :goto_0

    .line 241
    :cond_2
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_3

    .line 242
    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    .line 243
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 244
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotationOnly:Z

    .line 245
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->resetToOrigin()V

    .line 246
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->requestCompleteInter()V

    goto :goto_0

    .line 249
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCurrentScale:F

    sub-float/2addr v0, v4

    const v2, 0x3f19999a    # 0.6f

    div-float/2addr v0, v2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->applyTransform(F)V

    move v0, v1

    .line 250
    goto :goto_0
.end method

.method public startAnimation()Z
    .locals 9

    .prologue
    const/4 v3, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 113
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAutoAnimation:Z

    if-eqz v0, :cond_0

    .line 114
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 115
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mListener:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-interface {v0, p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;->onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 167
    :goto_0
    return v5

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v0, :cond_1

    .line 120
    const-string v0, "GlEnlargeAnimation"

    const-string/jumbo v1, "startAnimation mMainObj.mGlRoot = null skipped !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getParent()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v0, :cond_2

    .line 126
    const-string v0, "GlEnlargeAnimation"

    const-string/jumbo v1, "startAnimation parentObj = null skipped !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    goto :goto_0

    .line 130
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxAlb:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mIdxThm:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v6, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 134
    .local v6, "bitmap":Landroid/graphics/Bitmap;
    if-nez v6, :cond_3

    .line 135
    const-string v0, "GlEnlargeAnimation"

    const-string/jumbo v1, "startAnimation bitmap = null skipped !!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    goto :goto_0

    .line 139
    :cond_3
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgW:I

    .line 140
    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgH:I

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mRotation:I

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    if-nez v0, :cond_4

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgW:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mTgtImgH:I

    invoke-direct {v0, v5, v5, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_1
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    .line 144
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setOriginalPos(Z)V

    .line 145
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setObjectRect()V

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 147
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 148
    .local v7, "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    invoke-virtual {v7, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorVisibility(Z)V

    .line 149
    iget-object v0, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mExpansionObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 150
    iget-object v0, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mCheckObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsX:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsY:F

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mOrgAbsZ:F

    invoke-virtual {v0, v1, v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setRotationGesture(Z)V

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v1, v2, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFill(Z)V

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mCropRectPro:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTexCoords(FFFF)V

    .line 158
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAutoAnimation:Z

    if-eqz v0, :cond_5

    .line 159
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mAnimForward:Z

    .line 160
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 162
    const-wide/16 v0, 0x96

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->setDuration(J)V

    .line 163
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->start()V

    move v5, v8

    .line 164
    goto/16 :goto_0

    .line 142
    .end local v7    # "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    goto/16 :goto_1

    .line 166
    .restart local v7    # "thmObj":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
    :cond_5
    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->mState:I

    move v5, v8

    .line 167
    goto/16 :goto_0
.end method
