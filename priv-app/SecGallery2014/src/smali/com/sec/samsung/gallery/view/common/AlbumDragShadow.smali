.class public final Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;
.super Lcom/sec/samsung/gallery/view/common/DragShadow;
.source "AlbumDragShadow.java"


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "bitmap"    # Landroid/graphics/Bitmap;
    .param p4, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/common/DragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 14
    return-void
.end method


# virtual methods
.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 4
    .param p1, "size"    # Landroid/graphics/Point;
    .param p2, "touch"    # Landroid/graphics/Point;

    .prologue
    const/4 v3, 0x0

    .line 18
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->calculateSize()V

    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mShadowBitmapDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mHeight:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(IIII)V

    .line 23
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mWidth:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mHeight:I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 24
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mWidth:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/AlbumDragShadow;->mHeight:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 25
    return-void
.end method
