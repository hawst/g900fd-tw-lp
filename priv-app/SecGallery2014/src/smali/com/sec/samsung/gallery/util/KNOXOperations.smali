.class public Lcom/sec/samsung/gallery/util/KNOXOperations;
.super Landroid/os/AsyncTask;
.source "KNOXOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/KNOXOperations$1;,
        Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;,
        Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final OP_MOVE_TO_KNOX:I = 0x0

.field public static final OP_REMOVE_FROM_KNOX:I = 0x1

.field private static final TAG:Ljava/lang/String; = "KNOXOperations"

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field public static mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mIsKNOXVer:Z

.field private mOperationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 88
    new-instance v0, Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/util/KNOXOperations$MediaObjectComparator;-><init>(Lcom/sec/samsung/gallery/util/KNOXOperations$1;)V

    sput-object v0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(ILcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;Landroid/content/Context;)V
    .locals 1
    .param p1, "operationId"    # I
    .param p2, "onKNOXListener"    # Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 50
    iput p1, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOperationId:I

    .line 51
    sput-object p2, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    .line 52
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mContext:Landroid/content/Context;

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX2(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mIsKNOXVer:Z

    .line 55
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 6
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    .local v0, "failedCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 69
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 70
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-object v5, Lcom/sec/samsung/gallery/util/KNOXOperations;->mComparator:Ljava/util/Comparator;

    invoke-static {v2, v5}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 71
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 72
    .local v3, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    sget-object v5, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    invoke-interface {v5, v3}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 73
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 76
    .end local v3    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    return-object v5
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/KNOXOperations;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 82
    sget-object v0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mOnKNOXListener:Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/util/KNOXOperations$OnKNOXListener;->onRequestKNOXOperation()V

    .line 85
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 86
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 20
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/KNOXOperations;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mIsKNOXVer:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/KNOXOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 61
    :cond_0
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 62
    return-void
.end method
