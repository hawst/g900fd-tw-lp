.class public Lcom/sec/samsung/gallery/util/MediaOperations;
.super Landroid/os/AsyncTask;
.source "MediaOperations.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

.field private mTotal:I


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;Landroid/content/Context;)V
    .locals 1
    .param p1, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    .line 29
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mContext:Landroid/content/Context;

    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 32
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 9
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v8, 0x0

    .line 42
    const/4 v3, 0x0

    .line 43
    .local v3, "progress":I
    const/4 v4, 0x1

    .line 44
    .local v4, "result":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 45
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    if-nez v5, :cond_0

    .line 46
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    .line 68
    :goto_0
    return-object v6

    .line 47
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 48
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mTotal:I

    .line 51
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_1
    iget v6, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mTotal:I

    if-ge v0, v6, :cond_5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v6

    if-nez v6, :cond_5

    .line 52
    if-ltz v0, :cond_1

    iget v6, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mTotal:I

    if-le v0, v6, :cond_2

    .line 53
    :cond_1
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 56
    :cond_2
    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 57
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v1, :cond_3

    .line 51
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 61
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-interface {v6, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    .line 62
    if-nez v4, :cond_4

    .line 63
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0

    .line 65
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 66
    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/util/MediaOperations;->publishProgress([Ljava/lang/Object;)V

    goto :goto_2

    .line 68
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperations;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 92
    :cond_0
    return-void
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onCompleted(Z)V

    .line 84
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperations;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 36
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 38
    return-void
.end method

.method protected varargs onProgressUpdate([Ljava/lang/Integer;)V
    .locals 3
    .param p1, "progress"    # [Ljava/lang/Integer;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/util/MediaOperations;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mOnProgressListener:Lcom/sec/android/gallery3d/data/OnProgressListener;

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/samsung/gallery/util/MediaOperations;->mTotal:I

    invoke-interface {v0, v1, v2}, Lcom/sec/android/gallery3d/data/OnProgressListener;->onProgress(II)V

    .line 76
    :cond_0
    return-void
.end method

.method protected bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/MediaOperations;->onProgressUpdate([Ljava/lang/Integer;)V

    return-void
.end method
