.class Lcom/sec/samsung/gallery/view/AlbumTabBar$2;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 94
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 2
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/TabTagType;

    .line 86
    .local v0, "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->isReselectedTab(Lcom/sec/samsung/gallery/core/TabTagType;)Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$2;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->handleFilterAlbums(Lcom/sec/samsung/gallery/core/TabTagType;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$200(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)V

    goto :goto_0
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 81
    return-void
.end method
