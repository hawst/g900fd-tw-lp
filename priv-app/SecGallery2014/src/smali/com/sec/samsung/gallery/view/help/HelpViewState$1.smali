.class Lcom/sec/samsung/gallery/view/help/HelpViewState$1;
.super Ljava/lang/Object;
.source "HelpViewState.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpViewState;->setOnClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "arg0"    # Landroid/view/View;

    .prologue
    const v2, 0x7f0e02da

    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpMode:I

    packed-switch v0, :pswitch_data_0

    .line 193
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 169
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mViewMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 173
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$100(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 177
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mViewMode:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    .line 178
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 182
    :pswitch_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$300(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 185
    :pswitch_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$400(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 188
    :pswitch_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 167
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method
