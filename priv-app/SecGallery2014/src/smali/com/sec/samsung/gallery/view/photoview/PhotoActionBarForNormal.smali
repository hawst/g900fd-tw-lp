.class public Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "PhotoActionBarForNormal.java"


# instance fields
.field private mCameraButton:Landroid/widget/ImageButton;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 65
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->isReadyToEnableHomeButton()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;Landroid/widget/ImageButton;)Landroid/widget/ImageButton;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;
    .param p1, "x1"    # Landroid/widget/ImageButton;

    .prologue
    .line 36
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    const v0, 0x7f120020

    .line 347
    :goto_0
    return v0

    .line 339
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    const v0, 0x7f120032

    goto :goto_0

    .line 341
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 342
    const v0, 0x7f120035

    goto :goto_0

    .line 344
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 345
    const v0, 0x7f120033

    goto :goto_0

    .line 347
    :cond_3
    const v0, 0x7f120034

    goto :goto_0
.end method

.method private updateSortByOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v7, 0x7f0f0291

    const/4 v2, 0x1

    const v6, 0x7f0f0290

    const/4 v3, 0x0

    .line 195
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseSortbyPhotoView:Z

    if-eqz v4, :cond_0

    .line 196
    invoke-static {p1, v6, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 197
    invoke-static {p1, v7, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 209
    :goto_0
    return-void

    .line 200
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_1

    .line 201
    invoke-static {p1, v6, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 204
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v5, "sort_by_type_time"

    invoke-static {v4, v5, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v1

    .line 206
    .local v1, "type":I
    if-nez v1, :cond_2

    move v0, v2

    .line 207
    .local v0, "isLatest":Z
    :goto_1
    if-nez v0, :cond_3

    :goto_2
    invoke-static {p1, v6, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 208
    invoke-static {p1, v7, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .end local v0    # "isLatest":Z
    :cond_2
    move v0, v3

    .line 206
    goto :goto_1

    .restart local v0    # "isLatest":Z
    :cond_3
    move v2, v3

    .line 207
    goto :goto_2
.end method


# virtual methods
.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 100
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 101
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 102
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 6
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 213
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 281
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 215
    :sswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ABCO"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 216
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NEW_ALBUM:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 219
    :sswitch_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 222
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PHSL"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 223
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 226
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PHDL"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 227
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 230
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_LATEST:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 233
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "SORT"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 234
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_OLDEST:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 237
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setNeedMenuExpand(Z)V

    .line 238
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 241
    :sswitch_7
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v2, :cond_0

    .line 242
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setNeedMenuExpand(Z)V

    .line 243
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v2, v3, v5}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 247
    :sswitch_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "START_CAMERA"

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 254
    :sswitch_9
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE_FOR_STORY_ALBUM:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 260
    :sswitch_a
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v3, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 261
    .local v0, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 270
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_b
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    aput-object v5, v1, v4

    .line 271
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "S_STUDIO"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 275
    .end local v1    # "params":[Ljava/lang/Object;
    :sswitch_c
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_CREATE_GIF:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 278
    :sswitch_d
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_8
        0x7f0f0253 -> :sswitch_2
        0x7f0f0257 -> :sswitch_a
        0x7f0f026e -> :sswitch_3
        0x7f0f026f -> :sswitch_1
        0x7f0f0275 -> :sswitch_9
        0x7f0f0283 -> :sswitch_b
        0x7f0f028a -> :sswitch_d
        0x7f0f028e -> :sswitch_7
        0x7f0f0290 -> :sswitch_4
        0x7f0f0291 -> :sswitch_5
        0x7f0f02d2 -> :sswitch_0
        0x7f0f02d3 -> :sswitch_6
        0x7f0f02d5 -> :sswitch_c
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 14
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v13, 0x7f0f0275

    const v9, 0x7f0f0251

    const v12, 0x7f0f02d5

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 106
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;)Z

    move-result v4

    .line 107
    .local v4, "isContainsHiddenItems":Z
    const v7, 0x7f0f02d1

    invoke-interface {p1, v7, v11}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 109
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v2

    .line 111
    .local v2, "currentViewType":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    sget-object v8, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v7, v8, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_1

    :cond_0
    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-nez v7, :cond_2

    sget-object v7, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v7, v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v7

    if-nez v7, :cond_2

    .line 113
    :cond_1
    const v7, 0x7f0f02d1

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 114
    const v7, 0x7f0f02d2

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 117
    :cond_2
    const v7, 0x7f0f0257

    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 119
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v7, :cond_3

    .line 120
    invoke-static {p1, v9, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 128
    :cond_3
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->updateSortByOptionMenu(Landroid/view/Menu;)V

    .line 130
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v7, :cond_4

    .line 131
    const v7, 0x7f0f026f

    const/4 v8, 0x2

    invoke-static {p1, v7, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 133
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v7

    iget v7, v7, Landroid/content/res/Configuration;->orientation:I

    if-ne v7, v11, :cond_5

    .line 134
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v7

    if-nez v7, :cond_5

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 135
    const/4 v7, 0x2

    invoke-static {p1, v9, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 141
    :cond_5
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    if-nez v7, :cond_6

    .line 142
    invoke-static {p1, v12, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 144
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_7

    .line 145
    invoke-static {p1, v12, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    :cond_7
    invoke-static {p1, v13, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 148
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 149
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseStoryAlbum:Z

    if-eqz v7, :cond_8

    .line 150
    invoke-static {p1, v13, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 153
    :cond_8
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 154
    const v7, 0x7f0f02d2

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 155
    invoke-static {p1, v13, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 156
    const v7, 0x7f0f0257

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 157
    invoke-static {p1, v12, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 160
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v5

    .line 161
    .local v5, "isEventViewMode":Z
    if-eqz v5, :cond_a

    .line 162
    invoke-static {p1, v9, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 163
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    if-eqz v7, :cond_a

    .line 164
    invoke-static {p1, v12, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 166
    :cond_a
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v3

    .line 167
    .local v3, "isCategoryViewMode":Z
    if-nez v5, :cond_b

    if-eqz v3, :cond_e

    .line 168
    :cond_b
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v6

    .line 169
    .local v6, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v7, v6}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    .local v1, "category_name":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v8, 0x7f0e0463

    new-array v9, v11, [Ljava/lang/Object;

    aput-object v1, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "category_menu":Ljava/lang/String;
    const v7, 0x7f0f026e

    invoke-static {p1, v7, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 172
    const v7, 0x7f0f0297

    invoke-static {p1, v7, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 173
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_c

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_d

    .line 174
    :cond_c
    const v7, 0x7f0f0283

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 176
    :cond_d
    const v7, 0x7f0f02d2

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 177
    const v7, 0x7f0f028a

    invoke-static {p1, v7, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 178
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateGIF:Z

    if-eqz v7, :cond_e

    .line 179
    invoke-static {p1, v12, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    .end local v0    # "category_menu":Ljava/lang/String;
    .end local v1    # "category_name":Ljava/lang/String;
    .end local v6    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_e
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v7, :cond_10

    if-eqz v3, :cond_10

    .line 183
    const v7, 0x7f0f028e

    invoke-static {p1, v7, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 188
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v7}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v7

    if-nez v7, :cond_f

    .line 189
    const v7, 0x7f0f0283

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 191
    :cond_f
    return-void

    .line 185
    :cond_10
    const v7, 0x7f0f028e

    invoke-static {p1, v7, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method protected setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$3;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$3;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 304
    return-void
.end method

.method public setTitle(Ljava/lang/String;I)V
    .locals 2
    .param p1, "albumName"    # Ljava/lang/String;
    .param p2, "numberOfItemsInAlbum"    # I

    .prologue
    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;

    invoke-direct {v1, p0, p1, p2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$4;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 334
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 69
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->showAllButtonsOnTopLeft()V

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$2;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 95
    :cond_0
    return-void
.end method

.method public showCameraButton(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 353
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 354
    .local v0, "visibility":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;

    if-eqz v1, :cond_0

    .line 355
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mCameraButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 357
    :cond_0
    return-void

    .line 353
    .end local v0    # "visibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateFuntionSimplificationMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 361
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->updateFuntionSimplificationMenu(Landroid/view/Menu;)Z

    .line 367
    const/4 v0, 0x1

    return v0
.end method
