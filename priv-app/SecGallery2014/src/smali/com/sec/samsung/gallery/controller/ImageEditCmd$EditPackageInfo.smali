.class public Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;
.super Ljava/lang/Object;
.source "ImageEditCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ImageEditCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "EditPackageInfo"
.end annotation


# instance fields
.field private final mAppName:Ljava/lang/String;

.field private final mClassName:Ljava/lang/String;

.field private final mPackageName:Ljava/lang/String;

.field private final mResolveInfo:Landroid/content/pm/ResolveInfo;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ResolveInfo;)V
    .locals 0
    .param p2, "appName"    # Ljava/lang/String;
    .param p3, "packageName"    # Ljava/lang/String;
    .param p4, "className"    # Ljava/lang/String;
    .param p5, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 255
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 256
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mAppName:Ljava/lang/String;

    .line 257
    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mPackageName:Ljava/lang/String;

    .line 258
    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mClassName:Ljava/lang/String;

    .line 259
    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    .line 260
    return-void
.end method


# virtual methods
.method public getAppName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mAppName:Ljava/lang/String;

    return-object v0
.end method

.method public getClassName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mClassName:Ljava/lang/String;

    return-object v0
.end method

.method public getIcon()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 263
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->access$300(Lcom/sec/samsung/gallery/controller/ImageEditCmd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->getAppIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 264
    .local v0, "icon":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    const-string v1, "com.sec.android.mimage.photoretouching"

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mPackageName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->access$300(Lcom/sec/samsung/gallery/controller/ImageEditCmd;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201b5

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 268
    :cond_0
    return-object v0
.end method

.method public getPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mPackageName:Ljava/lang/String;

    return-object v0
.end method

.method public getResolveInfo()Landroid/content/pm/ResolveInfo;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    return-object v0
.end method
