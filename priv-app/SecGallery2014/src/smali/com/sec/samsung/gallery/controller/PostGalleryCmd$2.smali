.class Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;
.super Landroid/database/ContentObserver;
.source "PostGalleryCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/PostGalleryCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 364
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$500(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    .line 365
    .local v2, "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$600()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DCM mCategoryContentObserver onChange"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    const/4 v3, 0x0

    .line 367
    .local v3, "operation":Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 368
    invoke-virtual {p2}, Landroid/net/Uri;->getEncodedQuery()Ljava/lang/String;

    move-result-object v3

    .line 369
    :cond_0
    if-eqz v3, :cond_2

    const-string v5, "UPDATE"

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 370
    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$600()Ljava/lang/String;

    move-result-object v5

    const-string v6, "drawer DCM mCategoryContentObserver onChange UPDATE"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 371
    const-string v5, "UPDATE_CATEGORY"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 373
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$500(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 374
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$500(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/app/Activity;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    .line 375
    .local v4, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v5

    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SCENERY:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v6

    if-lt v5, v6, :cond_3

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v5

    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FLOWERS:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v6

    if-gt v5, v6, :cond_3

    instance-of v5, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v5, :cond_1

    instance-of v5, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v5, :cond_3

    .line 377
    :cond_1
    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$600()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UPDATE_SCREEN_CAHNGE_CATEGORY : CATEGORY "

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/PostGalleryCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$500(Lcom/sec/samsung/gallery/controller/PostGalleryCmd;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/sec/android/gallery3d/data/CategoryAlbum;->UPDATE_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 390
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v2    # "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    .end local v3    # "operation":Ljava/lang/String;
    .end local v4    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_2
    :goto_0
    return-void

    .line 379
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v2    # "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    .restart local v3    # "operation":Ljava/lang/String;
    .restart local v4    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_3
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_4

    instance-of v5, v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v5, :cond_4

    .line 380
    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$600()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DCM onChange onDirty : CLUSTER_BY_FACE"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 387
    .end local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v2    # "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    .end local v3    # "operation":Ljava/lang/String;
    .end local v4    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :catch_0
    move-exception v1

    .line 388
    .local v1, "e":Ljava/lang/Exception;
    const-string v5, "TAG"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mCategoryContentObserver can\'t update images: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 382
    .end local v1    # "e":Ljava/lang/Exception;
    .restart local v0    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v2    # "galleryFacade":Lcom/sec/samsung/gallery/core/GalleryFacade;
    .restart local v3    # "operation":Ljava/lang/String;
    .restart local v4    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_4
    :try_start_1
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_2

    .line 383
    # getter for: Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/PostGalleryCmd;->access$600()Ljava/lang/String;

    move-result-object v5

    const-string v6, "DCM onChange onDirty : CLUSTER_BY_SEARCH"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 384
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty(Z)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method
