.class public Lcom/sec/samsung/gallery/view/utils/MenuHelper;
.super Ljava/lang/Object;
.source "MenuHelper.java"


# static fields
.field private static final PREFIX_HAS_HELP:Ljava/lang/String; = "help-has-"

.field private static final PREFIX_UPDATE_HELP:Ljava/lang/String; = "help-update-"

.field public static final SHARE_MAX_ITEM_LIMIT:I

.field private static final TAG:Ljava/lang/String;

.field private static mResolveInfo:Landroid/content/pm/ResolveInfo;

.field private static mSystemUiVisibility:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/samsung/gallery/view/utils/MenuHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    .line 49
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChooserActivity:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x64

    :goto_0
    sput v0, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    .line 294
    const/4 v0, -0x1

    sput v0, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I

    return-void

    .line 49
    :cond_0
    const/16 v0, 0x1f4

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkAddUserTagSupport(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;J)J
    .locals 7
    .param p0, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p1, "supportOperation"    # J

    .prologue
    .line 469
    const-wide/high16 v2, 0x1000000000000000L

    and-long/2addr v2, p1

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 470
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkVideoItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    .line 471
    .local v0, "bContainsVideoItem":Z
    if-eqz v0, :cond_0

    .line 472
    const-wide v2, -0x1000000000000001L    # -3.1050361846014175E231

    and-long/2addr p1, v2

    .line 475
    .end local v0    # "bContainsVideoItem":Z
    :cond_0
    return-wide p1
.end method

.method public static checkDropboxItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 2
    .param p0, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 453
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 454
    .local v0, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->containsDropboxItems()Z

    move-result v1

    return v1
.end method

.method public static checkImageItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 2
    .param p0, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 465
    .local v0, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isImagePresentInSelectedItems()Z

    move-result v1

    return v1
.end method

.method public static checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "supportOperation"    # J

    .prologue
    .line 398
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J

    move-result-wide v0

    return-wide v0
.end method

.method public static checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;JZ)J
    .locals 28
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "supportOperation"    # J
    .param p4, "needUpdate"    # Z

    .prologue
    .line 402
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v23

    if-eqz v23, :cond_7

    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v23

    move-object/from16 v0, v23

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v23, v0

    const/16 v24, 0x2

    move/from16 v0, v23

    move/from16 v1, v24

    if-ne v0, v1, :cond_6

    const/16 v18, 0x1

    .line 404
    .local v18, "showLastShare":Z
    :goto_0
    if-eqz v18, :cond_8

    const-wide/16 v24, 0x4

    and-long v24, v24, p2

    const-wide/16 v26, 0x0

    cmp-long v23, v24, v26

    if-eqz v23, :cond_8

    move-object/from16 v23, p0

    .line 405
    check-cast v23, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkPicasaItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v9

    .local v9, "containsPicasa":Z
    move-object/from16 v23, p0

    .line 406
    check-cast v23, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkDropboxItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v7

    .line 407
    .local v7, "containsDropboxItems":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppDropBox(Landroid/content/Context;)Z

    move-result v11

    .line 408
    .local v11, "dropbox":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppFacebook(Landroid/content/Context;)Z

    move-result v12

    .line 409
    .local v12, "facebook":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppBluetooth(Landroid/content/Context;)Z

    move-result v5

    .line 410
    .local v5, "bluetooth":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppWiFiDirect(Landroid/content/Context;)Z

    move-result v22

    .local v22, "wifiDirect":Z
    move-object/from16 v23, p0

    .line 412
    check-cast v23, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkVideoItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v10

    .local v10, "containsVideo":Z
    move-object/from16 v23, p0

    .line 413
    check-cast v23, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkImageItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v8

    .line 414
    .local v8, "containsImage":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppYouTube(Landroid/content/Context;)Z

    move-result v16

    .line 415
    .local v16, "isYouTube":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppGroupPlay(Landroid/content/Context;)Z

    move-result v15

    .line 416
    .local v15, "isGroupPlay":Z
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->isLastShareAppExisted(Landroid/content/Context;)Z

    move-result v23

    if-eqz v23, :cond_8

    if-eqz v9, :cond_0

    if-nez v11, :cond_8

    if-nez v12, :cond_8

    if-nez v22, :cond_8

    :cond_0
    if-eqz v7, :cond_1

    if-nez v5, :cond_8

    :cond_1
    if-nez v10, :cond_2

    if-nez v16, :cond_8

    :cond_2
    if-nez v8, :cond_3

    if-nez v15, :cond_8

    .line 417
    :cond_3
    if-eqz p4, :cond_4

    .line 418
    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getResolveInfo(Landroid/content/Context;)Landroid/content/pm/ResolveInfo;

    move-result-object v23

    sput-object v23, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    .line 420
    :cond_4
    sget-object v23, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    if-eqz v23, :cond_8

    .line 421
    const-wide v24, 0x80000000000L

    or-long p2, p2, v24

    .line 422
    sget-object v23, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->updateLastShareAppName(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)V

    .line 424
    sget-object v23, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getLastShareAppIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 425
    .local v14, "icon":Landroid/graphics/drawable/Drawable;
    if-nez v14, :cond_5

    .line 426
    const v23, 0x7f0f026c

    move-object/from16 v0, p1

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v17

    .line 427
    .local v17, "shareItem":Landroid/view/MenuItem;
    if-eqz v17, :cond_5

    .line 428
    invoke-interface/range {v17 .. v17}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v23

    mul-int/lit8 v23, v23, 0x9

    div-int/lit8 v19, v23, 0xa

    .line 429
    .local v19, "width":I
    invoke-interface/range {v17 .. v17}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v23

    mul-int/lit8 v23, v23, 0x9

    div-int/lit8 v13, v23, 0xa

    .line 430
    .local v13, "height":I
    sget-object v23, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    move/from16 v0, v19

    move-object/from16 v1, v23

    invoke-static {v0, v13, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 431
    .local v4, "bitmap":Landroid/graphics/Bitmap;
    new-instance v6, Landroid/graphics/Canvas;

    invoke-direct {v6, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 433
    .local v6, "canvas":Landroid/graphics/Canvas;
    sget-object v23, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mResolveInfo:Landroid/content/pm/ResolveInfo;

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getResizedIcon(Landroid/content/Context;Landroid/content/pm/ResolveInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 434
    const/16 v23, 0x0

    const/16 v24, 0x0

    move/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v19

    invoke-virtual {v14, v0, v1, v2, v13}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 435
    invoke-virtual {v14, v6}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 438
    .end local v4    # "bitmap":Landroid/graphics/Bitmap;
    .end local v6    # "canvas":Landroid/graphics/Canvas;
    .end local v13    # "height":I
    .end local v17    # "shareItem":Landroid/view/MenuItem;
    .end local v19    # "width":I
    :cond_5
    const v23, 0x7f0f026d

    invoke-static/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->getLastShareAppName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v23

    move-object/from16 v3, v24

    invoke-static {v0, v1, v2, v14, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIconAndTitle(Landroid/content/Context;Landroid/view/Menu;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    move-wide/from16 v20, p2

    .line 444
    .end local v5    # "bluetooth":Z
    .end local v7    # "containsDropboxItems":Z
    .end local v8    # "containsImage":Z
    .end local v9    # "containsPicasa":Z
    .end local v10    # "containsVideo":Z
    .end local v11    # "dropbox":Z
    .end local v12    # "facebook":Z
    .end local v14    # "icon":Landroid/graphics/drawable/Drawable;
    .end local v15    # "isGroupPlay":Z
    .end local v16    # "isYouTube":Z
    .end local v22    # "wifiDirect":Z
    .end local p2    # "supportOperation":J
    .local v20, "supportOperation":J
    :goto_1
    return-wide v20

    .line 402
    .end local v18    # "showLastShare":Z
    .end local v20    # "supportOperation":J
    .restart local p2    # "supportOperation":J
    :cond_6
    const/16 v18, 0x0

    goto/16 :goto_0

    :cond_7
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 443
    .restart local v18    # "showLastShare":Z
    :cond_8
    const-wide v24, -0x80000000001L

    and-long p2, p2, v24

    move-wide/from16 v20, p2

    .line 444
    .end local p2    # "supportOperation":J
    .restart local v20    # "supportOperation":J
    goto :goto_1
.end method

.method public static checkPicasaItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 2
    .param p0, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 448
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 449
    .local v0, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->containsPicasa()Z

    move-result v1

    return v1
.end method

.method public static checkShowHiddenMenu(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 8
    .param p0, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v5, 0x0

    .line 524
    const/4 v4, 0x0

    .line 525
    .local v4, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    .line 527
    sget-object v3, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 528
    .local v3, "mediaFilterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    if-eqz v4, :cond_0

    .line 529
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentMediaFilterType()Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v3

    .line 531
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v6

    invoke-static {v6, v3}, Lcom/sec/android/gallery3d/data/HiddenSource;->hasHiddenMedias(Lcom/sec/android/gallery3d/app/GalleryApp;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)Z

    move-result v2

    .line 533
    .local v2, "isContainsHiddenItems":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 534
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v6, "photo-pick"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_2

    move v2, v5

    .line 544
    .end local v0    # "bundle":Landroid/os/Bundle;
    .end local v2    # "isContainsHiddenItems":Z
    :cond_1
    :goto_0
    return v2

    .line 536
    .restart local v0    # "bundle":Landroid/os/Bundle;
    .restart local v2    # "isContainsHiddenItems":Z
    :cond_2
    const-string v6, "android.intent.extra.LOCAL_ONLY"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    if-eqz v6, :cond_3

    move v2, v5

    .line 537
    goto :goto_0

    .line 538
    :cond_3
    const-string v6, "caller_id_pick"

    const/4 v7, 0x0

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-eqz v6, :cond_1

    move v2, v5

    .line 539
    goto :goto_0

    .line 543
    .end local v0    # "bundle":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 544
    .local v1, "e":Ljava/lang/NullPointerException;
    goto :goto_0
.end method

.method public static checkVideoItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "context"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 458
    const-string/jumbo v1, "youtube issue"

    const-string v2, " menuhelper check for video item present"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 459
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 460
    .local v0, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isVideoPresentInSelectedItems()Z

    move-result v1

    return v1
.end method

.method private static getHelpMenuAvailable(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "version"    # I
    .param p2, "updateKey"    # Ljava/lang/String;
    .param p3, "hasKey"    # Ljava/lang/String;
    .param p4, "prefs"    # Landroid/content/SharedPreferences;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 508
    const/4 v1, 0x0

    .line 510
    .local v1, "info":Landroid/content/pm/PackageInfo;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const-string v5, "com.samsung.helphub"

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 511
    if-eqz v1, :cond_3

    .line 512
    iget v4, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    rem-int/lit8 v4, v4, 0xa
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v5, 0x2

    if-lt v4, v5, :cond_3

    .line 518
    if-eqz p2, :cond_1

    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, p2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-interface {v4, p3, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    move v2, v3

    .line 520
    :cond_2
    :goto_0
    return v2

    .line 518
    :cond_3
    if-eqz p2, :cond_2

    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, p2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    if-eqz v1, :cond_4

    :goto_1
    invoke-interface {v4, p3, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_4
    move v3, v2

    goto :goto_1

    .line 515
    :catch_0
    move-exception v0

    .line 516
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    sget-object v4, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "helphub is not exist. "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 518
    if-eqz p2, :cond_2

    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4, p2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    if-eqz v1, :cond_5

    :goto_2
    invoke-interface {v4, p3, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :cond_5
    move v3, v2

    goto :goto_2

    .end local v0    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :catchall_0
    move-exception v4

    if-eqz p2, :cond_6

    invoke-interface {p4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    invoke-interface {v5, p2, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v5

    if-eqz v1, :cond_7

    :goto_3
    invoke-interface {v5, p3, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_6
    throw v4

    :cond_7
    move v3, v2

    goto :goto_3
.end method

.method private static getSystemUiVisibility()I
    .locals 4

    .prologue
    .line 296
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 297
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I

    .line 305
    .local v1, "field":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 299
    .end local v1    # "field":Ljava/lang/reflect/Field;
    :cond_0
    :try_start_0
    const-class v2, Landroid/view/View;

    const-string v3, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 300
    .restart local v1    # "field":Ljava/lang/reflect/Field;
    invoke-virtual {v1, v1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v2

    sput v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 305
    :goto_1
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I

    goto :goto_0

    .line 301
    :catch_0
    move-exception v0

    .line 302
    .local v0, "e":Ljava/lang/Exception;
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    const-string v3, "can not find FLAG_REMOVE_NAVIGATION, use FLAG_HIDE_NAVIGATION instead"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    const/4 v2, 0x2

    sput v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->mSystemUiVisibility:I

    goto :goto_1
.end method

.method public static goToHelpGallery(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 339
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->isAvailableDownloadableHelp(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 340
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-nez v2, :cond_0

    .line 341
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->requestDownloadingResource(Landroid/content/Context;)V

    .line 359
    :goto_0
    return-void

    .line 343
    :cond_0
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->requestDownloadingResourceForChn(Landroid/content/Context;)V

    goto :goto_0

    .line 346
    :cond_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.helphub.HELP"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 347
    .local v1, "helpIntent":Landroid/content/Intent;
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOnlineHelp:Z

    if-eqz v2, :cond_2

    .line 348
    const-string v2, "helphub:appid"

    const-string v3, "gallery"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 350
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 353
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 356
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_2
    const-string v2, "helphub:section"

    const-string v3, "gallery"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 357
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public static goToHome(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 325
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 326
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.MAIN"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 327
    const-string v2, "android.intent.category.HOME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 329
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 335
    :goto_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->closeCameraLens(Landroid/content/Context;)V

    .line 336
    return-void

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static goToSetupWidzard(Landroid/content/Context;)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 309
    sget-object v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->TAG:Ljava/lang/String;

    const-string v3, "goToSetupWidzard"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 311
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.SecSetupWizard"

    const-string v3, "com.sec.android.app.SecSetupWizard.SecSetupWizardActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    const-string v2, "fromCamera"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 315
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move-object v2, p0

    .line 319
    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/app/Activity;->setResult(I)V

    move-object v2, p0

    .line 320
    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 321
    check-cast p0, Landroid/app/Activity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0, v4, v4}, Landroid/app/Activity;->overridePendingTransition(II)V

    .line 322
    return-void

    .line 316
    .restart local p0    # "context":Landroid/content/Context;
    :catch_0
    move-exception v0

    .line 317
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static hideStatusIcon(Ljava/lang/Object;)V
    .locals 4
    .param p0, "object"    # Ljava/lang/Object;

    .prologue
    .line 277
    const/4 v2, 0x0

    .line 279
    .local v2, "params":Landroid/view/WindowManager$LayoutParams;
    if-eqz p0, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 280
    instance-of v3, p0, Landroid/app/Activity;

    if-eqz v3, :cond_1

    move-object v0, p0

    .line 281
    check-cast v0, Landroid/app/Activity;

    .line 282
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 283
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->getSystemUiVisibility()I

    move-result v3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 284
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 292
    .end local v0    # "activity":Landroid/app/Activity;
    :cond_0
    :goto_0
    return-void

    .line 285
    :cond_1
    instance-of v3, p0, Landroid/app/Dialog;

    if-eqz v3, :cond_0

    move-object v1, p0

    .line 286
    check-cast v1, Landroid/app/Dialog;

    .line 287
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    .line 288
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->getSystemUiVisibility()I

    move-result v3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->systemUiVisibility:I

    .line 289
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0
.end method

.method public static isHelpMenuAvailable(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 482
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHelp:Z

    if-nez v5, :cond_1

    .line 503
    :cond_0
    :goto_0
    return v4

    .line 490
    :cond_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/app/PackagesMonitor;->getPackagesVersion(Landroid/content/Context;)I

    move-result v3

    .line 492
    .local v3, "version":I
    const-string v2, "help-update-"

    .line 493
    .local v2, "updateKey":Ljava/lang/String;
    const-string v0, "help-has-"

    .line 495
    .local v0, "hasKey":Ljava/lang/String;
    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 496
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const/4 v5, 0x1

    invoke-interface {v1, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 497
    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-eq v4, v3, :cond_2

    .line 498
    invoke-static {p0, v3, v2, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->getHelpMenuAvailable(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)Z

    move-result v4

    goto :goto_0

    .line 500
    :cond_2
    const/4 v4, 0x0

    invoke-static {p0, v3, v4, v0, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->getHelpMenuAvailable(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Landroid/content/SharedPreferences;)Z

    move-result v4

    goto :goto_0
.end method

.method public static isOnlyIconDisplayedOnActionBar(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 392
    invoke-static {p0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->isOnlyTextDisplayedOnActionBar(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public static isOnlyTextDisplayedOnActionBar(Landroid/content/Context;)Z
    .locals 7
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 362
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 386
    :goto_0
    return v5

    .line 364
    :cond_0
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isQvga(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v5, v4

    .line 365
    goto :goto_0

    .line 367
    :cond_1
    const-string/jumbo v6, "window"

    invoke-virtual {p0, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    .line 368
    .local v3, "wm":Landroid/view/WindowManager;
    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 369
    .local v0, "display":Landroid/view/Display;
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v1

    .line 370
    .local v1, "rotation":I
    const/4 v2, 0x0

    .line 372
    .local v2, "textOnly":Z
    packed-switch v1, :pswitch_data_0

    .line 383
    :goto_1
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isLandScapeModel(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 384
    if-nez v2, :cond_3

    move v2, v4

    :cond_2
    :goto_2
    move v5, v2

    .line 386
    goto :goto_0

    .line 375
    :pswitch_0
    const/4 v2, 0x1

    .line 376
    goto :goto_1

    .line 379
    :pswitch_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    move v2, v5

    .line 384
    goto :goto_2

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static setMenuGroupVisibility(Landroid/view/Menu;IZ)V
    .locals 0
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "group_id"    # I
    .param p2, "visibility"    # Z

    .prologue
    .line 202
    if-nez p0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 205
    :cond_0
    invoke-interface {p0, p1, p2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto :goto_0
.end method

.method public static setMenuItemEnable(Landroid/view/Menu;IZ)V
    .locals 3
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "isEnabled"    # Z

    .prologue
    .line 229
    if-nez p0, :cond_1

    .line 238
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 233
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 234
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 235
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 236
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz p2, :cond_2

    const/16 v1, 0xff

    :goto_1
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x64

    goto :goto_1
.end method

.method public static setMenuItemEnabled(Landroid/view/Menu;IZ)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 209
    if-nez p0, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 213
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 214
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static setMenuItemIcon(Landroid/view/Menu;II)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "resId"    # I

    .prologue
    .line 241
    if-nez p0, :cond_1

    .line 247
    :cond_0
    :goto_0
    return-void

    .line 244
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 245
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 246
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static setMenuItemIconAndTitle(Landroid/content/Context;Landroid/view/Menu;ILandroid/graphics/drawable/Drawable;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "id"    # I
    .param p3, "icon"    # Landroid/graphics/drawable/Drawable;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 218
    if-nez p1, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 221
    :cond_1
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 222
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 223
    invoke-interface {v0, p3}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    .line 224
    invoke-interface {v0, p4}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static setMenuItemShowAsAction(Landroid/view/Menu;II)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "action"    # I

    .prologue
    .line 268
    if-nez p0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 272
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 273
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    goto :goto_0
.end method

.method public static setMenuItemTitle(Landroid/view/Menu;II)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "stringId"    # I

    .prologue
    .line 250
    if-nez p0, :cond_1

    .line 256
    :cond_0
    :goto_0
    return-void

    .line 253
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 254
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 255
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 259
    if-nez p0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 263
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 264
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static setMenuItemVisibility(Landroid/view/Menu;IZ)V
    .locals 1
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "id"    # I
    .param p2, "visibility"    # Z

    .prologue
    .line 193
    if-nez p0, :cond_1

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    invoke-interface {p0, p1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 197
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 198
    invoke-interface {v0, p2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public static updateMenuOperation(Landroid/view/Menu;J)V
    .locals 51
    .param p0, "menu"    # Landroid/view/Menu;
    .param p1, "supported"    # J

    .prologue
    .line 52
    const-wide/16 v46, 0x1

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_c

    const/4 v15, 0x1

    .line 53
    .local v15, "supportDelete":Z
    :goto_0
    const-wide/16 v46, 0x2

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_d

    const/16 v37, 0x1

    .line 54
    .local v37, "supportRotate":Z
    :goto_1
    const-wide/16 v46, 0x4

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_e

    const/16 v41, 0x1

    .line 55
    .local v41, "supportShare":Z
    :goto_2
    const-wide/16 v46, 0x8

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_f

    const/4 v14, 0x1

    .line 56
    .local v14, "supportCrop":Z
    :goto_3
    const-wide/16 v46, 0x10

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_10

    const/16 v42, 0x1

    .line 57
    .local v42, "supportShowOnMap":Z
    :goto_4
    const-wide/16 v46, 0x20

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_11

    const/16 v40, 0x1

    .line 58
    .local v40, "supportSetAs":Z
    :goto_5
    const-wide/16 v46, 0x200

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_12

    const/16 v18, 0x1

    .line 59
    .local v18, "supportEdit":Z
    :goto_6
    const-wide/32 v46, 0x200000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_13

    const/16 v38, 0x1

    .line 60
    .local v38, "supportSStudio":Z
    :goto_7
    const-wide/high16 v46, 0x1000000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_14

    const/4 v5, 0x1

    .line 61
    .local v5, "supportAddTag":Z
    :goto_8
    const-wide/high16 v46, -0x8000000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_15

    const/16 v33, 0x1

    .line 62
    .local v33, "supportPrint":Z
    :goto_9
    const-wide/high16 v46, 0x4000000000000000L    # 2.0

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_16

    const/16 v36, 0x1

    .line 63
    .local v36, "supportRename":Z
    :goto_a
    const-wide/high16 v46, 0x2000000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_17

    const/4 v11, 0x1

    .line 64
    .local v11, "supportCopyToClipboard":Z
    :goto_b
    const-wide/high16 v46, 0x20000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_18

    const/4 v9, 0x1

    .line 65
    .local v9, "supportCopy":Z
    :goto_c
    const-wide/high16 v46, 0x10000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_19

    const/16 v29, 0x1

    .line 66
    .local v29, "supportMove":Z
    :goto_d
    const-wide/high16 v46, 0x4000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1a

    const/16 v21, 0x1

    .line 67
    .local v21, "supportHideAlbum":Z
    :goto_e
    const-wide v46, 0x400000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1b

    const/16 v43, 0x1

    .line 68
    .local v43, "supportSlideShow":Z
    :goto_f
    const-wide/high16 v46, 0x800000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1c

    const/4 v6, 0x1

    .line 69
    .local v6, "supportAssignName":Z
    :goto_10
    const-wide/high16 v46, 0x400000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1d

    const/4 v8, 0x1

    .line 70
    .local v8, "supportConfirm":Z
    :goto_11
    const-wide/high16 v46, 0x200000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1e

    const/16 v34, 0x1

    .line 71
    .local v34, "supportRemove":Z
    :goto_12
    const-wide v46, 0x200000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_1f

    const/16 v20, 0x1

    .line 72
    .local v20, "supportGetText":Z
    :goto_13
    const-wide v46, 0x100000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_20

    const/4 v7, 0x1

    .line 73
    .local v7, "supportBurstPlay":Z
    :goto_14
    const-wide v46, 0x200000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_21

    const/16 v28, 0x1

    .line 74
    .local v28, "supportMotionPicture":Z
    :goto_15
    const-wide/16 v46, 0x400

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_22

    const/16 v24, 0x1

    .line 75
    .local v24, "supportInfo":Z
    :goto_16
    const-wide v46, 0x800000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_23

    const/16 v17, 0x1

    .line 76
    .local v17, "supportDownloadViaDownloadManager":Z
    :goto_17
    const-wide/high16 v46, 0x1000000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_24

    const/16 v16, 0x1

    .line 77
    .local v16, "supportDownloadSLink":Z
    :goto_18
    const-wide v46, 0x80000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_25

    const/16 v25, 0x1

    .line 78
    .local v25, "supportLastShare":Z
    :goto_19
    const-wide v46, 0x8000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_26

    const/16 v27, 0x1

    .line 79
    .local v27, "supportMmsSave":Z
    :goto_1a
    const-wide v46, 0x1000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_27

    const/16 v44, 0x1

    .line 80
    .local v44, "supportVideoTrim":Z
    :goto_1b
    const-wide v46, 0x400000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_28

    const/16 v22, 0x1

    .line 81
    .local v22, "supportImageNote":Z
    :goto_1c
    const-wide/32 v46, 0x80000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_29

    const/16 v19, 0x1

    .line 82
    .local v19, "supportFlashAnnotate":Z
    :goto_1d
    const-wide v46, 0x100000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2a

    const/16 v32, 0x1

    .line 83
    .local v32, "supportPhotoSignature":Z
    :goto_1e
    const-wide/32 v46, 0x40000000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2b

    const/16 v31, 0x1

    .line 84
    .local v31, "supportMoveToSecretbox":Z
    :goto_1f
    const-wide/32 v46, 0x20000000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2c

    const/16 v35, 0x1

    .line 85
    .local v35, "supportRemoveFromSecretbox":Z
    :goto_20
    const-wide/32 v46, 0x10000000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2d

    const/4 v13, 0x1

    .line 86
    .local v13, "supportCreateVideoAlbum":Z
    :goto_21
    const-wide/32 v46, 0x4000000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2e

    const/16 v26, 0x1

    .line 87
    .local v26, "supportMFD":Z
    :goto_22
    const-wide/32 v46, 0x800000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_2f

    const/4 v12, 0x1

    .line 88
    .local v12, "supportCopyToEvent":Z
    :goto_23
    const-wide/32 v46, 0x400000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_30

    const/4 v10, 0x1

    .line 89
    .local v10, "supportCopyToAlbum":Z
    :goto_24
    const-wide/32 v46, 0x100000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_31

    const/16 v30, 0x1

    .line 90
    .local v30, "supportMoveToAlbum":Z
    :goto_25
    const-wide/32 v46, 0x40000

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_32

    const/16 v39, 0x1

    .line 91
    .local v39, "supportSendtoOtherDevices":Z
    :goto_26
    const-wide v46, 0x20000000000L

    and-long v46, v46, p1

    const-wide/16 v48, 0x0

    cmp-long v45, v46, v48

    if-eqz v45, :cond_33

    const/16 v23, 0x1

    .line 93
    .local v23, "supportImport":Z
    :goto_27
    const v45, 0x7f0f026e

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 94
    const v46, 0x7f0f0280

    if-eqz v37, :cond_34

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryRotate:Z

    if-eqz v45, :cond_34

    const/16 v45, 0x1

    :goto_28
    move-object/from16 v0, p0

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 95
    const v46, 0x7f0f0281

    if-eqz v37, :cond_35

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryRotate:Z

    if-eqz v45, :cond_35

    const/16 v45, 0x1

    :goto_29
    move-object/from16 v0, p0

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 97
    const v46, 0x7f0f02a8

    if-eqz v14, :cond_36

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryCrop:Z

    if-eqz v45, :cond_36

    const/16 v45, 0x1

    :goto_2a
    move-object/from16 v0, p0

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 99
    const v45, 0x7f0f026c

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 100
    const v45, 0x7f0f026d

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v25

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 101
    const v45, 0x7f0f029a

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v40

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    const v45, 0x7f0f029c

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v18

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 103
    const v45, 0x7f0f0283

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v38

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 104
    const v46, 0x7f0f0282

    if-eqz v5, :cond_37

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v45, :cond_37

    const/16 v45, 0x1

    :goto_2b
    move-object/from16 v0, p0

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 105
    const v45, 0x7f0f0285

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v33

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 106
    const v45, 0x7f0f02af

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 107
    const v45, 0x7f0f0270

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v36

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 108
    const v45, 0x7f0f02a7

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v11}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 110
    const v45, 0x7f0f02b8

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v26

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 112
    const v45, 0x7f0f027d

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v9}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 113
    const v45, 0x7f0f027e

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v29

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 114
    const v45, 0x7f0f0271

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v21

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 115
    const v45, 0x7f0f026f

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v43

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 116
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBurstShotSettingIcon:Z

    if-eqz v45, :cond_38

    .line 117
    const v45, 0x7f0f02a3

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 121
    :goto_2c
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v45, :cond_39

    .line 122
    const v45, 0x7f0f02b3

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 123
    const v45, 0x7f0f02b4

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 128
    :goto_2d
    const v45, 0x7f0f029b

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 129
    const v45, 0x7f0f02a1

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v16

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 130
    const v45, 0x7f0f0276

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v17

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 131
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v45, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v45

    if-eqz v45, :cond_0

    .line 132
    const v45, 0x7f0f0277

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v31

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 133
    const v45, 0x7f0f0278

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v35

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 135
    :cond_0
    const v45, 0x7f0f02cd

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v13}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 138
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v45, :cond_1

    .line 139
    const v45, 0x7f0f02af

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 140
    const v45, 0x7f0f02b0

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 141
    const v45, 0x7f0f02a9

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v24

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 143
    :cond_1
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableShowMap:Z

    if-eqz v45, :cond_2

    .line 144
    const v45, 0x7f0f02af

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 147
    :cond_2
    const v45, 0x7f0f02b1

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v20

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 149
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNoteEditor:Z

    if-eqz v45, :cond_6

    .line 150
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUSA_PhotoEditTilteName:Z

    if-nez v45, :cond_3

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_PhotoEditTilteName:Z

    if-eqz v45, :cond_5

    .line 151
    :cond_3
    const v45, 0x7f0f02a4

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 152
    .local v4, "item":Landroid/view/MenuItem;
    if-eqz v4, :cond_5

    .line 153
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUSA_PhotoEditTilteName:Z

    if-eqz v45, :cond_4

    .line 154
    const v45, 0x7f0e0200

    move/from16 v0, v45

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 155
    :cond_4
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseATT_PhotoEditTilteName:Z

    if-eqz v45, :cond_5

    .line 156
    const v45, 0x7f0e0201

    move/from16 v0, v45

    invoke-interface {v4, v0}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 159
    .end local v4    # "item":Landroid/view/MenuItem;
    :cond_5
    const v45, 0x7f0f02a4

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v22

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 161
    :cond_6
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlashAnnotate:Z

    if-eqz v45, :cond_7

    .line 162
    const v45, 0x7f0f02a5

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-interface {v0, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 163
    .restart local v4    # "item":Landroid/view/MenuItem;
    const v45, 0x7f0f02a5

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v19

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 166
    .end local v4    # "item":Landroid/view/MenuItem;
    :cond_7
    const v45, 0x7f0f0273

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    const v45, 0x7f0f0272

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v8}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 168
    const v45, 0x7f0f0274

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v34

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 169
    const v46, 0x7f0f02b2

    if-eqz v27, :cond_3a

    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSaveMMSImage:Z

    if-eqz v45, :cond_3a

    const/16 v45, 0x1

    :goto_2e
    move-object/from16 v0, p0

    move/from16 v1, v46

    move/from16 v2, v45

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 170
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTrimAvailability:Z

    if-eqz v45, :cond_8

    .line 171
    const v45, 0x7f0f029f

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v44

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 173
    :cond_8
    const v45, 0x7f0f02a6

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v32

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 175
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGetDirection:Z

    if-nez v45, :cond_9

    .line 176
    const v45, 0x7f0f02b0

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 178
    :cond_9
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v45, :cond_a

    .line 179
    const v45, 0x7f0f0271

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    :cond_a
    sget-boolean v45, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v45, :cond_b

    .line 183
    const v45, 0x7f0f0298

    const/16 v46, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 185
    :cond_b
    const v45, 0x7f0f027f

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v12}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 186
    const v45, 0x7f0f02bc

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 187
    const v45, 0x7f0f02c6

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v30

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 188
    const v45, 0x7f0f02ad

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v39

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 189
    const v45, 0x7f0f027c

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v23

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 190
    return-void

    .line 52
    .end local v5    # "supportAddTag":Z
    .end local v6    # "supportAssignName":Z
    .end local v7    # "supportBurstPlay":Z
    .end local v8    # "supportConfirm":Z
    .end local v9    # "supportCopy":Z
    .end local v10    # "supportCopyToAlbum":Z
    .end local v11    # "supportCopyToClipboard":Z
    .end local v12    # "supportCopyToEvent":Z
    .end local v13    # "supportCreateVideoAlbum":Z
    .end local v14    # "supportCrop":Z
    .end local v15    # "supportDelete":Z
    .end local v16    # "supportDownloadSLink":Z
    .end local v17    # "supportDownloadViaDownloadManager":Z
    .end local v18    # "supportEdit":Z
    .end local v19    # "supportFlashAnnotate":Z
    .end local v20    # "supportGetText":Z
    .end local v21    # "supportHideAlbum":Z
    .end local v22    # "supportImageNote":Z
    .end local v23    # "supportImport":Z
    .end local v24    # "supportInfo":Z
    .end local v25    # "supportLastShare":Z
    .end local v26    # "supportMFD":Z
    .end local v27    # "supportMmsSave":Z
    .end local v28    # "supportMotionPicture":Z
    .end local v29    # "supportMove":Z
    .end local v30    # "supportMoveToAlbum":Z
    .end local v31    # "supportMoveToSecretbox":Z
    .end local v32    # "supportPhotoSignature":Z
    .end local v33    # "supportPrint":Z
    .end local v34    # "supportRemove":Z
    .end local v35    # "supportRemoveFromSecretbox":Z
    .end local v36    # "supportRename":Z
    .end local v37    # "supportRotate":Z
    .end local v38    # "supportSStudio":Z
    .end local v39    # "supportSendtoOtherDevices":Z
    .end local v40    # "supportSetAs":Z
    .end local v41    # "supportShare":Z
    .end local v42    # "supportShowOnMap":Z
    .end local v43    # "supportSlideShow":Z
    .end local v44    # "supportVideoTrim":Z
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_0

    .line 53
    .restart local v15    # "supportDelete":Z
    :cond_d
    const/16 v37, 0x0

    goto/16 :goto_1

    .line 54
    .restart local v37    # "supportRotate":Z
    :cond_e
    const/16 v41, 0x0

    goto/16 :goto_2

    .line 55
    .restart local v41    # "supportShare":Z
    :cond_f
    const/4 v14, 0x0

    goto/16 :goto_3

    .line 56
    .restart local v14    # "supportCrop":Z
    :cond_10
    const/16 v42, 0x0

    goto/16 :goto_4

    .line 57
    .restart local v42    # "supportShowOnMap":Z
    :cond_11
    const/16 v40, 0x0

    goto/16 :goto_5

    .line 58
    .restart local v40    # "supportSetAs":Z
    :cond_12
    const/16 v18, 0x0

    goto/16 :goto_6

    .line 59
    .restart local v18    # "supportEdit":Z
    :cond_13
    const/16 v38, 0x0

    goto/16 :goto_7

    .line 60
    .restart local v38    # "supportSStudio":Z
    :cond_14
    const/4 v5, 0x0

    goto/16 :goto_8

    .line 61
    .restart local v5    # "supportAddTag":Z
    :cond_15
    const/16 v33, 0x0

    goto/16 :goto_9

    .line 62
    .restart local v33    # "supportPrint":Z
    :cond_16
    const/16 v36, 0x0

    goto/16 :goto_a

    .line 63
    .restart local v36    # "supportRename":Z
    :cond_17
    const/4 v11, 0x0

    goto/16 :goto_b

    .line 64
    .restart local v11    # "supportCopyToClipboard":Z
    :cond_18
    const/4 v9, 0x0

    goto/16 :goto_c

    .line 65
    .restart local v9    # "supportCopy":Z
    :cond_19
    const/16 v29, 0x0

    goto/16 :goto_d

    .line 66
    .restart local v29    # "supportMove":Z
    :cond_1a
    const/16 v21, 0x0

    goto/16 :goto_e

    .line 67
    .restart local v21    # "supportHideAlbum":Z
    :cond_1b
    const/16 v43, 0x0

    goto/16 :goto_f

    .line 68
    .restart local v43    # "supportSlideShow":Z
    :cond_1c
    const/4 v6, 0x0

    goto/16 :goto_10

    .line 69
    .restart local v6    # "supportAssignName":Z
    :cond_1d
    const/4 v8, 0x0

    goto/16 :goto_11

    .line 70
    .restart local v8    # "supportConfirm":Z
    :cond_1e
    const/16 v34, 0x0

    goto/16 :goto_12

    .line 71
    .restart local v34    # "supportRemove":Z
    :cond_1f
    const/16 v20, 0x0

    goto/16 :goto_13

    .line 72
    .restart local v20    # "supportGetText":Z
    :cond_20
    const/4 v7, 0x0

    goto/16 :goto_14

    .line 73
    .restart local v7    # "supportBurstPlay":Z
    :cond_21
    const/16 v28, 0x0

    goto/16 :goto_15

    .line 74
    .restart local v28    # "supportMotionPicture":Z
    :cond_22
    const/16 v24, 0x0

    goto/16 :goto_16

    .line 75
    .restart local v24    # "supportInfo":Z
    :cond_23
    const/16 v17, 0x0

    goto/16 :goto_17

    .line 76
    .restart local v17    # "supportDownloadViaDownloadManager":Z
    :cond_24
    const/16 v16, 0x0

    goto/16 :goto_18

    .line 77
    .restart local v16    # "supportDownloadSLink":Z
    :cond_25
    const/16 v25, 0x0

    goto/16 :goto_19

    .line 78
    .restart local v25    # "supportLastShare":Z
    :cond_26
    const/16 v27, 0x0

    goto/16 :goto_1a

    .line 79
    .restart local v27    # "supportMmsSave":Z
    :cond_27
    const/16 v44, 0x0

    goto/16 :goto_1b

    .line 80
    .restart local v44    # "supportVideoTrim":Z
    :cond_28
    const/16 v22, 0x0

    goto/16 :goto_1c

    .line 81
    .restart local v22    # "supportImageNote":Z
    :cond_29
    const/16 v19, 0x0

    goto/16 :goto_1d

    .line 82
    .restart local v19    # "supportFlashAnnotate":Z
    :cond_2a
    const/16 v32, 0x0

    goto/16 :goto_1e

    .line 83
    .restart local v32    # "supportPhotoSignature":Z
    :cond_2b
    const/16 v31, 0x0

    goto/16 :goto_1f

    .line 84
    .restart local v31    # "supportMoveToSecretbox":Z
    :cond_2c
    const/16 v35, 0x0

    goto/16 :goto_20

    .line 85
    .restart local v35    # "supportRemoveFromSecretbox":Z
    :cond_2d
    const/4 v13, 0x0

    goto/16 :goto_21

    .line 86
    .restart local v13    # "supportCreateVideoAlbum":Z
    :cond_2e
    const/16 v26, 0x0

    goto/16 :goto_22

    .line 87
    .restart local v26    # "supportMFD":Z
    :cond_2f
    const/4 v12, 0x0

    goto/16 :goto_23

    .line 88
    .restart local v12    # "supportCopyToEvent":Z
    :cond_30
    const/4 v10, 0x0

    goto/16 :goto_24

    .line 89
    .restart local v10    # "supportCopyToAlbum":Z
    :cond_31
    const/16 v30, 0x0

    goto/16 :goto_25

    .line 90
    .restart local v30    # "supportMoveToAlbum":Z
    :cond_32
    const/16 v39, 0x0

    goto/16 :goto_26

    .line 91
    .restart local v39    # "supportSendtoOtherDevices":Z
    :cond_33
    const/16 v23, 0x0

    goto/16 :goto_27

    .line 94
    .restart local v23    # "supportImport":Z
    :cond_34
    const/16 v45, 0x0

    goto/16 :goto_28

    .line 95
    :cond_35
    const/16 v45, 0x0

    goto/16 :goto_29

    .line 97
    :cond_36
    const/16 v45, 0x0

    goto/16 :goto_2a

    .line 104
    :cond_37
    const/16 v45, 0x0

    goto/16 :goto_2b

    .line 119
    :cond_38
    const v45, 0x7f0f02a3

    move-object/from16 v0, p0

    move/from16 v1, v45

    invoke-static {v0, v1, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2c

    .line 125
    :cond_39
    const v45, 0x7f0f02b4

    const/16 v46, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v46

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 126
    const v45, 0x7f0f02b3

    move-object/from16 v0, p0

    move/from16 v1, v45

    move/from16 v2, v28

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2d

    .line 169
    :cond_3a
    const/16 v45, 0x0

    goto/16 :goto_2e
.end method
