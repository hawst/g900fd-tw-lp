.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupFilterView(Landroid/content/Context;Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "paramView"    # Landroid/view/View;
    .param p2, "paramInt"    # I
    .param p3, "paramKeyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v9, 0x2

    const/4 v5, 0x0

    const/4 v8, 0x3

    const/4 v4, 0x1

    .line 404
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 405
    .local v1, "keyCode":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->val$context:Landroid/content/Context;

    const-string v7, "SLIDE_SHOW_FILTER_PREFS"

    invoke-virtual {v6, v7, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 407
    .local v2, "preferences":Landroid/content/SharedPreferences;
    const-string v6, "FILTER"

    invoke-interface {v2, v6, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 408
    .local v0, "filter":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v6}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40800000    # 4.0f

    div-float v3, v6, v7

    .line 409
    .local v3, "step":F
    packed-switch v1, :pswitch_data_0

    move v4, v5

    .line 462
    :cond_0
    :goto_0
    return v4

    .line 411
    :pswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-eq v5, v4, :cond_2

    .line 412
    if-eqz v0, :cond_1

    if-ne v0, v8, :cond_0

    .line 413
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    int-to-float v6, v0

    mul-float/2addr v6, v3

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Landroid/widget/HorizontalScrollView;->setScrollX(I)V

    goto :goto_0

    .line 418
    :cond_2
    if-ltz v0, :cond_0

    if-eq v0, v4, :cond_0

    .line 419
    if-ne v0, v8, :cond_4

    .line 420
    const/4 v0, 0x2

    .line 427
    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)[Landroid/widget/ImageView;

    move-result-object v6

    aget-object v6, v6, v0

    invoke-interface {v5, v6}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 428
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v6}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v6

    float-to-int v7, v3

    sub-int/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/HorizontalScrollView;->setScrollX(I)V

    .line 429
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 430
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 431
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 432
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    goto :goto_0

    .line 421
    :cond_4
    if-ne v0, v9, :cond_5

    .line 422
    const/4 v0, 0x0

    goto :goto_1

    .line 423
    :cond_5
    if-nez v0, :cond_3

    .line 424
    const/4 v0, 0x1

    goto :goto_1

    .line 436
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v5

    if-eq v5, v4, :cond_6

    .line 437
    if-eqz v0, :cond_0

    .line 438
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    int-to-float v6, v0

    mul-float/2addr v6, v3

    float-to-int v6, v6

    invoke-virtual {v5, v6}, Landroid/widget/HorizontalScrollView;->setScrollX(I)V

    goto :goto_0

    .line 442
    :cond_6
    if-ge v0, v8, :cond_0

    .line 443
    if-nez v0, :cond_8

    .line 444
    const/4 v0, 0x2

    .line 451
    :cond_7
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterViews:[Landroid/widget/ImageView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)[Landroid/widget/ImageView;

    move-result-object v6

    aget-object v6, v6, v0

    invoke-interface {v5, v6}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 452
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_view:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v6}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v6

    float-to-int v7, v3

    add-int/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/widget/HorizontalScrollView;->setScrollX(I)V

    .line 453
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 454
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 455
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    .line 456
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->invalidate()V

    goto/16 :goto_0

    .line 445
    :cond_8
    if-ne v0, v4, :cond_9

    .line 446
    const/4 v0, 0x0

    goto :goto_2

    .line 447
    :cond_9
    if-ne v0, v9, :cond_7

    .line 448
    const/4 v0, 0x3

    goto :goto_2

    .line 409
    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
