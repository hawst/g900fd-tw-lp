.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;
.super Ljava/lang/Object;
.source "TimeViewActionBarForMultiPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;I)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    iput p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 12

    .prologue
    const v11, 0x7f0f0024

    const/4 v10, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 34
    const/4 v2, 0x0

    .line 35
    .local v2, "numberOfSelectedItems":Ljava/lang/String;
    const/4 v0, 0x0

    .line 36
    .local v0, "contentDescription":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    iput v8, v5, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mCount:I

    .line 37
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 38
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 40
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 41
    .local v4, "res":Landroid/content/res/Resources;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->getMaxCount()I

    move-result v1

    .line 42
    .local v1, "maxCount":I
    if-lez v1, :cond_4

    .line 43
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWQHD(Landroid/content/Context;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v5

    if-nez v5, :cond_1

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v5, :cond_3

    :cond_1
    const v3, 0x7f0e0051

    .line 45
    .local v3, "numberOfSelectedItemsStringID":I
    :goto_0
    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v8, v10, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 46
    const v5, 0x7f0e0052

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v8, v10, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v6

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 48
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionBar:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v5

    if-eqz v5, :cond_2

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisplaySelectAllButtonInLimitationPickMode:Z

    if-nez v5, :cond_2

    .line 49
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v5

    const/16 v8, 0x8

    invoke-virtual {v5, v8}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(I)V

    .line 59
    .end local v3    # "numberOfSelectedItemsStringID":I
    :cond_2
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 60
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v5, v8, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;ILjava/lang/String;)V

    .line 61
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->setSelectAllButtonTTS(Ljava/lang/String;)V
    invoke-static {v5, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;Ljava/lang/String;)V

    .line 62
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v8

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$900(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Z

    move-result v5

    if-eqz v5, :cond_6

    iget v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-lez v5, :cond_6

    move v5, v6

    :goto_2
    invoke-static {v8, v11, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 63
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mMenu:Landroid/view/Menu;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Landroid/view/Menu;

    move-result-object v5

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->mIsDoneEnabled:Z
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->access$900(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;)Z

    move-result v8

    if-eqz v8, :cond_7

    iget v8, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-lez v8, :cond_7

    :goto_3
    invoke-static {v5, v11, v6}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemEnable(Landroid/view/Menu;IZ)V

    .line 64
    return-void

    .line 43
    :cond_3
    const v3, 0x7f0e0050

    goto/16 :goto_0

    .line 50
    :cond_4
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-ltz v5, :cond_2

    .line 51
    iget v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    if-ne v5, v6, :cond_5

    .line 52
    const v5, 0x7f0e004f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v8, v6, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 56
    :goto_4
    move-object v0, v2

    goto :goto_1

    .line 54
    :cond_5
    const v5, 0x7f0e004e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-array v8, v6, [Ljava/lang/Object;

    iget v9, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick$1;->val$numberOfItemsSelected:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v7

    invoke-static {v5, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    :cond_6
    move v5, v7

    .line 62
    goto :goto_2

    :cond_7
    move v6, v7

    .line 63
    goto :goto_3
.end method
