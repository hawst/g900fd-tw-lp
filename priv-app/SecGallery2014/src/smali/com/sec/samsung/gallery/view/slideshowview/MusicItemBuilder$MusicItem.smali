.class public Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
.super Ljava/lang/Object;
.source "MusicItemBuilder.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MusicItem"
.end annotation


# instance fields
.field private mTitle:Ljava/lang/String;

.field private mType:I

.field private mUri:Landroid/net/Uri;


# direct methods
.method private constructor <init>(ILjava/lang/String;Landroid/net/Uri;)V
    .locals 0
    .param p1, "type"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "uri"    # Landroid/net/Uri;

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mType:I

    .line 131
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mTitle:Ljava/lang/String;

    .line 132
    iput-object p3, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mUri:Landroid/net/Uri;

    .line 133
    return-void
.end method

.method synthetic constructor <init>(ILjava/lang/String;Landroid/net/Uri;Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Landroid/net/Uri;
    .param p4, "x3"    # Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$1;

    .prologue
    .line 121
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;-><init>(ILjava/lang/String;Landroid/net/Uri;)V

    return-void
.end method


# virtual methods
.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method public getUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method public isAddMusic()Z
    .locals 2

    .prologue
    .line 144
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mType:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMusic()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 140
    iget v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mType:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNone()Z
    .locals 1

    .prologue
    .line 136
    iget v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->mType:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
