.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;
.super Ljava/lang/Object;
.source "GallerySearchFilterGroupViewPager.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 0

    .prologue
    .line 664
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public setFilterClick(Landroid/view/View;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 668
    invoke-virtual {p1}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->addSelectedItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 673
    :goto_0
    return-void

    .line 671
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->removeSelectedItem(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    goto :goto_0
.end method
