.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
.super Ljava/lang/Object;
.source "GallerySearchTagData.java"


# static fields
.field public static final SEARCH_FILTER_HANDWRITING:Ljava/lang/String; = "handwriting"

.field public static final TAG_TYPE_CATEGORY:I = 0x5

.field public static final TAG_TYPE_EVENT:I = 0x2

.field public static final TAG_TYPE_FACE:I = 0x1

.field public static final TAG_TYPE_FLAG_CATEGORY:I = 0x20

.field public static final TAG_TYPE_FLAG_EVENT:I = 0x1

.field public static final TAG_TYPE_FLAG_FACE:I = 0x2

.field public static final TAG_TYPE_FLAG_LOCATION:I = 0x4

.field public static final TAG_TYPE_FLAG_TIME:I = 0x10

.field public static final TAG_TYPE_FLAG_USERDEF:I = 0x8

.field public static final TAG_TYPE_LOCATION:I = 0x0

.field public static final TAG_TYPE_TIME:I = 0x4

.field public static final TAG_TYPE_USERDEF:I = 0x3


# instance fields
.field private mCategoryType:I

.field private mContext:Landroid/content/Context;

.field private mRawTagData:Ljava/lang/String;

.field private mSearchFilter:Ljava/lang/String;

.field private mSupportTimeTag:Z

.field private mTagFilterType:Ljava/lang/String;

.field private mTagTypeFlags:I

.field private mTextTTS:Ljava/lang/String;

.field private mTextTagData:Ljava/lang/String;

.field private mTextTagDataTranslation:Ljava/lang/String;

.field private mTtsStringId:I

.field private misEnabled:Z

.field private misSelected:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagData"    # Ljava/lang/String;
    .param p3, "tagType"    # Ljava/lang/String;
    .param p4, "categoryType"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 41
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTTS:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSearchFilter:Ljava/lang/String;

    .line 47
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misSelected:Z

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSupportTimeTag:Z

    .line 50
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mCategoryType:I

    .line 52
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTtsStringId:I

    .line 78
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 79
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 80
    iput p4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mCategoryType:I

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 82
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-nez v0, :cond_0

    .line 83
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 85
    :cond_0
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->AccumulateTagType(Ljava/lang/String;)V

    .line 86
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_TTS_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_TTS_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p2}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTtsStringId:I

    .line 89
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 90
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagData"    # Ljava/lang/String;
    .param p3, "tagType"    # Ljava/lang/String;
    .param p4, "rawTagData"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 41
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTTS:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSearchFilter:Ljava/lang/String;

    .line 47
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misSelected:Z

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSupportTimeTag:Z

    .line 50
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mCategoryType:I

    .line 52
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTtsStringId:I

    .line 55
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 57
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 59
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 61
    :cond_0
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->AccumulateTagType(Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tagData"    # Ljava/lang/String;
    .param p3, "tagType"    # Ljava/lang/String;
    .param p4, "rawTagData"    # Ljava/lang/String;
    .param p5, "textTTS"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 41
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    .line 42
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 43
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 44
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTTS:Ljava/lang/String;

    .line 45
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    .line 46
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSearchFilter:Ljava/lang/String;

    .line 47
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misSelected:Z

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 49
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSupportTimeTag:Z

    .line 50
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 51
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mCategoryType:I

    .line 52
    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTtsStringId:I

    .line 66
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    .line 67
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    .line 68
    iput-object p4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTTS:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 71
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 73
    :cond_0
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->AccumulateTagType(Ljava/lang/String;)V

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mContext:Landroid/content/Context;

    invoke-static {p2, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getName(Ljava/lang/String;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    .line 75
    return-void
.end method


# virtual methods
.method public AccumulateTagType(Ljava/lang/String;)V
    .locals 3
    .param p1, "tagType"    # Ljava/lang/String;

    .prologue
    .line 112
    if-eqz p1, :cond_0

    .line 114
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 115
    .local v1, "tagTypeIndex":I
    packed-switch v1, :pswitch_data_0

    .line 160
    .end local v1    # "tagTypeIndex":I
    :cond_0
    :goto_0
    return-void

    .line 118
    .restart local v1    # "tagTypeIndex":I
    :pswitch_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 119
    const-string v2, "Location"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    .end local v1    # "tagTypeIndex":I
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_0

    .line 124
    .end local v0    # "e":Ljava/lang/NumberFormatException;
    .restart local v1    # "tagTypeIndex":I
    :pswitch_1
    :try_start_1
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 125
    const-string v2, "People"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    goto :goto_0

    .line 130
    :pswitch_2
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 131
    const-string v2, "Event"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    goto :goto_0

    .line 136
    :pswitch_3
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 137
    const-string v2, "UserTag"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    goto :goto_0

    .line 142
    :pswitch_4
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x10

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 143
    const-string v2, "Time"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    goto :goto_0

    .line 148
    :pswitch_5
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    or-int/lit8 v2, v2, 0x20

    iput v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    .line 149
    const-string v2, "UserTag"

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 115
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public getCategoryType()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mCategoryType:I

    return v0
.end method

.method public getRawTagData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mRawTagData:Ljava/lang/String;

    return-object v0
.end method

.method public getSearchFilter()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSearchFilter:Ljava/lang/String;

    return-object v0
.end method

.method public getTagFilterType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagFilterType:Ljava/lang/String;

    return-object v0
.end method

.method public getTagTypeFlags()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    return v0
.end method

.method public getTextTagData()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagData:Ljava/lang/String;

    return-object v0
.end method

.method public getTransTagText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTagDataTranslation:Ljava/lang/String;

    return-object v0
.end method

.method public getTtsId()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTtsStringId:I

    return v0
.end method

.method public getTtsText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTextTTS:Ljava/lang/String;

    return-object v0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    return v0
.end method

.method public isEventTag()Z
    .locals 1

    .prologue
    .line 227
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLocationTag()Z
    .locals 1

    .prologue
    .line 215
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPersonTag()Z
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSelected()Z
    .locals 1

    .prologue
    .line 187
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misSelected:Z

    return v0
.end method

.method public isTimeTag()Z
    .locals 1

    .prologue
    .line 211
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x10

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUserTag()Z
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mTagTypeFlags:I

    and-int/lit8 v0, v0, 0x8

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 0
    .param p1, "isEnabled"    # Z

    .prologue
    .line 199
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misEnabled:Z

    .line 200
    return-void
.end method

.method public setSearchFilter(Ljava/lang/String;)V
    .locals 0
    .param p1, "queryFilter"    # Ljava/lang/String;

    .prologue
    .line 104
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSearchFilter:Ljava/lang/String;

    .line 105
    return-void
.end method

.method public setSelected(Z)V
    .locals 0
    .param p1, "isSelected"    # Z

    .prologue
    .line 191
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->misSelected:Z

    .line 192
    return-void
.end method

.method public setTimeTagSupport(Z)V
    .locals 0
    .param p1, "support"    # Z

    .prologue
    .line 96
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSupportTimeTag:Z

    .line 97
    return-void
.end method

.method public supportTimeTag()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->mSupportTimeTag:Z

    return v0
.end method
