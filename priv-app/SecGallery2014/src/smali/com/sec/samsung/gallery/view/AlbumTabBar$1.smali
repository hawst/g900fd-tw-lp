.class Lcom/sec/samsung/gallery/view/AlbumTabBar$1;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Landroid/app/ActionBar$OnNavigationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNavigationItemSelected(IJ)Z
    .locals 5
    .param p1, "position"    # I
    .param p2, "itemId"    # J

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 60
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$000(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 61
    if-lez p1, :cond_0

    .line 62
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e02da

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v2}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 74
    :cond_0
    :goto_0
    return v1

    .line 68
    :cond_1
    invoke-static {p1}, Lcom/sec/samsung/gallery/core/TabTagType;->from(I)Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    .line 69
    .local v0, "selectedTabTag":Lcom/sec/samsung/gallery/core/TabTagType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->isReselectedTab(Lcom/sec/samsung/gallery/core/TabTagType;)Z
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    .line 71
    goto :goto_0

    .line 73
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$1;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->handleFilterAlbums(Lcom/sec/samsung/gallery/core/TabTagType;)V
    invoke-static {v2, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$200(Lcom/sec/samsung/gallery/view/AlbumTabBar;Lcom/sec/samsung/gallery/core/TabTagType;)V

    goto :goto_0
.end method
