.class public final enum Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;
.super Ljava/lang/Enum;
.source "DMRViewControllerCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DMRCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

.field public static final enum REGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

.field public static final enum START_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

.field public static final enum STOP_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

.field public static final enum UNREGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 25
    new-instance v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    const-string v1, "START_VIEW_CONTROLLER"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->START_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    const-string v1, "STOP_VIEW_CONTROLLER"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->STOP_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    .line 26
    new-instance v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    const-string v1, "REGISTER_BROADCAST_RECEIVER"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->REGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    const-string v1, "UNREGISTER_BROADCAST_RECEIVER"

    invoke-direct {v0, v1, v5}, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->UNREGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    .line 24
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->START_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->STOP_VIEW_CONTROLLER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->REGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->UNREGISTER_BROADCAST_RECEIVER:Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/DMRViewControllerCmd$DMRCmdType;

    return-object v0
.end method
