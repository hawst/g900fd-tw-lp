.class Lcom/sec/samsung/gallery/view/allview/AllViewState$2;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "AllViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 188
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v2

    .line 189
    .local v2, "notiName":Ljava/lang/String;
    const-string v6, "VIEW_BY_TYPE_UPDATED"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 190
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleFilter()V

    .line 191
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    .line 192
    .local v0, "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    instance-of v6, v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    if-eqz v6, :cond_0

    .line 193
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v1

    .line 194
    .local v1, "currentViewByType":I
    check-cast v0, Lcom/sec/samsung/gallery/view/AlbumTabBar;

    .end local v0    # "actionBar":Lcom/sec/samsung/gallery/view/AbstractActionBarView;
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeViewByType(I)V

    .line 209
    .end local v1    # "currentViewByType":I
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    const-string v6, "REFRESH_SELECTION"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 197
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleBackgroundDeletion()V
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    goto :goto_0

    .line 198
    :cond_2
    const-string v6, "SECRET_MODE_CHANGED"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 200
    const-string v6, "EXIT_SELECTION_MODE"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 201
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->exitSelectionMode()V

    goto :goto_0

    .line 202
    :cond_3
    const-string v6, "DOWNLOAD_NEARBY"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 203
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v5

    .line 204
    .local v5, "serverName":Ljava/lang/String;
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v4, v6

    check-cast v4, [Ljava/lang/Object;

    .line 206
    .local v4, "params":[Ljava/lang/Object;
    const/4 v6, 0x0

    aget-object v3, v4, v6

    check-cast v3, Ljava/util/ArrayList;

    .line 207
    .local v3, "objectList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v6

    invoke-virtual {v6, v5, v3}, Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;->download(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 176
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "VIEW_BY_TYPE_UPDATED"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SECRET_MODE_CHANGED"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "REFRESH_SELECTION"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "DOWNLOAD_NEARBY"

    aput-object v2, v0, v1

    return-object v0
.end method
