.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;
.super Ljava/lang/Object;
.source "SlideshowPresentationVideoView.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 4
    .param p1, "focusChange"    # I

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 160
    packed-switch p1, :pswitch_data_0

    .line 184
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 163
    :pswitch_1
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUDIOFOCUS_GAIN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 168
    :pswitch_2
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUDIOFOCUS_GAIN_TRANSIENT_MAY_DUCK"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 169
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 174
    :pswitch_3
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUDIOFOCUS_LOSS"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0

    .line 179
    :pswitch_4
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;->access$100(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowPresentationVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto/16 :goto_0

    .line 160
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
