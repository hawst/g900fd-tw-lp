.class Lcom/sec/samsung/gallery/view/help/HelpViewState$2;
.super Landroid/os/CountDownTimer;
.source "HelpViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

.field final synthetic val$head:Landroid/widget/ImageView;

.field final synthetic val$text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;JJLandroid/widget/TextView;Landroid/widget/ImageView;)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iput-object p6, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$text:Landroid/widget/TextView;

    iput-object p7, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$head:Landroid/widget/ImageView;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 290
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$600(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const v2, 0x7f040002

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const v2, 0x7f040003

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    .line 292
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$text:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$text:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$text:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$head:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$head:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->val$head:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    :cond_1
    return-void
.end method

.method public onTick(J)V
    .locals 0
    .param p1, "l"    # J

    .prologue
    .line 305
    return-void
.end method
