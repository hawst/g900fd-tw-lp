.class Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;
.super Ljava/lang/Object;
.source "HelpAirCommandState.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 91
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->access$400(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getPenHovering(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 93
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 94
    .local v0, "action":I
    const/16 v1, 0x9

    if-ne v1, v0, :cond_0

    .line 95
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mTap:Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 98
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHoverView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setViewAirButton()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V

    .line 102
    .end local v0    # "action":I
    :cond_0
    const/4 v1, 0x1

    return v1
.end method
