.class public Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "CameraSearchActionBar.java"


# static fields
.field private static final NAME:Ljava/lang/String; = "name"


# instance fields
.field private mCursor:Landroid/database/MatrixCursor;

.field private mOldFilterText:Ljava/lang/String;

.field private mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$1;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->initializeSearchView()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/database/MatrixCursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 18
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V

    return-void
.end method

.method private addSuggestion(Ljava/lang/String;)V
    .locals 7
    .param p1, "filterText"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 65
    new-instance v1, Landroid/database/MatrixCursor;

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v4

    const-string v3, "name"

    aput-object v3, v2, v5

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    .line 69
    new-array v0, v6, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    aput-object v1, v0, v4

    aput-object p1, v0, v5

    .line 72
    .local v0, "object":[Ljava/lang/Object;
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_ADD_SUGGESTION:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 73
    return-void
.end method

.method private initializeSearchView()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getSearchText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01fb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setQueryHint(Ljava/lang/CharSequence;)V

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$3;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setOnQueryTextListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;)V

    .line 125
    return-void
.end method

.method private setOnSuggestionListener()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setOnSuggestionListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;)V

    .line 89
    return-void
.end method

.method private setSuggestionAdapter(Ljava/lang/String;)V
    .locals 8
    .param p1, "filterText"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 51
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->addSuggestion(Ljava/lang/String;)V

    .line 53
    new-instance v0, Landroid/widget/SimpleCursorAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x1090003

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;

    new-array v4, v6, [Ljava/lang/String;

    const-string v5, "name"

    aput-object v5, v4, v7

    new-array v5, v6, [I

    const v6, 0x1020014

    aput v6, v5, v7

    const/high16 v6, -0x80000000

    invoke-direct/range {v0 .. v6}, Landroid/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    .line 59
    .local v0, "suggestionAdapter":Landroid/widget/SimpleCursorAdapter;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setSuggestionsAdapter(Landroid/widget/CursorAdapter;)V

    .line 61
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->setOnSuggestionListener()V

    .line 62
    return-void
.end method


# virtual methods
.method public onDeleteKeyword(Ljava/lang/String;)V
    .locals 3
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->getQueryText()Ljava/lang/String;

    move-result-object v0

    .line 147
    .local v0, "key":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 148
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$5;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$5;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 155
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setImeVisibility(Z)V

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mOldFilterText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setSearchText(Ljava/lang/String;)V

    .line 143
    return-void
.end method

.method protected setSuggestionAdapter()V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method protected setupButtons()V
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->showHomeButtonOnTopLeft()V

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$4;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 137
    return-void
.end method
