.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
.super Ljava/lang/Object;
.source "AlbumViewEventHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;,
        Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;
    }
.end annotation


# instance fields
.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private final mAlbumViewMultiPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;

.field private final mAlbumViewPersonPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;

.field private final mAlbumViewPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;

.field private final mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

.field private mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "albumViewState"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 26
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    .line 27
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;

    .line 28
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewMultiPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;

    .line 29
    new-instance v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;-><init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewPersonPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;

    .line 30
    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;)Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;

    .prologue
    .line 14
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewState:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    return-object v0
.end method


# virtual methods
.method disableView()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;->disableView()V

    .line 54
    return-void
.end method

.method initializeView()V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;->initializeView()V

    .line 50
    return-void
.end method

.method onBackPressed()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;->onBackPressed()V

    .line 66
    return-void
.end method

.method onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 1
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;->onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 62
    return-void
.end method

.method onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;->onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 58
    return-void
.end method

.method setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V
    .locals 2
    .param p1, "mode"    # Lcom/sec/samsung/gallery/core/LaunchModeType;

    .prologue
    .line 33
    sget-object v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$1;->$SwitchMap$com$sec$samsung$gallery$core$LaunchModeType:[I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 46
    :goto_0
    return-void

    .line 35
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    goto :goto_0

    .line 38
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewMultiPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewMultiPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    goto :goto_0

    .line 41
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mAlbumViewPersonPick:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$AlbumViewPersonPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;

    goto :goto_0

    .line 33
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
