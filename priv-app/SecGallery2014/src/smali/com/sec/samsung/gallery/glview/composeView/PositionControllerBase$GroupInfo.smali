.class public Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;
.super Ljava/lang/Object;
.source "PositionControllerBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "GroupInfo"
.end annotation


# instance fields
.field protected mCol:I

.field protected mCount:I

.field protected mCx:[F

.field protected mCy:[F

.field protected mGroupCol:I

.field protected mGroupRow:I

.field protected mItemH:[F

.field protected mItemHG:F

.field protected mItemW:[F

.field protected mItemWG:F

.field protected mLineCount:I

.field protected mMargineLeft:F

.field protected mRatio:[B

.field protected mRatioAct:[B

.field protected mRow:I

.field protected mScrlAccu:F

.field protected mScrlAmount:F

.field protected mTitleCx:F

.field protected mTitleCy:F


# direct methods
.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1199
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 1211
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 1212
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 1215
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mMargineLeft:F

    return-void
.end method


# virtual methods
.method public setItemCount(I)V
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 1218
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    if-eq v0, p1, :cond_0

    .line 1219
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    .line 1220
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCx:[F

    .line 1221
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCy:[F

    .line 1222
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemW:[F

    .line 1223
    new-array v0, p1, [F

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mItemH:[F

    .line 1225
    :cond_0
    return-void
.end method
