.class Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;
.super Ljava/lang/Object;
.source "SlideshowSelectionTask.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/MediaSet$ItemConsumer;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Void;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

.field final synthetic val$selectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->val$selectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public consume(ILcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 6
    .param p1, "index"    # I
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const-wide/16 v4, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    # getter for: Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mTaskRunning:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->access$000(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    .line 114
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 115
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->val$selectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    move v1, v2

    .line 146
    :cond_2
    :goto_0
    return v1

    .line 119
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    invoke-virtual {v3, v4, v5, v2}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->increaseProgress(JZ)V

    .line 120
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    invoke-virtual {v2, v4, v5, v1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->increaseProgress(JZ)V

    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "isFaceImage":Z
    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v2, :cond_4

    .line 124
    const/4 v0, 0x1

    .line 125
    check-cast p2, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object p2

    .line 128
    .restart local p2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_4
    instance-of v2, p2, Lcom/sec/android/gallery3d/data/ActionImage;

    if-nez v2, :cond_2

    .line 131
    if-eqz p2, :cond_2

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-nez v2, :cond_2

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v2, :cond_5

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_2

    :cond_5
    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-nez v2, :cond_2

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTCloud:Z

    if-eqz v2, :cond_6

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v2, :cond_2

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-nez v2, :cond_2

    :cond_6
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v2

    if-nez v2, :cond_2

    .line 136
    if-eqz v0, :cond_7

    .line 137
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->val$selectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addUnchek(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0

    .line 139
    :cond_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->isClusterAlbum:Z

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    if-eqz v2, :cond_8

    .line 140
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->resultItems:Ljava/util/ArrayList;

    invoke-virtual {v2, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$3;->val$selectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method
