.class public Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
.super Ljava/lang/Object;
.source "DrawerHelpView.java"


# static fields
.field public static final DRAWER_HELP_PARAM_KEY:Ljava/lang/String; = "drawer_is_show"


# instance fields
.field private mCloseBtn:Landroid/widget/ImageButton;

.field private mCloseClickListener:Landroid/view/View$OnClickListener;

.field private mCloseKeyListener:Landroid/view/View$OnKeyListener;

.field private mContext:Landroid/content/Context;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

.field private mDrawerlHelpView:Landroid/widget/RelativeLayout;

.field private mHelpDrawerPopupImage:Landroid/widget/ImageView;

.field private mIsShowingHelpView:Z

.field private mMainView:Landroid/widget/RelativeLayout;

.field private mOkBtn:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z

    .line 163
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$4;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    .line 171
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$5;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseKeyListener:Landroid/view/View$OnKeyListener;

    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0f0026

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0f0027

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/DrawerLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 45
    return-void
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    .param p1, "x1"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    return-object p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->initializeView()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->setHelpState(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/support/v4/widget/DrawerLayout;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method private initializeView()V
    .locals 5

    .prologue
    const v4, 0x7f0f00e1

    .line 117
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v3, :cond_1

    .line 118
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mOkBtn:Landroid/widget/TextView;

    .line 119
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mOkBtn:Landroid/widget/TextView;

    if-nez v3, :cond_2

    .line 161
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    .line 123
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    const v4, 0x7f0f00e2

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mHelpDrawerPopupImage:Landroid/widget/ImageView;

    .line 125
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mHelpDrawerPopupImage:Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 128
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 129
    .local v0, "config":Landroid/content/res/Configuration;
    invoke-virtual {v0}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    .line 130
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mHelpDrawerPopupImage:Landroid/widget/ImageView;

    const v4, 0x7f02006f

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 136
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_2
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    new-instance v4, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$3;

    invoke-direct {v4, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$3;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 144
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v3, :cond_4

    .line 145
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mOkBtn:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 146
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mOkBtn:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 156
    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 157
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    const v4, 0x7f0f00e3

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 158
    .local v2, "textBody":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    .line 159
    const/16 v3, 0x40

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    goto :goto_0

    .line 132
    .end local v2    # "textBody":Landroid/view/View;
    .restart local v0    # "config":Landroid/content/res/Configuration;
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mHelpDrawerPopupImage:Landroid/widget/ImageView;

    const v4, 0x7f02006e

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_1

    .line 148
    .end local v0    # "config":Landroid/content/res/Configuration;
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 149
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 151
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    .line 152
    .local v1, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v3

    iput v3, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 153
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_2
.end method

.method private isShowHelpPopup()Z
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    const-string v1, "drawer_is_show"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private setHelpState(Z)V
    .locals 2
    .param p1, "show"    # Z

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    const-string v1, "drawer_is_show"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 97
    return-void
.end method


# virtual methods
.method public addView()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->isShowHelpPopup()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 82
    :cond_0
    return-void
.end method

.method public bringToFront()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->bringChildToFront(Landroid/view/View;)V

    goto :goto_0
.end method

.method public isShowingHelpView()Z
    .locals 1

    .prologue
    .line 203
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z

    return v0
.end method

.method public removeView()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_1

    .line 114
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$2;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    .line 200
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$6;-><init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
