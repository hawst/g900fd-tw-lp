.class Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;
.super Landroid/widget/TextView;
.source "ComposePhotoViewAlbumSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "NewAlbumTextView"
.end annotation


# static fields
.field private static final MAX_LINE:I = 0x2


# instance fields
.field private mBottomPadding:I

.field private mPadding:I

.field private mTextColor:I

.field private mTextSize:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->this$0:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    .line 339
    invoke-direct {p0, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 340
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumFontSize()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mTextSize:F

    .line 341
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumFontColor()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mTextColor:I

    .line 342
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTextViewBottomPadding()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mBottomPadding:I

    .line 343
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumTextViewPadding()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mPadding:I

    .line 344
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v7, 0x0

    .line 348
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->this$0:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumThumbnailHeightPixel(I)I

    move-result v3

    .line 349
    .local v3, "textboxH":I
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDraw, textboxH="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, before first super.onDraw"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 352
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 353
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, after first super.onDraw"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mTextSize:F

    .line 357
    .local v2, "textSize":F
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getLineCount()I

    move-result v4

    const/4 v5, 0x2

    if-gt v4, v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getLineCount()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getLineHeight()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getHeight()I

    move-result v5

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mBottomPadding:I

    sub-int/2addr v5, v6

    if-le v4, v5, :cond_1

    .line 358
    :cond_0
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v2, v4

    invoke-virtual {p0, v7, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->setTextSize(IF)V

    .line 359
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDraw, before super.onDraw() in while loop, textSize="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 361
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, after super.onDraw() in while loop"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 365
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getLineCount()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->getLineHeight()I

    move-result v5

    mul-int/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mBottomPadding:I

    add-int v1, v4, v5

    .line 366
    .local v1, "textHeight":I
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDraw, before if condition: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ">"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 367
    if-le v3, v1, :cond_2

    .line 368
    sub-int v4, v3, v1

    shr-int/lit8 v0, v4, 0x1

    .line 369
    .local v0, "padding":I
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "padding="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onDraw, before setPadding, mPadding="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mPadding:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", padding="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mPadding:I

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mPadding:I

    invoke-virtual {p0, v4, v0, v5, v0}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->setPadding(IIII)V

    .line 372
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, after setPadding"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    .end local v0    # "padding":I
    :cond_2
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, after if condition"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_3

    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v4, :cond_4

    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->this$0:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v4, :cond_6

    .line 377
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->mTextColor:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->setTextColor(I)V

    .line 382
    :goto_1
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragHereTextColorForWQHD:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->this$0:Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_5

    .line 383
    const v4, -0x3f0a0a0b

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->setTextColor(I)V

    .line 386
    :cond_5
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, before last super.onDraw"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 388
    const-string v4, "ComposePhotoViewAlbumSetAdapter"

    const-string v5, "onDraw, after last super.onDraw"

    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/util/Logger;->log(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    return-void

    .line 379
    :cond_6
    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter$NewAlbumTextView;->setTextColor(I)V

    goto :goto_1
.end method
