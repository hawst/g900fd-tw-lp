.class Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;
.super Ljava/lang/Object;
.source "ShowEditBurstShotDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->showSubOptions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 43
    packed-switch p2, :pswitch_data_0

    .line 55
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 56
    return-void

    .line 45
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v2, v2, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->burstShotList:Ljava/util/ArrayList;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startPicMovie(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$100(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 48
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v2, v2, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->burstShotList:Ljava/util/ArrayList;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startBestPhotos(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$200(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 51
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;

    iget-object v2, v2, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->burstShotList:Ljava/util/ArrayList;

    # invokes: Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->startClear(Landroid/content/Context;Ljava/util/ArrayList;)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;->access$300(Lcom/sec/samsung/gallery/controller/ShowEditBurstShotDialogCmd;Landroid/content/Context;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
