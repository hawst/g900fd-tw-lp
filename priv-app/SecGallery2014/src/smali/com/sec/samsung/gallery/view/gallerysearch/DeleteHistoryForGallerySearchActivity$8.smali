.class Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;
.super Ljava/lang/Object;
.source "DeleteHistoryForGallerySearchActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->addSelectAllHeader()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setCheckedAll(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Z)V

    .line 265
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->updateSelectAll()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)V

    .line 266
    return-void

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 263
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity$8;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->setCheckedAll(Z)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/DeleteHistoryForGallerySearchActivity;Z)V

    goto :goto_0
.end method
