.class public interface abstract Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
.super Ljava/lang/Object;
.source "DetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/PhotoView$Model;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Model"
.end annotation


# virtual methods
.method public abstract changeSource(Lcom/sec/android/gallery3d/data/MediaSet;)V
.end method

.method public abstract destroy()V
.end method

.method public abstract getCurrentIndex()I
.end method

.method public abstract getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;
.end method

.method public abstract isEmpty()Z
.end method

.method public abstract onAgifPlayRequests()V
.end method

.method public abstract pause()V
.end method

.method public abstract resume()V
.end method

.method public abstract setCurrentPhoto(Lcom/sec/android/gallery3d/data/Path;I)V
.end method

.method public abstract updateBrokenImage(Landroid/graphics/Bitmap;)V
.end method
