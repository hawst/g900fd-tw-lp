.class Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;
.super Landroid/os/CountDownTimer;
.source "HelpInfoPreviewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;JJ)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # J

    .prologue
    .line 263
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/os/CountDownTimer;-><init>(JJ)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 267
    return-void
.end method

.method public onTick(J)V
    .locals 2
    .param p1, "l"    # J

    .prologue
    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->IsShowCompletePopup:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$800(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->IsShowCompletePopup:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->access$802(Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;Z)Z

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    const/16 v1, 0xa

    iput v1, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->mViewMode:I

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3$1;->this$1:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpInfoPreviewState;->setHelpView()V

    .line 276
    :cond_0
    return-void
.end method
