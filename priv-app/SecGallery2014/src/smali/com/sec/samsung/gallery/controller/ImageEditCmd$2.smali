.class Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;
.super Landroid/os/Handler;
.source "ImageEditCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ImageEditCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    .line 181
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 198
    :cond_0
    :goto_0
    return-void

    .line 183
    :pswitch_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->mEditAppChoiceDialog:Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/common/AppChoiceDialog;->isShowing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 186
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->createAppList()Ljava/util/ArrayList;

    move-result-object v0

    .line 187
    .local v0, "shareAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;>;"
    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-le v1, v2, :cond_2

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ImageEditCmd;->showAppChoiceDialog(Ljava/util/ArrayList;)V
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->access$000(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Ljava/util/ArrayList;)V

    goto :goto_0

    .line 190
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;

    # invokes: Lcom/sec/samsung/gallery/controller/ImageEditCmd;->startShareApp(Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->access$100(Lcom/sec/samsung/gallery/controller/ImageEditCmd;Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;)V

    goto :goto_0

    .line 195
    .end local v0    # "shareAppList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/controller/ImageEditCmd$EditPackageInfo;>;"
    :pswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ImageEditCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/ImageEditCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/ImageEditCmd;->dismissDialog()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ImageEditCmd;->access$200(Lcom/sec/samsung/gallery/controller/ImageEditCmd;)V

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
