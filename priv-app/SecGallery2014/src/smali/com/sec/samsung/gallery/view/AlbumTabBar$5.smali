.class Lcom/sec/samsung/gallery/view/AlbumTabBar$5;
.super Ljava/lang/Object;
.source "AlbumTabBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar;->showTabs()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V
    .locals 0

    .prologue
    .line 326
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const v9, 0x7f0e00a4

    .line 329
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->updateCurrentSelectedTabType()V
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$700(Lcom/sec/samsung/gallery/view/AlbumTabBar;)V

    .line 331
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->tabAlreadyAdded()Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$800(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 368
    :goto_0
    return-void

    .line 335
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsTablet:Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$300(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 336
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentViewByType()I

    move-result v5

    .line 337
    .local v5, "type":I
    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-nez v6, :cond_1

    sget-object v6, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v6, v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 338
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 339
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mArrayAdapter:Landroid/widget/ArrayAdapter;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$400(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/widget/ArrayAdapter;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mOnNavigationListener:Landroid/app/ActionBar$OnNavigationListener;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$900(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Landroid/app/ActionBar$OnNavigationListener;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/app/ActionBar;->setListNavigationCallbacks(Landroid/widget/SpinnerAdapter;Landroid/app/ActionBar$OnNavigationListener;)V

    .line 340
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # getter for: Lcom/sec/samsung/gallery/view/AlbumTabBar;->mIsHelpMode:Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$000(Lcom/sec/samsung/gallery/view/AlbumTabBar;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 341
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/core/TabTagType;->getIndex()I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    .line 342
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 343
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v9}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    .line 344
    .local v0, "albumTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v0, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    goto :goto_0

    .line 347
    .end local v0    # "albumTab":Landroid/app/ActionBar$Tab;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/samsung/gallery/core/TabTagType;->from(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v7

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto/16 :goto_0

    .line 350
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/ActionBar;->setNavigationMode(I)V

    goto/16 :goto_0

    .line 355
    .end local v5    # "type":I
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v6}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 357
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v9}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v0

    .line 358
    .restart local v0    # "albumTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const v7, 0x7f0e00a5

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v1

    .line 359
    .local v1, "allTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const v7, 0x7f0e0073

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v4

    .line 360
    .local v4, "timelineTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const v7, 0x7f0e0074

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_LOCATION:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v2

    .line 361
    .local v2, "locationTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    const v7, 0x7f0e00a7

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->createTab(I)Landroid/app/ActionBar$Tab;
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1000(Lcom/sec/samsung/gallery/view/AlbumTabBar;I)Landroid/app/ActionBar$Tab;

    move-result-object v6

    sget-object v7, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v6, v7}, Landroid/app/ActionBar$Tab;->setTag(Ljava/lang/Object;)Landroid/app/ActionBar$Tab;

    move-result-object v3

    .line 363
    .local v3, "personTab":Landroid/app/ActionBar$Tab;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v0}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v0, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 364
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v1}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v1, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 365
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v4}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v4, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 366
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v2}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v2, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    .line 367
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v6, v6, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$5;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    # invokes: Lcom/sec/samsung/gallery/view/AlbumTabBar;->checkSelected(Landroid/app/ActionBar$Tab;)Z
    invoke-static {v7, v3}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->access$1100(Lcom/sec/samsung/gallery/view/AlbumTabBar;Landroid/app/ActionBar$Tab;)Z

    move-result v7

    invoke-virtual {v6, v3, v7}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;Z)V

    goto/16 :goto_0
.end method
