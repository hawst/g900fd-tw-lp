.class public abstract Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;
.super Landroid/os/AsyncTask;
.source "MediaCountCheckTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaSet;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mContext:Landroid/content/Context;

    .line 18
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "arg0"    # [Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    const/4 v3, 0x0

    .line 29
    aget-object v0, p1, v3

    .line 30
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_2

    .line 31
    sget-object v2, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v2

    .line 32
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->isNotEmpty(Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 33
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :goto_0
    monitor-exit v2

    .line 39
    :goto_1
    return-object v1

    .line 33
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 35
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 37
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1

    .line 35
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 39
    :cond_2
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_1
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 11
    check-cast p1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->doInBackground([Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected isNotEmpty(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 43
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 44
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCancelled()V
    .locals 1

    .prologue
    .line 49
    invoke-super {p0}, Landroid/os/AsyncTask;->onCancelled()V

    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 51
    return-void
.end method

.method protected abstract onMediaNotEmpty()V
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 58
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->onMediaNotEmpty()V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 11
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 22
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 23
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 24
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0032

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 25
    return-void
.end method
