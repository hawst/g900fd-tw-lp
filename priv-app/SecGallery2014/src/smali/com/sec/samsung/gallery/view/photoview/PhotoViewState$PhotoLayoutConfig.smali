.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.source "PhotoViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PhotoLayoutConfig"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 777
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;-><init>()V

    .line 778
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUsePenSelectInPickMode:Z

    .line 779
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseEnlargeAnim:Z

    .line 780
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseLayoutChange:Z

    .line 781
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseGroupSelect:Z

    .line 782
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseGroupTitle:Z

    .line 783
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseItemSelect:Z

    .line 785
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mAlbumList:Z

    .line 786
    sget v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_LAND:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mLandRatio:F

    .line 787
    sget v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->SPLIT_RATIO_PORT:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mPortRatio:F

    .line 788
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mItemSizeScale:F

    .line 791
    iput v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mInitialLevel:I

    .line 792
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMinLevel:I

    .line 793
    iput v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMaxLevel:I

    .line 794
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mTopGroupTitle:Z

    .line 795
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mUseQuickScroll:Z

    .line 796
    iget-object v0, p1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    .line 797
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v1, v0, v2

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-class v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v2, v0, v1

    const-class v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v1, v0, v4

    const/4 v1, 0x4

    const-class v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mPosCtrl:[Ljava/lang/Object;

    .line 801
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;
    .param p1, "x1"    # I

    .prologue
    .line 775
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->setModeLevel(I)V

    return-void
.end method

.method private setModeLevel(I)V
    .locals 3
    .param p1, "initLevel"    # I

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x3

    .line 804
    if-ne p1, v2, :cond_1

    .line 805
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mComponent:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsNoSplitMode:Z

    if-eqz v0, :cond_0

    .line 806
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mInitialLevel:I

    .line 807
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMinLevel:I

    .line 808
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMaxLevel:I

    .line 819
    :goto_0
    return-void

    .line 810
    :cond_0
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mInitialLevel:I

    .line 811
    iput v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMinLevel:I

    .line 812
    iput v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMaxLevel:I

    goto :goto_0

    .line 815
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mInitialLevel:I

    .line 816
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMinLevel:I

    .line 817
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c007b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState$PhotoLayoutConfig;->mMaxLevel:I

    goto :goto_0
.end method
