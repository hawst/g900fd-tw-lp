.class Lcom/sec/samsung/gallery/view/allview/AllViewState$11;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V
    .locals 5
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I

    .prologue
    const/4 v3, 0x1

    .line 455
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # setter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I
    invoke-static {v2, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2402(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)I

    .line 457
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v2, :cond_0

    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 459
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-nez v2, :cond_2

    .line 460
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->enterSelectionMode(Z)V
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2900(Lcom/sec/samsung/gallery/view/allview/AllViewState;Z)V

    .line 462
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 463
    .local v1, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v1, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 464
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectMedia(I)V

    .line 473
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-void

    .line 466
    .restart local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_3
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$3000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;-><init>(Landroid/content/Context;)V

    .line 467
    .local v0, "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v0, p3, v2, v3, v4}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->startPhotosDrag(ILcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_0

    .line 470
    .end local v0    # "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    .end local v1    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # invokes: Lcom/sec/samsung/gallery/view/allview/AllViewState;->enterSelectionMode(Z)V
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2900(Lcom/sec/samsung/gallery/view/allview/AllViewState;Z)V

    .line 471
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v2, p3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectMedia(I)V

    goto :goto_0
.end method
