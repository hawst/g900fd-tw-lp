.class Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;
.super Ljava/lang/Object;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EstimationInfo"
.end annotation


# instance fields
.field firstX:F

.field firstY:F

.field firstZ:F

.field gapH:F

.field gapW:F

.field hCount:I

.field objH:F

.field objW:F

.field scroll:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 608
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/samsung/gallery/glview/GlGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlGridView$1;

    .prologue
    .line 608
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V

    return-void
.end method


# virtual methods
.method public getObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 619
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlGridView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->objW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->objH:F

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 621
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->hCount:I

    rem-int v0, p1, v3

    .line 622
    .local v0, "col":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->hCount:I

    div-int v2, p1, v3

    .line 623
    .local v2, "row":I
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->gapW:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstY:F

    int-to-float v5, v2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->gapH:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->scroll:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstZ:F

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 624
    return-object v1
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->objW:F

    .line 629
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->objH:F

    .line 630
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->gapW:F

    .line 631
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->gapH:F

    .line 632
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstX:F

    .line 633
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstY:F

    .line 634
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemStartZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->firstZ:F

    .line 635
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mItemHVisibleCnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->hCount:I

    .line 636
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView;->mAlbumScroller:Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlGridView$ItemSetScroller;->mScroll:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$EstimationInfo;->scroll:F

    .line 637
    return-void
.end method
