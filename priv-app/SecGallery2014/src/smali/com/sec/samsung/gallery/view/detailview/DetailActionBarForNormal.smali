.class public Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;
.super Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;
.source "DetailActionBarForNormal.java"


# static fields
.field public static final CONTEXTUAL_URI:Landroid/net/Uri;

.field public static final NO_WEATHER_EDITABLE:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field public static final WEATHER_ADDABLE:I = 0x1

.field public static final WEATHER_EDITABLE:I = 0x2

.field public static final WEATHER_ID:Ljava/lang/String; = "weather_ID"


# instance fields
.field private mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

.field private final mCurrentfileUri:Ljava/lang/String;

.field private final mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private final mFromSetupWidzard:Z

.field private mHander:Landroid/os/Handler;

.field private mIsActive:Z

.field private mIsBurstShot:Z

.field private mIsKnox:Z

.field private mIsKnox2Enabled:Z

.field private mIsKnoxInstallMode:Z

.field private mIsUpAvailable:Z

.field private mMemoId:I

.field private mMenu:Landroid/view/Menu;

.field private mOCRDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private final mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    .line 97
    const-string v0, "content://media/external/contextural_tags"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->CONTEXTUAL_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V
    .locals 5
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "upAvailable"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 107
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 88
    iput v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMemoId:I

    .line 90
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentfileUri:Ljava/lang/String;

    .line 91
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 92
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    .line 98
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsBurstShot:Z

    .line 99
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mHander:Landroid/os/Handler;

    .line 100
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnoxInstallMode:Z

    .line 101
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox:Z

    .line 102
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox2Enabled:Z

    .line 103
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsUpAvailable:Z

    .line 104
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsActive:Z

    .line 108
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 109
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->isLaunchFromSetupWidzard()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mFromSetupWidzard:Z

    .line 111
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v3}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 122
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsUpAvailable:Z

    .line 123
    invoke-virtual {p0, p2, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setDisplayOptions(ZZ)V

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnoxInstallMode:Z

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox:Z

    .line 128
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKnoxEnabledStateOfKnox2(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox2Enabled:Z

    .line 130
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->refreshRotationIcon()V

    .line 133
    :cond_0
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsActive:Z

    .line 134
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Landroid/view/Menu;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    .prologue
    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private drawChangePlayerIcon()V
    .locals 3

    .prologue
    .line 1073
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    if-nez v1, :cond_0

    .line 1078
    :goto_0
    return-void

    .line 1076
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0299

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1077
    .local v0, "menuItem":Landroid/view/MenuItem;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->drawChangePlayerIcon(Landroid/view/MenuItem;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    goto :goto_0
.end method

.method private getContextualWeatherEditableStatus()I
    .locals 9

    .prologue
    .line 915
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->CONTEXTUAL_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v2

    int-to-long v2, v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 916
    .local v1, "ImageURI":Landroid/net/Uri;
    const/4 v6, 0x0

    .line 917
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 918
    .local v8, "weatherTag":I
    const/4 v7, 0x0

    .line 920
    .local v7, "status":I
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 922
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 923
    const-string/jumbo v0, "weather_ID"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 926
    :cond_0
    if-nez v8, :cond_1

    .line 927
    const/4 v7, 0x0

    .line 935
    :goto_0
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    move v0, v7

    .line 938
    :goto_1
    return v0

    .line 929
    :cond_1
    const/4 v7, 0x2

    goto :goto_0

    .line 932
    :catch_0
    move-exception v0

    .line 935
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 938
    const/4 v0, 0x0

    goto :goto_1

    .line 935
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method private getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 496
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_0

    .line 497
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 498
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 499
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 502
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getMenusForWeTogether()J
    .locals 2

    .prologue
    .line 1149
    const-wide/32 v0, 0x10000020

    return-wide v0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 1081
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1082
    const v0, 0x7f120020

    .line 1093
    :goto_0
    return v0

    .line 1083
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1084
    const v0, 0x7f120014

    goto :goto_0

    .line 1085
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1086
    const v0, 0x7f12001b

    goto :goto_0

    .line 1087
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1088
    const v0, 0x7f120023

    goto :goto_0

    .line 1090
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1091
    const v0, 0x7f120016

    goto :goto_0

    .line 1093
    :cond_4
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReodereredMenuForWQHD:Z

    if-eqz v0, :cond_5

    const v0, 0x7f120019

    goto :goto_0

    :cond_5
    const v0, 0x7f120018

    goto :goto_0
.end method

.method private getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 1144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1145
    .local v0, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0
.end method

.method private handleGetText()V
    .locals 10

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v0}, Lcom/dmc/ocr/OcrUtils;->isSupportedFormat(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v6

    .line 1106
    .local v6, "isSupportedFormat":Z
    if-nez v6, :cond_1

    .line 1107
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9}, Landroid/content/Intent;-><init>()V

    .line 1108
    .local v9, "requetIntent":Landroid/content/Intent;
    const-string v0, "gettext_result"

    const/4 v1, 0x3

    invoke-virtual {v9, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1109
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->showOCRFailedDialog(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Landroid/content/Intent;)V

    .line 1116
    .end local v9    # "requetIntent":Landroid/content/Intent;
    :goto_0
    if-eqz v6, :cond_0

    .line 1117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mOCRDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e0247

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e024a

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 1121
    :cond_0
    return-void

    .line 1111
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaType(Lcom/sec/android/gallery3d/data/Path;)I

    move-result v7

    .line 1112
    .local v7, "mediaType":I
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->getMimeType(I)Ljava/lang/String;

    move-result-object v8

    .line 1113
    .local v8, "mimeType":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewOCR;->startOCRActivity(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private final isAnyAppSupportEdit()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 1180
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePSTouch:Z

    if-eqz v2, :cond_1

    .line 1181
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->getPSTouchInfo(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 1182
    .local v0, "info":Landroid/content/pm/ResolveInfo;
    if-eqz v0, :cond_3

    .line 1199
    .end local v0    # "info":Landroid/content/pm/ResolveInfo;
    :cond_0
    :goto_0
    return v1

    .line 1186
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isSamsungAppsAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isPhotoRetouchingAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1187
    :cond_2
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditorAtShareList:Z

    if-nez v2, :cond_0

    .line 1193
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMimeType:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isPaperArtistAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1194
    const-string v2, "com.dama.paperartist"

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->queryResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    .line 1195
    .restart local v0    # "info":Landroid/content/pm/ResolveInfo;
    if-nez v0, :cond_0

    .line 1199
    .end local v0    # "info":Landroid/content/pm/ResolveInfo;
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private final queryResolveInfo(Ljava/lang/String;)Landroid/content/pm/ResolveInfo;
    .locals 6
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 1203
    new-instance v2, Landroid/content/Intent;

    const-string v4, "android.intent.action.EDIT"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1204
    .local v2, "mainIntent":Landroid/content/Intent;
    const-string v4, "image/*"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1205
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v5}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 1207
    .local v3, "resolveInfos":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 1208
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    iget-object v1, v4, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 1209
    .local v1, "info":Landroid/content/pm/ActivityInfo;
    iget-object v4, v1, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1210
    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/pm/ResolveInfo;

    .line 1212
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :goto_1
    return-object v4

    .line 1207
    .restart local v1    # "info":Landroid/content/pm/ActivityInfo;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1212
    .end local v1    # "info":Landroid/content/pm/ActivityInfo;
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private setContextualWeatherMenuVisibility()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 897
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f02a9

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 898
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f02aa

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 912
    return-void
.end method

.method private setImageEditMenuVisibility(J)J
    .locals 5
    .param p1, "supportedOperations"    # J

    .prologue
    const-wide/16 v2, -0x201

    .line 942
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/ImageEditorUtil;->isEditMenuAvailable(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 944
    and-long/2addr p1, v2

    .line 946
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 947
    :cond_1
    and-long/2addr p1, v2

    .line 949
    :cond_2
    return-wide p1
.end method

.method private setImageNoteMenuVisibility()V
    .locals 3

    .prologue
    .line 966
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v0, :cond_1

    .line 973
    :cond_0
    :goto_0
    return-void

    .line 968
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNoteEditor:Z

    if-eqz v0, :cond_0

    .line 969
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getAgifMode()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 970
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v1, 0x7f0f02a4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method private setMotionMenuVisibility()V
    .locals 3

    .prologue
    .line 976
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v1, :cond_0

    .line 977
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f02ae

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 978
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 979
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 981
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_0
    return-void
.end method

.method private setOcrMenuVisibility()V
    .locals 5

    .prologue
    const v4, 0x7f0f02b1

    const/4 v3, 0x0

    .line 953
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/dmc/ocr/OcrUtils;->isGetTextMenuAvailable(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 954
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 956
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-interface {v1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 957
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 958
    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 963
    .end local v0    # "item":Landroid/view/MenuItem;
    :cond_1
    :goto_0
    return-void

    .line 961
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    invoke-static {v1, v4, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method private setSetAsMenuVisibility()V
    .locals 3

    .prologue
    .line 984
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v1, :cond_1

    .line 993
    :cond_0
    :goto_0
    return-void

    .line 986
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f029a

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 988
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 989
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    .line 990
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private showMemo()V
    .locals 8

    .prologue
    .line 1022
    new-instance v3, Landroid/content/ComponentName;

    const-string v5, "com.diotek.mini_penmemo"

    const-string v6, "com.diotek.mini_penmemo.Mini_PenMemo_Service"

    invoke-direct {v3, v5, v6}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1024
    .local v3, "component":Landroid/content/ComponentName;
    new-instance v5, Landroid/content/Intent;

    const-string v6, "com.samsung.action.MINI_MODE_SERVICE"

    invoke-direct {v5, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v2

    .line 1025
    .local v2, "bi":Landroid/content/Intent;
    const-string v0, "com.diotek.penmemo.util.PenMemoProvider"

    .line 1026
    .local v0, "AUTHORITY":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/PenMemo"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 1027
    .local v1, "CONTENT_URI":Landroid/net/Uri;
    iget v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMemoId:I

    int-to-long v6, v5

    invoke-static {v1, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 1028
    .local v4, "mMemoUri":Landroid/net/Uri;
    invoke-virtual {v2, v4}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1029
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1030
    return-void
.end method

.method private showMultiSelectMemo()V
    .locals 4

    .prologue
    .line 1033
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1034
    .local v1, "i":Landroid/content/Intent;
    const-string/jumbo v2, "vnd.android.cursor.dir/vnd.diotek.pen_memo"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1035
    const-string v2, "LinkType"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1036
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getCurrentfileUri()Ljava/lang/String;

    move-result-object v0

    .line 1037
    .local v0, "fileUri":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 1041
    :goto_0
    return-void

    .line 1039
    :cond_0
    const-string v2, "metadata"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1040
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startVideoTrim()V
    .locals 3

    .prologue
    .line 1137
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 1140
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "VIDEO_TRIM"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1141
    return-void
.end method

.method private startWeatherSettingActivity()V
    .locals 3

    .prologue
    .line 1124
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/android/gallery3d/app/ContextualTagWeatherSetting;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1125
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1126
    const-string/jumbo v1, "weather"

    invoke-static {}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getWeather()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1127
    const-string v1, "location"

    invoke-static {}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getLocation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1128
    const-string v1, "face"

    invoke-static {}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getFace()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1129
    const-string v1, "date"

    invoke-static {}, Lcom/sec/android/gallery3d/ui/ContextualTagView;->getDate()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1130
    const-string v1, "id"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1131
    const-string v1, "filepath"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1132
    const-string/jumbo v1, "weathertagstatus"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getContextualWeatherEditableStatus()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1133
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    .line 1134
    return-void
.end method

.method private updateMenuOperations(Landroid/view/Menu;)V
    .locals 19
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 507
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    if-nez v14, :cond_1

    .line 890
    :cond_0
    :goto_0
    return-void

    .line 510
    :cond_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v14

    invoke-interface {v14}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 511
    const v14, 0x7f0f0253

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 518
    :cond_2
    const/4 v5, 0x0

    .line 519
    .local v5, "isFromCamera":Z
    const/4 v6, 0x0

    .line 520
    .local v6, "isFromWeTogether":Z
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    instance-of v14, v14, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v14, :cond_3

    .line 521
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v14, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v5

    .line 524
    :cond_3
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    if-eqz v14, :cond_36

    if-nez v5, :cond_36

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-nez v14, :cond_36

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v14, :cond_36

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriVideo;

    if-nez v14, :cond_36

    .line 526
    const/4 v14, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setCameraButtonVisibility(Landroid/view/Menu;Z)V

    .line 527
    const v14, 0x7f0f029c

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 528
    const v14, 0x7f0f02a2

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 542
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getAgifMode()Z

    move-result v14

    if-nez v14, :cond_5

    .line 543
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v14, v15}, Lcom/sec/android/gallery3d/util/GalleryUtils;->updateAgifAttribute(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 544
    :cond_5
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v12

    .line 546
    .local v12, "supportedOperations":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v14

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getAgifMode()Z

    move-result v14

    if-nez v14, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v14

    const/4 v15, 0x2

    if-ne v14, v15, :cond_6

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-nez v14, :cond_7

    .line 548
    :cond_6
    const-wide/32 v14, -0x4000001

    and-long/2addr v12, v14

    .line 550
    :cond_7
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMFDMenu:Z

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isFaceTagAvailable(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_9

    .line 551
    :cond_8
    const-wide/32 v14, -0x4000001

    and-long/2addr v12, v14

    .line 554
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox2Enabled:Z

    if-eqz v14, :cond_a

    .line 555
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0299

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 559
    :cond_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 560
    .local v4, "extras":Landroid/os/Bundle;
    if-eqz v4, :cond_b

    const-string v14, "NotSupportSaveMenu"

    const/4 v15, 0x0

    invoke-virtual {v4, v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_b

    .line 561
    const-wide v14, -0x8000000001L

    and-long/2addr v12, v14

    .line 564
    :cond_b
    if-eqz v4, :cond_c

    const-string v14, "from-WeTogether"

    const/4 v15, 0x0

    invoke-virtual {v4, v14, v15}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v14

    if-eqz v14, :cond_c

    .line 565
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getMenusForWeTogether()J

    move-result-wide v12

    .line 566
    const/4 v6, 0x1

    .line 569
    :cond_c
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setImageEditMenuVisibility(J)J

    move-result-wide v12

    .line 570
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mFromSetupWidzard:Z

    if-eqz v14, :cond_d

    .line 571
    const-wide/16 v12, 0x401

    .line 575
    :cond_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v14

    if-eqz v14, :cond_e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v14

    if-nez v14, :cond_e

    .line 576
    const-wide v14, 0x5ffffffefff7fdd1L    # 2.6815603071640594E154

    and-long/2addr v12, v14

    .line 588
    :cond_e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v14

    if-eqz v14, :cond_f

    .line 589
    const-wide/16 v14, -0x21

    and-long/2addr v12, v14

    .line 593
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->isAnyAppSupportEdit()Z

    move-result v14

    if-nez v14, :cond_10

    .line 594
    const-wide/16 v14, -0x201

    and-long/2addr v12, v14

    .line 596
    :cond_10
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p1

    invoke-static {v14, v0, v12, v13}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v12

    .line 598
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v14, :cond_11

    .line 599
    const-wide v14, -0x2000000000000001L    # -2.681561585988519E154

    and-long/2addr v12, v14

    .line 600
    const v14, 0x7f0f026f

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 603
    :cond_11
    move-object/from16 v0, p1

    invoke-static {v0, v12, v13}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 605
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v14, :cond_39

    .line 606
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b3

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 611
    :goto_2
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGroupId:Z

    if-eqz v14, :cond_3e

    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCreateAGIF:Z

    if-eqz v14, :cond_3e

    .line 614
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v14, :cond_3c

    .line 615
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 616
    .local v8, "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    iget-boolean v14, v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;->mIsGroupItem:Z

    if-eqz v14, :cond_3b

    .line 617
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v14, :cond_3a

    .line 618
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsBurstShot:Z

    .line 619
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 642
    .end local v8    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_12
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mFromSetupWidzard:Z

    if-nez v14, :cond_15

    .line 643
    if-nez v6, :cond_13

    .line 644
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->drawChangePlayerIcon()V

    .line 646
    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setMotionMenuVisibility()V

    .line 647
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setImageNoteMenuVisibility()V

    .line 648
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setOcrMenuVisibility()V

    .line 649
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v14, :cond_14

    .line 650
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setContextualWeatherMenuVisibility()V

    .line 652
    :cond_14
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBurstShotSettingIcon:Z

    if-eqz v14, :cond_40

    .line 653
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02a3

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 669
    :cond_15
    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setSetAsMenuVisibility()V

    .line 671
    const v14, 0x7f0f0257

    sget-boolean v15, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSettings:Z

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 673
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v14, :cond_16

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriVideo;

    if-eqz v14, :cond_17

    .line 674
    :cond_16
    const v14, 0x7f0f0257

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 675
    const v14, 0x7f0f0251

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 678
    :cond_17
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v14, :cond_18

    .line 679
    const v14, 0x7f0f0251

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 682
    :cond_18
    const-wide/16 v14, 0x400

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_19

    .line 683
    const v15, 0x7f0f029b

    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoreInfo:Z

    if-nez v14, :cond_42

    const/4 v14, 0x1

    :goto_5
    move-object/from16 v0, p1

    invoke-static {v0, v15, v14}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 684
    const v14, 0x7f0f02b7

    sget-boolean v15, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMoreInfo:Z

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 685
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    const-class v15, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v14, v15}, Lcom/sec/android/gallery3d/app/StateManager;->hasStateClass(Ljava/lang/Class;)Z

    move-result v14

    if-eqz v14, :cond_19

    .line 686
    const v14, 0x7f0f029b

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 687
    const v14, 0x7f0f02b7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 691
    :cond_19
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v14

    instance-of v14, v14, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    if-eqz v14, :cond_1a

    .line 692
    const v14, 0x7f0f0280

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 693
    const v14, 0x7f0f0281

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 694
    const v14, 0x7f0f0270

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 695
    const v14, 0x7f0f02a8

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 698
    :cond_1a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isLonglifeModeOn(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_1b

    .line 699
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 702
    :cond_1b
    const v14, 0x7f0f02de

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 703
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-eqz v14, :cond_1c

    .line 704
    const v14, 0x7f0f026e

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 707
    :cond_1c
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoEditor:Z

    if-nez v14, :cond_1d

    .line 708
    const v14, 0x7f0f029c

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 710
    :cond_1d
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoSignature:Z

    if-eqz v14, :cond_1e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v14

    if-nez v14, :cond_1e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v14

    if-nez v14, :cond_1e

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsBurstShot:Z

    if-eqz v14, :cond_1f

    .line 711
    :cond_1e
    const v14, 0x7f0f02a6

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 713
    :cond_1f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWvga(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_20

    .line 714
    const v14, 0x7f0f0299

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 715
    const v14, 0x7f0f026d

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 716
    const v14, 0x7f0f026e

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 719
    :cond_20
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCmcc:Z

    if-eqz v14, :cond_21

    .line 720
    const v14, 0x7f0f02b0

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 722
    :cond_21
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox:Z

    if-eqz v14, :cond_22

    .line 723
    const v14, 0x7f0f02b0

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 724
    const v14, 0x7f0f02a6

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 725
    const v14, 0x7f0f029c

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 726
    const v14, 0x7f0f0280

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 727
    const v14, 0x7f0f0281

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 730
    :cond_22
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v14, :cond_24

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v16, 0x10

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v14

    if-nez v14, :cond_23

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v14

    if-eqz v14, :cond_24

    .line 731
    :cond_23
    const v14, 0x7f0f026d

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 733
    :cond_24
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v14, :cond_45

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v14

    if-eqz v14, :cond_25

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_26

    :cond_25
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isReadyPrivateMode(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_45

    .line 735
    :cond_26
    const-wide/32 v14, 0x40000000

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_43

    .line 736
    const v14, 0x7f0f0277

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 740
    :goto_6
    const-wide/32 v14, 0x20000000

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_44

    .line 741
    const v14, 0x7f0f0278

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 750
    :goto_7
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v14, :cond_49

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnoxInstallMode:Z

    if-eqz v14, :cond_49

    .line 751
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsKnox:Z

    if-eqz v14, :cond_47

    .line 752
    const v14, 0x7f0f0279

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 753
    const-wide/32 v14, 0x1000000

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_46

    .line 754
    const v14, 0x7f0f027a

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 772
    :goto_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-virtual {v14}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v14

    iget v14, v14, Landroid/content/res/Configuration;->orientation:I

    const/4 v15, 0x1

    if-ne v14, v15, :cond_4b

    .line 773
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v14

    if-nez v14, :cond_4a

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-nez v14, :cond_4a

    .line 774
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCameraButtonAtDetailView:Z

    if-eqz v14, :cond_27

    if-eqz v5, :cond_28

    .line 775
    :cond_27
    const v14, 0x7f0f029c

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 777
    :cond_28
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHideChangePlayerText:Z

    if-eqz v14, :cond_29

    .line 778
    const v14, 0x7f0f0299

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 781
    :cond_29
    const v14, 0x7f0f026e

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 782
    const v14, 0x7f0f029f

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 783
    const v14, 0x7f0f0251

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 784
    const v14, 0x7f0f0276

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 796
    :cond_2a
    :goto_9
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v14, :cond_2b

    .line 797
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isDeleteIconDisabled()Z

    move-result v14

    if-eqz v14, :cond_4c

    .line 798
    const v14, 0x7f0f026e

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 804
    :cond_2b
    :goto_a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    const-wide/16 v16, 0x10

    move-wide/from16 v0, v16

    invoke-virtual {v14, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v14

    if-eqz v14, :cond_4d

    .line 805
    const v14, 0x7f0f02b5

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 810
    :goto_b
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v11

    .line 812
    .local v11, "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v14, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_EVENT:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v11, v14, :cond_4e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_4e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v14

    if-nez v14, :cond_4e

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v14, :cond_4e

    .line 814
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f026e

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 815
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b6

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 816
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 817
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02a8

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 818
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSupportDeleteFromMapView(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_2c

    .line 819
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f026e

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 820
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b6

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 840
    :cond_2c
    :goto_c
    sget-object v14, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v11, v14, :cond_2d

    .line 841
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 842
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02a8

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 845
    :cond_2d
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v14, :cond_2e

    .line 846
    const v14, 0x7f0f026d

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 850
    :cond_2e
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_2f

    .line 851
    const v14, 0x7f0f02aa

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 852
    const v14, 0x7f0f02a7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 853
    const v14, 0x7f0f029c

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 854
    const v14, 0x7f0f029f

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 855
    const v14, 0x7f0f0285

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 856
    const v14, 0x7f0f029b

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 857
    const v14, 0x7f0f02b7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 858
    const v14, 0x7f0f0257

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 859
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b1

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 860
    const v14, 0x7f0f02a6

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 861
    const v14, 0x7f0f02a4

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 862
    const v14, 0x7f0f02a5

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 863
    const v14, 0x7f0f0282

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 867
    :cond_2f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_31

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-nez v14, :cond_30

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-eqz v14, :cond_31

    .line 869
    :cond_30
    const v14, 0x7f0f0257

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 870
    const v14, 0x7f0f02b7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 871
    const v14, 0x7f0f029b

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 873
    :cond_31
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v14

    if-eqz v14, :cond_32

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v14}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v14

    if-nez v14, :cond_32

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_33

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v14

    if-eqz v14, :cond_33

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v14

    if-nez v14, :cond_33

    .line 875
    :cond_32
    const v14, 0x7f0f0283

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 879
    :cond_33
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v14, :cond_34

    .line 880
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    .line 881
    .local v10, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v14, v10, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    if-eqz v14, :cond_34

    check-cast v10, Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    .end local v10    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getBaseSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v14

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/CategoryAlbum;

    if-eqz v14, :cond_34

    .line 882
    const v14, 0x7f0f0270

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 885
    :cond_34
    if-eqz v6, :cond_35

    .line 886
    const v14, 0x7f0f0257

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 888
    :cond_35
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mNoUseCopytoClipboardInDetailview:Z

    if-eqz v14, :cond_0

    .line 889
    const v14, 0x7f0f02a7

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_0

    .line 530
    .end local v4    # "extras":Landroid/os/Bundle;
    .end local v11    # "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v12    # "supportedOperations":J
    :cond_36
    const/4 v14, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setCameraButtonVisibility(Landroid/view/Menu;Z)V

    .line 531
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-nez v14, :cond_37

    .line 532
    const v14, 0x7f0f02a2

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 535
    :cond_37
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v14

    if-eqz v14, :cond_38

    .line 536
    const v14, 0x7f0f029c

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_1

    .line 537
    :cond_38
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v14, :cond_4

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-nez v14, :cond_4

    .line 538
    const v14, 0x7f0f029c

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_1

    .line 608
    .restart local v4    # "extras":Landroid/os/Bundle;
    .restart local v12    # "supportedOperations":J
    :cond_39
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 621
    .restart local v8    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3a
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b3

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 624
    :cond_3b
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v14, Lcom/sec/android/gallery3d/data/LocalImage;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/LocalImage;->isBurstShotImage()Z

    move-result v14

    if-nez v14, :cond_12

    .line 625
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 628
    .end local v8    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    :cond_3c
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v14, :cond_3d

    .line 629
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 631
    :cond_3d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b3

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 635
    :cond_3e
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEditBurstShot:Z

    if-eqz v14, :cond_3f

    .line 636
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b4

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 638
    :cond_3f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b3

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_3

    .line 655
    :cond_40
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v14, :cond_15

    .line 656
    const/4 v7, 0x0

    .line 657
    .local v7, "isVisible":Z
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 658
    .restart local v8    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v14

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-lez v14, :cond_41

    .line 659
    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "group_id = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " and bucket_id = "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getBucketId()I

    move-result v15

    invoke-static {v15}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 661
    .local v9, "selection":Ljava/lang/String;
    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v8    # "localMediaItem":Lcom/sec/android/gallery3d/data/LocalMediaItem;
    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/data/LocalImage;->isBurstShotImage(Ljava/lang/String;)Z

    move-result v14

    if-eqz v14, :cond_41

    .line 662
    const/4 v7, 0x1

    .line 665
    .end local v9    # "selection":Ljava/lang/String;
    :cond_41
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02a3

    invoke-static {v14, v15, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 683
    .end local v7    # "isVisible":Z
    :cond_42
    const/4 v14, 0x0

    goto/16 :goto_5

    .line 738
    :cond_43
    const v14, 0x7f0f0277

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 744
    :cond_44
    const v14, 0x7f0f0278

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_7

    .line 747
    :cond_45
    const v14, 0x7f0f0277

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 748
    const v14, 0x7f0f0278

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_7

    .line 756
    :cond_46
    const v14, 0x7f0f027a

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_8

    .line 759
    :cond_47
    const v14, 0x7f0f027a

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 760
    const-wide/32 v14, 0x2000000

    and-long/2addr v14, v12

    const-wide/16 v16, 0x0

    cmp-long v14, v14, v16

    if-eqz v14, :cond_48

    .line 761
    const v14, 0x7f0f0279

    const/4 v15, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_8

    .line 763
    :cond_48
    const v14, 0x7f0f0279

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_8

    .line 767
    :cond_49
    const v14, 0x7f0f0279

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 768
    const v14, 0x7f0f027a

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_8

    .line 785
    :cond_4a
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-eqz v14, :cond_2a

    .line 786
    const v14, 0x7f0f029b

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 787
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f029b

    const v16, 0x7f020024

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto/16 :goto_9

    .line 790
    :cond_4b
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v14

    if-eqz v14, :cond_2a

    .line 791
    const v14, 0x7f0f029b

    const/4 v15, 0x2

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 792
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f029b

    const v16, 0x7f0203b8

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemIcon(Landroid/view/Menu;II)V

    goto/16 :goto_9

    .line 799
    :cond_4c
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableEditShowAsAction:Z

    if-eqz v14, :cond_2b

    .line 800
    const v14, 0x7f0f029c

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    goto/16 :goto_a

    .line 807
    :cond_4d
    const v14, 0x7f0f02b5

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v14, v15}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_b

    .line 822
    .restart local v11    # "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_4e
    const/4 v14, 0x1

    invoke-static {v14}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-static {v11, v14}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v14

    if-nez v14, :cond_4f

    sget-object v14, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v11, v14, :cond_51

    :cond_4f
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v14, :cond_51

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v14

    if-nez v14, :cond_51

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v14, v14, Lcom/sec/android/gallery3d/data/UriImage;

    if-nez v14, :cond_51

    .line 824
    sget-boolean v14, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v14, :cond_50

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v14

    invoke-virtual {v14}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v14

    instance-of v14, v14, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-nez v14, :cond_50

    .line 825
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v14, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v14, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-virtual {v15, v14}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 826
    .local v3, "category_name":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v15, 0x7f0e0463

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-virtual/range {v14 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 827
    .local v2, "category_menu":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    invoke-static {v14, v15, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 828
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    const/16 v16, 0x1

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 832
    .end local v2    # "category_menu":Ljava/lang/String;
    .end local v3    # "category_name":Ljava/lang/String;
    :goto_d
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f026e

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 833
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b6

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 834
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02a8

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_c

    .line 830
    :cond_50
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_d

    .line 836
    :cond_51
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f0297

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 837
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    const v15, 0x7f0f02b6

    const/16 v16, 0x0

    invoke-static/range {v14 .. v16}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_c
.end method


# virtual methods
.method public getCurrentfileUri()Ljava/lang/String;
    .locals 5

    .prologue
    .line 1044
    const/4 v1, 0x0

    .line 1046
    .local v1, "fileUri":Ljava/lang/String;
    :try_start_0
    sget-object v3, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v4, "getCurrentfileUri fileUri "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1049
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentfileUri:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1050
    sget-object v3, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v4, "getCurrentfileUri mCurrentfileUri: "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentfileUri:Ljava/lang/String;

    .line 1068
    :cond_0
    :goto_0
    return-object v1

    .line 1053
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v3}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1055
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v3, :cond_2

    .line 1056
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalImage;

    .line 1057
    .local v2, "item":Lcom/sec/android/gallery3d/data/LocalImage;
    new-instance v3, Ljava/io/File;

    iget-object v4, v2, Lcom/sec/android/gallery3d/data/LocalImage;->filePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1058
    goto :goto_0

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/LocalImage;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    instance-of v3, v3, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v3, :cond_3

    .line 1059
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    check-cast v2, Lcom/sec/android/gallery3d/data/LocalVideo;

    .line 1060
    .local v2, "item":Lcom/sec/android/gallery3d/data/LocalVideo;
    new-instance v3, Ljava/io/File;

    iget-object v4, v2, Lcom/sec/android/gallery3d/data/LocalVideo;->filePath:Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1061
    goto :goto_0

    .line 1062
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/LocalVideo;
    :cond_3
    sget-object v3, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v4, "getCurrentfileUri fileUri is null "

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1065
    :catch_0
    move-exception v0

    .line 1066
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 1154
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 1155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->isRotationMenuItemEnabled()Z

    move-result v0

    .line 1157
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0298

    .line 173
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mMenu:Landroid/view/Menu;

    .line 174
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsBurstShot:Z

    .line 175
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 176
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 178
    .local v0, "inflater":Landroid/view/MenuInflater;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getOptionMenuId()I

    move-result v1

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 183
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 189
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 190
    return-void

    .line 186
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 24
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    if-nez v18, :cond_1

    .line 493
    :cond_0
    :goto_0
    :sswitch_0
    return-void

    .line 207
    :cond_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    .line 210
    const/4 v14, 0x0

    .line 211
    .local v14, "params":[Ljava/lang/Object;
    :try_start_0
    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    .line 487
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->onOptionsItemSelected(Landroid/view/MenuItem;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 490
    :catch_0
    move-exception v4

    .line 491
    .local v4, "e":Ljava/lang/Exception;
    :goto_1
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 214
    .end local v4    # "e":Ljava/lang/Exception;
    :sswitch_1
    :try_start_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_HOME_ICON:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 217
    :sswitch_2
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsActive:Z

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 218
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    if-eqz v18, :cond_2

    .line 219
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_POPUP_DIALOG_VISIBLE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    .line 220
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V

    goto :goto_0

    .line 224
    :sswitch_3
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v18, :cond_0

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 229
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 238
    :sswitch_5
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryCrop:Z

    if-eqz v18, :cond_0

    .line 239
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_CROP_MEDIA:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 243
    :sswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto/16 :goto_0

    .line 246
    :sswitch_7
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v8, v0, [D

    .line 247
    .local v8, "latlng":[D
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getLatLong([D)V

    .line 248
    const/16 v18, 0x0

    aget-wide v6, v8, v18

    .line 249
    .local v6, "latitude":D
    const/16 v18, 0x1

    aget-wide v10, v8, v18

    .line 250
    .local v10, "longitude":D
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChnUsageAlertPopup:Z

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_5

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const-string v19, "LocationNetworkAlertDialogOff"

    const/16 v20, 0x0

    invoke-static/range {v18 .. v20}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v18

    if-nez v18, :cond_3

    .line 252
    const/16 v18, 0x4

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    aput-object v19, v15, v18

    const/16 v18, 0x1

    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    aput-object v19, v15, v18

    const/16 v18, 0x2

    const/16 v19, 0x5

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v15, v18

    const/16 v18, 0x3

    const/16 v19, 0x0

    aput-object v19, v15, v18
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 253
    .end local v14    # "params":[Ljava/lang/Object;
    .local v15, "params":[Ljava/lang/Object;
    :try_start_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "SHOW_USAGE_ALERT_DIALOG"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    move-object v14, v15

    .end local v15    # "params":[Ljava/lang/Object;
    .restart local v14    # "params":[Ljava/lang/Object;
    goto/16 :goto_0

    .line 254
    :cond_3
    :try_start_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_4

    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsWifiOnlyModel:Z

    if-eqz v18, :cond_4

    .line 255
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const v19, 0x7f0e02fa

    const/16 v20, 0x0

    invoke-static/range {v18 .. v20}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 257
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "MOREINFO_EVENT"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 263
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "MOREINFO_EVENT"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    const/16 v22, 0x0

    invoke-static/range {v22 .. v22}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 271
    .end local v6    # "latitude":D
    .end local v8    # "latlng":[D
    .end local v10    # "longitude":D
    :sswitch_8
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryRotate:Z

    if-eqz v18, :cond_0

    .line 272
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_ROTATE_MEDIA:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    invoke-interface/range {p1 .. p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v19

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setIntData(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 277
    :sswitch_9
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUndoDeletion:Z

    if-eqz v18, :cond_6

    .line 278
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACTION_DELETE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 280
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showDeleteDialog()V

    goto/16 :goto_0

    .line 284
    :sswitch_a
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePopupMenuDialog:Z

    if-eqz v18, :cond_7

    .line 285
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_POPUP_DIALOG_VISIBLE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    .line 286
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v19

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto/16 :goto_0

    .line 289
    :sswitch_b
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_ON_MAP:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 293
    :sswitch_c
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v18, :cond_0

    .line 295
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_NAVIGATION:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 299
    :sswitch_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    const-wide/16 v20, 0x800

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v18

    if-eqz v18, :cond_8

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->startMagicShotStudio()V

    goto/16 :goto_0

    .line 302
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->imageEdit()V

    goto/16 :goto_0

    .line 305
    :sswitch_e
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->startVideoTrim()V

    goto/16 :goto_0

    .line 308
    :sswitch_f
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEND_TO_OTHER_DEVICES:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 311
    :sswitch_10
    sget-object v18, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v19, "change player selected"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_CHANGE_PLAYER:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 318
    :sswitch_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    move/from16 v18, v0

    if-eqz v18, :cond_0

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    .line 320
    .local v16, "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    move/from16 v0, v19

    new-array v0, v0, [J

    move-object/from16 v19, v0

    const/16 v20, 0x0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;->getSlinkObjectId()J

    move-result-wide v22

    aput-wide v22, v19, v20

    invoke-static/range {v18 .. v19}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->downloadFiles(Landroid/content/Context;[J)V

    goto/16 :goto_0

    .line 326
    .end local v16    # "sLinkImage":Lcom/sec/android/gallery3d/remote/slink/SLinkImage;
    :sswitch_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    move/from16 v18, v0

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    move/from16 v18, v0

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    move/from16 v18, v0

    if-nez v18, :cond_9

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move/from16 v18, v0

    if-eqz v18, :cond_a

    .line 329
    :cond_9
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 330
    .local v9, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 331
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object v9, v15, v18
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    .line 334
    .end local v14    # "params":[Ljava/lang/Object;
    .restart local v15    # "params":[Ljava/lang/Object;
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "DOWNLOAD_CLOUDBY"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-object v14, v15

    .line 336
    .end local v15    # "params":[Ljava/lang/Object;
    .restart local v14    # "params":[Ljava/lang/Object;
    goto/16 :goto_0

    .end local v9    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_a
    :try_start_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    instance-of v0, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    move/from16 v18, v0

    if-eqz v18, :cond_b

    .line 337
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 338
    .restart local v9    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 339
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    aput-object v9, v15, v18
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0

    .line 342
    .end local v14    # "params":[Ljava/lang/Object;
    .restart local v15    # "params":[Ljava/lang/Object;
    :try_start_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "DOWNLOAD_NEARBY"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1

    move-object v14, v15

    .line 344
    .end local v15    # "params":[Ljava/lang/Object;
    .restart local v14    # "params":[Ljava/lang/Object;
    goto/16 :goto_0

    .line 345
    .end local v9    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_b
    :try_start_7
    sget-object v18, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->TAG:Ljava/lang/String;

    const-string v19, "not download requestable, please implement."

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 349
    :sswitch_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    goto/16 :goto_0

    .line 352
    :sswitch_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->printImage()V

    goto/16 :goto_0

    .line 355
    :sswitch_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->copyToClipboard()V

    goto/16 :goto_0

    .line 358
    :sswitch_16
    new-instance v5, Landroid/content/Intent;

    const-string v18, "android.settings.MOTION_SETTINGS"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 361
    .local v5, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 366
    .end local v5    # "intent":Landroid/content/Intent;
    :sswitch_17
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_MANUAL_DETECT:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 370
    :sswitch_18
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showMemo()V

    goto/16 :goto_0

    .line 373
    :sswitch_19
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showMultiSelectMemo()V

    goto/16 :goto_0

    .line 378
    :sswitch_1a
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_BURST_PLAY:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 382
    :sswitch_1b
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseContextualAwareness:Z

    if-eqz v18, :cond_0

    .line 383
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->startWeatherSettingActivity()V

    goto/16 :goto_0

    .line 387
    :sswitch_1c
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v18, :cond_0

    .line 388
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->handleGetText()V

    goto/16 :goto_0

    .line 391
    :sswitch_1d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getActionBar()Landroid/app/ActionBar;

    move-result-object v18

    if-eqz v18, :cond_c

    .line 392
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getActionBar()Landroid/app/ActionBar;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/app/ActionBar;->hide()V

    .line 393
    :cond_c
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMAGE_NOTE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 396
    :sswitch_1e
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v19, v0

    aput-object v19, v17, v18

    .line 397
    .local v17, "tempParams":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "FLASH_ANNOTATE"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 400
    .end local v17    # "tempParams":[Ljava/lang/Object;
    :sswitch_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHome(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 403
    :sswitch_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "START_CAMERA"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v20, v0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 407
    :sswitch_21
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_IMPORT:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 410
    :sswitch_22
    sget-boolean v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_e

    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v18

    if-nez v18, :cond_e

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->isSelected3DTour(Landroid/content/Context;)Z

    move-result v18

    if-eqz v18, :cond_f

    .line 412
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto/16 :goto_0

    .line 414
    :cond_f
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 418
    :sswitch_23
    new-instance v5, Landroid/content/Intent;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    const-class v19, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v5, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 419
    .restart local v5    # "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 422
    .end local v5    # "intent":Landroid/content/Intent;
    :sswitch_24
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x1

    const/16 v21, 0x0

    invoke-virtual/range {v18 .. v21}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto/16 :goto_0

    .line 425
    :sswitch_25
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto/16 :goto_0

    .line 428
    :sswitch_26
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 431
    :sswitch_27
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 434
    :sswitch_28
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 438
    :sswitch_29
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v12

    .line 440
    .local v12, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v12, :cond_0

    .line 441
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    aput-object v19, v15, v18

    const/16 v18, 0x1

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v19

    aput-object v19, v15, v18
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_0

    .line 445
    .end local v14    # "params":[Ljava/lang/Object;
    .restart local v15    # "params":[Ljava/lang/Object;
    :try_start_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "START_MMS_SAVE"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 448
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 449
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mHander:Landroid/os/Handler;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;)V

    const-wide/16 v20, 0x5dc

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1

    move-object v14, v15

    .end local v15    # "params":[Ljava/lang/Object;
    .restart local v14    # "params":[Ljava/lang/Object;
    goto/16 :goto_0

    .line 461
    .end local v12    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :sswitch_2a
    :try_start_9
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_MOTION_PICTURE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 464
    :sswitch_2b
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_MAKE_BUSRTSHOT_PICTURE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 467
    :sswitch_2c
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v13, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    aput-object v19, v13, v18

    const/16 v18, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    move-object/from16 v19, v0

    aput-object v19, v13, v18

    .line 470
    .local v13, "param":[Ljava/lang/Object;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "PHOTO_SIGNATURE"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v13}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 474
    .end local v13    # "param":[Ljava/lang/Object;
    :sswitch_2d
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 477
    :sswitch_2e
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    sget v19, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELETE_SOUND_IN_PICTURE:I

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v18

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 480
    :sswitch_2f
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v15, v0, [Ljava/lang/Object;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v19, v0

    aput-object v19, v15, v18

    const/16 v18, 0x1

    const/16 v19, 0x0

    aput-object v19, v15, v18
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_0

    .line 481
    .end local v14    # "params":[Ljava/lang/Object;
    .restart local v15    # "params":[Ljava/lang/Object;
    :try_start_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v18, v0

    invoke-static/range {v18 .. v18}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v18

    const-string v19, "S_STUDIO"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v15}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_1

    goto/16 :goto_0

    .line 490
    :catch_1
    move-exception v4

    move-object v14, v15

    .end local v15    # "params":[Ljava/lang/Object;
    .restart local v14    # "params":[Ljava/lang/Object;
    goto/16 :goto_1

    .line 484
    :sswitch_30
    :try_start_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-object/from16 v18, v0

    const/16 v19, 0x205

    const/16 v20, 0x1

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_0

    goto/16 :goto_0

    .line 211
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_1
        0x7f0f0251 -> :sswitch_20
        0x7f0f0257 -> :sswitch_23
        0x7f0f026c -> :sswitch_2
        0x7f0f026d -> :sswitch_22
        0x7f0f026e -> :sswitch_9
        0x7f0f026f -> :sswitch_4
        0x7f0f0270 -> :sswitch_13
        0x7f0f0276 -> :sswitch_12
        0x7f0f0277 -> :sswitch_25
        0x7f0f0278 -> :sswitch_26
        0x7f0f0279 -> :sswitch_27
        0x7f0f027a -> :sswitch_28
        0x7f0f027c -> :sswitch_21
        0x7f0f0280 -> :sswitch_8
        0x7f0f0281 -> :sswitch_8
        0x7f0f0282 -> :sswitch_30
        0x7f0f0283 -> :sswitch_2f
        0x7f0f0284 -> :sswitch_1f
        0x7f0f0285 -> :sswitch_14
        0x7f0f0297 -> :sswitch_2d
        0x7f0f0298 -> :sswitch_3
        0x7f0f0299 -> :sswitch_10
        0x7f0f029a -> :sswitch_a
        0x7f0f029b -> :sswitch_6
        0x7f0f029c -> :sswitch_d
        0x7f0f029d -> :sswitch_18
        0x7f0f029e -> :sswitch_19
        0x7f0f029f -> :sswitch_e
        0x7f0f02a0 -> :sswitch_0
        0x7f0f02a1 -> :sswitch_11
        0x7f0f02a2 -> :sswitch_24
        0x7f0f02a3 -> :sswitch_1a
        0x7f0f02a4 -> :sswitch_1d
        0x7f0f02a5 -> :sswitch_1e
        0x7f0f02a6 -> :sswitch_2c
        0x7f0f02a7 -> :sswitch_15
        0x7f0f02a8 -> :sswitch_5
        0x7f0f02a9 -> :sswitch_1b
        0x7f0f02aa -> :sswitch_1b
        0x7f0f02ad -> :sswitch_f
        0x7f0f02ae -> :sswitch_16
        0x7f0f02af -> :sswitch_b
        0x7f0f02b0 -> :sswitch_c
        0x7f0f02b1 -> :sswitch_1c
        0x7f0f02b2 -> :sswitch_29
        0x7f0f02b3 -> :sswitch_2a
        0x7f0f02b4 -> :sswitch_2b
        0x7f0f02b5 -> :sswitch_2e
        0x7f0f02b6 -> :sswitch_9
        0x7f0f02b7 -> :sswitch_7
        0x7f0f02b8 -> :sswitch_17
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 161
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 165
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mOCRDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    if-eqz v0, :cond_1

    .line 166
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mOCRDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 168
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsActive:Z

    .line 169
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 195
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 196
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->updateMenuOperations(Landroid/view/Menu;)V

    .line 197
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsActive:Z

    .line 150
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 154
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseOCR:Z

    if-eqz v0, :cond_1

    .line 155
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mOCRDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 156
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mIsUpAvailable:Z

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->setDisplayOptions(ZZ)V

    .line 157
    return-void
.end method

.method public setTitle(Ljava/lang/String;II)V
    .locals 2
    .param p1, "itemName"    # Ljava/lang/String;
    .param p2, "currentPosition"    # I
    .param p3, "numberOfItemsInAlbum"    # I

    .prologue
    .line 998
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;ILjava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1019
    return-void
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 138
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mFromSetupWidzard:Z

    if-eqz v0, :cond_1

    .line 140
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showHomeButtonOnTopLeft()V

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 142
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showAllButtonsOnTopLeft()V

    goto :goto_0
.end method

.method public showDeleteDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1098
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_LOCK_TOUCH:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->notifyObservers(Ljava/lang/Object;)V

    .line 1099
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 1100
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 1101
    return-void
.end method

.method public unregisterOrientationListener()V
    .locals 1

    .prologue
    .line 1162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 1163
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->unregisterListener()V

    .line 1165
    :cond_0
    return-void
.end method

.method public updateFuntionSimplificationMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1171
    const/4 v0, 0x1

    return v0
.end method
