.class public Lcom/sec/samsung/gallery/view/allview/AllViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/data/ContentListener;
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/allview/AllViewState$SelectionTask;
    }
.end annotation


# static fields
.field protected static final GROUP_ALL_BURST_IMAGES:I = -0x1

.field public static final KEY_IS_REFRESH:Ljava/lang/String; = "is_refresh"

.field private static final TAG:Ljava/lang/String;

.field private static final THUMB_PHOTO_CACHE_SIZE:I = 0x30

.field protected static final UNGROUP_ALL_BURST_IMAGES:I = -0x2


# instance fields
.field private bShowDisconnectDialog:Z

.field private mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

.field private mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mAllViewMediaRemovedMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mAllViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mBgImageIndex:I

.field private mConnectedDevicesObserver:Landroid/database/ContentObserver;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mCurrentMediaItemIndex:I

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mIsDeleteMode:Z

.field protected mIsSortByLatest:Z

.field private mMenu:Landroid/view/Menu;

.field private mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

.field private mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

.field private mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mSplitScroll:F

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

.field private mThumbScroll:F

.field private mTopSetPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const-class v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 121
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 124
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 127
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 129
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 131
    iput v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I

    .line 132
    iput v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mIsSortByLatest:Z

    .line 143
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mIsDeleteMode:Z

    .line 145
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z

    .line 149
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState$1;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mConnectedDevicesObserver:Landroid/database/ContentObserver;

    .line 173
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;

    const-string v1, "ALL_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState$2;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAllViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 212
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;

    const-string v1, "ALL_VIEW_MEDIA_REMOVED"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState$3;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAllViewMediaRemovedMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 242
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$4;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 256
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$5;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    .line 1131
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleLeaveMode()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->refreshNearbyData()V

    return-void
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I

    return v0
.end method

.method static synthetic access$1902(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # I

    .prologue
    .line 114
    iput p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->startDetailView(I)V

    return-void
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # I

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->setItemFocus(I)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/allview/AllViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/view/allview/AllViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/allview/AllViewState;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->isCloudItemDeleted(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/allview/AllViewState;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 114
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->showDisconnectedDialog(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleBackgroundDeletion()V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/allview/AllViewState;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    return-object v0
.end method

.method private cancelSwitchFilterTask()V
    .locals 2

    .prologue
    .line 929
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    if-eqz v0, :cond_0

    .line 930
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->cancel(Z)Z

    .line 931
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 933
    :cond_0
    return-void
.end method

.method private checkMediaAvailability()V
    .locals 1

    .prologue
    .line 920
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 921
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 922
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->startNoItemViewState()V

    .line 926
    :goto_0
    return-void

    .line 924
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    goto :goto_0
.end method

.method private dismissPopupMenu()V
    .locals 3

    .prologue
    .line 1014
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v0

    .line 1015
    .local v0, "menu":Landroid/widget/ListPopupWindow;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/allview/AllViewState$16;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$16;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Landroid/widget/ListPopupWindow;)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1021
    return-void
.end method

.method private enterSelectionMode(Z)V
    .locals 4
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v3, 0x0

    .line 990
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 997
    :goto_0
    return-void

    .line 993
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 994
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->initActionBar()V

    .line 995
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 996
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    goto :goto_0
.end method

.method private finishCurrentViewState()V
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 957
    return-void
.end method

.method private getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 341
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1}, Ljava/lang/String;-><init>()V

    .line 342
    .local v1, "title":Ljava/lang/String;
    if-eqz p1, :cond_1

    .line 343
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v3, "/slink"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v3, "/nearby"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 344
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 345
    .local v0, "strBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    const-string v2, " ("

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 347
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 348
    const-string v2, ")"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 349
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 354
    .end local v0    # "strBuilder":Ljava/lang/StringBuilder;
    :cond_1
    :goto_0
    return-object v1

    .line 351
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getTopSetPath(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 910
    if-eqz p1, :cond_0

    .line 911
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 912
    .local v0, "topSetPath":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 916
    .end local v0    # "topSetPath":Ljava/lang/String;
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getOneAlbumPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private handleBackgroundDeletion()V
    .locals 1

    .prologue
    .line 1216
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$19;->start()V

    .line 1247
    return-void
.end method

.method private handleLeaveMode()V
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_0

    .line 1190
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->exitSelectionMode()V

    .line 1191
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 1192
    return-void
.end method

.method private handleSortByAction(I)V
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 875
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I

    .line 876
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "sort_by_type_ALL"

    invoke-static {v0, v1, p1}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 877
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->setSortByType(I)V

    .line 878
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->resetAdapter()V

    .line 879
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->reloadData()V

    .line 880
    return-void
.end method

.method private initActionBar()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1250
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v1, "/slink"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v1, "/nearby"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1251
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1252
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 1253
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 1254
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v1, v2, v3, v5}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;I)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1256
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1271
    :goto_0
    return-void

    .line 1258
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/allview/SLinkActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 1262
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1263
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/ItemsActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {v1, v2, v3, v5}, Lcom/sec/samsung/gallery/view/ItemsActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/utils/EditModeHelper;I)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1265
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_0

    .line 1267
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1268
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/allview/AllActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0
.end method

.method private isCloudItemDeleted(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 9
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 1194
    const/4 v7, 0x0

    .line 1195
    .local v7, "res":Z
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    .line 1198
    .local v2, "proj":[Ljava/lang/String;
    const-string v3, "_id = ?"

    .line 1199
    .local v3, "selection":Ljava/lang/String;
    new-array v4, v8, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 1203
    .local v4, "args":[Ljava/lang/String;
    const/4 v6, 0x0

    .line 1204
    .local v6, "c":Landroid/database/Cursor;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 1205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->mBaseImageUri:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1209
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1210
    :cond_1
    const/4 v7, 0x1

    .line 1211
    :cond_2
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 1212
    return v7

    .line 1206
    :cond_3
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 1207
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;->mBaseVideoUri:Landroid/net/Uri;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    goto :goto_0
.end method

.method private refreshNearbyData()V
    .locals 4

    .prologue
    .line 1344
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object v1, v0

    .line 1346
    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 1347
    .local v1, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->initFlatBrowseFlag()V

    .line 1348
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->refreshItem()V

    .line 1350
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->resetAdapter()V

    .line 1351
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->reloadData()V

    .line 1353
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 1354
    return-void
.end method

.method private requestBrowsing()V
    .locals 4

    .prologue
    .line 809
    sget-object v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v3, "requestBrowsing"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 811
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v2, v0, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v2, :cond_1

    move-object v1, v0

    .line 812
    check-cast v1, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 813
    .local v1, "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->initFlatBrowseFlag()V

    .line 814
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->refreshItem()V

    .line 821
    .end local v1    # "nearbyDevice":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->resetAdapter()V

    .line 822
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->reloadData()V

    .line 823
    return-void

    .line 815
    :cond_1
    instance-of v2, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v2, :cond_0

    .line 816
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    if-nez v2, :cond_2

    .line 817
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 819
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->refresh(Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_0
.end method

.method private setItemFocus(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x0

    .line 510
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getGenericMotionFocus()I

    move-result v0

    .line 511
    .local v0, "genericMotionFocus":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setGenericMotionFocus(I)V

    .line 512
    if-ltz v0, :cond_0

    .line 513
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->notifyDataSetChanged(II)V

    .line 515
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->notifyDataSetChanged(II)V

    .line 516
    return-void
.end method

.method private showDisconnectedDialog(Ljava/lang/String;)V
    .locals 5
    .param p1, "deviceName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 1357
    sget-object v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "showDmsDisconnected!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1358
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z

    .line 1359
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 1360
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1361
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x1010355

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setIconAttribute(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e013a

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e0129

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/view/allview/AllViewState$20;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$20;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1372
    :cond_0
    return-void
.end method

.method private startDetailView(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 960
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 961
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 971
    :goto_0
    return-void

    .line 964
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 965
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_MEDIA_SET_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 966
    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 968
    const-string v2, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 969
    const-string v2, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 970
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startNoItemViewState()V
    .locals 3

    .prologue
    .line 1144
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 1146
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1147
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1148
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/allview/AllViewState$18;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$18;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1153
    return-void
.end method

.method private updateCountOnActionBar()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1122
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 1124
    .local v1, "selectedCount":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1125
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 1126
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCount()I

    move-result v4

    if-ne v3, v4, :cond_0

    const/4 v0, 0x1

    .line 1128
    .local v0, "isAllSelected":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v4, v2, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1129
    return-void

    .end local v0    # "isAllSelected":Z
    :cond_0
    move v0, v2

    .line 1126
    goto :goto_0
.end method

.method private updateSortByOptionMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0291

    const v5, 0x7f0f0290

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 883
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFunctionSimplification:Z

    if-eqz v1, :cond_0

    .line 893
    :goto_0
    return-void

    .line 884
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v2, "sort_by_type_ALL"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    .line 886
    .local v0, "type":I
    if-nez v0, :cond_1

    .line 887
    invoke-static {p1, v5, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 888
    invoke-static {p1, v6, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 890
    :cond_1
    invoke-static {p1, v5, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 891
    invoke-static {p1, v6, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method


# virtual methods
.method public exitSelectionMode()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1000
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1011
    :goto_0
    return-void

    .line 1002
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 1003
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->clearSelectedCount()V

    .line 1004
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(Ljava/lang/String;)V

    .line 1005
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->dismissPopupMenu()V

    .line 1006
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->initActionBar()V

    .line 1007
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 1008
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    .line 1009
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->reloadBurstShotImageData(I)V

    .line 1010
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mIsDeleteMode:Z

    goto :goto_0
.end method

.method public getContentsForDetailsDialog()Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1275
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1276
    .local v2, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-nez v11, :cond_0

    .line 1277
    sget-object v11, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v16, "mSelectionModeProxy is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1322
    .end local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .local v3, "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    return-object v3

    .line 1281
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v10

    .line 1282
    .local v10, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-nez v10, :cond_1

    .line 1283
    sget-object v11, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v16, "mediaList is null"

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v3, v2

    .line 1284
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 1287
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_1
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v9

    .line 1290
    .local v9, "mediaCount":I
    const/4 v11, 0x1

    if-ne v9, v11, :cond_3

    .line 1291
    const/4 v11, 0x0

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1292
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_2

    .line 1293
    new-instance v11, Lcom/sec/samsung/gallery/controller/DetailModel;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getDetails()Lcom/sec/android/gallery3d/data/MediaDetails;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-direct {v11, v0, v1}, Lcom/sec/samsung/gallery/controller/DetailModel;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaDetails;)V

    invoke-virtual {v11}, Lcom/sec/samsung/gallery/controller/DetailModel;->toStringList()Ljava/util/ArrayList;

    move-result-object v2

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    :goto_1
    move-object v3, v2

    .line 1322
    .restart local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    goto :goto_0

    .line 1296
    .end local v3    # "contentList":Ljava/lang/Object;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_3
    const-string v7, ""

    .line 1297
    .local v7, "line1":Ljava/lang/String;
    const-string v8, ""

    .line 1298
    .local v8, "line2":Ljava/lang/String;
    const-wide/16 v14, 0x0

    .line 1299
    .local v14, "sizeSummary":J
    const-wide/16 v12, 0x0

    .line 1302
    .local v12, "size":D
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_2
    if-ge v5, v9, :cond_5

    .line 1303
    :try_start_0
    invoke-interface {v10, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1304
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J

    move-result-wide v16

    const-wide/16 v18, 0x0

    cmp-long v11, v16, v18

    if-lez v11, :cond_4

    .line 1305
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaObject;->getSize()J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v16

    add-long v14, v14, v16

    .line 1302
    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 1308
    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :catch_0
    move-exception v4

    .line 1309
    .local v4, "e":Ljava/lang/Exception;
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V

    .line 1312
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->doubleValue()D

    move-result-wide v12

    .line 1313
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v16, 0x7f0e01d0

    move/from16 v0, v16

    invoke-virtual {v11, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    move-object/from16 v0, v16

    invoke-static {v11, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 1315
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e01d1

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v16, ": "

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-static {v0, v12, v13}, Lcom/sec/android/gallery3d/util/GalleryUtils;->formatItemsSelectedSize(Landroid/content/Context;D)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1318
    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1319
    const-wide/16 v16, 0x0

    cmpl-double v11, v12, v16

    if-lez v11, :cond_2

    .line 1320
    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method protected handleFilter()V
    .locals 5

    .prologue
    .line 936
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->cancelSwitchFilterTask()V

    .line 937
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/DataPath;->getOneAlbumPath(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Ljava/lang/String;

    move-result-object v0

    .line 938
    .local v0, "topPath":Ljava/lang/String;
    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$15;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, p0, v2, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$15;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    .line 946
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSwitchFilterTask:Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/sec/android/gallery3d/data/MediaSet;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/utils/MediaCountCheckTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 947
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-nez v0, :cond_0

    .line 741
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 743
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 744
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->exitSelectionMode()V

    .line 748
    :goto_0
    return-void

    .line 746
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->finishCurrentViewState()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 897
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 899
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 900
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 903
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_1

    .line 904
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 905
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 907
    :cond_1
    return-void
.end method

.method public onContentDirty()V
    .locals 5

    .prologue
    .line 1327
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v4, "/nearby"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v4, "/slink"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1328
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1329
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->getTitle()Ljava/lang/String;

    move-result-object v2

    .line 1331
    .local v2, "title":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1332
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v1

    .line 1334
    .local v1, "newTitle":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1341
    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v1    # "newTitle":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_1
    :goto_0
    return-void

    .line 1338
    .restart local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .restart local v1    # "newTitle":Ljava/lang/String;
    .restart local v2    # "title":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/16 v2, 0x30

    const/4 v10, 0x1

    const/4 v3, 0x0

    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f00bf

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 274
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 275
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 278
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getCacheSize()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 281
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v1, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 283
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getTopSetPath(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAllViewMediaRemovedMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 287
    const-wide/16 v4, 0x0

    .line 288
    .local v4, "mediaItemAdapterAttr":J
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-eqz v0, :cond_1

    .line 289
    const-wide/16 v4, 0x1

    .line 292
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setNewAlbumMode(Z)V

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v10}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setDisplayMediaIcon(Z)V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v10}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setConnectedDevice(Z)V

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 299
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 301
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAirJump:Z

    if-eqz v0, :cond_2

    .line 302
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;-><init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 310
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(Ljava/lang/String;)V

    .line 312
    if-eqz p1, :cond_3

    const-string v0, "is_refresh"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 313
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->requestBrowsing()V

    .line 315
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v7

    .line 316
    .local v7, "manager":Lcom/sec/android/gallery3d/data/DataManager;
    const-string v0, "nearby"

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getSource(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSource;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;

    .line 317
    .local v9, "nearbySource":Lcom/sec/android/gallery3d/remote/nearby/NearbySource;
    if-nez v9, :cond_4

    .line 337
    :goto_0
    return-void

    .line 320
    :cond_4
    :try_start_0
    invoke-virtual {v9}, Lcom/sec/android/gallery3d/remote/nearby/NearbySource;->getNearbyContext()Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyContext;->getNearbyClient()Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mNearbyClient:Lcom/sec/android/gallery3d/remote/nearby/NearbyClient;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 325
    :goto_1
    instance-of v0, v8, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v0, :cond_5

    move-object v0, v8

    .line 326
    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->registerContentObserver()V

    .line 329
    :cond_5
    invoke-virtual {v8, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 331
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_6

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->setSyncPriority()V

    .line 336
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mContentResolver:Landroid/content/ContentResolver;

    goto :goto_0

    .line 321
    :catch_0
    move-exception v6

    .line 322
    .local v6, "e":Ljava/lang/NullPointerException;
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v1, "NullPointerException at nearbySource.getNearbyContext().getNearbyClient()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 670
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mMenu:Landroid/view/Menu;

    .line 671
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 650
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->onStop()V

    .line 651
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    if-eqz v1, :cond_0

    .line 652
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V

    .line 653
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_1

    .line 654
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->destoryAirMotionDetector()V

    .line 657
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 658
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 660
    instance-of v1, v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v1, :cond_2

    .line 661
    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->unregisterContentObserver()V

    .line 664
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "ALL_VIEW_MEDIA_REMOVED"

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 665
    return-void
.end method

.method public onMWLayoutChanged()V
    .locals 2

    .prologue
    .line 951
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 952
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    .line 953
    :cond_0
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 710
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    const-string v3, "/slink"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 711
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isModalDownloadNeeded(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 712
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->enqueueMenuItemAfterModalDownload(I)V

    .line 713
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaItemArrayList()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getModalDownloadIntent(Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v1

    .line 714
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 715
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->exitSelectionMode()V

    .line 717
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v3, 0x30b

    invoke-virtual {v2, v1, v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 736
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 718
    .restart local v1    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 719
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 725
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    const v3, 0x102002c

    if-ne v2, v3, :cond_3

    .line 726
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 727
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->onBackPressed()V

    goto :goto_0

    .line 732
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 733
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 622
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 623
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause()V

    .line 624
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V

    .line 625
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getSplitScroll()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSplitScroll:F

    .line 627
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getThumbScroll()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mThumbScroll:F

    .line 628
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I

    .line 630
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->pause()V

    .line 632
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v0, :cond_1

    .line 633
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->onPause()V

    .line 634
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->setFocusedIndex(I)V

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "ALL_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 638
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_2

    .line 639
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mConnectedDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 642
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_3

    .line 643
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 645
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->cancelSwitchFilterTask()V

    .line 646
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 9
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v8, 0x7f0f026e

    const/4 v7, 0x0

    .line 675
    const-wide/16 v4, 0x0

    .line 677
    .local v4, "supported":J
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 678
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    .line 679
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v4

    .line 682
    :cond_0
    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 683
    const v6, 0x7f0f0292

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 685
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 686
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateSortByOptionMenu(Landroid/view/Menu;)V

    .line 687
    instance-of v6, v2, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v6, :cond_3

    .line 688
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 689
    const v6, 0x7f0f0283

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 690
    const v6, 0x7f0f0290

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 691
    const v6, 0x7f0f0291

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 696
    :cond_1
    :goto_0
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mIsDeleteMode:Z

    if-eqz v6, :cond_4

    .line 697
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_1
    if-ge v0, v3, :cond_4

    .line 698
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 699
    .local v1, "item":Landroid/view/MenuItem;
    invoke-interface {v1}, Landroid/view/MenuItem;->getItemId()I

    move-result v6

    if-eq v6, v8, :cond_2

    .line 700
    invoke-interface {v1, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 697
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 692
    .end local v0    # "i":I
    .end local v1    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_3
    instance-of v6, v2, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v6, :cond_1

    .line 693
    invoke-static {p1, v8, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_0

    .line 704
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v6, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 705
    return-void
.end method

.method public onResume()V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 520
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/app/FilterUtils;->toClusterType(Lcom/sec/samsung/gallery/core/TabTagType;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->setCurrentClusterType(I)V

    .line 521
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "sort_by_type_ALL"

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v10

    .line 523
    .local v10, "type":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/data/MediaSet;->setSortByType(I)V

    .line 524
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 526
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_0

    .line 527
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleBackgroundDeletion()V

    .line 529
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$14;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$14;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V

    .line 563
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 564
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->loadContentData(Z)V

    .line 565
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->onResume()V

    .line 567
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mCurrentMediaItemIndex:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSplitScroll:F

    iget v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mThumbScroll:F

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;-><init>(Landroid/content/Context;IIFF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    .line 569
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setExpandable(Z)V

    .line 570
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    .line 571
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mKeyboardRightFocused:Z

    .line 572
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const-string v1, "allViewMode"

    iput-object v1, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSPKeyString:Ljava/lang/String;

    .line 573
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 574
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setMode(IILjava/lang/Object;)V

    .line 577
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 579
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAllViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 580
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->initActionBar()V

    .line 582
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v9

    .line 584
    .local v9, "selectedCount":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCount()I

    move-result v0

    if-ne v9, v0, :cond_6

    .line 585
    .local v7, "isSelectAll":Z
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v0, v1, v2, v7}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 586
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 588
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v0, :cond_2

    .line 589
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 596
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_7

    .line 597
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_3

    .line 598
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->getActionBarTitle(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(Ljava/lang/String;)V

    .line 604
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    .line 605
    .local v8, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v8, :cond_4

    instance-of v0, v8, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    if-eqz v0, :cond_4

    move-object v0, v8

    check-cast v0, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->isConnect()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->bShowDisconnectDialog:Z

    if-nez v0, :cond_4

    .line 606
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v1, "Slink device is disconnect by onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->showDisconnectedDialog(Ljava/lang/String;)V

    .line 608
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartGalleryViewCmd;->getLastestActivityState(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v6

    .line 609
    .local v6, "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v6, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 612
    .end local v6    # "activityState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :cond_4
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v0, :cond_5

    .line 613
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/sec/android/gallery3d/remote/slink/SLinkClient;->CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mConnectedDevicesObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 617
    :cond_5
    new-instance v0, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewBeam;->setBeamListener()V

    .line 618
    return-void

    .end local v7    # "isSelectAll":Z
    .end local v8    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_6
    move v7, v2

    .line 584
    goto/16 :goto_0

    .line 601
    .restart local v7    # "isSelectAll":Z
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    goto :goto_1
.end method

.method protected onStateResult(IILandroid/content/Intent;)V
    .locals 12
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v10, -0x1

    .line 827
    const/4 v6, -0x1

    if-eq p2, v6, :cond_1

    .line 828
    sget-object v6, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v7, "activity result was not ok"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 872
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    const/16 v6, 0x30b

    if-ne p1, v6, :cond_0

    .line 833
    sget-object v6, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SLink Download Complete "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 834
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v6

    invoke-virtual {v6, p3}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getLocalUrisFromSuccessfulModalDownloadResult(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v1

    .line 837
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Landroid/net/Uri;>;"
    const/4 v2, 0x0

    .line 838
    .local v2, "mediaStoreUris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    instance-of v6, v1, Ljava/util/ArrayList;

    if-eqz v6, :cond_2

    move-object v2, v1

    .line 839
    check-cast v2, Ljava/util/ArrayList;

    .line 842
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->pollItemAfterModalDownload()I

    move-result v0

    .line 843
    .local v0, "itemId":I
    if-nez v0, :cond_3

    .line 844
    sget-object v6, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v7, "no action after modal download"

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 848
    :cond_3
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 851
    :sswitch_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->startChooser(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/List;)V

    goto :goto_0

    .line 855
    :sswitch_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v7, "image/*"

    invoke-static {v6, v2, v7}, Lcom/sec/samsung/gallery/controller/LastShareAppCmd;->launchLastShareApp(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0

    .line 858
    :sswitch_2
    const-string v6, "deviceId"

    invoke-virtual {p3, v6, v10, v11}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 859
    .local v4, "selectedDeviceId":J
    cmp-long v6, v4, v10

    if-eqz v6, :cond_0

    .line 862
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getSelectedItemIds()[J

    move-result-object v7

    invoke-static {v6, v7, v4, v5}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->transferFiles(Landroid/content/Context;[JJ)V

    goto :goto_0

    .line 865
    .end local v4    # "selectedDeviceId":J
    :sswitch_3
    const/4 v6, 0x2

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v3, v6

    const/4 v6, 0x1

    aput-object v2, v3, v6

    .line 866
    .local v3, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "S_STUDIO"

    invoke-virtual {v6, v7, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 848
    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_1
        0x7f0f0283 -> :sswitch_3
        0x7f0f02ad -> :sswitch_2
    .end sparse-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 359
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V

    .line 398
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$7;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V

    .line 413
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$8;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$8;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$9;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$9;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V

    .line 433
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$10;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V

    .line 451
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$11;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;)V

    .line 476
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnDropItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;)V

    .line 490
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    new-instance v1, Lcom/sec/samsung/gallery/view/allview/AllViewState$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$13;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V

    .line 507
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 975
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 976
    .local v0, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v1, v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    if-eqz v1, :cond_0

    .line 977
    if-eqz p1, :cond_1

    .line 978
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 979
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mGlAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    .line 987
    :cond_0
    :goto_0
    return-void

    .line 982
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    if-eqz v1, :cond_0

    .line 983
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAirMotionDetector:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V

    goto :goto_0
.end method

.method protected reloadBurstShotImageData(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 1156
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-nez v3, :cond_1

    .line 1187
    :cond_0
    :goto_0
    return-void

    .line 1159
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v3, :cond_0

    .line 1162
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    if-nez v3, :cond_2

    .line 1163
    sget-object v3, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v4, "photoView is null! reloadBurstShotImageData"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1167
    :cond_2
    const-wide/16 v0, 0x0

    .line 1168
    .local v0, "groupId":J
    const/4 v3, -0x1

    if-ne p1, v3, :cond_3

    .line 1169
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->groupAllBurstImages()V

    goto :goto_0

    .line 1170
    :cond_3
    const/4 v3, -0x2

    if-ne p1, v3, :cond_4

    .line 1171
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isCameraSetContainsBrustShot()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isAllBurstImagesUngroup()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1174
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->unGroupAllBurstImages()V

    goto :goto_0

    .line 1176
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 1177
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_0

    .line 1178
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v0

    .line 1179
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    .line 1180
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mMaintainScroll:Z

    .line 1181
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->getSelectedItemPosition()I

    move-result v4

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mItemFocused:I

    .line 1182
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->addToUngroupSet(J)V

    goto :goto_0
.end method

.method public selectAll(Z)V
    .locals 11
    .param p1, "selectAll"    # Z

    .prologue
    const/4 v10, 0x0

    .line 1065
    const/4 v5, 0x1

    .line 1066
    .local v5, "refresh":Z
    if-eqz p1, :cond_6

    .line 1067
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCurrentCacheSize()I

    move-result v0

    .line 1068
    .local v0, "count":I
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-eqz v6, :cond_2

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getIntrinsicCount()I

    move-result v4

    .line 1069
    .local v4, "realCount":I
    :goto_0
    const/16 v6, 0x3e8

    if-gt v4, v6, :cond_0

    if-ge v0, v4, :cond_3

    .line 1070
    :cond_0
    const/4 v5, 0x0

    .line 1071
    new-instance v6, Lcom/sec/samsung/gallery/view/allview/AllViewState$SelectionTask;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v8, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState$17;-><init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v9

    invoke-direct {v6, v7, v8, v9}, Lcom/sec/samsung/gallery/view/allview/AllViewState$SelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/allview/AllViewState$SelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1111
    :goto_1
    const/4 v6, -0x2

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->reloadBurstShotImageData(I)V

    .line 1116
    .end local v0    # "count":I
    .end local v4    # "realCount":I
    :goto_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1117
    if-eqz v5, :cond_1

    .line 1118
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->refreshView(Z)V

    .line 1119
    :cond_1
    return-void

    .line 1068
    .restart local v0    # "count":I
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCount()I

    move-result v4

    goto :goto_0

    .line 1096
    .restart local v4    # "realCount":I
    :cond_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAllviewBurstshot:Z

    if-eqz v6, :cond_5

    .line 1097
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getIntrItemsList()Ljava/util/List;

    move-result-object v1

    .line 1098
    .local v1, "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v1, :cond_1

    .line 1100
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 1101
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Ljava/util/List;)V

    .line 1109
    .end local v1    # "list":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateCountOnActionBar()V

    goto :goto_1

    .line 1103
    :cond_5
    const/4 v3, 0x0

    .local v3, "position":I
    :goto_3
    if-ge v3, v4, :cond_4

    .line 1104
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v6, v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 1105
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1106
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1103
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 1113
    .end local v0    # "count":I
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v3    # "position":I
    .end local v4    # "realCount":I
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 1114
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v6, v10}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_2
.end method

.method public selectMedia(I)V
    .locals 8
    .param p1, "position"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1025
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-nez v3, :cond_0

    .line 1026
    sget-object v3, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v4, "select item is ignored because adapter is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1062
    :goto_0
    return-void

    .line 1030
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 1032
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-nez v0, :cond_1

    .line 1033
    sget-object v3, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v4, "select item is ignored because media is null"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1037
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1038
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029e

    new-array v7, v2, [Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1039
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1040
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v5, 0x4e20

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1041
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1058
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getCount()I

    move-result v5

    if-ne v3, v5, :cond_7

    .line 1059
    .local v2, "selected":Z
    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1060
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v5, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v5, v4, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1061
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    goto :goto_0

    .line 1043
    .end local v2    # "selected":Z
    :cond_3
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v6, 0x7f0e029d

    new-array v7, v2, [Ljava/lang/Object;

    move-object v3, v0

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v4

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 1044
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1045
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v5, 0x4e2a

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1046
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isCameraSetContainsBrustShot()Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isAllBurstImagesUngroup()Z

    move-result v3

    if-nez v3, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-object v3, v0

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->isUngroup(J)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 1048
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1049
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 1051
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getIntrItems(I)Ljava/util/List;

    move-result-object v1

    .line 1052
    .local v1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 1053
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 1054
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->reloadBurstShotImageData(I)V

    goto/16 :goto_1

    .end local v1    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_7
    move v2, v4

    .line 1058
    goto :goto_2
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 12
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v11, 0x7

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 752
    move-object v7, p2

    check-cast v7, Lcom/sec/samsung/gallery/core/Event;

    .line 753
    .local v7, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v7}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v8

    .line 755
    .local v8, "eventType":I
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Event update ["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v8, v0, :cond_1

    .line 758
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mIsDeleteMode:Z

    .line 759
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->enterSelectionMode(Z)V

    .line 760
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateCountOnActionBar()V

    .line 806
    :cond_0
    :goto_0
    return-void

    .line 761
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SCAN_NEARBY_DEVICES:I

    if-ne v8, v0, :cond_2

    .line 763
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->scanForNearbyProviderDevices(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 764
    :catch_0
    move-exception v6

    .line 765
    .local v6, "e":Ljava/lang/NullPointerException;
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v2, "NullPointer Exception, maybe nearby device is not initiated yet"

    invoke-static {v0, v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 767
    .end local v6    # "e":Ljava/lang/NullPointerException;
    :cond_2
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_OLDEST:I

    if-ne v8, v0, :cond_3

    .line 768
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleSortByAction(I)V

    goto :goto_0

    .line 769
    :cond_3
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SORT_BY_LATEST:I

    if-ne v8, v0, :cond_4

    .line 770
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->handleSortByAction(I)V

    goto :goto_0

    .line 771
    :cond_4
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v8, v0, :cond_5

    .line 772
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectAll(Z)V

    goto :goto_0

    .line 773
    :cond_5
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v8, v0, :cond_6

    .line 774
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->selectAll(Z)V

    goto :goto_0

    .line 775
    :cond_6
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v8, v0, :cond_7

    .line 776
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->enterSelectionMode(Z)V

    .line 777
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 778
    :cond_7
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_TO_ALBUM:I

    if-ne v8, v0, :cond_8

    .line 779
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4, v11}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 781
    .local v1, "topLocalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 782
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 783
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZILjava/lang/String;)V

    goto :goto_0

    .line 786
    .end local v1    # "topLocalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_TO_ALBUM:I

    if-ne v8, v0, :cond_9

    .line 787
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v4, v11}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 789
    .restart local v1    # "topLocalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 790
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mTopSetPath:Ljava/lang/String;

    move v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZILjava/lang/String;)V

    goto :goto_0

    .line 792
    .end local v1    # "topLocalMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_9
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_SEND_TO_OTHER_DEVICES:I

    if-ne v8, v0, :cond_a

    .line 793
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    const v2, 0x7f0f02ad

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->enqueueMenuItemAfterModalDownload(I)V

    .line 794
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->setSelectedItemIds(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 795
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;

    .line 796
    .local v10, "slinkStorage":Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;->getStorageId()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSLinkManager:Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getSelectedItemIds()[J

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->createDeviceChooserActivityIntent(Landroid/content/Context;I[J)Landroid/content/Intent;

    move-result-object v9

    .line 798
    .local v9, "sintent":Landroid/content/Intent;
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x30b

    invoke-virtual {v0, v9, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 799
    :catch_1
    move-exception v6

    .line 800
    .local v6, "e":Landroid/content/ActivityNotFoundException;
    sget-object v0, Lcom/sec/samsung/gallery/view/allview/AllViewState;->TAG:Ljava/lang/String;

    const-string v2, "Activity Not Found"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 801
    invoke-virtual {v6}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_0

    .line 803
    .end local v6    # "e":Landroid/content/ActivityNotFoundException;
    .end local v9    # "sintent":Landroid/content/Intent;
    .end local v10    # "slinkStorage":Lcom/sec/android/gallery3d/remote/slink/SLinkStorage;
    :cond_a
    sget v0, Lcom/sec/samsung/gallery/core/Event;->EVENT_REFRESH_SLINK:I

    if-ne v8, v0, :cond_0

    .line 804
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->requestBrowsing()V

    goto/16 :goto_0
.end method
