.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$OnHoverControlListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1294
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public hoverClick(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)Z
    .locals 4
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "index"    # I

    .prologue
    const/4 v2, 0x0

    .line 1297
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-nez v3, :cond_1

    .line 1305
    :cond_0
    :goto_0
    return v2

    .line 1299
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->isEnabledQuickScroll()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1302
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v3, p1, p2, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setScreenNailImage(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)V

    .line 1303
    shr-int/lit8 v0, p3, 0x10

    .line 1304
    .local v0, "albumIndex":I
    const v2, 0xffff

    and-int v1, p3, v2

    .line 1305
    .local v1, "photoIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$13;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-interface {v2, v3, v0, v1, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;->onHoverClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;IILjava/lang/Object;)Z

    move-result v2

    goto :goto_0
.end method
