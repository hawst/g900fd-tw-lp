.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MusicEnabler"
.end annotation


# instance fields
.field private mCheckContext:Landroid/content/Context;

.field private mIsKeyEnter:Z

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;Landroid/content/Context;Z)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "isMusicEnabled"    # Z

    .prologue
    .line 597
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 595
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mIsKeyEnter:Z

    .line 598
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mCheckContext:Landroid/content/Context;

    .line 599
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 5
    .param p1, "compoundButton"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v4, 0x0

    .line 604
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mCheckContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$002(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .line 607
    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isPressed()Z

    move-result v0

    .line 608
    .local v0, "isPressed":Z
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 610
    .local v1, "music":Ljava/lang/String;
    if-eqz p2, :cond_1

    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mIsKeyEnter:Z

    if-nez v2, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 613
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->showMusicListDialog(Z)V

    .line 615
    :cond_1
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mIsKeyEnter:Z

    .line 616
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->setChecked(Z)V

    .line 619
    sget-boolean v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    if-eqz v2, :cond_2

    .line 620
    sput-boolean v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->LOAD_MUSIC_NONE_SELECTED:Z

    .line 622
    :cond_2
    return-void
.end method

.method public setChecked(Z)V
    .locals 3
    .param p1, "isChecked"    # Z

    .prologue
    .line 625
    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v0

    const/4 v1, 0x5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 627
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$MusicEnabler;->mCheckContext:Landroid/content/Context;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mSettings:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$000()Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V

    .line 628
    return-void
.end method
