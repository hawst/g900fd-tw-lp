.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;
.super Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;
.source "PhotoActionBarForHideItem.java"


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 23
    return-void
.end method

.method private isAvalableProceedHideItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 62
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v0, :cond_0

    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-nez v0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startCreateVideoAlbum(ZZZ)V
    .locals 3
    .param p1, "hide"    # Z
    .param p2, "show"    # Z
    .param p3, "delete"    # Z

    .prologue
    .line 57
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 58
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "HIDE_ALBUMS"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 59
    return-void
.end method


# virtual methods
.method public disablePopupMenu()V
    .locals 0

    .prologue
    .line 71
    return-void
.end method

.method public enablePopupMenu()V
    .locals 0

    .prologue
    .line 75
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0024

    if-ne v0, v1, :cond_1

    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, v0, v2, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->startCreateVideoAlbum(ZZZ)V

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x7f0f0022

    if-ne v0, v1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->onBackPressed()V

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0f0024

    const/4 v5, 0x0

    .line 27
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForMultiPick;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 28
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 29
    .local v3, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    .line 30
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x1

    .line 31
    .local v0, "bShowDone":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 32
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 33
    .restart local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_1

    .line 34
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 45
    :cond_0
    :goto_1
    return-void

    .line 38
    :cond_1
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForHideItem;->isAvalableProceedHideItem(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    .line 40
    if-nez v0, :cond_2

    .line 41
    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1

    .line 31
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
