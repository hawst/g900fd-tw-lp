.class Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;
.super Ljava/lang/Object;
.source "NoteActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/16 v2, 0x8

    .line 117
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 121
    .local v0, "buttonId":I
    const v1, 0x7f0f013f

    if-ne v0, v1, :cond_2

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverSettingVisibility(I)V

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    iget v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevButtonId:I

    if-ne v1, v0, :cond_1

    .line 125
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_PEN:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->toggleSettingViewVisibility(I)V

    .line 149
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    iput v0, v1, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevButtonId:I

    .line 150
    return-void

    .line 128
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenMode()V

    goto :goto_0

    .line 131
    :cond_2
    const v1, 0x7f0f0141

    if-ne v0, v1, :cond_4

    .line 132
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 133
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenSettingVisibility(I)V

    .line 134
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    iget v1, v1, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevButtonId:I

    if-ne v1, v0, :cond_3

    .line 135
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->TYPE_SETTING_REMOVER:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->toggleSettingViewVisibility(I)V

    goto :goto_0

    .line 138
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverMode()V

    goto :goto_0

    .line 141
    :cond_4
    const v1, 0x7f0f0144

    if-ne v0, v1, :cond_0

    .line 142
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenSettingVisibility(I)V

    .line 144
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setRemoverSettingVisibility(I)V

    .line 145
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$1;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setMovingMode()V

    goto :goto_0
.end method
