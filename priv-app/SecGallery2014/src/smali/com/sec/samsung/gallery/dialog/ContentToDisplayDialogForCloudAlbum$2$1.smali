.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;
.super Ljava/lang/Object;
.source "ContentToDisplayDialogForCloudAlbum.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

.field final synthetic val$cView:Landroid/view/View;

.field final synthetic val$colorOnClick:I

.field final synthetic val$position:I

.field final synthetic val$viewType:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;Landroid/view/View;III)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iput-object p2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$cView:Landroid/view/View;

    iput p3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$colorOnClick:I

    iput p4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$viewType:I

    iput p5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$position:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isAllItemsSelected()Z
    .locals 5

    .prologue
    .line 271
    const/4 v0, 0x1

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 272
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 273
    .local v2, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 275
    const/4 v3, 0x0

    .line 278
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :goto_1
    return v3

    .line 271
    .restart local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private updateListItem(Z)V
    .locals 8
    .param p1, "isAllSelected"    # Z

    .prologue
    .line 252
    const/4 v2, 0x1

    .local v2, "i":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v2, v3, :cond_3

    .line 253
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v6

    invoke-virtual {v6, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 254
    .local v0, "checkbox":Landroid/widget/CheckedTextView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 255
    .local v5, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    .line 257
    .local v4, "typeIndex":I
    if-eqz p1, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x1

    .line 258
    .local v1, "checked":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 259
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 262
    :cond_0
    if-eqz v1, :cond_2

    .line 263
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 252
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 257
    .end local v1    # "checked":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 265
    .restart local v1    # "checked":Z
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v6, v6, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    xor-int/lit8 v7, v4, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_2

    .line 268
    .end local v0    # "checkbox":Landroid/widget/CheckedTextView;
    .end local v1    # "checked":Z
    .end local v4    # "typeIndex":I
    .end local v5    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_3
    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 211
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .local v0, "action":I
    move-object v1, p1

    .line 212
    check-cast v1, Landroid/widget/CheckedTextView;

    .line 213
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-nez v0, :cond_1

    .line 214
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 215
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$cView:Landroid/view/View;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$colorOnClick:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 248
    :cond_0
    :goto_0
    return v4

    .line 216
    :cond_1
    const/4 v2, 0x3

    if-ne v0, v2, :cond_2

    .line 217
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$cView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 218
    :cond_2
    if-ne v0, v4, :cond_0

    .line 219
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$cView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 220
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->toggle()V

    .line 221
    const v2, 0x8000

    invoke-virtual {v1, v2}, Landroid/widget/CheckedTextView;->sendAccessibilityEvent(I)V

    .line 222
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 223
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$viewType:I

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 228
    :goto_1
    iget v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$position:I

    if-nez v2, :cond_4

    .line 230
    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v3, v3, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v2

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->updateListItem(Z)V

    .line 244
    :goto_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->updateSaveButton()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$600(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V

    .line 245
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mFormatListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$700(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_0

    .line 225
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    iget v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->val$viewType:I

    xor-int/lit8 v3, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_1

    .line 234
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->isAllItemsSelected()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 236
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 242
    :goto_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContentListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_2

    .line 238
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;->this$1:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;

    iget-object v2, v2, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    sget-object v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    xor-int/lit8 v3, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_3
.end method
