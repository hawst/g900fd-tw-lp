.class public Lcom/sec/samsung/gallery/view/help/HelpScrollView;
.super Landroid/widget/ScrollView;
.source "HelpScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;
    }
.end annotation


# instance fields
.field private mScrollListener:Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 11
    invoke-direct {p0, p1}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 12
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 13
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method


# virtual methods
.method public getMaxScroll()I
    .locals 2

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-super {p0}, Landroid/widget/ScrollView;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method protected onScrollChanged(IIII)V
    .locals 1
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "oldl"    # I
    .param p4, "oldt"    # I

    .prologue
    .line 25
    if-ge p2, p4, :cond_0

    .line 26
    invoke-virtual {p0, p4}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->setScrollY(I)V

    .line 28
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->mScrollListener:Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;

    if-eqz v0, :cond_1

    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->mScrollListener:Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;->onScrollChanged(II)V

    .line 31
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ScrollView;->onScrollChanged(IIII)V

    .line 32
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public setOnScrollChanged(Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->mScrollListener:Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;

    .line 36
    return-void
.end method
