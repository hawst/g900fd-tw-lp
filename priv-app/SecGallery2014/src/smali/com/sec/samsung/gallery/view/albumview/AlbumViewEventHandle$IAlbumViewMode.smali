.class interface abstract Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle$IAlbumViewMode;
.super Ljava/lang/Object;
.source "AlbumViewEventHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewEventHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "IAlbumViewMode"
.end annotation


# virtual methods
.method public abstract disableView()V
.end method

.method public abstract enableView()V
.end method

.method public abstract initializeView()V
.end method

.method public abstract onBackPressed()V
.end method

.method public abstract onHoverClick(Lcom/sec/android/gallery3d/data/MediaObject;)V
.end method

.method public abstract onItemClick(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlView;I)V
.end method
