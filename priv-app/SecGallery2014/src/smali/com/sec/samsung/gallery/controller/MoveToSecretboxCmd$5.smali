.class Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;
.super Ljava/lang/Object;
.source "MoveToSecretboxCmd.java"

# interfaces
.implements Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mErrorReason:Ljava/lang/String;

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

.field final synthetic val$albumPaths:Ljava/util/ArrayList;

.field final synthetic val$count:I

.field final synthetic val$itemCount:I

.field final synthetic val$newName:Ljava/lang/String;

.field final synthetic val$operationId:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IILjava/util/ArrayList;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 252
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iput p2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$itemCount:I

    iput p3, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$count:I

    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$albumPaths:Ljava/util/ArrayList;

    iput p5, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$operationId:I

    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$newName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->mErrorReason:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public cancelOperation()V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 362
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->onCancel(Landroid/content/DialogInterface;)V

    .line 364
    :cond_0
    return-void
.end method

.method public handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;)Z
    .locals 12
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "dstAlbum"    # Ljava/lang/String;

    .prologue
    .line 294
    const/4 v6, 0x0

    .line 295
    .local v6, "result":Z
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v8}, Landroid/net/Uri;->hashCode()I

    move-result v1

    .line 297
    .local v1, "key":I
    :try_start_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 298
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v9}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1300(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/util/FileUtil;

    move-result-object v9

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/FileUtil;->getSelectedMediaSize()Ljava/util/HashMap;

    move-result-object v9

    # setter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v8, v9}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1202(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 299
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v9

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v8

    sget-object v10, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalSize(J)V

    .line 301
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 302
    .local v2, "fileSize":J
    const/4 v7, 0x0

    .line 303
    .local v7, "statFs":Landroid/os/StatFs;
    if-eqz p2, :cond_2

    .line 304
    new-instance v7, Landroid/os/StatFs;

    .end local v7    # "statFs":Landroid/os/StatFs;
    invoke-direct {v7, p2}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 310
    .restart local v7    # "statFs":Landroid/os/StatFs;
    :goto_0
    invoke-virtual {v7}, Landroid/os/StatFs;->getFreeBlocks()I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {v7}, Landroid/os/StatFs;->getBlockSize()I

    move-result v10

    int-to-long v10, v10

    mul-long v4, v8, v10

    .line 311
    .local v4, "freeSize":J
    cmp-long v8, v2, v4

    if-lez v8, :cond_4

    .line 312
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/content/Context;

    move-result-object v8

    const v9, 0x7f0e0139

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->mErrorReason:Ljava/lang/String;

    .line 313
    const/4 v8, 0x0

    .line 342
    .end local v2    # "fileSize":J
    .end local v4    # "freeSize":J
    .end local v7    # "statFs":Landroid/os/StatFs;
    :goto_1
    return v8

    .line 305
    .restart local v2    # "fileSize":J
    .restart local v7    # "statFs":Landroid/os/StatFs;
    :cond_2
    iget v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$operationId:I

    if-nez v8, :cond_3

    .line 306
    new-instance v7, Landroid/os/StatFs;

    .end local v7    # "statFs":Landroid/os/StatFs;
    sget-object v8, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .restart local v7    # "statFs":Landroid/os/StatFs;
    goto :goto_0

    .line 308
    :cond_3
    new-instance v7, Landroid/os/StatFs;

    .end local v7    # "statFs":Landroid/os/StatFs;
    sget-object v8, Lcom/sec/android/gallery3d/util/MediaSetUtils;->RESTORE_PATH:Ljava/lang/String;

    invoke-direct {v7, v8}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v7    # "statFs":Landroid/os/StatFs;
    goto :goto_0

    .line 315
    .end local v2    # "fileSize":J
    .end local v7    # "statFs":Landroid/os/StatFs;
    :catch_0
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 317
    const/4 v8, 0x0

    goto :goto_1

    .line 319
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .restart local v2    # "fileSize":J
    .restart local v4    # "freeSize":J
    .restart local v7    # "statFs":Landroid/os/StatFs;
    :cond_4
    iget v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$operationId:I

    if-nez v8, :cond_9

    .line 320
    sget-object v8, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    if-eqz v8, :cond_5

    .line 321
    if-nez p2, :cond_8

    .line 322
    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSingleOperation:Z
    invoke-static {}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1600()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 323
    sget-object v8, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$newName:Ljava/lang/String;

    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaObject;->moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    .line 338
    :cond_5
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v8

    if-eqz v8, :cond_6

    .line 339
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v8

    const-wide/16 v10, 0x1

    const/4 v9, 0x0

    invoke-virtual {v8, v10, v11, v9}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    :cond_6
    move v8, v6

    .line 342
    goto :goto_1

    .line 325
    :cond_7
    sget-object v8, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaObject;->moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2

    .line 328
    :cond_8
    const/4 v8, 0x0

    invoke-virtual {p1, p2, v8}, Lcom/sec/android/gallery3d/data/MediaObject;->moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2

    .line 331
    :cond_9
    iget v8, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$operationId:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    .line 332
    if-nez p2, :cond_a

    .line 333
    sget-object v8, Lcom/sec/android/gallery3d/util/MediaSetUtils;->RESTORE_PATH:Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual {p1, v8, v9}, Lcom/sec/android/gallery3d/data/MediaObject;->moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2

    .line 335
    :cond_a
    const/4 v8, 0x0

    invoke-virtual {p1, p2, v8}, Lcom/sec/android/gallery3d/data/MediaObject;->moveToSecretbox(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v6

    goto :goto_2
.end method

.method public onCompleted(I)V
    .locals 13
    .param p1, "failedCount"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v6, 0x1

    .line 256
    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "onCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v0

    iget v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$itemCount:I

    int-to-long v4, v1

    const/4 v1, 0x0

    invoke-virtual {v0, v4, v5, v1, v6}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;

    move-result-object v0

    sget-object v2, Lcom/sec/samsung/gallery/util/FileUtil;->TOTAL_FILE_SIZE_KEY:Ljava/lang/Integer;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5, v6, v6}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZZ)V

    .line 262
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;
    invoke-static {v0, v12}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1202(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/util/HashMap;)Ljava/util/HashMap;

    .line 263
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;
    invoke-static {v0, v12}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1302(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;

    .line 264
    iget v3, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$count:I

    .line 265
    .local v3, "successAlbumsCount":I
    const/4 v10, 0x0

    .line 266
    .local v10, "secretAlbumsCount":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$albumPaths:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$albumPaths:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 268
    .local v11, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v11, :cond_3

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$albumPaths:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    .line 270
    .local v8, "filePath":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$100(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 271
    add-int/lit8 v10, v10, 0x1

    .line 273
    :cond_1
    new-instance v7, Ljava/io/File;

    invoke-direct {v7, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 274
    .local v7, "file":Ljava/io/File;
    invoke-virtual {v7}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v7}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_2

    .line 275
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    .line 268
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 278
    .end local v7    # "file":Ljava/io/File;
    .end local v8    # "filePath":Ljava/lang/String;
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$count:I

    sub-int v3, v0, v10

    .line 280
    .end local v9    # "i":I
    .end local v11    # "size":I
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->dismissDialog()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1400(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)V

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$800(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$700(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$800(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 286
    :cond_5
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v4, 0x64

    invoke-virtual {v0, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 289
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$operationId:I

    iget v4, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$count:I

    iget v5, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->val$itemCount:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->mErrorReason:Ljava/lang/String;

    move v2, p1

    # invokes: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->showToast(IIIIILjava/lang/String;)V
    invoke-static/range {v0 .. v6}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1500(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IIIIILjava/lang/String;)V

    .line 290
    return-void
.end method

.method public onProgress(II)V
    .locals 4
    .param p1, "progress"    # I
    .param p2, "flag"    # I

    .prologue
    .line 354
    const/4 v1, -0x1

    if-ne p2, v1, :cond_1

    const/4 v0, 0x1

    .line 355
    .local v0, "isUpdateFileSize":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 356
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3, v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 357
    :cond_0
    return-void

    .line 354
    .end local v0    # "isUpdateFileSize":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
