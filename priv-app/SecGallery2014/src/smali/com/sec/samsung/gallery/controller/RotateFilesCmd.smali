.class public Lcom/sec/samsung/gallery/controller/RotateFilesCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "RotateFilesCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field mAngle:I

.field mContext:Landroid/content/Context;

.field mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field mOperationId:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private handleRotateMedias(I)V
    .locals 9
    .param p1, "rotateAngle"    # I

    .prologue
    const/4 v3, 0x0

    .line 45
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    .line 46
    .local v8, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v1, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    invoke-direct {v1, v4}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 47
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v6

    .line 48
    .local v6, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    const/4 v1, 0x1

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>(II)V

    .line 51
    .local v0, "progressDialogHelper":Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;
    if-gez p1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0055

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 56
    .local v2, "title":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    const/4 v4, 0x0

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 57
    new-instance v7, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;

    invoke-direct {v7, p0, v0, v6}, Lcom/sec/samsung/gallery/controller/RotateFilesCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/RotateFilesCmd;Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 83
    .local v7, "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v1, v3, v7}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 84
    return-void

    .line 54
    .end local v2    # "title":Ljava/lang/String;
    .end local v7    # "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0056

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "title":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 38
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 39
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mAngle:I

    .line 40
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    .line 41
    iget v1, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mAngle:I

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->handleRotateMedias(I)V

    .line 42
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/FileUtil;->cancelOperation()V

    .line 90
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RotateFilesCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "EXIT_SELECTION_MODE"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 93
    return-void
.end method
