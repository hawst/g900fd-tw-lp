.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;
.super Ljava/lang/Object;
.source "GlComposeView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ScaleControl"
.end annotation


# instance fields
.field protected mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field protected mFromLevel:I

.field protected mScale:F

.field protected mScrollDelta:F

.field protected mToLevel:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1647
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected doScale(FF)Z
    .locals 12
    .param p1, "scale"    # F
    .param p2, "ratio"    # F

    .prologue
    .line 1721
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 1722
    .local v2, "fromLevel":I
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 1723
    .local v8, "toLevel":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    .line 1724
    .local v3, "level":I
    const/4 v4, 0x0

    .line 1725
    .local v4, "levelChanged":Z
    const/4 v5, 0x0

    .line 1727
    .local v5, "srcLevelChanged":Z
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMinLevel:I

    .line 1728
    .local v1, "actualMinLvl":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    .line 1729
    .local v0, "actualMaxLvl":I
    sget-boolean v9, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-eqz v9, :cond_1

    .line 1730
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->isTimeViewStateConfig()Z

    move-result v9

    if-nez v9, :cond_0

    .line 1731
    add-int/lit8 v1, v1, 0x1

    .line 1732
    :cond_0
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget-boolean v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    if-eqz v9, :cond_1

    .line 1733
    add-int/lit8 v0, v0, -0x1

    .line 1736
    :cond_1
    sget v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER:F

    cmpg-float v9, p1, v9

    if-gtz v9, :cond_7

    .line 1737
    if-le v3, v1, :cond_6

    .line 1738
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v10, -0x1

    invoke-virtual {v9, v3, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getNextLevel(II)I

    move-result v2

    .line 1739
    add-int/lit8 v8, v2, -0x1

    .line 1740
    if-ge v8, v1, :cond_2

    move v8, v1

    .line 1745
    :cond_2
    :goto_0
    const/high16 p1, 0x3f800000    # 1.0f

    .line 1771
    :cond_3
    :goto_1
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    if-eq v2, v9, :cond_4

    .line 1772
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 1773
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mLayoutConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    const/4 v10, 0x0

    invoke-virtual {v9, v2, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->setLevel(IZ)V

    .line 1774
    const/4 v5, 0x1

    .line 1775
    const/4 v4, 0x1

    .line 1777
    :cond_4
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    if-eq v8, v9, :cond_5

    .line 1778
    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    .line 1779
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mLayoutConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;

    const/4 v10, 0x1

    invoke-virtual {v9, v8, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$LayoutConfiguration;->setLevel(IZ)V

    .line 1780
    const/4 v4, 0x1

    .line 1782
    :cond_5
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    cmpl-float v9, v9, p1

    if-nez v9, :cond_e

    if-nez v4, :cond_e

    .line 1783
    const/4 v5, 0x0

    .line 1809
    .end local v5    # "srcLevelChanged":Z
    :goto_2
    return v5

    .line 1742
    .restart local v5    # "srcLevelChanged":Z
    :cond_6
    move v2, v1

    .line 1743
    move v8, v1

    goto :goto_0

    .line 1746
    :cond_7
    sget v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER:F

    cmpl-float v9, p1, v9

    if-ltz v9, :cond_a

    .line 1747
    if-ge v3, v0, :cond_9

    .line 1748
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v10, 0x1

    invoke-virtual {v9, v3, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getNextLevel(II)I

    move-result v2

    .line 1749
    add-int/lit8 v8, v2, 0x1

    .line 1750
    if-le v8, v0, :cond_8

    move v8, v0

    .line 1755
    :cond_8
    :goto_3
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_1

    .line 1752
    :cond_9
    move v2, v0

    .line 1753
    move v8, v0

    goto :goto_3

    .line 1756
    :cond_a
    const/high16 v9, 0x3f800000    # 1.0f

    cmpl-float v9, p1, v9

    if-lez v9, :cond_c

    .line 1757
    if-ge v3, v0, :cond_b

    .line 1758
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v10, 0x1

    invoke-virtual {v9, v3, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getNextLevel(II)I

    move-result v8

    goto :goto_1

    .line 1760
    :cond_b
    move v8, v0

    .line 1761
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_1

    .line 1763
    :cond_c
    const/high16 v9, 0x3f800000    # 1.0f

    cmpg-float v9, p1, v9

    if-gez v9, :cond_3

    .line 1764
    if-le v3, v1, :cond_d

    .line 1765
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/4 v10, -0x1

    invoke-virtual {v9, v3, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getNextLevel(II)I

    move-result v8

    goto :goto_1

    .line 1767
    :cond_d
    move v8, v1

    .line 1768
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_1

    .line 1784
    :cond_e
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    .line 1785
    if-eqz v4, :cond_13

    .line 1786
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v9, :cond_f

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    if-gez v9, :cond_10

    .line 1787
    :cond_f
    const-string v9, "GlComposeView"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "minus index. ignore, mFocusedObj = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1788
    const/4 v5, 0x0

    goto :goto_2

    .line 1790
    :cond_10
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v9

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScrollDelta:F

    sub-float v6, v9, v10

    .line 1791
    .local v6, "srcScrl":F
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v9

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScrollDelta:F

    sub-float v7, v9, v10

    .line 1792
    .local v7, "tgtScrl":F
    const/4 v9, 0x0

    cmpg-float v9, v6, v9

    if-gez v9, :cond_14

    const/4 v6, 0x0

    .line 1795
    :cond_11
    :goto_4
    const/4 v9, 0x0

    cmpg-float v9, v7, v9

    if-gez v9, :cond_15

    const/4 v7, 0x0

    .line 1799
    :cond_12
    :goto_5
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v9, v6, v7}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScrollRef(FF)V

    .line 1800
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->resetVisibleObjectAttribute()V

    .line 1801
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v9, v6}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1802
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v11, v11, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v11, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    invoke-virtual {v9, v10, v11}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 1804
    .end local v6    # "srcScrl":F
    .end local v7    # "tgtScrl":F
    :cond_13
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    const/high16 v10, 0x3f800000    # 1.0f

    cmpl-float v9, v9, v10

    if-ltz v9, :cond_16

    .line 1805
    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v9, v10

    sget v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_UPPER_DELTA:F

    div-float p2, v9, v10

    .line 1808
    :goto_6
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v9, p2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->interpolateVisibleObjectPosition(F)V

    goto/16 :goto_2

    .line 1793
    .restart local v6    # "srcScrl":F
    .restart local v7    # "tgtScrl":F
    :cond_14
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v9, v6, v9

    if-lez v9, :cond_11

    .line 1794
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v6, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_4

    .line 1796
    :cond_15
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    cmpl-float v9, v7, v9

    if-lez v9, :cond_12

    .line 1797
    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlNext:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v7, v9, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollableMax:F

    goto :goto_5

    .line 1807
    .end local v6    # "srcScrl":F
    .end local v7    # "tgtScrl":F
    :cond_16
    const/high16 v9, 0x3f800000    # 1.0f

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    sub-float/2addr v9, v10

    sget v10, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->SCALE_LOWER_DELTA:F

    div-float p2, v9, v10

    goto :goto_6
.end method

.method protected isAnimationAvailable(FF)Z
    .locals 4
    .param p1, "fromScale"    # F
    .param p2, "toScale"    # F

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1700
    cmpl-float v2, p1, p2

    if-nez v2, :cond_1

    .line 1713
    :cond_0
    :goto_0
    return v0

    .line 1702
    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, p2, v2

    if-nez v2, :cond_2

    move v0, v1

    .line 1703
    goto :goto_0

    .line 1705
    :cond_2
    cmpg-float v2, p1, p2

    if-gez v2, :cond_3

    .line 1706
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    if-ge v2, v3, :cond_0

    .line 1709
    :cond_3
    cmpl-float v2, p1, p2

    if-lez v2, :cond_4

    .line 1710
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    .line 1713
    goto :goto_0
.end method

.method protected isMaxLevel()Z
    .locals 2

    .prologue
    .line 1717
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mMaxLevel:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected prepareScale(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 4
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    .line 1680
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    .line 1683
    .local v0, "level":I
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    .line 1684
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 1685
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    .line 1686
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1687
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 1688
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v1

    .line 1689
    .local v1, "scroll":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    sub-float v2, v1, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScrollDelta:F

    .line 1690
    return-void
.end method

.method protected prepareScale(II)Z
    .locals 6
    .param p1, "focusX"    # I
    .param p2, "focusY"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1655
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget v0, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrlCurrent:I

    .line 1658
    .local v0, "level":I
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScale:F

    .line 1659
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFromLevel:I

    .line 1660
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mToLevel:I

    .line 1661
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v4, p1, p2, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getFocusedObject(IIZ)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1662
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v4, :cond_1

    .line 1663
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->moveToLast()V

    .line 1664
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v1

    .line 1665
    .local v1, "scroll":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    sub-float v4, v1, v4

    iput v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScrollDelta:F

    .line 1666
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 1667
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iput-boolean v3, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollEnabled:Z

    .line 1669
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-eqz v3, :cond_0

    .line 1670
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hide()V

    .line 1675
    .end local v1    # "scroll":F
    :cond_0
    :goto_0
    return v2

    .line 1674
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mScrollDelta:F

    move v2, v3

    .line 1675
    goto :goto_0
.end method

.method protected stopScaleAnimation()V
    .locals 2

    .prologue
    .line 1693
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 1694
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnablePosAnim(Z)V

    .line 1695
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$ScaleControl;->mFocusedObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setZOrder(I)V

    .line 1697
    :cond_0
    return-void
.end method
