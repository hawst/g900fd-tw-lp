.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;
.super Landroid/widget/BaseAdapter;
.source "GallerySearchFilterGroupDetailAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    .line 26
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    .line 27
    return-void
.end method

.method private getTextColorByType(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/content/res/ColorStateList;
    .locals 3
    .param p1, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 107
    const/4 v0, 0x0

    .line 109
    .local v0, "csl":Landroid/content/res/ColorStateList;
    const v1, 0x7f0200cf

    .line 111
    .local v1, "selector":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isPersonTag()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 112
    const v1, 0x7f0200cf

    .line 123
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 125
    return-object v0

    .line 113
    :cond_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isLocationTag()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 114
    const v1, 0x7f0200cd

    goto :goto_0

    .line 115
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isTimeTag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 116
    const v1, 0x7f0200d1

    goto :goto_0

    .line 117
    :cond_2
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isUserTag()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 118
    const v1, 0x7f0200d3

    goto :goto_0

    .line 120
    :cond_3
    const v1, 0x7f0200cb

    goto :goto_0
.end method


# virtual methods
.method public clearData()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 43
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 57
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v7, 0x7f0e0456

    .line 62
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 63
    .local v0, "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    const/4 v3, 0x0

    .line 65
    .local v3, "type":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 66
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isPersonTag()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 67
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0450

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 79
    :cond_0
    :goto_0
    if-nez p2, :cond_1

    .line 80
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 81
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f03005e

    const/4 v5, 0x0

    invoke-virtual {v1, v4, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 83
    if-eqz v0, :cond_1

    move-object v2, p2

    .line 84
    check-cast v2, Landroid/widget/TextView;

    .line 87
    .local v2, "title":Landroid/widget/TextView;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTransTagText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->getTextColorByType(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 89
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v4

    if-eqz v4, :cond_6

    .line 90
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    :goto_1
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v4

    invoke-virtual {p2, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 103
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "title":Landroid/widget/TextView;
    :cond_1
    return-object p2

    .line 68
    :cond_2
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isTimeTag()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 69
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e044c

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 70
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isUserTag()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 71
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e044d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 72
    :cond_4
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isLocationTag()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 73
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e044f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 74
    :cond_5
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEventTag()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 75
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0451

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    .line 92
    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    .restart local v2    # "title":Landroid/widget/TextView;
    :cond_6
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_7

    .line 93
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    .line 96
    :cond_7
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTransTagText()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 33
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 37
    :cond_0
    return-void
.end method
