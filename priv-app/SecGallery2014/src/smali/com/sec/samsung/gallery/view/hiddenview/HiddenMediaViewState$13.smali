.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->onResume()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSizeChanged(I)V
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 388
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->checkMediaAvailability()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$13;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->updateSelectAllActionBar()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 392
    :cond_0
    return-void
.end method

.method public onWindowContentChanged(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 396
    return-void
.end method
