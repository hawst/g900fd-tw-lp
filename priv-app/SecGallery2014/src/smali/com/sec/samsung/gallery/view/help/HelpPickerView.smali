.class public Lcom/sec/samsung/gallery/view/help/HelpPickerView;
.super Ljava/lang/Object;
.source "HelpPickerView.java"


# static fields
.field public static final HELP_PICKER_MODE_ALBUM:I = 0x1

.field public static final HELP_PICKER_MODE_PHOTO:I = 0x2

.field public static final HELP_PICKER_VIEW_PREFERENCE:Ljava/lang/String; = "helpPickerViewPreference"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mBlinkAnimation:Landroid/view/animation/Animation;

.field private mBubbleAnimation:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mHelpMode:I

.field private mImageArrow:Landroid/widget/ImageView;

.field private mImageHelperPoint:Landroid/widget/ImageView;

.field private mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field private mRelativeLayout:Landroid/widget/RelativeLayout;

.field private mTextHelp:Landroid/widget/TextView;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "helpMode"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    .line 39
    const-string/jumbo v0, "window"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mWindowManager:Landroid/view/WindowManager;

    .line 40
    iput p2, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    .line 41
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->initView()V

    .line 42
    return-void
.end method

.method private startAnimation()V
    .locals 3

    .prologue
    const v2, 0x7f040002

    .line 94
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 130
    :cond_2
    :goto_0
    return-void

    .line 105
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 113
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_6

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 116
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    if-eqz v0, :cond_7

    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 118
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_8

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 122
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageHelperPoint:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageHelperPoint:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 125
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBlinkAnimation:Landroid/view/animation/Animation;

    if-nez v0, :cond_a

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    const v1, 0x7f040001

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBlinkAnimation:Landroid/view/animation/Animation;

    .line 128
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageHelperPoint:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mBlinkAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method


# virtual methods
.method public hide()V
    .locals 4

    .prologue
    .line 76
    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 77
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v3, 0x7f0f013b

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 78
    .local v1, "imageView":Landroid/widget/ImageView;
    invoke-virtual {v1}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 79
    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    .line 83
    .end local v1    # "imageView":Landroid/widget/ImageView;
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    :goto_0
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public initView()V
    .locals 10

    .prologue
    const v9, 0x7f0f0128

    const/4 v8, 0x1

    const/4 v5, -0x3

    const/4 v3, 0x2

    const/4 v1, -0x1

    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/LayoutInflater;

    .line 46
    .local v6, "inflate":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 47
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x18

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 55
    :goto_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    if-ne v0, v8, :cond_2

    .line 56
    const v0, 0x7f03006c

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    .line 67
    :cond_0
    :goto_1
    return-void

    .line 51
    :cond_1
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v4, 0x418

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    goto :goto_0

    .line 58
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mHelpMode:I

    if-ne v0, v3, :cond_0

    .line 60
    const v0, 0x7f030070

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f013b

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageHelperPoint:Landroid/widget/ImageView;

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v1, 0x7f0e02db

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v8, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    const v4, 0x7f0e0070

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    .line 64
    .local v7, "text":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mTextHelp:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    const v1, 0x7f0f013a

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mImageArrow:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method public refreshLayout()V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->startAnimation()V

    .line 91
    return-void
.end method

.method public show(Z)V
    .locals 3
    .param p1, "IsAnimation"    # Z

    .prologue
    .line 70
    if-eqz p1, :cond_0

    .line 71
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->startAnimation()V

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpPickerView;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 73
    return-void
.end method
