.class Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;
.super Ljava/lang/Object;
.source "NavigationDrawer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field final synthetic val$title:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;I)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iput p2, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->val$title:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 298
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsOnlyAlbumModePick:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$600(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mIsSignatureAlbumPickMode:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$1100(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeAsUpIndicator(I)V

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # getter for: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$300(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    .line 303
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->val$title:I

    const v1, 0x7f0e0174

    if-eq v0, v1, :cond_2

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget v1, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->val$title:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 305
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/NavigationDrawer$10;->this$0:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    # invokes: Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->adjustLayout()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->access$700(Lcom/sec/samsung/gallery/drawer/NavigationDrawer;)V

    .line 306
    return-void
.end method
