.class public Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlConvSetAnimation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;,
        Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;,
        Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;,
        Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    }
.end annotation


# static fields
.field private static final ANIMATION_DURATION:J = 0xfaL

.field private static final CONV_ATTR_DST_EXIST:B = 0x2t

.field private static final CONV_ATTR_SRC_EXIST:B = 0x1t


# instance fields
.field private mAnimDoneListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field private mContext:Landroid/content/Context;

.field private mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

.field private mConvTotal:I

.field private mDstCount:I

.field private mDstEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mDstIdx:[I

.field private mGlConfig:Lcom/sec/android/gallery3d/glcore/GlConfig;

.field private mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mGlObjSrc:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

.field mProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

.field private mSrcCount:I

.field private mSrcEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mSrcIdx:[I

.field private mUseShortestRotationPath:Z

.field mlistenerConv:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 170
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$1;-><init>(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mlistenerConv:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mContext:Landroid/content/Context;

    .line 36
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->procAnimDone()V

    return-void
.end method

.method private procAnimDone()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 155
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    if-ge v0, v2, :cond_1

    .line 156
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    aget-object v1, v2, v0

    .line 157
    .local v1, "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    iget-byte v2, v1, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->attr:B

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 158
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 155
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 160
    :cond_0
    iget-object v2, v1, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    goto :goto_1

    .line 163
    .end local v1    # "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    :cond_1
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    .line 164
    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    .line 165
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mAnimDoneListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    if-eqz v2, :cond_2

    .line 166
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mAnimDoneListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;->onAnimationDone()V

    .line 168
    :cond_2
    return-void
.end method

.method private setUnionTable()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 43
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcCount:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    add-int v2, v4, v5

    .line 44
    .local v2, "totalCnt":I
    new-array v4, v2, [Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    .line 45
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    if-ge v0, v4, :cond_3

    .line 46
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    invoke-direct {v3, p0, v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;-><init>(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$1;)V

    .line 47
    .local v3, "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    aput-object v3, v4, v0

    .line 48
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcCount:I

    if-ge v1, v4, :cond_0

    .line 49
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstIdx:[I

    aget v4, v4, v0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcIdx:[I

    aget v5, v5, v1

    if-ne v4, v5, :cond_2

    .line 50
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjSrc:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v1

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 51
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v0

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 52
    const/4 v4, 0x3

    iput-byte v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->attr:B

    .line 56
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcCount:I

    if-ne v1, v4, :cond_1

    .line 57
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstIdx:[I

    aget v5, v5, v0

    invoke-interface {v4, v5}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->onGetObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 58
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v0

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 59
    const/4 v4, 0x2

    iput-byte v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->attr:B

    .line 45
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 62
    .end local v1    # "j":I
    .end local v3    # "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    :cond_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    .line 63
    const/4 v0, 0x0

    :goto_2
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcCount:I

    if-ge v0, v4, :cond_7

    .line 64
    const/4 v1, 0x0

    .restart local v1    # "j":I
    :goto_3
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    if-ge v1, v4, :cond_4

    .line 65
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcIdx:[I

    aget v4, v4, v0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstIdx:[I

    aget v5, v5, v1

    if-ne v4, v5, :cond_6

    .line 69
    :cond_4
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    if-ne v1, v4, :cond_5

    .line 70
    new-instance v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    invoke-direct {v3, p0, v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;-><init>(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$1;)V

    .line 71
    .restart local v3    # "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    aput-object v3, v4, v5

    .line 72
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjSrc:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v4, v4, v0

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 73
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcIdx:[I

    aget v5, v5, v0

    invoke-interface {v4, v5}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->onGetObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v4

    iput-object v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 74
    const/4 v4, 0x1

    iput-byte v4, v3, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->attr:B

    .line 75
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    .line 63
    .end local v3    # "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 64
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 78
    .end local v1    # "j":I
    :cond_7
    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 3
    .param p1, "ratio"    # F

    .prologue
    .line 141
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

    if-eqz v2, :cond_1

    .line 142
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v2, v0

    .line 144
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-eqz v1, :cond_0

    .line 145
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mProgressListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;

    invoke-interface {v2, p1, v1}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$GlAnimationProgressListener;->onAnimationProgress(FLcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 142
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 149
    .end local v0    # "i":I
    .end local v1    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    return-void
.end method

.method public findShortestRotationPath()V
    .locals 1

    .prologue
    .line 203
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mUseShortestRotationPath:Z

    .line 204
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 132
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 133
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 134
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->stop()V

    .line 132
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_1
    return-void
.end method

.method public setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .prologue
    .line 199
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mAnimDoneListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 200
    return-void
.end method

.method public setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V
    .locals 0
    .param p1, "dstObj"    # [Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dstIndex"    # [I
    .param p3, "dstEst"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;
    .param p4, "dstCount"    # I

    .prologue
    .line 88
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjDst:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 89
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstIdx:[I

    .line 90
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 91
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mDstCount:I

    .line 92
    return-void
.end method

.method public setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V
    .locals 0
    .param p1, "srcObj"    # [Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "srcIndex"    # [I
    .param p3, "srcEst"    # Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;
    .param p4, "srcCount"    # I

    .prologue
    .line 81
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mGlObjSrc:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 82
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcIdx:[I

    .line 83
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcEst:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 84
    iput p4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mSrcCount:I

    .line 85
    return-void
.end method

.method public start()V
    .locals 6

    .prologue
    .line 99
    :try_start_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setUnionTable()V

    .line 100
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    new-array v3, v3, [Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    .line 101
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    if-ge v1, v3, :cond_3

    .line 102
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mItems:[Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;

    aget-object v2, v3, v1

    .line 103
    .local v2, "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    iget-byte v3, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->attr:B

    and-int/lit8 v3, v3, 0x2

    if-lez v3, :cond_2

    .line 104
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    new-instance v4, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    iget-object v5, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    aput-object v4, v3, v1

    .line 105
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v3, :cond_0

    .line 106
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v1

    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setRefereceObject(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V

    .line 107
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 114
    :cond_0
    :goto_1
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mUseShortestRotationPath:Z

    if-eqz v3, :cond_1

    .line 115
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->findShortestRotationPath()V

    .line 117
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v1

    const-wide/16 v4, 0xfa

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setDuration(J)V

    .line 118
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->start()V

    .line 101
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 110
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    new-instance v4, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    iget-object v5, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->srcObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-direct {v4, v5}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    aput-object v4, v3, v1

    .line 111
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    aget-object v3, v3, v1

    iget-object v4, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setRefereceObject(Lcom/sec/android/gallery3d/glcore/GlObject;Z)V

    .line 112
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;->dstObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 124
    .end local v1    # "i":I
    .end local v2    # "uItem":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$UnionItem;
    :catch_0
    move-exception v0

    .line 125
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 127
    .end local v0    # "e":Ljava/lang/Exception;
    :goto_2
    return-void

    .line 120
    .restart local v1    # "i":I
    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvAnim:[Lcom/sec/samsung/gallery/glview/GlConverseAnimation;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mConvTotal:I

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mlistenerConv:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlConverseAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 121
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 122
    const-wide/16 v4, 0xfa

    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDuration(J)V

    .line 123
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;->start()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method
