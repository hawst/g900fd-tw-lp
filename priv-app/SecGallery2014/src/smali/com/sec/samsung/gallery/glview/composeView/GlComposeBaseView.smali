.class public abstract Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
.super Lcom/sec/android/gallery3d/glcore/GlLayer;
.source "GlComposeBaseView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnDropItemClickListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnFocusChangedListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;,
        Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;
    }
.end annotation


# static fields
.field protected static DEF_DISTANCE:F = 0.0f

.field protected static DEF_FOV:F = 0.0f

.field public static final KEYPAD_TAP:I = 0x5

.field public static final LIST_MODE_NEWALBUM:I = 0x2

.field public static final LIST_MODE_NORMAL:I = 0x0

.field public static final LIST_MODE_REORDER:I = 0x3

.field public static final LIST_MODE_SELECT:I = 0x1

.field public static final MAX_GENERIC_MOTION:I = 0x4

.field public static final MOVE_DOWN:I = 0x4

.field public static final MOVE_LEFT:I = 0x1

.field public static final MOVE_RIGHT:I = 0x2

.field public static final MOVE_UP:I = 0x3

.field public static final OPTION_EASY_MODE:I = 0x100

.field public static final OPTION_HOVER_ALBUM:I = 0x40

.field public static final OPTION_HOVER_ITEM:I = 0x80

.field public static final OPTION_SHRINK:I = 0x10

.field public static final OPTION_SHRINK_ANIM:I = 0x20

.field public static final REORDER_FINISH:I = 0x1

.field public static final STATUS_COLUMN_COUNT:I = 0x1

.field public static final STATUS_REACHED_LEAST_ROW:I = 0x2

.field public static final STATUS_THM_LEVEL_CHANGE:I = 0x3

.field private static final TAG:Ljava/lang/String; = "GlComposeBaseView"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

.field protected mGl:Ljavax/microedition/khronos/opengles/GL11;

.field protected mLockPos:Z

.field protected mMode:I

.field protected mModeObj:Ljava/lang/Object;

.field protected mModeParm:I

.field protected mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnDropItemClickListener;

.field protected mOnExtendListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

.field protected mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnFocusChangedListener;

.field protected mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

.field protected mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

.field protected mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

.field protected mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

.field protected mOnKeyListenner:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

.field protected mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

.field protected mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

.field protected mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

.field protected mSx:F

.field protected mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

.field protected mTransitionAnimationType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    const/high16 v0, 0x41f00000    # 30.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->DEF_FOV:F

    .line 48
    const/high16 v0, 0x44480000    # 800.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->DEF_DISTANCE:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;-><init>()V

    .line 62
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnExtendListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mSx:F

    .line 73
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mTransitionAnimationType:I

    .line 76
    const-string v0, "GlComposeBaseView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GlComposeBaseView = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    iput-object p0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    .line 78
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mContext:Landroid/content/Context;

    .line 79
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    .line 80
    return-void
.end method


# virtual methods
.method public IsCheckMode()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 88
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    if-ne v1, v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 92
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public checkIfIncorrectMode()Z
    .locals 2

    .prologue
    .line 314
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionAndSplitPinch:Z

    if-eqz v0, :cond_1

    .line 315
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    .line 317
    :goto_0
    return v0

    .line 315
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 317
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->IsCheckMode()Z

    move-result v0

    goto :goto_0
.end method

.method public getActionBarHeight()I
    .locals 2

    .prologue
    .line 162
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 163
    .local v0, "height":I
    return v0
.end method

.method protected getDistance()F
    .locals 1

    .prologue
    .line 154
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->DEF_DISTANCE:F

    return v0
.end method

.method protected getFOV()F
    .locals 1

    .prologue
    .line 150
    sget v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->DEF_FOV:F

    return v0
.end method

.method public getFirstContentPosition()I
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    return v0
.end method

.method public getLastContentPosition()I
    .locals 1

    .prologue
    .line 179
    const/4 v0, 0x0

    return v0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 171
    const/4 v0, 0x0

    return v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    return v0
.end method

.method public getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScroll()F
    .locals 1

    .prologue
    .line 183
    const/4 v0, 0x0

    return v0
.end method

.method protected isScreenLocked()Z
    .locals 1

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mLockPos:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->isCreated()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onGenericMotionCancel()V
    .locals 2

    .prologue
    .line 197
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 198
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 197
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v1, v1, v0

    invoke-interface {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    goto :goto_1

    .line 202
    :cond_1
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 144
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->isCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->resetLayout()V

    .line 147
    :cond_0
    return-void
.end method

.method public refreshCheckState()V
    .locals 0

    .prologue
    .line 209
    return-void
.end method

.method public refreshItem(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 219
    return-void
.end method

.method public refreshView(Z)V
    .locals 0
    .param p1, "reload"    # Z

    .prologue
    .line 216
    return-void
.end method

.method protected resetLayout()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 108
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->getFOV()F

    move-result v2

    .line 109
    .local v2, "fov":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->getDistance()F

    move-result v1

    .line 111
    .local v1, "distance":F
    const/16 v4, 0x10

    new-array v3, v4, [F

    .line 112
    .local v3, "mProjM":[F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mHeight:I

    int-to-float v5, v5

    div-float v0, v4, v5

    .line 114
    .local v0, "aspect":F
    invoke-virtual {p0, v2, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->setDefaultDistance(FF)V

    .line 116
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v4, :cond_0

    .line 117
    const-string v4, "GlComposeBaseView"

    const-string v5, "resetLayout : gl root is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :goto_0
    return-void

    .line 122
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLContext()Ljavax/microedition/khronos/opengles/GL11;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    .line 123
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v5, 0xb60

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glDisable(I)V

    .line 124
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v5, 0xbe2

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glEnable(I)V

    .line 126
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v5, 0x1701

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    .line 127
    invoke-static {v3, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 128
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x44fa0000    # 2000.0f

    invoke-static {v3, v2, v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/TUtils;->glhPerspectivef2([FFFFF)V

    .line 130
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/glcore/GlConfig;->setProspectMatrix([F)V

    .line 131
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    invoke-interface {v4, v3, v6}, Ljavax/microedition/khronos/opengles/GL11;->glLoadMatrixf([FI)V

    .line 132
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGl:Ljavax/microedition/khronos/opengles/GL11;

    const/16 v5, 0x1700

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL11;->glMatrixMode(I)V

    goto :goto_0
.end method

.method public setDefaultDistance(FF)V
    .locals 6
    .param p1, "fov"    # F
    .param p2, "distance"    # F

    .prologue
    .line 98
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mFov:F

    .line 99
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mDistance:F

    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 102
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 103
    .local v0, "aspect":F
    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mDistance:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mFov:F

    float-to-double v2, v2

    const-wide v4, 0x400921fafc8b007aL    # 3.141592

    mul-double/2addr v2, v4

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mHeightSpace:F

    .line 104
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mHeightSpace:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mWidthSpace:F

    goto :goto_0
.end method

.method public setLayerTransitionAnimation(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 205
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mTransitionAnimationType:I

    .line 206
    return-void
.end method

.method public setMode(IILjava/lang/Object;)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 187
    iput p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mMode:I

    .line 188
    iput p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mModeParm:I

    .line 189
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mModeObj:Ljava/lang/Object;

    .line 190
    return-void
.end method

.method public setOnExtendListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

    .prologue
    .line 258
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnExtendListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnExtendListener;

    .line 259
    return-void
.end method

.method public setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    .prologue
    const/4 v3, 0x4

    .line 242
    if-lt p1, v3, :cond_0

    .line 243
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnGenericMotionListener : index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aput-object p2, v0, p1

    .line 247
    return-void
.end method

.method public setOnHoverListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    .prologue
    .line 234
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;

    .line 235
    return-void
.end method

.method public setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    .prologue
    .line 222
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    .line 223
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    .prologue
    .line 226
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;

    .line 227
    return-void
.end method

.method public setOnKeyListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    .prologue
    .line 238
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;

    .line 239
    return-void
.end method

.method public setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    .line 255
    return-void
.end method

.method public setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    .prologue
    .line 230
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;

    .line 231
    return-void
.end method

.method public setOnSwitchViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;)V
    .locals 0
    .param p1, "onSwitchViewListener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    .prologue
    .line 250
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;->mOnSwitchViewListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;

    .line 251
    return-void
.end method
