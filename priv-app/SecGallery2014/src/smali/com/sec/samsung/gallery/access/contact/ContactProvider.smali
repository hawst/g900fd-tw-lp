.class public Lcom/sec/samsung/gallery/access/contact/ContactProvider;
.super Ljava/lang/Object;
.source "ContactProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/access/contact/ContactProvider$ContactObserver;
    }
.end annotation


# static fields
.field private static final ACCOUNT_TYPE_AND_DATA_SET_PROJECTION:[Ljava/lang/String;

.field private static final ACCOUNT_TYPE_AND_DATA_SET_SELECTION:Ljava/lang/String; = "contact_id=?"

.field private static final CONTACTS_SNS_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_DATA_SORT_ORDER:Ljava/lang/String; = "is_super_primary desc, _id asc"

.field private static final CONTACT_ID_PROJECTION:[Ljava/lang/String;

.field private static final CONTACT_LOOKUP_KEY_SELECTION:Ljava/lang/String; = "lookup=?"

.field private static final CONTACT_PROJECTION:[Ljava/lang/String;

.field private static final DISPLAY_NAME_SELECTION:Ljava/lang/String; = "display_name =?"

.field private static final EMAIL_LOOKUP_KEY_SELECTION:Ljava/lang/String; = "lookup=? AND mimetype=\'vnd.android.cursor.item/email_v2\'"

.field private static final EMAIL_PROJECTION:[Ljava/lang/String;

.field public static final FACEBOOK_ACCOUNT_TYPE_AND_DATA_SET:Ljava/lang/String; = "com.facebook.auth.login"

.field public static final GOOGLEPLUS_ACCOUNT_TYPE_AND_DATA_SET:Ljava/lang/String; = "com.google/plus"

.field private static final INDEX_SNS_ACCOUNT_NAME:I = 0x0

.field private static final INDEX_SNS_ACCOUNT_TYPE:I = 0x1

.field private static final INDEX_SNS_RES_PACKAGE:I = 0x2

.field private static final INDEX_SNS_TEXT:I = 0x3

.field private static final INDEX_SNS_TIMESTAMP:I = 0x4

.field private static final LOOKUPKEY_PROJECTION:[Ljava/lang/String;

.field private static final LOOKUP_KEY_SELECTION:Ljava/lang/String; = "lookup =?"

.field public static final ME_LOOKUP_KEY:Ljava/lang/String; = "me_lookup_key"

.field private static final MeContactId:Ljava/lang/String; = "-1"

.field public static final MeLookupKey:Ljava/lang/String; = "profile"

.field private static final NAME_PROJECTION:[Ljava/lang/String;

.field private static final PHONE_LOOKUP_KEY_SELECTION:Ljava/lang/String; = "lookup=? AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

.field private static final PHONE_PROJECTION:[Ljava/lang/String;

.field private static final RAWCONTACT_DATA_SORT_ORDER:Ljava/lang/String; = "_id asc"

.field private static final RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "ContactProvider"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mInfoSize:I

.field private mProfileKey:Ljava/lang/String;

.field private mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "display_name"

    aput-object v1, v0, v4

    const-string v1, "display_name_alt"

    aput-object v1, v0, v5

    const-string v1, "photo_id"

    aput-object v1, v0, v6

    const-string v1, "photo_thumb_uri"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string v2, "lookup"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    .line 78
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_ID_PROJECTION:[Ljava/lang/String;

    .line 79
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->EMAIL_PROJECTION:[Ljava/lang/String;

    .line 81
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "data1"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->PHONE_PROJECTION:[Ljava/lang/String;

    .line 83
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "display_name"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->NAME_PROJECTION:[Ljava/lang/String;

    .line 85
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "account_name"

    aput-object v1, v0, v3

    const-string v1, "account_type"

    aput-object v1, v0, v4

    const-string v1, "res_package"

    aput-object v1, v0, v5

    const-string/jumbo v1, "text"

    aput-object v1, v0, v6

    const-string/jumbo v1, "timestamp"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACTS_SNS_PROJECTION:[Ljava/lang/String;

    .line 93
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "account_name"

    aput-object v1, v0, v4

    const-string v1, "account_type"

    aput-object v1, v0, v5

    const-string v1, "data_set"

    aput-object v1, v0, v6

    const-string/jumbo v1, "sync1"

    aput-object v1, v0, v7

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    .line 978
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "account_type_and_data_set"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->ACCOUNT_TYPE_AND_DATA_SET_PROJECTION:[Ljava/lang/String;

    .line 991
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "lookup"

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->LOOKUPKEY_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mProfileKey:Ljava/lang/String;

    .line 133
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mInfoSize:I

    .line 127
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    .line 128
    return-void
.end method

.method private checkProviderState()Z
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 149
    const/4 v8, 0x0

    .line 150
    .local v8, "pr_status":I
    const/4 v6, 0x0

    .line 152
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$ProviderStatus;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "status"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 154
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 160
    :cond_0
    if-eqz v6, :cond_1

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 164
    :cond_1
    :goto_0
    if-eqz v8, :cond_2

    const/4 v0, 0x4

    if-ne v8, v0, :cond_4

    :cond_2
    move v0, v10

    :goto_1
    return v0

    .line 157
    :catch_0
    move-exception v7

    .line 158
    .local v7, "e":Ljava/lang/SecurityException;
    :try_start_1
    invoke-virtual {v7}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 160
    if-eqz v6, :cond_1

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 160
    .end local v7    # "e":Ljava/lang/SecurityException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 161
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    :cond_4
    move v0, v9

    .line 164
    goto :goto_1
.end method

.method public static getDisplayName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 537
    invoke-static {p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 555
    :goto_0
    return-object v8

    .line 541
    :cond_0
    const/4 v8, 0x0

    .line 542
    .local v8, "result":Ljava/lang/String;
    const/4 v6, 0x0

    .line 544
    .local v6, "cursor":Landroid/database/Cursor;
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 546
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->NAME_PROJECTION:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 547
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 548
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 553
    :cond_1
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 550
    :catch_0
    move-exception v7

    .line 551
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ContactProvider"

    const-string v2, "Can not create contact list! "

    invoke-static {v0, v2, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 553
    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v6}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static getGroup(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 24
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "lookupKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 559
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 560
    const/16 v21, 0x0

    .line 637
    :cond_0
    :goto_0
    return-object v21

    .line 563
    :cond_1
    const/4 v13, 0x0

    .line 564
    .local v13, "cursor":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 565
    .local v16, "groupCursor":Landroid/database/Cursor;
    new-instance v23, Ljava/util/HashSet;

    invoke-direct/range {v23 .. v23}, Ljava/util/HashSet;-><init>()V

    .line 566
    .local v23, "set":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v21, Ljava/util/ArrayList;

    invoke-direct/range {v21 .. v21}, Ljava/util/ArrayList;-><init>()V

    .line 567
    .local v21, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const-string v2, "ContactProvider"

    const-string v4, "Gallery getgroup +"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :try_start_0
    const-string v5, "mimetype=?"

    .line 572
    .local v5, "whereStr":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "vnd.android.cursor.item/group_membership"

    aput-object v4, v6, v2

    .line 575
    .local v6, "whereArg":[Ljava/lang/String;
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "data"

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 577
    .local v3, "contactUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "data1"

    aput-object v8, v4, v7

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 582
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 583
    .local v18, "groupRowIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v13, :cond_4

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 584
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-gtz v2, :cond_2

    .line 627
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 628
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 589
    :cond_2
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 590
    .local v17, "groupRowId":Ljava/lang/String;
    if-eqz v17, :cond_3

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_5

    .line 594
    :cond_3
    :goto_1
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_2

    .line 597
    .end local v17    # "groupRowId":Ljava/lang/String;
    :cond_4
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_6

    .line 627
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 628
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 593
    .restart local v17    # "groupRowId":Ljava/lang/String;
    :cond_5
    :try_start_2
    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 624
    .end local v3    # "contactUri":Landroid/net/Uri;
    .end local v5    # "whereStr":Ljava/lang/String;
    .end local v6    # "whereArg":[Ljava/lang/String;
    .end local v17    # "groupRowId":Ljava/lang/String;
    .end local v18    # "groupRowIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catch_0
    move-exception v14

    .line 625
    .local v14, "e":Ljava/lang/Exception;
    :try_start_3
    const-string v2, "ContactProvider"

    const-string v4, "Can not create contact list! "

    invoke-static {v2, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 627
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 628
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 632
    .end local v14    # "e":Ljava/lang/Exception;
    :goto_2
    invoke-virtual/range {v23 .. v23}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .line 633
    .local v20, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    :goto_3
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 634
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 600
    .end local v20    # "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/String;>;"
    .restart local v3    # "contactUri":Landroid/net/Uri;
    .restart local v5    # "whereStr":Ljava/lang/String;
    .restart local v6    # "whereArg":[Ljava/lang/String;
    .restart local v18    # "groupRowIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    :try_start_4
    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    .line 601
    .local v22, "sb":Ljava/lang/StringBuilder;
    const-string v2, "("

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 602
    invoke-virtual/range {v18 .. v18}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    .local v19, "i$":Ljava/util/Iterator;
    :goto_4
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/String;

    .line 603
    .restart local v17    # "groupRowId":Ljava/lang/String;
    move-object/from16 v0, v22

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 604
    const-string v2, ","

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_4

    .line 627
    .end local v3    # "contactUri":Landroid/net/Uri;
    .end local v5    # "whereStr":Ljava/lang/String;
    .end local v6    # "whereArg":[Ljava/lang/String;
    .end local v17    # "groupRowId":Ljava/lang/String;
    .end local v18    # "groupRowIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v19    # "i$":Ljava/util/Iterator;
    .end local v22    # "sb":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 628
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 606
    .restart local v3    # "contactUri":Landroid/net/Uri;
    .restart local v5    # "whereStr":Ljava/lang/String;
    .restart local v6    # "whereArg":[Ljava/lang/String;
    .restart local v18    # "groupRowIdArrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v19    # "i$":Ljava/util/Iterator;
    .restart local v22    # "sb":Ljava/lang/StringBuilder;
    :cond_7
    :try_start_5
    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 607
    const-string v2, ")"

    move-object/from16 v0, v22

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 610
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id IN "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 611
    .local v10, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    sget-object v8, Landroid/provider/ContactsContract$Groups;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v4, "title"

    aput-object v4, v9, v2

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v7 .. v12}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 615
    if-eqz v16, :cond_a

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_a

    .line 617
    :cond_8
    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 618
    .local v15, "group":Ljava/lang/String;
    if-eqz v15, :cond_9

    .line 619
    move-object/from16 v0, v23

    invoke-virtual {v0, v15}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 621
    :cond_9
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result v2

    if-nez v2, :cond_8

    .line 627
    .end local v15    # "group":Ljava/lang/String;
    :cond_a
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 628
    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_2
.end method

.method public static getResolveExternalResId(Landroid/content/Context;Ljava/lang/String;)I
    .locals 22
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "packageName"    # Ljava/lang/String;

    .prologue
    .line 900
    const/4 v2, 0x0

    .line 903
    .local v2, "accountTypeIconAttribute":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 905
    .local v13, "pm":Landroid/content/pm/PackageManager;
    const/16 v19, 0x84

    :try_start_0
    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v13, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v11

    .line 907
    .local v11, "packageInfo":Landroid/content/pm/PackageInfo;
    iget-object v3, v11, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    .local v3, "arr$":[Landroid/content/pm/ServiceInfo;
    array-length v9, v3

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_5

    aget-object v16, v3, v8

    .line 908
    .local v16, "serviceInfo":Landroid/content/pm/ServiceInfo;
    const-string v19, "android.provider.CONTACTS_STRUCTURE"

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v13, v1}, Landroid/content/pm/ServiceInfo;->loadXmlMetaData(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/res/XmlResourceParser;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v12

    .line 910
    .local v12, "parser":Landroid/content/res/XmlResourceParser;
    if-nez v12, :cond_1

    .line 907
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 916
    :cond_1
    :try_start_1
    invoke-interface {v12}, Landroid/content/res/XmlResourceParser;->next()I

    move-result v17

    .local v17, "type":I
    const/16 v19, 0x2

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_2

    const/16 v19, 0x1

    move/from16 v0, v17

    move/from16 v1, v19

    if-ne v0, v1, :cond_1

    .line 920
    :cond_2
    const/16 v19, 0x2

    move/from16 v0, v17

    move/from16 v1, v19

    if-eq v0, v1, :cond_3

    .line 921
    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "No start tag found"

    invoke-direct/range {v19 .. v20}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v19
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 933
    .end local v17    # "type":I
    :catch_0
    move-exception v6

    .line 934
    .local v6, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_2
    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "Problem reading XML"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    .line 940
    .end local v3    # "arr$":[Landroid/content/pm/ServiceInfo;
    .end local v6    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .end local v12    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v16    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :catch_1
    move-exception v6

    .line 943
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 944
    const/4 v15, -0x1

    .line 974
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :goto_1
    return v15

    .line 924
    .restart local v3    # "arr$":[Landroid/content/pm/ServiceInfo;
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    .restart local v11    # "packageInfo":Landroid/content/pm/PackageInfo;
    .restart local v12    # "parser":Landroid/content/res/XmlResourceParser;
    .restart local v16    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    .restart local v17    # "type":I
    :cond_3
    :try_start_3
    invoke-interface {v12}, Landroid/content/res/XmlResourceParser;->getAttributeCount()I

    move-result v5

    .line 925
    .local v5, "attributeCount":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_2
    if-ge v7, v5, :cond_0

    .line 926
    invoke-interface {v12, v7}, Landroid/content/res/XmlResourceParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v4

    .line 927
    .local v4, "attr":Ljava/lang/String;
    invoke-interface {v12, v7}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v18

    .line 928
    .local v18, "value":Ljava/lang/String;
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getResolveExternalResId"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, "="

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 929
    const-string v19, "accountTypeIcon"

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_1

    move-result v19

    if-eqz v19, :cond_4

    .line 930
    move-object/from16 v2, v18

    .line 925
    :cond_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 935
    .end local v4    # "attr":Ljava/lang/String;
    .end local v5    # "attributeCount":I
    .end local v7    # "i":I
    .end local v17    # "type":I
    .end local v18    # "value":Ljava/lang/String;
    :catch_2
    move-exception v6

    .line 936
    .local v6, "e":Ljava/io/IOException;
    :try_start_4
    new-instance v19, Ljava/lang/IllegalStateException;

    const-string v20, "Problem reading XML"

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-direct {v0, v1, v6}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v19
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_1

    .line 947
    .end local v6    # "e":Ljava/io/IOException;
    .end local v12    # "parser":Landroid/content/res/XmlResourceParser;
    .end local v16    # "serviceInfo":Landroid/content/pm/ServiceInfo;
    :cond_5
    if-eqz v2, :cond_6

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v19

    if-eqz v19, :cond_7

    .line 948
    :cond_6
    const/4 v15, -0x1

    goto :goto_1

    .line 950
    :cond_7
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getResolveExternalResId+"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 951
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v19

    const/16 v20, 0x40

    move/from16 v0, v19

    move/from16 v1, v20

    if-eq v0, v1, :cond_8

    .line 952
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " must be a resource name beginnig with \'@\'"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 953
    const/4 v15, -0x1

    goto/16 :goto_1

    .line 955
    :cond_8
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v2, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v10

    .line 960
    .local v10, "name":Ljava/lang/String;
    :try_start_5
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_3

    move-result-object v14

    .line 966
    .local v14, "res":Landroid/content/res/Resources;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p1

    invoke-virtual {v14, v10, v0, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v15

    .line 968
    .local v15, "resId":I
    if-nez v15, :cond_9

    .line 969
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getResolveExternalResId Unable to load "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, " from package "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    const/4 v15, -0x1

    goto/16 :goto_1

    .line 961
    .end local v14    # "res":Landroid/content/res/Resources;
    .end local v15    # "resId":I
    :catch_3
    move-exception v6

    .line 962
    .local v6, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getResolveExternalResId Unable to load package "

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 963
    invoke-virtual {v6}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    .line 964
    const/4 v15, -0x1

    goto/16 :goto_1

    .line 973
    .end local v6    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v14    # "res":Landroid/content/res/Resources;
    .restart local v15    # "resId":I
    :cond_9
    const-string v19, "ContactProvider"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "getResolveExternalResId+"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;
    .locals 11
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1003
    const/4 v10, 0x0

    .line 1025
    :goto_0
    return-object v10

    .line 1005
    :cond_0
    const/4 v10, 0x0

    .line 1006
    .local v10, "result":[Ljava/lang/String;
    const/4 v7, 0x0

    .line 1008
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1010
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1011
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v6

    .line 1012
    .local v6, "cnt":I
    if-lez v6, :cond_1

    .line 1013
    new-array v10, v6, [Ljava/lang/String;

    .line 1014
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_1
    if-ge v9, v6, :cond_1

    .line 1015
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v10, v9

    .line 1016
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1014
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 1023
    .end local v6    # "cnt":I
    .end local v9    # "i":I
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 1020
    :catch_0
    move-exception v8

    .line 1021
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ContactProvider"

    const-string v1, "getStringDataFromDB - Can not create contact list! "

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1023
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public static isWritableAccountWithDataSet(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p0, "accountType"    # Ljava/lang/String;
    .param p1, "dataSet"    # Ljava/lang/String;

    .prologue
    .line 645
    const/4 v5, 0x0

    .line 646
    .local v5, "result":Z
    const-string/jumbo v1, "vnd.sec.contact.phone"

    .line 647
    .local v1, "PHONE_ACCOUNT_TYPE":Ljava/lang/String;
    const-string/jumbo v3, "vnd.sec.contact.phone_knox"

    .line 648
    .local v3, "PHONE_ACCOUNT_TYPE_FROM_KNOX_TO_PERSONAL":Ljava/lang/String;
    const-string/jumbo v2, "vnd.sec.contact.phone_knox2"

    .line 649
    .local v2, "PHONE_ACCOUNT_TYPE_FROM_KNOX2_TO_PERSONAL":Ljava/lang/String;
    const-string/jumbo v4, "vnd.sec.contact.phone_personal"

    .line 650
    .local v4, "PHONE_ACCOUNT_TYPE_FROM_PERSONAL_TO_KNOX":Ljava/lang/String;
    const-string v0, "com.google"

    .line 651
    .local v0, "Google":Ljava/lang/String;
    if-nez p0, :cond_0

    .line 652
    const/4 v5, 0x0

    .line 654
    :cond_0
    const-string/jumbo v6, "vnd.sec.contact.phone"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string/jumbo v6, "vnd.sec.contact.phone_knox"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string/jumbo v6, "vnd.sec.contact.phone_knox2"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    const-string/jumbo v6, "vnd.sec.contact.phone_personal"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 658
    :cond_1
    const/4 v5, 0x1

    .line 663
    :cond_2
    :goto_0
    return v5

    .line 659
    :cond_3
    const-string v6, "com.google"

    invoke-virtual {v6, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 660
    const/4 v5, 0x1

    goto :goto_0
.end method


# virtual methods
.method public checkCallerIdPhoto(Ljava/lang/String;)Z
    .locals 20
    .param p1, "lookupkey"    # Ljava/lang/String;

    .prologue
    .line 667
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 668
    const/16 v16, 0x0

    .line 722
    :goto_0
    return v16

    .line 672
    :cond_0
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 674
    .local v3, "uri":Landroid/net/Uri;
    const/16 v16, 0x0

    .line 675
    .local v16, "result":Z
    if-nez p1, :cond_1

    .line 676
    const/16 v16, 0x0

    goto :goto_0

    .line 679
    :cond_1
    const/4 v12, 0x0

    .line 680
    .local v12, "cursor":Landroid/database/Cursor;
    const/4 v11, 0x0

    .line 681
    .local v11, "contactId":Ljava/lang/String;
    const/4 v15, 0x0

    .line 683
    .local v15, "photoId":Ljava/lang/Long;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 684
    if-eqz v12, :cond_2

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 685
    const/4 v2, 0x0

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 686
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v15

    .line 691
    :cond_2
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 695
    :goto_1
    if-eqz v11, :cond_5

    if-eqz v15, :cond_3

    :try_start_1
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v18, 0x0

    cmp-long v2, v4, v18

    if-nez v2, :cond_5

    .line 696
    :cond_3
    const-string v7, "contact_id=?"

    .line 698
    .local v7, "where":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v8, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v11, v8, v2

    .line 699
    .local v8, "args":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 702
    const/4 v10, 0x0

    .line 703
    .local v10, "accountType":Ljava/lang/String;
    const/4 v13, 0x0

    .line 705
    .local v13, "dataSet":Ljava/lang/String;
    if-eqz v12, :cond_4

    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 706
    const/4 v2, 0x2

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 707
    const/4 v2, 0x3

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    .line 711
    :cond_4
    invoke-static {v10, v13}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->isWritableAccountWithDataSet(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-eqz v2, :cond_5

    .line 712
    const/16 v16, 0x1

    .line 717
    .end local v7    # "where":Ljava/lang/String;
    .end local v8    # "args":[Ljava/lang/String;
    .end local v10    # "accountType":Ljava/lang/String;
    .end local v13    # "dataSet":Ljava/lang/String;
    :cond_5
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_0

    .line 688
    :catch_0
    move-exception v14

    .line 689
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "ContactProvider"

    const-string v4, "checkCallerIdPhoto! "

    invoke-static {v2, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 691
    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 717
    :catchall_1
    move-exception v2

    invoke-static {v12}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public checkWritableAccountWithLookupkey(Ljava/lang/String;)Z
    .locals 12
    .param p1, "lookupkey"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    .line 726
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 778
    :goto_0
    return v11

    .line 730
    :cond_0
    const/4 v11, 0x0

    .line 731
    .local v11, "result":Z
    if-nez p1, :cond_1

    .line 732
    const/4 v11, 0x0

    goto :goto_0

    .line 735
    :cond_1
    const/4 v8, 0x0

    .line 736
    .local v8, "cursor":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 738
    .local v7, "contactId":Ljava/lang/String;
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v3, "lookup=?"

    const/4 v5, 0x1

    new-array v4, v5, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 741
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 742
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 747
    :cond_2
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 751
    :goto_1
    if-eqz v7, :cond_4

    .line 752
    :try_start_1
    const-string v3, "contact_id=?"

    .line 754
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v7, v4, v0

    .line 755
    .local v4, "args":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 758
    const/4 v6, 0x0

    .line 759
    .local v6, "accountType":Ljava/lang/String;
    const/4 v9, 0x0

    .line 761
    .local v9, "dataSet":Ljava/lang/String;
    if-eqz v8, :cond_3

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 762
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 763
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 767
    :cond_3
    invoke-static {v6, v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->isWritableAccountWithDataSet(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_4

    .line 768
    const/4 v11, 0x1

    .line 773
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "args":[Ljava/lang/String;
    .end local v6    # "accountType":Ljava/lang/String;
    .end local v9    # "dataSet":Ljava/lang/String;
    :cond_4
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_0

    .line 744
    :catch_0
    move-exception v10

    .line 745
    .local v10, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v0, "ContactProvider"

    const-string v1, "checkCallerIdPhoto! "

    invoke-static {v0, v1, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 747
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v10    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0

    .line 773
    :catchall_1
    move-exception v0

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public findContactIDWithName(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 277
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 278
    .local v6, "contactInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;>;"
    const/4 v8, 0x0

    .line 281
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    const-string v0, "ContactProvider"

    const-string v1, "findContactIDWithName Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    const-string v3, "display_name =?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 286
    if-eqz v8, :cond_1

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 288
    :cond_0
    new-instance v7, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    invoke-direct {v7}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;-><init>()V

    .line 289
    .local v7, "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setId(Ljava/lang/Long;)V

    .line 290
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setDisplayName(Ljava/lang/String;)V

    .line 291
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setDisplayNameAlter(Ljava/lang/String;)V

    .line 292
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setPhotoId(Ljava/lang/Long;)V

    .line 293
    const/4 v0, 0x4

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setPhotoThumbnailUri(Ljava/lang/String;)V

    .line 294
    const/4 v0, 0x5

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setSingleLookupKey(Ljava/lang/String;)V

    .line 295
    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 301
    .end local v7    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :cond_1
    if-eqz v8, :cond_2

    .line 302
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 305
    :cond_2
    :goto_0
    const-string v0, "ContactProvider"

    const-string v1, "findContactIDWithName End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 306
    return-object v6

    .line 298
    :catch_0
    move-exception v9

    .line 299
    .local v9, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ContactProvider"

    const-string v1, "Can not create contact list! "

    invoke-static {v0, v1, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 301
    if-eqz v8, :cond_2

    .line 302
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 301
    .end local v9    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_3

    .line 302
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method public getAccountTypeNdataSet(Ljava/lang/String;)[Ljava/lang/String;
    .locals 8
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 984
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getContactId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 985
    .local v6, "contactId":Ljava/lang/String;
    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->ACCOUNT_TYPE_AND_DATA_SET_PROJECTION:[Ljava/lang/String;

    const-string v3, "contact_id=?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v6, v4, v0

    const-string v5, "_id asc"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    .line 988
    .local v7, "result":[Ljava/lang/String;
    return-object v7
.end method

.method public getChatOnBuddies(Ljava/lang/Long;)[Ljava/lang/String;
    .locals 10
    .param p1, "id"    # Ljava/lang/Long;

    .prologue
    .line 426
    const/4 v4, 0x0

    .line 427
    .local v4, "result":[Ljava/lang/String;
    const/4 v1, 0x0

    .line 429
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v8, 0x0

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x0

    invoke-static {v5, v6, v7, v8}, Lcom/sec/android/gallery3d/util/ChatONAPI;->getBuddyList(Landroid/content/Context;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 433
    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 434
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 435
    .local v0, "cnt":I
    if-lez v0, :cond_0

    .line 436
    new-array v4, v0, [Ljava/lang/String;

    .line 437
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_0

    .line 438
    const/4 v5, 0x0

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v3

    .line 439
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 437
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 447
    .end local v0    # "cnt":I
    .end local v3    # "i":I
    :cond_0
    if-eqz v1, :cond_1

    .line 448
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 451
    :cond_1
    :goto_1
    return-object v4

    .line 444
    :catch_0
    move-exception v2

    .line 445
    .local v2, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v5, "ContactProvider"

    const-string v6, "Can not get buddies list! "

    invoke-static {v5, v6, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    if-eqz v1, :cond_1

    .line 448
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 447
    .end local v2    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    if-eqz v1, :cond_2

    .line 448
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v5
.end method

.method public getChatOnBuddies(Ljava/lang/String;)[Ljava/lang/String;
    .locals 18
    .param p1, "lookupkey"    # Ljava/lang/String;

    .prologue
    .line 455
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 456
    const/16 v16, 0x0

    .line 512
    :goto_0
    return-object v16

    .line 459
    :cond_0
    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "requesting_package"

    const-string v4, "facetag"

    invoke-virtual {v1, v3, v4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    .line 463
    .local v2, "uri":Landroid/net/Uri;
    const/16 v16, 0x0

    .line 464
    .local v16, "result":[Ljava/lang/String;
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 465
    .local v11, "chatOnName":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_6

    .line 467
    const/4 v14, 0x0

    .line 468
    .local v14, "cursor":Landroid/database/Cursor;
    sget-object v3, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 469
    .local v13, "contactIds":[Ljava/lang/String;
    if-nez v13, :cond_4

    const/4 v12, 0x0

    .line 470
    .local v12, "contactId":Ljava/lang/String;
    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 471
    .local v10, "accountTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 474
    .local v17, "sync1List":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v12, :cond_5

    .line 475
    :try_start_0
    const-string v6, "contact_id=?"

    .line 477
    .local v6, "where":Ljava/lang/String;
    const/4 v1, 0x1

    new-array v7, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object v12, v7, v1

    .line 478
    .local v7, "args":[Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v5, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v8, 0x0

    invoke-virtual/range {v3 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 481
    const/4 v9, 0x0

    .line 483
    .local v9, "accountType":Ljava/lang/String;
    if-eqz v14, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 485
    :cond_1
    const/4 v1, 0x2

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 486
    const/4 v1, 0x4

    invoke-interface {v14, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 487
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_1

    .line 490
    :cond_2
    if-eqz v10, :cond_5

    .line 491
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v15, v1, :cond_5

    .line 492
    invoke-virtual {v10, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    .end local v9    # "accountType":Ljava/lang/String;
    check-cast v9, Ljava/lang/String;

    .line 494
    .restart local v9    # "accountType":Ljava/lang/String;
    const-string v1, "com.sec.chaton"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 496
    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 491
    :cond_3
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 469
    .end local v6    # "where":Ljava/lang/String;
    .end local v7    # "args":[Ljava/lang/String;
    .end local v9    # "accountType":Ljava/lang/String;
    .end local v10    # "accountTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "contactId":Ljava/lang/String;
    .end local v15    # "i":I
    .end local v17    # "sync1List":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_4
    const/4 v1, 0x0

    aget-object v12, v13, v1

    goto :goto_1

    .line 504
    .restart local v10    # "accountTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "contactId":Ljava/lang/String;
    .restart local v17    # "sync1List":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_5
    if-eqz v14, :cond_6

    .line 505
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 510
    .end local v10    # "accountTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v12    # "contactId":Ljava/lang/String;
    .end local v13    # "contactIds":[Ljava/lang/String;
    .end local v14    # "cursor":Landroid/database/Cursor;
    .end local v17    # "sync1List":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    .end local v16    # "result":[Ljava/lang/String;
    check-cast v16, [Ljava/lang/String;

    .line 512
    .restart local v16    # "result":[Ljava/lang/String;
    goto/16 :goto_0

    .line 504
    .restart local v10    # "accountTypeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v12    # "contactId":Ljava/lang/String;
    .restart local v13    # "contactIds":[Ljava/lang/String;
    .restart local v14    # "cursor":Landroid/database/Cursor;
    .restart local v17    # "sync1List":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :catchall_0
    move-exception v1

    if-eqz v14, :cond_7

    .line 505
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v1
.end method

.method public getContactId(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 522
    const/4 v6, 0x0

    .line 523
    .local v6, "result":[Ljava/lang/String;
    if-nez p1, :cond_1

    .line 533
    :cond_0
    :goto_0
    return-object v3

    .line 527
    :cond_1
    const-string v0, "profile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 528
    const-string v3, "-1"

    goto :goto_0

    .line 531
    :cond_2
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 532
    .local v1, "uri":Landroid/net/Uri;
    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_ID_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 533
    if-eqz v6, :cond_0

    const/4 v0, 0x0

    aget-object v3, v6, v0

    goto :goto_0
.end method

.method public getContactInfoSize()I
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mInfoSize:I

    return v0
.end method

.method public getEmailAddresses(Ljava/lang/String;)[Ljava/lang/String;
    .locals 12
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 407
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->EMAIL_PROJECTION:[Ljava/lang/String;

    const-string v3, "lookup=? AND mimetype=\'vnd.android.cursor.item/email_v2\'"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v9

    const-string v5, "is_super_primary desc, _id asc"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 409
    .local v11, "result":[Ljava/lang/String;
    const-string v0, "profile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    sget-object v0, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "entities"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 411
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "mimetype = \'vnd.android.cursor.item/email_v2\'"

    .line 412
    .local v3, "selection":Ljava/lang/String;
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "data1"

    aput-object v0, v2, v9

    move-object v0, p0

    move-object v4, v6

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 415
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v3    # "selection":Ljava/lang/String;
    :cond_0
    if-nez v11, :cond_3

    .line 416
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getLookupKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 417
    .local v10, "newLookupKey":Ljava/lang/String;
    if-eqz v10, :cond_1

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object v0, v6

    .line 422
    .end local v10    # "newLookupKey":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 419
    .restart local v10    # "newLookupKey":Ljava/lang/String;
    :cond_2
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->EMAIL_PROJECTION:[Ljava/lang/String;

    const-string v7, "lookup=? AND mimetype=\'vnd.android.cursor.item/email_v2\'"

    new-array v8, v8, [Ljava/lang/String;

    aput-object v10, v8, v9

    const-string v9, "is_super_primary desc, _id asc"

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .end local v10    # "newLookupKey":Ljava/lang/String;
    :cond_3
    move-object v0, v11

    .line 422
    goto :goto_0
.end method

.method public getLookupKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 7
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 994
    const/4 v6, 0x0

    .line 995
    .local v6, "result":[Ljava/lang/String;
    sget-object v0, Landroid/provider/ContactsContract$Contacts;->CONTENT_LOOKUP_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 996
    .local v1, "uri":Landroid/net/Uri;
    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->LOOKUPKEY_PROJECTION:[Ljava/lang/String;

    move-object v0, p0

    move-object v4, v3

    move-object v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    .line 997
    if-nez v6, :cond_0

    :goto_0
    return-object v3

    :cond_0
    const/4 v0, 0x0

    aget-object v3, v6, v0

    goto :goto_0
.end method

.method public getPhoneNumbers(Ljava/lang/String;)[Ljava/lang/String;
    .locals 12
    .param p1, "lookupKey"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 388
    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->PHONE_PROJECTION:[Ljava/lang/String;

    const-string v3, "lookup=? AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    new-array v4, v8, [Ljava/lang/String;

    aput-object p1, v4, v9

    const-string v5, "is_super_primary desc, _id asc"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 390
    .local v11, "result":[Ljava/lang/String;
    const-string v0, "profile"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 391
    sget-object v0, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "entities"

    invoke-virtual {v0, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 392
    .local v1, "uri":Landroid/net/Uri;
    const-string v3, "mimetype = \'vnd.android.cursor.item/phone_v2\'"

    .line 393
    .local v3, "selection":Ljava/lang/String;
    new-array v2, v8, [Ljava/lang/String;

    const-string v0, "data1"

    aput-object v0, v2, v9

    move-object v0, p0

    move-object v4, v6

    move-object v5, v6

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .line 396
    .end local v1    # "uri":Landroid/net/Uri;
    .end local v3    # "selection":Ljava/lang/String;
    :cond_0
    if-nez v11, :cond_3

    .line 397
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getLookupKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 398
    .local v10, "newLookupKey":Ljava/lang/String;
    if-eqz v10, :cond_1

    invoke-virtual {v10, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move-object v0, v6

    .line 403
    .end local v10    # "newLookupKey":Ljava/lang/String;
    :goto_0
    return-object v0

    .line 400
    .restart local v10    # "newLookupKey":Ljava/lang/String;
    :cond_2
    sget-object v5, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->PHONE_PROJECTION:[Ljava/lang/String;

    const-string v7, "lookup=? AND mimetype=\'vnd.android.cursor.item/phone_v2\'"

    new-array v8, v8, [Ljava/lang/String;

    aput-object v10, v8, v9

    const-string v9, "is_super_primary desc, _id asc"

    move-object v4, p0

    invoke-direct/range {v4 .. v9}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getStringDataFromDB(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v11

    .end local v10    # "newLookupKey":Ljava/lang/String;
    :cond_3
    move-object v0, v11

    .line 403
    goto :goto_0
.end method

.method public getProfileLookupKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 896
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mProfileKey:Ljava/lang/String;

    return-object v0
.end method

.method public getRawContactId(Ljava/lang/String;)Ljava/lang/Long;
    .locals 10
    .param p1, "lookupkey"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x0

    .line 782
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isContactDBAvailable(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 812
    :goto_0
    return-object v9

    .line 786
    :cond_0
    const/4 v9, 0x0

    .line 787
    .local v9, "rawContactId":Ljava/lang/Long;
    const/4 v7, 0x0

    .line 790
    .local v7, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->getContactId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 792
    .local v6, "contactId":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 794
    const-string v3, "contact_id=?"

    .line 795
    .local v3, "where":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object v6, v4, v0

    .line 796
    .local v4, "args":[Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/ContactsContract$RawContacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->RAW_CONTACTS_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 800
    if-eqz v7, :cond_1

    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 801
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 808
    .end local v3    # "where":Ljava/lang/String;
    .end local v4    # "args":[Ljava/lang/String;
    :cond_1
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 810
    .end local v6    # "contactId":Ljava/lang/String;
    :goto_1
    const-string v0, "ContactProvider"

    const-string v1, "findContactIDWithName End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 805
    :catch_0
    move-exception v8

    .line 806
    .local v8, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v0, "ContactProvider"

    const-string v1, "Can not create contact list! "

    invoke-static {v0, v1, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 808
    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v8    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v0

    invoke-static {v7}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v0
.end method

.method public getSNSInformation(Landroid/content/Context;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "lookupKey"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 319
    const-string v2, "me_lookup_key"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-object v1

    .line 323
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    if-eqz v2, :cond_0

    .line 327
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 329
    .local v1, "snsInfoList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->hasInfo(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 331
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-virtual {v2, p2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->getInfo(Ljava/lang/String;)Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;

    move-result-object v0

    .line 332
    .local v0, "snsInfo":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    if-eqz v0, :cond_2

    .line 333
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 337
    .end local v0    # "snsInfo":Lcom/sec/android/gallery3d/remote/sns/RecogFaceSnsInfo;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/remote/sns/FacebookService;->getLatestData(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public getSNSInformation(Lcom/sec/android/gallery3d/provider/SnsData;Ljava/lang/String;)V
    .locals 18
    .param p1, "data"    # Lcom/sec/android/gallery3d/provider/SnsData;
    .param p2, "lookupKey"    # Ljava/lang/String;

    .prologue
    .line 344
    sget-object v2, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v4, "lookup"

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static/range {p2 .. p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v4, "stream_items"

    invoke-virtual {v2, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 348
    .local v3, "uri":Landroid/net/Uri;
    const/4 v13, 0x0

    .line 350
    .local v13, "cursor":Landroid/database/Cursor;
    const-string v5, "res_package =? or res_package =? or res_package =?"

    .line 352
    .local v5, "selection":Ljava/lang/String;
    const/4 v2, 0x3

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v4, "com.facebook.katana"

    aput-object v4, v6, v2

    const/4 v2, 0x1

    const-string v4, "com.google.android.apps.plus"

    aput-object v4, v6, v2

    const/4 v2, 0x2

    const-string v4, "com.twitter.android"

    aput-object v4, v6, v2

    .line 357
    .local v6, "args":[Ljava/lang/String;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v4, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACTS_SNS_PROJECTION:[Ljava/lang/String;

    const-string/jumbo v7, "timestamp DESC "

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 359
    if-eqz v13, :cond_0

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 360
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 361
    .local v8, "account_name":Ljava/lang/String;
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 362
    .local v9, "account_type":Ljava/lang/String;
    const/4 v2, 0x2

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 363
    .local v10, "res_package":Ljava/lang/String;
    const/4 v2, 0x3

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 364
    .local v11, "text":Ljava/lang/String;
    const/4 v2, 0x4

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    .local v12, "timestamp":Ljava/lang/Long;
    move-object/from16 v7, p1

    .line 372
    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/gallery3d/provider/SnsData;->setSnsData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 380
    .end local v8    # "account_name":Ljava/lang/String;
    .end local v9    # "account_type":Ljava/lang/String;
    .end local v10    # "res_package":Ljava/lang/String;
    .end local v11    # "text":Ljava/lang/String;
    .end local v12    # "timestamp":Ljava/lang/Long;
    :goto_0
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 383
    :goto_1
    return-void

    .line 376
    :cond_0
    :try_start_1
    const-string v2, "ContactProvider"

    const-string v4, "SNS information: cursor null"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 377
    :catch_0
    move-exception v14

    .line 378
    .local v14, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v2, "ContactProvider"

    const-string v4, "Can not create contact list! "

    invoke-static {v2, v4, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 380
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_1

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2
.end method

.method public getSnsService()Lcom/sec/android/gallery3d/remote/sns/FacebookService;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    return-object v0
.end method

.method public insertPhoto(Landroid/content/ContentValues;Landroid/net/Uri;Z)V
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "rawContactDataUri"    # Landroid/net/Uri;
    .param p3, "assertAccount"    # Z

    .prologue
    const/4 v7, 0x0

    .line 817
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 818
    .local v1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    if-eqz p3, :cond_0

    .line 821
    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newAssertQuery(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype=? AND data_set IS NULL AND (account_type IN (?,?) OR account_type IS NULL)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "vnd.android.cursor.item/photo"

    aput-object v5, v4, v7

    const/4 v5, 0x1

    const-string v6, "com.google"

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 836
    :cond_0
    const-string v2, "mimetype"

    const-string/jumbo v3, "vnd.android.cursor.item/photo"

    invoke-virtual {p1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 837
    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 840
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.contacts"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 854
    :goto_0
    return-void

    .line 842
    :catch_0
    move-exception v0

    .line 843
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Problem querying raw_contacts/data"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 845
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 847
    .local v0, "e":Landroid/content/OperationApplicationException;
    if-eqz p3, :cond_1

    .line 848
    invoke-virtual {p0, p1, p2, v7}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->updatePhoto(Landroid/content/ContentValues;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 850
    :cond_1
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Problem inserting photo into raw_contacts/data"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public setSnsService(Lcom/sec/android/gallery3d/remote/sns/FacebookService;)V
    .locals 0
    .param p1, "facebookService"    # Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    .prologue
    .line 314
    iput-object p1, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mSnsService:Lcom/sec/android/gallery3d/remote/sns/FacebookService;

    .line 315
    return-void
.end method

.method public showContactFinder(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x1

    .line 261
    new-instance v1, Landroid/content/Intent;

    const-string v2, "intent.action.INTERACTION_TAB"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 263
    .local v1, "phonebookIntent":Landroid/content/Intent;
    const-string v2, "additional"

    const-string v3, "email-phone-multi"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 264
    const-string v2, "existingRecipientCount"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 265
    const-string v2, "maxRecipientCount"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 268
    :try_start_0
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 272
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "ex":Landroid/content/ActivityNotFoundException;
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public syncAllContactInfoList(Landroid/content/Context;)V
    .locals 24
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 171
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->checkProviderState()Z

    move-result v2

    if-nez v2, :cond_0

    .line 258
    :goto_0
    return-void

    .line 174
    :cond_0
    const/4 v12, 0x0

    .line 176
    .local v12, "counter":I
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 181
    .local v8, "allLookupKeys":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;>;"
    const/4 v10, 0x0

    .line 182
    .local v10, "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    const/4 v13, 0x0

    .line 183
    .local v13, "cursor":Landroid/database/Cursor;
    const/16 v20, 0x0

    .line 184
    .local v20, "key":Ljava/lang/String;
    const/4 v15, 0x0

    .line 185
    .local v15, "head":Ljava/lang/String;
    const/16 v23, 0x0

    .line 186
    .local v23, "name":Ljava/lang/String;
    const/16 v21, 0x0

    .line 187
    .local v21, "keys":[Ljava/lang/String;
    const-wide/16 v18, 0x0

    .line 189
    .local v18, "id":J
    :try_start_0
    const-string v2, "ContactProvider"

    const-string v3, "getAllContactInfoList Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->CONTACT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 193
    if-eqz v13, :cond_3

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    move-object v11, v10

    .line 195
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .local v11, "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    const/4 v2, 0x0

    :try_start_1
    invoke-interface {v13, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    .line 196
    const/4 v2, 0x1

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    .line 197
    const/4 v2, 0x5

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    .line 200
    const-string v2, "."

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 201
    const-string v2, "\\."

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 202
    move-object/from16 v9, v21

    .local v9, "arr$":[Ljava/lang/String;
    array-length v0, v9

    move/from16 v22, v0

    .local v22, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_1
    move/from16 v0, v16

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    aget-object v17, v9, v16

    .line 203
    .local v17, "k":Ljava/lang/String;
    new-instance v10, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    invoke-direct {v10}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    .line 204
    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :try_start_2
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setId(Ljava/lang/Long;)V

    .line 205
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setDisplayName(Ljava/lang/String;)V

    .line 206
    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setSingleLookupKey(Ljava/lang/String;)V

    .line 207
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setJoinedLookupKey(Ljava/lang/String;)V

    .line 209
    add-int/lit8 v12, v12, 0x1

    .line 210
    const-string v2, "-"

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v15, v2, v3

    .line 211
    invoke-virtual {v8, v15, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    add-int/lit8 v16, v16, 0x1

    move-object v11, v10

    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    goto :goto_1

    .end local v17    # "k":Ljava/lang/String;
    :cond_2
    move-object v10, v11

    .line 225
    .end local v9    # "arr$":[Ljava/lang/String;
    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .end local v16    # "i$":I
    .end local v22    # "len$":I
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :goto_2
    invoke-interface {v13}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 230
    :cond_3
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 234
    :goto_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mProfileKey:Ljava/lang/String;

    .line 236
    :try_start_3
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "lookup"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 239
    if-eqz v13, :cond_4

    invoke-interface {v13}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 240
    const/4 v2, 0x0

    invoke-interface {v13, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mProfileKey:Ljava/lang/String;

    .line 241
    new-instance v11, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    invoke-direct {v11}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;-><init>()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 242
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :try_start_4
    const-string v2, "profile/Me"

    invoke-virtual {v11, v2}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setDisplayName(Ljava/lang/String;)V

    .line 243
    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setSingleLookupKey(Ljava/lang/String;)V

    .line 244
    move-object/from16 v0, v20

    invoke-virtual {v8, v0, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-object v10, v11

    .line 249
    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :cond_4
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 252
    :goto_4
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/sec/samsung/gallery/access/face/PersonList;->syncAllContacts(Landroid/content/Context;Ljava/util/HashMap;)V

    .line 254
    move-object/from16 v0, p0

    iput v12, v0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mInfoSize:I

    .line 257
    const-string v2, "ContactProvider"

    const-string v3, "getAllContactInfoList End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 215
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :cond_5
    :try_start_5
    new-instance v10, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;

    invoke-direct {v10}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;-><init>()V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    .line 216
    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :try_start_6
    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setId(Ljava/lang/Long;)V

    .line 217
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setDisplayName(Ljava/lang/String;)V

    .line 218
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;->setSingleLookupKey(Ljava/lang/String;)V

    .line 220
    add-int/lit8 v12, v12, 0x1

    .line 221
    const-string v2, "-"

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    aget-object v15, v2, v3

    .line 222
    invoke-virtual {v8, v15, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    .line 227
    :catch_0
    move-exception v14

    .line 228
    .local v14, "e":Ljava/lang/Exception;
    :goto_5
    :try_start_7
    const-string v2, "ContactProvider"

    const-string v3, "Can not create contact list! "

    invoke-static {v2, v3, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 230
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto/16 :goto_3

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    :goto_6
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .line 246
    :catch_1
    move-exception v14

    .line 247
    .restart local v14    # "e":Ljava/lang/Exception;
    :goto_7
    :try_start_8
    const-string v2, "ContactProvider"

    const-string v3, "Can not get a profileLookupKey! "

    invoke-static {v2, v3, v14}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    .line 249
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_4

    .end local v14    # "e":Ljava/lang/Exception;
    :catchall_1
    move-exception v2

    :goto_8
    invoke-static {v13}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v2

    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :catchall_2
    move-exception v2

    move-object v10, v11

    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    goto :goto_8

    .line 246
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :catch_2
    move-exception v14

    move-object v10, v11

    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    goto :goto_7

    .line 230
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :catchall_3
    move-exception v2

    move-object v10, v11

    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    goto :goto_6

    .line 227
    .end local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    :catch_3
    move-exception v14

    move-object v10, v11

    .end local v11    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    .restart local v10    # "contactMediaInfo":Lcom/sec/samsung/gallery/access/contact/ContactMediaInfo;
    goto :goto_5
.end method

.method public updatePhoto(Landroid/content/ContentValues;Landroid/net/Uri;Z)V
    .locals 8
    .param p1, "values"    # Landroid/content/ContentValues;
    .param p2, "rawContactDataUri"    # Landroid/net/Uri;
    .param p3, "allowInsert"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 863
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 864
    .local v1, "operations":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    const-string v2, "mimetype"

    invoke-virtual {p1, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 866
    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newAssertQuery(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype=?"

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "vnd.android.cursor.item/photo"

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withExpectedCount(I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 872
    invoke-static {p2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "mimetype=?"

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "vnd.android.cursor.item/photo"

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 878
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "com.android.contacts"

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 893
    :goto_0
    return-void

    .line 880
    :catch_0
    move-exception v0

    .line 881
    .local v0, "e":Landroid/os/RemoteException;
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Problem querying raw_contacts/data"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 883
    .end local v0    # "e":Landroid/os/RemoteException;
    :catch_1
    move-exception v0

    .line 884
    .local v0, "e":Landroid/content/OperationApplicationException;
    if-eqz p3, :cond_0

    .line 887
    invoke-virtual {p0, p1, p2, v6}, Lcom/sec/samsung/gallery/access/contact/ContactProvider;->insertPhoto(Landroid/content/ContentValues;Landroid/net/Uri;Z)V

    goto :goto_0

    .line 889
    :cond_0
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Problem inserting photo raw_contacts/data"

    invoke-direct {v2, v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method
