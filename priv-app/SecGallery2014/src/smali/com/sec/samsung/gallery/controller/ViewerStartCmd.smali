.class public Lcom/sec/samsung/gallery/controller/ViewerStartCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ViewerStartCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final BURST_SHOT_IMAGE_PROCESS:Ljava/lang/String; = "BurstShotImageProcess"

.field private static final BURST_SHOT_IMAGE_PROCESS_ACTION:Ljava/lang/String; = "android.intent.action.BurstShotImageProcess"

.field private static final TAG:Ljava/lang/String; = "ViewerStartCmd"


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 319
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ViewerStartCmd;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method private getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;
    .locals 6
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v3, 0x0

    .line 292
    if-nez p2, :cond_1

    move-object v1, v3

    .line 307
    :cond_0
    :goto_0
    return-object v1

    .line 296
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 298
    .local v1, "type":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 301
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 304
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 305
    :catch_0
    move-exception v0

    .line 306
    .local v0, "t":Ljava/lang/Throwable;
    const-string v4, "ViewerStartCmd"

    const-string v5, "Failed to get intent content type!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v1, v3

    .line 307
    goto :goto_0
.end method

.method private startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 2
    .param p2, "data"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<+",
            "Lcom/sec/android/gallery3d/app/ActivityState;",
            ">;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 313
    .local p1, "viewState":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 317
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    .line 315
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 48
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 61
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v42

    check-cast v42, [Ljava/lang/Object;

    move-object/from16 v28, v42

    check-cast v28, [Ljava/lang/Object;

    .line 62
    .local v28, "params":[Ljava/lang/Object;
    const/16 v42, 0x0

    aget-object v42, v28, v42

    check-cast v42, Landroid/app/Activity;

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    .line 63
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    check-cast v42, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 64
    const/16 v42, 0x1

    aget-object v18, v28, v42

    check-cast v18, Landroid/content/Intent;

    .line 65
    .local v18, "intent":Landroid/content/Intent;
    if-nez v18, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    move-object/from16 v2, v18

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v8

    .line 69
    .local v8, "contentType":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    if-eqz v42, :cond_2

    if-nez v8, :cond_3

    .line 70
    :cond_2
    const-string v42, "ViewerStartCmd"

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "Data Manager : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v44, v0

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v43

    const-string v44, ", Content Type : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    const v43, 0x7f0e0057

    invoke-static/range {v42 .. v43}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 76
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    check-cast v42, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v42 .. v42}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v34

    .line 78
    .local v34, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const-string v42, "SingleItemOnly"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v31

    .line 79
    .local v31, "singleItemOnly":Z
    const-string v42, "isFromGallery"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v20

    .line 80
    .local v20, "isFromGallery":Z
    const-string/jumbo v42, "useUriList"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v40

    .line 82
    .local v40, "useUriList":Z
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v15, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 83
    .local v15, "galleryActivity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    const-string v42, "from-Camera"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    .line 84
    .local v14, "fromCamera":Z
    invoke-virtual {v15, v14}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setFromCamera(Z)V

    .line 85
    const-string v42, "from-myfiles"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v42

    move/from16 v0, v42

    invoke-virtual {v15, v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->setFromMyFiles(Z)V

    .line 86
    const-string v42, "from-sw"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v42

    move-object/from16 v0, v34

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setLaunchFromSetupWidzard(Z)V

    .line 87
    const/16 v24, 0x0

    .line 88
    .local v24, "mediaSetPath":Ljava/lang/String;
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v13

    .line 91
    .local v13, "extras":Landroid/os/Bundle;
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/sconnect/SConnectUtil;->isSConnectIntent(Landroid/content/Intent;)Z

    move-result v42

    if-eqz v42, :cond_4

    .line 92
    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/sconnect/SConnectUtil;->getUris(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v39

    .line 93
    .local v39, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    if-eqz v39, :cond_0

    .line 94
    const/16 v42, 0x0

    move-object/from16 v0, v39

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Landroid/net/Uri;

    .line 95
    .local v37, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v21

    .line 96
    .local v21, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v21, :cond_0

    .line 98
    const-string v42, "KEY_MEDIA_SET_PATH"

    const-string v43, "/uriList"

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    const-string v42, "android.intent.extra.STREAM"

    move-object/from16 v0, v42

    move-object/from16 v1, v39

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 101
    const-string v42, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    const/16 v42, 0x1

    move/from16 v0, v42

    iput-boolean v0, v15, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsFromQuickStart:Z

    .line 103
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v13}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 105
    .end local v21    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v37    # "uri":Landroid/net/Uri;
    .end local v39    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v18

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->isSLinkIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v42

    if-eqz v42, :cond_7

    .line 106
    const/4 v9, 0x0

    .line 107
    .local v9, "cursor":Landroid/database/Cursor;
    const/16 v29, -0x1

    .line 108
    .local v29, "position":I
    const/16 v16, -0x1

    .line 109
    .local v16, "imageId":I
    const/4 v11, -0x1

    .line 111
    .local v11, "deviceId":I
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-static/range {v42 .. v42}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getCursorFromViewIntent(Landroid/content/Intent;)Landroid/database/Cursor;

    move-result-object v9

    .line 112
    if-eqz v9, :cond_5

    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v42

    if-nez v42, :cond_6

    .line 113
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->finish()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 121
    :catch_0
    move-exception v12

    .line 122
    .local v12, "e":Ljava/lang/Exception;
    invoke-virtual {v12}, Ljava/lang/Exception;->printStackTrace()V

    .line 125
    .end local v12    # "e":Ljava/lang/Exception;
    :goto_1
    const-string v42, "KEY_MEDIA_SET_PATH"

    const-string v43, "/slink/viewer"

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    const-string v42, "KEY_MEDIA_ITEM_PATH"

    const-string v43, "/slink/%d/%d"

    const/16 v44, 0x2

    move/from16 v0, v44

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v44, v0

    const/16 v45, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v46

    aput-object v46, v44, v45

    const/16 v45, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v46

    aput-object v46, v44, v45

    invoke-static/range {v43 .. v44}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    const-string v42, "KEY_ITEM_POSITION"

    move-object/from16 v0, v42

    move/from16 v1, v29

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const-string v43, "/slink/viewer"

    invoke-virtual/range {v42 .. v43}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v41

    check-cast v41, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;

    .line 130
    .local v41, "viewAlbum":Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;
    move-object/from16 v0, v41

    invoke-virtual {v0, v9}, Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;->init(Landroid/database/Cursor;)V

    .line 132
    const-string v42, "ViewerStartCmd"

    const-string v43, "multiple devices"

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const-string v42, "ViewerStartCmd"

    const-string v43, "media set path : /slink/viewer"

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    const-string v42, "ViewerStartCmd"

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "media item path : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    const-string v44, "/slink/%d/%d"

    const/16 v45, 0x2

    move/from16 v0, v45

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v45, v0

    const/16 v46, 0x0

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v47

    aput-object v47, v45, v46

    const/16 v46, 0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v47

    aput-object v47, v45, v46

    invoke-static/range {v44 .. v45}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    const-string v42, "ViewerStartCmd"

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "item position : "

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v13}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 116
    .end local v41    # "viewAlbum":Lcom/sec/android/gallery3d/remote/slink/SLinkViewAlbum;
    :cond_6
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    .line 117
    const-string v42, "_id"

    move-object/from16 v0, v42

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v42

    move/from16 v0, v42

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    .line 119
    const-string v42, "device_id"

    move-object/from16 v0, v42

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v42

    move/from16 v0, v42

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v11

    goto/16 :goto_1

    .line 139
    .end local v9    # "cursor":Landroid/database/Cursor;
    .end local v11    # "deviceId":I
    .end local v16    # "imageId":I
    .end local v29    # "position":I
    :cond_7
    if-eqz v40, :cond_8

    .line 140
    const-string/jumbo v42, "uriListData"

    move-object/from16 v0, v42

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v39

    check-cast v39, Ljava/util/ArrayList;

    .line 141
    .restart local v39    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const-string v42, "KEY_ITEM_POSITION"

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v42

    move-object/from16 v0, v39

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Landroid/net/Uri;

    .line 142
    .restart local v37    # "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v21

    .line 143
    .restart local v21    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    const-string v42, "KEY_MEDIA_SET_PATH"

    const-string v43, "/uriList"

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v42, "android.intent.extra.STREAM"

    move-object/from16 v0, v42

    move-object/from16 v1, v39

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 145
    const-string v42, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v13, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v13}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 150
    .end local v21    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v37    # "uri":Landroid/net/Uri;
    .end local v39    # "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_8
    if-eqz v13, :cond_b

    const-string v42, "query_uri"

    move-object/from16 v0, v42

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v42

    check-cast v42, Landroid/net/Uri;

    move-object/from16 v30, v42

    .line 151
    .local v30, "queryUri":Landroid/net/Uri;
    :goto_2
    if-eqz v30, :cond_c

    .line 152
    new-instance v42, Ljava/lang/StringBuilder;

    invoke-direct/range {v42 .. v42}, Ljava/lang/StringBuilder;-><init>()V

    const-string v43, "/allinone/"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    const/16 v43, 0x2

    invoke-static/range {v43 .. v43}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v43

    invoke-virtual/range {v42 .. v43}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    .line 158
    :goto_3
    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    .line 159
    .local v10, "data":Landroid/os/Bundle;
    invoke-virtual/range {v18 .. v18}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v37

    .line 161
    .restart local v37    # "uri":Landroid/net/Uri;
    if-eqz v8, :cond_e

    const-string/jumbo v42, "vnd.android.cursor.dir"

    move-object/from16 v0, v42

    invoke-virtual {v8, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v42

    if-eqz v42, :cond_e

    .line 162
    const-string v42, "mediaTypes"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v25

    .line 163
    .local v25, "mediaType":I
    if-eqz v25, :cond_9

    if-eqz v37, :cond_9

    .line 164
    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v42

    const-string v43, "mediaTypes"

    invoke-static/range {v25 .. v25}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v44

    invoke-virtual/range {v42 .. v44}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v37

    .line 170
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 171
    .local v6, "albumPath":Lcom/sec/android/gallery3d/data/Path;
    if-eqz v6, :cond_a

    .line 172
    const-string v42, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 175
    :cond_a
    if-eqz v6, :cond_d

    .line 176
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v10}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 150
    .end local v6    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "data":Landroid/os/Bundle;
    .end local v25    # "mediaType":I
    .end local v30    # "queryUri":Landroid/net/Uri;
    .end local v37    # "uri":Landroid/net/Uri;
    :cond_b
    const/16 v30, 0x0

    goto/16 :goto_2

    .line 154
    .restart local v30    # "queryUri":Landroid/net/Uri;
    :cond_c
    const-string v42, "KEY_MEDIA_SET_PATH"

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v24

    goto :goto_3

    .line 178
    .restart local v6    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v10    # "data":Landroid/os/Bundle;
    .restart local v25    # "mediaType":I
    .restart local v37    # "uri":Landroid/net/Uri;
    :cond_d
    const-class v42, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v10}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 183
    .end local v6    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v25    # "mediaType":I
    :cond_e
    if-eqz v37, :cond_f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExist(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/Boolean;

    move-result-object v42

    invoke-virtual/range {v42 .. v42}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v42

    if-nez v42, :cond_f

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    const v43, 0x7f0e0057

    invoke-static/range {v42 .. v43}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    new-instance v43, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;

    move-object/from16 v0, v43

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)V

    invoke-virtual/range {v42 .. v43}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 196
    :cond_f
    if-eqz v37, :cond_11

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v42

    const-string v43, "content://mms/part/"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v42

    if-eqz v42, :cond_11

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v22

    .line 198
    .local v22, "itemPath1":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v42

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getMidFromMmsList(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v26

    .line 199
    .local v26, "mid":J
    new-instance v38, Ljava/util/ArrayList;

    invoke-direct/range {v38 .. v38}, Ljava/util/ArrayList;-><init>()V

    .line 200
    .local v38, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v42

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v38

    move-object/from16 v2, v43

    move-wide/from16 v3, v26

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getIndexFromMmsList(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;J)I

    move-result v17

    .line 202
    .local v17, "index":I
    const-string v42, "KEY_MEDIA_SET_PATH"

    new-instance v43, Ljava/lang/StringBuilder;

    invoke-direct/range {v43 .. v43}, Ljava/lang/StringBuilder;-><init>()V

    const-string v44, "/uri/mediaset/"

    invoke-virtual/range {v43 .. v44}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v43

    move-object/from16 v0, v43

    move-wide/from16 v1, v26

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v43

    invoke-virtual/range {v43 .. v43}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    if-eqz v22, :cond_10

    .line 205
    const-string v42, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_10
    const-string v42, "KEY_ITEM_POSITION"

    move-object/from16 v0, v42

    move/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 208
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v10}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 211
    .end local v17    # "index":I
    .end local v22    # "itemPath1":Lcom/sec/android/gallery3d/data/Path;
    .end local v26    # "mid":J
    .end local v38    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    :cond_11
    if-eqz v37, :cond_12

    invoke-virtual/range {v37 .. v37}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v42

    const-string v43, "content://rcs/"

    invoke-virtual/range {v42 .. v43}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v42

    if-eqz v42, :cond_12

    .line 212
    const-string v42, "KEY_ITEM_RCS"

    const/16 v43, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 215
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, v37

    move-object/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v21

    .line 216
    .restart local v21    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getDefaultSetOf(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    .line 218
    .restart local v6    # "albumPath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v21, :cond_13

    .line 219
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    const v43, 0x7f0e0057

    invoke-static/range {v42 .. v43}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 224
    :cond_13
    const/16 v35, 0x0

    .line 226
    .local v35, "strAlbumPath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v31, :cond_14

    if-eqz v6, :cond_14

    .line 227
    const-string v42, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    move-object/from16 v35, v6

    .line 231
    :cond_14
    if-eqz v24, :cond_15

    .line 232
    const-string v42, "KEY_MEDIA_SET_PATH"

    move-object/from16 v0, v42

    move-object/from16 v1, v24

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    invoke-static/range {v24 .. v24}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v35

    .line 236
    :cond_15
    if-eqz v35, :cond_16

    .line 237
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    move-object/from16 v42, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v23

    check-cast v23, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 239
    .local v23, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v23, :cond_16

    if-nez v20, :cond_16

    .line 240
    const/16 v42, 0x1

    move-object/from16 v0, v34

    move/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setUpButtonVisible(Z)V

    .line 243
    .end local v23    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_16
    const/16 v42, 0x1

    move/from16 v0, v42

    iput-boolean v0, v15, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsInViewMode:Z

    .line 244
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromCamera()Z

    move-result v42

    if-nez v42, :cond_17

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/app/GalleryActivity;->isFromMyFiles()Z

    move-result v42

    if-eqz v42, :cond_18

    .line 245
    :cond_17
    sget-object v42, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v34

    move-object/from16 v1, v42

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 248
    :cond_18
    const-string v42, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v43

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v42, "mime-type"

    move-object/from16 v0, v42

    invoke-virtual {v10, v0, v8}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string/jumbo v42, "view-mode"

    const/16 v43, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 251
    const-string v42, "Quick_Time"

    const-wide/16 v44, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move-wide/from16 v2, v44

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v32

    .line 253
    .local v32, "quickTime":J
    const-string v42, "Quick_Time"

    move-object/from16 v0, v42

    move-wide/from16 v1, v32

    invoke-virtual {v10, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 255
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v42

    if-nez v42, :cond_19

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isMEnabled()Z

    move-result v42

    if-eqz v42, :cond_1a

    .line 256
    :cond_19
    const-string v42, "BurstShotImageProcess"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 257
    .local v7, "burstshotImageProcess":Z
    if-eqz v7, :cond_1a

    .line 258
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v42

    check-cast v42, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface/range {v42 .. v42}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v42

    const/16 v43, 0x0

    invoke-virtual/range {v42 .. v43}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 260
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v42, v0

    if-nez v42, :cond_1a

    .line 261
    new-instance v42, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;

    const/16 v43, 0x0

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    move-object/from16 v2, v43

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$BurstShotBroadcastReceiver;-><init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;)V

    move-object/from16 v0, v42

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 262
    new-instance v19, Landroid/content/IntentFilter;

    const-string v42, "android.intent.action.BurstShotImageProcess"

    move-object/from16 v0, v19

    move-object/from16 v1, v42

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 263
    .local v19, "intentFilter":Landroid/content/IntentFilter;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v42, v0

    invoke-virtual/range {v42 .. v42}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v42

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mBurstShotBroadcastReceiver:Landroid/content/BroadcastReceiver;

    move-object/from16 v43, v0

    move-object/from16 v0, v42

    move-object/from16 v1, v43

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 270
    .end local v7    # "burstshotImageProcess":Z
    .end local v19    # "intentFilter":Landroid/content/IntentFilter;
    :cond_1a
    sget-boolean v42, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v42, :cond_1c

    .line 271
    sget-boolean v42, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSPCDMR:Z

    if-eqz v42, :cond_1b

    .line 272
    if-eqz v18, :cond_1b

    const-string v42, "DMR"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v42

    if-eqz v42, :cond_1b

    .line 273
    const-string v42, "ViewerStartCmd"

    const-string v43, "DMR !!!"

    invoke-static/range {v42 .. v43}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    const-string v42, "KEY_SPC_DMR_MODE"

    const/16 v43, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 278
    :cond_1b
    if-eqz v18, :cond_1c

    const-string v42, "FromSpcGalleryWidget"

    const/16 v43, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v42

    if-eqz v42, :cond_1c

    .line 279
    const-string v42, "TotalCount"

    const/16 v43, 0x10

    move-object/from16 v0, v18

    move-object/from16 v1, v42

    move/from16 v2, v43

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v36

    .line 280
    .local v36, "totalCount":I
    const-string v42, "KEY_FROM_SPC_GALLERY_WIDGET"

    const/16 v43, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 281
    const-string v42, "KEY_SPC_GALLERY_WIDGET_MAX_CNT"

    move-object/from16 v0, v42

    move/from16 v1, v36

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 285
    .end local v36    # "totalCount":I
    :cond_1c
    if-eqz v14, :cond_1d

    .line 286
    const-string v42, "in_camera_roll"

    const/16 v43, 0x1

    move-object/from16 v0, v42

    move/from16 v1, v43

    invoke-virtual {v10, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 288
    :cond_1d
    const-class v42, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v0, p0

    move-object/from16 v1, v42

    invoke-direct {v0, v1, v10}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->startViewState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
