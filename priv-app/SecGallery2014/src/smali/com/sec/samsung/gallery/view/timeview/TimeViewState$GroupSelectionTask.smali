.class Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;
.super Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;
.source "TimeViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GroupSelectionTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/android/gallery3d/data/OnProgressListener;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "position"    # I
    .param p5, "onProgressListener"    # Lcom/sec/android/gallery3d/data/OnProgressListener;

    .prologue
    .line 3040
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 3041
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/view/utils/SelectionUpdateTaskForTimeGroup;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;ILcom/sec/android/gallery3d/data/OnProgressListener;)V

    .line 3042
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 3039
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 3046
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->mPosition:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v0

    .line 3047
    .local v0, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->access$1100(Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    .line 3048
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState$GroupSelectionTask;->setFinishingState()V

    .line 3049
    const/4 v1, 0x0

    return-object v1
.end method
