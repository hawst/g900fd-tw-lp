.class public Lcom/sec/samsung/gallery/util/DownloadUtil;
.super Ljava/lang/Object;
.source "DownloadUtil.java"


# static fields
.field private static final KEY_DOWNLOAD:Ljava/lang/String; = "Download"

.field private static final PATH:Ljava/lang/String;

.field private static final TAG:Ljava/lang/String; = "DownloadUtil"

.field private static downloadReceiver:Landroid/content/BroadcastReceiver;

.field private static mHandlerDownloadInfo:Landroid/os/Handler;

.field private static mIDArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private static mRunnableDownloadCheck:Ljava/lang/Runnable;

.field private static smIsReceiverRegistered:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "/Download/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->PATH:Ljava/lang/String;

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    .line 30
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    .line 60
    new-instance v0, Lcom/sec/samsung/gallery/util/DownloadUtil$1;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/util/DownloadUtil$1;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadReceiver:Landroid/content/BroadcastReceiver;

    .line 170
    sput-object v2, Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;

    .line 172
    sput-object v2, Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 24
    sget-boolean v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    return v0
.end method

.method static synthetic access$200()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;

    return-object v0
.end method

.method public static checkDownloadStatus(Landroid/content/Context;J)V
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "downloadedId"    # J

    .prologue
    const/4 v10, 0x0

    .line 87
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;

    sget-object v8, Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;

    invoke-virtual {v7, v8}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 88
    const-string v7, "download"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/DownloadManager;

    .line 90
    .local v1, "downloadManager":Landroid/app/DownloadManager;
    new-instance v7, Landroid/app/DownloadManager$Query;

    invoke-direct {v7}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [J

    aput-wide p1, v8, v10

    invoke-virtual {v7, v8}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v3

    .line 91
    .local v3, "query":Landroid/app/DownloadManager$Query;
    invoke-virtual {v1, v3}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v0

    .line 93
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 94
    const-string/jumbo v7, "status"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 95
    .local v5, "state":I
    const-string v7, "reason"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 96
    .local v4, "reason":I
    const-string v7, "DownloadUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Downloaded an item. state : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", reason : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    const/16 v7, 0x10

    if-ne v5, v7, :cond_3

    .line 98
    const v6, 0x7f0e0141

    .line 99
    .local v6, "strResId":I
    packed-switch v4, :pswitch_data_0

    .line 123
    :goto_0
    :pswitch_0
    invoke-static {p0, v6}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 124
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    .end local v4    # "reason":I
    .end local v5    # "state":I
    .end local v6    # "strResId":I
    :cond_0
    :goto_1
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 148
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 149
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 150
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 151
    sget-boolean v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    if-eqz v7, :cond_1

    .line 152
    const-string v7, "DownloadUtil"

    const-string/jumbo v8, "unregister download receiver"

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :try_start_1
    sget-object v7, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v7}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 160
    :goto_2
    sput-boolean v10, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    .line 164
    :cond_1
    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.MEDIA_SCAN"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "file://"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    sget-object v10, Lcom/sec/samsung/gallery/util/DownloadUtil;->PATH:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v7}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 168
    :cond_2
    :goto_3
    return-void

    .line 101
    .restart local v4    # "reason":I
    .restart local v5    # "state":I
    .restart local v6    # "strResId":I
    :pswitch_1
    const v6, 0x7f0e013b

    .line 102
    goto :goto_0

    .line 104
    :pswitch_2
    const v6, 0x7f0e013c

    .line 105
    goto :goto_0

    .line 107
    :pswitch_3
    const v6, 0x7f0e013d

    .line 108
    goto :goto_0

    .line 110
    :pswitch_4
    const v6, 0x7f0e013e

    .line 111
    goto :goto_0

    .line 115
    :pswitch_5
    const v6, 0x7f0e013f

    .line 116
    goto :goto_0

    .line 118
    :pswitch_6
    const v6, 0x7f0e0140

    .line 119
    goto :goto_0

    .line 125
    .end local v6    # "strResId":I
    :cond_3
    const/16 v7, 0x8

    if-ne v5, v7, :cond_4

    .line 127
    const v7, 0x7f0e0131

    :try_start_2
    invoke-static {p0, v7}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 145
    .end local v4    # "reason":I
    .end local v5    # "state":I
    :catchall_0
    move-exception v7

    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v7

    .line 128
    .restart local v4    # "reason":I
    .restart local v5    # "state":I
    :cond_4
    const/4 v7, 0x4

    if-ne v5, v7, :cond_5

    .line 129
    packed-switch v4, :pswitch_data_1

    .line 145
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_3

    .line 132
    :pswitch_7
    :try_start_3
    const-string v7, "DownloadUtil"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Download cancelled : ItemID- "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ","

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 145
    :cond_5
    invoke-static {v0}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    goto :goto_3

    .line 155
    .end local v4    # "reason":I
    .end local v5    # "state":I
    :catch_0
    move-exception v2

    .line 156
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v7, "DownloadUtil"

    const-string v8, "download receiver is not registered. no need to unregister receiver"

    invoke-static {v7, v8, v2}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 99
    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_6
        :pswitch_2
        :pswitch_5
        :pswitch_0
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 129
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public static download(Landroid/content/Context;Landroid/app/DownloadManager$Request;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "request"    # Landroid/app/DownloadManager$Request;

    .prologue
    .line 39
    const-string v4, "download"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 42
    .local v0, "downloadManager":Landroid/app/DownloadManager;
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    move-result-wide v2

    .line 43
    .local v2, "enqueuedId":J
    sget-object v4, Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 44
    invoke-static {p0}, Lcom/sec/samsung/gallery/util/DownloadUtil;->registerDownloadReceiver(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    .end local v2    # "enqueuedId":J
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method public static downloadEnqueue(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 211
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/sec/samsung/gallery/util/DownloadUtil$3;

    invoke-direct {v1, p1, p0}, Lcom/sec/samsung/gallery/util/DownloadUtil$3;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    const-string v2, "DownloadThread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 221
    return-void
.end method

.method public static getDownloadDir(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1
    .param p0, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 57
    const-string v0, "Download"

    invoke-static {p0, v0}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static registerDownloadReceiver(Landroid/content/Context;)V
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 181
    sget-boolean v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    if-eqz v1, :cond_0

    .line 208
    :goto_0
    return-void

    .line 184
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 185
    .local v0, "downloadFilter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 186
    const-string v1, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 188
    const-string v1, "DownloadUtil"

    const-string v2, "register download receiver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    sget-object v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 190
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z

    .line 192
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;

    .line 193
    new-instance v1, Lcom/sec/samsung/gallery/util/DownloadUtil$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/util/DownloadUtil$2;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;

    .line 207
    sget-object v1, Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;

    sget-object v2, Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;

    const-wide/16 v4, 0x1388

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
