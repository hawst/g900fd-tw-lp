.class public Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartMmsSaveCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd$CallMmsSave;
    }
.end annotation


# static fields
.field public static final DOORFLIP2EFFECT:I = 0x6

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 76
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 22
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v2, v4

    check-cast v2, [Ljava/lang/Object;

    .line 23
    .local v2, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v0, v2, v4

    check-cast v0, Landroid/app/Activity;

    .line 24
    .local v0, "activity":Landroid/app/Activity;
    const/4 v4, 0x1

    aget-object v3, v2, v4

    check-cast v3, Landroid/net/Uri;

    .line 31
    .local v3, "partUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/sec/samsung/gallery/controller/StartMmsSaveCmd$CallMmsSave;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 32
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 33
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 54
    return-void
.end method
