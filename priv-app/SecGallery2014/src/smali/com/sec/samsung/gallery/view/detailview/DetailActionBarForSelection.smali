.class public Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;
.source "DetailActionBarForSelection.java"


# instance fields
.field private mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mIsDeleteOnly:Z

.field private mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "deleteOnly"    # Z

    .prologue
    .line 39
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Z)V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 47
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 48
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 49
    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mIsDeleteOnly:Z

    .line 51
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->refreshRotationIcon()V

    .line 54
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->initActionBarBg()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->setSelectAllButtonTitle(ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->setSelectAllButtonTTS(Ljava/lang/String;)V

    return-void
.end method

.method private getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 353
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_0

    .line 354
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 355
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 356
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 359
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getTagType()Lcom/sec/samsung/gallery/core/TabTagType;
    .locals 2

    .prologue
    .line 375
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSourceMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 376
    .local v0, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_0
    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    goto :goto_0
.end method

.method private initActionBarBg()V
    .locals 4

    .prologue
    .line 57
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ab

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 60
    :cond_0
    return-void
.end method

.method private isValidSelection()Z
    .locals 8

    .prologue
    .line 275
    const/4 v0, 0x1

    .line 277
    .local v0, "isValid":Z
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 278
    .local v1, "selectedCount":I
    if-nez v1, :cond_1

    .line 279
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0e0113

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 280
    const/4 v0, 0x0

    .line 288
    :cond_0
    :goto_0
    return v0

    .line 281
    :cond_1
    sget v3, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v1, v3, :cond_0

    .line 282
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0114

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    sget v7, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 284
    .local v2, "text":Ljava/lang/CharSequence;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 285
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateLastShareAppVisibility(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 369
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-wide/16 v4, 0x4

    invoke-static {v3, p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v0

    .line 370
    .local v0, "lastShareSupported":J
    const-wide v4, 0x80000000000L

    and-long/2addr v4, v0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-eqz v3, :cond_0

    const/4 v2, 0x1

    .line 371
    .local v2, "showLastShare":Z
    :goto_0
    const v3, 0x7f0f026d

    invoke-static {p1, v3, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 372
    return-void

    .line 370
    .end local v2    # "showLastShare":Z
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->isRotationMenuItemEnabled()Z

    move-result v0

    .line 384
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v3, 0x7f0f0298

    const/4 v2, 0x2

    .line 64
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 65
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 66
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f12001a

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 67
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v1, :cond_0

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-nez v1, :cond_0

    .line 68
    const v1, 0x7f0f026c

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 70
    const v1, 0x7f0f026e

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 72
    const v1, 0x7f0f0276

    invoke-static {p1, v1, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 76
    :cond_0
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_1

    .line 77
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_1
    invoke-interface {p1, v3}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 6
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 199
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-nez v2, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_DELAY_HIDE_BARS:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    .line 205
    :try_start_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 207
    :sswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->isValidSelection()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->chooseShareDialog()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 212
    .end local v0    # "e":Ljava/lang/Exception;
    :sswitch_1
    :try_start_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->isValidSelection()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->isSelected3DTour(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 215
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v3, 0x1

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    goto :goto_0

    .line 217
    :cond_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    :sswitch_2
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUndoDeletion:Z

    if-eqz v2, :cond_5

    .line 224
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACTION_DELETE:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 226
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->showDeleteDialog()V

    goto :goto_0

    .line 230
    :sswitch_3
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v2, :cond_0

    .line 231
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 235
    :sswitch_4
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 238
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->hideAlbums(ZZZ)V

    goto/16 :goto_0

    .line 241
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToSecretbox(ZZ)V

    goto/16 :goto_0

    .line 244
    :sswitch_7
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 247
    :sswitch_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 250
    :sswitch_9
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->moveToKNOX(ZZ)V

    goto/16 :goto_0

    .line 254
    :sswitch_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->download()V

    goto/16 :goto_0

    .line 257
    :sswitch_b
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_CATEGORY:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 260
    :sswitch_c
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/16 v3, 0x205

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAddUserTagDialog(IZ)V

    goto/16 :goto_0

    .line 263
    :sswitch_d
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 264
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "S_STUDIO"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 205
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f026c -> :sswitch_0
        0x7f0f026d -> :sswitch_1
        0x7f0f026e -> :sswitch_2
        0x7f0f026f -> :sswitch_4
        0x7f0f0271 -> :sswitch_5
        0x7f0f0276 -> :sswitch_a
        0x7f0f0277 -> :sswitch_6
        0x7f0f0278 -> :sswitch_7
        0x7f0f0279 -> :sswitch_8
        0x7f0f027a -> :sswitch_9
        0x7f0f0282 -> :sswitch_c
        0x7f0f0283 -> :sswitch_d
        0x7f0f0297 -> :sswitch_b
        0x7f0f0298 -> :sswitch_3
        0x7f0f02b6 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 338
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause()V

    .line 339
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 342
    :cond_0
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 85
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getSupportedOperationForSelectedItem()J

    move-result-wide v2

    .line 86
    .local v2, "supportOperation":J
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mIsDeleteOnly:Z

    if-eqz v6, :cond_c

    .line 87
    const-wide/16 v2, 0x1

    .line 92
    :goto_0
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 93
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 95
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v6, :cond_f

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/samsung/android/privatemode/PrivateModeManager;->isPrivateStorageMounted(Landroid/content/Context;)Z

    move-result v6

    if-nez v6, :cond_1

    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isReadyPrivateMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_f

    .line 97
    :cond_1
    const-wide/32 v6, 0x40000000

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_d

    .line 98
    const v6, 0x7f0f0277

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 102
    :goto_1
    const-wide/32 v6, 0x20000000

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_e

    .line 103
    const v6, 0x7f0f0278

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 104
    const v6, 0x7f0f0271

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 112
    :goto_2
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuHide:Z

    if-eqz v6, :cond_2

    .line 113
    const v6, 0x7f0f0271

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 115
    :cond_2
    const-wide v6, 0x80000000000L

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_3

    .line 116
    const-wide/32 v6, 0x8000000

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_10

    const/4 v5, 0x1

    .line 118
    .local v5, "visibility":Z
    :goto_3
    const v6, 0x7f0f026d

    invoke-static {p1, v6, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 121
    .end local v5    # "visibility":Z
    :cond_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsGuestMode:Z

    if-nez v6, :cond_14

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOXInstallMode(Landroid/content/Context;)Z

    move-result v6

    const/4 v7, 0x1

    if-ne v6, v7, :cond_14

    .line 122
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKNOX(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_12

    .line 123
    const v6, 0x7f0f0279

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 124
    const-wide/32 v6, 0x1000000

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_11

    .line 125
    const v6, 0x7f0f027a

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 142
    :goto_4
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseLastShare:Z

    if-nez v6, :cond_4

    .line 143
    const v6, 0x7f0f026d

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 146
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->getTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    .line 147
    .local v4, "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALL:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v6, :cond_5

    .line 148
    const v6, 0x7f0f0271

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 151
    :cond_5
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v6, :cond_6

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isDeleteIconDisabled()Z

    move-result v6

    if-eqz v6, :cond_6

    .line 152
    const v6, 0x7f0f026e

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 155
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_15

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_15

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v6

    if-nez v6, :cond_15

    .line 156
    const v6, 0x7f0f026e

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 157
    const v6, 0x7f0f02b6

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 158
    const v6, 0x7f0f0271

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 159
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSupportDeleteFromMapView(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 160
    const v6, 0x7f0f026e

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 161
    const v6, 0x7f0f02b6

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 167
    :cond_7
    :goto_5
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v6

    if-eqz v6, :cond_17

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v6, :cond_17

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->isHiddenItem()Z

    move-result v6

    if-nez v6, :cond_17

    .line 168
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-eqz v6, :cond_16

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/StateManager;->getPreviousState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v6

    instance-of v6, v6, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    if-nez v6, :cond_16

    .line 169
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v6, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v7, v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 170
    .local v1, "category_name":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v7, 0x7f0e0463

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 171
    .local v0, "category_menu":Ljava/lang/String;
    const v6, 0x7f0f0297

    invoke-static {p1, v6, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemTitle(Landroid/view/Menu;ILjava/lang/String;)V

    .line 172
    const v6, 0x7f0f0297

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 176
    .end local v0    # "category_menu":Ljava/lang/String;
    .end local v1    # "category_name":Ljava/lang/String;
    :goto_6
    const v6, 0x7f0f0271

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 177
    const v6, 0x7f0f026e

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 182
    :goto_7
    sget-object v6, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SEARCH:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v6, :cond_8

    .line 183
    const v6, 0x7f0f0297

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 186
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->isSStudioAvailable(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_9

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_a

    .line 187
    :cond_9
    const v6, 0x7f0f0283

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 190
    :cond_a
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v6, :cond_b

    .line 191
    const v6, 0x7f0f0282

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 194
    :cond_b
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 195
    return-void

    .line 89
    .end local v4    # "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_c
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6, p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkLastShareAppSupport(Landroid/content/Context;Landroid/view/Menu;J)J

    move-result-wide v2

    goto/16 :goto_0

    .line 100
    :cond_d
    const v6, 0x7f0f0277

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_1

    .line 106
    :cond_e
    const v6, 0x7f0f0278

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 109
    :cond_f
    const v6, 0x7f0f0277

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 110
    const v6, 0x7f0f0278

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_2

    .line 116
    :cond_10
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 127
    :cond_11
    const v6, 0x7f0f027a

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 130
    :cond_12
    const v6, 0x7f0f027a

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 131
    const-wide/32 v6, 0x2000000

    and-long/2addr v6, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_13

    .line 132
    const v6, 0x7f0f0279

    const/4 v7, 0x1

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 134
    :cond_13
    const v6, 0x7f0f0279

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 138
    :cond_14
    const v6, 0x7f0f0279

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 139
    const v6, 0x7f0f027a

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_4

    .line 164
    .restart local v4    # "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    :cond_15
    const v6, 0x7f0f02b6

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_5

    .line 174
    :cond_16
    const v6, 0x7f0f0297

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_6

    .line 179
    :cond_17
    const v6, 0x7f0f0297

    const/4 v7, 0x0

    invoke-static {p1, v6, v7}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto/16 :goto_7
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 346
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onResume()V

    .line 347
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 350
    :cond_0
    return-void
.end method

.method protected setTitle(I)V
    .locals 2
    .param p1, "numberOfItemsSelected"    # I

    .prologue
    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;I)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 324
    :cond_0
    return-void
.end method

.method public showDeleteDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 363
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_LOCK_TOUCH:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->notifyObservers(Ljava/lang/Object;)V

    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 365
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 366
    return-void
.end method

.method public unregisterOrientationListener()V
    .locals 1

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSelection;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->unregisterListener()V

    .line 392
    :cond_0
    return-void
.end method
