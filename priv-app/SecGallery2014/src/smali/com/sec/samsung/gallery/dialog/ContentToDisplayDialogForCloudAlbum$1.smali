.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;
.super Ljava/lang/Object;
.source "ContentToDisplayDialogForCloudAlbum.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    const/4 v1, 0x0

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->dismissDialog()V

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$100(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v3

    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$100(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v4, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$100(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewByType(IZ)V

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v2, "VIEW_BY_TYPE_UPDATED"

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$1;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v2, "UPDATE_CATEGORY"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 158
    return-void

    :cond_1
    move v0, v1

    .line 149
    goto :goto_0
.end method
