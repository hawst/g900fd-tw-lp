.class Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;
.super Ljava/lang/Object;
.source "GlComposePhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 453
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mNotNeedToResetLayout:Z

    .line 454
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v0, :cond_0

    .line 455
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setEnabled(Z)V

    .line 456
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 0
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 460
    return-void
.end method

.method public onAnimationStart(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V
    .locals 2
    .param p1, "animation"    # Lcom/sec/android/gallery3d/glcore/GlAnimationBase;

    .prologue
    .line 447
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView$2;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->setEnabled(Z)V

    .line 449
    :cond_0
    return-void
.end method
