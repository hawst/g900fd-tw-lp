.class public Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
.super Ljava/lang/Object;
.source "SlideShowSetting.java"


# static fields
.field public static final DEFAULT_EFFECT:I = 0x0

.field public static final DEFAULT_FILTER:I = 0x1

.field public static final DEFAULT_IMAGEINDEX:I = 0x0

.field public static final DEFAULT_SORT:I = 0x1

.field public static final DEFAULT_SPEED:I = 0x1

.field public static final DEFAULT_TAB:I = 0x0

.field public static final GREY_FILTER:I = 0x0

.field public static final INDEX_CURRENT_TAB:I = 0x6

.field private static final INDEX_EFFECT:I = 0x1

.field public static final INDEX_FILTER:I = 0x4

.field public static final INDEX_IMAGE:I = 0x8

.field public static final INDEX_MUSIC:I = 0x3

.field public static final INDEX_MUSICS_ADDED:I = 0x2

.field public static final INDEX_MUSIC_ENABLE:I = 0x5

.field public static final INDEX_MUSIC_EXTRA_URI:I = 0x6

.field public static final INDEX_SPEED:I = 0x7

.field public static final INDEX_VIDEOPLAY:I = 0x9

.field private static final KEY_CUURENT_TAB:Ljava/lang/String; = "curr_tab"

.field private static final KEY_EFFECT:Ljava/lang/String; = "effect-mode"

.field private static final KEY_FILTER:Ljava/lang/String; = "filter"

.field private static final KEY_IMAGEINDEX:Ljava/lang/String; = "image-index"

.field public static final KEY_MUSIC:Ljava/lang/String; = "music-uri"

.field private static final KEY_MUSICS_ADDED:Ljava/lang/String; = "musics-added"

.field public static final KEY_MUSIC_ENABLED:Ljava/lang/String; = "music-enabled"

.field public static final KEY_MUSIC_EXTRA_URI:Ljava/lang/String; = "music-extra"

.field private static final KEY_SORT:Ljava/lang/String; = "sort"

.field private static final KEY_SPEED:Ljava/lang/String; = "slideshow-speed"

.field private static final KEY_VIDEO_PLAY:Ljava/lang/String; = "slideshow-videoplay"

.field public static final NO_MUSIC:Ljava/lang/String; = ""

.field public static final SEPIA_FILTER:I = 0x2

.field public static final SLIDE_SHOW_PREFS:Ljava/lang/String; = "slideShowPrerences"

.field public static final SORT_BY_NEWEST:I = 0x1

.field public static final SORT_BY_OLDEST:I = 0x0

.field private static final TAG:Ljava/lang/String;

.field public static final VINTAGE_FILTER:I = 0x3


# instance fields
.field private mSetting:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    return-void
.end method

.method public static getEffectSelected(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 195
    const-string/jumbo v1, "slideShowPrerences"

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 197
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "effect-mode"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    return v1
.end method

.method public static getEffectTimeMsec(I)I
    .locals 1
    .param p0, "speedIndex"    # I

    .prologue
    const/16 v0, 0xbb8

    .line 233
    packed-switch p0, :pswitch_data_0

    .line 241
    :goto_0
    :pswitch_0
    return v0

    .line 235
    :pswitch_1
    const/16 v0, 0x3e8

    goto :goto_0

    .line 239
    :pswitch_2
    const/16 v0, 0x1388

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static getMusicSelected(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    const-string/jumbo v1, "slideShowPrerences"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 203
    .local v0, "prefs":Landroid/content/SharedPreferences;
    const-string v1, "music-uri"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static load(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    .locals 15
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 154
    new-instance v11, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    invoke-direct {v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;-><init>()V

    .line 156
    .local v11, "setting":Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;
    const-string/jumbo v13, "slideShowPrerences"

    const/4 v14, 0x0

    invoke-virtual {p0, v13, v14}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 159
    .local v10, "preferences":Landroid/content/SharedPreferences;
    const-string v13, "effect-mode"

    const/4 v14, 0x0

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 160
    .local v1, "effect":I
    const-string/jumbo v13, "slideshow-speed"

    const/4 v14, 0x1

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v12

    .line 161
    .local v12, "speed":I
    const-string v13, "filter"

    const/4 v14, 0x1

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 162
    .local v3, "filter":I
    const-string v13, "curr_tab"

    const/4 v14, 0x0

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 163
    .local v0, "currTab":I
    const-string v13, "image-index"

    const/4 v14, 0x0

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    .line 164
    .local v4, "imageIndex":I
    const-string/jumbo v14, "slideshow-videoplay"

    sget-boolean v13, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideshowVideoPlay:Z

    if-eqz v13, :cond_1

    const/4 v13, 0x1

    :goto_0
    invoke-interface {v10, v14, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    .line 167
    .local v6, "isVideoPlayEnabled":Z
    const-string v13, "music-uri"

    const-string v14, ""

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 168
    .local v7, "music":Ljava/lang/String;
    const-string v13, "music-enabled"

    const/4 v14, 0x0

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    .line 169
    .local v5, "isMusicEnabled":Z
    const-string v13, "music-extra"

    const-string v14, ""

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 171
    .local v2, "extraMusic":Ljava/lang/String;
    const-string v13, "musics-added"

    new-instance v14, Ljava/util/HashSet;

    invoke-direct {v14}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v10, v13, v14}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v9

    .line 174
    .local v9, "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->getMediaTitle(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v8

    .line 176
    .local v8, "musicTitle":Ljava/lang/String;
    if-nez v8, :cond_0

    .line 177
    const-string v7, ""

    .line 180
    :cond_0
    const/4 v13, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 181
    const/4 v13, 0x7

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 182
    const/4 v13, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 183
    const/4 v13, 0x6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 184
    const/4 v13, 0x3

    invoke-virtual {v11, v13, v7}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 185
    const/4 v13, 0x5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 186
    const/4 v13, 0x2

    invoke-virtual {v11, v13, v9}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 187
    const/4 v13, 0x6

    invoke-virtual {v11, v13, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 188
    const/16 v13, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 189
    const/16 v13, 0x9

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    invoke-virtual {v11, v13, v14}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->addSetting(ILjava/lang/Object;)V

    .line 191
    return-object v11

    .line 164
    .end local v2    # "extraMusic":Ljava/lang/String;
    .end local v5    # "isMusicEnabled":Z
    .end local v6    # "isVideoPlayEnabled":Z
    .end local v7    # "music":Ljava/lang/String;
    .end local v8    # "musicTitle":Ljava/lang/String;
    .end local v9    # "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_1
    const/4 v13, 0x0

    goto :goto_0
.end method

.method public static save(Landroid/content/Context;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;)V
    .locals 13
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "slideShowSetting"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;

    .prologue
    .line 207
    const-string/jumbo v11, "slideShowPrerences"

    const/4 v12, 0x0

    invoke-virtual {p0, v11, v12}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v9

    .line 209
    .local v9, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 211
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const/4 v11, 0x1

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 212
    .local v1, "effect":I
    const/4 v11, 0x7

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v10

    .line 213
    .local v10, "speed":I
    const/4 v11, 0x4

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 214
    .local v3, "filter":I
    const/4 v11, 0x3

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    .line 215
    .local v7, "music":Ljava/lang/String;
    const/4 v11, 0x5

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    .line 216
    .local v5, "isMusicEnable":Z
    const/4 v11, 0x6

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 217
    .local v2, "extraMusic":Ljava/lang/String;
    const/4 v11, 0x2

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Set;

    .line 218
    .local v8, "musicsAdded":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/16 v11, 0x8

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 219
    .local v4, "index":I
    const/16 v11, 0x9

    invoke-virtual {p1, v11}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->getSetting(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    .line 220
    .local v6, "isVideoPlayEnable":Z
    const-string v11, "effect-mode"

    invoke-interface {v0, v11, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string/jumbo v12, "slideshow-speed"

    invoke-interface {v11, v12, v10}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "music-uri"

    invoke-interface {v11, v12, v7}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "filter"

    invoke-interface {v11, v12, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "image-index"

    invoke-interface {v11, v12, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "music-enabled"

    invoke-interface {v11, v12, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "musics-added"

    invoke-interface {v11, v12, v8}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string v12, "music-extra"

    invoke-interface {v11, v12, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v11

    const-string/jumbo v12, "slideshow-videoplay"

    invoke-interface {v11, v12, v6}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 229
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 230
    return-void
.end method


# virtual methods
.method public addSetting(ILjava/lang/Object;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "value"    # Ljava/lang/Object;

    .prologue
    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public getEffect()I
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getFilter()I
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getImageIndex()I
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getMusicUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public getMusicsAdded()Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method public getSeletedTab()I
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getSetting(I)Ljava/lang/Object;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSpeed()I
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getVideoPlayEnable()Z
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public setCuurentTab(I)V
    .locals 3
    .param p1, "tabIndex"    # I

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    return-void
.end method

.method public setEffect(I)V
    .locals 3
    .param p1, "effectPosition"    # I

    .prologue
    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    return-void
.end method

.method public setFiltter(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 89
    return-void
.end method

.method public setImageIndex(I)V
    .locals 3
    .param p1, "imageIndex"    # I

    .prologue
    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 114
    return-void
.end method

.method public setMusicUri(Ljava/lang/String;)V
    .locals 2
    .param p1, "uriString"    # Ljava/lang/String;

    .prologue
    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    return-void
.end method

.method public setSpeed(I)V
    .locals 3
    .param p1, "speedPosition"    # I

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 122
    return-void
.end method

.method public setVideoPlayEnable(Z)V
    .locals 3
    .param p1, "videoPlay"    # Z

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSetting;->mSetting:Ljava/util/TreeMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    return-void
.end method
