.class public Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;
.super Ljava/lang/Object;
.source "GlGridConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "GlGridview"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;


# instance fields
.field public final glspread_anim_time:I

.field public final item_hsize_ratio_port:F

.field public final item_hsize_ratio_wide:F

.field public final item_vgap_ratio_2:F

.field public final item_vgap_ratio_3:F

.field public final item_vgap_ratio_4:F

.field public final item_vgap_ratio_5:F

.field public final item_vsize_ratio_port:F

.field public final item_vsize_ratio_wide:F

.field public final padding_bottom:F

.field public final padding_left_h:F

.field public final padding_left_v:F

.field public final padding_right:F

.field public final padding_top_h:F

.field public final padding_top_v:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 54
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_top_h:F

    .line 55
    const v1, 0x7f0e0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_top_v:F

    .line 56
    const v1, 0x7f0e0004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_left_v:F

    .line 57
    const v1, 0x7f0e0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_left_h:F

    .line 58
    const v1, 0x7f0e0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_right:F

    .line 59
    const v1, 0x7f0e0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->padding_bottom:F

    .line 60
    const v1, 0x7f0e0008

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vgap_ratio_2:F

    .line 61
    const v1, 0x7f0e0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vgap_ratio_3:F

    .line 62
    const v1, 0x7f0e000a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vgap_ratio_4:F

    .line 63
    const v1, 0x7f0e000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vgap_ratio_5:F

    .line 64
    const v1, 0x7f0e000c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_hsize_ratio_wide:F

    .line 65
    const v1, 0x7f0e000d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_hsize_ratio_port:F

    .line 66
    const v1, 0x7f0e000e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vsize_ratio_port:F

    .line 67
    const v1, 0x7f0e000f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->item_vsize_ratio_wide:F

    .line 68
    const v1, 0x7f0c001a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->glspread_anim_time:I

    .line 69
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 46
    const-class v1, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->sInstance:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    if-nez v0, :cond_0

    .line 47
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->sInstance:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;

    .line 49
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;->sInstance:Lcom/sec/samsung/gallery/glview/GlGridConfig$GlGridview;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
