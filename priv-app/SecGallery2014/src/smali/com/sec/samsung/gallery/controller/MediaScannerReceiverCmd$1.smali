.class Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$1;
.super Landroid/content/BroadcastReceiver;
.source "MediaScannerReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 61
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "action":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 66
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "unmountedStorage":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;->access$000(Lcom/sec/samsung/gallery/controller/MediaScannerReceiverCmd;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "MEDIA_EJECT"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
