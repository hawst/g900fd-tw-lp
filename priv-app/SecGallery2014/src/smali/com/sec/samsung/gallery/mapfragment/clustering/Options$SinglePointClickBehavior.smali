.class public final enum Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;
.super Ljava/lang/Enum;
.source "Options.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SinglePointClickBehavior"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

.field public static final enum NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

.field public static final enum SHOW_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 358
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    const-string v1, "SHOW_INFO_WINDOW"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->SHOW_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    const-string v1, "NO_OP"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    .line 357
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->SHOW_INFO_WINDOW:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->NO_OP:Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 357
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 357
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;
    .locals 1

    .prologue
    .line 357
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->$VALUES:[Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/mapfragment/clustering/Options$SinglePointClickBehavior;

    return-object v0
.end method
