.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;
.source "GlComposeQuickScrollBar.java"


# static fields
.field public static final CENTER_POPUP_LIMITED_RATIO:F = 0.6f

.field private static final FADE_OUT_DELAY:I = 0x7d0

.field public static QUICK_ITEM_PITCH:F = 0.0f

.field public static QUICK_ROOT_PITCH_RATIO:F = 0.0f

.field public static QUICK_ROOT_Y:F = 0.0f

.field public static QUICK_ROOT_Z:F = 0.0f

.field private static final RES_ID_BORDER:I = 0x6

.field private static final RES_ID_CENTER_POPUP:I = 0x4

.field private static final RES_ID_CENTER_SUBID_TITLE:I = 0x5

.field private static final RES_ID_HANDLER_ICON:I = 0x1

.field private static final RES_ID_POPUP_BACKGROUND:I = 0x0

.field private static final RES_ID_SCROLL_BAR:I = 0x2

.field private static final RES_ID_SUBID_TITLE:I = 0x3


# instance fields
.field private mCanvasH:I

.field private mCanvasW:I

.field private mCenterPopupCanvasH:I

.field private mCenterPopupCanvasW:I

.field private mCenterPopupObjH:F

.field private mCenterPopupObjW:F

.field private mCenterPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

.field private mContext:Landroid/content/Context;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mGroupTitle:Ljava/lang/String;

.field public mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field private mObjH:F

.field private mObjW:F

.field private mPopupHeight:I

.field private mPopupRightMargin:I

.field private mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

.field private mQuickScrollPopupPadding:I

.field private mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

.field private mScrollBarCanvasH:I

.field private mScrollBarCanvasW:I

.field private mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mScrollBarObjH:F

.field private mScrollBarObjW:F

.field private mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

.field private mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mScrollPopupCanvasH:I

.field private mScrollPopupCanvasW:I

.field private mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mScrollPopupObjH:F

.field private mScrollPopupObjW:F

.field private mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 36
    const/high16 v0, 0x42b40000    # 90.0f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    .line 37
    const/high16 v0, 0x40200000    # 2.5f

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_PITCH_RATIO:F

    .line 38
    sput v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Z:F

    .line 39
    sput v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Y:F

    return-void
.end method

.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Landroid/content/Context;)V
    .locals 1
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 72
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 45
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 46
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 47
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/GlView;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 56
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    .line 418
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 73
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    .line 74
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollItemPitch()F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ITEM_PITCH:F

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollRootPitchRatio()F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_PITCH_RATIO:F

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollRootZ()F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Z:F

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollRootY()F

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->QUICK_ROOT_Y:F

    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollPopupPadding()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollPopupPadding:I

    .line 83
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object v0
.end method

.method private createCenterPopup()V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 112
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjH:F

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 113
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v4, v4, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasW:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasH:I

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 115
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawCenterPopup(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 116
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 117
    const/4 v0, 0x0

    .line 118
    .local v0, "cY":F
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d03bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v1, v2

    .line 120
    .local v1, "topMargin":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjH:F

    div-float/2addr v2, v7

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mTop:F

    div-float/2addr v3, v7

    sub-float v0, v2, v3

    .line 121
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v3, 0x0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDefZ:F

    invoke-virtual {v2, v3, v0, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 122
    return-void
.end method

.method private drawCenterPopup(Lcom/sec/android/gallery3d/glcore/GlView;)V
    .locals 11
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 221
    const/4 v7, 0x4

    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 222
    .local v3, "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0d03be

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 224
    .local v1, "fontSize":F
    invoke-static {v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v2

    .line 226
    .local v2, "paint":Landroid/text/TextPaint;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    const/4 v8, 0x0

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasW:I

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollPopupPadding:I

    mul-int/lit8 v10, v10, 0x2

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-static {v7, v8, v9, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v6

    .line 227
    .local v6, "title":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0b0066

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 228
    .local v4, "textColor":I
    if-nez v3, :cond_2

    .line 229
    invoke-static {v6, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v5

    .line 230
    .local v5, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    invoke-virtual {v5, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 231
    const/4 v7, 0x2

    const/4 v8, 0x2

    invoke-virtual {v5, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 232
    const/4 v7, 0x0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v8

    invoke-virtual {v8}, Landroid/text/TextPaint;->descent()F

    move-result v8

    float-to-int v8, v8

    div-int/lit8 v8, v8, 0x2

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual {v5, v7, v8, v9, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 234
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f02019a

    invoke-virtual {v7, v8, v9}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 235
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v3    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {v3, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 236
    .restart local v3    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 237
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseQuickScrollTypeWinset:Z

    if-eqz v7, :cond_1

    .line 238
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupHeight()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 242
    :goto_0
    const/4 v7, 0x2

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 243
    const/4 v7, 0x5

    invoke-virtual {v3, v5, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 244
    const/4 v7, 0x4

    invoke-virtual {p1, v3, v7}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 259
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_1
    return-void

    .line 240
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasW:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupHeight()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    goto :goto_0

    .line 246
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v5    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_2
    const/4 v7, 0x5

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 248
    .restart local v5    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-eqz v5, :cond_0

    .line 250
    invoke-virtual {v5, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 251
    invoke-virtual {v5, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 252
    const/4 v7, 0x2

    const/4 v8, 0x2

    invoke-virtual {v5, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 253
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseQuickScrollTypeWinset:Z

    if-eqz v7, :cond_3

    .line 254
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupWidth()I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupHeight()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 257
    :goto_2
    const/4 v7, 0x2

    const/4 v8, 0x1

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_1

    .line 256
    :cond_3
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasW:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupHeight()I

    move-result v8

    invoke-virtual {v3, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    goto :goto_2
.end method

.method private drawScrollBar(Lcom/sec/android/gallery3d/glcore/GlView;Z)V
    .locals 10
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "borderVisible"    # Z

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 125
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 128
    .local v2, "scrollBar":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v2, :cond_0

    .line 129
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f02019b

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 130
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v2    # "scrollBar":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 131
    .restart local v2    # "scrollBar":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 132
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollBarHeight()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 134
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v3

    if-ne v3, v7, :cond_2

    .line 135
    invoke-virtual {v2, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 139
    :goto_0
    invoke-virtual {v2, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 140
    invoke-virtual {p1, v2, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 143
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {p1, v7}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 144
    .local v1, "handler":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v1, :cond_1

    .line 145
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v3

    if-ne v3, v7, :cond_3

    .line 146
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020198

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 150
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "handler":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 151
    .restart local v1    # "handler":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 154
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v3

    if-ne v3, v7, :cond_4

    .line 155
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollBarWidth()I

    move-result v3

    invoke-virtual {v1, v3, v6, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 156
    invoke-virtual {v1, v7, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 161
    :goto_2
    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 163
    invoke-virtual {p1, v1, v7}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 165
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0, v1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    .line 166
    return-void

    .line 137
    .end local v1    # "handler":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    invoke-virtual {v2, v9, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 148
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v1    # "handler":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020196

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_1

    .line 158
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollBarWidth()I

    move-result v3

    invoke-virtual {v1, v6, v6, v3, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 159
    invoke-virtual {v1, v9, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_2
.end method

.method private drawScrollPopup(Lcom/sec/android/gallery3d/glcore/GlView;Z)V
    .locals 12
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "borderVisible"    # Z

    .prologue
    .line 169
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    instance-of v8, v8, Lcom/sec/android/gallery3d/app/GalleryActivity;

    if-eqz v8, :cond_1

    .line 170
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    .line 171
    .local v0, "activity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v8

    invoke-interface {v8}, Lcom/sec/android/gallery3d/app/GalleryApp;->isFestivalMode()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 218
    .end local v0    # "activity":Lcom/sec/android/gallery3d/app/GalleryActivity;
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const/4 v8, 0x0

    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 178
    .local v4, "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0d03bd

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 179
    .local v2, "fontSize":F
    invoke-static {v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v3

    .line 180
    .local v3, "paint":Landroid/text/TextPaint;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    const/4 v9, 0x0

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupCanvasW:I

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarCanvasW:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollPopupPadding:I

    mul-int/lit8 v11, v11, 0x2

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-static {v8, v9, v10, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v7

    .line 181
    .local v7, "title":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b0065

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 183
    .local v5, "textColor":I
    if-nez v4, :cond_3

    .line 184
    const/4 v6, 0x0

    .line 185
    .local v6, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f020199

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 186
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {v7, v2, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v6

    .line 187
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v8

    iput v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupHeight:I

    .line 188
    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 189
    const/4 v8, 0x2

    const/4 v9, 0x2

    invoke-virtual {v6, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 190
    const/4 v8, 0x0

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollPopupTextBottomPadding()I

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v6, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 192
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v4    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {v4, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 193
    .restart local v4    # "popupView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v8

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollPopupPadding:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupHeight:I

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 195
    const/4 v8, 0x3

    invoke-virtual {v4, v6, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 196
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_2

    .line 197
    const/4 v8, 0x1

    const/4 v9, 0x2

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 198
    iget v8, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupRightMargin:I

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v4, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 203
    :goto_1
    const/4 v8, 0x0

    invoke-virtual {p1, v4, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 217
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_2
    invoke-virtual {p0, v4, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    goto/16 :goto_0

    .line 200
    .restart local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_2
    const/4 v8, 0x3

    const/4 v9, 0x2

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 201
    const/4 v8, 0x0

    const/4 v9, 0x0

    iget v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupRightMargin:I

    const/4 v11, 0x0

    invoke-virtual {v4, v8, v9, v10, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    goto :goto_1

    .line 205
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v6    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_3
    const/4 v8, 0x3

    invoke-virtual {v4, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 206
    .restart local v6    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-eqz v6, :cond_0

    .line 208
    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 209
    invoke-virtual {v6, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 210
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v8

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollPopupPadding:I

    mul-int/lit8 v9, v9, 0x2

    add-int/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupHeight:I

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 211
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_4

    .line 212
    const/4 v8, 0x1

    const/4 v9, 0x2

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_2

    .line 214
    :cond_4
    const/4 v8, 0x3

    const/4 v9, 0x2

    invoke-virtual {v4, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_2
.end method

.method private resetScrollBar()V
    .locals 7

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 86
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_0

    .line 99
    :goto_0
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    .line 89
    .local v0, "cX":F
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->getLayoutDirectionFromLocale(Ljava/util/Locale;)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 90
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjW:F

    sub-float/2addr v1, v2

    neg-float v1, v1

    div-float v0, v1, v3

    .line 94
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawScrollBar(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    .line 95
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjW:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjH:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setSize(FF)V

    .line 96
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarCanvasW:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarCanvasH:I

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 97
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 98
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0

    .line 92
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjW:F

    sub-float/2addr v1, v2

    div-float v0, v1, v3

    goto :goto_1
.end method

.method private resetScrollPopup()V
    .locals 5

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v0, :cond_0

    .line 109
    :goto_0
    return-void

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawScrollPopup(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjW:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjH:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setSize(FF)V

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupCanvasW:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupCanvasH:I

    invoke-direct {v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method


# virtual methods
.method public disableQuickScroll()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 336
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->disableQuickScroll()V

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollCenterPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setVisibility(Z)V

    .line 341
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->forceUpdateScrollTitle(Ljava/lang/String;Z)V

    .line 342
    return-void
.end method

.method public drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)V
    .locals 7
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "visible"    # Z

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x2

    .line 264
    if-nez p1, :cond_0

    .line 265
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 268
    .restart local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_0
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 269
    .local v0, "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    .line 270
    if-nez v0, :cond_1

    .line 271
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02026e

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 272
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 273
    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 274
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setScaleRatio(F)V

    .line 275
    invoke-virtual {v0, v5, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 276
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 283
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return-void

    .line 279
    :cond_2
    if-eqz v0, :cond_1

    .line 280
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_0
.end method

.method public enableQuickScroll()V
    .locals 2

    .prologue
    .line 325
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->enableQuickScroll()V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 328
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setAlpha(F)V

    .line 330
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->createCenterPopup()V

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setVisibility(Z)V

    .line 332
    return-void
.end method

.method public fadeOut(JJ)V
    .locals 3
    .param p1, "delay"    # J
    .param p3, "duration"    # J

    .prologue
    const/4 v2, 0x0

    .line 443
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->disableQuickScroll()V

    .line 444
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mEnabledQuickScroll:Z

    .line 445
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-nez v0, :cond_0

    .line 446
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->getAlpha()F

    move-result v1

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;-><init>(Lcom/sec/android/gallery3d/glcore/GlObject;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    .line 447
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    new-instance v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 458
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->getAlpha()F

    move-result v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setParam(FF)V

    .line 461
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p3, p4}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->setDuration(J)V

    .line 462
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->startAfter(J)V

    .line 463
    return-void
.end method

.method public forceUpdateScrollTitle(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "groupTitle"    # Ljava/lang/String;
    .param p2, "isForceUpdate"    # Z

    .prologue
    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_0

    .line 321
    :goto_0
    return-void

    .line 315
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;

    .line 316
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->isEnabledQuickScroll()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 317
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawCenterPopup(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawScrollPopup(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    goto :goto_0
.end method

.method protected getHeight(FF)F
    .locals 1
    .param p1, "min"    # F
    .param p2, "max"    # F

    .prologue
    .line 302
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjH:F

    return v0
.end method

.method public hideNow()V
    .locals 1

    .prologue
    .line 346
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->hideNow()V

    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isReady()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 349
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setAlpha(F)V

    .line 351
    :cond_1
    return-void
.end method

.method protected initScrollBar()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 287
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 288
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 290
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    invoke-direct {v0, v1, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 292
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mFadeOutDelay:I

    .line 293
    return-void
.end method

.method public reset(FFFFFF)V
    .locals 7
    .param p1, "widthViewRatio"    # F
    .param p2, "heightViewRatio"    # F
    .param p3, "widthSpace"    # F
    .param p4, "heightSpace"    # F
    .param p5, "top"    # F
    .param p6, "bottom"    # F

    .prologue
    const/4 v6, 0x0

    .line 354
    invoke-super/range {p0 .. p6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->reset(FFFFFF)V

    .line 356
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v2, v2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v2, :cond_0

    .line 385
    :goto_0
    return-void

    .line 360
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    const v3, 0x7f020196

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDrawableWidth(Landroid/content/Context;I)I

    move-result v0

    .line 361
    .local v0, "handlerWidth":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    const v3, 0x7f02019b

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDrawableWidth(Landroid/content/Context;I)I

    move-result v1

    .line 362
    .local v1, "scrollBarWidth":I
    add-int v2, v0, v1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupRightMargin:I

    .line 364
    iput p3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    .line 365
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollBarHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjH:F

    .line 366
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjH:F

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjH:F

    .line 367
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mPopupRightMargin:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjW:F

    .line 368
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjW:F

    .line 369
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjH:F

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjH:F

    .line 370
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d02e8

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p1

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjW:F

    .line 371
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getQuickScrollCenterPopupHeight()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, p2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjH:F

    .line 373
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    div-float/2addr v2, p1

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCanvasW:I

    .line 374
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjH:F

    div-float/2addr v2, p2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCanvasH:I

    .line 375
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjH:F

    div-float/2addr v2, p2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarCanvasH:I

    .line 376
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObjW:F

    div-float/2addr v2, p1

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarCanvasW:I

    .line 377
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjW:F

    div-float/2addr v2, p1

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupCanvasW:I

    .line 378
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObjH:F

    div-float/2addr v2, p2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupCanvasH:I

    .line 379
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjW:F

    div-float/2addr v2, p1

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasW:I

    .line 380
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupObjH:F

    div-float/2addr v2, p2

    float-to-int v2, v2

    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCenterPopupCanvasH:I

    .line 381
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    iget-object v3, v3, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCanvasW:I

    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mCanvasH:I

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 382
    invoke-virtual {p0, v6, v6, v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setPos(FFF)V

    .line 383
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->resetScrollBar()V

    .line 384
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->resetScrollPopup()V

    goto/16 :goto_0
.end method

.method public setFocusBorderVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 399
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawScrollBar(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    .line 400
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 401
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->drawScrollPopup(Lcom/sec/android/gallery3d/glcore/GlView;Z)V

    .line 402
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 403
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setBorderColor(I)V

    .line 404
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setFocusBorderVisible(Z)V

    .line 405
    return-void
.end method

.method public setGenericMotionListener()V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 395
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollPopupObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 396
    return-void
.end method

.method public setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 390
    return-void
.end method

.method public setThinkness(F)V
    .locals 1
    .param p1, "thinkness"    # F

    .prologue
    .line 297
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mObjW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mThinkness:F

    .line 298
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 408
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->show()V

    .line 409
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mQuickScrollFadeOutAnim:Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlAlphaAnimation;->stop()V

    .line 411
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mEnabledQuickScroll:Z

    .line 413
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->getFocusBorderVisible()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 414
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setFocusBorderVisible(Z)V

    .line 416
    :cond_1
    return-void
.end method

.method public updateScrollTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "groupTitle"    # Ljava/lang/String;

    .prologue
    .line 307
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->forceUpdateScrollTitle(Ljava/lang/String;Z)V

    .line 308
    return-void
.end method
