.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setupButtons()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;)V
    .locals 0

    .prologue
    .line 385
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 388
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 390
    .local v0, "selectedString":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-nez v1, :cond_0

    .line 391
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setupPopupMenu(Ljava/lang/String;)V

    .line 395
    :goto_0
    return-void

    .line 393
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$6;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    invoke-virtual {v1, v4, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->setSelectAllButtonTitle(ILjava/lang/String;)V

    goto :goto_0
.end method
