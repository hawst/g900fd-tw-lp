.class public Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;
.super Lcom/sec/samsung/gallery/dialog/GalleryDialog;
.source "ConfirmationDialog.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ValidFragment"
    }
.end annotation


# instance fields
.field private final mCtx:Landroid/content/Context;

.field private final mEvent:Lcom/sec/samsung/gallery/core/Event;

.field private mIsAirButton:Z

.field private final mMessage:Ljava/lang/String;

.field private mTitle:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lcom/sec/samsung/gallery/core/Event;)V
    .locals 1
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "title"    # I
    .param p3, "message"    # Ljava/lang/String;
    .param p4, "event"    # Lcom/sec/samsung/gallery/core/Event;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;-><init>(Landroid/content/Context;)V

    .line 43
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    .line 44
    iput-object p4, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    .line 45
    iput p2, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mTitle:I

    .line 46
    iput-object p3, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mMessage:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mIsAirButton:Z

    .line 48
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)Lcom/sec/samsung/gallery/core/Event;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mEvent:Lcom/sec/samsung/gallery/core/Event;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 52
    const v3, 0x7f0e00db

    .line 53
    .local v3, "positiveButtonTitle":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v6, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mTitle:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 54
    .local v2, "dialogTitle":Ljava/lang/String;
    iget v5, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mTitle:I

    sparse-switch v5, :sswitch_data_0

    .line 71
    const v3, 0x7f0e00db

    .line 74
    :goto_0
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mMessage:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    new-instance v6, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;

    invoke-direct {v6, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$2;-><init>(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)V

    invoke-virtual {v5, v3, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const/high16 v6, 0x1040000

    new-instance v7, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$1;

    invoke-direct {v7, p0}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog$1;-><init>(Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;)V

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 99
    .local v1, "dialog":Landroid/app/Dialog;
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x100

    invoke-virtual {v5, v6}, Landroid/view/Window;->addFlags(I)V

    .line 100
    invoke-virtual {v1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    const/16 v6, 0x3eb

    invoke-virtual {v5, v6}, Landroid/view/Window;->setType(I)V

    .line 101
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 102
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->hideStatusIcon(Ljava/lang/Object;)V

    .line 103
    return-object v1

    .line 56
    .end local v1    # "dialog":Landroid/app/Dialog;
    :sswitch_0
    const v3, 0x7f0e0041

    .line 57
    goto :goto_0

    .line 60
    :sswitch_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    .line 61
    .local v4, "type":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v5, v4}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 62
    .local v0, "category_name":Ljava/lang/String;
    const v5, 0x7f0e0463

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 67
    .end local v0    # "category_name":Ljava/lang/String;
    .end local v4    # "type":Lcom/sec/samsung/gallery/core/TabTagType;
    :sswitch_2
    const v3, 0x7f0e0335

    .line 68
    goto :goto_0

    .line 54
    :sswitch_data_0
    .sparse-switch
        0x7f0e0041 -> :sswitch_0
        0x7f0e0449 -> :sswitch_2
        0x7f0e0463 -> :sswitch_1
        0x7f0e04a3 -> :sswitch_2
    .end sparse-switch
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "UNLOCK_TOUCH"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 112
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mIsAirButton:Z

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mCtx:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 115
    :cond_1
    invoke-super {p0}, Lcom/sec/samsung/gallery/dialog/GalleryDialog;->onDestroy()V

    .line 116
    return-void
.end method

.method public setIsAirButton(Z)V
    .locals 0
    .param p1, "isAirButton"    # Z

    .prologue
    .line 119
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/dialog/ConfirmationDialog;->mIsAirButton:Z

    .line 120
    return-void
.end method
