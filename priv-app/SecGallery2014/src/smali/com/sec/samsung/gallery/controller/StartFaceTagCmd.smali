.class public Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartFaceTagCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$4;,
        Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;
    }
.end annotation


# instance fields
.field mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field mContext:Landroid/content/Context;

.field mDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 45
    return-void
.end method

.method private dismissRemoveTagDialog()V
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 261
    :cond_0
    return-void
.end method

.method private getSelectedList()Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation

    .prologue
    .line 245
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 246
    .local v5, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 247
    .local v3, "selectedItems":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 248
    .local v4, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 249
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 250
    .local v2, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 251
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaObject;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v1

    .line 252
    .local v1, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 248
    .end local v1    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 255
    .end local v2    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    return-object v4
.end method

.method private requestAssignName(Landroid/content/Intent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 18
    .param p1, "data"    # Landroid/content/Intent;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 110
    const/4 v14, 0x0

    .local v14, "name":Ljava/lang/String;
    const/4 v7, 0x0

    .line 111
    .local v7, "joinedName":Ljava/lang/String;
    const/4 v12, 0x0

    .local v12, "lookupKey":Ljava/lang/String;
    const/4 v11, 0x0

    .line 112
    .local v11, "joinedLookupKey":Ljava/lang/String;
    const/4 v10, 0x0

    .line 113
    .local v10, "constructedName":Ljava/lang/String;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    .local v15, "nameBuffer":Ljava/lang/StringBuilder;
    const-string v16, "profile"

    .line 116
    .local v16, "profile":Ljava/lang/String;
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 117
    .local v13, "mediaSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v17

    .line 118
    .local v17, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_2

    .line 119
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 120
    .local v2, "uri":Landroid/net/Uri;
    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v3, :cond_0

    .line 121
    move-object/from16 v0, p2

    invoke-virtual {v13, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 123
    .local v1, "cr":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 125
    .local v9, "c":Landroid/database/Cursor;
    const/4 v3, 0x2

    :try_start_0
    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "lookup"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "display_name"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 130
    if-eqz v9, :cond_1

    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 131
    const/4 v3, 0x0

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 132
    move-object/from16 v0, v16

    invoke-virtual {v0, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 133
    const-string v14, "Me"
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :cond_1
    :goto_0
    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 143
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_2
    if-nez v14, :cond_5

    .line 144
    const-string v3, "FaceScan"

    const-string v4, "name is null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, v10

    .line 179
    .end local v10    # "constructedName":Ljava/lang/String;
    .end local p3    # "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .local v6, "constructedName":Ljava/lang/String;
    :cond_3
    :goto_1
    return-void

    .line 135
    .end local v6    # "constructedName":Ljava/lang/String;
    .restart local v1    # "cr":Landroid/content/ContentResolver;
    .restart local v2    # "uri":Landroid/net/Uri;
    .restart local v9    # "c":Landroid/database/Cursor;
    .restart local v10    # "constructedName":Ljava/lang/String;
    .restart local p3    # "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    const/4 v3, 0x1

    :try_start_1
    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v14

    goto :goto_0

    .line 139
    :catchall_0
    move-exception v3

    invoke-static {v9}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v3

    .line 148
    .end local v1    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v9    # "c":Landroid/database/Cursor;
    :cond_5
    const-string v3, "."

    invoke-virtual {v12, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 149
    move-object v11, v12

    .line 150
    const-string v3, "\\."

    invoke-virtual {v12, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v12, v3, v4

    .line 153
    :cond_6
    invoke-virtual {v15, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 156
    .end local v10    # "constructedName":Ljava/lang/String;
    .restart local v6    # "constructedName":Ljava/lang/String;
    if-eqz v11, :cond_8

    .line 157
    new-instance v15, Ljava/lang/StringBuilder;

    .end local v15    # "nameBuffer":Ljava/lang/StringBuilder;
    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 158
    .restart local v15    # "nameBuffer":Ljava/lang/StringBuilder;
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 162
    :goto_2
    invoke-virtual {v6}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0e00c2

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 164
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0e005d

    const v5, 0x7f0e005e

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->showAlertDialog(Landroid/content/Context;II)V

    .line 167
    :cond_7
    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz p3, :cond_3

    .line 168
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    instance-of v3, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v3, :cond_9

    move-object/from16 v0, p3

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v3, :cond_9

    .line 169
    check-cast p3, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .end local p3    # "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v13, v6, v7}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateClusterName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    const-string v3, "FaceScan"

    const-string v4, "AlbumViewState"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 160
    .restart local p3    # "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    const-string v7, ""

    goto :goto_2

    .line 173
    :cond_9
    if-eqz p2, :cond_a

    move-object/from16 v0, p2

    instance-of v3, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v3, :cond_a

    move-object/from16 v3, p2

    .line 174
    check-cast v3, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v5, v17

    move-object/from16 v8, p3

    invoke-virtual/range {v3 .. v8}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateSlotName(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 176
    :cond_a
    const-string v3, "FaceScan"

    const-string v4, "not an AlbumViewState"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private requestRemoveConfirmTag(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V
    .locals 11
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p4, "action"    # Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    .prologue
    .line 183
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 184
    .local v4, "mediaSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->getSelectedList()Ljava/util/ArrayList;

    move-result-object v5

    .line 185
    .local v5, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    if-eqz p1, :cond_0

    .line 186
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 187
    :cond_0
    const/4 v8, 0x0

    .line 188
    .local v8, "dialogMessage":Ljava/lang/String;
    const/4 v9, 0x0

    .line 189
    .local v9, "dialogTitle":Ljava/lang/String;
    const/4 v7, 0x0

    .line 190
    .local v7, "actionPositiveButton":I
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->REMOVE_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne p4, v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0061

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e00be

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 193
    const v7, 0x7f0e0335

    .line 199
    :cond_1
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0e0046

    new-instance v2, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v10

    new-instance v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/sec/android/gallery3d/data/MediaSet;)V

    invoke-virtual {v10, v7, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mDialog:Landroid/app/AlertDialog;

    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 242
    return-void

    .line 194
    :cond_2
    sget-object v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-ne p4, v0, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0060

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 197
    const v7, 0x7f0e005a

    goto :goto_0
.end method

.method private showAlertDialog(Landroid/content/Context;II)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "titleId"    # I
    .param p3, "messageId"    # I

    .prologue
    .line 264
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0e00db

    new-instance v3, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$3;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 274
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 275
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 13
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 52
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Ljava/lang/Object;

    move-object v7, v10

    check-cast v7, [Ljava/lang/Object;

    .line 53
    .local v7, "params":[Ljava/lang/Object;
    const/4 v10, 0x0

    aget-object v10, v7, v10

    check-cast v10, Landroid/content/Context;

    iput-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    .line 54
    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mContext:Landroid/content/Context;

    check-cast v10, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iput-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 55
    const/4 v10, 0x1

    aget-object v2, v7, v10

    check-cast v2, Ljava/util/ArrayList;

    .line 56
    .local v2, "contents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const/4 v10, 0x2

    aget-object v0, v7, v10

    check-cast v0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    .line 58
    .local v0, "action":Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;
    if-nez v0, :cond_1

    .line 59
    const/4 v10, 0x3

    aget-object v10, v7, v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 60
    .local v8, "show":Z
    if-nez v8, :cond_0

    .line 61
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->dismissRemoveTagDialog()V

    .line 107
    .end local v8    # "show":Z
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    const/4 v5, 0x0

    .line 67
    .local v5, "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v1, 0x0

    .line 68
    .local v1, "clusterAlbumSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v6, 0x0

    .line 70
    .local v6, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_2

    .line 71
    const/4 v10, 0x0

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    .end local v5    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v5, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 72
    .restart local v5    # "dstMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    .end local v6    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    check-cast v6, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 75
    .restart local v6    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    .line 77
    .local v3, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    sget-object v10, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$4;->$SwitchMap$com$sec$samsung$gallery$controller$StartFaceTagCmd$FaceTagAction:[I

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->ordinal()I

    move-result v11

    aget v10, v10, v11

    packed-switch v10, :pswitch_data_0

    .line 106
    :goto_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v10}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v10

    const-string v11, "EXIT_SELECTION_MODE"

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 79
    :pswitch_0
    const-string v10, "FaceScan"

    const-string v11, "ASSIGN_TAG"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v10, 0x3

    aget-object v4, v7, v10

    check-cast v4, Landroid/content/Intent;

    .line 81
    .local v4, "data":Landroid/content/Intent;
    invoke-direct {p0, v4, v5, v6}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->requestAssignName(Landroid/content/Intent;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;)V

    goto :goto_1

    .line 85
    .end local v4    # "data":Landroid/content/Intent;
    :pswitch_1
    const/4 v10, 0x3

    aget-object v10, v7, v10

    check-cast v10, Ljava/lang/Boolean;

    invoke-virtual {v10}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 86
    .restart local v8    # "show":Z
    if-nez v8, :cond_3

    .line 87
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->dismissRemoveTagDialog()V

    goto :goto_0

    .line 90
    :cond_3
    instance-of v10, v3, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v10, :cond_6

    .line 91
    if-eqz v6, :cond_4

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v10

    if-eqz v10, :cond_4

    .line 92
    move-object v1, v6

    .line 93
    :cond_4
    const/4 v6, 0x0

    .line 98
    :cond_5
    :goto_2
    invoke-direct {p0, v5, v1, v6, v0}, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->requestRemoveConfirmTag(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    .line 99
    iget-object v10, p0, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v9

    .line 100
    .local v9, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    goto :goto_0

    .line 94
    .end local v9    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_6
    instance-of v10, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v10, :cond_7

    instance-of v10, v3, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v10, :cond_5

    .line 95
    :cond_7
    move-object v1, v5

    .line 96
    const/4 v5, 0x0

    goto :goto_2

    .line 77
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
