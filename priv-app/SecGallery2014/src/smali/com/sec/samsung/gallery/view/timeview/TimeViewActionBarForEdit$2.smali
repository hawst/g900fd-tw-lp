.class Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;
.super Ljava/lang/Object;
.source "TimeViewActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

.field final synthetic val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field final synthetic val$toggleImage:Z

.field final synthetic val$visible:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;Lcom/sec/samsung/gallery/util/Consts$ButtonType;ZI)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$toggleImage:Z

    iput p4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$visible:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    if-ne v0, v1, :cond_1

    .line 325
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$toggleImage:Z

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget v2, v2, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mQuantitySelectedAlbum:I

    # invokes: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;ZI)V

    .line 326
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 328
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$visible:I

    if-nez v0, :cond_0

    .line 329
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setEnabled(Z)V

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setChecked(Z)V

    .line 337
    :cond_1
    :goto_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_2

    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0f026e

    invoke-static {v0, v1, v4}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 341
    :cond_2
    return-void

    .line 331
    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v0, :cond_1

    .line 332
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$visible:I

    if-nez v0, :cond_4

    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->setEnableSelectionOnActionBar(Z)V

    .line 334
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit$2;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForEdit;->setCheckboxState(Z)V

    goto :goto_0
.end method
