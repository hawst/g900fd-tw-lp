.class Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;
.super Ljava/lang/Object;
.source "MediaSetAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "IndexInfo"
.end annotation


# instance fields
.field private actEnd:I

.field private actStart:I

.field private activeRange:Z

.field private cntEnd:I

.field private cntStart:I

.field public count:I

.field public fwdIndex:I

.field public index:I

.field public revIndex:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;)V
    .locals 0

    .prologue
    .line 1231
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$1;

    .prologue
    .line 1231
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;-><init>(Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;)V

    return-void
.end method


# virtual methods
.method public getNextIndex()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1253
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->activeRange:Z

    if-eqz v1, :cond_4

    .line 1254
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 1255
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actStart:I

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actEnd:I

    if-lez v1, :cond_1

    .line 1256
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actStart:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    .line 1286
    :cond_0
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->count:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->count:I

    .line 1287
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->count:I

    if-gez v1, :cond_8

    :cond_1
    :goto_1
    return v0

    .line 1259
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actEnd:I

    add-int/lit8 v2, v2, -0x2

    if-ge v1, v2, :cond_3

    .line 1260
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    goto :goto_0

    .line 1262
    :cond_3
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    .line 1263
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->activeRange:Z

    goto :goto_0

    .line 1266
    :cond_4
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    if-ne v1, v2, :cond_6

    .line 1267
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntEnd:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_5

    .line 1268
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    .line 1269
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    goto :goto_0

    .line 1270
    :cond_5
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntStart:I

    if-le v1, v2, :cond_1

    .line 1271
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    .line 1272
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    goto :goto_0

    .line 1275
    :cond_6
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    if-ne v1, v2, :cond_0

    .line 1276
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntStart:I

    if-le v1, v2, :cond_7

    .line 1277
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    .line 1278
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    goto :goto_0

    .line 1279
    :cond_7
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntEnd:I

    add-int/lit8 v2, v2, -0x1

    if-ge v1, v2, :cond_1

    .line 1280
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    .line 1281
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    goto :goto_0

    .line 1287
    :cond_8
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public reset()V
    .locals 2

    .prologue
    .line 1241
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->index:I

    .line 1242
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->activeRange:Z

    .line 1243
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveStart:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actStart:I

    .line 1244
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mThumbnailActiveEnd:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actEnd:I

    .line 1245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContentStart:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntStart:I

    .line 1246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->this$0:Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iget v0, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mContentEnd:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntEnd:I

    .line 1247
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntEnd:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->cntStart:I

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->count:I

    .line 1248
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actStart:I

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->revIndex:I

    .line 1249
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->actEnd:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter$IndexInfo;->fwdIndex:I

    .line 1250
    return-void
.end method
