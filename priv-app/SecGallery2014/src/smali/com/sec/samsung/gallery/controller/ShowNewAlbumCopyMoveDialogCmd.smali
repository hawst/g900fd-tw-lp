.class public Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "ShowNewAlbumCopyMoveDialogCmd.java"

# interfaces
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field public static final OP_COPY:I = 0x0

.field public static final OP_MOVE:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEventType:I

.field private mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field private mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

.field private mNewAlbumPath:Ljava/lang/String;

.field private mOperationId:I

.field private mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

.field private mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    .line 66
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/FileUtil;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    return v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Lcom/sec/samsung/gallery/util/MediaOperations;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/samsung/gallery/util/MediaOperations;)Lcom/sec/samsung/gallery/util/MediaOperations;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/MediaOperations;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationId:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private dismissNewAlbumCopyMoveDialog()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->dismissDialog()V

    .line 307
    :cond_0
    return-void
.end method

.method private handleSelectedOperation(I)V
    .locals 13
    .param p1, "operationId"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 106
    iput p1, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationId:I

    .line 108
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v8, 0x7f0e01a5

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 109
    .local v6, "title":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    .line 110
    .local v5, "selectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 111
    .local v3, "newAlbumSelectionProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 112
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v5, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Ljava/util/List;)V

    .line 113
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 115
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    check-cast v7, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 116
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    if-eqz v7, :cond_2

    .line 117
    new-instance v7, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    const v10, 0x7f0e00ca

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 118
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 119
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    new-instance v8, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$1;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)V

    invoke-virtual {v7, v6, v11, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 166
    :goto_0
    new-instance v4, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;

    invoke-direct {v4, p0, v0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 281
    .local v4, "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    :try_start_0
    new-instance v2, Ljava/io/File;

    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;

    invoke-direct {v2, v7}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 282
    .local v2, "newAlbum":Ljava/io/File;
    if-eqz v2, :cond_0

    .line 283
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    if-eqz v7, :cond_1

    .line 291
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    invoke-virtual {v7, v12, v4}, Lcom/sec/samsung/gallery/util/FileUtil;->operateMedias(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/OnProgressListener;)Lcom/sec/samsung/gallery/util/MediaOperations;

    move-result-object v7

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mOperationTask:Lcom/sec/samsung/gallery/util/MediaOperations;

    .line 292
    .end local v2    # "newAlbum":Ljava/io/File;
    :cond_1
    :goto_1
    return-void

    .line 141
    .end local v4    # "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    :cond_2
    new-instance v7, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v7, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 142
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v8

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 143
    iget-object v7, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    new-instance v8, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$2;

    invoke-direct {v8, p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;)V

    invoke-virtual {v7, v6, v12, v11, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_0

    .line 285
    .restart local v4    # "onProgressListener":Lcom/sec/android/gallery3d/data/OnProgressListener;
    :catch_0
    move-exception v1

    .line 286
    .local v1, "ex":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private showNewAlbumCopyMoveDialog(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    :goto_0
    return-void

    .line 298
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->addObserver(Ljava/util/Observer;)V

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumCopyMoveDialog:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->showDialog()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v6, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 70
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object v0, v2

    check-cast v0, [Ljava/lang/Object;

    .line 71
    .local v0, "params":[Ljava/lang/Object;
    aget-object v2, v0, v3

    check-cast v2, Landroid/content/Context;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    .line 72
    aget-object v2, v0, v4

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mNewAlbumPath:Ljava/lang/String;

    .line 73
    const/4 v2, 0x2

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    .line 75
    .local v1, "show":Z
    if-nez v1, :cond_0

    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->dismissNewAlbumCopyMoveDialog()V

    .line 89
    :goto_0
    return-void

    .line 79
    :cond_0
    new-instance v2, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v5}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 80
    const/4 v2, -0x1

    iput v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    .line 81
    array-length v2, v0

    if-le v2, v6, :cond_3

    .line 82
    aget-object v2, v0, v6

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    .line 83
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-eq v2, v5, :cond_1

    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-eq v2, v5, :cond_1

    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v2, v5, :cond_3

    .line 84
    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mEventType:I

    sget v5, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v2, v5, :cond_2

    move v2, v3

    :goto_1
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->handleSelectedOperation(I)V

    goto :goto_0

    :cond_2
    move v2, v4

    goto :goto_1

    .line 88
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->showNewAlbumCopyMoveDialog(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 3
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 93
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 95
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_MOVE_FILES:I

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_REMOVE_FROM_SECRETBOX:I

    if-ne v1, v2, :cond_2

    .line 96
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->handleSelectedOperation(I)V

    .line 100
    :cond_1
    :goto_0
    return-void

    .line 97
    :cond_2
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v1, v2, :cond_1

    .line 98
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowNewAlbumCopyMoveDialogCmd;->handleSelectedOperation(I)V

    goto :goto_0
.end method
