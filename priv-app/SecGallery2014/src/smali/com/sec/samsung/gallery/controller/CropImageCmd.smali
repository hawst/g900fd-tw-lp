.class public Lcom/sec/samsung/gallery/controller/CropImageCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "CropImageCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final BOTH_SCREEN:I = 0x402

.field public static final LOCK_SCREEN:I = 0x401

.field private static final TAG:Ljava/lang/String;

.field public static final WALLPAPER_REQUEST_CODE:I = 0x403

.field public static final WALLPAPER_SCREEN:I = 0x400


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/sec/samsung/gallery/controller/CropImageCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/CropImageCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private static startCropActivity(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 44
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.camera.action.CROP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 46
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 47
    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/picasa/PicasaSource;->isPicasaImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1}, Lcom/sec/android/gallery3d/remote/cloud/CloudSource;->isCloudImage(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/16 v1, 0x302

    :goto_0
    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 51
    return-void

    .line 47
    :cond_1
    const/16 v1, 0x301

    goto :goto_0
.end method

.method public static startCropAndDeleteActivity(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 3
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 54
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.camera.action.CROP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    .local v0, "intent":Landroid/content/Intent;
    const-class v1, Lcom/sec/android/gallery3d/app/CropImage;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 56
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 57
    const-string v1, "is_slink"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 58
    const/16 v1, 0x30c

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 59
    return-void
.end method

.method public static startCropImageActivity(Landroid/app/Activity;Landroid/net/Uri;Ljava/lang/String;IZ)V
    .locals 19
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "pickerItem"    # Landroid/net/Uri;
    .param p2, "imageType"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "isFromrcs"    # Z

    .prologue
    .line 62
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0a0001

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v16

    if-nez v16, :cond_7

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v16

    if-nez v16, :cond_7

    .line 63
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWallpaperDesiredMinimumWidth()I

    move-result v15

    .line 64
    .local v15, "width":I
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWallpaperDesiredMinimumHeight()I

    move-result v5

    .line 65
    .local v5, "height":I
    if-le v15, v5, :cond_0

    move v6, v15

    .line 66
    .local v6, "longLength":I
    :goto_0
    const/4 v10, 0x0

    .line 67
    .local v10, "spotlightX":F
    const/4 v11, 0x0

    .line 68
    .local v11, "spotlightY":F
    const/16 v16, 0x402

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_1

    .line 69
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v10, v16, v17

    .line 70
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v11, v16, v17

    .line 79
    :goto_1
    new-instance v17, Landroid/content/Intent;

    const/16 v16, 0x400

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_2

    if-nez p4, :cond_2

    const-string v16, "com.android.camera.action.CROP"

    :goto_2
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v16, Lcom/sec/android/gallery3d/app/CropImage;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    move-object/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "outputX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "outputY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "aspectX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "aspectY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string/jumbo v17, "spotlightX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v16

    const-string/jumbo v17, "spotlightY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "scale"

    const/16 v18, 0x1

    invoke-virtual/range {v16 .. v18}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "noFaceDetection"

    const/16 v18, 0x1

    invoke-virtual/range {v16 .. v18}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-wallpaper"

    const/16 v16, 0x400

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_3

    const/16 v16, 0x1

    :goto_3
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-lockscreen"

    const/16 v16, 0x401

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_4

    const/16 v16, 0x1

    :goto_4
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-bothscreen"

    const/16 v16, 0x402

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_5

    const/16 v16, 0x1

    :goto_5
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    .line 93
    .local v8, "request":Landroid/content/Intent;
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v16, :cond_6

    .line 94
    const/16 v16, 0x403

    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 155
    :goto_6
    return-void

    .end local v6    # "longLength":I
    .end local v8    # "request":Landroid/content/Intent;
    .end local v10    # "spotlightX":F
    .end local v11    # "spotlightY":F
    :cond_0
    move v6, v5

    .line 65
    goto/16 :goto_0

    .line 72
    .restart local v6    # "longLength":I
    .restart local v10    # "spotlightX":F
    .restart local v11    # "spotlightY":F
    :cond_1
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 73
    .local v3, "display":Landroid/view/Display;
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 74
    .local v7, "outSize":Landroid/graphics/Point;
    invoke-virtual {v3, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 75
    iget v0, v7, Landroid/graphics/Point;->x:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v10, v16, v17

    .line 76
    iget v0, v7, Landroid/graphics/Point;->y:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v11, v16, v17

    goto/16 :goto_1

    .line 79
    .end local v3    # "display":Landroid/view/Display;
    .end local v7    # "outSize":Landroid/graphics/Point;
    :cond_2
    const-string v16, "com.sed.android.gallery3d.CROP_SEC_ONLY"

    goto/16 :goto_2

    :cond_3
    const/16 v16, 0x0

    goto :goto_3

    :cond_4
    const/16 v16, 0x0

    goto :goto_4

    :cond_5
    const/16 v16, 0x0

    goto :goto_5

    .line 96
    .restart local v8    # "request":Landroid/content/Intent;
    :cond_6
    const/high16 v16, 0x2000000

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 97
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_6

    .line 100
    .end local v5    # "height":I
    .end local v6    # "longLength":I
    .end local v8    # "request":Landroid/content/Intent;
    .end local v10    # "spotlightX":F
    .end local v11    # "spotlightY":F
    .end local v15    # "width":I
    :cond_7
    const/4 v15, 0x0

    .line 101
    .restart local v15    # "width":I
    const/4 v5, 0x0

    .line 102
    .restart local v5    # "height":I
    const/16 v16, 0x402

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_9

    .line 103
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v15

    .line 104
    invoke-static/range {p0 .. p0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v5

    .line 114
    :goto_7
    if-le v15, v5, :cond_a

    move v9, v5

    .line 115
    .local v9, "shortLength":I
    :goto_8
    if-ge v15, v5, :cond_b

    move v6, v5

    .line 116
    .restart local v6    # "longLength":I
    :goto_9
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWallpaperDesiredMinimumWidth()I

    move-result v13

    .line 117
    .local v13, "wallpaperWidth":I
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWallpaperDesiredMinimumHeight()I

    move-result v12

    .line 118
    .local v12, "wallpaperHeight":I
    if-le v13, v12, :cond_c

    move v14, v13

    .line 120
    .local v14, "wallpaperlongLength":I
    :goto_a
    int-to-float v0, v9

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v10, v16, v17

    .line 121
    .restart local v10    # "spotlightX":F
    int-to-float v0, v6

    move/from16 v16, v0

    int-to-float v0, v6

    move/from16 v17, v0

    div-float v11, v16, v17

    .line 122
    .restart local v11    # "spotlightY":F
    new-instance v17, Landroid/content/Intent;

    const/16 v16, 0x400

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_d

    const-string v16, "com.android.camera.action.CROP"

    :goto_b
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "outputX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "outputY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v14}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "aspectX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "aspectY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v16

    const-string/jumbo v17, "spotlightX"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v16

    const-string/jumbo v17, "spotlightY"

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "scale"

    const/16 v18, 0x1

    invoke-virtual/range {v16 .. v18}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v16

    const-string v17, "noFaceDetection"

    const/16 v18, 0x1

    invoke-virtual/range {v16 .. v18}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-wallpaper"

    const/16 v16, 0x400

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_e

    const/16 v16, 0x1

    :goto_c
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-lockscreen"

    const/16 v16, 0x401

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_f

    const/16 v16, 0x1

    :goto_d
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v17

    const-string v18, "set-as-bothscreen"

    const/16 v16, 0x402

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_10

    const/16 v16, 0x1

    :goto_e
    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move/from16 v2, v16

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v8

    .line 135
    .restart local v8    # "request":Landroid/content/Intent;
    const/16 v16, 0x400

    move/from16 v0, p3

    move/from16 v1, v16

    if-eq v0, v1, :cond_8

    .line 136
    const-string v16, "scaleUpIfNeeded"

    const/16 v17, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 138
    :cond_8
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWallpaperCropNavigation:Z

    if-eqz v16, :cond_11

    .line 140
    const/16 v16, 0x403

    :try_start_0
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v8, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_6

    .line 141
    :catch_0
    move-exception v4

    .line 142
    .local v4, "e":Landroid/content/ActivityNotFoundException;
    sget-object v16, Lcom/sec/samsung/gallery/controller/CropImageCmd;->TAG:Ljava/lang/String;

    const-string v17, "Activity Not Found"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    invoke-virtual {v4}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_6

    .line 106
    .end local v4    # "e":Landroid/content/ActivityNotFoundException;
    .end local v6    # "longLength":I
    .end local v8    # "request":Landroid/content/Intent;
    .end local v9    # "shortLength":I
    .end local v10    # "spotlightX":F
    .end local v11    # "spotlightY":F
    .end local v12    # "wallpaperHeight":I
    .end local v13    # "wallpaperWidth":I
    .end local v14    # "wallpaperlongLength":I
    :cond_9
    invoke-virtual/range {p0 .. p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    .line 107
    .restart local v3    # "display":Landroid/view/Display;
    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    .line 108
    .restart local v7    # "outSize":Landroid/graphics/Point;
    invoke-virtual {v3, v7}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 110
    iget v15, v7, Landroid/graphics/Point;->x:I

    .line 111
    iget v5, v7, Landroid/graphics/Point;->y:I

    goto/16 :goto_7

    .end local v3    # "display":Landroid/view/Display;
    .end local v7    # "outSize":Landroid/graphics/Point;
    :cond_a
    move v9, v15

    .line 114
    goto/16 :goto_8

    .restart local v9    # "shortLength":I
    :cond_b
    move v6, v15

    .line 115
    goto/16 :goto_9

    .restart local v6    # "longLength":I
    .restart local v12    # "wallpaperHeight":I
    .restart local v13    # "wallpaperWidth":I
    :cond_c
    move v14, v12

    .line 118
    goto/16 :goto_a

    .line 122
    .restart local v10    # "spotlightX":F
    .restart local v11    # "spotlightY":F
    .restart local v14    # "wallpaperlongLength":I
    :cond_d
    const-string v16, "com.sed.android.gallery3d.CROP_SEC_ONLY"

    goto/16 :goto_b

    :cond_e
    const/16 v16, 0x0

    goto/16 :goto_c

    :cond_f
    const/16 v16, 0x0

    goto :goto_d

    :cond_10
    const/16 v16, 0x0

    goto :goto_e

    .line 146
    .restart local v8    # "request":Landroid/content/Intent;
    :cond_11
    const/high16 v16, 0x2000000

    move/from16 v0, v16

    invoke-virtual {v8, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 148
    :try_start_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_6

    .line 149
    :catch_1
    move-exception v4

    .line 150
    .restart local v4    # "e":Landroid/content/ActivityNotFoundException;
    sget-object v16, Lcom/sec/samsung/gallery/controller/CropImageCmd;->TAG:Ljava/lang/String;

    const-string v17, "Activity Not Found"

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    invoke-virtual {v4}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto/16 :goto_6
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 36
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 37
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/app/Activity;

    .line 38
    .local v0, "activity":Landroid/app/Activity;
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 40
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/CropImageCmd;->startCropActivity(Landroid/app/Activity;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 41
    return-void
.end method
