.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.source "GlComposeEventView.java"


# static fields
.field protected static final CMD_SELECT_ALBUM:I = 0x65

.field protected static final DURATION:I = 0x190


# instance fields
.field protected mComposeEventViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

.field private mFlags:I

.field public mListenerMapClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mNeedSetMode:Z

.field private mPressed:Z

.field private mValidView:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "initCode"    # I
    .param p3, "initItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "option"    # I
    .param p5, "config"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    .line 25
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPressed:Z

    .line 26
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mNeedSetMode:Z

    .line 27
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlags:I

    .line 201
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mListenerMapClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 226
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mComposeEventViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    .line 32
    return-void
.end method

.method private setValidView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mValidView:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHeight:I

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mValidView:Landroid/graphics/Rect;

    .line 68
    :goto_0
    return-void

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mValidView:Landroid/graphics/Rect;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHeight:I

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method


# virtual methods
.method public getFirstThumbObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mGlObject:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public getItemWidth()F
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemW:F

    return v0
.end method

.method public handleSelectAlbum(II)V
    .locals 6
    .param p1, "index"    # I
    .param p2, "changed"    # I

    .prologue
    const/4 v5, 0x1

    .line 136
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v3, :cond_0

    .line 152
    :goto_0
    return-void

    .line 139
    :cond_0
    if-eqz p2, :cond_1

    .line 140
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 142
    :cond_1
    shl-int/lit8 v0, p1, 0x10

    .line 143
    .local v0, "code":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getScrollForIndex(I)F

    move-result v2

    .line 144
    .local v2, "scroll":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getVisibleScrollDelta(F)F

    move-result v1

    .line 145
    .local v1, "newScroll":F
    const/4 v3, 0x0

    cmpl-float v3, v1, v3

    if-eqz v3, :cond_2

    .line 146
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mScrollable:F

    add-float/2addr v1, v3

    .line 147
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->getValidScroll(F)F

    move-result v1

    .line 148
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 149
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setScroll(FZ)V

    .line 151
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v3, v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 3

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setValidView()V

    .line 37
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onCreate()V

    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mInitialCode:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->setFocused(IZ)V

    .line 40
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->IsCheckMode()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mNeedSetMode:Z

    .line 42
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlingInTalkBack:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mGlComposeViewAccessibility:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mComposeEventViewItemAccessibilityListener:Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility;->setAccessibilityNodeInfoListener(Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;)V

    .line 45
    :cond_0
    return-void
.end method

.method protected onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 163
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->isSlideShowMode:Z

    if-eqz v5, :cond_1

    .line 189
    :cond_0
    :goto_0
    return v3

    .line 165
    :cond_1
    const/16 v5, 0xa8

    if-eq p1, v5, :cond_2

    const/16 v5, 0xa9

    if-eq p1, v5, :cond_2

    const/16 v5, 0x118

    if-eq p1, v5, :cond_2

    const/16 v5, 0x117

    if-ne p1, v5, :cond_3

    :cond_2
    move v3, v4

    .line 167
    goto :goto_0

    .line 170
    :cond_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->isScreenLocked()Z

    move-result v5

    if-nez v5, :cond_0

    .line 173
    const/16 v5, 0x42

    if-eq p1, v5, :cond_4

    const/16 v5, 0x17

    if-ne p1, v5, :cond_6

    .line 174
    :cond_4
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    if-ltz v5, :cond_5

    .line 175
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v5, :cond_5

    .line 176
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v2

    .line 177
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 178
    if-eqz v2, :cond_0

    .line 181
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    shr-int/lit8 v0, v5, 0x10

    .line 182
    .local v0, "albumIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    const v6, 0xffff

    and-int v1, v5, v6

    .line 183
    .local v1, "itemIndex":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    invoke-interface {v5, v6, v7, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 184
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v6, 0x4

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mFocused:I

    invoke-virtual {v5, v6, v7, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .end local v0    # "albumIndex":I
    .end local v1    # "itemIndex":I
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_5
    move v3, v4

    .line 187
    goto :goto_0

    .line 189
    :cond_6
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto :goto_0
.end method

.method protected onMessageExtra(ILjava/lang/Object;III)V
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 72
    const/16 v0, 0x65

    if-ne p1, v0, :cond_0

    .line 73
    invoke-virtual {p0, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->handleSelectAlbum(II)V

    .line 75
    :cond_0
    return-void
.end method

.method protected onMoved(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPressed:Z

    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onMoved(II)Z

    move-result v0

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 92
    if-ne p1, v1, :cond_0

    if-eq p2, v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mValidView:Landroid/graphics/Rect;

    invoke-virtual {v1, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 93
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPressed:Z

    .line 94
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onPressed(II)Z

    move-result v0

    .line 97
    :goto_0
    return v0

    .line 96
    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPressed:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mPressed:Z

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 113
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onReleased(IIII)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSetMode(IILjava/lang/Object;)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 118
    return-void
.end method

.method protected playSoundOnClickThumb()V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 195
    return-void
.end method

.method protected resetLayout()V
    .locals 5

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setValidView()V

    .line 58
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetLayout()V

    .line 59
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mWidthSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHeightSpace:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mWidth:I

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHeight:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 61
    :cond_1
    return-void
.end method

.method protected resetScrollBar()V
    .locals 3

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 84
    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->resetScrollBar()V

    .line 86
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setValidView()V

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mScrollBar:Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mWidth:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mValidView:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int/2addr v1, v2

    add-int/lit8 v1, v1, 0xa

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->convX(I)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeScrollBar;->setRightPadding(F)V

    goto :goto_0
.end method

.method public selectAlbum(IZ)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "changed"    # Z

    .prologue
    const/4 v1, 0x0

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/16 v3, 0x65

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v3, p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 133
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 131
    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 49
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 50
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mNeedSetMode:Z

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mNeedSetMode:Z

    .line 53
    :cond_0
    return-void
.end method

.method public setFlags(IZ)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "set"    # Z

    .prologue
    .line 155
    if-eqz p2, :cond_0

    .line 156
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlags:I

    .line 159
    :goto_0
    return-void

    .line 158
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mFlags:I

    goto :goto_0
.end method

.method public updateFolderItem(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/4 v3, 0x0

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v1, 0x3

    shl-int/lit8 v2, p1, 0x10

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 127
    :cond_0
    return-void
.end method
