.class public Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;
.super Ljava/lang/Object;
.source "DetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/util/ThreadPool$Job;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "InitAGIFJob"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/sec/android/gallery3d/util/ThreadPool$Job",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p2, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 5764
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5765
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 5766
    return-void
.end method


# virtual methods
.method public bridge synthetic run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 5761
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Void;
    .locals 4
    .param p1, "jc"    # Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    .prologue
    .line 5770
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->initAgif(Lcom/sec/android/gallery3d/data/MediaItem;)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$8500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 5771
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$InitAGIFJob;->mMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v2

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mAgifVersion:J
    invoke-static {v0, v2, v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$7802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;J)J

    .line 5772
    const/4 v0, 0x0

    return-object v0
.end method
