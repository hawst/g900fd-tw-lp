.class Lcom/sec/samsung/gallery/view/allview/AllViewState$12;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 476
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDropAlbumItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;ILandroid/net/Uri;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 485
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SAVE_OBJINFO_FORANIM:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setFlags(IZ)V

    .line 486
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$3200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v1, 0x7f0e02e4

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 487
    return-void
.end method

.method public onDropItemClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;ILandroid/net/Uri;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I
    .param p4, "uri"    # Landroid/net/Uri;

    .prologue
    .line 479
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SAVE_OBJINFO_FORANIM:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setFlags(IZ)V

    .line 480
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$12;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$3100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const v1, 0x7f0e02e4

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 481
    return-void
.end method
