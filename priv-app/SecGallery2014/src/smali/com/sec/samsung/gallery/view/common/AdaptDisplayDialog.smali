.class public Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;
.super Lcom/sec/samsung/gallery/view/ViewObservable;
.source "AdaptDisplayDialog.java"


# static fields
.field private static final SCREEN_MODE_AUTOMATIC_SETTING:Ljava/lang/String; = "screen_mode_automatic_setting"


# instance fields
.field private mAdaptDisplayDialogOff:Z

.field private final mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

.field private final mContext:Landroid/content/Context;

.field private mDialog:Landroid/app/Dialog;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/GalleryApp;Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "app"    # Lcom/sec/android/gallery3d/app/GalleryApp;
    .param p2, "c"    # Landroid/content/Context;
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/ViewObservable;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mAdaptDisplayDialogOff:Z

    .line 31
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mApp:Lcom/sec/android/gallery3d/app/GalleryApp;

    .line 32
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    .line 33
    return-void
.end method


# virtual methods
.method public createDialog()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 50
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v5}, Landroid/app/Dialog;->isShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 98
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    const-string v6, "layout_inflater"

    invoke-virtual {v5, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 56
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f030019

    const/4 v6, 0x0

    invoke-virtual {v2, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 57
    .local v3, "layout":Landroid/view/View;
    const v5, 0x7f0f0050

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 58
    .local v4, "message1":Landroid/widget/TextView;
    const v5, 0x7f0f001d

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 60
    .local v1, "checkBox":Landroid/widget/CheckBox;
    const v5, 0x7f0e02e6

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(I)V

    .line 62
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 63
    const v5, 0x7f0e02e5

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 64
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 66
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setSoundEffectsEnabled(Z)V

    .line 67
    invoke-virtual {v1, v7}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 68
    new-instance v5, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$1;-><init>(Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    new-instance v5, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$2;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$2;-><init>(Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;)V

    invoke-virtual {v1, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 81
    const v5, 0x7f0e0070

    new-instance v6, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$3;

    invoke-direct {v6, p0, v1}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$3;-><init>(Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 90
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    .line 91
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    new-instance v6, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$4;

    invoke-direct {v6, p0, v1}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog$4;-><init>(Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 97
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    goto :goto_0
.end method

.method public dismissDialog()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 46
    :cond_0
    return-void
.end method

.method public isAdaptDisplayDialogOff()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 102
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "screen_mode_automatic_setting"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 103
    .local v0, "adaptDisplaySettingOn":I
    if-ne v0, v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    const-string v2, "AdaptDisplayDialogOff"

    invoke-static {v1, v2, v4}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mAdaptDisplayDialogOff:Z

    .line 105
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mAdaptDisplayDialogOff:Z

    .line 107
    :cond_0
    return v1
.end method

.method public setAdaptDisplayDialogOff(Z)V
    .locals 3
    .param p1, "adaptDisplayDialogOff"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mAdaptDisplayDialogOff:Z

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mContext:Landroid/content/Context;

    const-string v1, "AdaptDisplayDialogOff"

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mAdaptDisplayDialogOff:Z

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 114
    return-void
.end method

.method public showDialog()V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->createDialog()V

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/AdaptDisplayDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 40
    :cond_0
    return-void
.end method
