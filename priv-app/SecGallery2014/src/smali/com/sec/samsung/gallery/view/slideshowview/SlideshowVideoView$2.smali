.class Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;
.super Ljava/lang/Object;
.source "SlideshowVideoView.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V
    .locals 0

    .prologue
    .line 137
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 5
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const/4 v4, 0x0

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->getGlSlideShowView()Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->abandonAudioFocus()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)V

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mSlideShowView:Lcom/sec/samsung/gallery/glview/GlSlideShowView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$000(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Lcom/sec/samsung/gallery/glview/GlSlideShowView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSlideShowView;->getInstance()Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->updateLayer()V

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->misVideoPlaying:Z

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$300(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView$2;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->mMediaPlayer:Landroid/media/MediaPlayer;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;->access$302(Lcom/sec/samsung/gallery/view/slideshowview/SlideshowVideoView;Landroid/media/MediaPlayer;)Landroid/media/MediaPlayer;

    .line 153
    :cond_0
    return-void
.end method
