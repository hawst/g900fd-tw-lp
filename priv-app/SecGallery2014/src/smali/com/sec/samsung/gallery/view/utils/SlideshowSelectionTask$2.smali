.class Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;
.super Ljava/lang/Object;
.source "SlideshowSelectionTask.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)V
    .locals 0

    .prologue
    .line 83
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 4
    .param p1, "lhs"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "rhs"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;->this$0:Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    # getter for: Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->mIsOldest:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->access$200(Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    .line 90
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v0

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/common/Utils;->compare(JJ)I

    move-result v0

    neg-int v0, v0

    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 83
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask$2;->compare(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method
