.class public Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlSpreadAnimation.java"


# static fields
.field private static final DURATION:I = 0x190


# instance fields
.field private mActiveObjs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;"
        }
    .end annotation
.end field

.field private mAlbumObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

.field private mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mRan:Ljava/util/Random;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 18
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mRan:Ljava/util/Random;

    .line 19
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    .line 20
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 4
    .param p1, "ratio"    # F

    .prologue
    .line 79
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 82
    .local v0, "activeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 83
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 84
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget-object v3, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 82
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 86
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    return-void
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->onStop()V

    .line 107
    return-void
.end method

.method protected onStop()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 89
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 92
    .local v6, "activeCount":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 93
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 94
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->applyTransform(F)V

    .line 95
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v6, :cond_0

    .line 96
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 97
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 98
    invoke-virtual {v0, v8, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setScale(FF)V

    move v2, v1

    move v4, v3

    move v5, v3

    .line 99
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 95
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 101
    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 103
    .end local v7    # "i":I
    :cond_1
    return-void
.end method

.method public startAnimation(Landroid/util/SparseArray;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 12
    .param p2, "albumObj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<+",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "activeObjs":Landroid/util/SparseArray;, "Landroid/util/SparseArray<+Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;>;"
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 23
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v6

    .line 26
    .local v6, "activeCount":I
    if-nez p2, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->isRunning()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 30
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->stop()V

    .line 32
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 33
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v6, :cond_3

    .line 34
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mActiveObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 33
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 37
    :cond_3
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mAlbumObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 38
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mAlbumObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsX()F

    move-result v9

    .line 39
    .local v9, "sx":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mAlbumObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsY()F

    move-result v10

    .line 40
    .local v10, "sy":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mAlbumObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getAbsZ()F

    move-result v11

    .line 42
    .local v11, "sz":F
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 43
    .local v0, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getParent()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 47
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v3, :cond_0

    .line 50
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsX()F

    move-result v3

    sub-float/2addr v9, v3

    .line 51
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsY()F

    move-result v3

    sub-float/2addr v10, v3

    .line 52
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getAbsZ()F

    move-result v3

    sub-float/2addr v11, v3

    .line 53
    const/4 v7, 0x0

    :goto_2
    if-ge v7, v6, :cond_5

    .line 54
    invoke-virtual {p1, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 55
    .restart local v0    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v0, v9, v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourcePos(FFF)V

    .line 56
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setCurrentPosToTarget()V

    .line 57
    invoke-virtual {v0, v9, v10, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 59
    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceScale(F)V

    .line 60
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetScale(F)V

    move v3, v2

    move v4, v1

    move v5, v1

    .line 61
    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAnim(ZZZZZ)V

    .line 62
    const/16 v3, 0x190

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setDimDuration(I)V

    .line 63
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mRan:Ljava/util/Random;

    invoke-virtual {v3}, Ljava/util/Random;->nextInt()I

    move-result v3

    rem-int/lit8 v8, v3, 0x1e

    .line 64
    .local v8, "roll":I
    if-gez v8, :cond_4

    .line 65
    add-int/lit8 v8, v8, -0xf

    .line 68
    :goto_3
    int-to-float v3, v8

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceRoll(F)V

    .line 69
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetRoll(F)V

    .line 70
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setVisibility(Z)V

    .line 53
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 67
    :cond_4
    add-int/lit8 v8, v8, 0xf

    goto :goto_3

    .line 72
    .end local v8    # "roll":I
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mParentObj:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 73
    const-wide/16 v2, 0x190

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->setDuration(J)V

    .line 74
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->setInterpolator(Lcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 75
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlSpreadAnimation;->start()V

    goto/16 :goto_0
.end method
