.class Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;
.super Ljava/lang/Object;
.source "ChnUsageAlertDialogCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->showDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 157
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/16 v7, 0xe

    const/16 v6, 0xd

    const/4 v4, 0x6

    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 160
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_2

    .line 162
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "LocationNetworkAlertDialogOff"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 173
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # setter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->justSelectedKeyOK:Z
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$102(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;Z)Z

    .line 175
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_6

    .line 176
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "LocationPermissionAlertDialogOff"

    invoke-static {v1, v2, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "MOREINFO_EVENT"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 198
    :cond_1
    :goto_1
    return-void

    .line 163
    :cond_2
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_3

    .line 164
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "ChangePlayerWifiDataAlertDialogOff"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 165
    :cond_3
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_4

    .line 166
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "ScanNearbyDeviceWifiDataAlertDialogOff"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 167
    :cond_4
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v6, :cond_5

    .line 168
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "TagBuddyPermissionAlertDialogOff"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 169
    :cond_5
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v7, :cond_0

    .line 170
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    const-string v2, "FaceTagPermissionAlertDialogOff"

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 181
    :cond_6
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_7

    .line 182
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 183
    .local v0, "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 184
    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$200()Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0xfa

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    .line 185
    .end local v0    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_7
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_8

    .line 186
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 187
    .restart local v0    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->startNearbyFramework(Lcom/sec/android/gallery3d/data/DataManager;)V

    .line 188
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSamsungLinkApi:Z

    if-eqz v1, :cond_1

    .line 189
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/android/gallery3d/remote/slink/SLinkManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->acquireWakeLockIfNeeded(Ljava/lang/String;)V

    goto/16 :goto_1

    .line 191
    .end local v0    # "dataManager":Lcom/sec/android/gallery3d/data/DataManager;
    :cond_8
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v6, :cond_9

    .line 192
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeFaceTag()V

    .line 193
    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$200()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1

    .line 194
    :cond_9
    sget-object v1, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->popupType:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v7, :cond_1

    .line 195
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd$3;->this$0:Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;)Landroid/content/Context;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->initializeFaceTag()V

    .line 196
    # getter for: Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->mHandler:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/ChnUsageAlertDialogCmd;->access$200()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_1
.end method
