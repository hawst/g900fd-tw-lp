.class public Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlShrinkAnimation.java"


# static fields
.field private static final EVT_ANIMATION_CANCEL:I = 0x1

.field private static final SCALE_DELTA:F = 0.6f

.field private static final SCALE_LIMIT:F = 10.0f

.field private static final SCALE_MIN:F = 0.39999998f

.field public static final SHRINK_CANCLED:I = -0x1

.field public static final SHRINK_DONE:I = 0x1

.field public static final SHRINK_RUNNING:I = 0x0

.field private static final TAG:Ljava/lang/String; = "GlShrinkAnimation "


# instance fields
.field private inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

.field public mActive:Z

.field private mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field public mAnimForward:Z

.field public mAutoAnimation:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCropRect:Landroid/graphics/Rect;

.field private mCurrentScale:F

.field private mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

.field private mFadeInObjs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;"
        }
    .end annotation
.end field

.field private mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

.field private mIdxAlb:I

.field private mIdxThm:I

.field private mIsRequestComplete:Z

.field public mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

.field private mManual:Z

.field private mMessageSent:Z

.field private mOrigX:F

.field private mOrigY:F

.field private mOrigZ:F

.field private mParentObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mRatioRange:F

.field mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

.field private mRotation:I

.field private mSrcImgH:I

.field private mSrcImgW:I


# direct methods
.method public constructor <init>(Z)V
    .locals 2
    .param p1, "doAnimation"    # Z

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    .line 321
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation$2;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    .line 60
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAutoAnimation:Z

    .line 61
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    .line 62
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 63
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    .line 64
    return-void
.end method

.method private setObjectRect()V
    .locals 15

    .prologue
    .line 288
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v0, v11, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 289
    .local v0, "layer":Lcom/sec/android/gallery3d/glcore/GlLayer;
    iget v4, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    .line 290
    .local v4, "spaceW":F
    iget v3, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    .line 291
    .local v3, "spaceH":F
    iget v11, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidth:I

    int-to-float v10, v11

    .line 292
    .local v10, "viewW":F
    iget v11, v0, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeight:I

    int-to-float v9, v11

    .line 297
    .local v9, "viewH":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotation:I

    if-eqz v11, :cond_0

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotation:I

    const/16 v12, 0xb4

    if-ne v11, v12, :cond_2

    .line 298
    :cond_0
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgW:I

    int-to-float v7, v11

    .line 299
    .local v7, "srcImgW":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgH:I

    int-to-float v6, v11

    .line 304
    .local v6, "srcImgH":F
    :goto_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v13, 0x0

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getWidth(Z)F

    move-result v12

    iget-object v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getHeight(Z)F

    move-result v13

    invoke-virtual {v11, v12, v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTargetSize(FF)V

    .line 305
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v12}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsX()F

    move-result v12

    iget-object v13, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsY()F

    move-result v13

    iget-object v14, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsZ()F

    move-result v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTargetPos(FFF)V

    .line 306
    mul-float v11, v10, v6

    mul-float v12, v9, v7

    cmpl-float v11, v11, v12

    if-lez v11, :cond_3

    .line 308
    div-float v2, v9, v6

    .line 313
    .local v2, "scale":F
    :goto_1
    const/high16 v11, 0x41200000    # 10.0f

    cmpl-float v11, v2, v11

    if-lez v11, :cond_1

    const/high16 v2, 0x41200000    # 10.0f

    .line 314
    :cond_1
    div-float v1, v4, v10

    .line 315
    .local v1, "ratio":F
    mul-float v11, v7, v2

    mul-float v8, v11, v1

    .line 316
    .local v8, "srcW":F
    mul-float v11, v6, v2

    mul-float v5, v11, v1

    .line 317
    .local v5, "srcH":F
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v11, v8, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSourceSize(FF)V

    .line 318
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v12, 0x0

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getAbsZ()F

    move-result v14

    invoke-virtual {v11, v12, v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setSourcePos(FFF)V

    .line 319
    return-void

    .line 301
    .end local v1    # "ratio":F
    .end local v2    # "scale":F
    .end local v5    # "srcH":F
    .end local v6    # "srcImgH":F
    .end local v7    # "srcImgW":F
    .end local v8    # "srcW":F
    :cond_2
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgH:I

    int-to-float v7, v11

    .line 302
    .restart local v7    # "srcImgW":F
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgW:I

    int-to-float v6, v11

    .restart local v6    # "srcImgH":F
    goto :goto_0

    .line 311
    :cond_3
    div-float v2, v10, v7

    .restart local v2    # "scale":F
    goto :goto_1
.end method

.method private setOriginalPos(Z)V
    .locals 4
    .param p1, "isSet"    # Z

    .prologue
    .line 278
    if-eqz p1, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigX:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigY:F

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigZ:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setPos(FFF)V

    .line 285
    :goto_0
    return-void

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getX()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigX:F

    .line 282
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigY:F

    .line 283
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getY()F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mOrigY:F

    goto :goto_0
.end method


# virtual methods
.method public addFadeInObj(Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)V
    .locals 2
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p1, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mFadeInObjs:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 143
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->applyTransform(F)V

    goto :goto_0
.end method

.method public addFadeInObjs(Landroid/util/SparseArray;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<+",
            "Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "objList":Landroid/util/SparseArray;, "Landroid/util/SparseArray<+Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;>;"
    const/4 v5, 0x0

    .line 147
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    if-nez v3, :cond_0

    .line 159
    :goto_0
    return-void

    .line 150
    :cond_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    .line 153
    .local v0, "activeCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 154
    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 155
    .local v2, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v2, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    .line 156
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mFadeInObjs:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 158
    .end local v2    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    :cond_1
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->applyTransform(F)V

    goto :goto_0
.end method

.method protected applyTransform(F)V
    .locals 17
    .param p1, "ratio"    # F

    .prologue
    .line 218
    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float v13, v13, p1

    const/high16 v14, 0x3f800000    # 1.0f

    sub-float v14, v14, p1

    mul-float/2addr v13, v14

    sub-float v5, v12, v13

    .line 219
    .local v5, "intRatio":F
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    if-eqz v12, :cond_1

    move/from16 v8, p1

    .line 221
    .local v8, "proRatio":F
    :goto_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->left:I

    int-to-float v12, v12

    mul-float/2addr v12, v8

    float-to-int v6, v12

    .line 222
    .local v6, "left":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgW:I

    int-to-float v12, v12

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v8

    mul-float/2addr v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCropRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    int-to-float v13, v13

    mul-float/2addr v13, v8

    add-float/2addr v12, v13

    float-to-int v10, v12

    .line 223
    .local v10, "right":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCropRect:Landroid/graphics/Rect;

    iget v12, v12, Landroid/graphics/Rect;->top:I

    int-to-float v12, v12

    mul-float/2addr v12, v8

    float-to-int v11, v12

    .line 224
    .local v11, "top":I
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgH:I

    int-to-float v12, v12

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v8

    mul-float/2addr v12, v13

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCropRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    int-to-float v13, v13

    mul-float/2addr v13, v8

    add-float/2addr v12, v13

    float-to-int v2, v12

    .line 225
    .local v2, "bottom":I
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    if-nez v12, :cond_0

    .line 226
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mSrcRoll:F

    const/high16 v14, 0x3f800000    # 1.0f

    sub-float/2addr v14, v5

    mul-float/2addr v13, v14

    invoke-virtual {v12, v13}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRoll(F)V

    .line 228
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    int-to-float v13, v6

    int-to-float v14, v11

    int-to-float v15, v10

    int-to-float v0, v2

    move/from16 v16, v0

    invoke-virtual/range {v12 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTexCoords(FFFF)V

    .line 229
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v12, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mTransAnim:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;

    invoke-virtual {v12, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject$TranformAnimation;->applyTransform(F)V

    .line 230
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mFadeInObjs:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 231
    .local v7, "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setAlpha(F)V

    goto :goto_1

    .line 219
    .end local v2    # "bottom":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v6    # "left":I
    .end local v7    # "obj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v8    # "proRatio":F
    .end local v10    # "right":I
    .end local v11    # "top":I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    const/high16 v13, 0x3f800000    # 1.0f

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    sub-float/2addr v13, v14

    mul-float/2addr v13, v5

    add-float v8, v12, v13

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    const/high16 v13, 0x3f800000    # 1.0f

    sub-float/2addr v13, v5

    mul-float v8, v12, v13

    goto/16 :goto_0

    .line 233
    .restart local v2    # "bottom":I
    .restart local v4    # "i$":Ljava/util/Iterator;
    .restart local v6    # "left":I
    .restart local v8    # "proRatio":F
    .restart local v10    # "right":I
    .restart local v11    # "top":I
    :cond_3
    sget-boolean v12, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v12, :cond_4

    .line 234
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    if-eqz v12, :cond_5

    .line 235
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v12, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    sget v13, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    sget v14, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_GREEN:F

    sget v15, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    const/high16 v16, 0x3f800000    # 1.0f

    invoke-virtual/range {v12 .. v16}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    .line 245
    :cond_4
    :goto_2
    return-void

    .line 239
    :cond_5
    sget v12, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    mul-float v9, v12, v5

    .line 240
    .local v9, "r":F
    sget v12, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_GREEN:F

    mul-float v3, v12, v5

    .line 241
    .local v3, "g":F
    sget v12, Lcom/sec/android/gallery3d/glcore/GlRootView;->WHITE_THEME_CLEARCOLOR_RED:F

    mul-float v1, v12, v5

    .line 242
    .local v1, "b":F
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v12, v12, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/high16 v13, 0x3f800000    # 1.0f

    invoke-virtual {v12, v9, v3, v1, v13}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setGlBackgroundColor(FFFF)V

    goto :goto_2
.end method

.method public cancel()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 205
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 206
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    .line 207
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    .line 208
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    .line 209
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    .line 210
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRoll(F)V

    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRotationGesture(Z)V

    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 215
    :cond_0
    return-void
.end method

.method protected onCancel()V
    .locals 3

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 264
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->applyTransform(F)V

    .line 265
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 266
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    .line 267
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    .line 268
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    .line 269
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    .line 270
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRoll(F)V

    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRotationGesture(Z)V

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 275
    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 248
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->getLastRatio()F

    move-result v0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_0

    .line 249
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->applyTransform(F)V

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v1, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setTexCoords(FFFF)V

    .line 252
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mParentObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 255
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setOriginalPos(Z)V

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRotationGesture(Z)V

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 259
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    .line 260
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    .line 261
    return-void
.end method

.method public prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;)Z
    .locals 13
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    .param p2, "object"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x3

    const-wide/16 v8, 0x64

    const/4 v12, 0x1

    const/4 v5, 0x0

    .line 67
    check-cast p1, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-object v0, p2

    .line 68
    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->getParent()Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mParentObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    shr-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxAlb:I

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mIndex:I

    const v1, 0xffff

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxThm:I

    .line 72
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mFadeInObjs:Ljava/util/ArrayList;

    .line 73
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p2, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getWidth(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual {p2, v12}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getHeight(Z)F

    move-result v1

    iput v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxAlb:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxThm:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v10, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 78
    .local v10, "bitmap":Landroid/graphics/Bitmap;
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWaitForShrinkAnimationUntilDecodingCompleted:Z

    if-eqz v0, :cond_0

    if-nez v10, :cond_0

    .line 79
    const-string v0, "GlShrinkAnimation "

    const-string v1, "prepareAnimation wait util bitmap is prepared !"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 85
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxAlb:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIdxThm:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v10, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 88
    :cond_0
    if-eqz v10, :cond_1

    invoke-virtual {v10}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    :cond_1
    const-string v0, "GlShrinkAnimation "

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "prepareAnimation bitmap = null or recycled : skipped !!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    :goto_1
    return v5

    .line 82
    :catch_0
    move-exception v11

    .line 83
    .local v11, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v11}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    .line 92
    .end local v11    # "e":Ljava/lang/InterruptedException;
    :cond_2
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mActive:Z

    .line 93
    iput-object v10, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mBitmap:Landroid/graphics/Bitmap;

    .line 94
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgW:I

    .line 95
    invoke-virtual {v10}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mSrcImgH:I

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotation:I

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCropRect:Landroid/graphics/Rect;

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->inter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 100
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setOriginalPos(Z)V

    .line 101
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setObjectRect()V

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mParentObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->removeChild(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEmptyFill(Z)V

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setDecorState(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotation:I

    neg-int v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setVertexRotation(I)V

    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->moveToLast()V

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setEnableRollAnim(Z)V

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, v12}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRotationGesture(Z)V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRotateListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setRotationListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlRotateListener;)V

    .line 112
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    .line 113
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    .line 114
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    .line 115
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation$1;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation$1;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;Lcom/sec/android/gallery3d/glcore/GlRootView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    .line 124
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAutoAnimation:Z

    if-eqz v0, :cond_3

    .line 125
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMessageSent:Z

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 127
    const-wide/16 v0, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setDuration(J)V

    .line 128
    invoke-virtual {p0, v8, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->startAfter(J)V

    .line 133
    :goto_2
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    move v5, v12

    .line 134
    goto/16 :goto_1

    .line 130
    :cond_3
    iput-boolean v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMessageSent:Z

    .line 131
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    move v4, v12

    move v6, v5

    move v7, v5

    invoke-virtual/range {v3 .. v9}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessageDelayed(IIIIJ)V

    goto :goto_2
.end method

.method public requestComplete()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 180
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    sub-float/2addr v0, v1

    const v1, 0x3f19999a    # 0.6f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    .line 181
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    .line 182
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mIsRequestComplete:Z

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 184
    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setDuration(J)V

    .line 185
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->start()V

    .line 186
    return-void
.end method

.method public requestCompleteForward(Z)V
    .locals 2
    .param p1, "useAnimation"    # Z

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->isRunning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    :goto_0
    return-void

    .line 191
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 192
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    sub-float/2addr v0, v1

    const v1, 0x3f19999a    # 0.6f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mRatioRange:F

    .line 193
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimForward:Z

    .line 194
    if-eqz p1, :cond_1

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMainObj:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 196
    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->setDuration(J)V

    .line 197
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->start()V

    goto :goto_0

    .line 199
    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mAnimState:I

    .line 200
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->stop()V

    goto :goto_0
.end method

.method public setScale(F)V
    .locals 3
    .param p1, "scale"    # F

    .prologue
    const v2, 0x3ecccccc    # 0.39999998f

    const/high16 v1, 0x3f800000    # 1.0f

    .line 162
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMessageSent:Z

    if-eqz v0, :cond_0

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mMessageSent:Z

    .line 164
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeAllMessage()V

    .line 166
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mManual:Z

    .line 168
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    cmpg-float v0, v0, v2

    if-gtz v0, :cond_1

    .line 169
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    .line 176
    :goto_0
    return-void

    .line 171
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    .line 172
    iput v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    goto :goto_0

    .line 175
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->mCurrentScale:F

    sub-float v0, v1, v0

    const v1, 0x3f19999a    # 0.6f

    div-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlShrinkAnimation;->applyTransform(F)V

    goto :goto_0
.end method
