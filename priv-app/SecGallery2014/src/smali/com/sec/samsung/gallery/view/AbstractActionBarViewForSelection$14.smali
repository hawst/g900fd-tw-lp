.class Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;
.super Ljava/lang/Object;
.source "AbstractActionBarViewForSelection.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->onPause(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

.field final synthetic val$useAnim:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;Z)V
    .locals 0

    .prologue
    .line 632
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->val$useAnim:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 635
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mUsingSelectionModeBar:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-nez v0, :cond_1

    .line 636
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 647
    :goto_0
    return-void

    .line 642
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    if-nez v0, :cond_2

    .line 643
    # getter for: Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->access$600()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSelectionModeBar is null"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 646
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->this$0:Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/AbstractActionBarViewForSelection$14;->val$useAnim:Z

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setVisibility(IZ)V

    goto :goto_0
.end method
