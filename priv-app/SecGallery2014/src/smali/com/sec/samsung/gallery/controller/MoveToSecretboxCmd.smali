.class public Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "MoveToSecretboxCmd.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Ljava/util/Observer;
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

.field private static final MSG_EXIT_SELECTION_MODE:I = 0x0

.field private static final MSG_EXIT_SELECTION_MODE_AND_RESET:I = 0x1

.field private static final MSG_PRIVATE_MODE_ON:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static mAlbumPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static mItemCount:I

.field public static mPrivateTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;",
            "Lcom/sec/samsung/gallery/util/SecretboxOperations;",
            ">;"
        }
    .end annotation
.end field

.field private static mSelectedCount:I

.field private static mSingleOperation:Z


# instance fields
.field private mContext:Landroid/content/Context;

.field private mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

.field private mHandler:Landroid/os/Handler;

.field private mMediaSize:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

.field private mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

.field private mPrivateModeBinder:Landroid/os/IBinder;

.field private mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

.field private mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

.field private mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->TAG:Ljava/lang/String;

    .line 68
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;

    .line 71
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;

    .line 72
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 73
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 80
    new-instance v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->EXTERNAL_SD_DIRECTORY:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/io/File;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # Ljava/io/File;
    .param p2, "x2"    # I

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->showRenameDialog(Ljava/io/File;I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/util/HashMap;)Ljava/util/HashMap;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # Ljava/util/HashMap;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Lcom/sec/samsung/gallery/util/FileUtil;)Lcom/sec/samsung/gallery/util/FileUtil;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/util/FileUtil;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->dismissDialog()V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IIIIILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I
    .param p6, "x6"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct/range {p0 .. p6}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->showToast(IIIIILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1600()Z
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSingleOperation:Z

    return v0
.end method

.method static synthetic access$200()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    return v0
.end method

.method static synthetic access$300()I
    .locals 1

    .prologue
    .line 54
    sget v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    return v0
.end method

.method static synthetic access$400()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IIILjava/util/ArrayList;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I
    .param p4, "x4"    # Ljava/util/ArrayList;
    .param p5, "x5"    # Ljava/lang/String;

    .prologue
    .line 54
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/samsung/android/privatemode/PrivateModeManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/IBinder;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Landroid/os/IBinder;)Landroid/os/IBinder;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 54
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private dismissDialog()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->closeCustomProgressDialog()V

    .line 384
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 385
    return-void
.end method

.method private handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V
    .locals 10
    .param p1, "operationId"    # I
    .param p2, "count"    # I
    .param p3, "itemCount"    # I
    .param p5, "newName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p4, "albumPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v9, 0x1

    .line 228
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-nez v0, :cond_1

    .line 230
    if-nez p1, :cond_3

    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e01e1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 235
    .local v8, "title":Ljava/lang/String;
    :goto_0
    const/4 v7, 0x0

    .line 237
    .local v7, "message":Ljava/lang/String;
    if-le p3, v9, :cond_0

    .line 238
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e01aa

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 241
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDialogWithMessage:Z

    if-eqz v0, :cond_4

    .line 242
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v7}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 243
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 244
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, v8, v9, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 252
    .end local v7    # "message":Ljava/lang/String;
    .end local v8    # "title":Ljava/lang/String;
    :cond_1
    :goto_1
    new-instance v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;

    move-object v1, p0

    move v2, p3

    move v3, p2

    move-object v4, p4

    move v5, p1

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$5;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IILjava/util/ArrayList;ILjava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    if-eqz v0, :cond_2

    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    invoke-virtual {v0, v9}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->cancel(Z)Z

    .line 371
    :cond_2
    new-instance v0, Lcom/sec/samsung/gallery/util/SecretboxOperations;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/sec/samsung/gallery/util/SecretboxOperations;-><init>(ILcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    .line 372
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 374
    return-void

    .line 233
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    const v1, 0x7f0e01e2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .restart local v8    # "title":Ljava/lang/String;
    goto :goto_0

    .line 246
    .restart local v7    # "message":Ljava/lang/String;
    :cond_4
    new-instance v0, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->setTotalCount(I)V

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    invoke-virtual {v0, v8, v7, v9, p0}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->showProgressDialog(Ljava/lang/String;Ljava/lang/String;ZLandroid/content/DialogInterface$OnCancelListener;)V

    goto :goto_1
.end method

.method private handleSingleFileOperation(Lcom/sec/android/gallery3d/ui/SelectionManager;III)V
    .locals 17
    .param p1, "selectionModeProxy"    # Lcom/sec/android/gallery3d/ui/SelectionManager;
    .param p2, "operationId"    # I
    .param p3, "count"    # I
    .param p4, "itemCount"    # I

    .prologue
    .line 190
    const/4 v14, 0x0

    .line 191
    .local v14, "showConfirmDialog":Z
    invoke-virtual/range {p1 .. p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 192
    .local v13, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v13, :cond_0

    instance-of v3, v13, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v3, :cond_0

    move-object v12, v13

    .line 193
    check-cast v12, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 195
    .local v12, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    new-instance v11, Ljava/io/File;

    sget-object v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-direct {v11, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 196
    .local v11, "dstFolder":Ljava/io/File;
    new-instance v15, Ljava/io/File;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v15, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 197
    .local v15, "srcFile":Ljava/io/File;
    new-instance v10, Ljava/io/File;

    invoke-virtual {v15}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v10, v11, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 199
    .local v10, "dstFile":Ljava/io/File;
    invoke-virtual {v15}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v11, v3}, Ljava/io/File;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v10}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 200
    const/4 v14, 0x1

    .line 201
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0285

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v10}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 202
    .local v16, "title":Ljava/lang/String;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v4, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    if-nez p2, :cond_2

    const v3, 0x7f0e01e1

    :goto_0
    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e010e

    new-instance v5, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$4;

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v5, v0, v10, v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$4;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/io/File;I)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e0283

    new-instance v5, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$3;

    move-object/from16 v0, p0

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v5, v0, v1, v2, v10}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$3;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;IILjava/io/File;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e0046

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v9

    .line 218
    .local v9, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v9}, Landroid/app/AlertDialog;->show()V

    .line 222
    .end local v9    # "dialog":Landroid/app/AlertDialog;
    .end local v10    # "dstFile":Ljava/io/File;
    .end local v11    # "dstFolder":Ljava/io/File;
    .end local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v15    # "srcFile":Ljava/io/File;
    .end local v16    # "title":Ljava/lang/String;
    :cond_0
    if-nez v14, :cond_1

    .line 223
    sget-object v7, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    const/4 v8, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p2

    move/from16 v5, p3

    move/from16 v6, p4

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V

    .line 225
    :cond_1
    return-void

    .line 202
    .restart local v10    # "dstFile":Ljava/io/File;
    .restart local v11    # "dstFolder":Ljava/io/File;
    .restart local v12    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v15    # "srcFile":Ljava/io/File;
    .restart local v16    # "title":Ljava/lang/String;
    :cond_2
    const v3, 0x7f0e01e2

    goto :goto_0
.end method

.method private showRenameDialog(Ljava/io/File;I)V
    .locals 5
    .param p1, "dstFile"    # Ljava/io/File;
    .param p2, "operationId"    # I

    .prologue
    const/16 v4, 0x2e

    .line 465
    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    .line 466
    .local v1, "name":Ljava/lang/String;
    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 467
    .local v0, "extention":Ljava/lang/String;
    const/4 v3, 0x0

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    invoke-virtual {v1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 469
    new-instance v2, Lcom/sec/samsung/gallery/view/common/RenameDialog;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;-><init>(Landroid/content/Context;)V

    .line 470
    .local v2, "renameDialog":Lcom/sec/samsung/gallery/view/common/RenameDialog;
    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentName(Ljava/lang/String;)V

    .line 471
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->setCurrentFilePath(Ljava/lang/String;)V

    .line 472
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->showMediaRenameDialog()V

    .line 474
    new-instance v3, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;

    invoke-direct {v3, p0, v0, p2}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$6;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Ljava/lang/String;I)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/common/RenameDialog;->addObserver(Ljava/util/Observer;)V

    .line 482
    return-void
.end method

.method private showToast(IIIIILjava/lang/String;)V
    .locals 7
    .param p1, "operationId"    # I
    .param p2, "successCount"    # I
    .param p3, "successAlbumsCount"    # I
    .param p4, "count"    # I
    .param p5, "totalItemCount"    # I
    .param p6, "errorReason"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 388
    const/4 v1, 0x0

    .line 389
    .local v1, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 390
    .local v0, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz p6, :cond_2

    .line 391
    move-object v1, p6

    .line 435
    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    .line 436
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 437
    :cond_1
    return-void

    .line 392
    :cond_2
    if-nez p1, :cond_a

    .line 393
    if-ge p2, p5, :cond_6

    instance-of v2, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v2, :cond_6

    .line 394
    if-ne p2, v5, :cond_4

    .line 395
    sub-int v2, p5, p2

    if-ne v2, v5, :cond_3

    .line 396
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 398
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 400
    :cond_4
    if-le p2, v5, :cond_0

    .line 401
    sub-int v2, p5, p2

    if-ne v2, v5, :cond_5

    .line 402
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 404
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 407
    :cond_6
    instance-of v2, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v2, :cond_8

    .line 408
    if-ne p3, v5, :cond_7

    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 410
    :cond_7
    if-le p3, v5, :cond_0

    .line 411
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 414
    :cond_8
    if-ne p4, v5, :cond_9

    .line 415
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 416
    :cond_9
    if-le p4, v5, :cond_0

    .line 417
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 420
    :cond_a
    if-ne p1, v5, :cond_0

    .line 421
    instance-of v2, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v2, :cond_c

    .line 422
    if-ne p4, v5, :cond_b

    .line 423
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01ed

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->RESTORE_PATH:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 424
    :cond_b
    if-le p4, v5, :cond_0

    .line 425
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01ee

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    sget-object v4, Lcom/sec/android/gallery3d/util/MediaSetUtils;->RESTORE_PATH:Ljava/lang/String;

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 428
    :cond_c
    if-ne p4, v5, :cond_d

    .line 429
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01e9

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0

    .line 430
    :cond_d
    if-le p4, v5, :cond_0

    .line 431
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01ea

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 15
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 106
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    move-object v13, v0

    check-cast v13, [Ljava/lang/Object;

    .line 107
    .local v13, "params":[Ljava/lang/Object;
    const/4 v0, 0x0

    aget-object v0, v13, v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    .line 108
    const/4 v0, 0x1

    aget-object v0, v13, v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    .line 109
    .local v7, "hide":Z
    const/4 v0, 0x2

    aget-object v0, v13, v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v14

    .line 110
    .local v14, "show":Z
    const/4 v1, 0x0

    .line 111
    .local v1, "operationType":I
    if-eqz v7, :cond_1

    .line 112
    const/4 v1, 0x0

    .line 116
    :cond_0
    :goto_0
    new-instance v0, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "PRMO"

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 122
    new-instance v0, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v0

    if-nez v0, :cond_2

    sget v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    :goto_1
    sput v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    instance-of v0, v0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v0, :cond_4

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getList()Ljava/util/LinkedList;

    move-result-object v9

    .line 130
    .local v9, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v9, :cond_4

    .line 131
    invoke-virtual {v9}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 132
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    .line 133
    monitor-enter v9

    .line 134
    :try_start_0
    invoke-virtual {v9}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    .local v8, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 135
    .local v12, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    check-cast v12, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    .end local v12    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 137
    .end local v8    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 113
    .end local v9    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_1
    if-eqz v14, :cond_0

    .line 114
    const/4 v1, 0x1

    goto/16 :goto_0

    .line 125
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberItemOfMarkedAsSelected()I

    move-result v0

    goto :goto_1

    .line 137
    .restart local v8    # "i$":Ljava/util/Iterator;
    .restart local v9    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_3
    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    .end local v8    # "i$":Ljava/util/Iterator;
    .end local v9    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 143
    sget v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    .line 144
    new-instance v10, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    invoke-direct {v10, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 145
    .local v10, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v10, :cond_5

    .line 146
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 148
    .local v11, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v0, :cond_6

    .line 149
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSingleOperation:Z

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    sget v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    sget v3, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleSingleFileOperation(Lcom/sec/android/gallery3d/ui/SelectionManager;III)V

    .line 187
    .end local v10    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v11    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    :goto_3
    return-void

    .line 152
    .restart local v10    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .restart local v11    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_6
    sget v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    sget v3, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    sget-object v4, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_3

    .line 157
    .end local v10    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v11    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_7
    sget v2, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mSelectedCount:I

    sget v3, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mItemCount:I

    sget-object v4, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mAlbumPaths:Ljava/util/ArrayList;

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->handleMultipleFileOperation(IIILjava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_3

    .line 161
    :cond_8
    :try_start_2
    new-instance v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;-><init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    invoke-static {v0, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    :try_end_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    .line 183
    :catch_0
    move-exception v6

    .line 184
    .local v6, "e":Ljava/lang/NoClassDefFoundError;
    invoke-virtual {v6}, Ljava/lang/NoClassDefFoundError;->printStackTrace()V

    goto :goto_3
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 441
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->TAG:Ljava/lang/String;

    const-string v1, "onCancel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mFileUtils:Lcom/sec/samsung/gallery/util/FileUtil;

    .line 443
    iput-object v2, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mMediaSize:Ljava/util/HashMap;

    .line 444
    sget-object v0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateTasks:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mOperatePrivateAsyncTask:Lcom/sec/samsung/gallery/util/SecretboxOperations;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->cancel(Z)Z

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e04c1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 454
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->dismissDialog()V

    .line 456
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;

    if-eqz v0, :cond_2

    .line 457
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;

    invoke-virtual {v0, v1, v4}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 460
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 461
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageAtTime(IJ)Z

    .line 462
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 378
    return-void
.end method
