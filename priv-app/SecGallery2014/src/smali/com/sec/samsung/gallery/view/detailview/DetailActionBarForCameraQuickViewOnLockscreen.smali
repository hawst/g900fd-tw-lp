.class public Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;
.super Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;
.source "DetailActionBarForCameraQuickViewOnLockscreen.java"


# instance fields
.field private final mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 22
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 23
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 24
    return-void
.end method


# virtual methods
.method protected initDisplayOtpions()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 28
    invoke-virtual {p0, v0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->setDisplayOptions(ZZ)V

    .line 29
    return-void
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0298

    .line 33
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 34
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120015

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 35
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_0

    .line 36
    const/4 v1, 0x1

    invoke-static {p1, v2, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 37
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 42
    :goto_0
    return-void

    .line 39
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 46
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 60
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only delete supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :sswitch_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseUndoDeletion:Z

    if-eqz v0, :cond_1

    .line 49
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_ACTION_DELETE:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->notifyObservers(Ljava/lang/Object;)V

    .line 58
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->showDeleteDialog()V

    goto :goto_0

    .line 55
    :sswitch_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 46
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f026e -> :sswitch_0
        0x7f0f0298 -> :sswitch_1
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->onPause()V

    .line 67
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 70
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->onResume()V

    .line 75
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 78
    :cond_0
    return-void
.end method

.method public showDeleteDialog()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 81
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_LOCK_TOUCH:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->notifyObservers(Ljava/lang/Object;)V

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForCameraQuickViewOnLockscreen;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 84
    return-void
.end method
