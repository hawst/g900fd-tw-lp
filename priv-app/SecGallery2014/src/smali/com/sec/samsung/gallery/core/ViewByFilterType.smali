.class public final enum Lcom/sec/samsung/gallery/core/ViewByFilterType;
.super Ljava/lang/Enum;
.source "ViewByFilterType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

.field public static final enum TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;


# instance fields
.field private mIndex:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 26
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v7, v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v4, v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 36
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "CLOUD"

    invoke-direct {v0, v1, v5, v6}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 41
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "FACEBOOK"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 46
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "PICASA"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v6, v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 51
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "TCLOUD"

    const/4 v2, 0x5

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 56
    new-instance v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    const-string v1, "CLOUDALBUM_FORMAT"

    const/4 v2, 0x6

    const/16 v3, 0x40

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 22
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/sec/samsung/gallery/core/ViewByFilterType;

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v1, v0, v8

    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v1, v0, v6

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->$VALUES:[Lcom/sec/samsung/gallery/core/ViewByFilterType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 61
    iput p3, p0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->mIndex:I

    .line 62
    return-void
.end method

.method public static changeCloudAlbumFormat(Z)V
    .locals 1
    .param p0, "isMergeAlbum"    # Z

    .prologue
    .line 115
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v0, :cond_0

    .line 116
    sput-boolean p0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMergeAlbum:Z

    .line 118
    :cond_0
    return-void
.end method

.method public static getValidFormatOptions()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 81
    .local v0, "mValidFormatOptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/core/ViewByFilterType;>;"
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUDALBUM_FORMAT:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 82
    return-object v0
.end method

.method public static getValidOptions()Ljava/util/ArrayList;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 70
    .local v0, "mValidOptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/core/ViewByFilterType;>;"
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->LOCAL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 72
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->CLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 73
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->FACEBOOK:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->PICASA:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 75
    sget-object v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;->TCLOUD:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    return-object v0
.end method

.method public static getViewByDisplayOptions(Landroid/content/Context;Ljava/util/ArrayList;)[Ljava/lang/String;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/core/ViewByFilterType;",
            ">;)[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 91
    .local p1, "validOptions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/core/ViewByFilterType;>;"
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 92
    .local v1, "len":I
    const/4 v3, 0x0

    .line 93
    .local v3, "options":[Ljava/lang/String;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFlexibleCloudAlbumFormat:Z

    if-eqz v4, :cond_2

    .line 94
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v4, :cond_1

    .line 95
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090040

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    .line 104
    :goto_0
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsVZW:Z

    if-eqz v4, :cond_0

    .line 105
    const/4 v4, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0e0152

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 107
    :cond_0
    new-array v2, v1, [Ljava/lang/String;

    .line 108
    .local v2, "menus":[Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 109
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ordinal()I

    move-result v4

    aget-object v4, v3, v4

    aput-object v4, v2, v0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 97
    .end local v0    # "i":I
    .end local v2    # "menus":[Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09003f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 99
    :cond_2
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v4, :cond_3

    .line 100
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f090041

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f09003e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 111
    .restart local v0    # "i":I
    .restart local v2    # "menus":[Ljava/lang/String;
    :cond_4
    return-object v2
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/ViewByFilterType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 22
    const-class v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/core/ViewByFilterType;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->$VALUES:[Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/core/ViewByFilterType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/core/ViewByFilterType;

    return-object v0
.end method


# virtual methods
.method public getIndex()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->mIndex:I

    return v0
.end method

.method public isOptionSelected(I)Z
    .locals 1
    .param p1, "currentViewByType"    # I

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v0

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
