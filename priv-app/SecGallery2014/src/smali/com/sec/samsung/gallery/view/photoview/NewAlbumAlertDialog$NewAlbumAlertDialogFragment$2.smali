.class Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;
.super Ljava/lang/Object;
.source "NewAlbumAlertDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 11
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 173
    invoke-static {p1}, Lcom/sec/samsung/gallery/view/common/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 174
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v8, v8, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 175
    const-string v7, ""

    .line 231
    :goto_0
    return-object v7

    .line 178
    :cond_0
    sub-int v8, p3, p2

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->access$600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)I

    move-result v9

    mul-int/lit8 v9, v9, 0x2

    if-le v8, v9, :cond_1

    .line 179
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->access$600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)I

    move-result v8

    mul-int/lit8 v8, v8, 0x2

    add-int p3, p2, v8

    .line 180
    :cond_1
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 181
    .local v5, "origTxt":Ljava/lang/String;
    move-object v7, v5

    .line 182
    .local v7, "validTxt":Ljava/lang/String;
    const/4 v2, 0x0

    .line 183
    .local v2, "invalidFlag":Z
    const/4 v4, 0x0

    .line 185
    .local v4, "keep":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v8, Lcom/sec/samsung/gallery/view/common/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_4

    .line 186
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v6

    .line 187
    .local v6, "validLength":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-ge v3, v6, :cond_3

    .line 188
    sget-object v8, Lcom/sec/samsung/gallery/view/common/RenameDialog;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v7, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 189
    .local v1, "index":I
    if-ltz v1, :cond_2

    .line 190
    const/4 v2, 0x1

    .line 192
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_2

    .line 194
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v7, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v7, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 187
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 185
    .end local v1    # "index":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 222
    .end local v3    # "j":I
    .end local v6    # "validLength":I
    :cond_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->MAX_FOLDER_NAME_LENGTH:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->access$600(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;)I

    move-result v8

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v9

    sub-int v10, p6, p5

    sub-int/2addr v9, v10

    sub-int v4, v8, v9

    .line 223
    if-gtz v4, :cond_5

    .line 227
    :cond_5
    if-eqz v2, :cond_6

    .line 228
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;

    iget-object v8, v8, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog$NewAlbumAlertDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;->access$300(Lcom/sec/samsung/gallery/view/photoview/NewAlbumAlertDialog;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 231
    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0
.end method
