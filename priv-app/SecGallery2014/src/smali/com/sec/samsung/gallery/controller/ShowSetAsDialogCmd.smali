.class public Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;
.super Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;
.source "ShowSetAsDialogCmd.java"


# instance fields
.field private STAS:Ljava/lang/String;

.field private mRcs:Z

.field mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;-><init>()V

    .line 32
    const-string v0, "STAS"

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->STAS:Ljava/lang/String;

    .line 36
    const v0, 0x7f0e0064

    iput v0, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mTitleOfDialog:I

    .line 37
    return-void
.end method

.method private createIntentForSetAs()Landroid/content/Intent;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 153
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 154
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    .line 155
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v3, 0x0

    .line 156
    .local v3, "secOnly":Z
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 157
    invoke-interface {v2, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 158
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v6

    const-wide/16 v8, 0x20

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-eqz v5, :cond_0

    .line 160
    const/4 v3, 0x1

    .line 163
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    const/4 v0, 0x0

    .line 164
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v3, :cond_1

    .line 165
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.ATTACH_DATA_SEC_LIMIT"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 169
    .restart local v0    # "intent":Landroid/content/Intent;
    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mUriList:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 170
    .local v4, "uri":Landroid/net/Uri;
    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const-string v5, "mimeType"

    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mMimeType:Ljava/lang/String;

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    const-string v5, "rcs"

    iget-boolean v6, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mRcs:Z

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 173
    return-object v0

    .line 167
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.ATTACH_DATA"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "intent":Landroid/content/Intent;
    goto :goto_0
.end method

.method private startSetAsApp(Landroid/content/pm/ResolveInfo;)V
    .locals 4
    .param p1, "info"    # Landroid/content/pm/ResolveInfo;

    .prologue
    .line 143
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->STAS:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 145
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->createIntentForSetAs()Landroid/content/Intent;

    move-result-object v0

    .line 147
    .local v0, "intent":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v3, p1, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v3, v3, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 148
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 149
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void
.end method


# virtual methods
.method protected createAppList()Ljava/util/List;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/ResolveInfo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->createIntentForSetAs()Landroid/content/Intent;

    move-result-object v8

    .line 43
    .local v8, "intent":Landroid/content/Intent;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v13

    .line 44
    .local v13, "packageManager":Landroid/content/pm/PackageManager;
    const/high16 v16, 0x10000

    move/from16 v0, v16

    invoke-virtual {v13, v8, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v15

    .line 47
    .local v15, "setAsAppList":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/ResolveInfo;>;"
    const/4 v10, 0x0

    .line 48
    .local v10, "mKnox":Z
    const/16 v16, 0x0

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mRcs:Z

    .line 50
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 51
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    check-cast v16, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 52
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mViewStateProxy:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v5

    .line 53
    .local v5, "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v11

    .line 55
    .local v11, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mDisableMenuSeaAsContact:Z

    if-eqz v16, :cond_3

    instance-of v0, v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move/from16 v16, v0

    if-eqz v16, :cond_3

    const/4 v3, 0x1

    .line 58
    .local v3, "blockContact":Z
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v16

    const-string v17, "sec_container_1.com.sec.android.gallery3d"

    invoke-virtual/range {v16 .. v17}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_0

    .line 59
    const/4 v10, 0x1

    .line 61
    :cond_0
    if-eqz v5, :cond_1

    instance-of v0, v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move/from16 v16, v0

    if-eqz v16, :cond_1

    .line 62
    check-cast v5, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .end local v5    # "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isRcsUri()Z

    move-result v16

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mRcs:Z

    .line 65
    :cond_1
    if-eqz v10, :cond_4

    .line 66
    const/4 v6, 0x0

    .local v6, "i":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v12

    .local v12, "n":I
    move v7, v6

    .end local v6    # "i":I
    .local v7, "i":I
    :goto_1
    if-ge v7, v12, :cond_4

    .line 67
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 68
    .local v14, "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 71
    .local v2, "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 73
    .local v4, "className":Ljava/lang/String;
    const-string v16, "com.sec.android.gallery3d.app.LockScreen"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_2

    const-string v16, "com.sec.android.gallery3d.app.BothScreen"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_c

    .line 75
    :cond_2
    add-int/lit8 v6, v7, -0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    invoke-interface {v15, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 76
    add-int/lit8 v12, v12, -0x1

    .line 66
    :goto_2
    add-int/lit8 v6, v6, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_1

    .line 55
    .end local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v3    # "blockContact":Z
    .end local v4    # "className":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v12    # "n":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    .restart local v5    # "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_3
    const/4 v3, 0x0

    goto :goto_0

    .line 82
    .end local v5    # "currentState":Lcom/sec/android/gallery3d/app/ActivityState;
    .restart local v3    # "blockContact":Z
    :cond_4
    invoke-interface {v11}, Ljava/util/List;->isEmpty()Z

    move-result v16

    if-nez v16, :cond_5

    .line 83
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-interface {v11, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 84
    .local v9, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v9, :cond_5

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v16

    const-wide/16 v18, 0x20

    and-long v16, v16, v18

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-eqz v16, :cond_5

    .line 86
    const/4 v3, 0x1

    .line 90
    .end local v9    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_5
    if-eqz v3, :cond_6

    .line 91
    const/4 v6, 0x0

    .restart local v6    # "i":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v12

    .restart local v12    # "n":I
    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :goto_3
    if-ge v7, v12, :cond_6

    .line 92
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 93
    .restart local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 94
    .restart local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 96
    .restart local v4    # "className":Ljava/lang/String;
    if-eqz v4, :cond_b

    const-string v16, "com.android.contacts.activities.AttachPhotoActivity"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_b

    .line 97
    add-int/lit8 v6, v7, -0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    invoke-interface {v15, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 98
    add-int/lit8 v12, v12, -0x1

    .line 91
    :goto_4
    add-int/lit8 v6, v6, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_3

    .line 103
    .end local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "className":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v12    # "n":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->mRcs:Z

    move/from16 v16, v0

    if-eqz v16, :cond_7

    .line 104
    const/4 v6, 0x0

    .restart local v6    # "i":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v12

    .restart local v12    # "n":I
    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :goto_5
    if-ge v7, v12, :cond_7

    .line 105
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 106
    .restart local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 107
    .restart local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 109
    .restart local v4    # "className":Ljava/lang/String;
    if-eqz v4, :cond_a

    const-string v16, "com.google.android.apps.photos.phone.SetWallpaperActivity"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_a

    .line 110
    add-int/lit8 v6, v7, -0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    invoke-interface {v15, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 111
    add-int/lit8 v12, v12, -0x1

    .line 104
    :goto_6
    add-int/lit8 v6, v6, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_5

    .line 116
    .end local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "className":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v12    # "n":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_7
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsChn:Z

    if-eqz v16, :cond_8

    .line 117
    const/4 v6, 0x0

    .restart local v6    # "i":I
    invoke-interface {v15}, Ljava/util/List;->size()I

    move-result v12

    .restart local v12    # "n":I
    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :goto_7
    if-ge v7, v12, :cond_8

    .line 118
    invoke-interface {v15, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Landroid/content/pm/ResolveInfo;

    .line 119
    .restart local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    iget-object v2, v14, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    .line 120
    .restart local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    iget-object v4, v2, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    .line 122
    .restart local v4    # "className":Ljava/lang/String;
    if-eqz v4, :cond_9

    const-string v16, "com.sec.android.mimage.expressme.ExpressMe"

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_9

    .line 123
    add-int/lit8 v6, v7, -0x1

    .end local v7    # "i":I
    .restart local v6    # "i":I
    invoke-interface {v15, v7}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 124
    add-int/lit8 v12, v12, -0x1

    .line 117
    :goto_8
    add-int/lit8 v6, v6, 0x1

    move v7, v6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    goto :goto_7

    .line 129
    .end local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .end local v4    # "className":Ljava/lang/String;
    .end local v7    # "i":I
    .end local v12    # "n":I
    .end local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_8
    return-object v15

    .restart local v2    # "activityInfo":Landroid/content/pm/ActivityInfo;
    .restart local v4    # "className":Ljava/lang/String;
    .restart local v7    # "i":I
    .restart local v12    # "n":I
    .restart local v14    # "resolveInfo":Landroid/content/pm/ResolveInfo;
    :cond_9
    move v6, v7

    .end local v7    # "i":I
    .restart local v6    # "i":I
    goto :goto_8

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :cond_a
    move v6, v7

    .end local v7    # "i":I
    .restart local v6    # "i":I
    goto :goto_6

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :cond_b
    move v6, v7

    .end local v7    # "i":I
    .restart local v6    # "i":I
    goto :goto_4

    .end local v6    # "i":I
    .restart local v7    # "i":I
    :cond_c
    move v6, v7

    .end local v7    # "i":I
    .restart local v6    # "i":I
    goto/16 :goto_2
.end method

.method public bridge synthetic execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 0
    .param p1, "x0"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowAppChoiceDialogCmdTemplate;->execute(Lorg/puremvc/java/interfaces/INotification;)V

    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 4
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 134
    move-object v0, p2

    check-cast v0, Lcom/sec/samsung/gallery/core/Event;

    .line 136
    .local v0, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v2

    sget v3, Lcom/sec/samsung/gallery/core/Event;->EVENT_START_SELECTED_APP:I

    if-ne v2, v3, :cond_0

    .line 137
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/Event;->getData()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/ResolveInfo;

    .line 138
    .local v1, "info":Landroid/content/pm/ResolveInfo;
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/ShowSetAsDialogCmd;->startSetAsApp(Landroid/content/pm/ResolveInfo;)V

    .line 140
    .end local v1    # "info":Landroid/content/pm/ResolveInfo;
    :cond_0
    return-void
.end method
