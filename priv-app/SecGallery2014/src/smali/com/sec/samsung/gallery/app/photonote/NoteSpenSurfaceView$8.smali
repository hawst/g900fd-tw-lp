.class Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;
.super Ljava/lang/Object;
.source "NoteSpenSurfaceView.java"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$EventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)V
    .locals 0

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClearAll()V
    .locals 3

    .prologue
    const v2, 0x7f0f013f

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->update()V

    .line 617
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mRemoverSettingLayout:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v0

    iput v2, v0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mPrevButtonId:I

    .line 619
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->mActionBar:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->access$200(Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;)Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->setCheckedButton(I)V

    .line 620
    iget-object v0, p0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$8;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->setPenMode()V

    .line 621
    return-void
.end method
