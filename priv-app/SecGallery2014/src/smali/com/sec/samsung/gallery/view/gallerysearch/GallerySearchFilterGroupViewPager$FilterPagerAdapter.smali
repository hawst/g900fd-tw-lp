.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;
.super Landroid/support/v4/view/PagerAdapter;
.source "GallerySearchFilterGroupViewPager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FilterPagerAdapter"
.end annotation


# instance fields
.field private mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

.field private mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

.field private mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

.field private mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

.field private mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 440
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    .line 441
    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    .line 430
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 432
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 434
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 436
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 438
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 442
    return-void
.end method


# virtual methods
.method public destroyItem(Landroid/view/View;ILjava/lang/Object;)V
    .locals 0
    .param p1, "pager"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "view"    # Ljava/lang/Object;

    .prologue
    .line 529
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "pager":Landroid/view/View;
    check-cast p3, Landroid/view/View;

    .end local p3    # "view":Ljava/lang/Object;
    invoke-virtual {p1, p3}, Landroid/support/v4/view/ViewPager;->removeView(Landroid/view/View;)V

    .line 530
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 451
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/view/PagerAdapter;->destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 452
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x5

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 5
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    const/4 v4, -0x1

    .line 456
    const/4 v1, 0x0

    .line 457
    .local v1, "view":Landroid/view/View;
    packed-switch p2, :pswitch_data_0

    .line 515
    :goto_0
    if-eqz p1, :cond_5

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-nez v2, :cond_5

    .line 516
    new-instance v0, Landroid/widget/ScrollView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/widget/ScrollView;-><init>(Landroid/content/Context;)V

    .line 517
    .local v0, "sv":Landroid/widget/ScrollView;
    invoke-virtual {v0, v1}, Landroid/widget/ScrollView;->addView(Landroid/view/View;)V

    .line 518
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2}, Landroid/widget/ScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 520
    check-cast p1, Landroid/support/v4/view/ViewPager;

    .end local p1    # "container":Landroid/view/ViewGroup;
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/support/v4/view/ViewPager;->addView(Landroid/view/View;I)V

    .line 523
    .end local v0    # "sv":Landroid/widget/ScrollView;
    :goto_1
    return-object v0

    .line 461
    .restart local p1    # "container":Landroid/view/ViewGroup;
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    if-nez v2, :cond_0

    .line 462
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 463
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V

    .line 464
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mTimeTagList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 467
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mTimePage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 468
    goto :goto_0

    .line 472
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    if-nez v2, :cond_1

    .line 473
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 474
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V

    .line 475
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mEventTagList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 478
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mEventTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 479
    goto :goto_0

    .line 483
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    if-nez v2, :cond_2

    .line 484
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 485
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V

    .line 486
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mLocationTagList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 489
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mLocationPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 490
    goto/16 :goto_0

    .line 494
    :pswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    if-nez v2, :cond_3

    .line 495
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 496
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V

    .line 497
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mPersonTagList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 500
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mPersonTagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 501
    goto/16 :goto_0

    .line 505
    :pswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    if-nez v2, :cond_4

    .line 506
    new-instance v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;-><init>(Landroid/content/Context;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    .line 507
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mFilterClickListener:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setFilterClickListener(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$OnFilterClickListener;)V

    .line 508
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager;->mUserTagTagList:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->setData(Ljava/util/ArrayList;)V

    .line 511
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupViewPager$FilterPagerAdapter;->mUsertagPage:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 523
    goto/16 :goto_1

    .line 457
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1
    .param p1, "pager"    # Landroid/view/View;
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 534
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
