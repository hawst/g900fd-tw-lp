.class Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;
.super Ljava/lang/Object;
.source "OutOfFocusShotHelpView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->addView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mIsShowingHelpView:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$002(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Z)Z

    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 66
    :goto_0
    return-void

    .line 58
    :cond_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v0, :cond_1

    .line 59
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030064

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 62
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->initializeView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$400(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    .line 64
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->setHelpState(Z)V
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$500(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Z)V

    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->bringToFront()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$600(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)V

    goto :goto_0

    .line 61
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030063

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mMainView:Landroid/view/ViewGroup;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;)Landroid/view/ViewGroup;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->mOutOfFocusShotHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    goto :goto_1
.end method
