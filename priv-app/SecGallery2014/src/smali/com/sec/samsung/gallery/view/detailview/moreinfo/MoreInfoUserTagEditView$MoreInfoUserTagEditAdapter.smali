.class public final Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;
.super Landroid/widget/BaseAdapter;
.source "MoreInfoUserTagEditView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "MoreInfoUserTagEditAdapter"
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MoreInfoTagAdapter"


# instance fields
.field private mCheckedData:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mContext:Landroid/content/Context;

.field private mInterimAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private mUserTagCountMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p3, "userTagList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 198
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    .line 202
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 204
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    .line 205
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-direct {p0, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getAllUserTags(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 207
    return-void
.end method

.method private getAllUserTags(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "userTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 213
    .local v3, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 214
    .local v4, "newInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 216
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    if-nez v5, :cond_0

    .line 217
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUri:Ljava/lang/String;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTagCountMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    .line 220
    :cond_0
    const/4 v2, 0x0

    .line 221
    .local v2, "info":Ljava/lang/String;
    const/4 v0, 0x0

    .line 222
    .local v0, "bContain":Z
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "i":I
    :goto_0
    if-lez v1, :cond_3

    .line 223
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "info":Ljava/lang/String;
    check-cast v2, Ljava/lang/String;

    .line 224
    .restart local v2    # "info":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    .line 225
    if-eqz v0, :cond_1

    if-eqz v0, :cond_2

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_2

    .line 226
    :cond_1
    add-int/lit8 v5, v1, -0x1

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 222
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 231
    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_4

    .line 232
    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    .line 234
    :cond_4
    return-object v4
.end method


# virtual methods
.method public addTag(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 325
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 326
    .local v0, "tag":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 330
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 239
    const/4 v2, 0x0

    .line 240
    .local v2, "size":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 241
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/2addr v2, v3

    .line 242
    goto :goto_0

    .line 243
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_0
    return v2
.end method

.method public getData()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 333
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 248
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 249
    .local v0, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 250
    .local v2, "size":I
    if-ge p1, v2, :cond_0

    .line 251
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    .line 255
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :goto_1
    return-object v3

    .line 253
    .restart local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v2    # "size":I
    :cond_0
    sub-int/2addr p1, v2

    .line 254
    goto :goto_0

    .line 255
    .end local v0    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "size":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 260
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 265
    if-nez p2, :cond_0

    .line 266
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mInflater:Landroid/view/LayoutInflater;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f03009d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 269
    :cond_0
    const v3, 0x7f0f01aa

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 271
    .local v2, "textView":Landroid/widget/TextView;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 272
    .local v1, "tag":Ljava/lang/String;
    if-nez v1, :cond_1

    const-string v3, ""

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 273
    const v3, -0xa0a0b

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 275
    const v3, 0x7f0f01ab

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 277
    .local v0, "checkBox":Landroid/widget/CheckBox;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 279
    return-object p2

    .end local v0    # "checkBox":Landroid/widget/CheckBox;
    :cond_1
    move-object v3, v1

    .line 272
    goto :goto_0
.end method

.method public removeTag(I)V
    .locals 11
    .param p1, "position"    # I

    .prologue
    .line 283
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    monitor-enter v7

    .line 284
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mInterimAdapters:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 285
    .local v1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 286
    .local v4, "size":I
    if-ge p1, v4, :cond_9

    .line 287
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 288
    .local v5, "tag":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    if-nez v6, :cond_0

    .line 289
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mUri:Ljava/lang/String;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getUserTagCountMap(Landroid/content/Context;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    iput-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    .line 292
    :cond_0
    const/4 v3, 0x0

    .line 293
    .local v3, "showToast":Z
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_8

    .line 294
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mUserTagCountMap:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 295
    .local v0, "c":Ljava/lang/Integer;
    if-eqz v0, :cond_3

    .line 296
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/4 v8, 0x1

    if-ne v6, v8, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mOriginalUserTag:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    if-nez v6, :cond_3

    .line 297
    :cond_2
    const/4 v3, 0x1

    .line 298
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->notifyDataSetChanged()V

    .line 305
    .end local v0    # "c":Ljava/lang/Integer;
    :cond_3
    :goto_1
    if-eqz v3, :cond_5

    .line 306
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mDeleteToast:Landroid/widget/Toast;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/widget/Toast;

    move-result-object v6

    if-nez v6, :cond_4

    .line 307
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mContext:Landroid/content/Context;

    const v10, 0x7f0e0470

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v8, v9, v10}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mDeleteToast:Landroid/widget/Toast;
    invoke-static {v6, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$702(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 310
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mDeleteToast:Landroid/widget/Toast;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 313
    :cond_5
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 314
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->mCheckedData:Ljava/util/ArrayList;

    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 316
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$MoreInfoUserTagEditAdapter;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->mHandler:Landroid/os/Handler;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Landroid/os/Handler;

    move-result-object v6

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 321
    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v3    # "showToast":Z
    .end local v4    # "size":I
    .end local v5    # "tag":Ljava/lang/String;
    :cond_7
    monitor-exit v7

    .line 322
    return-void

    .line 302
    .restart local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .restart local v3    # "showToast":Z
    .restart local v4    # "size":I
    .restart local v5    # "tag":Ljava/lang/String;
    :cond_8
    const/4 v3, 0x1

    goto :goto_1

    .line 319
    .end local v3    # "showToast":Z
    .end local v5    # "tag":Ljava/lang/String;
    :cond_9
    sub-int/2addr p1, v4

    .line 320
    goto/16 :goto_0

    .line 321
    .end local v1    # "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v4    # "size":I
    :catchall_0
    move-exception v6

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method
