.class public Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumViewVI"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;
    }
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;


# instance fields
.field public mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 450
    .local v0, "r":Landroid/content/res/Resources;
    new-instance v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    .line 451
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    const v2, 0x7f0c0076

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    iput v2, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree_count:I

    .line 452
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget v2, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree_count:I

    new-array v2, v2, [I

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree:[I

    .line 453
    const v1, 0x7f090086

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree:[I

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->mAlbumViewVISpec:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;

    iget v3, v3, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI$Spec;->album_view_end_affordance_row_degree_count:I

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy([II[III)V

    .line 456
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 442
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    if-nez v0, :cond_0

    .line 443
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;

    .line 445
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 442
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
