.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2799
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemLongClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z
    .locals 6
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "albumIndex"    # I
    .param p4, "itemIndex"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2802
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "PHLP"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 2803
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 2804
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_NORMAL:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v3, v4, :cond_1

    .line 2834
    :cond_0
    :goto_0
    return v1

    .line 2807
    :cond_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2808
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v3, v1, v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->enterSelectionMode(ZZ)V

    .line 2809
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItem(I)I

    goto :goto_0

    .line 2813
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsGifMode:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2816
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->IsCategoryView(Lcom/sec/samsung/gallery/core/TabTagType;Ljava/lang/Boolean;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2819
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-nez v3, :cond_8

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v3, :cond_5

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v3, :cond_5

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_6

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isCallWindow(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isConnetedSideSync(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 2821
    :cond_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isSelected(I)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2822
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v2, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItem(I)I

    .line 2824
    :cond_7
    new-instance v0, Lcom/sec/samsung/gallery/view/common/DragAndDrop;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;-><init>(Landroid/content/Context;)V

    .line 2825
    .local v0, "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v5

    move v2, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/common/DragAndDrop;->startPhotosDrag(IILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto/16 :goto_0

    .line 2828
    .end local v0    # "drag":Lcom/sec/samsung/gallery/view/common/DragAndDrop;
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumMode:Z

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v3, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isSelectedForNewAlbum(I)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2831
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v3, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->isSelected(I)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2832
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$20;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v3, p4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->selectItem(I)I

    move-result v3

    if-ne v3, v2, :cond_0

    move v1, v2

    goto/16 :goto_0

    :cond_a
    move v1, v2

    .line 2834
    goto/16 :goto_0
.end method
