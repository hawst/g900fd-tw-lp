.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;
.super Landroid/widget/BaseAdapter;
.source "GallerySearchSelectedItemListAdapter.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mContext:Landroid/content/Context;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    .line 25
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mContext:Landroid/content/Context;

    .line 26
    return-void
.end method

.method private getTextColorByType(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/content/res/ColorStateList;
    .locals 3
    .param p1, "data"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    .line 85
    const/4 v0, 0x0

    .line 87
    .local v0, "csl":Landroid/content/res/ColorStateList;
    const v1, 0x7f0200cf

    .line 89
    .local v1, "selector":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isTimeTag()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 90
    const v1, 0x7f0200d1

    .line 101
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    .line 103
    return-object v0

    .line 91
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isUserTag()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 92
    const v1, 0x7f0200d3

    goto :goto_0

    .line 93
    :cond_2
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEventTag()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 94
    const v1, 0x7f0200cb

    goto :goto_0

    .line 95
    :cond_3
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isPersonTag()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 96
    const v1, 0x7f0200cf

    goto :goto_0

    .line 97
    :cond_4
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isLocationTag()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 98
    const v1, 0x7f0200cd

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 53
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 60
    .local v0, "data":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    if-nez p2, :cond_1

    .line 61
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030061

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 64
    const v2, 0x7f0f002f

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 66
    .local v1, "textview":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 67
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTransTagText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 68
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->getTextColorByType(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)Landroid/content/res/ColorStateList;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 69
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v2

    if-eqz v2, :cond_2

    .line 70
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 78
    :cond_0
    :goto_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 81
    .end local v1    # "textview":Landroid/widget/TextView;
    :cond_1
    return-object p2

    .line 71
    .restart local v1    # "textview":Landroid/widget/TextView;
    :cond_2
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 72
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 74
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTransTagText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public resetData()V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 39
    :cond_0
    return-void
.end method

.method public setData(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 30
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchSelectedItemListAdapter;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 33
    :cond_0
    return-void
.end method
