.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
.super Landroid/os/AsyncTask;
.source "GallerySearchViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateSelectionModeTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0

    .prologue
    .line 1845
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$1;

    .prologue
    .line 1845
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    return-void
.end method

.method private selectAllPostProcess()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1886
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    .line 1887
    .local v1, "selectedCount":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getCount()I

    move-result v3

    if-ne v1, v3, :cond_0

    const/4 v0, 0x1

    .line 1888
    .local v0, "isSelectAll":Z
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$4700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v3

    sget-object v4, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v3, v4, v2, v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 1889
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$4800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 1890
    return-void

    .end local v0    # "isSelectAll":Z
    :cond_0
    move v0, v2

    .line 1887
    goto :goto_0
.end method

.method private updateSelectionMode()Ljava/lang/Boolean;
    .locals 10

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 1852
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    iget-object v9, v9, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v5

    .line 1854
    .local v5, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v5}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 1856
    .local v6, "object":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v9, v6, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v9, :cond_0

    move-object v4, v6

    .line 1857
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1858
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v3, v4, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .line 1859
    .local v3, "isLocalItem":Z
    instance-of v9, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v9, :cond_1

    instance-of v9, v4, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v9, :cond_4

    :cond_1
    move v2, v8

    .line 1861
    .local v2, "isCloudItem":Z
    :goto_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_0

    .line 1862
    new-instance v0, Ljava/io/File;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v0, v9}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1863
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v9

    if-nez v9, :cond_2

    if-eqz v2, :cond_3

    :cond_2
    if-eqz v3, :cond_0

    check-cast v4, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isDeleted()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1864
    :cond_3
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->exitSelectionMode()V

    .line 1865
    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    .line 1870
    .end local v0    # "file":Ljava/io/File;
    .end local v2    # "isCloudItem":Z
    .end local v3    # "isLocalItem":Z
    .end local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_1
    return-object v7

    .restart local v3    # "isLocalItem":Z
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    move v2, v7

    .line 1859
    goto :goto_0

    .line 1870
    .end local v3    # "isLocalItem":Z
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "object":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_5
    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    goto :goto_1
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 1
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    .line 1849
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->updateSelectionMode()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1845
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 1874
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 1876
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1877
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->selectAllPostProcess()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1884
    :cond_0
    :goto_0
    return-void

    .line 1879
    :catch_0
    move-exception v0

    .line 1880
    .local v0, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 1881
    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$4600()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onPostExecute : NullPointerException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1845
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method
