.class Lcom/sec/samsung/gallery/glview/GlPhotoView$10;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GContentObserver;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1252
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 1255
    const-string v0, "GlPhotoView"

    const-string v1, "CMD_NOTIFY_FOLDER_UPDATE"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1256
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->removeCommand(I)V

    .line 1257
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_NOTIFY_FOLDER_UPDATE:I

    const-wide/16 v6, 0xc8

    move v4, v3

    move v5, v3

    invoke-virtual/range {v1 .. v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommandDelayed(IIIIJ)V

    .line 1258
    return-void
.end method

.method public onChanged(II)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v3, 0x0

    .line 1262
    const-string v0, "GlPhotoView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "CMD_UPDATE_FOLDER index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1263
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    .line 1264
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_UPDATE_FOLDER:I

    invoke-virtual {v0, v1, p1, p2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 1269
    :cond_1
    :goto_0
    return-void

    .line 1265
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleFirst:I

    if-gt v0, p1, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mVisibleLast:I

    if-le p1, v0, :cond_4

    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v0, :cond_1

    .line 1267
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$10;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->CMD_UPDATE_FOLDER:I

    invoke-virtual {v0, v1, p1, v3, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    goto :goto_0
.end method
