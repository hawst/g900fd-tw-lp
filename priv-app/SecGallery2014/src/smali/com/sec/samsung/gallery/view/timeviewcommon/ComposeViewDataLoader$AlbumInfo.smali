.class public Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
.super Ljava/lang/Object;
.source "ComposeViewDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumInfo"
.end annotation


# instance fields
.field public mCount:I

.field public mItemCount:I

.field public mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

.field public mItemVersion:[J

.field public mLineCount:I

.field public mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

.field public mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field public mSetVersion:[J

.field public mThmSizeType:B

.field public mUpdated:[B

.field public mVersion:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1273
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1274
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mVersion:J

    .line 1275
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    .line 1276
    iput v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mLineCount:I

    .line 1277
    iput-byte v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mThmSizeType:B

    .line 1278
    return-void
.end method


# virtual methods
.method public setMediaItemCount(IZ)V
    .locals 12
    .param p1, "count"    # I
    .param p2, "force"    # Z

    .prologue
    const-wide/16 v10, -0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 1281
    if-gez p1, :cond_0

    .line 1282
    # getter for: Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->access$400()Ljava/lang/String;

    move-result-object v6

    const-string v7, "count less than 0"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1283
    const/4 p1, 0x0

    .line 1285
    :cond_0
    iget v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge v6, p1, :cond_4

    .line 1286
    new-array v4, p1, [J

    .line 1287
    .local v4, "setVersion":[J
    new-array v2, p1, [J

    .line 1288
    .local v2, "itemVersion":[J
    new-array v5, p1, [B

    .line 1289
    .local v5, "updated":[B
    new-array v3, p1, [Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1290
    .local v3, "mediaItem":[Lcom/sec/android/gallery3d/data/MediaItem;
    new-array v1, p1, [Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 1291
    .local v1, "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    .line 1293
    .local v0, "copyCount":I
    if-eqz p2, :cond_2

    .line 1294
    invoke-static {v5, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 1295
    invoke-static {v4, v10, v11}, Ljava/util/Arrays;->fill([JJ)V

    .line 1307
    :goto_0
    iput-object v4, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    .line 1308
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    .line 1309
    iput-object v5, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    .line 1310
    iput-object v3, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1311
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 1318
    .end local v0    # "copyCount":I
    .end local v1    # "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v2    # "itemVersion":[J
    .end local v3    # "mediaItem":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "setVersion":[J
    .end local v5    # "updated":[B
    :cond_1
    :goto_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    .line 1319
    return-void

    .line 1297
    .restart local v0    # "copyCount":I
    .restart local v1    # "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .restart local v2    # "itemVersion":[J
    .restart local v3    # "mediaItem":[Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v4    # "setVersion":[J
    .restart local v5    # "updated":[B
    :cond_2
    if-lez v0, :cond_3

    .line 1298
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    invoke-static {v6, v8, v4, v8, v0}, Ljava/lang/System;->arraycopy([JI[JII)V

    .line 1299
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemVersion:[J

    invoke-static {v6, v8, v2, v8, v0}, Ljava/lang/System;->arraycopy([JI[JII)V

    .line 1300
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    invoke-static {v6, v8, v5, v8, v0}, Ljava/lang/System;->arraycopy([BI[BII)V

    .line 1301
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-static {v6, v8, v3, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1302
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-static {v6, v8, v1, v8, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1304
    :cond_3
    invoke-static {v5, v0, p1, v9}, Ljava/util/Arrays;->fill([BIIB)V

    .line 1305
    invoke-static {v4, v0, p1, v10, v11}, Ljava/util/Arrays;->fill([JIIJ)V

    goto :goto_0

    .line 1312
    .end local v0    # "copyCount":I
    .end local v1    # "itemImage":[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v2    # "itemVersion":[J
    .end local v3    # "mediaItem":[Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "setVersion":[J
    .end local v5    # "updated":[B
    :cond_4
    if-eqz p2, :cond_1

    .line 1313
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    if-eqz v6, :cond_1

    .line 1314
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    invoke-static {v6, v9}, Ljava/util/Arrays;->fill([BB)V

    .line 1315
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mSetVersion:[J

    invoke-static {v6, v10, v11}, Ljava/util/Arrays;->fill([JJ)V

    goto :goto_1
.end method
