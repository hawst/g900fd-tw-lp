.class public Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;
.source "EditMediaItemAdapter.java"


# static fields
.field public static final ATTR_DRAW_CNT:I = 0x100


# instance fields
.field private mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

.field private mIsDragging:Z

.field private mNewAlbumMode:Z

.field private mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mThumbSize:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IJ)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "setPath"    # Ljava/lang/String;
    .param p3, "index"    # I
    .param p4, "attr"    # J

    .prologue
    .line 36
    invoke-direct/range {p0 .. p5}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;IJ)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mIsDragging:Z

    .line 33
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mThumbSize:I

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    .line 38
    return-void
.end method

.method private drawAddButton(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/16 v6, 0x3c

    const/16 v5, 0x15

    const/16 v4, 0xc

    const/16 v3, 0xa

    const/4 v2, 0x1

    .line 132
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 133
    .local v0, "addButton":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "addButton":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 135
    .restart local v0    # "addButton":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 138
    :cond_0
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 139
    invoke-virtual {v0, v4, v3, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 140
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 141
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 142
    const v1, 0x7f020477

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 143
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 144
    return-void
.end method

.method private drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 15
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 147
    const/4 v11, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 148
    .local v1, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v11

    if-eqz v11, :cond_b

    .line 149
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v8

    .line 150
    .local v8, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v11, v8, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v11, :cond_0

    instance-of v11, v8, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-nez v11, :cond_0

    instance-of v11, v8, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v11, :cond_0

    instance-of v11, v8, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v11, :cond_4

    :cond_0
    const/4 v7, 0x1

    .line 152
    .local v7, "isHide":Z
    :goto_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isHideItemInSelectionMode()Z

    move-result v11

    if-eqz v11, :cond_5

    if-eqz v8, :cond_5

    instance-of v11, v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v11, :cond_1

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    .end local v8    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->isHideBlockedItem()Z

    move-result v11

    if-nez v11, :cond_2

    :cond_1
    if-eqz v7, :cond_5

    .line 155
    :cond_2
    const/4 v11, 0x1

    iput v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mViewResult:I

    .line 156
    if-eqz v1, :cond_3

    .line 157
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 198
    .end local v7    # "isHide":Z
    :cond_3
    :goto_1
    return-void

    .line 150
    .restart local v8    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_4
    const/4 v7, 0x0

    goto :goto_0

    .line 162
    .end local v8    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v7    # "isHide":Z
    :cond_5
    const/4 v11, 0x0

    iput v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mViewResult:I

    .line 165
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v11, :cond_8

    const v3, 0x7f020122

    .line 166
    .local v3, "checkOnId":I
    :goto_2
    sget-boolean v11, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v11, :cond_9

    const v2, 0x7f020121

    .line 168
    .local v2, "checkOffId":I
    :goto_3
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v12

    invoke-virtual {v11, v12}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v11

    if-eqz v11, :cond_a

    .line 169
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v11, v12, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 174
    .local v4, "drawable":Landroid/graphics/drawable/Drawable;
    :goto_4
    if-nez v1, :cond_6

    .line 175
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 176
    .restart local v1    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v11, 0x4

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 178
    :cond_6
    const/4 v5, 0x0

    .line 179
    .local v5, "fitMode":I
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v10

    .line 180
    .local v10, "width":I
    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    .line 181
    .local v6, "height":I
    iget-object v11, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v11

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v9

    .line 182
    .local v9, "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    instance-of v11, v9, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v11, :cond_7

    .line 183
    int-to-float v11, v10

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v12, v12, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_size_factor:F

    mul-float/2addr v11, v12

    float-to-int v10, v11

    .line 184
    int-to-float v11, v6

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v12, v12, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_size_factor:F

    mul-float/2addr v11, v12

    float-to-int v6, v11

    .line 185
    const/4 v5, 0x1

    .line 187
    :cond_7
    invoke-virtual {v1, v10, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 188
    const/4 v11, 0x0

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v12, v12, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_top_margine:I

    iget-object v13, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mConfig:Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;

    iget v13, v13, Lcom/sec/samsung/gallery/view/photoview/ConfigPhoto$PhotoView;->edit_mode_checkbox_right_margine:I

    const/4 v14, 0x0

    invoke-virtual {v1, v11, v12, v13, v14}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 189
    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 191
    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-virtual {v1, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 192
    const/4 v11, 0x0

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_1

    .line 165
    .end local v2    # "checkOffId":I
    .end local v3    # "checkOnId":I
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v5    # "fitMode":I
    .end local v6    # "height":I
    .end local v9    # "viewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .end local v10    # "width":I
    :cond_8
    const v3, 0x7f0204ad

    goto :goto_2

    .line 166
    .restart local v3    # "checkOnId":I
    :cond_9
    const v2, 0x7f0204a6

    goto :goto_3

    .line 171
    .restart local v2    # "checkOffId":I
    :cond_a
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v11

    iget-object v12, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v12}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    invoke-virtual {v11, v12, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .restart local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    goto :goto_4

    .line 194
    .end local v2    # "checkOffId":I
    .end local v3    # "checkOnId":I
    .end local v4    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v7    # "isHide":Z
    :cond_b
    if-eqz v1, :cond_3

    .line 195
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method private drawDeleteButton(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 9
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v8, 0x6

    const/4 v3, 0x1

    const/4 v7, 0x3

    const/4 v2, 0x0

    .line 104
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 106
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_1

    .line 107
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020478

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 108
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 109
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 110
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 111
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 112
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 114
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGapInLand:Z

    if-eqz v4, :cond_0

    .line 115
    invoke-virtual {v0, v2, v7, v7, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 117
    :cond_0
    invoke-virtual {v0, v7, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 119
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v4, :cond_2

    :goto_0
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 120
    return-void

    :cond_2
    move v2, v3

    .line 119
    goto :goto_0
.end method


# virtual methods
.method public getIsDragging()Z
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mIsDragging:Z

    return v0
.end method

.method public getThumbSize()I
    .locals 1

    .prologue
    .line 200
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mThumbSize:I

    return v0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v5, 0x1

    .line 55
    const/4 v4, 0x0

    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mIsDragging:Z

    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 57
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v1, :cond_1

    .line 58
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 59
    .local v3, "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 60
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumHeader:Z

    if-eqz v4, :cond_2

    .line 61
    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 80
    :cond_0
    :goto_0
    invoke-direct {p0, v1, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->drawDeleteButton(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 82
    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v3    # "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_1
    return-object v1

    .line 65
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v3    # "selectionManager":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_2
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v2

    .line 66
    .local v2, "selected":Z
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumMode:Z

    if-eqz v4, :cond_4

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v4

    if-eqz v4, :cond_4

    if-eqz v2, :cond_4

    .line 68
    if-eqz v0, :cond_3

    .line 69
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 71
    :cond_3
    iput v5, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mViewResult:I

    goto :goto_0

    .line 73
    :cond_4
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewAlbumDrop:Z

    if-nez v4, :cond_5

    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumMode:Z

    if-nez v4, :cond_6

    .line 74
    :cond_5
    invoke-direct {p0, v1, p1, p3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->drawCheckboxAndMark(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/glcore/GlLayer;)V

    goto :goto_0

    .line 76
    :cond_6
    invoke-direct {p0, v1, p1, p3}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->drawAddButton(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/glcore/GlLayer;)V

    goto :goto_0
.end method

.method public getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p4, "ext"    # I
    .param p5, "obj"    # Ljava/lang/Object;

    .prologue
    .line 87
    const/16 v0, 0x100

    if-ne p4, v0, :cond_0

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mIsDragging:Z

    .line 89
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    goto :goto_0
.end method

.method public isNewAlbumMode()Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumMode:Z

    return v0
.end method

.method public isSelected(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public selectedCount()I
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    return v0
.end method

.method public setIsDragging(Z)V
    .locals 0
    .param p1, "isDragging"    # Z

    .prologue
    .line 97
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mIsDragging:Z

    .line 98
    return-void
.end method

.method public setNewAlbumMode(Z)V
    .locals 1
    .param p1, "isNewAlbumMode"    # Z

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mNewAlbumMode:Z

    .line 42
    if-eqz p1, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 47
    :goto_0
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    goto :goto_0
.end method

.method public setThumbSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 204
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->mThumbSize:I

    .line 205
    return-void
.end method
