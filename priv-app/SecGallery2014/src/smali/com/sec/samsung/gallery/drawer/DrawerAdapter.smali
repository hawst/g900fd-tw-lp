.class public Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
.super Landroid/widget/ArrayAdapter;
.source "DrawerAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/CharSequence;",
        ">;"
    }
.end annotation


# static fields
.field public static final GROUP_LEVEL:I = 0x2710

.field private static final TAG:Ljava/lang/String;

.field private static final TAG_CHILD_VIEW:I = 0x2

.field private static final TAG_GROUP_VIEW:I = 0x1


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mDrawerItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/drawer/DrawerItem;",
            ">;"
        }
    .end annotation
.end field

.field public mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

.field private mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

.field private mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

.field private mGroups:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/drawer/Group;",
            ">;"
        }
    .end annotation
.end field

.field private mHasFilterBy:Z

.field private mIsExpanderMode:Z

.field private mIsFilterbyClicked:Z

.field private mIsPickMode:Z

.field private mResources:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0, p1, v1, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II)V

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;

    .line 44
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsPickMode:Z

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    .line 46
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsFilterbyClicked:Z

    .line 47
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mHasFilterBy:Z

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    .line 53
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    .line 56
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$1;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    .line 68
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$2;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    .line 85
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$3;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    .line 119
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->initialize()V

    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->initialize()V

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->initialize()V

    .line 122
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroups:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mHasFilterBy:Z

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)Landroid/content/res/Resources;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$701(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/drawer/DrawerAdapter;

    .prologue
    .line 27
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method private wasChildView(Landroid/view/View;)Z
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 324
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 325
    .local v0, "tag":Ljava/lang/Object;
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "tag":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method private wasGroupNameView(Landroid/view/View;)Z
    .locals 4
    .param p1, "convertView"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 319
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 320
    .local v0, "tag":Ljava/lang/Object;
    if-eqz v0, :cond_1

    check-cast v0, Ljava/lang/Integer;

    .end local v0    # "tag":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-ne v3, v1, :cond_0

    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0

    .restart local v0    # "tag":Ljava/lang/Object;
    :cond_1
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x0

    return v0
.end method

.method public getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getDrawerItem(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 28
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 148
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v9

    .line 149
    .local v9, "childItem":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isGroupNameItem()Z

    move-result v3

    if-eqz v3, :cond_f

    .line 150
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isEmptyItem()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 151
    if-nez p2, :cond_0

    .line 152
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030056

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object/from16 v10, p2

    .line 315
    .end local p2    # "convertView":Landroid/view/View;
    .local v10, "convertView":Landroid/view/View;
    :goto_0
    return-object v10

    .line 157
    .end local v10    # "convertView":Landroid/view/View;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerExpanderMode:Z

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0e04cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030058

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 163
    :goto_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_2

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04cc

    if-ne v3, v4, :cond_2

    .line 164
    const v3, 0x7f0f00e4

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 165
    .local v13, "dividerView":Landroid/widget/ImageView;
    if-eqz v13, :cond_2

    .line 166
    const/4 v3, 0x4

    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 170
    .end local v13    # "dividerView":Landroid/widget/ImageView;
    :cond_2
    const v3, 0x7f0f00e5

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/TextView;

    .line 171
    .local v16, "groupTextView":Landroid/widget/TextView;
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 172
    invoke-virtual/range {v16 .. v16}, Landroid/widget/TextView;->setSingleLine()V

    .line 173
    sget-object v3, Landroid/text/TextUtils$TruncateAt;->MARQUEE:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 174
    const/4 v3, -0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMarqueeRepeatLimit(I)V

    .line 175
    const/4 v3, 0x1

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 177
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerExpanderMode:Z

    if-eqz v3, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v4, 0x7f0e04cb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    .line 178
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewFilterByHeaderForDarkTheme:Z

    if-eqz v3, :cond_3

    .line 179
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_7

    .line 180
    const/high16 v3, -0x1000000

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 185
    :cond_3
    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e02ae

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e043e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 186
    const v3, 0x7f0f00e6

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/ImageView;

    .line 187
    .local v17, "imageView":Landroid/widget/ImageView;
    if-eqz v17, :cond_5

    .line 188
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_9

    .line 189
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    if-eqz v3, :cond_8

    const v15, 0x7f0200b0

    .line 190
    .local v15, "expandDrawerResId":I
    :goto_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v15}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v14

    .line 191
    .local v14, "expandDrawer":Landroid/graphics/drawable/Drawable;
    const/high16 v3, 0x7f000000

    invoke-virtual {v14, v3}, Landroid/graphics/drawable/Drawable;->setTint(I)V

    .line 192
    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 196
    .end local v14    # "expandDrawer":Landroid/graphics/drawable/Drawable;
    .end local v15    # "expandDrawerResId":I
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    if-eqz v3, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e043f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    :goto_5
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 198
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsFilterbyClicked:Z

    if-eqz v3, :cond_4

    .line 199
    new-instance v2, Landroid/view/animation/RotateAnimation;

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    if-nez v3, :cond_c

    const/high16 v3, 0x43340000    # 180.0f

    :goto_6
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    const/4 v7, 0x1

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-direct/range {v2 .. v8}, Landroid/view/animation/RotateAnimation;-><init>(FFIFIF)V

    .line 200
    .local v2, "anim":Landroid/view/animation/RotateAnimation;
    new-instance v3, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v3}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 201
    const-wide/16 v4, 0x14a

    invoke-virtual {v2, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 202
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillEnabled(Z)V

    .line 203
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 204
    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 206
    .end local v2    # "anim":Landroid/view/animation/RotateAnimation;
    :cond_4
    const/4 v3, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 207
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsFilterbyClicked:Z

    .line 216
    .end local v17    # "imageView":Landroid/widget/ImageView;
    :cond_5
    :goto_7
    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v10, p2

    .line 217
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v10    # "convertView":Landroid/view/View;
    goto/16 :goto_0

    .line 160
    .end local v10    # "convertView":Landroid/view/View;
    .end local v16    # "groupTextView":Landroid/widget/TextView;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_6
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030057

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto/16 :goto_1

    .line 182
    .restart local v16    # "groupTextView":Landroid/widget/TextView;
    :cond_7
    const v3, -0x111112

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_2

    .line 189
    .restart local v17    # "imageView":Landroid/widget/ImageView;
    :cond_8
    const v15, 0x7f0200b1

    goto/16 :goto_3

    .line 194
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    if-eqz v3, :cond_a

    const v3, 0x7f0200b0

    :goto_8
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto/16 :goto_4

    :cond_a
    const v3, 0x7f0200b1

    goto :goto_8

    .line 196
    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0440

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_5

    .line 199
    :cond_c
    const/high16 v3, -0x3ccc0000    # -180.0f

    goto/16 :goto_6

    .line 210
    .end local v17    # "imageView":Landroid/widget/ImageView;
    :cond_d
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getTtsId()I

    move-result v3

    if-eqz v3, :cond_e

    .line 211
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getTtsId()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e043d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 213
    :cond_e
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e043d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_7

    .line 219
    .end local v16    # "groupTextView":Landroid/widget/TextView;
    :cond_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f030059

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 221
    const/16 v25, 0x0

    .line 222
    .local v25, "slinkItem":Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;
    const/16 v22, 0x0

    .line 223
    .local v22, "nearbyItem":Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;
    instance-of v3, v9, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    if-eqz v3, :cond_19

    move-object/from16 v25, v9

    .line 224
    check-cast v25, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    .line 229
    :cond_10
    :goto_9
    const v3, 0x7f0f00e6

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/ImageView;

    .line 230
    .local v18, "imageview":Landroid/widget/ImageView;
    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->setIcon(Landroid/widget/ImageView;)V

    .line 232
    const v3, 0x7f0f00e8

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v23

    check-cast v23, Landroid/widget/ImageView;

    .line 233
    .local v23, "networkMode":Landroid/widget/ImageView;
    if-eqz v25, :cond_1a

    .line 234
    move-object/from16 v0, v25

    move-object/from16 v1, v23

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->setNetworkMode(Landroid/widget/ImageView;)V

    .line 239
    :goto_a
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-nez v3, :cond_11

    .line 240
    const v3, 0x7f0f00e9

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/ImageView;

    .line 241
    .restart local v13    # "dividerView":Landroid/widget/ImageView;
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->isLastItem(I)Z

    move-result v3

    if-nez v3, :cond_1b

    const/4 v3, 0x4

    :goto_b
    invoke-virtual {v13, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 243
    .end local v13    # "dividerView":Landroid/widget/ImageView;
    :cond_11
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04cb

    if-ne v3, v4, :cond_12

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFilterBG:Z

    if-eqz v3, :cond_12

    .line 244
    const v3, 0x7f0200b2

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 247
    :cond_12
    const v3, 0x7f0f00e7

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v27

    check-cast v27, Landroid/widget/TextView;

    .line 249
    .local v27, "textView":Landroid/widget/TextView;
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v21

    .line 250
    .local v21, "name":Ljava/lang/CharSequence;
    if-eqz v21, :cond_1c

    .line 251
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 256
    :goto_c
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getTtsId()I

    move-result v3

    if-eqz v3, :cond_1d

    .line 257
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getTtsId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 262
    :goto_d
    if-eqz v25, :cond_13

    .line 263
    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->getNetworkMode()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_13

    .line 264
    const/16 v3, 0xc8

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->dpToPixel(I)I

    move-result v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 268
    :cond_13
    const/16 v24, 0x0

    .line 269
    .local v24, "selected":Z
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v19

    .line 270
    .local v19, "itemTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v11

    .line 271
    .local v11, "currentTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Lcom/sec/samsung/gallery/core/TabTagType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 272
    const/4 v12, 0x0

    .line 273
    .local v12, "data":Landroid/os/Bundle;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    if-eqz v3, :cond_14

    .line 274
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/ActivityState;->getData()Landroid/os/Bundle;

    move-result-object v12

    .line 276
    :cond_14
    const/16 v20, 0x0

    .line 277
    .local v20, "mediaSetPath":Ljava/lang/String;
    if-eqz v12, :cond_15

    .line 278
    const-string v3, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v12, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 281
    :cond_15
    if-eqz v20, :cond_18

    .line 282
    if-eqz v25, :cond_16

    invoke-virtual/range {v25 .. v25}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->getMediaSetPath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_17

    :cond_16
    if-eqz v22, :cond_18

    invoke-virtual/range {v22 .. v22}, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->getMediaSetPath()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    .line 284
    :cond_17
    const/16 v24, 0x1

    .line 303
    .end local v12    # "data":Landroid/os/Bundle;
    .end local v20    # "mediaSetPath":Ljava/lang/String;
    :cond_18
    :goto_e
    if-eqz v24, :cond_20

    .line 304
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200b3

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 314
    :goto_f
    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object/from16 v10, p2

    .line 315
    .end local p2    # "convertView":Landroid/view/View;
    .restart local v10    # "convertView":Landroid/view/View;
    goto/16 :goto_0

    .line 225
    .end local v10    # "convertView":Landroid/view/View;
    .end local v11    # "currentTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v18    # "imageview":Landroid/widget/ImageView;
    .end local v19    # "itemTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .end local v21    # "name":Ljava/lang/CharSequence;
    .end local v23    # "networkMode":Landroid/widget/ImageView;
    .end local v24    # "selected":Z
    .end local v27    # "textView":Landroid/widget/TextView;
    .restart local p2    # "convertView":Landroid/view/View;
    :cond_19
    instance-of v3, v9, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;

    if-eqz v3, :cond_10

    move-object/from16 v22, v9

    .line 226
    check-cast v22, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;

    goto/16 :goto_9

    .line 236
    .restart local v18    # "imageview":Landroid/widget/ImageView;
    .restart local v23    # "networkMode":Landroid/widget/ImageView;
    :cond_1a
    const/4 v3, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_a

    .line 241
    .restart local v13    # "dividerView":Landroid/widget/ImageView;
    :cond_1b
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 253
    .end local v13    # "dividerView":Landroid/widget/ImageView;
    .restart local v21    # "name":Ljava/lang/CharSequence;
    .restart local v27    # "textView":Landroid/widget/TextView;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getNameStringId()I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_c

    .line 259
    :cond_1d
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_d

    .line 288
    .restart local v11    # "currentTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .restart local v19    # "itemTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    .restart local v24    # "selected":Z
    :cond_1e
    if-eqz v11, :cond_18

    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_18

    .line 289
    sget-object v3, Lcom/sec/samsung/gallery/core/TabTagType;->TABTAG_STARING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v11}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v26

    .line 291
    .local v26, "tagStringId":I
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04cc

    if-ne v3, v4, :cond_1f

    .line 292
    if-eqz v26, :cond_18

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getNameStringId()I

    move-result v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_18

    .line 293
    const/16 v24, 0x1

    goto/16 :goto_e

    .line 295
    :cond_1f
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04cb

    if-ne v3, v4, :cond_18

    .line 296
    if-eqz v26, :cond_18

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getNameStringId()I

    move-result v3

    move/from16 v0, v26

    if-ne v0, v3, :cond_18

    .line 297
    const/16 v24, 0x1

    goto/16 :goto_e

    .line 306
    .end local v26    # "tagStringId":I
    :cond_20
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_21

    .line 307
    const/high16 v3, -0x1000000

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_f

    .line 309
    :cond_21
    const v3, -0xa0a0b

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_f
.end method

.method public hasFilterBy()Z
    .locals 1

    .prologue
    .line 423
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mHasFilterBy:Z

    return v0
.end method

.method public isEnabled(I)Z
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 388
    if-ltz p1, :cond_1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getCount()I

    move-result v3

    if-ge p1, v3, :cond_1

    .line 389
    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;

    .line 394
    .local v0, "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isGroupNameItem()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 395
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDrawerExpanderMode:Z

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getName()Ljava/lang/CharSequence;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mResources:Landroid/content/res/Resources;

    const v5, 0x7f0e04cb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 403
    .end local v0    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    :cond_0
    :goto_0
    return v1

    :cond_1
    move v1, v2

    .line 391
    goto :goto_0

    .restart local v0    # "item":Lcom/sec/samsung/gallery/drawer/DrawerItem;
    :cond_2
    move v1, v2

    .line 398
    goto :goto_0

    .line 400
    :cond_3
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsPickMode:Z

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->getGroupId()I

    move-result v3

    const v4, 0x7f0e04cc

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 401
    goto :goto_0
.end method

.method public isLastItem(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 377
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mDrawerItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    add-int/lit8 v0, p1, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerItem;->isGroupNameItem()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/drawer/DrawerAdapter$4;-><init>(Lcom/sec/samsung/gallery/drawer/DrawerAdapter;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 366
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->onDestroy()V

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->onDestroy()V

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->onDestroy()V

    .line 144
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->onPause()V

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->onPause()V

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->onPause()V

    .line 138
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupViewBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupViewBy;->onResume()V

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->onResume()V

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupRemote:Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/DrawerGroupRemote;->onResume()V

    .line 132
    return-void
.end method

.method public setAlbumPickMode(Z)V
    .locals 0
    .param p1, "flag"    # Z

    .prologue
    .line 413
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsPickMode:Z

    .line 414
    return-void
.end method

.method public updateFilterByExpandMode()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 417
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsExpanderMode:Z

    .line 418
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mIsFilterbyClicked:Z

    .line 419
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerAdapter;->mGroupFilterBy:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    iget-object v0, v0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    .line 420
    return-void

    .line 417
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
