.class public Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
.source "MapMarkerOptionsChooser.java"


# instance fields
.field private final clusterPaintMedium:Landroid/graphics/Paint;

.field private final contextRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationIconPin:Landroid/graphics/Bitmap;

.field private mLocationTouchedIconPin:Landroid/graphics/Bitmap;

.field mResource:Landroid/content/res/Resources;

.field private final twoToasters:Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "twoToasters"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;-><init>()V

    .line 81
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    .line 82
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    .line 40
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->contextRef:Ljava/lang/ref/WeakReference;

    .line 41
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->twoToasters:Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    const/16 v1, 0x26

    const/16 v2, 0x38

    const/16 v3, 0x45

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoRegularFont()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    const v2, 0x7f0d02f3

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 58
    return-void
.end method


# virtual methods
.method public choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 9
    .param p1, "markerOptions"    # Lcom/google/android/gms/maps/model/MarkerOptions;
    .param p2, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 62
    iget-object v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->contextRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 63
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_0

    .line 64
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 65
    .local v5, "res":Landroid/content/res/Resources;
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->size()I

    move-result v8

    if-le v8, v6, :cond_1

    move v4, v6

    .line 68
    .local v4, "isCluster":Z
    :goto_0
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->size()I

    move-result v0

    .line 69
    .local v0, "clusterSize":I
    const v8, 0x7f0203a1

    invoke-virtual {p0, v1, v5, v8, p2}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->getClusterBitmap(Landroid/content/Context;Landroid/content/res/Resources;ILcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v3

    .line 70
    .local v3, "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    invoke-virtual {p2, v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPointAtOffset(I)Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/mapfragment/MarkerData;

    .line 71
    .local v2, "data":Lcom/sec/samsung/gallery/mapfragment/MarkerData;
    invoke-virtual {p1, v3}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 72
    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {p1, v7, v8}, Lcom/google/android/gms/maps/model/MarkerOptions;->anchor(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 74
    if-ne v0, v6, :cond_2

    .line 75
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 79
    .end local v0    # "clusterSize":I
    .end local v2    # "data":Lcom/sec/samsung/gallery/mapfragment/MarkerData;
    .end local v3    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .end local v4    # "isCluster":Z
    .end local v5    # "res":Landroid/content/res/Resources;
    :cond_0
    :goto_1
    return-void

    .restart local v5    # "res":Landroid/content/res/Resources;
    :cond_1
    move v4, v7

    .line 65
    goto :goto_0

    .line 77
    .restart local v0    # "clusterSize":I
    .restart local v2    # "data":Lcom/sec/samsung/gallery/mapfragment/MarkerData;
    .restart local v3    # "icon":Lcom/google/android/gms/maps/model/BitmapDescriptor;
    .restart local v4    # "isCluster":Z
    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/maps/model/LatLng;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0e0102

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->title(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    goto :goto_1
.end method

.method public getClusterBitmap(Landroid/content/Context;Landroid/content/res/Resources;ILcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Landroid/graphics/Bitmap;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "res"    # Landroid/content/res/Resources;
    .param p3, "resourceId"    # I
    .param p4, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 86
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 87
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    const/16 v16, 0x1

    move/from16 v0, v16

    iput-boolean v0, v10, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    .line 89
    new-instance v7, Landroid/util/DisplayMetrics;

    invoke-direct {v7}, Landroid/util/DisplayMetrics;-><init>()V

    .line 90
    .local v7, "displayMetrics":Landroid/util/DisplayMetrics;
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual/range {p1 .. p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v16

    invoke-interface/range {v16 .. v16}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 92
    const/4 v2, 0x0

    .line 93
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    if-nez v16, :cond_3

    const v16, 0x7f0203a1

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_3

    .line 94
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    .line 98
    :cond_0
    :goto_0
    const v16, 0x7f0203a2

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    if-eqz v16, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v16

    if-nez v16, :cond_4

    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    .line 103
    :cond_1
    :goto_1
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 104
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 105
    .local v5, "canvas":Landroid/graphics/Canvas;
    const/4 v12, 0x0

    .line 107
    .local v12, "paint":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->clusterPaintMedium:Landroid/graphics/Paint;

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0e0022

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v3

    .line 109
    .local v3, "bitmapLeftMarginFactor":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0e0023

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v13

    .line 110
    .local v13, "resizeCountTextHeightFactor":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0e0024

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v14

    .line 111
    .local v14, "resizeCountTextWidthFactor":F
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v16, v0

    const v17, 0x7f0c0052

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 113
    .local v4, "bitmapTopMargin":I
    if-eqz p4, :cond_2

    .line 114
    const/16 v16, 0x0

    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPointAtOffset(I)Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    move-result-object v8

    .line 115
    .local v8, "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    if-eqz v8, :cond_2

    .line 116
    invoke-virtual {v8}, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;->getInfo()Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    move-result-object v11

    .line 117
    .local v11, "overlayInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    if-eqz v11, :cond_2

    .line 118
    invoke-virtual {v11}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getMakerRepresentativeImg()Landroid/graphics/Bitmap;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d02f4

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mResource:Landroid/content/res/Resources;

    move-object/from16 v18, v0

    const v19, 0x7f0d02f5

    invoke-virtual/range {v18 .. v19}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    const/16 v19, 0x0

    invoke-static/range {v16 .. v19}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v15

    .line 121
    .local v15, "thumbnail":Landroid/graphics/Bitmap;
    const-string v16, "clustering"

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    const-string v16, "clustering"

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v17

    invoke-static/range {v16 .. v17}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v3

    int-to-float v0, v4

    move/from16 v17, v0

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v15, v0, v1, v12}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 126
    const v16, 0x7f0203a2

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_5

    .line 127
    const v16, 0x7f0b0054

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 128
    .local v6, "color":I
    invoke-virtual {v12, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    :goto_2
    invoke-virtual/range {p4 .. p4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->size()I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v9

    .line 135
    .local v9, "itemsCount":Ljava/lang/String;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v16, v16, v14

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    mul-float v17, v17, v13

    const/high16 v18, 0x40800000    # 4.0f

    sub-float v17, v17, v18

    move/from16 v0, v16

    move/from16 v1, v17

    invoke-virtual {v5, v9, v0, v1, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 139
    .end local v6    # "color":I
    .end local v8    # "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .end local v9    # "itemsCount":Ljava/lang/String;
    .end local v11    # "overlayInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    .end local v15    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_2
    return-object v2

    .line 95
    .end local v3    # "bitmapLeftMarginFactor":F
    .end local v4    # "bitmapTopMargin":I
    .end local v5    # "canvas":Landroid/graphics/Canvas;
    .end local v12    # "paint":Landroid/graphics/Paint;
    .end local v13    # "resizeCountTextHeightFactor":F
    .end local v14    # "resizeCountTextWidthFactor":F
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    if-nez v16, :cond_0

    const v16, 0x7f0203a2

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_0

    .line 96
    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationTouchedIconPin:Landroid/graphics/Bitmap;

    goto/16 :goto_0

    .line 100
    :cond_4
    const v16, 0x7f0203a1

    move/from16 v0, p3

    move/from16 v1, v16

    if-ne v0, v1, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v16

    if-nez v16, :cond_1

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->mLocationIconPin:Landroid/graphics/Bitmap;

    goto/16 :goto_1

    .line 130
    .restart local v3    # "bitmapLeftMarginFactor":F
    .restart local v4    # "bitmapTopMargin":I
    .restart local v5    # "canvas":Landroid/graphics/Canvas;
    .restart local v8    # "inputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .restart local v11    # "overlayInfo":Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;
    .restart local v12    # "paint":Landroid/graphics/Paint;
    .restart local v13    # "resizeCountTextHeightFactor":F
    .restart local v14    # "resizeCountTextWidthFactor":F
    .restart local v15    # "thumbnail":Landroid/graphics/Bitmap;
    :cond_5
    const v16, 0x7f0b0053

    move-object/from16 v0, p2

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    .line 131
    .restart local v6    # "color":I
    invoke-virtual {v12, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2
.end method
