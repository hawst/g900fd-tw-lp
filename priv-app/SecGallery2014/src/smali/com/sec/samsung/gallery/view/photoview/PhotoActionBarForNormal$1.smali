.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;
.super Ljava/lang/Object;
.source "PhotoActionBarForNormal.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 45
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 46
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030004

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 48
    .local v0, "customView":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$100(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 53
    .end local v0    # "customView":Landroid/view/View;
    :goto_0
    const/16 v1, 0xc

    .line 55
    .local v1, "flags":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$300(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 57
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$400(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 58
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 60
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->isReadyToEnableHomeButton()Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 61
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$700(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 63
    :cond_0
    return-void

    .line 50
    .end local v1    # "flags":I
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;->access$200(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForNormal;)Landroid/app/ActionBar;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    goto :goto_0
.end method
