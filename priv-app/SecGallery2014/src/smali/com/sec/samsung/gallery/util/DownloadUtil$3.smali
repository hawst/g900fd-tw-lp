.class final Lcom/sec/samsung/gallery/util/DownloadUtil$3;
.super Ljava/lang/Object;
.source "DownloadUtil.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/util/DownloadUtil;->downloadEnqueue(Landroid/content/Context;Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$list:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 211
    iput-object p1, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$3;->val$list:Ljava/util/ArrayList;

    iput-object p2, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$3;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 214
    const/4 v1, 0x0

    .line 215
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$3;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 216
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$3;->val$list:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 217
    .restart local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v2, p0, Lcom/sec/samsung/gallery/util/DownloadUtil$3;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->download(Landroid/content/Context;)V

    .line 215
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    return-void
.end method
