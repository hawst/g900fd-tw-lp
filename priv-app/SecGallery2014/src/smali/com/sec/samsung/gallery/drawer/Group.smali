.class public Lcom/sec/samsung/gallery/drawer/Group;
.super Ljava/lang/Object;
.source "Group.java"


# static fields
.field public static final GROUP_TYPE_DEFAULT:I = 0x0

.field public static final GROUP_TYPE_OTHERS:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Group"


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/drawer/DrawerItem;",
            ">;"
        }
    .end annotation
.end field

.field private mName:Ljava/lang/String;

.field private mTitleItem:Lcom/sec/samsung/gallery/drawer/DrawerItem;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;II)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "groupNameId"    # I
    .param p3, "type"    # I

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 28
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mName:Ljava/lang/String;

    .line 29
    move v0, p2

    .line 30
    .local v0, "ttsId":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mName:Ljava/lang/String;

    invoke-static {p2, v0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;->createGroupTitleItems(IILjava/lang/String;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mTitleItem:Lcom/sec/samsung/gallery/drawer/DrawerItem;

    .line 31
    invoke-virtual {p0, p2, p3}, Lcom/sec/samsung/gallery/drawer/Group;->checkGroupType(II)V

    .line 32
    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;III)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "groupNameId"    # I
    .param p3, "ttsId"    # I
    .param p4, "type"    # I

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/Group;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mName:Ljava/lang/String;

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mName:Ljava/lang/String;

    invoke-static {p2, p3, v0}, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;->createGroupTitleItems(IILjava/lang/String;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mTitleItem:Lcom/sec/samsung/gallery/drawer/DrawerItem;

    .line 38
    invoke-virtual {p0, p2, p4}, Lcom/sec/samsung/gallery/drawer/Group;->checkGroupType(II)V

    .line 39
    return-void
.end method


# virtual methods
.method public addChild(Lcom/sec/samsung/gallery/drawer/DrawerItem;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/samsung/gallery/drawer/DrawerItem;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    return-void
.end method

.method public checkGroupType(II)V
    .locals 5
    .param p1, "groupNameId"    # I
    .param p2, "type"    # I

    .prologue
    const v4, 0x7f0e00a4

    const v3, 0x7f0e0073

    .line 42
    packed-switch p2, :pswitch_data_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 44
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    const v1, 0x7f020190

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {p1, v3, v3, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->createViewByItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    const v1, 0x7f020213

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-static {p1, v4, v4, v1, v2}, Lcom/sec/samsung/gallery/drawer/DrawerItemViewBy;->createViewByItems(IIIILcom/sec/samsung/gallery/core/TabTagType;)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 52
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/sec/samsung/gallery/drawer/DrawerItemTitle;->createEmptyTitleItems(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public getChild(I)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 85
    if-gez p1, :cond_0

    .line 86
    const/4 p1, 0x0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/drawer/DrawerItem;

    return-object v0
.end method

.method public getChildrenCount()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleItem()Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mTitleItem:Lcom/sec/samsung/gallery/drawer/DrawerItem;

    return-object v0
.end method

.method public removeChild(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public removeChildren()V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/Group;->mItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 82
    return-void
.end method
