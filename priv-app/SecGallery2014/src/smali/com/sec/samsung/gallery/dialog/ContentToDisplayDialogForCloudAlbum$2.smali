.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;
.super Landroid/widget/ArrayAdapter;
.source "ContentToDisplayDialogForCloudAlbum.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->createViewtypeDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;Landroid/content/Context;II[Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # I
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 179
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 189
    if-eqz p2, :cond_1

    move-object v2, p2

    .line 192
    .local v2, "cView":Landroid/view/View;
    :goto_0
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_2

    sget v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mWhiteThemeListClickedColor:I

    .line 194
    .local v3, "colorOnClick":I
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    .line 195
    .local v4, "viewType":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v7

    .line 196
    .local v7, "isEnabled":Z
    invoke-virtual {v2, v7}, Landroid/view/View;->setEnabled(Z)V

    move-object v6, v2

    .line 198
    check-cast v6, Landroid/widget/CheckedTextView;

    .line 199
    .local v6, "ctv":Landroid/widget/CheckedTextView;
    if-eqz v7, :cond_3

    .line 200
    sget-object v0, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    .line 205
    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)I

    move-result v0

    and-int/2addr v0, v4

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {v6, v0}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 207
    new-instance v0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;

    move-object v1, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2$1;-><init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;Landroid/view/View;III)V

    invoke-virtual {v6, v0}, Landroid/widget/CheckedTextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 282
    const v0, 0x1020014

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 283
    .local v8, "tv":Landroid/widget/TextView;
    if-nez v8, :cond_5

    .line 295
    :goto_4
    return-object v2

    .line 189
    .end local v2    # "cView":Landroid/view/View;
    .end local v3    # "colorOnClick":I
    .end local v4    # "viewType":I
    .end local v6    # "ctv":Landroid/widget/CheckedTextView;
    .end local v7    # "isEnabled":Z
    .end local v8    # "tv":Landroid/widget/TextView;
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto :goto_0

    .line 192
    .restart local v2    # "cView":Landroid/view/View;
    :cond_2
    sget v3, Lcom/sec/android/gallery3d/util/GalleryUtils;->mBlackThemeListClickedColor:I

    goto :goto_1

    .line 203
    .restart local v3    # "colorOnClick":I
    .restart local v4    # "viewType":I
    .restart local v6    # "ctv":Landroid/widget/CheckedTextView;
    .restart local v7    # "isEnabled":Z
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    xor-int/lit8 v1, v4, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mCurrentViewType:I
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)I

    goto :goto_2

    .line 205
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 287
    .restart local v8    # "tv":Landroid/widget/TextView;
    :cond_5
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 289
    if-nez v7, :cond_6

    .line 290
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Landroid/content/Context;

    move-result-object v1

    const v5, 0x7f0e02ac

    invoke-virtual {v1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 293
    :cond_6
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method public isEnabled(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 183
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->mValidContentOptions:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v0

    .line 184
    .local v0, "viewType":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum$2;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->isViewByOptionEnabled(I)Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialogForCloudAlbum;I)Z

    move-result v1

    return v1
.end method
