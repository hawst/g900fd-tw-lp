.class public Lcom/sec/samsung/gallery/controller/CopyToClipBoardCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "CopyToClipBoardCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 6
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v5, 0x0

    .line 17
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 18
    .local v3, "params":[Ljava/lang/Object;
    aget-object v0, v3, v5

    check-cast v0, Landroid/content/Context;

    .line 19
    .local v0, "context":Landroid/content/Context;
    const/4 v4, 0x1

    aget-object v2, v3, v4

    check-cast v2, Ljava/lang/String;

    .line 21
    .local v2, "filePath":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.samsung.clipboardsaveservice.CLIPBOARD_COPY_RECEIVER"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 22
    .local v1, "copyIntent":Landroid/content/Intent;
    const-string v4, "copyPath"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 23
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v4, :cond_0

    .line 24
    const-string v4, "darkTheme"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 25
    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 26
    return-void
.end method
