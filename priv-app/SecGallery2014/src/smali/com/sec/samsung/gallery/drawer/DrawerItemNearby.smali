.class public Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;
.super Lcom/sec/samsung/gallery/drawer/DrawerItem;
.source "DrawerItemNearby.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDevice:Lcom/samsung/android/allshare/Device;

.field private mHandler:Landroid/os/Handler;

.field private mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(ILcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;Lcom/samsung/android/allshare/Device;Landroid/os/Handler;)V
    .locals 2
    .param p1, "groupId"    # I
    .param p2, "nearbyDeviceSet"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;
    .param p3, "device"    # Lcom/samsung/android/allshare/Device;
    .param p4, "handler"    # Landroid/os/Handler;

    .prologue
    .line 28
    invoke-virtual {p3}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-direct {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/drawer/DrawerItem;-><init>(ILjava/lang/CharSequence;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 30
    iput-object p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    .line 31
    iput-object p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    .line 32
    iput-object p4, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mHandler:Landroid/os/Handler;

    .line 33
    sget-object v0, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_OTHER_DEVICE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->setTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mNearbyDeviceSet:Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mMediaSetPath:Ljava/lang/String;

    .line 39
    :goto_0
    return-void

    .line 37
    :cond_0
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mMediaSetPath:Ljava/lang/String;

    goto :goto_0
.end method

.method public static createNearByItems(ILcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;Lcom/samsung/android/allshare/Device;Landroid/os/Handler;)Lcom/sec/samsung/gallery/drawer/DrawerItem;
    .locals 1
    .param p0, "groupId"    # I
    .param p1, "nearbyDeviceSet"    # Lcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "handler"    # Landroid/os/Handler;

    .prologue
    .line 42
    new-instance v0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;-><init>(ILcom/sec/android/gallery3d/remote/nearby/NearbyDeviceSet;Lcom/samsung/android/allshare/Device;Landroid/os/Handler;)V

    return-object v0
.end method


# virtual methods
.method public getMediaSetPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mMediaSetPath:Ljava/lang/String;

    return-object v0
.end method

.method public selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
    .locals 3
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "statusProxy"    # Lcom/sec/android/gallery3d/app/StateManager;
    .param p3, "index"    # I
    .param p4, "isPickMode"    # Z

    .prologue
    .line 63
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 64
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->getMediaSetPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 66
    return-void
.end method

.method public setIcon(Landroid/widget/ImageView;)V
    .locals 5
    .param p1, "imageView"    # Landroid/widget/ImageView;

    .prologue
    const/4 v4, 0x0

    .line 47
    const-string v1, "NearbyIcon"

    const-string v2, "setIcon"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    invoke-virtual {p1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "icon":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v1}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 51
    sget-object v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter;->sIconCache:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "icon":Landroid/graphics/Bitmap;
    check-cast v0, Landroid/graphics/Bitmap;

    .line 54
    .restart local v0    # "icon":Landroid/graphics/Bitmap;
    :cond_0
    if-nez v0, :cond_1

    .line 55
    new-instance v1, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mDevice:Lcom/samsung/android/allshare/Device;

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;-><init>(Landroid/net/Uri;)V

    const/4 v2, 0x1

    new-array v2, v2, [Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemNearby;->mHandler:Landroid/os/Handler;

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/remote/nearby/ChangePlayerListAdapter$NearbyIconDownloader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
