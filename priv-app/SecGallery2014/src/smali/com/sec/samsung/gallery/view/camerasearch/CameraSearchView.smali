.class public Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
.super Landroid/widget/LinearLayout;
.source "CameraSearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchImageSpan;,
        Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;,
        Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;,
        Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;,
        Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field public static final VOICE_RECOGNITION_REQUEST:I = 0x22b


# instance fields
.field private mCloseButton:Landroid/widget/ImageView;

.field private mContext:Landroid/content/Context;

.field private mOldQueryText:Ljava/lang/CharSequence;

.field private final mOnClickListener:Landroid/view/View$OnClickListener;

.field private final mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

.field private final mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

.field private mOnSuggestionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;

.field private mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;

.field private mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

.field private mShowImeRunnable:Ljava/lang/Runnable;

.field private mSuggestionsAdapter:Landroid/widget/CursorAdapter;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mVoiceButton:Landroid/view/View;

.field private mVoiceButtonEnabled:Z

.field private mVoiceFrame:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 100
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 101
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 104
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$1;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    .line 202
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$2;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    .line 237
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$3;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    .line 281
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$4;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 289
    new-instance v0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$5;-><init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mTextWatcher:Landroid/text/TextWatcher;

    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    .line 106
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->initSearchView()V

    .line 107
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mCloseButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->onCloseClicked()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->startVoiceRecognitionActivity()V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->onSubmitQuery()V

    return-void
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnSuggestionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;Ljava/lang/CharSequence;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
    .param p1, "x1"    # Ljava/lang/CharSequence;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->onTextChanged(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 8
    .param p1, "hintText"    # Ljava/lang/CharSequence;

    .prologue
    const/4 v7, 0x0

    .line 220
    new-instance v3, Landroid/text/SpannableString;

    if-eqz p1, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-direct {v3, v4}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    .line 221
    .local v3, "ssb":Landroid/text/SpannableString;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v4

    if-eqz v4, :cond_1

    const v0, 0x7f0201de

    .line 223
    .local v0, "icon":I
    :goto_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 224
    .local v1, "searchIcon":Landroid/graphics/drawable/Drawable;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getTextSize()F

    move-result v4

    float-to-int v2, v4

    .line 225
    .local v2, "size":I
    invoke-virtual {v1, v7, v7, v2, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 226
    new-instance v4, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchImageSpan;

    invoke-direct {v4, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchImageSpan;-><init>(Landroid/graphics/drawable/Drawable;)V

    const/4 v5, 0x1

    const/16 v6, 0x21

    invoke-virtual {v3, v4, v7, v5, v6}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    .line 227
    return-object v3

    .line 220
    .end local v0    # "icon":I
    .end local v1    # "searchIcon":Landroid/graphics/drawable/Drawable;
    .end local v2    # "size":I
    .end local v3    # "ssb":Landroid/text/SpannableString;
    :cond_0
    const-string v4, " "

    goto :goto_0

    .line 221
    .restart local v3    # "ssb":Landroid/text/SpannableString;
    :cond_1
    const v0, 0x7f02002f

    goto :goto_1
.end method

.method private initSearchView()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 110
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 112
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f0300c9

    invoke-virtual {v0, v1, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 114
    const v1, 0x7f0f0118

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mCloseButton:Landroid/widget/ImageView;

    .line 115
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mCloseButton:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 116
    const v1, 0x7f0f0117

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButton:Landroid/view/View;

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButton:Landroid/view/View;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 118
    const v1, 0x7f0f0209

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceFrame:Landroid/view/View;

    .line 120
    const v1, 0x7f0f00eb

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnEditorActionListener:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 125
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->requestFocus()Z

    .line 126
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setImeVisibility(Z)V

    .line 127
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->updateButtons()V

    .line 128
    return-void
.end method

.method private onCloseClicked()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 263
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->requestFocus()Z

    .line 264
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setImeVisibility(Z)V

    .line 265
    return-void
.end method

.method private onSubmitQuery()V
    .locals 3

    .prologue
    .line 253
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 254
    .local v0, "query":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_0

    .line 255
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;->onQueryTextSubmit(Ljava/lang/String;)Z

    .line 256
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setImeVisibility(Z)V

    .line 257
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->dismissDropDown()V

    .line 259
    :cond_0
    return-void
.end method

.method private onTextChanged(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "newText"    # Ljava/lang/CharSequence;

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->updateButtons()V

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOldQueryText:Ljava/lang/CharSequence;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;->onQueryTextChange(Ljava/lang/String;)Z

    .line 249
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOldQueryText:Ljava/lang/CharSequence;

    .line 250
    return-void
.end method

.method private startVoiceRecognitionActivity()V
    .locals 5

    .prologue
    .line 268
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;

    if-eqz v2, :cond_0

    .line 269
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;->onVoiceRecognitionStarted()V

    .line 270
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 271
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string v3, "free_form"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 272
    const-string v2, "android.speech.extra.PROMPT"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0257

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 274
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0x22b

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return-void

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->LOG_TAG:Ljava/lang/String;

    const-string v3, "Activity Not Found"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateButtons()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 231
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    .line 232
    .local v0, "empty":Z
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButtonEnabled:Z

    if-eqz v4, :cond_0

    if-eqz v0, :cond_0

    move v1, v2

    .line 233
    .local v1, "visibility":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceFrame:Landroid/view/View;

    invoke-virtual {v4, v1}, Landroid/view/View;->setVisibility(I)V

    .line 234
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mCloseButton:Landroid/widget/ImageView;

    if-nez v0, :cond_1

    :goto_1
    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 235
    return-void

    .end local v1    # "visibility":I
    :cond_0
    move v1, v3

    .line 232
    goto :goto_0

    .restart local v1    # "visibility":I
    :cond_1
    move v2, v3

    .line 234
    goto :goto_1
.end method


# virtual methods
.method getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 182
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 183
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 184
    .local v0, "width":I
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-super {p0, v1, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 185
    return-void
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 169
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    .line 170
    if-nez p1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->requestFocus()Z

    .line 172
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 178
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/Window;->setSoftInputMode(I)V

    goto :goto_0
.end method

.method public setImeVisibility(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "input_method"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 190
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz p1, :cond_1

    .line 191
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v1

    if-nez v1, :cond_0

    .line 192
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->post(Ljava/lang/Runnable;)Z

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mShowImeRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 196
    if-eqz v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method public setOnQueryTextListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

    .prologue
    .line 136
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnQueryChangeListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnQueryTextListener;

    .line 137
    return-void
.end method

.method public setOnSuggestionListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;

    .prologue
    .line 140
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnSuggestionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;

    .line 141
    return-void
.end method

.method public setOnVoiceRecognitionListener(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mOnVoiceRecognitionListener:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnVoiceRecognitionListener;

    .line 145
    return-void
.end method

.method public setQuery(Ljava/lang/CharSequence;Z)V
    .locals 2
    .param p1, "query"    # Ljava/lang/CharSequence;
    .param p2, "submit"    # Z

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 149
    if-eqz p1, :cond_0

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setSelection(I)V

    .line 153
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 154
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->onSubmitQuery()V

    .line 156
    :cond_1
    return-void
.end method

.method public setQueryHint(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "hint"    # Ljava/lang/CharSequence;

    .prologue
    .line 216
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->getDecoratedHint(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setHint(Ljava/lang/CharSequence;)V

    .line 217
    return-void
.end method

.method public setSuggestionsAdapter(Landroid/widget/CursorAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/widget/CursorAdapter;

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    .line 164
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mQueryTextView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mSuggestionsAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 165
    return-void
.end method

.method public setVoiceButtonEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 131
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->mVoiceButtonEnabled:Z

    .line 132
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->updateButtons()V

    .line 133
    return-void
.end method
