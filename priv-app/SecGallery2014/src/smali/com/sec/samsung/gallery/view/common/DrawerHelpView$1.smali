.class Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;
.super Ljava/lang/Object;
.source "DrawerHelpView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->addView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 11

    .prologue
    const v5, 0x7f02006e

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 53
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # setter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mIsShowingHelpView:Z
    invoke-static {v4, v10}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$002(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Z)Z

    .line 54
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 79
    :goto_0
    return-void

    .line 56
    :cond_0
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewOverlayHelp:Z

    if-eqz v4, :cond_2

    .line 57
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v7, 0x7f030055

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v4, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    .line 60
    :goto_1
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 61
    .local v3, "locale":Ljava/lang/String;
    const-string v4, "ar"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string v4, "fa"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, "ur"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_1
    const v2, 0x7f02006f

    .line 63
    .local v2, "icon":I
    :goto_2
    const/4 v1, 0x0

    .line 65
    .local v1, "helpBodyIcon":Landroid/graphics/drawable/Drawable;
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 70
    :goto_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v4

    const v5, 0x7f0f00e2

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 72
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mCloseBtn:Landroid/widget/ImageButton;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$400(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/ImageButton;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RelativeLayout;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 73
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->initializeView()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$500(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)V

    .line 74
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # invokes: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->setHelpState(Z)V
    invoke-static {v4, v9}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$600(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Z)V

    .line 75
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->bringToFront()V

    .line 76
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerLayout:Landroid/support/v4/widget/DrawerLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$700(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/support/v4/widget/DrawerLayout;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/support/v4/widget/DrawerLayout;->setScrimColor(I)V

    .line 77
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$100(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v4

    invoke-virtual {v4, v10}, Landroid/widget/RelativeLayout;->setFocusableInTouchMode(Z)V

    .line 78
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Landroid/app/Activity;

    invoke-virtual {v4}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v4

    invoke-virtual {v4, v9}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto/16 :goto_0

    .line 59
    .end local v1    # "helpBodyIcon":Landroid/graphics/drawable/Drawable;
    .end local v2    # "icon":I
    .end local v3    # "locale":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v7, 0x7f030054

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mMainView:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$200(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v4, v7, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout;

    # setter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mDrawerlHelpView:Landroid/widget/RelativeLayout;
    invoke-static {v6, v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$102(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;Landroid/widget/RelativeLayout;)Landroid/widget/RelativeLayout;

    goto/16 :goto_1

    .restart local v3    # "locale":Ljava/lang/String;
    :cond_3
    move v2, v5

    .line 61
    goto/16 :goto_2

    .line 67
    .restart local v1    # "helpBodyIcon":Landroid/graphics/drawable/Drawable;
    .restart local v2    # "icon":I
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Landroid/content/res/Resources$NotFoundException;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/common/DrawerHelpView$1;->this$0:Lcom/sec/samsung/gallery/view/common/DrawerHelpView;

    # getter for: Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/common/DrawerHelpView;->access$300(Lcom/sec/samsung/gallery/view/common/DrawerHelpView;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto/16 :goto_3
.end method
