.class public Lcom/sec/samsung/gallery/util/MotionUtils;
.super Ljava/lang/Object;
.source "MotionUtils.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isMotionHelp(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 30
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 34
    :cond_0
    :goto_0
    return v1

    .line 33
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 34
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionHelpPanning(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 40
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 44
    :cond_0
    :goto_0
    return v1

    .line 43
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 44
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionHelpPeek(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 56
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 59
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 60
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionHelpTilt(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 48
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 52
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionPeekSeries(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 64
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return v1

    .line 67
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 68
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_HELP:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionPreview(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 10
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 14
    :cond_0
    :goto_0
    return v1

    .line 13
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 14
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_PREVIEW:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionTutorial(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 20
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v2, :cond_1

    .line 24
    :cond_0
    :goto_0
    return v1

    .line 23
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v0

    .line 24
    .local v0, "motionType":Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;
    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_TILT_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v0, v2, :cond_2

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_PANNING_PEEK_TUTORIAL:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-ne v0, v2, :cond_0

    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public static isMotionTutorialNone(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z
    .locals 3
    .param p0, "galleryActivity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v0, 0x1

    .line 74
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMotion:Z

    if-nez v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryMotion()Lcom/sec/android/gallery3d/app/GalleryMotion;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryMotion;->getTutorialType()Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;->MOTION_NONE:Lcom/sec/android/gallery3d/app/GalleryMotion$MotionType;

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method
