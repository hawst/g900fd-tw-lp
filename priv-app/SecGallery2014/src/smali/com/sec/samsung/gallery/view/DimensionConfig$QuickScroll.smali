.class public Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "QuickScroll"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;


# instance fields
.field public quick_center_popup_text_bottom_padding:I

.field public quick_center_popup_text_top_margin:I

.field public quick_scroll_cetner_popup_height:I

.field public quick_scroll_cetner_popup_width:I

.field public quick_scroll_font_size:I

.field public quick_scroll_height:I

.field public quick_scroll_item_pitch:F

.field public quick_scroll_popup_padding:I

.field public quick_scroll_popup_text_bottom_padding:I

.field public quick_scroll_root_pitch_ratio:F

.field public quick_scroll_root_y:F

.field public quick_scroll_root_z:F

.field public quick_scroll_width:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 717
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 718
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 719
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d03b6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_width:I

    .line 720
    const v1, 0x7f0d03b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_height:I

    .line 721
    const v1, 0x7f0d03bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_font_size:I

    .line 722
    const v1, 0x7f0d03b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_popup_padding:I

    .line 723
    const v1, 0x7f0d03b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_popup_text_bottom_padding:I

    .line 724
    const v1, 0x7f0d03ba

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_center_popup_text_bottom_padding:I

    .line 725
    const v1, 0x7f0d03bb

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_center_popup_text_top_margin:I

    .line 726
    const v1, 0x7f0d03bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_cetner_popup_height:I

    .line 727
    const v1, 0x7f0d03c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_cetner_popup_width:I

    .line 728
    const v1, 0x7f0e0027

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_item_pitch:F

    .line 729
    const v1, 0x7f0e0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_pitch_ratio:F

    .line 730
    const v1, 0x7f0e0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_y:F

    .line 731
    const v1, 0x7f0e002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->quick_scroll_root_z:F

    .line 732
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 711
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    if-nez v0, :cond_0

    .line 712
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;

    .line 714
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
