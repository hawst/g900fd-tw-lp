.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "GallerySearchViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;
    .param p3, "x1"    # Ljava/lang/Object;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-direct {p0, p2, p3}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 10
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const-wide/16 v8, 0xbb8

    const/4 v7, 0x0

    const/4 v5, 0x1

    .line 219
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v2

    .line 221
    .local v2, "notiName":Ljava/lang/String;
    const-string v3, "PREPARE_SLIDE_SHOW_DATA"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 222
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->addToSildeProxy()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    .line 259
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    const-string v3, "EXIT_SELECTION_MODE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 224
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->exitSelectionMode()V

    goto :goto_0

    .line 225
    :cond_2
    const-string v3, "UPDATE_CATEGORY"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 227
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->initHandler()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    .line 228
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 229
    .local v1, "msg":Landroid/os/Message;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 230
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I

    move-result v3

    const/16 v4, 0xa

    if-le v3, v4, :cond_4

    .line 231
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I
    invoke-static {v3, v7}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$102(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)I

    .line 232
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 234
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # operator++ for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mRemoveCountUpdateMessage:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$108(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I

    .line 235
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->removeMessages(I)V

    .line 236
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v1, v8, v9}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 239
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    move-result-object v3

    invoke-virtual {v3, v1, v8, v9}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 242
    .end local v1    # "msg":Landroid/os/Message;
    :cond_6
    const-string v3, "REFRESH_SELECTION"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 243
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    move-result-object v3

    if-eqz v3, :cond_7

    .line 244
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->cancel(Z)Z

    .line 246
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    new-instance v4, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$502(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    .line 247
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 248
    :cond_8
    const-string v3, "USER_TAG_LOADING_FINISHED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 249
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 250
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->refreshTagList()V

    .line 251
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/widget/ExpandableListView;

    move-result-object v3

    if-eqz v3, :cond_9

    .line 252
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mTagCloudListAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/TagCloudExpandableListAdapter;->getGroupCount()I

    move-result v3

    if-ge v0, v3, :cond_9

    .line 253
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mListStyleTagCloudView:Landroid/widget/ExpandableListView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$800(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Landroid/widget/ExpandableListView;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/ExpandableListView;->expandGroup(I)Z

    .line 252
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 257
    .end local v0    # "i":I
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->updateNoItemViewTopMargin()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V

    goto/16 :goto_0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "PREPARE_SLIDE_SHOW_DATA"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "REFRESH_SELECTION"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "EXIT_SELECTION_MODE"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "UPDATE_CATEGORY"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "USER_TAG_LOADING_FINISHED"

    aput-object v2, v0, v1

    return-object v0
.end method
