.class public Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;
.super Lcom/sec/samsung/gallery/view/common/DragShadow;
.source "SinglePhotoDragShadow.java"


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;II)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "bmp"    # Landroid/graphics/Bitmap;
    .param p4, "widthScreen"    # I
    .param p5, "heightScreen"    # I

    .prologue
    .line 11
    const/4 v5, 0x0

    invoke-direct {p0, p1, p2, p3, v5}, Lcom/sec/samsung/gallery/view/common/DragShadow;-><init>(Landroid/view/View;Landroid/content/Context;Landroid/graphics/Bitmap;Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 13
    const/high16 v4, 0x40400000    # 3.0f

    .line 14
    .local v4, "rate":F
    const/16 v2, 0x96

    .line 16
    .local v2, "minWidth":I
    invoke-static {p4, p5}, Ljava/lang/Math;->min(II)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v5, v6

    float-to-int v3, v5

    .line 17
    .local v3, "minWidthOrHeight":I
    const/16 v5, 0x96

    if-ge v3, v5, :cond_0

    .line 18
    const/16 v3, 0x96

    .line 20
    :cond_0
    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-double v6, v5

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-double v8, v5

    div-double v0, v6, v8

    .line 22
    .local v0, "coef":D
    iput v3, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mWidth:I

    .line 23
    int-to-double v6, v3

    div-double/2addr v6, v0

    double-to-int v5, v6

    iput v5, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mHeight:I

    .line 24
    return-void
.end method


# virtual methods
.method public onProvideShadowMetrics(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 2
    .param p1, "size"    # Landroid/graphics/Point;
    .param p2, "touch"    # Landroid/graphics/Point;

    .prologue
    .line 28
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mWidth:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mHeight:I

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 29
    iget v0, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mWidth:I

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/sec/samsung/gallery/view/common/SinglePhotoDragShadow;->mHeight:I

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 30
    return-void
.end method
