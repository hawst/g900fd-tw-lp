.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;
.super Ljava/lang/Object;
.source "GallerySearchActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->setTitle(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

.field final synthetic val$numberOfItemsSelected:I


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;I)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    iput p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 319
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isFinishing()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 340
    :cond_0
    :goto_0
    return-void

    .line 321
    :cond_1
    const/4 v0, 0x0

    .line 322
    .local v0, "numberOfSelectedItems":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 323
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$300(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->invalidateOptionsMenu()V

    .line 325
    :cond_2
    const/4 v1, 0x0

    .line 326
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$400(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 327
    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-ne v2, v5, :cond_3

    .line 328
    const v2, 0x7f0e004f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->setSelectAllButtonTitle(ILjava/lang/String;)V
    invoke-static {v2, v3, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$500(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;ILjava/lang/String;)V

    .line 337
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    if-lez v2, :cond_0

    .line 338
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;->access$200(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit;)Landroid/view/Menu;

    move-result-object v2

    const v3, 0x7f0f02d6

    invoke-interface {v2, v3, v5}, Landroid/view/Menu;->setGroupEnabled(IZ)V

    goto :goto_0

    .line 331
    :cond_3
    const v2, 0x7f0e004e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    iget v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBarForEdit$1;->val$numberOfItemsSelected:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method
