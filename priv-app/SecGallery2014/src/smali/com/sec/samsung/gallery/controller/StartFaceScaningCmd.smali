.class public Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartFaceScaningCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static volatile mIsScanTriggered:Z

.field private static scanCount:I


# instance fields
.field private action:Ljava/lang/String;

.field private currentReceiverStatus:I

.field private mContext:Landroid/content/Context;

.field private mData:Landroid/content/Intent;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    sput-boolean v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mIsScanTriggered:Z

    .line 40
    sput v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->currentReceiverStatus:I

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    return-void
.end method

.method private getTotalFaceCount()I
    .locals 9

    .prologue
    .line 157
    const/4 v7, 0x0

    .line 158
    .local v7, "count":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 159
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v8, 0x0

    .line 160
    .local v8, "faceCursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getCloudIds(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v6

    .line 162
    .local v6, "cloudIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    :try_start_0
    sget-object v1, Lcom/sec/samsung/gallery/access/face/FaceData;->FACES_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "image_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "recommended_id desc,group_id desc"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 167
    if-eqz v8, :cond_2

    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 170
    :cond_0
    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 171
    add-int/lit8 v7, v7, 0x1

    .line 173
    :cond_1
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 177
    :cond_2
    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    .line 179
    return v7

    .line 177
    :catchall_0
    move-exception v1

    invoke-static {v8}, Lcom/sec/android/gallery3d/common/Utils;->closeSilently(Landroid/database/Cursor;)V

    throw v1
.end method

.method private startScaning()V
    .locals 18

    .prologue
    .line 56
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v6

    .line 57
    .local v6, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 58
    .local v8, "galleryApp":Lcom/sec/android/gallery3d/app/GalleryAppImpl;
    const-string v1, "reload"

    const-string v2, "onReceive"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    const-string v2, "com.android.media.FACE_SCANNER_STARTED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 60
    const-string v1, "reload"

    const-string v2, "ACTION_FACE_SCANNER_STARTED"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->currentReceiverStatus:I

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    const-string v2, "com.android.media.FACE_SCANNER_PROGRESS"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 63
    const-string v1, "reload"

    const-string v2, "ACTION_FACE_SCANNER_PROGRESS"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->currentReceiverStatus:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 65
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->currentReceiverStatus:I

    .line 66
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->getTotalFaceCount()I

    move-result v1

    sput v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    .line 70
    :cond_2
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isArcMode()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 71
    instance-of v1, v6, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v1, :cond_3

    instance-of v1, v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v1, :cond_4

    .line 72
    :cond_3
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty()V

    .line 76
    :cond_4
    if-eqz v6, :cond_0

    instance-of v1, v6, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    if-eqz v1, :cond_0

    .line 77
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mData:Landroid/content/Intent;

    const-string/jumbo v2, "uris"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 78
    .local v15, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v15, :cond_0

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 79
    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :cond_5
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/String;

    .line 80
    .local v13, "uri":Ljava/lang/String;
    invoke-static {v13}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v14

    .line 81
    .local v14, "uriData":Landroid/net/Uri;
    const-string v1, "content"

    invoke-virtual {v14}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 82
    invoke-virtual {v14}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v11

    .line 83
    .local v11, "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v7

    .line 84
    .local v7, "cnt":I
    if-lez v7, :cond_5

    .line 85
    const/4 v1, 0x2

    invoke-interface {v11, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v10

    .line 86
    .local v10, "imageId":I
    const-string v1, "FACESCANING"

    const-string v2, "notify file uri = "

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v6

    .line 87
    check-cast v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v1, v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->notifyFaceTagUpdate(I)V

    goto :goto_1

    .line 93
    .end local v7    # "cnt":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "imageId":I
    .end local v11    # "list":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "uri":Ljava/lang/String;
    .end local v14    # "uriData":Landroid/net/Uri;
    .end local v15    # "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    const-string v2, "com.android.media.FACE_SCANNER_FINISHED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    const-string v2, "com.android.media.FACE_SCANNER_STOPPED"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 94
    :cond_7
    const-string v1, "reload"

    const-string v2, "ACTION_FACE_SCANNER_FINISHED"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/util/ArcLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 95
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/LoadImageFaceRectTask;->start(Landroid/content/Context;)V

    .line 96
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->isArcMode()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 97
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->currentReceiverStatus:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_8

    .line 98
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->getTotalFaceCount()I

    move-result v1

    sget v2, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    sub-int/2addr v1, v2

    sput v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    .line 101
    :cond_8
    sget v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    if-lez v1, :cond_9

    .line 102
    sget-boolean v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mIsScanTriggered:Z

    if-eqz v1, :cond_9

    .line 103
    sget v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_c

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00cb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 106
    .local v12, "toastStr":Ljava/lang/String;
    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-static {v1, v12}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 110
    .end local v12    # "toastStr":Ljava/lang/String;
    :cond_9
    if-eqz v6, :cond_b

    .line 111
    instance-of v1, v6, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v1, :cond_a

    instance-of v1, v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-nez v1, :cond_a

    instance-of v1, v6, Lcom/sec/samsung/gallery/view/noitemview/NoItemViewState;

    if-eqz v1, :cond_d

    .line 114
    :cond_a
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty()V

    .line 120
    :cond_b
    :goto_3
    const/4 v1, 0x0

    sput v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    .line 121
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mIsScanTriggered:Z

    goto/16 :goto_0

    .line 103
    :cond_c
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e00cc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/16 v16, 0x0

    sget v17, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->scanCount:I

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v3, v16

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    goto :goto_2

    .line 116
    :cond_d
    instance-of v1, v6, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    if-eqz v1, :cond_b

    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDCM:Z

    if-nez v1, :cond_b

    .line 117
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Lcom/sec/android/gallery3d/app/ActivityState;->onDirty(Z)V

    goto :goto_3

    .line 122
    :cond_e
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v1, :cond_0

    .line 123
    if-eqz v6, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 126
    instance-of v1, v6, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-eqz v1, :cond_f

    .line 127
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v4

    .line 128
    .local v4, "albumThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    const/4 v5, 0x0

    .line 135
    .local v5, "photoThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    :goto_4
    new-instance v1, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;

    const-string v3, "RefreshFaceThread"

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;Ljava/lang/String;Lcom/sec/samsung/gallery/model/ThumbnailProxy;Lcom/sec/samsung/gallery/model/ThumbnailProxy;Lcom/sec/android/gallery3d/app/ActivityState;)V

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd$1;->start()V

    goto/16 :goto_0

    .line 129
    .end local v4    # "albumThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .end local v5    # "photoThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    :cond_f
    instance-of v1, v6, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    if-eqz v1, :cond_0

    .line 130
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v4

    .line 131
    .restart local v4    # "albumThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPhotoThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v5

    .restart local v5    # "photoThumbnailProxy":Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    goto :goto_4
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 2
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 46
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Ljava/lang/Object;

    move-object v0, v1

    check-cast v0, [Ljava/lang/Object;

    .line 47
    .local v0, "params":[Ljava/lang/Object;
    const/4 v1, 0x0

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mContext:Landroid/content/Context;

    .line 48
    const/4 v1, 0x1

    aget-object v1, v0, v1

    check-cast v1, Landroid/content/Intent;

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mData:Landroid/content/Intent;

    .line 49
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mData:Landroid/content/Intent;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->mData:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->action:Ljava/lang/String;

    .line 51
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/StartFaceScaningCmd;->startScaning()V

    .line 53
    :cond_0
    return-void
.end method
