.class Lcom/sec/samsung/gallery/view/allview/AllViewState$6;
.super Ljava/lang/Object;
.source "AllViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/allview/AllViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/allview/AllViewState;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V
    .locals 10
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 363
    packed-switch p2, :pswitch_data_0

    .line 395
    :cond_0
    :goto_0
    return-void

    .line 365
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v4

    .line 366
    .local v4, "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v5

    .line 367
    .local v5, "lastIndex":I
    add-int v6, v4, v5

    div-int/lit8 v0, v6, 0x2

    .line 368
    .local v0, "bgIndex":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)I

    move-result v6

    if-eq v6, v0, :cond_0

    .line 369
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # setter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I
    invoke-static {v6, v0}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1902(Lcom/sec/samsung/gallery/view/allview/AllViewState;I)I

    .line 370
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoView:Lcom/sec/samsung/gallery/glview/GlPhotoView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1600(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mBgImageIndex:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$1900(Lcom/sec/samsung/gallery/view/allview/AllViewState;)I

    move-result v8

    const/4 v9, 0x2

    invoke-virtual {v7, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 376
    .end local v0    # "bgIndex":I
    .end local v4    # "firstIndex":I
    .end local v5    # "lastIndex":I
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v4

    .line 377
    .restart local v4    # "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v5

    .line 378
    .restart local v5    # "lastIndex":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2000(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v6

    if-nez v6, :cond_2

    .line 379
    const/4 v3, 0x0

    .line 381
    .local v3, "dateText":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-eqz v6, :cond_1

    .line 382
    new-instance v1, Ljava/util/Date;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mAdapter:Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v6

    invoke-direct {v1, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 383
    .local v1, "date":Ljava/util/Date;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2100(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v6

    invoke-static {v6}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    .line 384
    .local v2, "dateFormat":Ljava/text/DateFormat;
    invoke-virtual {v2, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 386
    .end local v1    # "date":Ljava/util/Date;
    .end local v2    # "dateFormat":Ljava/text/DateFormat;
    :cond_1
    if-eqz v3, :cond_2

    .line 387
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2200(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v6

    invoke-virtual {v6, v3}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 390
    .end local v3    # "dateText":Ljava/lang/String;
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getAdapter()Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 391
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/allview/AllViewState$6;->this$0:Lcom/sec/samsung/gallery/view/allview/AllViewState;

    # getter for: Lcom/sec/samsung/gallery/view/allview/AllViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/allview/AllViewState;->access$2300(Lcom/sec/samsung/gallery/view/allview/AllViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setActiveWindow(II)V

    goto/16 :goto_0

    .line 363
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
