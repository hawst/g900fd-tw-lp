.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;
.super Ljava/lang/Object;
.source "HelpDetailViewState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->enableDnieMode(ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

.field final synthetic val$isEnable:Z

.field final synthetic val$params:Ljava/lang/Object;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;ZLjava/lang/Object;)V
    .locals 0

    .prologue
    .line 758
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    iput-boolean p2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->val$isEnable:Z

    iput-object p3, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->val$params:Ljava/lang/Object;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 761
    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$3100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "enableDnieMode ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->val$isEnable:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "] with delay"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "SET_DNIE_MODE"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;->val$params:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 763
    return-void
.end method
