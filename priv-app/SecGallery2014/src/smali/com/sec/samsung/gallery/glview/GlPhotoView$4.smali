.class Lcom/sec/samsung/gallery/glview/GlPhotoView$4;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationDone()V
    .locals 5

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 810
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mConvSetAnim:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$102(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;)Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    .line 811
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnConvAnim:Z
    invoke-static {v1, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$402(Lcom/sec/samsung/gallery/glview/GlPhotoView;Z)Z

    .line 812
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iput-boolean v4, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mLockPos:Z

    .line 814
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 815
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    move-result-object v1

    iput v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 816
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$500(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    move-result-object v1

    iput-object v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 817
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$502(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;)Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 819
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    .line 820
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;
    if-eqz v0, :cond_1

    .line 821
    iput v3, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    .line 822
    iput v3, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 824
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 826
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v1, :cond_2

    .line 827
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->startExpandAnimation()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    .line 830
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_TYPE_ALBUM:I

    sget v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->requestAnimation(IIZ)V

    .line 831
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$4;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->ANIM_PRECOND_THUMB:I

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->checkStartAnimation(I)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$700(Lcom/sec/samsung/gallery/glview/GlPhotoView;I)V

    .line 832
    return-void
.end method
