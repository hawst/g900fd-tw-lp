.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;
.super Ljava/lang/Object;
.source "PhotoViewBeam.java"

# interfaces
.implements Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$OnGetFilePathListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->setBeamListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;)V
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetFilePath()[Landroid/net/Uri;
    .locals 6

    .prologue
    .line 30
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 31
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 32
    .local v0, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onGetFilePath(). Selected item count= "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam$1;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;->access$000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewBeam;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/controller/AndroidBeamCmd$AndroidBeamUtils;->toUriPathArray(Landroid/content/Context;Ljava/util/List;)[Landroid/net/Uri;

    move-result-object v2

    .line 36
    .local v2, "uriPathArray":[Landroid/net/Uri;
    return-object v2
.end method
