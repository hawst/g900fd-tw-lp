.class public Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;
.super Lcom/sec/samsung/gallery/glview/GlScrollBar;
.source "GlGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AlbumViewScrollBar"
.end annotation


# static fields
.field public static final SCROLL_MOVEMENT_HORIZONTAL:I = 0x1

.field public static final SCROLL_MOVEMENT_VERTICAL:I


# instance fields
.field private mCanvasColor:I

.field private mDefZ:F

.field private mFaceThickness:F

.field private mHeightSpace:F

.field private mMinFaceLength:F

.field private mMovementDirection:I

.field private mPaddingRight:F

.field private mTop:F

.field private mTrackColor:I

.field private mTrackLength:F

.field private mWidthSpace:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 1
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 1909
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    .line 1910
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 1897
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mDefZ:F

    .line 1903
    const v0, -0xff01

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackColor:I

    .line 1904
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMinFaceLength:F

    .line 1906
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMovementDirection:I

    .line 1907
    const v0, -0xca9889

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mCanvasColor:I

    .line 1912
    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 7
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "textureWidth"    # I

    .prologue
    .line 1915
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 1917
    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 2
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "textureWidth"    # I
    .param p6, "textureHeight"    # I

    .prologue
    .line 1919
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    .line 1921
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 1897
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mDefZ:F

    .line 1903
    const v0, -0xff01

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackColor:I

    .line 1904
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMinFaceLength:F

    .line 1906
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMovementDirection:I

    .line 1907
    const v0, -0xca9889

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mCanvasColor:I

    .line 1922
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p5, p6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 1923
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar$1;

    invoke-direct {v0, p0, p1}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar$1;-><init>(Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;Lcom/sec/samsung/gallery/glview/GlGridView;)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1930
    const v0, -0xbb8372

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setBorderColor(I)V

    .line 1931
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setBorderWidth(F)V

    .line 1932
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setBorderVisible(Z)V

    .line 1933
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setAlpha(F)V

    .line 1935
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mWidthSpace:F

    .line 1936
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    .line 1938
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    .line 1939
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewScrollbarWidth()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mFaceThickness:F

    .line 1940
    return-void
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;

    .prologue
    .line 1893
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mCanvasColor:I

    return v0
.end method

.method private getHeight(F)F
    .locals 8
    .param p1, "range"    # F

    .prologue
    const/4 v4, 0x0

    .line 1989
    sget v5, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->THRESHOLD:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    mul-float v0, v5, v6

    .line 1990
    .local v0, "T":F
    cmpl-float v5, p1, v0

    if-lez v5, :cond_1

    .line 1991
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMinFaceLength:F

    .line 1999
    :cond_0
    :goto_0
    return v3

    .line 1994
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMinFaceLength:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    sub-float/2addr v5, v6

    div-float v1, v5, v0

    .line 1995
    .local v1, "a":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    .line 1996
    .local v2, "b":F
    mul-float v5, v1, p1

    add-float v3, v5, v2

    .line 1997
    .local v3, "result":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    sub-float/2addr v5, v3

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    const/high16 v7, 0x41400000    # 12.0f

    div-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 1998
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1999
    :cond_2
    cmpl-float v5, p1, v4

    if-gtz v5, :cond_0

    move v3, v4

    goto :goto_0
.end method


# virtual methods
.method public reset(FFF)V
    .locals 3
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "top"    # F

    .prologue
    .line 1944
    const-string v0, "GlGridView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset : width = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mWidthSpace:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1945
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mWidthSpace:F

    .line 1946
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    .line 1947
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTop:F

    .line 1948
    return-void
.end method

.method public setDefZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 1952
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mDefZ:F

    .line 1953
    return-void
.end method

.method public setMaxHeight(F)V
    .locals 0
    .param p1, "maxHeight"    # F

    .prologue
    .line 1980
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTrackLength:F

    .line 1981
    return-void
.end method

.method public setMinHeight(F)V
    .locals 0
    .param p1, "minHeight"    # F

    .prologue
    .line 1985
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mMinFaceLength:F

    .line 1986
    return-void
.end method

.method public setRightPadding(F)V
    .locals 0
    .param p1, "padding"    # F

    .prologue
    .line 1957
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mPaddingRight:F

    .line 1958
    return-void
.end method

.method public update(FFF)V
    .locals 10
    .param p1, "scroll"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 1962
    sub-float v5, p3, p2

    .line 1963
    .local v5, "range":F
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->getHeight(F)F

    move-result v4

    .line 1964
    .local v4, "height":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGridView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v7

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mFaceThickness:F

    mul-float v3, v7, v8

    .line 1965
    .local v3, "glFaceThickness":F
    invoke-virtual {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setSize(FF)V

    .line 1967
    const/4 v7, 0x0

    cmpg-float v7, v5, v7

    if-gtz v7, :cond_0

    .line 1976
    :goto_0
    return-void

    .line 1971
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mHeightSpace:F

    .line 1972
    .local v0, "H":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mTop:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mBottom:F

    add-float/2addr v7, v8

    add-float/2addr v7, v4

    sub-float/2addr v7, v0

    div-float v1, v7, v5

    .line 1973
    .local v1, "a":F
    neg-float v7, v0

    div-float/2addr v7, v9

    div-float v8, v4, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mBottom:F

    add-float/2addr v7, v8

    mul-float v8, p3, v1

    sub-float v2, v7, v8

    .line 1974
    .local v2, "b":F
    mul-float v7, v1, p1

    add-float v6, v7, v2

    .line 1975
    .local v6, "y":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mWidthSpace:F

    div-float/2addr v7, v9

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mWidth:F

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    sub-float/2addr v7, v9

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->mDefZ:F

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    invoke-virtual {p0, v7, v6, v8}, Lcom/sec/samsung/gallery/glview/GlGridView$AlbumViewScrollBar;->setPos(FFF)V

    goto :goto_0
.end method
