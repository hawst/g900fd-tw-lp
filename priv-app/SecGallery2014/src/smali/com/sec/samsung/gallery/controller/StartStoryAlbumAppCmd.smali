.class public Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "StartStoryAlbumAppCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final STORYALBUM_CLASS_NAME:Ljava/lang/String; = "com.samsung.android.app.episodes.MainActivity"

.field private static final STORYALBUM_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.episodes"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method public static getStoryAlbumMaxItemCount(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 44
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string/jumbo v1, "storyalbum_max_photo_limit"

    const/16 v2, 0x96

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private startStoryAlbum(Landroid/app/Activity;)V
    .locals 11
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 48
    move-object v9, p1

    check-cast v9, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v8

    .line 50
    .local v8, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v9

    if-nez v9, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    const-string v9, "intent.action.SEND_MULTIPLE_TO_STORYALBUM"

    invoke-direct {v4, v9}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 55
    .local v4, "intent":Landroid/content/Intent;
    const-string/jumbo v9, "type"

    const/4 v10, 0x3

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 56
    invoke-virtual {v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalItemList()Ljava/util/LinkedList;

    move-result-object v7

    .line 57
    .local v7, "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/LinkedList;->size()I

    move-result v9

    invoke-direct {v6, v9}, Ljava/util/ArrayList;-><init>(I)V

    .line 58
    .local v6, "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v7}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 59
    .local v5, "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getMediaType()I

    move-result v9

    const/4 v10, 0x4

    if-eq v9, v10, :cond_2

    instance-of v9, v5, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v9, :cond_3

    move-object v0, v5

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v9, v0

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v9

    if-nez v9, :cond_2

    .line 63
    :cond_3
    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 71
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v4    # "intent":Landroid/content/Intent;
    .end local v5    # "obj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v6    # "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v7    # "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :catch_0
    move-exception v1

    .line 73
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    :try_start_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 74
    .restart local v4    # "intent":Landroid/content/Intent;
    const-string v9, "com.samsung.android.app.episodes"

    const-string v10, "com.samsung.android.app.episodes.MainActivity"

    invoke-virtual {v4, v9, v10}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    invoke-virtual {p1, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 76
    .end local v4    # "intent":Landroid/content/Intent;
    :catch_1
    move-exception v2

    .line 77
    .local v2, "er":Landroid/content/ActivityNotFoundException;
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;->mContext:Landroid/content/Context;

    const v10, 0x7f0e02bc

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 65
    .end local v1    # "e":Landroid/content/ActivityNotFoundException;
    .end local v2    # "er":Landroid/content/ActivityNotFoundException;
    .restart local v3    # "i$":Ljava/util/Iterator;
    .restart local v4    # "intent":Landroid/content/Intent;
    .restart local v6    # "selectedList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .restart local v7    # "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_4
    :try_start_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 66
    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;->mContext:Landroid/content/Context;

    const v10, 0x7f0e0107

    invoke-static {v9, v10}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 69
    :cond_5
    const-string v9, "selectedItems"

    invoke-virtual {v4, v9, v6}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 70
    invoke-virtual {p1, v4}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 36
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 37
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;->mContext:Landroid/content/Context;

    .line 39
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/controller/StartStoryAlbumAppCmd;->startStoryAlbum(Landroid/app/Activity;)V

    .line 40
    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "EXIT_SELECTION_MODE"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 41
    return-void
.end method
