.class Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;
.super Ljava/lang/Object;
.source "EventActionBarForEdit.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

.field final synthetic val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

.field final synthetic val$toggleImage:Z


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;Lcom/sec/samsung/gallery/util/Consts$ButtonType;Z)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    iput-boolean p3, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$toggleImage:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$button:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    if-ne v0, v1, :cond_0

    .line 193
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$toggleImage:Z

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget v2, v2, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mQuantitySelectedAlbum:I

    # invokes: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->updatePopupMenuItemsVisibility(ZI)V
    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$400(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;ZI)V

    .line 194
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$500(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mSelectionModeBar:Lcom/sec/samsung/gallery/view/common/SelectionModeBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$600(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/samsung/gallery/view/common/SelectionModeBar;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/common/SelectionModeBar;->setChecked(Z)V

    .line 200
    :cond_0
    :goto_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$700(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v3, :cond_1

    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    # getter for: Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->access$800(Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;)Landroid/view/Menu;

    move-result-object v0

    const v1, 0x7f0f026e

    invoke-static {v0, v1, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 204
    :cond_1
    return-void

    .line 196
    :cond_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSelectionOnActionBar:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->this$0:Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit$2;->val$toggleImage:Z

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;->setCheckboxState(Z)V

    goto :goto_0
.end method
