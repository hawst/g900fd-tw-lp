.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;
.super Ljava/lang/Object;
.source "SlideshowDefaultMusicDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 157
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 158
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->stopMusic()V

    .line 159
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .line 161
    .local v0, "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->isAddMusic()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 162
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 163
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v2

    const-string v3, "SLIDESHOW_PICK_MUSIC"

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 180
    :cond_1
    :goto_0
    return-void

    .line 168
    :cond_2
    if-ltz p2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListAdapter:Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getCount()I

    move-result v2

    if-ge p2, v2, :cond_1

    .line 169
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    if-nez v2, :cond_3

    .line 170
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    new-instance v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$002(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    .line 172
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getUri()Landroid/net/Uri;

    move-result-object v3

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$402(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;Landroid/net/Uri;)Landroid/net/Uri;

    .line 173
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 174
    .local v1, "uriString":Ljava/lang/String;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->isMusic()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 175
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->preparePlayWithUri(Landroid/content/Context;Ljava/lang/String;)V

    .line 176
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->setMusicLooping(Z)V

    .line 177
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$1;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$000(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->playMusic()V

    goto :goto_0
.end method
