.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;
.super Lcom/sec/samsung/gallery/view/AbstractActionBarView;
.source "TimeViewActionBarForPick.java"


# instance fields
.field private mIsFocusInited:Z


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 34
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;I)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mIsFocusInited:Z

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick$1;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method private setTextOnlyButton(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 123
    const v1, 0x7f0f0288

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 124
    .local v0, "cancel":Landroid/view/MenuItem;
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->setTextOnlyButton(Landroid/view/MenuItem;)V

    .line 125
    return-void
.end method


# virtual methods
.method protected isPickMode()Z
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 68
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 69
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f12000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 71
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->setTextOnlyButton(Landroid/view/Menu;)V

    .line 72
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 97
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 120
    :goto_0
    return-void

    .line 99
    :sswitch_0
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_HIDDEN_ALBUMS:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 102
    :sswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->setResult(I)V

    .line 103
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->finish()V

    goto :goto_0

    .line 106
    :sswitch_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "START_CAMERA"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :sswitch_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_CONTENT_TO_DISPLAY_DIALOG"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 113
    :sswitch_4
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/android/gallery3d/settings/AccountSettingActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 114
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 117
    .end local v0    # "intent":Landroid/content/Intent;
    :sswitch_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->goToHelpGallery(Landroid/content/Context;)V

    goto :goto_0

    .line 97
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0f0251 -> :sswitch_2
        0x7f0f0257 -> :sswitch_4
        0x7f0f0287 -> :sswitch_0
        0x7f0f0288 -> :sswitch_1
        0x7f0f0289 -> :sswitch_3
        0x7f0f028a -> :sswitch_5
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 76
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v4, v3}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 77
    const v4, 0x7f0f0286

    invoke-interface {p1, v4, v2}, Landroid/view/Menu;->setGroupVisible(IZ)V

    .line 79
    const v4, 0x7f0f0287

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->checkShowHiddenMenu(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v5

    invoke-static {p1, v4, v5}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 80
    const v4, 0x7f0f0288

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v5

    if-nez v5, :cond_1

    :goto_0
    invoke-static {p1, v4, v2}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 81
    const v2, 0x7f0f0257

    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 82
    const v2, 0x7f0f028a

    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 83
    const v2, 0x7f0f0289

    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 85
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mIsFocusInited:Z

    if-nez v2, :cond_0

    .line 86
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 87
    .local v0, "actionBar":Landroid/app/ActionBar;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 88
    .local v1, "actionView":Landroid/view/View;
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->requestFocus()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->mIsFocusInited:Z

    .line 92
    .end local v0    # "actionBar":Landroid/app/ActionBar;
    .end local v1    # "actionView":Landroid/view/View;
    :cond_0
    const v2, 0x7f0f0251

    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 93
    return-void

    :cond_1
    move v2, v3

    .line 80
    goto :goto_0

    .line 87
    .restart local v0    # "actionBar":Landroid/app/ActionBar;
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected setupButtons()V
    .locals 1

    .prologue
    .line 61
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForPick;->showHomeButtonOnTopLeft()V

    .line 64
    :cond_0
    return-void
.end method
