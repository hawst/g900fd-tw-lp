.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2673
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAllContentReady()V
    .locals 0

    .prologue
    .line 2699
    return-void
.end method

.method public onContentChanged(II)V
    .locals 0
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 2695
    return-void
.end method

.method public onSizeChanged(I)V
    .locals 6
    .param p1, "size"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2676
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeMessages(I)V

    .line 2677
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMainHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2700(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/os/Handler;

    move-result-object v1

    const-wide/16 v2, 0x3c

    invoke-virtual {v1, v5, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2678
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getCount()I

    move-result v0

    .line 2679
    .local v0, "totalCount":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    if-lt v1, v0, :cond_0

    .line 2680
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput v4, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mCurrentMediaItemIndex:I

    .line 2682
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2683
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_1

    .line 2684
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->tempTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1400(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v2

    monitor-enter v2

    .line 2685
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    new-instance v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;-><init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$1;)V

    # setter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1302(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    .line 2686
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mHandleDismissDialogsTask:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$HandleDismissDialogsTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 2687
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2690
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$15;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->postUpdateAlbumNameOfActionBar()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2900(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V

    .line 2691
    return-void

    .line 2687
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
