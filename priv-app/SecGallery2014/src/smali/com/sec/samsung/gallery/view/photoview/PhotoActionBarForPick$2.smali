.class Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;
.super Ljava/lang/Object;
.source "PhotoActionBarForPick.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->setTitle(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

.field final synthetic val$albumName:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->val$albumName:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setTitleOnCustomView(Ljava/lang/String;)V
    .locals 3
    .param p1, "albumName"    # Ljava/lang/String;

    .prologue
    .line 103
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const v2, 0x7f0f0016

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 104
    .local v0, "albumNameView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    :cond_0
    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 95
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->val$albumName:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->setTitleOnCustomView(Ljava/lang/String;)V

    .line 100
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;->access$500(Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick;)Landroid/app/ActionBar;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoActionBarForPick$2;->val$albumName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
