.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;
.super Ljava/lang/Object;
.source "HelpDetailViewState.java"

# interfaces
.implements Landroid/view/View$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setGenericFocusIndex(I)V

    .line 356
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 357
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    move-result-object v1

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 358
    :cond_1
    const/4 v0, 0x1

    return v0
.end method
