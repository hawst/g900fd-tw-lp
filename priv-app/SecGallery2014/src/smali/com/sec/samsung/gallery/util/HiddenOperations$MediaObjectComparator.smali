.class Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;
.super Ljava/lang/Object;
.source "HiddenOperations.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/HiddenOperations;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MediaObjectComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/sec/android/gallery3d/data/MediaObject;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/util/HiddenOperations$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/util/HiddenOperations$1;

    .prologue
    .line 111
    invoke-direct {p0}, Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I
    .locals 8
    .param p1, "object1"    # Lcom/sec/android/gallery3d/data/MediaObject;
    .param p2, "object2"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 113
    instance-of v4, p1, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    instance-of v4, p2, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 114
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v2

    .line 115
    .local v2, "setPath1":Ljava/lang/String;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPathOnFileSystem()Ljava/lang/String;

    move-result-object v3

    .line 116
    .local v3, "setPath2":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    .line 145
    .end local v2    # "setPath1":Ljava/lang/String;
    .end local v3    # "setPath2":Ljava/lang/String;
    :goto_0
    return v4

    .line 117
    .restart local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    instance-of v4, p1, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_8

    instance-of v4, p2, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v4, :cond_8

    move-object v4, p1

    .line 119
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .local v0, "itemPath1":Ljava/lang/String;
    move-object v4, p2

    .line 120
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "itemPath2":Ljava/lang/String;
    if-nez v0, :cond_1

    if-nez v1, :cond_1

    move v4, v5

    .line 123
    goto :goto_0

    .line 124
    :cond_1
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    move v4, v6

    .line 125
    goto :goto_0

    .line 126
    :cond_2
    if-eqz v0, :cond_3

    if-nez v1, :cond_3

    move v4, v7

    .line 127
    goto :goto_0

    .line 129
    :cond_3
    const-string v4, "/"

    invoke-virtual {v0, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v0, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 130
    const-string v4, "/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v1, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 132
    if-nez v0, :cond_4

    if-nez v1, :cond_4

    move v4, v5

    .line 133
    goto :goto_0

    .line 134
    :cond_4
    if-nez v0, :cond_5

    if-eqz v1, :cond_5

    move v4, v6

    .line 135
    goto :goto_0

    .line 136
    :cond_5
    if-eqz v0, :cond_6

    if-nez v1, :cond_6

    move v4, v7

    .line 137
    goto :goto_0

    .line 139
    :cond_6
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    if-nez v4, :cond_7

    .line 140
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    check-cast p2, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .line 142
    .restart local p1    # "object1":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local p2    # "object2":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    sub-int/2addr v4, v5

    goto :goto_0

    .end local v0    # "itemPath1":Ljava/lang/String;
    .end local v1    # "itemPath2":Ljava/lang/String;
    :cond_8
    move v4, v5

    .line 145
    goto :goto_0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 111
    check-cast p1, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/sec/android/gallery3d/data/MediaObject;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/util/HiddenOperations$MediaObjectComparator;->compare(Lcom/sec/android/gallery3d/data/MediaObject;Lcom/sec/android/gallery3d/data/MediaObject;)I

    move-result v0

    return v0
.end method
