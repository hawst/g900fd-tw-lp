.class public Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
.super Ljava/lang/Object;
.source "Clusterkraf.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;
.implements Lcom/google/android/gms/maps/GoogleMap$OnMapLongClickListener;
.implements Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$3;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$UpdateClustersAndTransitionClusteringTaskHost;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static final THUMBNAIL_FADEOUT_ANIMATION_NO_DURATION:I

.field private static currentClusterPointsByMarker:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field

.field private static mContext:Landroid/content/Context;

.field static mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

.field static mMapViewLayout:Landroid/view/View;

.field private static mMediaItemList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation
.end field

.field private static mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

.field private static options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;


# instance fields
.field private final THUMBNAIL_FADEOUT_ANIMATION_DURATION:I

.field private clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

.field private clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

.field private currentClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field

.field private currentMarkers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field

.field private final innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

.field private final mapRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/maps/GoogleMap;",
            ">;"
        }
    .end annotation
.end field

.field private final points:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private previousClusters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field

.field private previousMarkers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/gms/maps/model/Marker;",
            ">;"
        }
    .end annotation
.end field

.field private final transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->TAG:Ljava/lang/String;

    .line 71
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMediaItemList:Ljava/util/ArrayList;

    .line 79
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;Ljava/util/ArrayList;Landroid/content/Context;)V
    .locals 2
    .param p1, "map"    # Lcom/google/android/gms/maps/GoogleMap;
    .param p2, "options"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    .param p4, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/maps/GoogleMap;",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/Options;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    .line 91
    .local p3, "points":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    .line 80
    const/16 v0, 0xfa

    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->THUMBNAIL_FADEOUT_ANIMATION_DURATION:I

    .line 92
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    .line 93
    sput-object p2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    .line 94
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    .line 95
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;-><init>(Lcom/google/android/gms/maps/GoogleMap;Lcom/sec/samsung/gallery/mapfragment/clustering/Options;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$Host;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    .line 96
    sput-object p4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    .line 98
    if-eqz p3, :cond_0

    .line 99
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 102
    :cond_0
    if-eqz p1, :cond_1

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMarkerClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMarkerClickListener;)V

    .line 105
    invoke-virtual {p1, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapClickListener;)V

    .line 106
    invoke-virtual {p1, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnMapLongClickListener(Lcom/google/android/gms/maps/GoogleMap$OnMapLongClickListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/GoogleMap;->setOnInfoWindowClickListener(Lcom/google/android/gms/maps/GoogleMap$OnInfoWindowClickListener;)V

    .line 110
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->showAllClusters()V

    .line 111
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->updateClustersAndTransition()V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->removePreviousMarkers()V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->drawMarkers()V

    return-void
.end method

.method static synthetic access$1300()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1400()Landroid/content/Context;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/google/android/gms/maps/model/Marker;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->showMarkerThumbnail(Lcom/google/android/gms/maps/model/Marker;)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/lang/ref/WeakReference;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousClusters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->showAllClusters()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/google/android/gms/maps/Projection;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Lcom/google/android/gms/maps/Projection;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->startClusterTransitionsBuildingTask(Lcom/google/android/gms/maps/Projection;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .prologue
    .line 59
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionClusters(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    return-object v0
.end method

.method static synthetic access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    return-object p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    return-object v0
.end method

.method static synthetic access$802(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    return-object p1
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    return-object v0
.end method

.method private drawMarkers()V
    .locals 8

    .prologue
    .line 185
    iget-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v6}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/GoogleMap;

    .line 186
    .local v2, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v2, :cond_1

    iget-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    if-eqz v6, :cond_1

    .line 187
    new-instance v6, Ljava/util/ArrayList;

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    .line 188
    new-instance v6, Ljava/util/HashMap;

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v6, v7}, Ljava/util/HashMap;-><init>(I)V

    sput-object v6, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    .line 189
    sget-object v6, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getMarkerOptionsChooser()Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;

    .line 190
    .local v5, "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    iget-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 191
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    new-instance v4, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v4}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 192
    .local v4, "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 193
    if-eqz v5, :cond_0

    .line 194
    invoke-virtual {v5, v4, v0}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    .line 196
    :cond_0
    invoke-virtual {v2, v4}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object v3

    .line 197
    .local v3, "marker":Lcom/google/android/gms/maps/model/Marker;
    iget-object v6, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 198
    sget-object v6, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    invoke-virtual {v6, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 201
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v3    # "marker":Lcom/google/android/gms/maps/model/Marker;
    .end local v4    # "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    .end local v5    # "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    :cond_1
    return-void
.end method

.method public static hideLocationThumbnail(I)V
    .locals 6
    .param p0, "durationTime"    # I

    .prologue
    .line 696
    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    if-eqz v3, :cond_0

    .line 697
    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    const v4, 0x7f040007

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 698
    .local v0, "animation":Landroid/view/animation/Animation;
    int-to-long v4, p0

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 699
    new-instance v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$2;

    invoke-direct {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$2;-><init>()V

    invoke-virtual {v0, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 718
    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    const v4, 0x7f0f0184

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    .line 719
    .local v1, "mMapThumbnailListView":Lcom/sec/samsung/gallery/hlistview/widget/HListView;
    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 730
    .end local v0    # "animation":Landroid/view/animation/Animation;
    .end local v1    # "mMapThumbnailListView":Lcom/sec/samsung/gallery/hlistview/widget/HListView;
    :cond_0
    const/4 v2, 0x0

    .line 762
    .local v2, "mMapViewLayout":Landroid/view/View;
    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    if-eqz v3, :cond_1

    .line 763
    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->TAG:Ljava/lang/String;

    const-string v4, "remove listAdapter"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    const/4 v3, 0x0

    sput-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    .line 773
    :cond_1
    return-void
.end method

.method private removeMarker(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 6
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    .line 204
    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v5}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/GoogleMap;

    .line 205
    .local v2, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v2, :cond_1

    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    if-eqz v5, :cond_1

    .line 210
    :try_start_0
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 211
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    .line 214
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getMarkerOptionsChooser()Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;

    .line 217
    .local v4, "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    if-eqz v0, :cond_1

    .line 218
    new-instance v3, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    .line 219
    .local v3, "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v5

    invoke-virtual {v3, v5}, Lcom/google/android/gms/maps/model/MarkerOptions;->position(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 220
    if-eqz v4, :cond_0

    .line 221
    invoke-virtual {v4, v3, v0}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;->choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    .line 223
    :cond_0
    invoke-virtual {v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->addMarker(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/Marker;

    move-result-object p1

    .line 224
    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 225
    sget-object v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    invoke-virtual {v5, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    .end local v0    # "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v3    # "markerOptions":Lcom/google/android/gms/maps/model/MarkerOptions;
    .end local v4    # "moc":Lcom/sec/samsung/gallery/mapfragment/MapMarkerOptionsChooser;
    :cond_1
    :goto_0
    const/4 v5, 0x0

    sput-object v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    .line 233
    return-void

    .line 228
    :catch_0
    move-exception v1

    .line 229
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private removePreviousMarkers()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 236
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/GoogleMap;

    .line 237
    .local v1, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousClusters:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousMarkers:Ljava/util/ArrayList;

    if-eqz v3, :cond_1

    .line 238
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousMarkers:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/model/Marker;

    .line 239
    .local v2, "marker":Lcom/google/android/gms/maps/model/Marker;
    invoke-virtual {v2}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    goto :goto_0

    .line 241
    .end local v2    # "marker":Lcom/google/android/gms/maps/model/Marker;
    :cond_0
    iput-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousMarkers:Ljava/util/ArrayList;

    .line 242
    iput-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousClusters:Ljava/util/ArrayList;

    .line 244
    .end local v0    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method private showAllClusters()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    if-eqz v0, :cond_0

    .line 274
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->drawMarkers()V

    .line 275
    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 280
    :goto_0
    return-void

    .line 277
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 278
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->executeTask()V

    goto :goto_0
.end method

.method private showMarkerThumbnail(Lcom/google/android/gms/maps/model/Marker;)V
    .locals 7
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x1

    .line 631
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    if-eqz v2, :cond_0

    .line 632
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->removeMarker(Lcom/google/android/gms/maps/model/Marker;)V

    .line 633
    const/16 v2, 0xfa

    invoke-static {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 635
    :cond_0
    sput-object p1, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    .line 636
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 637
    .local v0, "clusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    if-nez v0, :cond_1

    .line 663
    :goto_0
    return-void

    .line 643
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    if-nez v2, :cond_3

    .line 644
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f030088

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sput-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    .line 649
    :goto_1
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    new-instance v4, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v4, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 650
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    const v3, 0x7f0f0184

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    .line 652
    .local v1, "mapThumbnailListView":Lcom/sec/samsung/gallery/hlistview/widget/HListView;
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    if-nez v2, :cond_2

    .line 653
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->TAG:Ljava/lang/String;

    const-string v3, "make listAdapter"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 654
    new-instance v2, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    sget-object v4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    invoke-direct {v2, v3, v0, v4}, Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;Landroid/view/View;)V

    sput-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    .line 657
    :cond_2
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setClickable(Z)V

    .line 658
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setFocusable(Z)V

    .line 659
    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setFocusableInTouchMode(Z)V

    .line 660
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapMarkerThumbnailListAdapter:Lcom/sec/samsung/gallery/mapfragment/MapMarkerThumbnailListAdapter;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 661
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mContext:Landroid/content/Context;

    const v3, 0x7f04000c

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 646
    .end local v1    # "mapThumbnailListView":Lcom/sec/samsung/gallery/hlistview/widget/HListView;
    :cond_3
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mMapViewLayout:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_1
.end method

.method private startClusterTransitionsBuildingTask(Lcom/google/android/gms/maps/Projection;)V
    .locals 1
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;

    .prologue
    .line 266
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    .line 267
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;->executeTask(Lcom/google/android/gms/maps/Projection;)V

    .line 269
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 270
    return-void
.end method

.method private startClusteringTask()V
    .locals 2

    .prologue
    .line 254
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$UpdateClustersAndTransitionClusteringTaskHost;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$UpdateClustersAndTransitionClusteringTaskHost;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 255
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->executeTask()V

    .line 256
    return-void
.end method

.method private transitionClusters(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;)V
    .locals 1
    .param p1, "clusterTransitions"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .prologue
    .line 259
    if-eqz p1, :cond_0

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->transitionsAnimation:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;->animate(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;)V

    .line 262
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    .line 263
    return-void
.end method

.method private updateClustersAndTransition()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusters:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousClusters:Ljava/util/ArrayList;

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->previousMarkers:Ljava/util/ArrayList;

    .line 250
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->startClusteringTask()V

    .line 251
    return-void
.end method


# virtual methods
.method public add(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V
    .locals 1
    .param p1, "inputPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 122
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->updateClustersAndTransition()V

    .line 124
    :cond_0
    return-void
.end method

.method public addAll(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 132
    .local p1, "inputPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;>;"
    if-eqz p1, :cond_0

    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 134
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->updateClustersAndTransition()V

    .line 136
    :cond_0
    return-void
.end method

.method public clear()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 155
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    if-eqz v2, :cond_0

    .line 156
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;->cancel()V

    .line 157
    iput-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusteringTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;

    .line 162
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    if-eqz v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;->cancel()V

    .line 164
    iput-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clusterTransitionsBuildingTaskHost:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ClusterTransitionsBuildingTaskHost;

    .line 172
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    .line 173
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/Marker;

    .line 174
    .local v1, "marker":Lcom/google/android/gms/maps/model/Marker;
    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/Marker;->remove()V

    goto :goto_0

    .line 177
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "marker":Lcom/google/android/gms/maps/model/Marker;
    :cond_2
    iput-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentMarkers:Ljava/util/ArrayList;

    .line 178
    sput-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->currentClusterPointsByMarker:Ljava/util/HashMap;

    .line 179
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->points:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 180
    return-void
.end method

.method public onMapClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 778
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 779
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->removeMarker(Lcom/google/android/gms/maps/model/Marker;)V

    .line 780
    const/16 v0, 0xfa

    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 782
    :cond_0
    return-void
.end method

.method public onMapLongClick(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 1
    .param p1, "point"    # Lcom/google/android/gms/maps/model/LatLng;

    .prologue
    .line 788
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    if-eqz v0, :cond_0

    .line 789
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mTouched_marker:Lcom/google/android/gms/maps/model/Marker;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->removeMarker(Lcom/google/android/gms/maps/model/Marker;)V

    .line 790
    const/16 v0, 0xfa

    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 792
    :cond_0
    return-void
.end method

.method public onMapTouch(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 677
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 690
    :goto_0
    return-void

    .line 679
    :pswitch_0
    const-string v0, "TEST"

    const-string v1, "onMapTouch ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 682
    :pswitch_1
    const-string v0, "TEST"

    const-string v1, "onMapTouch ACTION_MOVE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 686
    :pswitch_2
    const-string v0, "MapViewState"

    const-string v1, "onMapClick"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    const/16 v0, 0xfa

    invoke-static {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    goto :goto_0

    .line 677
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public replace(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "inputPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;>;"
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clear()V

    .line 145
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->addAll(Ljava/util/ArrayList;)V

    .line 146
    return-void
.end method

.method public showInfoWindow(Lcom/google/android/gms/maps/model/Marker;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 8
    .param p1, "marker"    # Lcom/google/android/gms/maps/model/Marker;
    .param p2, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 303
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v4}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/GoogleMap;

    .line 304
    .local v1, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 305
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v6, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getShowInfoWindowAnimationDuration()I

    move-result v6

    int-to-long v6, v6

    add-long v2, v4, v6

    .line 306
    .local v2, "dirtyUntil":J
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->setDirty(J)V

    .line 307
    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/Marker;->getPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLng(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    .line 308
    .local v0, "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    sget-object v4, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getShowInfoWindowAnimationDuration()I

    move-result v4

    new-instance v5, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 330
    .end local v0    # "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    .end local v2    # "dirtyUntil":J
    :cond_0
    return-void
.end method

.method public zoomToBounds(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 6
    .param p1, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 288
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->mapRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/GoogleMap;

    .line 289
    .local v1, "map":Lcom/google/android/gms/maps/GoogleMap;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 290
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->innerCallbackListener:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->clusteringOnCameraChangeListener:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;
    invoke-static {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;->access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$InnerCallbackListener;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringOnCameraChangeListener;->setDirty(J)V

    .line 291
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getBoundsOfInputPoints()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v2

    sget-object v3, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getZoomToBoundsPadding()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newLatLngBounds(Lcom/google/android/gms/maps/model/LatLngBounds;I)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v0

    .line 292
    .local v0, "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getZoomToBoundsAnimationDuration()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/maps/GoogleMap;->animateCamera(Lcom/google/android/gms/maps/CameraUpdate;ILcom/google/android/gms/maps/GoogleMap$CancelableCallback;)V

    .line 294
    .end local v0    # "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    :cond_0
    return-void
.end method
