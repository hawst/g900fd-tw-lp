.class Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;
.super Ljava/lang/Object;
.source "DrawerItemSLink.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->selectItem(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/app/StateManager;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

.field final synthetic val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;Landroid/widget/CheckBox;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    iput-object p2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->val$checkBox:Landroid/widget/CheckBox;

    iput-object p3, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 126
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "allow_mobile_network"

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 130
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 131
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    iget-object v2, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink;->getMediaSetPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerItemSLink$1;->val$activity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/allview/AllViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 136
    return-void
.end method
