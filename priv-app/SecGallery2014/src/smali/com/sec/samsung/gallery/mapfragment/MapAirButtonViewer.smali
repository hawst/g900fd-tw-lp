.class public Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;
.super Ljava/lang/Object;
.source "MapAirButtonViewer.java"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field private mItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mWidgetMenuImpl:Lcom/samsung/android/airbutton/AirButtonImpl;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mWidgetMenuImpl:Lcom/samsung/android/airbutton/AirButtonImpl;

    .line 26
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 31
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    .line 32
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/airbutton/AirButtonImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mWidgetMenuImpl:Lcom/samsung/android/airbutton/AirButtonImpl;

    .line 33
    iput-object p3, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 34
    check-cast p2, Landroid/widget/ImageView;

    .end local p2    # "v":Landroid/view/View;
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mImageView:Landroid/widget/ImageView;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method


# virtual methods
.method public createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/airbutton/AirButtonImpl;
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 39
    new-instance v0, Lcom/samsung/android/airbutton/AirButtonImpl;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->getAdapterMenuList()Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/samsung/android/airbutton/AirButtonImpl;-><init>(Landroid/view/View;Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;IZ)V

    .line 41
    .local v0, "airButtonWidget":Lcom/samsung/android/airbutton/AirButtonImpl;
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->setOnItemSelectedListener(Lcom/samsung/android/airbutton/AirButtonImpl$OnItemSelectedListener;)V

    .line 78
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/samsung/android/airbutton/AirButtonImpl;->setGravity(I)V

    .line 79
    return-object v0
.end method

.method public getAdapterMenuList()Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 84
    .local v0, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;>;"
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020060

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e01ff

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02005d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0063

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02005f

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0058

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 87
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020061

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0047

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3, v5}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter$AirButtonItem;-><init>(Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v1, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;

    invoke-direct {v1, v0}, Lcom/samsung/android/sdk/look/airbutton/SlookAirButtonAdapter;-><init>(Ljava/util/ArrayList;)V

    return-object v1
.end method

.method public getAirButtonImpl()Lcom/samsung/android/airbutton/AirButtonImpl;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mWidgetMenuImpl:Lcom/samsung/android/airbutton/AirButtonImpl;

    return-object v0
.end method
