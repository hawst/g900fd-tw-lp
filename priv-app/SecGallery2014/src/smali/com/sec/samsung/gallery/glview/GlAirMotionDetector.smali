.class public Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;
.super Ljava/lang/Object;
.source "GlAirMotionDetector.java"

# interfaces
.implements Lcom/samsung/android/service/gesture/GestureListener;
.implements Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;,
        Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;,
        Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$IAirMotionListener;
    }
.end annotation


# static fields
.field public static final AIR_MOTION_AIRSCROLL:I = 0x0

.field public static final AIR_MOTION_AIRTURN:I = 0x1

.field public static final AIR_MOTION_AIRTURN_TUTORIAL:I = 0x2

.field private static final TAG:Ljava/lang/String; = "GlAirMotionDetector"


# instance fields
.field private final AIR_MOTION_PROVIDER:Ljava/lang/String;

.field private mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

.field private mAirMotionSettings:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;

.field private mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

.field private mMotionType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/BroadcastReceiver;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "receiver"    # Landroid/content/BroadcastReceiver;
    .param p3, "motionType"    # I

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "ir_provider"

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->AIR_MOTION_PROVIDER:Ljava/lang/String;

    .line 24
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 25
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mConnected:Z

    .line 27
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    .line 59
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    .line 60
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mMotionType:I

    .line 61
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p3}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;-><init>(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettings:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;

    .line 62
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->registerReceiver(Landroid/content/BroadcastReceiver;)V

    .line 64
    return-void
.end method

.method private registerListener()V
    .locals 3

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mConnected:Z

    if-eqz v0, :cond_0

    .line 95
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mMotionType:I

    packed-switch v0, :pswitch_data_0

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 97
    :pswitch_0
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector register listener : AIR_MOTION_PROVIDER, AIR_MOTION_AIRSCROLL"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_scroll"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 102
    :pswitch_1
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector register listener : AIR_MOTION_PROVIDER, AIR_MOTION_AIRBROWSE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    const-string v2, "air_motion_turn"

    invoke-virtual {v0, p0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private registerReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 3
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 38
    const-string v1, "GlAirMotionDetector"

    const-string v2, "GlAirMotionDetector register broadcast receiver"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.sec.gesture.AIR_MOTION_SETTINGS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 40
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v1, p1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 41
    return-void
.end method

.method private unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    .locals 2
    .param p1, "receiver"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 44
    if-eqz p1, :cond_0

    .line 45
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector unregister broadcast receiver"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 47
    const/4 p1, 0x0

    .line 49
    :cond_0
    return-void
.end method


# virtual methods
.method public destoryAirMotionDetector()V
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->disableAirMotion()V

    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettingsReceiver:Landroid/content/BroadcastReceiver;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 112
    return-void
.end method

.method public disableAirMotion()V
    .locals 2

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 87
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mConnected:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 89
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector unregister listener : AIR_MOTION_PROVIDER"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    return-void
.end method

.method public onGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 2
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    .line 133
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 165
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 135
    :pswitch_1
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector GESTURE_EVENT_SWEEP_LEFT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;->onPrevious()V

    goto :goto_0

    .line 141
    :pswitch_2
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector GESTURE_EVENT_SWEEP_RIGHT"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;->onNext()V

    goto :goto_0

    .line 147
    :pswitch_3
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector GESTURE_EVENT_SWEEP_UP"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;->onPrevious()V

    goto :goto_0

    .line 153
    :pswitch_4
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector GESTURE_EVENT_SWEEP_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;->onNext()V

    goto :goto_0

    .line 160
    :pswitch_5
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector GESTURE_EVENT_HOVER"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onServiceConnected()V
    .locals 2

    .prologue
    .line 116
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector Service is connected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mConnected:Z

    .line 118
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    if-eqz v0, :cond_0

    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->registerListener()V

    .line 120
    :cond_0
    return-void
.end method

.method public onServiceDisconnected()V
    .locals 2

    .prologue
    .line 124
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector Service is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    const-string v1, "ir_provider"

    invoke-virtual {v0, p0, v1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 127
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 128
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mConnected:Z

    .line 129
    return-void
.end method

.method public setAirMotionLintener(Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->disableAirMotion()V

    .line 83
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionSettings:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionSettings;->checkAirMotionEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector setting : enable"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_1

    .line 73
    new-instance v0, Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/samsung/android/service/gesture/GestureManager;-><init>(Landroid/content/Context;Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    .line 75
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->mAirMotionListener:Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;

    .line 76
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->registerListener()V

    goto :goto_0

    .line 79
    :cond_2
    const-string v0, "GlAirMotionDetector"

    const-string v1, "GlAirMotionDetector setting : disabled"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector;->disableAirMotion()V

    goto :goto_0
.end method
