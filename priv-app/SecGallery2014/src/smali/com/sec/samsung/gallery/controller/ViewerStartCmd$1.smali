.class Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;
.super Ljava/lang/Object;
.source "ViewerStartCmd.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->execute(Lorg/puremvc/java/interfaces/INotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)V
    .locals 0

    .prologue
    .line 186
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$000(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setStartIdleState(Z)V

    .line 190
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/ViewerStartCmd$1;->this$0:Lcom/sec/samsung/gallery/controller/ViewerStartCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/ViewerStartCmd;->access$000(Lcom/sec/samsung/gallery/controller/ViewerStartCmd;)Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 191
    return-void
.end method
