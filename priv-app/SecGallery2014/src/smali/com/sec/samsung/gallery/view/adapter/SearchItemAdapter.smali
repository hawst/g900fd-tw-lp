.class public Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "SearchItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$1;,
        Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;
    }
.end annotation


# instance fields
.field private mChildSetPath:Ljava/lang/String;

.field protected final mContext:Landroid/content/Context;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mGenericMotionKeywordFocus:I

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

.field private mSetPath:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "setPath"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 46
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 37
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 38
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 47
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSetPath:Ljava/lang/String;

    .line 48
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSetPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    const-string v1, "children"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/Path;->getChild(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/SearchAlbum;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    .line 54
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->clear()V

    .line 56
    :cond_0
    return-void
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IZ)V
    .locals 4
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "keyword"    # Z

    .prologue
    const/4 v3, 0x5

    .line 188
    invoke-virtual {p1, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 189
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 191
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v2, 0x7f020291

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 192
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 195
    :cond_0
    if-eqz p3, :cond_1

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 196
    .local v1, "genericMotionKeywordFocus":I
    :goto_0
    if-ne v1, p2, :cond_2

    .line 197
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 201
    :goto_1
    return-void

    .line 195
    .end local v1    # "genericMotionKeywordFocus":I
    :cond_1
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mGenericMotionFocus:I

    goto :goto_0

    .line 199
    .restart local v1    # "genericMotionKeywordFocus":I
    :cond_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 14
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 284
    if-nez p1, :cond_0

    .line 309
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :goto_0
    return-void

    .line 287
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_0
    if-nez p3, :cond_1

    .line 288
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 292
    :cond_1
    :try_start_0
    move/from16 v0, p2

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 293
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 294
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 306
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v13

    .line 307
    .local v13, "e":Ljava/lang/Exception;
    invoke-virtual {v13}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 296
    .end local v13    # "e":Ljava/lang/Exception;
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    :try_start_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v5

    .line 297
    .local v5, "rotation":I
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v1, :cond_3

    move-object/from16 v0, p3

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v1, :cond_3

    invoke-static/range {p3 .. p3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 299
    check-cast p3, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v6

    .line 300
    .local v6, "faceRect":Landroid/graphics/RectF;
    const/16 v3, 0x140

    const/16 v4, 0x140

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V

    goto :goto_0

    .line 303
    .end local v6    # "faceRect":Landroid/graphics/RectF;
    .restart local p3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_3
    const/16 v9, 0x140

    const/16 v10, 0x140

    const/4 v12, 0x0

    move-object v7, p1

    move-object v8, v2

    move v11, v5

    invoke-virtual/range {v7 .. v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setCroppedBitmap(Landroid/graphics/Bitmap;IIILandroid/graphics/RectF;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/16 v6, 0x30

    const/4 v5, 0x7

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x3

    .line 241
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 242
    .local v0, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v1

    if-nez v1, :cond_3

    .line 243
    if-nez v0, :cond_0

    .line 244
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 245
    .restart local v0    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f0200de

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 246
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 247
    invoke-virtual {v0, v2, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 251
    :goto_0
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 252
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 253
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 255
    :cond_0
    invoke-virtual {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 259
    :cond_1
    :goto_1
    return-void

    .line 249
    :cond_2
    invoke-virtual {v0, v4, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto :goto_0

    .line 256
    :cond_3
    if-eqz v0, :cond_1

    .line 257
    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 10
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 262
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 263
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    instance-of v6, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v6, :cond_2

    move v1, v4

    .line 265
    .local v1, "isVideo":Z
    :goto_0
    invoke-virtual {p1, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 267
    .local v3, "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v1, :cond_3

    .line 268
    if-nez v3, :cond_0

    .line 269
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020308

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 270
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 271
    .restart local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 272
    invoke-virtual {v3, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 273
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 274
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 275
    invoke-virtual {p1, v3, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 277
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 281
    :cond_1
    :goto_1
    return-void

    .end local v1    # "isVideo":Z
    .end local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    move v1, v5

    .line 263
    goto :goto_0

    .line 278
    .restart local v1    # "isVideo":Z
    .restart local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    if-eqz v3, :cond_1

    .line 279
    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private getKeyView(ILcom/sec/android/gallery3d/glcore/GlView;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 12
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;

    .prologue
    .line 205
    if-nez p2, :cond_2

    .line 206
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v1, :cond_1

    const v8, 0x7f0204c8

    .line 207
    .local v8, "buttonSelector":I
    :goto_0
    new-instance v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v9, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 208
    .local v9, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v9, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 213
    .end local v8    # "buttonSelector":I
    :goto_1
    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 214
    .local v10, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyItem(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    .local v0, "albumName":Ljava/lang/String;
    const/high16 v7, 0x43480000    # 200.0f

    .line 217
    .local v7, "ELLIPSIZE_LENGTH":F
    if-nez v10, :cond_3

    .line 218
    const-string v1, ""

    const/high16 v2, 0x42040000    # 33.0f

    const/high16 v3, -0x1000000

    const/high16 v4, 0x43480000    # 200.0f

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v10

    .line 219
    const/4 v1, 0x2

    const/4 v2, 0x2

    invoke-virtual {v10, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 220
    const/4 v1, 0x1

    invoke-virtual {v9, v10, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 224
    :goto_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0x14

    const/4 v4, 0x0

    invoke-virtual {v10, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 226
    const/4 v1, 0x2

    invoke-virtual {v9, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 227
    .local v11, "xView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v11, :cond_0

    .line 228
    new-instance v11, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v11    # "xView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v11, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 229
    .restart local v11    # "xView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f020089

    invoke-virtual {v11, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 230
    const/4 v1, 0x3

    const/4 v2, 0x2

    invoke-virtual {v11, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 231
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/16 v3, 0xa

    const/4 v4, 0x0

    invoke-virtual {v11, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 232
    const/16 v1, 0x20

    const/16 v2, 0x28

    invoke-virtual {v11, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 233
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 234
    const/4 v1, 0x2

    invoke-virtual {v9, v11, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 236
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v9, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IZ)V

    .line 237
    return-object v9

    .line 206
    .end local v0    # "albumName":Ljava/lang/String;
    .end local v7    # "ELLIPSIZE_LENGTH":F
    .end local v9    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v10    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v11    # "xView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    const v8, 0x7f0204c7

    goto :goto_0

    :cond_2
    move-object v9, p2

    .line 210
    check-cast v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v9    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1

    .line 222
    .restart local v0    # "albumName":Ljava/lang/String;
    .restart local v7    # "ELLIPSIZE_LENGTH":F
    .restart local v10    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_3
    const-string v1, ""

    const/high16 v2, 0x43480000    # 200.0f

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_2
.end method


# virtual methods
.method protected addContentListener()V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 346
    return-void
.end method

.method public addKeyword(Ljava/lang/String;)Z
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->addKeyword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 82
    :cond_0
    const/4 v0, 0x1

    .line 85
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlbumPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mChildSetPath:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItemCount()I

    move-result v0

    return v0
.end method

.method public getGenericMotionKeywordFocus()I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mGenericMotionKeywordFocus:I

    return v0
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 141
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 144
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getMediaItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    goto :goto_0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 35
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    const/4 v0, 0x0

    .line 153
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v1

    if-ge p1, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-nez v1, :cond_1

    .line 159
    :cond_0
    :goto_0
    return-object v0

    .line 156
    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getKeyItem(I)Ljava/lang/String;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getKeyword(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyWordCount()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getKeywordCount()I

    move-result v0

    return v0
.end method

.method public getMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/SearchAlbum;
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    return-object v0
.end method

.method public getSuggestionNames()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 108
    .local v3, "nameList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v5, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/SearchClusterAlbumSet;->getAlbums()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    .line 109
    .local v0, "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->getName()Ljava/lang/String;

    move-result-object v1

    .line 113
    .local v1, "albumName":Ljava/lang/String;
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v5, :cond_1

    if-eqz v1, :cond_1

    .line 114
    const-string v5, "/"

    invoke-virtual {v1, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 115
    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 116
    .local v4, "values":[Ljava/lang/String;
    const/4 v5, 0x1

    aget-object v1, v4, v5

    .line 120
    .end local v4    # "values":[Ljava/lang/String;
    :cond_1
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 123
    .end local v0    # "album":Lcom/sec/android/gallery3d/data/ClusterAlbum;
    .end local v1    # "albumName":Ljava/lang/String;
    :cond_2
    return-object v3
.end method

.method public getThumbnailBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 315
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v3, 0x0

    .line 164
    if-nez p3, :cond_0

    .line 165
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyView(ILcom/sec/android/gallery3d/glcore/GlView;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 182
    :goto_0
    return-object v0

    .line 168
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 171
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iput v3, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mViewResult:I

    .line 172
    if-nez p2, :cond_1

    .line 173
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 178
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 179
    invoke-direct {p0, v0, p1, v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/android/gallery3d/data/MediaItem;)V

    .line 180
    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->drawUnconfirmIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 181
    invoke-direct {p0, v0, p1, v3}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;IZ)V

    goto :goto_0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    move-object v0, p2

    .line 175
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1
.end method

.method protected loadData()Z
    .locals 6

    .prologue
    .line 333
    sget-object v3, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v3

    .line 334
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/SearchAlbum;->getDataVersion()J

    move-result-wide v0

    .line 335
    .local v0, "oldVersion":J
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/SearchAlbum;->reload()J

    move-result-wide v4

    cmp-long v2, v4, v0

    if-lez v2, :cond_0

    .line 336
    new-instance v2, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;

    const/4 v4, 0x0

    invoke-direct {v2, p0, v4}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;-><init>(Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$1;)V

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    .line 338
    :cond_0
    monitor-exit v3

    .line 340
    const/4 v2, 0x1

    return v2

    .line 338
    .end local v0    # "oldVersion":J
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public declared-synchronized onPause()V
    .locals 1

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onPause()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    monitor-exit p0

    return-void

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 319
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->onResume()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 320
    monitor-exit p0

    return-void

    .line 319
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method protected removeContentListener()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 351
    return-void
.end method

.method public removeKeyword(I)V
    .locals 2
    .param p1, "positon"    # I

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyItem(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->removeKeyword(Ljava/lang/String;Z)V

    .line 90
    return-void
.end method

.method public removeKeyword(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "keyword"    # Ljava/lang/String;
    .param p2, "notifyUI"    # Z

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mSearchAlbum:Lcom/sec/android/gallery3d/data/SearchAlbum;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/SearchAlbum;->removeKeyword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 99
    :cond_0
    return-void
.end method

.method public setGenericMotionKeywordFocus(I)V
    .locals 0
    .param p1, "focus"    # I

    .prologue
    .line 59
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mGenericMotionKeywordFocus:I

    .line 60
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 1
    .param p1, "loadingListener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 73
    return-void
.end method
