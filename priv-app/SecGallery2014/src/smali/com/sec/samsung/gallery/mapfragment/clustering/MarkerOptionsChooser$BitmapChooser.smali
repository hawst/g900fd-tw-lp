.class public Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
.source "MarkerOptionsChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "BitmapChooser"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;
    }
.end annotation


# instance fields
.field private final hostRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;)V
    .locals 1
    .param p1, "host"    # Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;-><init>()V

    .line 55
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser;->hostRef:Ljava/lang/ref/WeakReference;

    .line 56
    return-void
.end method


# virtual methods
.method public choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 3
    .param p1, "markerOptions"    # Lcom/google/android/gms/maps/model/MarkerOptions;
    .param p2, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 60
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;

    .line 61
    .local v1, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;
    if-eqz v1, :cond_0

    .line 62
    invoke-interface {v1, p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$BitmapChooser$Host;->getIconBitmap(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 63
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromBitmap(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 65
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    return-void
.end method
