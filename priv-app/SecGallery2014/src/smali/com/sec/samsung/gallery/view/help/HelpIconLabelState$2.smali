.class Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;
.super Ljava/lang/Object;
.source "HelpIconLabelState.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    const/4 v7, 0x0

    .line 131
    invoke-virtual {p2, v7}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    .line 170
    :goto_0
    return v0

    .line 134
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    .line 135
    .local v6, "action":I
    packed-switch v6, :pswitch_data_0

    :goto_1
    :pswitch_0
    move v0, v7

    .line 170
    goto :goto_0

    .line 137
    :pswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHoverEnterTime:J

    .line 138
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHoverModeOn:Z

    goto :goto_1

    .line 141
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHoverModeOn:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    iget-wide v2, v2, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHoverEnterTime:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x190

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    const/16 v1, 0xa

    iput v1, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mViewMode:I

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->setHelpView()V

    .line 146
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;

    const-wide/16 v2, 0xbb8

    const-wide/16 v4, 0x3e8

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;JJ)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2$1;->start()Landroid/os/CountDownTimer;

    goto :goto_1

    .line 163
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState$2;->this$0:Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;

    iput-boolean v7, v0, Lcom/sec/samsung/gallery/view/help/HelpIconLabelState;->mHoverModeOn:Z

    goto :goto_1

    .line 135
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method
