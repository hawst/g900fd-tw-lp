.class public Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;
.super Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;
.source "ComposeMediaItemAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter$MediaSelectionCallback;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "ComposeMediaItemAdapter"


# instance fields
.field protected mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field protected mHeaderImage:Landroid/graphics/Bitmap;

.field protected mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field protected mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mNewMarkBucketID:I

.field private mNewMarkItemID:I

.field private mNewMarkItemTakenTime:J

.field protected mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

.field protected mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field protected readyDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "config"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
    .param p4, "modeOption"    # I

    .prologue
    const/4 v0, -0x1

    const/4 v2, 0x0

    .line 79
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    .line 69
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 70
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 71
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkBucketID:I

    .line 72
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemID:I

    .line 73
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemTakenTime:J

    .line 74
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    .line 76
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const v0, -0x4b4b4c

    :goto_0
    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    .line 81
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    .line 82
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 85
    return-void

    .line 76
    :cond_0
    const v0, -0xd0d0d1

    goto :goto_0
.end method

.method public static calcFaceCropRect(IILandroid/graphics/RectF;I)Landroid/graphics/Rect;
    .locals 15
    .param p0, "srcWidth"    # I
    .param p1, "srcHeight"    # I
    .param p2, "faceRect"    # Landroid/graphics/RectF;
    .param p3, "rotation"    # I

    .prologue
    .line 1190
    if-eqz p2, :cond_3

    if-eqz p3, :cond_3

    .line 1191
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v11

    .line 1192
    .local v11, "w":F
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v7

    .line 1193
    .local v7, "h":F
    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/RectF;->left:F

    .line 1194
    .local v8, "left":F
    move-object/from16 v0, p2

    iget v10, v0, Landroid/graphics/RectF;->top:F

    .line 1196
    .local v10, "top":F
    const/16 v12, 0x5a

    move/from16 v0, p3

    if-eq v0, v12, :cond_0

    const/16 v12, 0xb4

    move/from16 v0, p3

    if-ne v0, v12, :cond_1

    .line 1197
    :cond_0
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v11

    sub-float v8, v12, v8

    .line 1198
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v7

    sub-float v10, v12, v10

    .line 1200
    :cond_1
    move/from16 v0, p3

    rem-int/lit16 v12, v0, 0xb4

    if-eqz v12, :cond_2

    .line 1202
    move v9, v10

    .line 1203
    .local v9, "temp":F
    move v10, v8

    .line 1204
    const/high16 v12, 0x3f800000    # 1.0f

    sub-float/2addr v12, v9

    sub-float v8, v12, v7

    .line 1206
    move v9, v11

    .line 1207
    move v11, v7

    .line 1208
    move v7, v9

    .line 1210
    .end local v9    # "temp":F
    :cond_2
    new-instance p2, Landroid/graphics/RectF;

    .end local p2    # "faceRect":Landroid/graphics/RectF;
    add-float v12, v8, v11

    add-float v13, v10, v7

    move-object/from16 v0, p2

    invoke-direct {v0, v8, v10, v12, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 1216
    .end local v7    # "h":F
    .end local v8    # "left":F
    .end local v10    # "top":F
    .end local v11    # "w":F
    .restart local p2    # "faceRect":Landroid/graphics/RectF;
    :cond_3
    move/from16 v0, p1

    if-le v0, p0, :cond_6

    .line 1218
    move v2, p0

    .line 1219
    .local v2, "cropWidth":I
    move v1, p0

    .line 1220
    .local v1, "cropHeight":I
    const/4 v3, 0x0

    .line 1221
    .local v3, "cropX":I
    sub-int v12, p1, v1

    div-int/lit8 v4, v12, 0x2

    .line 1229
    .local v4, "cropY":I
    :goto_0
    if-eqz p2, :cond_5

    .line 1231
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->left:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_4

    .line 1232
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    int-to-float v13, p0

    mul-float/2addr v12, v13

    float-to-int v5, v12

    .line 1233
    .local v5, "faceCenterH":I
    div-int/lit8 v12, v2, 0x2

    sub-int v3, v5, v12

    .line 1234
    if-gez v3, :cond_7

    .line 1235
    const/4 v3, 0x0

    .line 1240
    .end local v5    # "faceCenterH":I
    :cond_4
    :goto_1
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->top:F

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_5

    .line 1241
    move-object/from16 v0, p2

    iget v12, v0, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v12, v13

    const/high16 v13, 0x40000000    # 2.0f

    div-float/2addr v12, v13

    move/from16 v0, p1

    int-to-float v13, v0

    mul-float/2addr v12, v13

    float-to-int v6, v12

    .line 1242
    .local v6, "faceCenterV":I
    div-int/lit8 v12, v1, 0x2

    sub-int v4, v6, v12

    .line 1243
    if-gez v4, :cond_8

    .line 1244
    const/4 v4, 0x0

    .line 1250
    .end local v6    # "faceCenterV":I
    :cond_5
    :goto_2
    new-instance v12, Landroid/graphics/Rect;

    add-int v13, v3, v2

    add-int v14, v4, v1

    invoke-direct {v12, v3, v4, v13, v14}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v12

    .line 1224
    .end local v1    # "cropHeight":I
    .end local v2    # "cropWidth":I
    .end local v3    # "cropX":I
    .end local v4    # "cropY":I
    :cond_6
    move/from16 v1, p1

    .line 1225
    .restart local v1    # "cropHeight":I
    move/from16 v2, p1

    .line 1226
    .restart local v2    # "cropWidth":I
    const/4 v4, 0x0

    .line 1227
    .restart local v4    # "cropY":I
    sub-int v12, p0, v2

    div-int/lit8 v3, v12, 0x2

    .restart local v3    # "cropX":I
    goto :goto_0

    .line 1236
    .restart local v5    # "faceCenterH":I
    :cond_7
    sub-int v12, p0, v2

    if-le v3, v12, :cond_4

    .line 1237
    sub-int v3, p0, v2

    goto :goto_1

    .line 1245
    .end local v5    # "faceCenterH":I
    .restart local v6    # "faceCenterV":I
    :cond_8
    sub-int v12, p1, v1

    if-le v4, v12, :cond_5

    .line 1246
    sub-int v4, p1, v1

    goto :goto_2
.end method

.method private drawDecorViewAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 43
    .param p1, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 280
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v17, v0

    .line 281
    .local v17, "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispSelectCnt:Z

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v36

    .line 283
    .local v36, "selectedCount":I
    :goto_0
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispCheckBox:Z

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    and-int v22, v4, v5

    .line 284
    .local v22, "isChecked":Z
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkBucketID:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_0

    if-nez p2, :cond_4

    :cond_0
    const/16 v23, 0x0

    .line 285
    .local v23, "isNewMark":Z
    :goto_1
    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v24

    .line 286
    .local v24, "isSecret":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v33

    .line 287
    .local v33, "rsrcPlayType":I
    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getContentTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Z)I

    move-result v32

    .line 289
    .local v32, "rsrcMediaType":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewIconLeftPadding()I

    move-result v25

    .line 291
    .local v25, "leftPadding":I
    if-eqz v23, :cond_2

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemID:I

    if-le v4, v5, :cond_1

    invoke-virtual/range {p3 .. p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDateInMs()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemTakenTime:J

    cmp-long v4, v4, v8

    if-gtz v4, :cond_2

    .line 292
    :cond_1
    const/16 v23, 0x0

    .line 294
    :cond_2
    if-nez v36, :cond_6

    if-nez v22, :cond_6

    if-nez v23, :cond_6

    if-nez v33, :cond_6

    if-nez v32, :cond_6

    if-nez v24, :cond_6

    .line 295
    const/4 v4, 0x0

    .line 443
    :goto_2
    return-object v4

    .line 281
    .end local v22    # "isChecked":Z
    .end local v23    # "isNewMark":Z
    .end local v24    # "isSecret":Z
    .end local v25    # "leftPadding":I
    .end local v32    # "rsrcMediaType":I
    .end local v33    # "rsrcPlayType":I
    .end local v36    # "selectedCount":I
    :cond_3
    const/16 v36, 0x0

    goto :goto_0

    .line 284
    .restart local v22    # "isChecked":Z
    .restart local v36    # "selectedCount":I
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkBucketID:I

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v5

    if-ne v4, v5, :cond_5

    const/16 v23, 0x1

    goto :goto_1

    :cond_5
    const/16 v23, 0x0

    goto :goto_1

    .line 297
    .restart local v23    # "isNewMark":Z
    .restart local v24    # "isSecret":Z
    .restart local v25    # "leftPadding":I
    .restart local v32    # "rsrcMediaType":I
    .restart local v33    # "rsrcPlayType":I
    :cond_6
    if-nez v17, :cond_7

    .line 298
    new-instance v17, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v17    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v17

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 300
    .restart local v17    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_7
    const/4 v4, 0x3

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v31

    check-cast v31, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 301
    .local v31, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v33, :cond_f

    .line 302
    if-nez v31, :cond_8

    .line 303
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move/from16 v0, v33

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 304
    .local v18, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v31, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v31    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v31

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 305
    .restart local v31    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v31

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 306
    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 307
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move-object/from16 v0, v31

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 308
    const/4 v4, 0x3

    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 314
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_8
    :goto_3
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 315
    .local v10, "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v32, :cond_11

    .line 316
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    move/from16 v0, v32

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 317
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v10, :cond_10

    .line 318
    new-instance v10, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v10, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 319
    .restart local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 320
    const/4 v4, 0x1

    const/4 v5, 0x3

    invoke-virtual {v10, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 321
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    invoke-virtual {v10, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 322
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v25

    invoke-virtual {v10, v0, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 323
    const/4 v4, 0x4

    move-object/from16 v0, v17

    invoke-virtual {v0, v10, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 327
    :goto_4
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 328
    const/16 v26, 0x3

    .line 329
    .local v26, "leftpadding":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move/from16 v0, v26

    invoke-virtual {v10, v0, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 335
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v26    # "leftpadding":I
    :cond_9
    :goto_5
    const/4 v4, 0x7

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v35

    check-cast v35, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 336
    .local v35, "selectCountView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-lez v36, :cond_15

    .line 337
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontColor()I

    move-result v20

    .line 338
    .local v20, "fontColor":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontSize()I

    move-result v21

    .line 339
    .local v21, "fontSize":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move/from16 v0, v36

    invoke-virtual {v4, v5, v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountBoxWidth(Landroid/content/Context;I)I

    move-result v15

    .line 340
    .local v15, "boxWidth":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountBoxHeight()I

    move-result v12

    .line 341
    .local v12, "boxHeight":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountTopPaddingPixel()I

    move-result v14

    .line 342
    .local v14, "boxTopPadding":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountRightPaddingPixel()I

    move-result v13

    .line 343
    .local v13, "boxRightPadding":I
    const-string v37, ""

    .line 344
    .local v37, "selectedStr":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-static {v4}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v30

    .line 345
    .local v30, "numberFormat":Ljava/text/NumberFormat;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v27

    .line 346
    .local v27, "locale":Ljava/lang/String;
    const-string v4, "ar"

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    const-string v4, "fa"

    move-object/from16 v0, v27

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_12

    .line 347
    :cond_a
    move/from16 v0, v36

    int-to-long v4, v0

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v37

    .line 351
    :goto_6
    if-nez v35, :cond_14

    .line 352
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v4

    if-eqz v4, :cond_13

    .line 353
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020209

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 357
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_7
    move/from16 v0, v21

    int-to-float v4, v0

    move-object/from16 v0, v37

    move/from16 v1, v20

    invoke-static {v0, v4, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v38

    .line 358
    .local v38, "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    invoke-virtual/range {v38 .. v38}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextBold()V

    .line 359
    invoke-virtual/range {v38 .. v38}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v19

    .line 360
    .local v19, "fm":Landroid/graphics/Paint$FontMetrics;
    int-to-float v4, v12

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v5, v6

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v42, v4, 0x2

    .line 361
    .local v42, "topMargin":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v38

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 362
    move/from16 v0, v21

    int-to-float v4, v0

    move-object/from16 v0, v38

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setTextSize(F)V

    .line 363
    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, v38

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 365
    new-instance v35, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v35    # "selectCountView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v35

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 366
    .restart local v35    # "selectCountView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v35

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 368
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v14, v13, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 369
    const/4 v4, 0x3

    const/4 v5, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 370
    move-object/from16 v0, v35

    invoke-virtual {v0, v15, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 371
    const/4 v4, 0x7

    move-object/from16 v0, v17

    move-object/from16 v1, v35

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 373
    const/4 v4, 0x1

    move-object/from16 v0, v35

    move-object/from16 v1, v38

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 384
    .end local v12    # "boxHeight":I
    .end local v13    # "boxRightPadding":I
    .end local v14    # "boxTopPadding":I
    .end local v15    # "boxWidth":I
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v19    # "fm":Landroid/graphics/Paint$FontMetrics;
    .end local v20    # "fontColor":I
    .end local v21    # "fontSize":I
    .end local v27    # "locale":Ljava/lang/String;
    .end local v30    # "numberFormat":Ljava/text/NumberFormat;
    .end local v37    # "selectedStr":Ljava/lang/String;
    .end local v38    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v42    # "topMargin":I
    :cond_b
    :goto_8
    const/16 v4, 0xc

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v40

    check-cast v40, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 385
    .local v40, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/4 v4, 0x6

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v29

    check-cast v29, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 386
    .local v29, "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e04ad

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 387
    .local v28, "newLabel":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkFontSize()I

    move-result v39

    .line 388
    .local v39, "textSize":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewAlbumFontColor()I

    move-result v7

    .line 389
    .local v7, "textColor":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    const-string v6, "albumViewMode"

    const/4 v8, 0x0

    invoke-static {v5, v6, v8}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNameTextboxWidthPixel(I)I

    move-result v41

    .line 391
    .local v41, "textW":I
    if-gtz v36, :cond_16

    if-eqz v23, :cond_16

    .line 392
    if-nez v29, :cond_c

    .line 393
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020110

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 394
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v0, v39

    int-to-float v6, v0

    move/from16 v0, v41

    int-to-float v8, v0

    const/4 v9, 0x1

    invoke-static/range {v4 .. v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;Ljava/lang/String;FIFZ)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v40

    .line 395
    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, v40

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 396
    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v4

    invoke-virtual {v4}, Landroid/text/TextPaint;->getFontMetrics()Landroid/graphics/Paint$FontMetrics;

    move-result-object v19

    .line 397
    .restart local v19    # "fm":Landroid/graphics/Paint$FontMetrics;
    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    move-object/from16 v0, v19

    iget v5, v0, Landroid/graphics/Paint$FontMetrics;->descent:F

    move-object/from16 v0, v19

    iget v6, v0, Landroid/graphics/Paint$FontMetrics;->ascent:F

    sub-float/2addr v5, v6

    sub-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    float-to-int v0, v4

    move/from16 v42, v0

    .line 398
    .restart local v42    # "topMargin":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, v40

    move/from16 v1, v42

    invoke-virtual {v0, v4, v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 399
    new-instance v29, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v29    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v29

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 400
    .restart local v29    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v29

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 401
    const/4 v4, 0x3

    const/4 v5, 0x1

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 402
    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getWidth()I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewNewLabelPadding()I

    move-result v5

    mul-int/lit8 v5, v5, 0x2

    add-int/2addr v4, v5

    invoke-virtual/range {v40 .. v40}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getHeight()I

    move-result v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 403
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkTopMargin()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewMarkRightMargin()I

    move-result v6

    const/4 v8, 0x0

    move-object/from16 v0, v29

    invoke-virtual {v0, v4, v5, v6, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 404
    const/4 v4, 0x6

    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 405
    const/16 v4, 0xc

    move-object/from16 v0, v29

    move-object/from16 v1, v40

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 411
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v19    # "fm":Landroid/graphics/Paint$FontMetrics;
    .end local v42    # "topMargin":I
    :cond_c
    :goto_9
    const/16 v4, 0xa

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v34

    check-cast v34, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 412
    .local v34, "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v24, :cond_18

    .line 413
    if-nez v34, :cond_d

    .line 414
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0108

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 415
    .local v11, "bottomMargin":I
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v4, :cond_17

    .line 416
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0201a9

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 419
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_a
    new-instance v34, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v34    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v34

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 420
    .restart local v34    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v34

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 421
    const/4 v4, 0x1

    const/4 v5, 0x3

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 422
    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual/range {v18 .. v18}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v5

    move-object/from16 v0, v34

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 423
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v25

    invoke-virtual {v0, v1, v4, v5, v11}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 424
    const/4 v4, 0x0

    move-object/from16 v0, v34

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 425
    const/16 v4, 0xa

    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 431
    .end local v11    # "bottomMargin":I
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_d
    :goto_b
    const/16 v4, 0x9

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v16

    check-cast v16, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 432
    .local v16, "checkedView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v22, :cond_19

    .line 433
    if-nez v16, :cond_e

    .line 434
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f02055e

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .line 435
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v16, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v16    # "checkedView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 436
    .restart local v16    # "checkedView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 437
    const/4 v4, 0x2

    const/4 v5, 0x2

    move-object/from16 v0, v16

    invoke-virtual {v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 438
    const/16 v4, 0x9

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_e
    :goto_c
    move-object/from16 v4, v17

    .line 443
    goto/16 :goto_2

    .line 310
    .end local v7    # "textColor":I
    .end local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v16    # "checkedView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v28    # "newLabel":Ljava/lang/String;
    .end local v29    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v34    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v35    # "selectCountView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v39    # "textSize":I
    .end local v40    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v41    # "textW":I
    :cond_f
    if-eqz v31, :cond_8

    .line 311
    move-object/from16 v0, v17

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_3

    .line 325
    .restart local v10    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_10
    move-object/from16 v0, v18

    invoke-virtual {v10, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4

    .line 331
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_11
    if-eqz v10, :cond_9

    .line 332
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_5

    .line 349
    .restart local v12    # "boxHeight":I
    .restart local v13    # "boxRightPadding":I
    .restart local v14    # "boxTopPadding":I
    .restart local v15    # "boxWidth":I
    .restart local v20    # "fontColor":I
    .restart local v21    # "fontSize":I
    .restart local v27    # "locale":Ljava/lang/String;
    .restart local v30    # "numberFormat":Ljava/text/NumberFormat;
    .restart local v35    # "selectCountView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v37    # "selectedStr":Ljava/lang/String;
    :cond_12
    invoke-static/range {v36 .. v36}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v37

    goto/16 :goto_6

    .line 355
    :cond_13
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f020208

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_7

    .line 375
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_14
    move-object/from16 v0, v35

    invoke-virtual {v0, v15, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 376
    const/4 v4, 0x1

    move-object/from16 v0, v35

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v38

    check-cast v38, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 377
    .restart local v38    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-eqz v38, :cond_b

    move-object/from16 v0, v38

    move-object/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto/16 :goto_8

    .line 379
    .end local v12    # "boxHeight":I
    .end local v13    # "boxRightPadding":I
    .end local v14    # "boxTopPadding":I
    .end local v15    # "boxWidth":I
    .end local v20    # "fontColor":I
    .end local v21    # "fontSize":I
    .end local v27    # "locale":Ljava/lang/String;
    .end local v30    # "numberFormat":Ljava/text/NumberFormat;
    .end local v37    # "selectedStr":Ljava/lang/String;
    .end local v38    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_15
    if-eqz v35, :cond_b

    .line 380
    move-object/from16 v0, v17

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_8

    .line 407
    .restart local v7    # "textColor":I
    .restart local v28    # "newLabel":Ljava/lang/String;
    .restart local v29    # "newMarkView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local v39    # "textSize":I
    .restart local v40    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v41    # "textW":I
    :cond_16
    if-eqz v29, :cond_c

    .line 408
    move-object/from16 v0, v17

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_9

    .line 418
    .restart local v11    # "bottomMargin":I
    .restart local v34    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_17
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0200d7

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v18

    .restart local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_a

    .line 427
    .end local v11    # "bottomMargin":I
    .end local v18    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_18
    if-eqz v34, :cond_d

    .line 428
    move-object/from16 v0, v17

    move-object/from16 v1, v34

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_b

    .line 440
    .restart local v16    # "checkedView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_19
    if-eqz v16, :cond_e

    .line 441
    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_c
.end method

.method private getContentTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Z)I
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p3, "albumList"    # Z

    .prologue
    .line 718
    const/4 v1, 0x0

    .line 720
    .local v1, "rsrcMediaType":I
    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v2, :cond_0

    instance-of v2, p2, Lcom/sec/android/gallery3d/data/LocalVideo;

    if-eqz v2, :cond_a

    .line 721
    :cond_0
    if-eqz p3, :cond_8

    .line 723
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 724
    .local v0, "path":Ljava/lang/String;
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->isCameraAlbum()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getBucketId()I

    move-result v2

    sget v3, Lcom/sec/android/gallery3d/util/MediaSetUtils;->GEAR_BUCKET_ID:I

    if-ne v2, v3, :cond_6

    .line 726
    :cond_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_4

    .line 727
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const v1, 0x7f0201a1

    .line 779
    .end local v0    # "path":Ljava/lang/String;
    :cond_2
    :goto_0
    return v1

    .line 727
    .restart local v0    # "path":Ljava/lang/String;
    :cond_3
    const v1, 0x7f0201a0

    goto :goto_0

    .line 731
    :cond_4
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardCameraPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    const v1, 0x7f020103

    :goto_1
    goto :goto_0

    :cond_5
    const v1, 0x7f020102

    goto :goto_1

    .line 735
    :cond_6
    invoke-static {v0}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 736
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_7

    .line 737
    const v1, 0x7f0201ac

    goto :goto_0

    .line 739
    :cond_7
    const v1, 0x7f020119

    goto :goto_0

    .line 742
    .end local v0    # "path":Ljava/lang/String;
    :cond_8
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSDCardPath(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 743
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_9

    .line 744
    const v1, 0x7f0201ac

    goto :goto_0

    .line 746
    :cond_9
    const v1, 0x7f020119

    goto :goto_0

    .line 749
    :cond_a
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/sns/SNSAlbum;

    if-nez v2, :cond_b

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v2, :cond_d

    .line 750
    :cond_b
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_c

    .line 751
    const v1, 0x7f0201b0

    goto :goto_0

    .line 753
    :cond_c
    const v1, 0x7f0200da

    goto :goto_0

    .line 755
    :cond_d
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudAlbum;

    if-nez v2, :cond_e

    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudMergeAlbum;

    if-nez v2, :cond_e

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v2, :cond_e

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/cloud/CloudVideo;

    if-eqz v2, :cond_11

    .line 757
    :cond_e
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseBaidu:Z

    if-nez v2, :cond_10

    .line 758
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_f

    .line 759
    const v1, 0x7f0201af

    goto :goto_0

    .line 761
    :cond_f
    const v1, 0x7f0200d9

    goto :goto_0

    .line 764
    :cond_10
    const v1, 0x7f0200d8

    goto :goto_0

    .line 765
    :cond_11
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    if-nez v2, :cond_12

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-eqz v2, :cond_14

    .line 766
    :cond_12
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_13

    .line 767
    const v1, 0x7f0201b3

    goto/16 :goto_0

    .line 769
    :cond_13
    const v1, 0x7f0201e2

    goto/16 :goto_0

    .line 771
    :cond_14
    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudAlbum;

    if-nez v2, :cond_15

    instance-of v2, p1, Lcom/sec/android/gallery3d/remote/tCloud/TCloudMergeAlbum;

    if-nez v2, :cond_15

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudImage;

    if-nez v2, :cond_15

    instance-of v2, p2, Lcom/sec/android/gallery3d/remote/tCloud/TCloudVideo;

    if-eqz v2, :cond_2

    .line 774
    :cond_15
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v2, :cond_16

    .line 775
    const v1, 0x7f0201b4

    goto/16 :goto_0

    .line 777
    :cond_16
    const v1, 0x7f0200dd

    goto/16 :goto_0
.end method


# virtual methods
.method public addShowGroupId(Ljava/lang/Long;)Z
    .locals 1
    .param p1, "groupId"    # Ljava/lang/Long;

    .prologue
    .line 1307
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->addShowGroupId(Ljava/lang/Long;)Z

    move-result v0

    return v0
.end method

.method public changeToNewAlbumHeader(Z)V
    .locals 0
    .param p1, "isNewAlbumHeader"    # Z

    .prologue
    .line 123
    return-void
.end method

.method public clearShowGroupId()V
    .locals 1

    .prologue
    .line 1315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->clearShowGroupId()V

    .line 1316
    return-void
.end method

.method public drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V
    .locals 24
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "sizeType"    # I
    .param p3, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 996
    move-object/from16 v0, p3

    iget-object v10, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 997
    .local v10, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p3

    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemCount:I

    .line 998
    .local v7, "count":I
    if-eqz v10, :cond_3

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    .line 1008
    .local v4, "albumName":Ljava/lang/String;
    :goto_0
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceTag:Z

    if-eqz v20, :cond_0

    instance-of v0, v10, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    move/from16 v20, v0

    if-nez v20, :cond_0

    instance-of v0, v10, Lcom/sec/android/gallery3d/remote/picasa/PicasaAlbum;

    move/from16 v20, v0

    if-nez v20, :cond_0

    .line 1010
    if-eqz v4, :cond_0

    const-string v20, "/"

    move-object/from16 v0, v20

    invoke-virtual {v4, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 1011
    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    .line 1012
    .local v19, "values":[Ljava/lang/String;
    const/16 v20, 0x1

    aget-object v4, v19, v20

    .line 1016
    .end local v19    # "values":[Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d0304

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 1017
    .local v5, "checkHMarginPixel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d02e3

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v6

    .line 1018
    .local v6, "checkboxWidthPixel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d0321

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v17

    .line 1020
    .local v17, "titleTextMarginLeftPixel":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d0322

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 1021
    .local v14, "textSize":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v18

    .line 1022
    .local v18, "titleWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d031c

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 1023
    .local v16, "titleHeight":I
    sub-int v20, v18, v17

    sub-int v20, v20, v6

    sub-int v8, v20, v5

    .line 1024
    .local v8, "elipsize":I
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v20, :cond_4

    .line 1025
    const/high16 v13, -0x1000000

    .line 1030
    .local v13, "textColor_title":I
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1

    .line 1031
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v20, :cond_5

    .line 1032
    const/high16 v13, -0x1000000

    .line 1036
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d031d

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 1037
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0d0323

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v14

    .line 1040
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v0, v7}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getAlbumNumber(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v12

    .line 1041
    .local v12, "photoNumlabel":Ljava/lang/String;
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 1043
    .local v15, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    int-to-float v0, v14

    move/from16 v20, v0

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getDefaultPaintForLabel(F)Landroid/text/TextPaint;

    move-result-object v11

    .line 1044
    .local v11, "paint":Landroid/text/TextPaint;
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v11, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 1045
    const/4 v9, 0x0

    .line 1046
    .local v9, "label":Ljava/lang/String;
    int-to-float v0, v8

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v4, v12, v0, v11}, Lcom/sec/android/gallery3d/glcore/GlTextView;->ellipsizeString(Ljava/lang/String;Ljava/lang/String;FLandroid/text/TextPaint;)Ljava/lang/String;

    move-result-object v9

    .line 1047
    if-nez v15, :cond_6

    .line 1048
    int-to-float v0, v14

    move/from16 v20, v0

    move/from16 v0, v20

    invoke-static {v9, v0, v13}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v15

    .line 1049
    const/16 v20, 0x1

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 1050
    const/16 v20, 0x0

    invoke-virtual {v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/text/TextPaint;->descent()F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move/from16 v0, v20

    move/from16 v1, v21

    move/from16 v2, v22

    move/from16 v3, v23

    invoke-virtual {v15, v0, v1, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setMargine(IIII)V

    .line 1051
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    .line 1052
    const/16 v20, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-virtual {v0, v15, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1058
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mIsRTL:Z

    move/from16 v20, v0

    if-eqz v20, :cond_2

    .line 1059
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setIsRTL(Z)V

    .line 1060
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v20

    sget-object v21, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual/range {v20 .. v21}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1062
    :cond_2
    move/from16 v0, v18

    move/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setSize(II)V

    .line 1063
    return-void

    .line 998
    .end local v4    # "albumName":Ljava/lang/String;
    .end local v5    # "checkHMarginPixel":I
    .end local v6    # "checkboxWidthPixel":I
    .end local v8    # "elipsize":I
    .end local v9    # "label":Ljava/lang/String;
    .end local v11    # "paint":Landroid/text/TextPaint;
    .end local v12    # "photoNumlabel":Ljava/lang/String;
    .end local v13    # "textColor_title":I
    .end local v14    # "textSize":I
    .end local v15    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .end local v16    # "titleHeight":I
    .end local v17    # "titleTextMarginLeftPixel":I
    .end local v18    # "titleWidth":I
    :cond_3
    const-string v4, ""

    goto/16 :goto_0

    .line 1027
    .restart local v4    # "albumName":Ljava/lang/String;
    .restart local v5    # "checkHMarginPixel":I
    .restart local v6    # "checkboxWidthPixel":I
    .restart local v8    # "elipsize":I
    .restart local v14    # "textSize":I
    .restart local v16    # "titleHeight":I
    .restart local v17    # "titleTextMarginLeftPixel":I
    .restart local v18    # "titleWidth":I
    :cond_4
    const v13, -0xa0a0b

    .restart local v13    # "textColor_title":I
    goto/16 :goto_1

    .line 1034
    :cond_5
    const v13, -0xa0a0b

    goto/16 :goto_2

    .line 1054
    .restart local v9    # "label":Ljava/lang/String;
    .restart local v11    # "paint":Landroid/text/TextPaint;
    .restart local v12    # "photoNumlabel":Ljava/lang/String;
    .restart local v15    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_6
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v20

    int-to-float v0, v14

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Landroid/text/TextPaint;->setTextSize(F)V

    .line 1055
    invoke-virtual {v15, v9}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    .line 1056
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextPaint()Landroid/text/TextPaint;

    move-result-object v20

    const/16 v21, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->setTextViewPaint(Landroid/text/TextPaint;Z)V

    goto :goto_3
.end method

.method protected drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 2
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "sizeType"    # I

    .prologue
    .line 980
    if-nez p1, :cond_0

    .line 981
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 992
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_0
    move-object v0, p1

    .line 983
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0
.end method

.method protected drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 7
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "visible"    # Z

    .prologue
    const/4 v6, 0x2

    const/16 v5, 0xd

    .line 1102
    const/4 v0, 0x0

    .line 1104
    .local v0, "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz p2, :cond_2

    .line 1105
    if-nez p1, :cond_1

    .line 1106
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1110
    .restart local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :goto_0
    if-nez v0, :cond_0

    .line 1111
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02026e

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 1113
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1114
    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1115
    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setScaleRatio(F)V

    .line 1116
    invoke-virtual {v0, v6, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 1117
    invoke-virtual {p1, v0, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1126
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    :goto_1
    return-object p1

    .line 1108
    :cond_1
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 1119
    :cond_2
    if-eqz p1, :cond_0

    .line 1120
    invoke-virtual {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .end local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1121
    .restart local v0    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v0, :cond_0

    .line 1122
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_1
.end method

.method protected drawCheckBox(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 5
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 963
    if-nez p1, :cond_0

    .line 964
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 968
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_1

    const v2, 0x7f020122

    .line 969
    .local v2, "checkOnId":I
    :goto_1
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v3, :cond_2

    const v1, 0x7f020121

    .line 970
    .local v1, "checkOffId":I
    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v4, p2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 971
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 974
    :goto_3
    return-object v0

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v1    # "checkOffId":I
    .end local v2    # "checkOnId":I
    :cond_0
    move-object v0, p1

    .line 966
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 968
    :cond_1
    const v2, 0x7f0204ad

    goto :goto_1

    .line 969
    .restart local v2    # "checkOnId":I
    :cond_2
    const v1, 0x7f0204a6

    goto :goto_2

    .line 973
    .restart local v1    # "checkOffId":I
    :cond_3
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    goto :goto_3
.end method

.method public drawDecorViewOnRequest(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 40
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 449
    const/4 v8, 0x0

    .line 450
    .local v8, "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-static/range {p2 .. p2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v22

    .line 451
    .local v22, "isPanorama":Z
    const-wide/16 v38, 0x20

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v37

    if-eqz v37, :cond_2

    const-wide/32 v38, 0x40000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v37

    if-nez v37, :cond_2

    const/16 v16, 0x1

    .line 452
    .local v16, "isCinePic":Z
    :goto_0
    const-wide/16 v38, 0x10

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v27

    .line 453
    .local v27, "isSoundScene":Z
    const-wide/16 v38, 0x8

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v37

    if-nez v37, :cond_0

    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->is3DPanorama()Z

    move-result v37

    if-eqz v37, :cond_3

    :cond_0
    const/4 v11, 0x1

    .line 454
    .local v11, "is3DPanorama":Z
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v37

    if-nez v37, :cond_4

    const/16 v28, 0x1

    .line 455
    .local v28, "isUnconfirmFace":Z
    :goto_2
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Z

    move-result v15

    .line 456
    .local v15, "isBurstShot":Z
    const-wide/16 v38, 0x800

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v20

    .line 457
    .local v20, "isMagicShot":Z
    const-wide/16 v38, 0x1000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v14

    .line 458
    .local v14, "isBestPhoto":Z
    const-wide/16 v38, 0x2000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v13

    .line 459
    .local v13, "isBestFace":Z
    const-wide/16 v38, 0x4000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v19

    .line 460
    .local v19, "isEraser":Z
    const-wide/32 v38, 0x8000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v18

    .line 461
    .local v18, "isDramaShot":Z
    const-wide/32 v38, 0x10000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v24

    .line 462
    .local v24, "isPicMotion":Z
    const-wide/16 v38, 0x400

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v12

    .line 463
    .local v12, "is3DTour":Z
    const-wide/32 v38, 0x20000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v21

    .line 464
    .local v21, "isOutOfFocus":Z
    invoke-virtual/range {p2 .. p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v37

    invoke-static/range {v37 .. v37}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v25

    .line 465
    .local v25, "isSecret":Z
    sget-boolean v37, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePhotoNote:Z

    if-eqz v37, :cond_5

    const-wide/32 v38, 0x80000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v37

    if-eqz v37, :cond_5

    const/16 v23, 0x1

    .line 466
    .local v23, "isPhotoNote":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    move/from16 v17, v0

    .line 467
    .local v17, "isDeleteIcon":Z
    const-wide/32 v38, 0x40000

    move-object/from16 v0, p2

    move-wide/from16 v1, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v26

    .line 470
    .local v26, "isSequenceShot":Z
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v34

    .line 471
    .local v34, "rsrcPlayType":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mTimeMode:Z

    move/from16 v37, v0

    if-eqz v37, :cond_6

    const/16 v37, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, v37

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getContentTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;Z)I

    move-result v33

    .line 473
    .local v33, "rsrcMediaType":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0102

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v29

    .line 474
    .local v29, "leftMargine":I
    const/16 v30, 0x0

    .line 475
    .local v30, "marginFromStart":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0103

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 477
    .local v6, "bottomMargine":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-eqz v37, :cond_1

    .line 478
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0101

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v30

    .line 479
    move/from16 v29, v30

    .line 480
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0101

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 483
    :cond_1
    if-nez v33, :cond_7

    if-nez v34, :cond_7

    if-nez v22, :cond_7

    if-nez v16, :cond_7

    if-nez v27, :cond_7

    if-nez v11, :cond_7

    if-nez v15, :cond_7

    if-nez v28, :cond_7

    if-nez v20, :cond_7

    if-nez v12, :cond_7

    if-nez v25, :cond_7

    if-nez v23, :cond_7

    if-nez v17, :cond_7

    if-nez v14, :cond_7

    if-nez v13, :cond_7

    if-nez v19, :cond_7

    if-nez v18, :cond_7

    if-nez v24, :cond_7

    if-nez v21, :cond_7

    if-nez v26, :cond_7

    .line 486
    const/16 v37, 0x0

    .line 681
    :goto_5
    return-object v37

    .line 451
    .end local v6    # "bottomMargine":I
    .end local v11    # "is3DPanorama":Z
    .end local v12    # "is3DTour":Z
    .end local v13    # "isBestFace":Z
    .end local v14    # "isBestPhoto":Z
    .end local v15    # "isBurstShot":Z
    .end local v16    # "isCinePic":Z
    .end local v17    # "isDeleteIcon":Z
    .end local v18    # "isDramaShot":Z
    .end local v19    # "isEraser":Z
    .end local v20    # "isMagicShot":Z
    .end local v21    # "isOutOfFocus":Z
    .end local v23    # "isPhotoNote":Z
    .end local v24    # "isPicMotion":Z
    .end local v25    # "isSecret":Z
    .end local v26    # "isSequenceShot":Z
    .end local v27    # "isSoundScene":Z
    .end local v28    # "isUnconfirmFace":Z
    .end local v29    # "leftMargine":I
    .end local v30    # "marginFromStart":I
    .end local v33    # "rsrcMediaType":I
    .end local v34    # "rsrcPlayType":I
    :cond_2
    const/16 v16, 0x0

    goto/16 :goto_0

    .line 453
    .restart local v16    # "isCinePic":Z
    .restart local v27    # "isSoundScene":Z
    :cond_3
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 454
    .restart local v11    # "is3DPanorama":Z
    :cond_4
    const/16 v28, 0x0

    goto/16 :goto_2

    .line 465
    .restart local v12    # "is3DTour":Z
    .restart local v13    # "isBestFace":Z
    .restart local v14    # "isBestPhoto":Z
    .restart local v15    # "isBurstShot":Z
    .restart local v18    # "isDramaShot":Z
    .restart local v19    # "isEraser":Z
    .restart local v20    # "isMagicShot":Z
    .restart local v21    # "isOutOfFocus":Z
    .restart local v24    # "isPicMotion":Z
    .restart local v25    # "isSecret":Z
    .restart local v28    # "isUnconfirmFace":Z
    :cond_5
    const/16 v23, 0x0

    goto/16 :goto_3

    .line 471
    .restart local v17    # "isDeleteIcon":Z
    .restart local v23    # "isPhotoNote":Z
    .restart local v26    # "isSequenceShot":Z
    .restart local v34    # "rsrcPlayType":I
    :cond_6
    const/16 v33, 0x0

    goto/16 :goto_4

    .line 488
    .restart local v6    # "bottomMargine":I
    .restart local v29    # "leftMargine":I
    .restart local v30    # "marginFromStart":I
    .restart local v33    # "rsrcMediaType":I
    :cond_7
    if-nez v8, :cond_8

    .line 489
    new-instance v8, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v8    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-direct {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 491
    .restart local v8    # "decorView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_8
    const/16 v37, 0x3

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v31

    check-cast v31, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 492
    .local v31, "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v34, :cond_16

    .line 493
    if-nez v31, :cond_9

    .line 494
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move/from16 v2, v34

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 495
    .local v10, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v31, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v31    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 496
    .restart local v31    # "playView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v31

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 497
    const/16 v37, 0x2

    const/16 v38, 0x2

    move-object/from16 v0, v31

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 499
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    move-object/from16 v0, v31

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 500
    const/16 v37, 0x3

    move-object/from16 v0, v31

    move/from16 v1, v37

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 506
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_9
    :goto_6
    const/16 v37, 0x5

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 507
    .local v7, "cntTypeView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v33, :cond_17

    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move/from16 v2, v33

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 509
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v7, :cond_a

    .line 510
    new-instance v7, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v7    # "cntTypeView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-direct {v7, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 511
    .restart local v7    # "cntTypeView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/16 v37, 0x1

    const/16 v38, 0x3

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 512
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v7, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 513
    const/16 v37, 0x5

    move/from16 v0, v37

    invoke-virtual {v8, v7, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 515
    :cond_a
    invoke-virtual {v7, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 516
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v29

    .line 521
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_b
    :goto_7
    const/16 v37, 0xa

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v35

    check-cast v35, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 522
    .local v35, "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v25, :cond_19

    .line 523
    if-nez v35, :cond_c

    .line 524
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    move/from16 v37, v0

    if-eqz v37, :cond_18

    .line 525
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0201a9

    invoke-virtual/range {v37 .. v39}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 528
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_8
    new-instance v35, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v35    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v35

    move-object/from16 v1, v37

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 529
    .restart local v35    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v35

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 530
    const/16 v37, 0x1

    const/16 v38, 0x3

    move-object/from16 v0, v35

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 531
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    move-object/from16 v0, v35

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 532
    const/16 v37, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 533
    const/16 v37, 0xa

    move-object/from16 v0, v35

    move/from16 v1, v37

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 535
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_c
    const/16 v37, 0x0

    const/16 v38, 0x0

    move-object/from16 v0, v35

    move/from16 v1, v29

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-virtual {v0, v1, v2, v3, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 536
    invoke-virtual/range {v35 .. v35}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v37

    add-int v37, v37, v30

    add-int v29, v29, v37

    .line 541
    :cond_d
    :goto_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    move/from16 v37, v0

    if-eqz v37, :cond_2a

    .line 542
    if-eqz v11, :cond_1a

    .line 543
    const v32, 0x7f0200fb

    .line 544
    .local v32, "rsrcId":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-nez v37, :cond_e

    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0102

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 623
    :cond_e
    :goto_a
    const/16 v37, 0x4

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 624
    .local v4, "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v32, :cond_3b

    .line 625
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move/from16 v2, v32

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 626
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    if-nez v4, :cond_f

    .line 627
    new-instance v4, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v4    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-direct {v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 628
    .restart local v4    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/16 v37, 0x1

    const/16 v38, 0x3

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 629
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 630
    const/16 v37, 0x4

    move/from16 v0, v37

    invoke-virtual {v8, v4, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 632
    :cond_f
    invoke-virtual {v4, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 633
    const/16 v37, 0x0

    const/16 v38, 0x0

    move/from16 v0, v29

    move/from16 v1, v37

    move/from16 v2, v38

    invoke-virtual {v4, v0, v1, v2, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 634
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-nez v37, :cond_10

    .line 635
    const/4 v6, 0x0

    .line 636
    :cond_10
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v37

    add-int v37, v37, v30

    add-int v29, v29, v37

    .line 641
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_11
    :goto_b
    const/16 v37, 0x8

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v36

    check-cast v36, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 642
    .local v36, "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v28, :cond_3d

    .line 643
    if-nez v36, :cond_12

    .line 644
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v37

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0200de

    invoke-virtual/range {v37 .. v39}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 645
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v36, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v36    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-direct/range {v36 .. v37}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 646
    .restart local v36    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, v36

    invoke-virtual {v0, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 647
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-eqz v37, :cond_3c

    .line 648
    const/16 v37, 0x3

    const/16 v38, 0x3

    invoke-virtual/range {v36 .. v38}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 652
    :goto_c
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    invoke-virtual/range {v36 .. v38}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 653
    const/16 v37, 0x0

    invoke-virtual/range {v36 .. v37}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 654
    const/16 v37, 0x8

    move-object/from16 v0, v36

    move/from16 v1, v37

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 656
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_12
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-eqz v37, :cond_13

    .line 657
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0108

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 658
    .local v5, "bottomMargin":I
    const/16 v37, 0x0

    const/16 v38, 0x0

    move-object/from16 v0, v36

    move/from16 v1, v29

    move/from16 v2, v37

    move/from16 v3, v38

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 660
    .end local v5    # "bottomMargin":I
    :cond_13
    invoke-virtual/range {v36 .. v36}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getWidth()I

    move-result v37

    add-int v37, v37, v30

    add-int v29, v29, v37

    .line 665
    :cond_14
    :goto_d
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    check-cast v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 666
    .local v9, "deleteView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v17, :cond_3f

    .line 667
    if-nez v9, :cond_15

    .line 668
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    move/from16 v37, v0

    if-eqz v37, :cond_3e

    .line 669
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0200bd

    invoke-virtual/range {v37 .. v39}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .line 672
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_e
    new-instance v9, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v9    # "deleteView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    move-object/from16 v0, v37

    invoke-direct {v9, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 673
    .restart local v9    # "deleteView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v9, v10}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 674
    const/16 v37, 0x3

    const/16 v38, 0x1

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 675
    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v37

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v38

    move/from16 v0, v37

    move/from16 v1, v38

    invoke-virtual {v9, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 676
    const/16 v37, 0xb

    move/from16 v0, v37

    invoke-virtual {v8, v9, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_15
    :goto_f
    move-object/from16 v37, v8

    .line 681
    goto/16 :goto_5

    .line 502
    .end local v4    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v7    # "cntTypeView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v9    # "deleteView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v32    # "rsrcId":I
    .end local v35    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v36    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_16
    if-eqz v31, :cond_9

    .line 503
    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_6

    .line 517
    .restart local v7    # "cntTypeView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_17
    if-eqz v7, :cond_b

    .line 518
    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_7

    .line 527
    .restart local v35    # "secretIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f0200d7

    invoke-virtual/range {v37 .. v39}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_8

    .line 537
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_19
    if-eqz v35, :cond_d

    .line 538
    move-object/from16 v0, v35

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_9

    .line 547
    :cond_1a
    if-eqz v16, :cond_1b

    .line 548
    const v32, 0x7f0201a6

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 549
    .end local v32    # "rsrcId":I
    :cond_1b
    if-eqz v27, :cond_1c

    .line 550
    const v32, 0x7f0201ad

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 551
    .end local v32    # "rsrcId":I
    :cond_1c
    if-eqz v22, :cond_1d

    .line 552
    const v32, 0x7f0201a7

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 553
    .end local v32    # "rsrcId":I
    :cond_1d
    if-eqz v15, :cond_1e

    .line 554
    const v32, 0x7f02019f

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 555
    .end local v32    # "rsrcId":I
    :cond_1e
    if-eqz v20, :cond_1f

    .line 556
    const v32, 0x7f0201ab

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 557
    .end local v32    # "rsrcId":I
    :cond_1f
    if-eqz v14, :cond_20

    .line 558
    const v32, 0x7f02019e

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 559
    .end local v32    # "rsrcId":I
    :cond_20
    if-eqz v13, :cond_21

    .line 560
    const v32, 0x7f02019d

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 561
    .end local v32    # "rsrcId":I
    :cond_21
    if-eqz v19, :cond_22

    .line 562
    const v32, 0x7f0201a4

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 563
    .end local v32    # "rsrcId":I
    :cond_22
    if-eqz v18, :cond_23

    .line 564
    const v32, 0x7f0201a3

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 565
    .end local v32    # "rsrcId":I
    :cond_23
    if-eqz v24, :cond_24

    .line 566
    const v32, 0x7f0201a2

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 567
    .end local v32    # "rsrcId":I
    :cond_24
    if-eqz v12, :cond_25

    .line 568
    const v32, 0x7f0201ae

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 569
    .end local v32    # "rsrcId":I
    :cond_25
    if-eqz v23, :cond_26

    .line 570
    const v32, 0x7f0201a8

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 571
    .end local v32    # "rsrcId":I
    :cond_26
    if-eqz v21, :cond_27

    .line 572
    const v32, 0x7f0201a5

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 573
    .end local v32    # "rsrcId":I
    :cond_27
    if-eqz v26, :cond_29

    .line 574
    sget-boolean v37, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v37, :cond_28

    .line 575
    const v32, 0x7f0201aa

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 577
    .end local v32    # "rsrcId":I
    :cond_28
    const/16 v32, 0x0

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 580
    .end local v32    # "rsrcId":I
    :cond_29
    const/16 v32, 0x0

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 582
    .end local v32    # "rsrcId":I
    :cond_2a
    if-eqz v11, :cond_2b

    .line 583
    const v32, 0x7f0200fb

    .line 584
    .restart local v32    # "rsrcId":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v37

    if-nez v37, :cond_e

    .line 585
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v37

    const v38, 0x7f0d0102

    invoke-virtual/range {v37 .. v38}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    goto/16 :goto_a

    .line 587
    .end local v32    # "rsrcId":I
    :cond_2b
    if-eqz v16, :cond_2c

    .line 588
    const v32, 0x7f02010e

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 589
    .end local v32    # "rsrcId":I
    :cond_2c
    if-eqz v27, :cond_2d

    .line 590
    const v32, 0x7f02011c

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 591
    .end local v32    # "rsrcId":I
    :cond_2d
    if-eqz v22, :cond_2e

    .line 592
    const v32, 0x7f020111

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 593
    .end local v32    # "rsrcId":I
    :cond_2e
    if-eqz v15, :cond_2f

    .line 594
    const v32, 0x7f020100

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 595
    .end local v32    # "rsrcId":I
    :cond_2f
    if-eqz v20, :cond_30

    .line 596
    const v32, 0x7f020118

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 597
    .end local v32    # "rsrcId":I
    :cond_30
    if-eqz v14, :cond_31

    .line 598
    const v32, 0x7f0200ff

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 599
    .end local v32    # "rsrcId":I
    :cond_31
    if-eqz v13, :cond_32

    .line 600
    const v32, 0x7f0200fe

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 601
    .end local v32    # "rsrcId":I
    :cond_32
    if-eqz v19, :cond_33

    .line 602
    const v32, 0x7f020109

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 603
    .end local v32    # "rsrcId":I
    :cond_33
    if-eqz v18, :cond_34

    .line 604
    const v32, 0x7f020107

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 605
    .end local v32    # "rsrcId":I
    :cond_34
    if-eqz v24, :cond_35

    .line 606
    const v32, 0x7f020105

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 607
    .end local v32    # "rsrcId":I
    :cond_35
    if-eqz v12, :cond_36

    .line 608
    const v32, 0x7f0200fc

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 609
    .end local v32    # "rsrcId":I
    :cond_36
    if-eqz v23, :cond_37

    .line 610
    const v32, 0x7f020112

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 611
    .end local v32    # "rsrcId":I
    :cond_37
    if-eqz v21, :cond_38

    .line 612
    const v32, 0x7f02010b

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 613
    .end local v32    # "rsrcId":I
    :cond_38
    if-eqz v26, :cond_3a

    .line 614
    sget-boolean v37, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSequenceIconSupported:Z

    if-eqz v37, :cond_39

    .line 615
    const v32, 0x7f020117

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 617
    .end local v32    # "rsrcId":I
    :cond_39
    const/16 v32, 0x0

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 620
    .end local v32    # "rsrcId":I
    :cond_3a
    const/16 v32, 0x0

    .restart local v32    # "rsrcId":I
    goto/16 :goto_a

    .line 637
    .restart local v4    # "attrView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3b
    if-eqz v4, :cond_11

    .line 638
    invoke-virtual {v8, v4}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_b

    .line 650
    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    .restart local v36    # "unknowIconView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3c
    const/16 v37, 0x1

    const/16 v38, 0x3

    invoke-virtual/range {v36 .. v38}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    goto/16 :goto_c

    .line 661
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3d
    if-eqz v36, :cond_14

    .line 662
    move-object/from16 v0, v36

    invoke-virtual {v8, v0}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_d

    .line 671
    .restart local v9    # "deleteView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v37, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    move-object/from16 v38, v0

    invoke-virtual/range {v38 .. v38}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v38

    const v39, 0x7f020478

    invoke-virtual/range {v37 .. v39}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    .restart local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    goto/16 :goto_e

    .line 678
    .end local v10    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_3f
    if-eqz v9, :cond_15

    .line 679
    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto/16 :goto_f
.end method

.method protected drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;
    .locals 13
    .param p1, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "album"    # Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    .prologue
    .line 1066
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 1067
    .local v1, "dimensionUtil":Lcom/sec/samsung/gallery/util/DimensionUtil;
    iget-object v6, p2, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1071
    .local v6, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez p1, :cond_1

    .line 1072
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v5, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1073
    .local v5, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v2, 0x0

    .line 1074
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v9

    if-eqz v9, :cond_0

    .line 1075
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f020209

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1079
    :goto_0
    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1080
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v5, v9, v10, v11, v12}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setMargine(IIII)V

    .line 1084
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :goto_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v9, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    .line 1085
    .local v0, "count":I
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontColor()I

    move-result v3

    .line 1086
    .local v3, "fontColor":I
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumSelectCountFontSize()I

    move-result v4

    .line 1087
    .local v4, "fontSize":I
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    .line 1088
    .local v7, "selectedStr":Ljava/lang/String;
    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .line 1089
    .local v8, "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    if-nez v8, :cond_2

    .line 1090
    int-to-float v9, v4

    invoke-static {v7, v9, v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->newInstance(Ljava/lang/String;FI)Lcom/sec/android/gallery3d/glcore/GlTextView;

    move-result-object v8

    .line 1091
    const/4 v9, 0x2

    const/4 v10, 0x2

    invoke-virtual {v8, v9, v10}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setAlign(II)V

    .line 1092
    const/4 v9, 0x2

    invoke-virtual {v5, v8, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1096
    :goto_2
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mRetLevel:I

    .line 1097
    return-object v5

    .line 1077
    .end local v0    # "count":I
    .end local v3    # "fontColor":I
    .end local v4    # "fontSize":I
    .end local v7    # "selectedStr":Ljava/lang/String;
    .end local v8    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    .restart local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v10}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f020208

    invoke-virtual {v9, v10, v11}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0

    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    .end local v5    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_1
    move-object v5, p1

    .line 1082
    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v5    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1

    .line 1094
    .restart local v0    # "count":I
    .restart local v3    # "fontColor":I
    .restart local v4    # "fontSize":I
    .restart local v7    # "selectedStr":Ljava/lang/String;
    .restart local v8    # "selectedTextView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    :cond_2
    invoke-virtual {v8, v7}, Lcom/sec/android/gallery3d/glcore/GlTextView;->setText(Ljava/lang/String;)V

    goto :goto_2
.end method

.method protected drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "imageItem"    # Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p4, "subPosition"    # I
    .param p5, "totalCount"    # I

    .prologue
    .line 933
    const/4 v0, 0x0

    .line 935
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 936
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 937
    const/4 v2, 0x0

    .line 957
    :goto_0
    return v2

    .line 939
    :cond_1
    iget-object v0, p2, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 940
    if-nez v0, :cond_2

    .line 941
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 957
    :goto_1
    const/4 v2, 0x1

    goto :goto_0

    .line 943
    :cond_2
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    .line 944
    .local v1, "rotation":I
    invoke-virtual {p1, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    goto :goto_1
.end method

.method protected drawTitleBorder(Lcom/sec/android/gallery3d/glcore/GlView;ZI)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 9
    .param p1, "glView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p2, "visible"    # Z
    .param p3, "textResId"    # I

    .prologue
    const/16 v8, 0xd

    .line 1131
    const/4 v1, 0x0

    .line 1132
    .local v1, "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const/4 v3, 0x0

    .line 1133
    .local v3, "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    const/4 v4, 0x0

    .line 1134
    .local v4, "textWidth":I
    const/4 v0, 0x0

    .line 1135
    .local v0, "borderHeight":I
    if-eqz p2, :cond_4

    .line 1136
    if-nez p1, :cond_2

    .line 1137
    new-instance p1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {p1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1142
    .restart local p1    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :goto_0
    if-nez v1, :cond_1

    .line 1143
    if-nez v3, :cond_3

    const/4 v4, 0x0

    .line 1144
    :goto_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02026e

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 1145
    .local v2, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 1146
    .restart local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1147
    const/high16 v5, 0x3f000000    # 0.5f

    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setScaleRatio(F)V

    .line 1148
    const/4 v5, 0x1

    const/4 v6, 0x2

    invoke-virtual {v1, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 1149
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlView;->getHeight()I

    move-result v0

    .line 1150
    if-nez v0, :cond_0

    .line 1151
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0d031c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 1153
    :cond_0
    invoke-virtual {v1, v4, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 1154
    invoke-virtual {p1, v1, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 1163
    .end local v2    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_2
    return-object p1

    .line 1139
    :cond_2
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    .end local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1140
    .restart local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, p3}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    .end local v3    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlTextView;

    .restart local v3    # "textView":Lcom/sec/android/gallery3d/glcore/GlTextView;
    goto :goto_0

    .line 1143
    :cond_3
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getTextWidth()I

    move-result v4

    goto :goto_1

    .line 1156
    :cond_4
    if-eqz p1, :cond_1

    .line 1157
    invoke-virtual {p1, v8}, Lcom/sec/android/gallery3d/glcore/GlView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    .end local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 1158
    .restart local v1    # "borderView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v1, :cond_1

    .line 1159
    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlView;->removeChild(Lcom/sec/android/gallery3d/glcore/GlView;)Z

    goto :goto_2
.end method

.method public forceReload()V
    .locals 1

    .prologue
    .line 1330
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->forceReload()V

    .line 1331
    return-void
.end method

.method public getAlbumIndexFromFilePath(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1285
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAlbumIndexFromFilePath(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1289
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAlbumIndexFromMediaSet(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v0

    return v0
.end method

.method public getAllCount()I
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllCount()I

    move-result v0

    return v0
.end method

.method public getAllItem(I)Ljava/util/ArrayList;
    .locals 1
    .param p1, "albumIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 814
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItem(I)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAllItemCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 819
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getAllItemCount(I)I

    move-result v0

    return v0
.end method

.method public getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount()I

    move-result v0

    return v0
.end method

.method public getCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 794
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getCount(I)I

    move-result v0

    return v0
.end method

.method public getCurrentState(II)I
    .locals 9
    .param p1, "position"    # I
    .param p2, "subPosition"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 136
    const/4 v3, 0x0

    .line 138
    .local v3, "retVal":I
    if-ltz p1, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v7, :cond_2

    .line 139
    :cond_0
    const-string v5, "ComposeMediaItemAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState pos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", size = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v8, v8, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_1
    :goto_0
    return v6

    .line 142
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v7, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v0, v7, p1

    .line 143
    .local v0, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v0, :cond_1

    .line 146
    if-gez p2, :cond_7

    .line 147
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v7, v8}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getSetSelectedCount(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v1

    .line 148
    .local v1, "count":I
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mAllSelectMode:Z

    if-eqz v7, :cond_4

    iget v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-lt v1, v7, :cond_3

    move v4, v5

    .line 149
    .local v4, "selected":Z
    :goto_1
    if-eqz v4, :cond_6

    :goto_2
    move v6, v5

    goto :goto_0

    .end local v4    # "selected":Z
    :cond_3
    move v4, v6

    .line 148
    goto :goto_1

    :cond_4
    if-lez v1, :cond_5

    move v4, v5

    goto :goto_1

    :cond_5
    move v4, v6

    goto :goto_1

    .restart local v4    # "selected":Z
    :cond_6
    move v5, v6

    .line 149
    goto :goto_2

    .line 151
    .end local v1    # "count":I
    .end local v4    # "selected":Z
    :cond_7
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v7, v7

    if-lt p2, v7, :cond_8

    .line 152
    const-string v5, "ComposeMediaItemAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState subPos = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", length = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    array-length v8, v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 155
    :cond_8
    iget-object v7, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v2, v7, p2

    .line 156
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_9

    .line 157
    const-string v5, "ComposeMediaItemAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getSelectState mediaItem is null = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ", sub = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 160
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v7, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v7

    if-eqz v7, :cond_b

    move v3, v5

    .line 161
    :goto_3
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v5, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v6, 0x2

    :cond_a
    or-int/2addr v3, v6

    move v6, v3

    .line 162
    goto/16 :goto_0

    :cond_b
    move v3, v6

    .line 160
    goto :goto_3
.end method

.method public getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 809
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "photoIndex"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getImage(II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getLineCount(I)I
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 804
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getLineCount(I)I

    move-result v0

    return v0
.end method

.method public getMediaSetName(I)Ljava/lang/String;
    .locals 4
    .param p1, "albumIndex"    # I

    .prologue
    .line 1345
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 1348
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_1

    .line 1349
    const/4 v0, 0x0

    .line 1361
    :cond_0
    :goto_0
    return-object v0

    .line 1351
    :cond_1
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v0

    .line 1352
    .local v0, "groupName":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 1353
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1355
    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 1356
    .local v2, "strList":[Ljava/lang/String;
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v2, v3

    goto :goto_0
.end method

.method protected getNewAlbumPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 3
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 702
    if-nez p1, :cond_1

    .line 703
    const/4 v0, 0x0

    .line 714
    :cond_0
    :goto_0
    return v0

    .line 706
    :cond_1
    const/4 v0, 0x0

    .line 708
    .local v0, "rsrcPlayType":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v1, :cond_0

    .line 709
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v1, :cond_2

    .line 710
    const v0, 0x7f020195

    goto :goto_0

    .line 712
    :cond_2
    const v0, 0x7f0200f8

    goto :goto_0
.end method

.method protected getPlayTypeIconRsrc(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 3
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 686
    if-nez p1, :cond_1

    .line 687
    const/4 v0, 0x0

    .line 698
    :cond_0
    :goto_0
    return v0

    .line 690
    :cond_1
    const/4 v0, 0x0

    .line 692
    .local v0, "rsrcPlayType":I
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v1, :cond_0

    .line 693
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mEasyMode:Z

    if-eqz v1, :cond_2

    .line 694
    const v0, 0x7f020194

    goto :goto_0

    .line 696
    :cond_2
    const v0, 0x7f0200f7

    goto :goto_0
.end method

.method public getScreenNailImage()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-nez v0, :cond_0

    .line 868
    const/4 v0, 0x0

    .line 869
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public getShowGroupId()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1311
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getShowGroupId()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getSource()Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method public getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "albumIndex"    # I

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    return-object v0
.end method

.method public getView(IILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 8
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p4, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p5, "ext"    # I
    .param p6, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 888
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mRetLevel:I

    .line 889
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    if-lt p1, v0, :cond_1

    .line 928
    .end local p6    # "obj":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-object v1

    .line 892
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v6, v0, p1

    .line 893
    .local v6, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    if-eqz v6, :cond_0

    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-eqz v0, :cond_0

    .line 895
    if-gez p2, :cond_4

    .line 896
    const/16 v0, -0xa

    if-ne p2, v0, :cond_2

    .line 897
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawCheckBox(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 904
    .local v1, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_1
    instance-of v0, p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v0, :cond_0

    .line 905
    check-cast p6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .end local p6    # "obj":Ljava/lang/Object;
    invoke-virtual {p6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getFocusBorderVisible()Z

    move-result v0

    invoke-virtual {p0, v1, v0, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawTitleBorder(Lcom/sec/android/gallery3d/glcore/GlView;ZI)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0

    .line 898
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .restart local p6    # "obj":Ljava/lang/Object;
    :cond_2
    const/4 v0, -0x4

    if-ne p2, v0, :cond_3

    .line 899
    invoke-virtual {p0, p3, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawSelectedCountView(Lcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_1

    .line 901
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    invoke-virtual {p0, p3, p2}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawAlbumLabelBG(Lcom/sec/android/gallery3d/glcore/GlView;I)Lcom/sec/android/gallery3d/glcore/GlImageView;

    move-result-object v1

    .line 902
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p0, v1, p2, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawAlbumLabel(Lcom/sec/android/gallery3d/glcore/GlImageView;ILcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;)V

    goto :goto_1

    .line 909
    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_4
    iget v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    if-ge p2, v0, :cond_0

    .line 912
    if-nez p3, :cond_5

    .line 913
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 918
    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_2
    if-ne p5, v4, :cond_6

    .line 919
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v2, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSlideImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 920
    .local v2, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v3

    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    goto :goto_0

    .end local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    .end local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    :cond_5
    move-object v1, p3

    .line 915
    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v1    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_2

    .line 922
    :cond_6
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v3, v0, p2

    .line 923
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v2, v0, p2

    .line 924
    .restart local v2    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    iget v5, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawThumbnailImage(Lcom/sec/android/gallery3d/glcore/GlImageView;Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;Lcom/sec/android/gallery3d/data/MediaItem;II)Z

    move-result v7

    .line 925
    .local v7, "result":Z
    if-eqz v7, :cond_0

    .line 926
    iget-object v0, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mUpdated:[B

    aget-byte v0, v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mRetLevel:I

    goto :goto_0
.end method

.method public getViewInfo(IIILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Z)Z
    .locals 21
    .param p1, "position"    # I
    .param p2, "subPosition"    # I
    .param p3, "option"    # I
    .param p4, "inter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;
    .param p5, "borderVisible"    # Z

    .prologue
    .line 167
    const/4 v10, 0x0

    .line 172
    .local v10, "album":Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;
    const/4 v13, 0x0

    .line 175
    .local v13, "cropRect":Landroid/graphics/Rect;
    if-nez p4, :cond_0

    const/4 v4, 0x0

    .line 276
    :goto_0
    return v4

    .line 176
    :cond_0
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 177
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 178
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 179
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mReorderEnable:Z

    .line 180
    if-eqz p3, :cond_6

    .line 181
    const/16 v19, 0x0

    .line 182
    .local v19, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    move-object/from16 v17, v0

    .line 183
    .local v17, "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    invoke-virtual/range {v17 .. v17}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v18

    .line 184
    .local v18, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    .line 185
    .local v11, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 217
    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mSelected:Z

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    if-eqz v4, :cond_e

    const/4 v4, 0x0

    :goto_2
    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDisabled:Z

    .line 220
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 221
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_f

    .line 222
    const/4 v4, 0x2

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    .line 236
    :cond_1
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPickMode(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_2

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mAlbumMode:Z

    if-eqz v4, :cond_11

    .line 237
    :cond_2
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    .line 242
    :goto_4
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mAlbumMode:Z

    if-eqz v4, :cond_13

    .line 243
    move-object/from16 v0, p0

    move-object/from16 v1, p4

    move-object/from16 v2, v19

    move-object/from16 v3, v18

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawDecorViewAlbum(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 250
    :cond_3
    :goto_5
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v0, p0

    move/from16 v1, p5

    invoke-virtual {v0, v4, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->drawBorder(Lcom/sec/android/gallery3d/glcore/GlView;Z)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    .line 252
    if-eqz v11, :cond_14

    move-object/from16 v0, p4

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    if-nez v4, :cond_14

    .line 253
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v20

    .line 254
    .local v20, "width":I
    invoke-virtual {v11}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v16

    .line 256
    .local v16, "height":I
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFaceThumbnail:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, v18

    instance-of v4, v0, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v4, :cond_4

    invoke-static/range {v18 .. v18}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isPanorama(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v4

    if-nez v4, :cond_4

    if-eqz v17, :cond_4

    move-object/from16 v0, v17

    iget v4, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mThmType:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_4

    .line 260
    check-cast v18, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/LocalImage;->getRectOfAllFaces()Landroid/graphics/RectF;

    move-result-object v15

    .line 261
    .local v15, "faceRect":Landroid/graphics/RectF;
    if-eqz v15, :cond_4

    .line 262
    move-object/from16 v0, p4

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    move/from16 v0, v20

    move/from16 v1, v16

    invoke-static {v0, v1, v15, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->calcFaceCropRect(IILandroid/graphics/RectF;I)Landroid/graphics/Rect;

    move-result-object v13

    .line 265
    .end local v15    # "faceRect":Landroid/graphics/RectF;
    :cond_4
    if-nez v13, :cond_5

    .line 266
    move/from16 v0, v20

    int-to-float v5, v0

    move/from16 v0, v16

    int-to-float v6, v0

    move-object/from16 v0, p4

    iget v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->calcCenterCropRect(FFFFI)Landroid/graphics/Rect;

    move-result-object v13

    .line 268
    :cond_5
    move-object/from16 v0, p4

    iput-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 270
    move-object/from16 v0, p4

    iput-object v11, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 276
    .end local v16    # "height":I
    .end local v20    # "width":I
    :goto_6
    const/4 v4, 0x1

    goto/16 :goto_0

    .line 187
    .end local v11    # "bitmap":Landroid/graphics/Bitmap;
    .end local v17    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    .end local v19    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_6
    if-ltz p1, :cond_7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    move/from16 v0, p1

    if-lt v0, v4, :cond_8

    .line 188
    :cond_7
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo pos = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", size = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget v6, v6, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSize:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 193
    :cond_8
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mAlbumSet:[Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;

    aget-object v10, v4, p1

    .line 194
    if-eqz v10, :cond_9

    iget v4, v10, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    move/from16 v0, p2

    if-lt v0, v4, :cond_b

    .line 195
    :cond_9
    if-nez v10, :cond_a

    .line 196
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo album is null = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :goto_7
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 198
    :cond_a
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo position is over index  = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", length = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v10, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mCount:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_7

    .line 208
    :catch_0
    move-exception v14

    .line 209
    .local v14, "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    invoke-virtual {v14}, Ljava/lang/ArrayIndexOutOfBoundsException;->printStackTrace()V

    .line 210
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 203
    .end local v14    # "e":Ljava/lang/ArrayIndexOutOfBoundsException;
    :cond_b
    :try_start_1
    iget-object v4, v10, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaItem:[Lcom/sec/android/gallery3d/data/MediaItem;

    aget-object v18, v4, p2

    .line 204
    .restart local v18    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v18, :cond_c

    .line 205
    const-string v4, "ComposeMediaItemAdapter"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "GetViewInfo mediaItem is null = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p1

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", sub = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move/from16 v0, p2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_0

    .line 206
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 212
    :cond_c
    invoke-virtual/range {v18 .. v18}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v4

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    .line 213
    iget-object v0, v10, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    move-object/from16 v19, v0

    .line 214
    .restart local v19    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v4, v10, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$AlbumInfo;->mItemImage:[Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    aget-object v17, v4, p2

    .line 215
    .restart local v17    # "imageItem":Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;
    if-nez v17, :cond_d

    const/4 v11, 0x0

    .restart local v11    # "bitmap":Landroid/graphics/Bitmap;
    :goto_8
    goto/16 :goto_1

    .end local v11    # "bitmap":Landroid/graphics/Bitmap;
    :cond_d
    move-object/from16 v0, v17

    iget-object v11, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mBitmap:Landroid/graphics/Bitmap;

    goto :goto_8

    .line 218
    .restart local v11    # "bitmap":Landroid/graphics/Bitmap;
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v4

    goto/16 :goto_2

    .line 224
    :cond_f
    const/4 v4, 0x1

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    goto/16 :goto_3

    .line 226
    :cond_10
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    .line 227
    if-nez v11, :cond_1

    .line 230
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 231
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDynamicCropRect:Landroid/graphics/Rect;

    .line 232
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    .line 233
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 239
    :cond_11
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-boolean v4, v4, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSelectionSet:Z

    if-nez v4, :cond_12

    const/4 v4, 0x1

    :goto_9
    move-object/from16 v0, p4

    iput-boolean v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDispExpansionIcon:Z

    goto/16 :goto_4

    :cond_12
    const/4 v4, 0x0

    goto :goto_9

    .line 245
    :cond_13
    move-object/from16 v0, p4

    iget-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-nez v4, :cond_3

    if-eqz v17, :cond_3

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    if-eqz v4, :cond_3

    .line 246
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->mGlView:Lcom/sec/android/gallery3d/glcore/GlView;

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mDecorView:Lcom/sec/android/gallery3d/glcore/GlView;

    goto/16 :goto_5

    .line 272
    :cond_14
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResourceMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mResources:Landroid/content/res/Resources;

    move-object/from16 v0, p4

    iget v6, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mIsBroken:I

    invoke-virtual {v4, v5, v6}, Lcom/sec/android/gallery3d/util/ResourceManager;->getBrokenThumnailSize(Landroid/content/res/Resources;I)I

    move-result v12

    .line 273
    .local v12, "brokenThumnailsize":I
    int-to-float v5, v12

    int-to-float v6, v12

    move-object/from16 v0, p4

    iget v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjWidth:F

    move-object/from16 v0, p4

    iget v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mObjHeight:F

    move-object/from16 v0, p4

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mRotation:I

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->calcCenterCropRect(FFFFI)Landroid/graphics/Rect;

    move-result-object v4

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mCropRect:Landroid/graphics/Rect;

    .line 274
    const/4 v4, 0x0

    move-object/from16 v0, p4

    iput-object v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$AdapterInterface;->mBitmap:Landroid/graphics/Bitmap;

    goto/16 :goto_6
.end method

.method public hasLocalMediaSet()Z
    .locals 6

    .prologue
    .line 1271
    const/4 v3, 0x0

    .line 1272
    .local v3, "result":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v4, v5, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mSource:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 1273
    .local v4, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 1274
    .local v0, "albumCount":I
    const/4 v1, 0x0

    .local v1, "ix":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1275
    invoke-virtual {v4, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 1276
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalAlbum;

    if-nez v5, :cond_0

    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v5, :cond_2

    .line 1277
    :cond_0
    const/4 v3, 0x1

    .line 1281
    .end local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1
    return v3

    .line 1274
    .restart local v2    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public isBurstShot(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1336
    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->isShowAllGroup()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->getShowGroupId()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {p2, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isBurstShot(Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 1341
    .local v1, "result":Z
    :goto_0
    return v1

    .line 1336
    .end local v1    # "result":Z
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 1338
    :catch_0
    move-exception v0

    .line 1339
    .local v0, "e":Ljava/lang/NullPointerException;
    const/4 v1, 0x0

    .restart local v1    # "result":Z
    goto :goto_0
.end method

.method public isCameraSet()Z
    .locals 1

    .prologue
    .line 1266
    const/4 v0, 0x0

    return v0
.end method

.method public isNewAlbumHeader()Z
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public isShowAllGroup()Z
    .locals 1

    .prologue
    .line 1326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->isShowAllGroup()Z

    move-result v0

    return v0
.end method

.method public requestScreenNail(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 834
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 829
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(II)Z
    .locals 2
    .param p1, "code"    # I
    .param p2, "type"    # I

    .prologue
    .line 844
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(IIZ)Z

    move-result v0

    return v0
.end method

.method public requestScreenNailUrgent(Lcom/sec/android/gallery3d/data/MediaItem;I)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "type"    # I

    .prologue
    .line 839
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestScreenNail(Lcom/sec/android/gallery3d/data/MediaItem;IZ)Z

    move-result v0

    return v0
.end method

.method public requestToNext(I)Z
    .locals 1
    .param p1, "toward"    # I

    .prologue
    .line 824
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestToNext(I)Z

    move-result v0

    return v0
.end method

.method public requestUpdateBitmap(III)V
    .locals 1
    .param p1, "code"    # I
    .param p2, "level"    # I
    .param p3, "type"    # I

    .prologue
    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->requestUpdateBitmap(III)V

    .line 128
    return-void
.end method

.method public setActiveWindow(IIII)V
    .locals 1
    .param p1, "activeStart"    # I
    .param p2, "activeEnd"    # I
    .param p3, "contentStart"    # I
    .param p4, "contentEnd"    # I

    .prologue
    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setActiveWindow(IIII)V

    .line 111
    return-void
.end method

.method public setFilterMimeType(Ljava/lang/String;)V
    .locals 0
    .param p1, "filterMimeType"    # Ljava/lang/String;

    .prologue
    .line 1298
    return-void
.end method

.method public setFocusAlbumFilePath(Ljava/lang/String;)V
    .locals 0
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 1294
    return-void
.end method

.method public setHeaderItem(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 849
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 850
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 852
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 853
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mHeaderImage:Landroid/graphics/Bitmap;

    .line 854
    return-void
.end method

.method public setInitThumbType(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    int-to-byte v1, p1

    iput-byte v1, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mLevel:B

    .line 101
    return-void
.end method

.method public setNewMarkID(IIJ)V
    .locals 1
    .param p1, "bucketID"    # I
    .param p2, "itemID"    # I
    .param p3, "takenTime"    # J

    .prologue
    .line 1301
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkBucketID:I

    .line 1302
    iput p2, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemID:I

    .line 1303
    iput-wide p3, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mNewMarkItemTakenTime:J

    .line 1304
    return-void
.end method

.method public setScreenNailImage(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;I)V
    .locals 8
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "type"    # I

    .prologue
    .line 874
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    if-eqz v0, :cond_0

    .line 875
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;->recycle()V

    .line 877
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x3

    move-object v2, p1

    move v5, p3

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;IIILandroid/graphics/Bitmap;)V

    iput-object v0, v7, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->mScreenNailImage:Lcom/sec/samsung/gallery/view/image_manager/ComposeImageItem;

    .line 878
    return-void
.end method

.method public setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V
    .locals 1
    .param p1, "selectionMgr"    # Lcom/sec/android/gallery3d/ui/SelectionManager;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSelectionSource(Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 97
    return-void
.end method

.method public setShowAllGroup(Z)V
    .locals 2
    .param p1, "showAll"    # Z

    .prologue
    .line 1319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setShowAllGroup(Z)V

    .line 1320
    sget-object v1, Lcom/sec/android/gallery3d/data/DataManager;->LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 1321
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->forceReload()V

    .line 1322
    monitor-exit v1

    .line 1323
    return-void

    .line 1322
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setSource(Lcom/sec/android/gallery3d/data/MediaSet;)Z
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setSource(Lcom/sec/android/gallery3d/data/MediaSet;Z)Z

    move-result v0

    return v0
.end method

.method protected setTextViewPaint(Landroid/text/TextPaint;Z)V
    .locals 2
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "forTitle"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1167
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1168
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1169
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1170
    const/4 v0, -0x1

    invoke-virtual {p1, v1, v1, v1, v0}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1172
    :cond_0
    return-void
.end method

.method protected setTextViewPaintForAlbum(Landroid/text/TextPaint;Z)V
    .locals 2
    .param p1, "textPaint"    # Landroid/text/TextPaint;
    .param p2, "forTitle"    # Z

    .prologue
    const/4 v1, 0x0

    .line 1175
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    .line 1177
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRobotoLightFontForAlbumName:Z

    if-eqz v0, :cond_1

    .line 1178
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoLightFont()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1182
    :goto_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseRobotoLightFontForAlbumName:Z

    if-nez v0, :cond_0

    .line 1183
    const/4 v0, -0x1

    invoke-virtual {p1, v1, v1, v1, v0}, Landroid/text/TextPaint;->setShadowLayer(FFFI)V

    .line 1185
    :cond_0
    return-void

    .line 1180
    :cond_1
    invoke-static {}, Lcom/sec/samsung/gallery/util/FontUtil;->getRobotoRegularFont()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public setThumbReslevel(I)V
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/ComposeMediaItemAdapter;->mDataLoader:Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;->setThumbReslevel(I)V

    .line 106
    return-void
.end method
