.class public final enum Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
.super Ljava/lang/Enum;
.source "NoteSpenSurfaceView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Brush"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum CHINESE_BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum MARKER:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum MARKERALPHA:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum PEN:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

.field public static final enum PENCIL:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "PEN"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PEN:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "BRUSH"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "CHINESE_BRUSH"

    invoke-direct {v0, v1, v5}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->CHINESE_BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "PENCIL"

    invoke-direct {v0, v1, v6}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PENCIL:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "MARKER"

    invoke-direct {v0, v1, v7}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKER:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    new-instance v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    const-string v1, "MARKERALPHA"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKERALPHA:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    .line 72
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PEN:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->CHINESE_BRUSH:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v1, v0, v5

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->PENCIL:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v1, v0, v6

    sget-object v1, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKER:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->MARKERALPHA:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->$VALUES:[Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 72
    const-class v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;
    .locals 1

    .prologue
    .line 72
    sget-object v0, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->$VALUES:[Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView$Brush;

    return-object v0
.end method
