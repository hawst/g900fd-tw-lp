.class public Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
.super Ljava/lang/Object;
.source "AbsHListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "RecycleBin"
.end annotation


# instance fields
.field private mActiveViews:[Landroid/view/View;

.field private mCurrentScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mFirstActivePosition:I

.field private mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

.field private mScrapViews:[Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mSkippedScrap:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/support/v4/util/SparseArrayCompat",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mViewTypeCount:I

.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 1

    .prologue
    .line 5344
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 5358
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    return-void
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .prologue
    .line 5344
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    return-object v0
.end method

.method static synthetic access$2202(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;)Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    .prologue
    .line 5344
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    return-object p1
.end method

.method private pruneScrapViews()V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 5653
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v3, v10

    .line 5654
    .local v3, "maxViews":I
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5655
    .local v9, "viewTypeCount":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5656
    .local v5, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 5657
    aget-object v4, v5, v1

    .line 5658
    .local v4, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 5659
    .local v6, "size":I
    sub-int v0, v6, v3

    .line 5660
    .local v0, "extras":I
    add-int/lit8 v6, v6, -0x1

    .line 5661
    const/4 v2, 0x0

    .local v2, "j":I
    move v7, v6

    .end local v6    # "size":I
    .local v7, "size":I
    :goto_1
    if-ge v2, v0, :cond_0

    .line 5662
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    add-int/lit8 v6, v7, -0x1

    .end local v7    # "size":I
    .restart local v6    # "size":I
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/View;

    const/4 v12, 0x0

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v11, v10, v12}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2700(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V

    .line 5661
    add-int/lit8 v2, v2, 0x1

    move v7, v6

    .end local v6    # "size":I
    .restart local v7    # "size":I
    goto :goto_1

    .line 5656
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5666
    .end local v0    # "extras":I
    .end local v2    # "j":I
    .end local v4    # "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v7    # "size":I
    :cond_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v10, :cond_3

    .line 5667
    const/4 v1, 0x0

    :goto_2
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v10

    if-ge v1, v10, :cond_3

    .line 5668
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    .line 5671
    .local v8, "v":Landroid/view/View;
    invoke-virtual {v8}, Landroid/view/View;->hasTransientState()Z

    move-result v10

    if-nez v10, :cond_2

    .line 5672
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v10, v1}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    .line 5673
    add-int/lit8 v1, v1, -0x1

    .line 5667
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5677
    .end local v8    # "v":Landroid/view/View;
    :cond_3
    return-void
.end method


# virtual methods
.method public addScrapView(Landroid/view/View;I)V
    .locals 5
    .param p1, "scrap"    # Landroid/view/View;
    .param p2, "position"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 5532
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 5533
    .local v0, "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-nez v0, :cond_1

    .line 5576
    :cond_0
    :goto_0
    return-void

    .line 5537
    :cond_1
    iput p2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->scrappedFromPosition:I

    .line 5541
    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    .line 5543
    .local v2, "viewType":I
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->hasTransientState()Z

    move-result v1

    .line 5545
    .local v1, "scrapHasTransientState":Z
    :goto_1
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v3

    if-eqz v3, :cond_2

    if-eqz v1, :cond_8

    .line 5546
    :cond_2
    const/4 v3, -0x2

    if-ne v2, v3, :cond_3

    if-eqz v1, :cond_5

    .line 5547
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    if-nez v3, :cond_4

    .line 5548
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    .line 5550
    :cond_4
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5552
    :cond_5
    if-eqz v1, :cond_0

    .line 5553
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v3, :cond_6

    .line 5554
    new-instance v3, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v3}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 5556
    :cond_6
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 5557
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v3, p2, p1}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 5543
    .end local v1    # "scrapHasTransientState":Z
    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    .line 5562
    .restart local v1    # "scrapHasTransientState":Z
    :cond_8
    invoke-virtual {p1}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 5563
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_a

    .line 5564
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5569
    :goto_2
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_9

    .line 5570
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5573
    :cond_9
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    if-eqz v3, :cond_0

    .line 5574
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    invoke-interface {v3, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_0

    .line 5566
    :cond_a
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v3, v3, v2

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public clear()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 5421
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    .line 5422
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5423
    .local v2, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5424
    .local v3, "scrapCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 5425
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v0

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v6, v5, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2300(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V

    .line 5424
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5428
    .end local v0    # "i":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5429
    .local v4, "typeCount":I
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v4, :cond_2

    .line 5430
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v2, v5, v0

    .line 5431
    .restart local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 5432
    .restart local v3    # "scrapCount":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v3, :cond_1

    .line 5433
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    add-int/lit8 v5, v3, -0x1

    sub-int/2addr v5, v1

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v6, v5, v7}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2400(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V

    .line 5432
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5429
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5437
    .end local v1    # "j":I
    .end local v2    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v3    # "scrapCount":I
    .end local v4    # "typeCount":I
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v5, :cond_3

    .line 5438
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v5}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 5440
    :cond_3
    return-void
.end method

.method clearTransientStateViews()V
    .locals 1

    .prologue
    .line 5504
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v0, :cond_0

    .line 5505
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v0}, Landroid/support/v4/util/SparseArrayCompat;->clear()V

    .line 5507
    :cond_0
    return-void
.end method

.method public fillActiveViews(II)V
    .locals 6
    .param p1, "childCount"    # I
    .param p2, "firstActivePosition"    # I

    .prologue
    .line 5451
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    array-length v4, v4

    if-ge v4, p1, :cond_0

    .line 5452
    new-array v4, p1, [Landroid/view/View;

    iput-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5454
    :cond_0
    iput p2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mFirstActivePosition:I

    .line 5456
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5457
    .local v0, "activeViews":[Landroid/view/View;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p1, :cond_2

    .line 5458
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 5459
    .local v1, "child":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 5461
    .local v3, "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-eqz v3, :cond_1

    iget v4, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    const/4 v5, -0x2

    if-eq v4, v5, :cond_1

    .line 5464
    aput-object v1, v0, v2

    .line 5457
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5467
    .end local v1    # "child":Landroid/view/View;
    .end local v3    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_2
    return-void
.end method

.method public getActiveView(I)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x0

    .line 5477
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mFirstActivePosition:I

    sub-int v1, p1, v4

    .line 5478
    .local v1, "index":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5479
    .local v0, "activeViews":[Landroid/view/View;
    if-ltz v1, :cond_0

    array-length v4, v0

    if-ge v1, v4, :cond_0

    .line 5480
    aget-object v2, v0, v1

    .line 5481
    .local v2, "match":Landroid/view/View;
    aput-object v3, v0, v1

    .line 5484
    .end local v2    # "match":Landroid/view/View;
    :goto_0
    return-object v2

    :cond_0
    move-object v2, v3

    goto :goto_0
.end method

.method getScrapView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 5513
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 5514
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    .line 5521
    :goto_0
    return-object v1

    .line 5516
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v0

    .line 5517
    .local v0, "whichScrap":I
    if-ltz v0, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5518
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v1, v1, v0

    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->retrieveFromScrap(Ljava/util/ArrayList;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    .line 5521
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method getTransientStateView(I)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 5488
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v2, :cond_1

    .line 5497
    :cond_0
    :goto_0
    return-object v1

    .line 5491
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, p1}, Landroid/support/v4/util/SparseArrayCompat;->indexOfKey(I)I

    move-result v0

    .line 5492
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 5495
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 5496
    .local v1, "result":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v2, v0}, Landroid/support/v4/util/SparseArrayCompat;->removeAt(I)V

    goto :goto_0
.end method

.method public markChildrenDirty()V
    .locals 8

    .prologue
    .line 5389
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_0

    .line 5390
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5391
    .local v3, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 5392
    .local v4, "scrapCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_2

    .line 5393
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5392
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5396
    .end local v1    # "i":I
    .end local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "scrapCount":I
    :cond_0
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5397
    .local v5, "typeCount":I
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v5, :cond_2

    .line 5398
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v3, v6, v1

    .line 5399
    .restart local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 5400
    .restart local v4    # "scrapCount":I
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_2
    if-ge v2, v4, :cond_1

    .line 5401
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5400
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5397
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5405
    .end local v2    # "j":I
    .end local v3    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v4    # "scrapCount":I
    .end local v5    # "typeCount":I
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-eqz v6, :cond_3

    .line 5406
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6}, Landroid/support/v4/util/SparseArrayCompat;->size()I

    move-result v0

    .line 5407
    .local v0, "count":I
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v0, :cond_3

    .line 5408
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    invoke-virtual {v6, v1}, Landroid/support/v4/util/SparseArrayCompat;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->forceLayout()V

    .line 5407
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5411
    .end local v0    # "count":I
    :cond_3
    return-void
.end method

.method reclaimScrapViews(Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 5683
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 5684
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    invoke-interface {p1, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5693
    :cond_0
    return-void

    .line 5686
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5687
    .local v3, "viewTypeCount":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5688
    .local v2, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 5689
    aget-object v1, v2, v0

    .line 5690
    .local v1, "scrapPile":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-interface {p1, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 5688
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public removeSkippedScrap()V
    .locals 5

    .prologue
    .line 5582
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    if-nez v2, :cond_0

    .line 5590
    :goto_0
    return-void

    .line 5585
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 5586
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 5587
    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    const/4 v4, 0x0

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v3, v2, v4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2500(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V

    .line 5586
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 5589
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mSkippedScrap:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public scrapActiveViews()V
    .locals 14
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v13, 0x0

    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 5597
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5598
    .local v0, "activeViews":[Landroid/view/View;
    iget-object v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    if-eqz v12, :cond_5

    move v2, v10

    .line 5599
    .local v2, "hasListener":Z
    :goto_0
    iget v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    if-le v12, v10, :cond_6

    move v5, v10

    .line 5601
    .local v5, "multipleScraps":Z
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5602
    .local v7, "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    array-length v1, v0

    .line 5603
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_2
    if-ltz v3, :cond_b

    .line 5604
    aget-object v8, v0, v3

    .line 5605
    .local v8, "victim":Landroid/view/View;
    if-eqz v8, :cond_4

    .line 5606
    invoke-virtual {v8}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 5607
    .local v4, "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    iget v9, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    .line 5609
    .local v9, "whichScrap":I
    aput-object v13, v0, v3

    .line 5611
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0x10

    if-lt v10, v12, :cond_7

    invoke-virtual {v8}, Landroid/view/View;->hasTransientState()Z

    move-result v6

    .line 5612
    .local v6, "scrapHasTransientState":Z
    :goto_3
    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v6, :cond_8

    .line 5614
    :cond_0
    const/4 v10, -0x2

    if-ne v9, v10, :cond_1

    if-eqz v6, :cond_2

    .line 5616
    :cond_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->removeDetachedView(Landroid/view/View;Z)V
    invoke-static {v10, v8, v11}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2600(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Landroid/view/View;Z)V

    .line 5618
    :cond_2
    if-eqz v6, :cond_4

    .line 5619
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    if-nez v10, :cond_3

    .line 5620
    new-instance v10, Landroid/support/v4/util/SparseArrayCompat;

    invoke-direct {v10}, Landroid/support/v4/util/SparseArrayCompat;-><init>()V

    iput-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    .line 5622
    :cond_3
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mTransientStateViews:Landroid/support/v4/util/SparseArrayCompat;

    iget v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v12, v3

    invoke-virtual {v10, v12, v8}, Landroid/support/v4/util/SparseArrayCompat;->put(ILjava/lang/Object;)V

    .line 5603
    .end local v4    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .end local v6    # "scrapHasTransientState":Z
    .end local v9    # "whichScrap":I
    :cond_4
    :goto_4
    add-int/lit8 v3, v3, -0x1

    goto :goto_2

    .end local v1    # "count":I
    .end local v2    # "hasListener":Z
    .end local v3    # "i":I
    .end local v5    # "multipleScraps":Z
    .end local v7    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v8    # "victim":Landroid/view/View;
    :cond_5
    move v2, v11

    .line 5598
    goto :goto_0

    .restart local v2    # "hasListener":Z
    :cond_6
    move v5, v11

    .line 5599
    goto :goto_1

    .restart local v1    # "count":I
    .restart local v3    # "i":I
    .restart local v4    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .restart local v5    # "multipleScraps":Z
    .restart local v7    # "scrapViews":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .restart local v8    # "victim":Landroid/view/View;
    .restart local v9    # "whichScrap":I
    :cond_7
    move v6, v11

    .line 5611
    goto :goto_3

    .line 5627
    .restart local v6    # "scrapHasTransientState":Z
    :cond_8
    if-eqz v5, :cond_9

    .line 5628
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v7, v10, v9

    .line 5630
    :cond_9
    invoke-virtual {v8}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 5631
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mFirstActivePosition:I

    add-int/2addr v10, v3

    iput v10, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->scrappedFromPosition:I

    .line 5632
    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 5634
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v12, 0xe

    if-lt v10, v12, :cond_a

    .line 5635
    invoke-virtual {v8, v13}, Landroid/view/View;->setAccessibilityDelegate(Landroid/view/View$AccessibilityDelegate;)V

    .line 5638
    :cond_a
    if-eqz v2, :cond_4

    .line 5639
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mRecyclerListener:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;

    invoke-interface {v10, v8}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    goto :goto_4

    .line 5644
    .end local v4    # "lp":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .end local v6    # "scrapHasTransientState":Z
    .end local v8    # "victim":Landroid/view/View;
    .end local v9    # "whichScrap":I
    :cond_b
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->pruneScrapViews()V

    .line 5645
    return-void
.end method

.method setCacheColorHint(I)V
    .locals 10
    .param p1, "color"    # I

    .prologue
    .line 5702
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_0

    .line 5703
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5704
    .local v4, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 5705
    .local v5, "scrapCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v5, :cond_2

    .line 5706
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 5705
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 5709
    .end local v2    # "i":I
    .end local v4    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v5    # "scrapCount":I
    :cond_0
    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5710
    .local v6, "typeCount":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    if-ge v2, v6, :cond_2

    .line 5711
    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    aget-object v4, v8, v2

    .line 5712
    .restart local v4    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 5713
    .restart local v5    # "scrapCount":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_2
    if-ge v3, v5, :cond_1

    .line 5714
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/view/View;

    invoke-virtual {v8, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 5713
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 5710
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 5719
    .end local v3    # "j":I
    .end local v4    # "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    .end local v5    # "scrapCount":I
    .end local v6    # "typeCount":I
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mActiveViews:[Landroid/view/View;

    .line 5720
    .local v0, "activeViews":[Landroid/view/View;
    array-length v1, v0

    .line 5721
    .local v1, "count":I
    const/4 v2, 0x0

    :goto_3
    if-ge v2, v1, :cond_4

    .line 5722
    aget-object v7, v0, v2

    .line 5723
    .local v7, "victim":Landroid/view/View;
    if-eqz v7, :cond_3

    .line 5724
    invoke-virtual {v7, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 5721
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 5727
    .end local v7    # "victim":Landroid/view/View;
    :cond_4
    return-void
.end method

.method public setViewTypeCount(I)V
    .locals 4
    .param p1, "viewTypeCount"    # I

    .prologue
    .line 5375
    const/4 v2, 0x1

    if-ge p1, v2, :cond_0

    .line 5376
    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Can\'t have a viewTypeCount < 1"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 5379
    :cond_0
    new-array v1, p1, [Ljava/util/ArrayList;

    .line 5380
    .local v1, "scrapViews":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 5381
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aput-object v2, v1, v0

    .line 5380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5383
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mViewTypeCount:I

    .line 5384
    const/4 v2, 0x0

    aget-object v2, v1, v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;

    .line 5385
    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;

    .line 5386
    return-void
.end method

.method public shouldRecycleViewType(I)Z
    .locals 1
    .param p1, "viewType"    # I

    .prologue
    .line 5414
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
