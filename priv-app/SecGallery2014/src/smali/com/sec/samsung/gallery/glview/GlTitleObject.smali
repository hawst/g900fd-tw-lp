.class public Lcom/sec/samsung/gallery/glview/GlTitleObject;
.super Lcom/sec/android/gallery3d/glcore/GlObject;
.source "GlTitleObject.java"


# instance fields
.field public final mGlThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlObject;II)V
    .locals 2
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "glThumbObject"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 11
    invoke-direct {p0, p1, v0, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 12
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlTitleObject;->mGlThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 13
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v1, p1, Lcom/sec/android/gallery3d/glcore/GlLayer;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v0, v1, p3, p4}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlTitleObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 14
    return-void
.end method


# virtual methods
.method public getMainObject()Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1

    .prologue
    .line 17
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTitleObject;->mGlThumbObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    return-object v0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 21
    invoke-super {p0}, Lcom/sec/android/gallery3d/glcore/GlObject;->onDestroy()V

    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTitleObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    if-eqz v0, :cond_0

    .line 23
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTitleObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->recycle()V

    .line 24
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlTitleObject;->mGlCanvas:Lcom/sec/android/gallery3d/glcore/GlCanvas;

    .line 26
    :cond_0
    return-void
.end method
