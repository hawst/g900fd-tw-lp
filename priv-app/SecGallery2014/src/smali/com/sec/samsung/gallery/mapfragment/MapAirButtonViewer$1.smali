.class Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;
.super Ljava/lang/Object;
.source "MapAirButtonViewer.java"

# interfaces
.implements Lcom/samsung/android/airbutton/AirButtonImpl$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->createMenuWidgetFromView(Landroid/view/View;)Lcom/samsung/android/airbutton/AirButtonImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/view/View;ILjava/lang/Object;)V
    .locals 10
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "itemIndex"    # I
    .param p3, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 44
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v4

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Landroid/media/AudioManager;->playSoundEffect(I)V

    .line 47
    packed-switch p2, :pswitch_data_0

    .line 75
    :goto_0
    return-void

    .line 49
    :pswitch_0
    new-array v1, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v1, v7

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v8

    .line 50
    .local v1, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "START_IMAGE_NOTE"

    invoke-virtual {v4, v5, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    .end local v1    # "params":[Ljava/lang/Object;
    :pswitch_1
    new-array v1, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v1, v7

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    aput-object v4, v1, v6

    .line 54
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "CROP_IMAGE"

    invoke-virtual {v4, v5, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 57
    .end local v1    # "params":[Ljava/lang/Object;
    :pswitch_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 58
    .local v2, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    .line 59
    .local v3, "uri":Landroid/net/Uri;
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 60
    const/4 v4, 0x4

    new-array v1, v4, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    aput-object v4, v1, v7

    aput-object v2, v1, v6

    const/4 v4, 0x0

    aput-object v4, v1, v8

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v9

    .line 61
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "IMAGE_EDIT"

    invoke-virtual {v4, v5, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 64
    .end local v1    # "params":[Ljava/lang/Object;
    .end local v2    # "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    .end local v3    # "uri":Landroid/net/Uri;
    :pswitch_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    .line 65
    .local v0, "mSelectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 66
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 67
    new-array v1, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v1, v7

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v8

    .line 68
    .restart local v1    # "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$000(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "SHOW_SHARE_DIALOG"

    invoke-virtual {v4, v5, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 69
    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer$1;->this$0:Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;

    # getter for: Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->mItem:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v4}, Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;->access$100(Lcom/sec/samsung/gallery/mapfragment/MapAirButtonViewer;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto/16 :goto_0

    .line 47
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
