.class Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "MoveToSecretboxCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->execute(Lorg/puremvc/java/interfaces/INotification;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 4
    .param p1, "state"    # I
    .param p2, "extInfo"    # I

    .prologue
    .line 164
    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$600()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChange: state="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    packed-switch p1, :pswitch_data_0

    .line 180
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 167
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$700(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 168
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeManager:Lcom/samsung/android/privatemode/PrivateModeManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$700(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    # setter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mPrivateModeBinder:Landroid/os/IBinder;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$802(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;Landroid/os/IBinder;)Landroid/os/IBinder;

    goto :goto_0

    .line 172
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$900(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x2

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 175
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd$2;->this$0:Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->mProgressDialog:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->access$1000(Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/controller/MoveToSecretboxCmd;->onCancel(Landroid/content/DialogInterface;)V

    goto :goto_0

    .line 165
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
