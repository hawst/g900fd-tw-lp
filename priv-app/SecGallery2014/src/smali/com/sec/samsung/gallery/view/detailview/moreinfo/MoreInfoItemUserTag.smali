.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
.super Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;
.source "MoreInfoItemUserTag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;
    }
.end annotation


# static fields
.field private static final ME_NAME:Ljava/lang/String; = "profile/Me"

.field private static final TYPE_ADD_TAG:I = 0x1

.field private static final TYPE_RUN_EDIT:I


# instance fields
.field private mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private mAdapters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;",
            ">;"
        }
    .end annotation
.end field

.field private mAddButtonClickListener:Landroid/view/View$OnClickListener;

.field private mAddTagButton:Landroid/widget/ImageButton;

.field private mDuplicatedToast:Landroid/widget/Toast;

.field private mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

.field private mEditTextHasFocus:Z

.field private mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

.field private mExpandableLayoutContainer:Landroid/widget/FrameLayout;

.field private mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

.field private mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

.field private mFaceTextView:Landroid/widget/TextView;

.field private mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

.field private mNoUserTagTextView:Landroid/widget/TextView;

.field private mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

.field private mRunEditButton:Landroid/widget/ImageButton;

.field private mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

.field private mSubCatTextView:Landroid/widget/TextView;

.field private mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

.field private mTextEditSetView:Landroid/widget/RelativeLayout;

.field private mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

.field private mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

.field private mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

.field private mUserTagTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/ActionBarManager;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "entry"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;
    .param p4, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p5, "manager"    # Lcom/sec/samsung/gallery/view/ActionBarManager;

    .prologue
    .line 135
    invoke-direct {p0, p1, p2, p4, p3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfo$Entry;)V

    .line 43
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddButtonClickListener:Landroid/view/View$OnClickListener;

    .line 100
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    .line 109
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$3;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    .line 121
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$4;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

    .line 136
    iput-object p5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 137
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoSubCategoryAdapter;

    invoke-direct {v0, p1, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoSubCategoryAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->registerDataSetChangeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;)V

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;

    invoke-direct {v0, p1, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->registerDataSetChangeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;)V

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-direct {v0, p1, p4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->registerDataSetChangeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter$DataSetChangeListener;)V

    .line 147
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextHasFocus:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextHasFocus:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->runUserTagEditView(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mDuplicatedToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Landroid/widget/ImageButton;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Landroid/os/IBinder;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
    .param p1, "x1"    # Landroid/os/IBinder;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->hideSoftInput(Landroid/os/IBinder;)V

    return-void
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .prologue
    .line 42
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    return-object p1
.end method

.method private hideSoftInput(Landroid/os/IBinder;)V
    .locals 4
    .param p1, "windowToken"    # Landroid/os/IBinder;

    .prologue
    .line 422
    :try_start_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 424
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    .line 426
    .local v1, "imm":Landroid/view/inputmethod/InputMethodManager;
    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 430
    .end local v1    # "imm":Landroid/view/inputmethod/InputMethodManager;
    :goto_0
    return-void

    .line 427
    :catch_0
    move-exception v0

    .line 428
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0
.end method

.method private runUserTagEditView(Ljava/util/ArrayList;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 387
    .local p1, "moreinfoBaseAdapters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;>;"
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->hideSoftInput(Landroid/os/IBinder;)V

    .line 388
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRootView:Landroid/view/View;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getAllTags()Ljava/util/ArrayList;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getBaseUri()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/samsung/gallery/view/ActionBarManager;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->registerResultCallBack(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;)V

    .line 413
    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v0

    .line 416
    .local v6, "params":[Ljava/lang/Object;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "MOREINFO_EVENT"

    invoke-virtual {v0, v1, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 418
    return-void
.end method

.method private updateContentViews()V
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-nez v0, :cond_0

    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01b2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    .line 307
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->updateExpandableItems(Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;Landroid/widget/BaseAdapter;)V

    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceTextView:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01ae

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceTextView:Landroid/widget/TextView;

    .line 312
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->getTags()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-nez v0, :cond_2

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01b3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    .line 319
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->updateExpandableItems(Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;Landroid/widget/BaseAdapter;)V

    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatTextView:Landroid/widget/TextView;

    if-nez v0, :cond_3

    .line 321
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01af

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatTextView:Landroid/widget/TextView;

    .line 323
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getTags()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-nez v0, :cond_4

    .line 327
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01b4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    .line 330
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->updateExpandableItems(Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;Landroid/widget/BaseAdapter;)V

    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagTextView:Landroid/widget/TextView;

    if-nez v0, :cond_5

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v1, 0x7f0f01b0

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagTextView:Landroid/widget/TextView;

    .line 334
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getTags()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 335
    return-void
.end method

.method private updateExpandableItems(Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;Landroid/widget/BaseAdapter;)V
    .locals 6
    .param p1, "expandable"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
    .param p2, "adapter"    # Landroid/widget/BaseAdapter;

    .prologue
    const/4 v5, 0x0

    .line 338
    invoke-virtual {p2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v3

    .line 339
    .local v3, "size":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildCount()I

    move-result v0

    .line 340
    .local v0, "childSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 341
    invoke-virtual {p1, v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 342
    .local v1, "childTagView":Landroid/view/View;
    if-nez v1, :cond_0

    .line 343
    invoke-virtual {p2, v2, v5, v5}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->addView(Landroid/view/View;)V

    .line 340
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 345
    :cond_0
    invoke-virtual {p2, v2, v1, v5}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    goto :goto_1

    .line 348
    .end local v1    # "childTagView":Landroid/view/View;
    :cond_1
    if-le v0, v3, :cond_2

    .line 349
    sub-int v4, v0, v3

    invoke-virtual {p1, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->removeViews(II)V

    .line 351
    :cond_2
    return-void
.end method


# virtual methods
.method public actionEditCancel()V
    .locals 3

    .prologue
    .line 251
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    .line 252
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->editCancel()V

    goto :goto_0

    .line 254
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->resetEditMode()V

    .line 255
    return-void
.end method

.method public actionEditDone()V
    .locals 3

    .prologue
    .line 244
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    .line 245
    .local v0, "adapter":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->editDone()V

    goto :goto_0

    .line 247
    .end local v0    # "adapter":Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;
    :cond_0
    return-void
.end method

.method public dataEdited()Z
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->isDataSetChanged()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->isDataSetChanged()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public exitUserTagEditMode(Z)V
    .locals 1
    .param p1, "resultDone"    # Z

    .prologue
    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    if-eqz v0, :cond_0

    .line 355
    if-eqz p1, :cond_1

    .line 356
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->editActionDone()V

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->editActionCancel()V

    goto :goto_0
.end method

.method public getView()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 165
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->getView()Landroid/view/View;

    .line 167
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f01ac

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 168
    .local v1, "view":Landroid/view/View;
    if-nez v1, :cond_0

    .line 169
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f019c

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    .line 171
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03009e

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 173
    .local v0, "topLayout":Landroid/widget/FrameLayout;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 176
    .end local v0    # "topLayout":Landroid/widget/FrameLayout;
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->updateContentViews()V

    .line 178
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mNoUserTagTextView:Landroid/widget/TextView;

    if-nez v2, :cond_1

    .line 179
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f018b

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mNoUserTagTextView:Landroid/widget/TextView;

    .line 180
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mNoUserTagTextView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0469

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mTextEditSetView:Landroid/widget/RelativeLayout;

    if-nez v2, :cond_2

    .line 184
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f019d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mTextEditSetView:Landroid/widget/RelativeLayout;

    .line 186
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f01a2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    .line 187
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->clearFocus()V

    .line 188
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mOnKeyPreImeListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setOnKeyPreImeListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText$onKeyPreImeListener;)V

    .line 189
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 191
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    new-instance v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;

    invoke-direct {v3, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$5;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 209
    new-instance v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    invoke-direct {v2, v3, v4}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;-><init>(Landroid/content/Context;Landroid/widget/EditText;)V

    .line 211
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f01a4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    .line 212
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 213
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setId(I)V

    .line 215
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    const v3, 0x7f0f01a5

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;

    .line 216
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 217
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAddTagButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setId(I)V

    .line 220
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->setVisibilityByViewMode()V

    .line 222
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    if-eqz v2, :cond_3

    .line 223
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->getAllUserTag(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 224
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    const v3, 0x3ecccccd    # 0.4f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 225
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v5}, Landroid/widget/ImageButton;->setEnabled(Z)V

    .line 231
    :cond_3
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    return-object v2

    .line 227
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setAlpha(F)V

    .line 228
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mRunEditButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v6}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_0
.end method

.method public invalidate()V
    .locals 2

    .prologue
    .line 236
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditMode:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->hasFocus()Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->setCursorVisible(Z)V

    .line 239
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 240
    return-void
.end method

.method public isUserTagEditMode()Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public resetEditMode()V
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$6;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 378
    return-void
.end method

.method public setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    .prologue
    .line 151
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->setEditListener(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;)V

    .line 154
    return-void
.end method

.method public setMode(Z)V
    .locals 1
    .param p1, "editMode"    # Z

    .prologue
    .line 158
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem;->setMode(Z)V

    .line 159
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->setVisibilityByViewMode()V

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 161
    return-void
.end method

.method protected setVisibilityByViewMode()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/16 v7, 0x8

    const/4 v5, 0x0

    .line 265
    iget-boolean v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditMode:Z

    if-eqz v6, :cond_9

    .line 266
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_1

    move v1, v4

    .line 267
    .local v1, "hasFace":Z
    :goto_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-eqz v1, :cond_2

    move v6, v5

    :goto_1
    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 268
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 270
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_3

    move v2, v4

    .line 271
    .local v2, "hasSubCat":Z
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-eqz v2, :cond_4

    move v6, v5

    :goto_3
    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 272
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 274
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_5

    move v3, v4

    .line 275
    .local v3, "hasUserTag":Z
    :goto_4
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    if-eqz v3, :cond_6

    move v6, v5

    :goto_5
    invoke-virtual {v8, v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 276
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 277
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mTextEditSetView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v5}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 279
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    if-eqz v3, :cond_7

    :cond_0
    move v0, v4

    .line 280
    .local v0, "hasData":Z
    :goto_6
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_8

    :goto_7
    invoke-virtual {v4, v5}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 281
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mNoUserTagTextView:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    :goto_8
    return-void

    .end local v0    # "hasData":Z
    .end local v1    # "hasFace":Z
    .end local v2    # "hasSubCat":Z
    .end local v3    # "hasUserTag":Z
    :cond_1
    move v1, v5

    .line 266
    goto :goto_0

    .restart local v1    # "hasFace":Z
    :cond_2
    move v6, v7

    .line 267
    goto :goto_1

    :cond_3
    move v2, v5

    .line 270
    goto :goto_2

    .restart local v2    # "hasSubCat":Z
    :cond_4
    move v6, v7

    .line 271
    goto :goto_3

    :cond_5
    move v3, v5

    .line 274
    goto :goto_4

    .restart local v3    # "hasUserTag":Z
    :cond_6
    move v6, v7

    .line 275
    goto :goto_5

    :cond_7
    move v0, v5

    .line 279
    goto :goto_6

    .restart local v0    # "hasData":Z
    :cond_8
    move v5, v7

    .line 280
    goto :goto_7

    .line 283
    .end local v0    # "hasData":Z
    .end local v1    # "hasFace":Z
    .end local v2    # "hasSubCat":Z
    .end local v3    # "hasUserTag":Z
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_b

    move v1, v4

    .line 284
    .restart local v1    # "hasFace":Z
    :goto_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 285
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mFaceTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_c

    move v6, v5

    :goto_a
    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 287
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCategoryAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_d

    move v2, v4

    .line 288
    .restart local v2    # "hasSubCat":Z
    :goto_b
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 289
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mSubCatTextView:Landroid/widget/TextView;

    if-eqz v2, :cond_e

    move v6, v5

    :goto_c
    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 291
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagAdapter:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getCount()I

    move-result v6

    if-lez v6, :cond_f

    move v3, v4

    .line 292
    .restart local v3    # "hasUserTag":Z
    :goto_d
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagExpandableLayout:Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;

    invoke-virtual {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;->setVisibility(I)V

    .line 293
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagTextView:Landroid/widget/TextView;

    if-eqz v3, :cond_10

    move v6, v5

    :goto_e
    invoke-virtual {v8, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 294
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mTextEditSetView:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 295
    if-nez v1, :cond_a

    if-nez v2, :cond_a

    if-eqz v3, :cond_11

    :cond_a
    move v0, v4

    .line 296
    .restart local v0    # "hasData":Z
    :goto_f
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mExpandableLayoutContainer:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_12

    move v4, v5

    :goto_10
    invoke-virtual {v6, v4}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 297
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mNoUserTagTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_13

    :goto_11
    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_8

    .end local v0    # "hasData":Z
    .end local v1    # "hasFace":Z
    .end local v2    # "hasSubCat":Z
    .end local v3    # "hasUserTag":Z
    :cond_b
    move v1, v5

    .line 283
    goto :goto_9

    .restart local v1    # "hasFace":Z
    :cond_c
    move v6, v7

    .line 285
    goto :goto_a

    :cond_d
    move v2, v5

    .line 287
    goto :goto_b

    .restart local v2    # "hasSubCat":Z
    :cond_e
    move v6, v7

    .line 289
    goto :goto_c

    :cond_f
    move v3, v5

    .line 291
    goto :goto_d

    .restart local v3    # "hasUserTag":Z
    :cond_10
    move v6, v7

    .line 293
    goto :goto_e

    :cond_11
    move v0, v5

    .line 295
    goto :goto_f

    .restart local v0    # "hasData":Z
    :cond_12
    move v4, v7

    .line 296
    goto :goto_10

    :cond_13
    move v7, v5

    .line 297
    goto :goto_11
.end method

.method public updateEditViewActionBar()V
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;->updateActionBar()V

    .line 384
    :cond_0
    return-void
.end method
