.class abstract Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;
.super Ljava/lang/Object;
.source "BasePoint.java"


# instance fields
.field protected mapPosition:Lcom/google/android/gms/maps/model/LatLng;

.field private screenPosition:Landroid/graphics/Point;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method buildScreenPosition(Lcom/google/android/gms/maps/Projection;)V
    .locals 1
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;

    .prologue
    .line 34
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/maps/Projection;->toScreenLocation(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    .line 37
    :cond_0
    return-void
.end method

.method clearScreenPosition()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    .line 31
    return-void
.end method

.method public getMapPosition()Lcom/google/android/gms/maps/model/LatLng;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->mapPosition:Lcom/google/android/gms/maps/model/LatLng;

    return-object v0
.end method

.method getPixelDistanceFrom(Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;)D
    .locals 2
    .param p1, "otherPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    iget-object v1, p1, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/util/Distance;->from(Landroid/graphics/Point;Landroid/graphics/Point;)D

    move-result-wide v0

    return-wide v0
.end method

.method getScreenPosition()Landroid/graphics/Point;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    return-object v0
.end method

.method hasScreenPosition()Z
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method setScreenPosition(Landroid/graphics/Point;)V
    .locals 0
    .param p1, "screenPosition"    # Landroid/graphics/Point;

    .prologue
    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/BasePoint;->screenPosition:Landroid/graphics/Point;

    .line 41
    return-void
.end method
