.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;
.super Ljava/lang/Object;
.source "MoreInfoItemLocation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 92
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    const-string v3, "input_method"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 93
    .local v0, "imr":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 96
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoLocationEditActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 97
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "currentImg_Lat"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)[D

    move-result-object v3

    aget-wide v4, v3, v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 98
    const-string v2, "currentImg_Log"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mInerimLatLong:[D
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;)[D

    move-result-object v3

    const/4 v4, 0x1

    aget-wide v4, v3, v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;D)Landroid/content/Intent;

    .line 99
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation$2;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemLocation;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    const/16 v3, 0x30a

    invoke-virtual {v2, v1, v3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 100
    return-void
.end method
