.class public Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;
.super Ljava/lang/Object;
.source "ComposeViewDataLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DataConfig"
.end annotation


# instance fields
.field public mCheckSetVersion:Z

.field public mFirstRangeOptimization:Z

.field public mRetLineCount:I

.field public mRetThmType:B

.field public mScanAllSet:Z

.field public mScanFirstOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 1322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1323
    iput v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetLineCount:I

    .line 1324
    iput-byte v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mRetThmType:B

    .line 1325
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mScanFirstOnly:Z

    .line 1326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mScanAllSet:Z

    .line 1327
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mCheckSetVersion:Z

    .line 1328
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;->mFirstRangeOptimization:Z

    return-void
.end method


# virtual methods
.method public setAlbumAttribute(Lcom/sec/android/gallery3d/data/MediaSet;I)I
    .locals 1
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "count"    # I

    .prologue
    .line 1331
    const/4 v0, 0x0

    return v0
.end method
