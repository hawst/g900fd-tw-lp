.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;
.super Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
.source "HiddenMediaViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;

    move-result-object v0

    const/16 v1, -0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAirMotionImageFling(I)V

    .line 165
    :cond_0
    return-void
.end method

.method public onPrevious()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$800(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$3;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;

    move-result-object v0

    const/16 v1, 0x1f40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setAirMotionImageFling(I)V

    .line 158
    :cond_0
    return-void
.end method
