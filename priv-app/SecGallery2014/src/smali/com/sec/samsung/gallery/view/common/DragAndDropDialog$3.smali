.class Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;
.super Ljava/lang/Object;
.source "DragAndDropDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->show(II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

.field final synthetic val$checkBoxDoNotShowAgain:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->val$checkBoxDoNotShowAgain:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->val$checkBoxDoNotShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    const/4 v1, 0x1

    # invokes: Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->setDoNotShowAgain(Z)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->access$000(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;Z)V

    .line 73
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mOkButtonListener:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->access$100(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;)Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$3;->this$0:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->mOkButtonListener:Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;->access$100(Lcom/sec/samsung/gallery/view/common/DragAndDropDialog;)Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/common/DragAndDropDialog$OkButtonClickListener;->onClick()V

    .line 75
    :cond_1
    return-void
.end method
