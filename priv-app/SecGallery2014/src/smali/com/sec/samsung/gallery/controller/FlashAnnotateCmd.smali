.class public Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "FlashAnnotateCmd.java"


# static fields
.field public static final FLASH_ANNOTATE_ACTIVITY:Ljava/lang/String; = "com.sec.spen.flashannotate.FlashAnnotateActivity"

.field public static final FLASH_ANNOTATE_PACKET:Ljava/lang/String; = "com.sec.spen.flashannotate"

.field public static final INTENT_FA_IMAGE_HEIGHT:Ljava/lang/String; = "imageHeight"

.field public static final INTENT_FA_IMAGE_WIDTH:Ljava/lang/String; = "imageWidth"

.field public static final INTENT_FA_REMOVE_CAPTURED_IMAGE:Ljava/lang/String; = "removeCapturedImage"

.field public static final INTENT_FA_SAVE_PATH:Ljava/lang/String; = "savepath"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mImageHeight:I

.field private mImagePath:Ljava/lang/String;

.field private mImageWidth:I

.field private final mRemoveCapturedImage:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mRemoveCapturedImage:Z

    return-void
.end method

.method private isFArunning()Z
    .locals 8

    .prologue
    .line 69
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const-string v7, "activity"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 70
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v6, 0x64

    invoke-virtual {v0, v6}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v4

    .line 72
    .local v4, "runningTaskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/4 v3, 0x0

    .line 74
    .local v3, "runningTaskCount":I
    if-eqz v4, :cond_0

    .line 75
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    .line 77
    :cond_0
    if-lez v3, :cond_2

    .line 78
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_2

    .line 79
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 80
    .local v5, "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    iget v6, v5, Landroid/app/ActivityManager$RunningTaskInfo;->windowMode:I

    const/high16 v7, 0x800000

    and-int/2addr v6, v7

    if-nez v6, :cond_1

    iget v6, v5, Landroid/app/ActivityManager$RunningTaskInfo;->windowMode:I

    const/high16 v7, 0x200000

    and-int/2addr v6, v7

    if-nez v6, :cond_1

    .line 82
    iget-object v6, v5, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v6}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    .line 83
    .local v1, "className":Ljava/lang/String;
    const-string v6, "com.sec.spen.flashannotate.FlashAnnotateActivity"

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 84
    const/4 v6, 0x1

    .line 88
    .end local v1    # "className":Ljava/lang/String;
    .end local v2    # "i":I
    .end local v5    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :goto_1
    return v6

    .line 78
    .restart local v2    # "i":I
    .restart local v5    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 88
    .end local v2    # "i":I
    .end local v5    # "taskInfo":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 7
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v6, 0x0

    .line 39
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 40
    .local v3, "params":[Ljava/lang/Object;
    aget-object v4, v3, v6

    check-cast v4, Landroid/content/Context;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mContext:Landroid/content/Context;

    .line 41
    const/4 v4, 0x1

    aget-object v2, v3, v4

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 43
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->isFArunning()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 66
    :goto_0
    return-void

    .line 47
    :cond_0
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImagePath:Ljava/lang/String;

    .line 48
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getWidth()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImageWidth:I

    .line 49
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getHeight()I

    move-result v4

    iput v4, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImageHeight:I

    .line 51
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 52
    .local v1, "flashAnnotateActivity":Landroid/content/Intent;
    const/high16 v4, 0x14010000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 55
    const-string v4, "com.sec.spen.flashannotate"

    const-string v5, "com.sec.spen.flashannotate.FlashAnnotateActivity"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v4, "savepath"

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImagePath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 57
    const-string v4, "imageWidth"

    iget v5, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImageWidth:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 58
    const-string v4, "imageHeight"

    iget v5, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mImageHeight:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 59
    const-string v4, "removeCapturedImage"

    invoke-virtual {v1, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 61
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/samsung/gallery/controller/FlashAnnotateCmd;->TAG:Ljava/lang/String;

    const-string v5, "Activity Not Found"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
