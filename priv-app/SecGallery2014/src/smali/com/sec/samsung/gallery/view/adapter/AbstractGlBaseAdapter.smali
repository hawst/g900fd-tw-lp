.class public abstract Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.super Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
.source "AbstractGlBaseAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;
    }
.end annotation


# static fields
.field public static final DATA_CACHE_SIZE:I = 0x3e8

.field protected static final FIRST_LOAD_COUNT:I = 0x15

.field protected static final INDEX_NONE:I = -0x1

.field protected static final MAX_LOAD_COUNT:I = 0x40

.field protected static final MAX_LOAD_COUNT_CONNECTED_DEVICE:I = 0x1388

.field protected static final MIN_LOAD_COUNT:I = 0x4

.field protected static final MSG_RUN_OBJECT:I = 0x1

.field public static final NEWALBUM_HEADER_ID:I = -0x64

.field protected static final RES_ID_3DPANORAMA_ICON:I = 0x17

.field protected static final RES_ID_3DTOUR_ICON:I = 0x2a

.field protected static final RES_ID_3D_MPO_ICON:I = 0x1b

.field protected static final RES_ID_ADD_ICON:I = 0x15

.field protected static final RES_ID_ALBUM_LABEL:I = 0x2

.field protected static final RES_ID_ALBUM_LABEL_COUNT:I = 0x22

.field protected static final RES_ID_BESTFACE_ICON:I = 0x26

.field protected static final RES_ID_BESTPHOTO_ICON:I = 0x25

.field protected static final RES_ID_BEST_SHOT:I = 0x8

.field protected static final RES_ID_BITMAP:I = 0xd

.field protected static final RES_ID_BURSTSHOT_ICON:I = 0x18

.field protected static final RES_ID_CHECKMARK:I = 0x4

.field protected static final RES_ID_CHECKMARKBG:I = 0x23

.field protected static final RES_ID_CINEPIC_ICON:I = 0x1a

.field protected static final RES_ID_DATE_LABEL:I = 0x12

.field protected static final RES_ID_DATE_LABEL_TEXT:I = 0x13

.field protected static final RES_ID_DELETE:I = 0x6

.field protected static final RES_ID_DRAG_CNT:I = 0x9

.field protected static final RES_ID_DRAG_CNT_BG:I = 0xa

.field protected static final RES_ID_DRAG_TEXT:I = 0xb

.field protected static final RES_ID_DRAMASHOT_ICON:I = 0x28

.field protected static final RES_ID_ERASER_ICON:I = 0x27

.field protected static final RES_ID_FOCUS_BOX:I = 0x5

.field protected static final RES_ID_FRAME:I = 0x1

.field protected static final RES_ID_GOLF_ICON:I = 0x1c

.field protected static final RES_ID_HERE_TEXT:I = 0xc

.field protected static final RES_ID_IMAGE_NOTE:I = 0xf

.field protected static final RES_ID_MAGICSHOT_ICON:I = 0x24

.field protected static final RES_ID_NEW_ICON:I = 0x19

.field protected static final RES_ID_NEW_LABEL:I = 0x2d

.field protected static final RES_ID_OUTOFFOCUS_ICON:I = 0x2b

.field protected static final RES_ID_PARORAMA_ICON:I = 0x14

.field protected static final RES_ID_PICMOTION_ICON:I = 0x29

.field protected static final RES_ID_PLAY:I = 0x3

.field protected static final RES_ID_REORDER_ALBUM:I = 0x2e

.field protected static final RES_ID_SECRETBOX:I = 0x1e

.field protected static final RES_ID_SELECTED_FRAME:I = 0x21

.field protected static final RES_ID_SELECT_CNT:I = 0x10

.field protected static final RES_ID_SELECT_CNT_BG:I = 0x11

.field protected static final RES_ID_SEQUENCE_ICON:I = 0x2c

.field protected static final RES_ID_SNS_ALBUM_ICON:I = 0x20

.field protected static final RES_ID_SNS_PHOTO_ICON:I = 0x1f

.field protected static final RES_ID_SOUNDSCENE_ICON:I = 0x16

.field protected static final RES_ID_SOURCE_ICON:I = 0xe

.field protected static final RES_ID_TAG_LABEL:I = 0x1

.field protected static final RES_ID_UNKNOWN:I = 0x7

.field private static final TAG:Ljava/lang/String;


# instance fields
.field public mContentEnd:I

.field public mContentStart:I

.field private mContext:Landroid/content/Context;

.field protected final mData:[Lcom/sec/android/gallery3d/data/MediaObject;

.field protected mFocusOnText:Z

.field protected mFocusedIndex:I

.field protected mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

.field protected mGenericMotionFocus:I

.field private final mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

.field protected mItemVersion:[J

.field private mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

.field protected mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

.field protected mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

.field protected mSetVersion:[J

.field protected mSize:I

.field protected final mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

.field protected mSourceVersion:J

.field public mThumbnailActiveEnd:I

.field public mThumbnailActiveStart:I

.field public mVisibleEnd:I

.field public mVisibleStart:I

.field protected readyDrawable:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const-wide/16 v4, -0x1

    const/16 v2, 0x3e8

    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;-><init>()V

    .line 101
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveStart:I

    .line 102
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveEnd:I

    .line 104
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mVisibleStart:I

    .line 105
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mVisibleEnd:I

    .line 107
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    .line 108
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    .line 110
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    .line 112
    iput-wide v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSourceVersion:J

    .line 113
    const/4 v0, -0x2

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 116
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mGenericMotionFocus:I

    .line 117
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusOnText:Z

    .line 119
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    const v0, -0x4b4b4c

    :goto_0
    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    .line 133
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$1;-><init>(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    .line 141
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContext:Landroid/content/Context;

    .line 142
    new-array v0, v2, [Lcom/sec/android/gallery3d/data/MediaObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    .line 143
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mItemVersion:[J

    .line 144
    new-array v0, v2, [J

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSetVersion:[J

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mItemVersion:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSetVersion:[J

    invoke-static {v0, v4, v5}, Ljava/util/Arrays;->fill([JJ)V

    .line 148
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$2;

    move-object v0, p1

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v0

    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v0, v2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$2;-><init>(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;Lcom/sec/android/gallery3d/ui/GLRoot;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    .line 159
    return-void

    .line 119
    .restart local p1    # "context":Landroid/content/Context;
    :cond_0
    const v0, -0xd0d0d1

    goto :goto_0
.end method

.method private setContentWindow(II)V
    .locals 6
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I

    .prologue
    .line 278
    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    if-ne p1, v5, :cond_0

    iget v5, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    if-ne p2, v5, :cond_0

    .line 301
    :goto_0
    return-void

    .line 280
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v2, v5

    .line 282
    .local v2, "length":I
    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    .line 283
    .local v4, "start":I
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    .line 285
    .local v0, "end":I
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    .line 286
    iput p2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    .line 288
    if-ge p1, v0, :cond_1

    if-lt v4, p2, :cond_2

    .line 289
    :cond_1
    move v1, v4

    .local v1, "i":I
    move v3, v0

    .local v3, "n":I
    :goto_1
    if-ge v1, v3, :cond_4

    .line 290
    rem-int v5, v1, v2

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->clearSlot(I)V

    .line 289
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 293
    .end local v1    # "i":I
    .end local v3    # "n":I
    :cond_2
    move v1, v4

    .restart local v1    # "i":I
    :goto_2
    if-ge v1, p1, :cond_3

    .line 294
    rem-int v5, v1, v2

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->clearSlot(I)V

    .line 293
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 296
    :cond_3
    move v1, p2

    move v3, v0

    .restart local v3    # "n":I
    :goto_3
    if-ge v1, v3, :cond_4

    .line 297
    rem-int v5, v1, v2

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->clearSlot(I)V

    .line 296
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 300
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->reloadData()V

    goto :goto_0
.end method


# virtual methods
.method protected abstract addContentListener()V
.end method

.method protected clearSlot(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 257
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    .line 258
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mItemVersion:[J

    aput-wide v2, v0, p1

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSetVersion:[J

    aput-wide v2, v0, p1

    .line 260
    return-void
.end method

.method public final executeAndWait(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 208
    .local p1, "callable":Ljava/util/concurrent/Callable;, "Ljava/util/concurrent/Callable<TT;>;"
    new-instance v1, Ljava/util/concurrent/FutureTask;

    invoke-direct {v1, p1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 209
    .local v1, "task":Ljava/util/concurrent/FutureTask;, "Ljava/util/concurrent/FutureTask<TT;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mHandler:Lcom/sec/android/gallery3d/ui/SynchronizedHandler;

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v1}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;->sendMessage(Landroid/os/Message;)Z

    .line 211
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 213
    :goto_0
    return-object v2

    .line 212
    :catch_0
    move-exception v0

    .line 213
    .local v0, "e":Ljava/lang/InterruptedException;
    const/4 v2, 0x0

    goto :goto_0

    .line 214
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 215
    .local v0, "e":Ljava/util/concurrent/ExecutionException;
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method public getCurrentCacheSize()I
    .locals 3

    .prologue
    .line 186
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    if-nez v2, :cond_1

    .line 187
    const/4 v1, 0x0

    .line 195
    :cond_0
    return v1

    .line 189
    :cond_1
    const/4 v1, 0x0

    .line 190
    .local v1, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    .line 192
    add-int/lit8 v1, v1, 0x1

    .line 190
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final getFocusedIndex()I
    .locals 2

    .prologue
    .line 353
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    const/16 v1, -0x64

    if-ne v0, v1, :cond_0

    .line 354
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    .line 363
    :goto_0
    return v0

    .line 356
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    if-lez v0, :cond_1

    .line 357
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    .line 359
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    if-gez v0, :cond_2

    .line 360
    const/4 v0, -0x1

    goto :goto_0

    .line 363
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    goto :goto_0
.end method

.method public getGenericMotionFocus()I
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mGenericMotionFocus:I

    return v0
.end method

.method protected abstract loadData()Z
.end method

.method public declared-synchronized onPause()V
    .locals 1

    .prologue
    .line 248
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->terminate()V

    .line 250
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 251
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->removeContentListener()V

    .line 252
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setGenericMotionFocus(I)V

    .line 253
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setModelListener(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    monitor-exit p0

    return-void

    .line 248
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized onResume()V
    .locals 1

    .prologue
    .line 243
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->addContentListener()V

    .line 244
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->reloadData()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    monitor-exit p0

    return-void

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 199
    return-void
.end method

.method public final declared-synchronized reloadData()V
    .locals 3

    .prologue
    .line 220
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    if-eqz v0, :cond_0

    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->notifyDirty()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :goto_0
    monitor-exit p0

    return-void

    .line 224
    :cond_0
    :try_start_1
    new-instance v0, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContext:Landroid/content/Context;

    const-class v2, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 225
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V

    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$3;-><init>(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setOnLoadDataListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnLoadDataListener;)V

    .line 232
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$4;-><init>(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->setOnCheckLoadingFailedListener(Lcom/sec/samsung/gallery/view/adapter/ReloadTask$OnCheckLoadingFailedListener;)V

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public reloadVersion()V
    .locals 2

    .prologue
    .line 182
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSourceVersion:J

    .line 183
    return-void
.end method

.method protected abstract removeContentListener()V
.end method

.method public final setActiveWindow(IIII)V
    .locals 6
    .param p1, "start"    # I
    .param p2, "end"    # I
    .param p3, "visibleStart"    # I
    .param p4, "visibleEnd"    # I

    .prologue
    const/4 v5, 0x0

    .line 304
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveStart:I

    if-ne p1, v3, :cond_1

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveEnd:I

    if-ne p2, v3, :cond_1

    .line 324
    :cond_0
    :goto_0
    return-void

    .line 307
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveStart:I

    .line 308
    iput p2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveEnd:I

    .line 310
    iput p3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mVisibleStart:I

    .line 311
    iput p4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mVisibleEnd:I

    .line 313
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v2, v3

    .line 315
    .local v2, "length":I
    if-eq p1, p2, :cond_0

    .line 318
    add-int v3, p1, p2

    div-int/lit8 v3, v3, 0x2

    div-int/lit8 v4, v2, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    sub-int/2addr v4, v2

    invoke-static {v5, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    invoke-static {v3, v5, v4}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 320
    .local v1, "contentStart":I
    add-int v3, v1, v2

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 321
    .local v0, "contentEnd":I
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    if-gt v3, v1, :cond_2

    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    if-ge v3, v0, :cond_0

    .line 322
    :cond_2
    invoke-direct {p0, v1, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setContentWindow(II)V

    goto :goto_0
.end method

.method protected setContentRange(IZ)V
    .locals 6
    .param p1, "size"    # I
    .param p2, "firstLoading"    # Z

    .prologue
    const/4 v5, 0x0

    .line 263
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    .line 265
    if-eqz p2, :cond_0

    const/16 v0, 0x15

    .line 266
    .local v0, "activeSize":I
    :goto_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveEnd:I

    add-int/2addr v3, v4

    sub-int/2addr v3, v0

    div-int/lit8 v3, v3, 0x2

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 267
    .local v2, "start":I
    add-int v3, v2, v0

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 268
    .local v1, "end":I
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveStart:I

    .line 269
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mThumbnailActiveEnd:I

    .line 271
    iget v3, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    add-int/2addr v3, v4

    add-int/lit16 v3, v3, -0x3e8

    div-int/lit8 v3, v3, 0x2

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 272
    add-int/lit16 v3, v2, 0x3e8

    iget v4, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mSize:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 273
    iput v2, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    .line 274
    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    .line 275
    return-void

    .line 265
    .end local v0    # "activeSize":I
    .end local v1    # "end":I
    .end local v2    # "start":I
    :cond_0
    const/16 v0, 0x40

    goto :goto_0
.end method

.method public setFocusOnText(Z)V
    .locals 0
    .param p1, "focusOnText"    # Z

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusOnText:Z

    .line 171
    return-void
.end method

.method public final setFocusedIndex(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 327
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setFocusedIndex(IZ)V

    .line 328
    return-void
.end method

.method public final setFocusedIndex(IZ)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "isChangeFocusMedia"    # Z

    .prologue
    const/4 v1, 0x0

    const/16 v0, -0x64

    .line 331
    if-ne p1, v0, :cond_1

    .line 332
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 333
    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 337
    :cond_1
    if-ltz p1, :cond_0

    .line 341
    if-eqz p2, :cond_2

    .line 342
    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentStart:I

    if-lt p1, v0, :cond_3

    iget v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mContentEnd:I

    if-ge p1, v0, :cond_3

    .line 343
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mData:[Lcom/sec/android/gallery3d/data/MediaObject;

    array-length v1, v1

    rem-int v1, p1, v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    .line 349
    :cond_2
    :goto_1
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedIndex:I

    goto :goto_0

    .line 345
    :cond_3
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mFocusedMedia:Lcom/sec/android/gallery3d/data/MediaObject;

    goto :goto_1
.end method

.method public setGenericMotionFocus(I)V
    .locals 0
    .param p1, "focus"    # I

    .prologue
    .line 162
    iput p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mGenericMotionFocus:I

    .line 163
    return-void
.end method

.method public setLoadingListener(Lcom/sec/android/gallery3d/app/LoadingListener;)V
    .locals 0
    .param p1, "loadingListener"    # Lcom/sec/android/gallery3d/app/LoadingListener;

    .prologue
    .line 174
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mLoadingListener:Lcom/sec/android/gallery3d/app/LoadingListener;

    .line 175
    return-void
.end method

.method public final setModelListener(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .line 179
    return-void
.end method

.method public final updateVisiableRange(II)V
    .locals 3
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 368
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "start = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " end = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 369
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->setActiveWindow(II)V

    .line 373
    :goto_0
    return-void

    .line 371
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mModelListener is null. start = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " end = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
