.class public Lcom/sec/samsung/gallery/controller/Play3DTourCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "Play3DTourCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final COMPONENT_NAME:Ljava/lang/String; = "com.sec.android.app.tourviewer.TourPlayerActivity"

.field private static final PACKAGE_NAME:Ljava/lang/String; = "com.sec.android.app.tourviewer"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method public static isSelected3DTour(Landroid/content/Context;)Z
    .locals 14
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    move-object v12, p0

    check-cast v12, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v12

    invoke-virtual {v12}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 77
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    check-cast p0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .end local p0    # "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v11

    .line 79
    .local v11, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    instance-of v12, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    if-nez v12, :cond_0

    instance-of v12, v1, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    if-nez v12, :cond_0

    instance-of v12, v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    if-eqz v12, :cond_4

    .line 80
    :cond_0
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getCloneMediaList()Ljava/util/LinkedList;

    move-result-object v9

    .line 81
    .local v9, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v9}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 82
    .local v8, "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v8, :cond_1

    .line 85
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v12, :cond_3

    move-object v10, v8

    .line 86
    check-cast v10, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 87
    .local v10, "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v0

    .line 88
    .local v0, "count":I
    const/4 v12, 0x0

    invoke-virtual {v10, v12, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v5

    .line 90
    .local v5, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 91
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_2

    check-cast v6, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    const-wide/16 v12, 0x400

    invoke-virtual {v6, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 92
    const/4 v12, 0x1

    .line 108
    .end local v0    # "count":I
    .end local v4    # "i$":Ljava/util/Iterator;
    .end local v5    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .end local v10    # "selectedSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_0
    return v12

    .line 94
    .restart local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    .restart local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_3
    instance-of v12, v8, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_1

    check-cast v8, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v8    # "selectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    const-wide/16 v12, 0x400

    invoke-virtual {v8, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 95
    const/4 v12, 0x1

    goto :goto_0

    .line 99
    .end local v9    # "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_4
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v7

    .line 100
    .local v7, "selectedCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v7, :cond_6

    .line 101
    invoke-virtual {v11, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    .line 102
    .local v6, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v12, v6, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v12, :cond_5

    check-cast v6, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v6    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    const-wide/16 v12, 0x400

    invoke-virtual {v6, v12, v13}, Lcom/sec/android/gallery3d/data/LocalImage;->hasAttribute(J)Z

    move-result v12

    if-eqz v12, :cond_5

    .line 103
    const/4 v12, 0x1

    goto :goto_0

    .line 100
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 108
    .end local v2    # "i":I
    .end local v7    # "selectedCount":I
    :cond_6
    const/4 v12, 0x0

    goto :goto_0
.end method

.method private start3Dtour(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 60
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 61
    .local v2, "filePath":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 73
    :goto_0
    return-void

    .line 63
    :cond_0
    new-instance v0, Landroid/content/ComponentName;

    const-string v4, "com.sec.android.app.tourviewer"

    const-string v5, "com.sec.android.app.tourviewer.TourPlayerActivity"

    invoke-direct {v0, v4, v5}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    .local v0, "component":Landroid/content/ComponentName;
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    .line 65
    .local v3, "intent":Landroid/content/Intent;
    invoke-virtual {v3, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 66
    const-string v4, "sef_file_name"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v1

    .line 70
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    sget-object v4, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->TAG:Ljava/lang/String;

    const-string v5, "Activity Not Found"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 49
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 50
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v0, v2, v3

    check-cast v0, Landroid/app/Activity;

    .line 51
    .local v0, "activity":Landroid/app/Activity;
    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->mContext:Landroid/content/Context;

    .line 52
    const/4 v3, 0x1

    aget-object v1, v2, v3

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 54
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 57
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->start3Dtour(Lcom/sec/android/gallery3d/data/MediaItem;)V

    goto :goto_0
.end method
