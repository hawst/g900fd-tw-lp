.class public Lcom/sec/samsung/gallery/view/eventview/EventViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "EventViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;,
        Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;,
        Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;,
        Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;
    }
.end annotation


# static fields
.field public static final REQUEST_ALBUM_CREATION:I = 0x701

.field private static final SELECT_MAX_ITEM_LIMIT:I

.field private static final TAG:Ljava/lang/String; = "EventViewState"


# instance fields
.field private isfromEditmode:Z

.field private mAdapterConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;

.field private mAlbumIndex:I

.field private mAlbumItemIndex:I

.field public mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

.field private mComposeViewConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;

.field private mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mCurrentMediaSetIndex:I

.field private mCurrentTopSetPath:Ljava/lang/String;

.field private mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

.field private mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

.field public mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

.field private mEventViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mFirstMediaCheck:Z

.field private mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mIsDeleteMode:Z

.field private mIsEasyMode:Z

.field private mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mMenu:Landroid/view/Menu;

.field mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

.field private mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mShowMapInEventViewHandler:Landroid/os/Handler;

.field private mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field private mShrinkOption:I

.field private mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field private mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    sput v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->SELECT_MAX_ITEM_LIMIT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 131
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    .line 136
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 137
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 138
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaSetIndex:I

    .line 145
    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 154
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->isfromEditmode:Z

    .line 155
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShrinkOption:I

    .line 157
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsDeleteMode:Z

    .line 159
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumIndex:I

    .line 160
    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumItemIndex:I

    .line 164
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$1;

    const-string v1, "EVENT_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$1;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 212
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$2;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShowMapInEventViewHandler:Landroid/os/Handler;

    .line 224
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$3;

    const-string v1, "SLIDESHOW_SETTINGS"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$3;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 247
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$4;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    .line 1240
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # I

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startMapView(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/drawer/NavigationDrawer;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object v0
.end method

.method static synthetic access$1602(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;)Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    return-object p1
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->checkMediaAvailability()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # I

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAlbum(I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # I

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startPhotoView(I)V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/data/EventAlbumManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 125
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->enterSelectionMode(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->showDeleteDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->exitSelectionMode()V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;II)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 125
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startDetailView(II)V

    return-void
.end method

.method static synthetic access$3100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3600(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$3800(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startSlideShow()V

    return-void
.end method

.method static synthetic access$3900(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllPostProcess()V

    return-void
.end method

.method static synthetic access$4200(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "x3"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;

    .prologue
    .line 125
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V

    return-void
.end method

.method static synthetic access$4300(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method static synthetic access$4400(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->addToSildeProxy()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    .prologue
    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private addToSildeProxy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 887
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 888
    iput-boolean v7, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->isfromEditmode:Z

    .line 889
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "sort_by_type_time"

    invoke-static {v0, v1, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v6

    .line 891
    .local v6, "type":I
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState$14;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$14;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    if-ne v6, v3, :cond_0

    move v4, v3

    :goto_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;ZZI)V

    new-array v1, v3, [Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 936
    .end local v6    # "type":I
    :goto_1
    return-void

    .restart local v6    # "type":I
    :cond_0
    move v4, v7

    .line 891
    goto :goto_0

    .line 913
    .end local v6    # "type":I
    :cond_1
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->isfromEditmode:Z

    .line 914
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState$15;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$15;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->size()I

    move-result v4

    invoke-direct {v0, v1, v2, v4}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/OnProgressListener;I)V

    new-array v1, v3, [Lcom/sec/android/gallery3d/ui/SelectionManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    aput-object v2, v1, v7

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SlideshowSelectionTaskForEdit;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method private checkMediaAvailability()V
    .locals 3

    .prologue
    .line 946
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mFirstMediaCheck:Z

    if-eqz v1, :cond_1

    .line 947
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/ui/EmptySetDrawer;->removeNoItemLayout(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 950
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v1, :cond_1

    .line 951
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getRootLayer()Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-result-object v1

    if-nez v1, :cond_0

    .line 952
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v1, v2, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 953
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshView(Z)V

    .line 956
    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mFirstMediaCheck:Z

    .line 958
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getEventItemCount()I

    move-result v0

    .line 959
    .local v0, "eventItemCount":I
    if-gtz v0, :cond_2

    .line 960
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->startTimeViewState()V

    .line 963
    :cond_2
    return-void
.end method

.method private enterSelectionMode(Z)V
    .locals 5
    .param p1, "useVibrator"    # Z

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 768
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isSelectionMode(Ljava/lang/String;)V

    .line 769
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 770
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->enterSelectionMode(Z)V

    .line 771
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 772
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 773
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_0

    .line 774
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setMode(IILjava/lang/Object;)V

    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshSelectionBarState(Z)V

    .line 777
    :cond_0
    return-void
.end method

.method private exitSelectionMode()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 780
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->leaveSelectionMode()V

    .line 781
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->exitSelectionMode()V

    .line 782
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 783
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->isDownloadMode()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 784
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setDownloadMode(Z)V

    .line 785
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->changeFilterAlbums(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/samsung/gallery/core/TabTagType;)V

    .line 798
    :cond_0
    :goto_0
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsDeleteMode:Z

    .line 799
    return-void

    .line 787
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getPopUpMenu()Landroid/widget/ListPopupWindow;

    move-result-object v0

    .line 788
    .local v0, "menu":Landroid/widget/ListPopupWindow;
    if-eqz v0, :cond_2

    .line 789
    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 791
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 792
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 793
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v1, :cond_0

    .line 794
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    const/4 v2, 0x0

    invoke-virtual {v1, v4, v4, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setMode(IILjava/lang/Object;)V

    .line 795
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v1, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshSelectionBarState(Z)V

    goto :goto_0
.end method

.method private finishCurrentViewState()V
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 1134
    return-void
.end method

.method private getRefreshState(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 877
    const/4 v0, 0x0

    .line 879
    .local v0, "refreshView":Z
    if-eqz p1, :cond_0

    .line 880
    const-string v1, "KEY_VIEW_REDRAW"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 882
    :cond_0
    return v0
.end method

.method private handleNewEventAlbum()V
    .locals 3

    .prologue
    .line 1027
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-class v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1028
    .local v0, "request":Landroid/content/Intent;
    const-string v1, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1029
    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1030
    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1032
    const-string v1, "pick-max-item"

    sget v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->SELECT_MAX_ITEM_LIMIT:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1033
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/16 v2, 0x701

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1034
    return-void
.end method

.method private runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;)V
    .locals 2
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;

    .prologue
    .line 1198
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$17;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$17;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Lcom/sec/android/gallery3d/data/MediaSet;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 1223
    return-void
.end method

.method private selectAlbum(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 841
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 843
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 844
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 845
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    const v2, 0x7f0e029e

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 852
    :goto_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    .line 853
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshCheckState()V

    .line 854
    return-void

    .line 848
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 849
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    const v2, 0x7f0e029d

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private selectAll()V
    .locals 4

    .prologue
    .line 802
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 803
    .local v0, "topMediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getTotalMediaItemCount()I

    move-result v1

    .line 805
    .local v1, "totalCount":I
    sget v2, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    if-le v1, v2, :cond_0

    .line 806
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->runSelectionTask(Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 811
    :goto_0
    return-void

    .line 808
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/4 v3, 0x0

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V

    .line 809
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAllPostProcess()V

    goto :goto_0
.end method

.method private selectAllPostProcess()V
    .locals 1

    .prologue
    .line 814
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    .line 815
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshCheckState()V

    .line 816
    return-void
.end method

.method private selectAllProcess(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "topMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "task"    # Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;

    .prologue
    const-wide/16 v4, 0x1

    .line 821
    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    .line 822
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "position":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 823
    if-eqz p3, :cond_1

    invoke-virtual {p3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 838
    :cond_0
    return-void

    .line 826
    :cond_1
    if-eqz p3, :cond_2

    .line 827
    const/4 v3, 0x0

    invoke-virtual {p3, v4, v5, v3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->increaseProgress(JZ)V

    .line 828
    const/4 v3, 0x1

    invoke-virtual {p3, v4, v5, v3}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$SelectionTask;->increaseProgress(JZ)V

    .line 831
    :cond_2
    invoke-virtual {p2, v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 832
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v1, :cond_3

    .line 822
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 835
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 836
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->addShareProperty(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1
.end method

.method private setLaunchMode()V
    .locals 3

    .prologue
    .line 1037
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1038
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1039
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1040
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1058
    :cond_0
    :goto_0
    return-void

    .line 1042
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1043
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForEdit;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1044
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    goto :goto_0

    .line 1047
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1048
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1049
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    goto :goto_0

    .line 1050
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1052
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/eventview/EventActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1053
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 1054
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    goto :goto_0
.end method

.method private showDeleteDialog()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1114
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getCount()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v3

    if-ne v2, v3, :cond_0

    move v0, v1

    .line 1116
    .local v0, "isSelectAll":Z
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v2, v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 1117
    return-void

    .line 1114
    .end local v0    # "isSelectAll":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private startDetailView(II)V
    .locals 5
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I

    .prologue
    .line 995
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 996
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    if-gt v3, p2, :cond_1

    .line 1010
    :cond_0
    :goto_0
    return-void

    .line 999
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v2, p2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 1000
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    .line 1004
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1005
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v3, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1006
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1007
    const-string v3, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1008
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1009
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startMapView(I)V
    .locals 6
    .param p1, "albumIndex"    # I

    .prologue
    .line 1013
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    .line 1014
    .local v2, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getLocation(Lcom/sec/android/gallery3d/data/MediaSet;)[D

    move-result-object v1

    .line 1015
    .local v1, "latLong":[D
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    invoke-virtual {v4, v2}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInitialScale(Lcom/sec/android/gallery3d/data/MediaSet;)I

    move-result v3

    .line 1016
    .local v3, "zoomLevel":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v5, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 1017
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1018
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "KEY_MEDIA_SET_PATH"

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1019
    const-string v4, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1020
    const-string v4, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1021
    const-string v4, "KEY_MEDIA_ITEM_LOCATION"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putDoubleArray(Ljava/lang/String;[D)V

    .line 1022
    const-string v4, "KEY_MAP_VIEW_ZOOM_LEVEL"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1023
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1024
    return-void
.end method

.method private startPhotoView(I)V
    .locals 4
    .param p1, "albumIndex"    # I

    .prologue
    .line 980
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 981
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v2

    if-gtz v2, :cond_1

    .line 992
    :cond_0
    :goto_0
    return-void

    .line 985
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v3, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/app/StateManager;->setPreviousViewState(Ljava/lang/Class;)V

    .line 986
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 987
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "KEY_MEDIA_SET_PATH"

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 988
    const-string v2, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 989
    const-string v2, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 990
    const-string v2, "KEY_NO_SPLIT_MODE"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 991
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    const-class v3, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v2, v3, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startSlideShow()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 939
    const/4 v1, 0x6

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    aput-object v3, v0, v1

    const/4 v1, 0x4

    aput-object v3, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->isfromEditmode:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 942
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_SLIDESHOW"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 943
    return-void
.end method

.method private startSlideshowSettings()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1147
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-eqz v0, :cond_1

    .line 1148
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 1149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 1150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 1151
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 1152
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v1, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 1153
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 1159
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_0

    .line 1160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setSlideShowMode(Z)V

    .line 1162
    :cond_0
    return-void

    .line 1155
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private startTimeViewState()V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$16;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$16;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 977
    return-void
.end method

.method private unselectAll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 857
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 858
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 859
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 860
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshCheckState()V

    .line 861
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v1, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v0, v1, v2, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 862
    return-void
.end method

.method private updateCountOnActionBar()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 865
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v2

    .line 866
    .local v2, "selectedCount":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getAllCount()I

    move-result v0

    .line 868
    .local v0, "allItemsCount":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    .line 869
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setSelectedItemCount(I)V

    .line 872
    if-gtz v0, :cond_0

    move v1, v3

    .line 873
    .local v1, "isAllSelected":Z
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    sget-object v5, Lcom/sec/samsung/gallery/util/Consts$ButtonType;->BUTTON_SELECTALL:Lcom/sec/samsung/gallery/util/Consts$ButtonType;

    invoke-virtual {v4, v5, v3, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateButton(Lcom/sec/samsung/gallery/util/Consts$ButtonType;IZ)V

    .line 874
    return-void

    .line 872
    .end local v1    # "isAllSelected":Z
    :cond_0
    if-ne v2, v0, :cond_1

    const/4 v1, 0x1

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_0
.end method


# virtual methods
.method public getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method

.method protected isAvailableSelect(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 2
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 1125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isAvailableDrm(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1126
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e0107

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 1127
    const/4 v0, 0x0

    .line 1129
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 680
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 681
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 683
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setSlideShowMode(Z)V

    .line 684
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->setLaunchMode()V

    .line 700
    :goto_0
    return-void

    .line 686
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->exitSelectionMode()V

    goto :goto_0

    .line 687
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 688
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 689
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setSlideShowMode(Z)V

    .line 690
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 691
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->setLaunchMode()V

    .line 692
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 693
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 694
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const v1, 0x7f0e0441

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 695
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    goto :goto_0

    .line 698
    :cond_4
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->finishCurrentViewState()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 1062
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1063
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 1066
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_1

    .line 1067
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onConfigChanged(Landroid/content/res/Configuration;)V

    .line 1068
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 1071
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_2

    .line 1072
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->resetLayout()V

    .line 1074
    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 7
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 268
    const-string v1, "Gallery_Performance"

    const-string v4, "EventViewState onCreate Start"

    invoke-static {v1, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const-string v1, "EventViewState"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onCreate  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    if-nez v1, :cond_0

    .line 272
    const-string v1, "EventViewState"

    const-string v2, "mActivity is null!"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    :goto_0
    return-void

    .line 276
    :cond_0
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mFirstMediaCheck:Z

    .line 278
    new-instance v4, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-direct {v4, v1}, Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;-><init>(Lcom/sec/android/gallery3d/app/GalleryActivity;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEnhancedAssistantMenu:Lcom/sec/samsung/gallery/view/common/EnhancedAssistantMenu;

    .line 279
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->hidePreDisplayScreen()V

    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 281
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    .line 282
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 283
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 284
    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v4, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 285
    new-instance v1, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v4}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 286
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->getRefreshState(Landroid/os/Bundle;)Z

    .line 287
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    const-class v4, Lcom/sec/samsung/gallery/view/eventview/EventViewState;

    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentViewMode(Ljava/lang/Class;)V

    .line 288
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    .line 289
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v4, 0x7f0f00bf

    invoke-virtual {v1, v4}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 290
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    .line 291
    iput-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 292
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_1
    move v1, v3

    :goto_1
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsEasyMode:Z

    .line 293
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 294
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 295
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle()V

    .line 297
    if-eqz p1, :cond_2

    .line 298
    const-string v1, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaSetIndex:I

    .line 301
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v2

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 303
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_3

    .line 304
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 306
    :cond_3
    const-string v1, "/local/event/all"

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    .line 307
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/DataManager;->getTopMediaSet()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 310
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :goto_2
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;

    .line 311
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;

    .line 312
    new-instance v1, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAdapterConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$DataLoaderConfig;

    const/4 v4, 0x3

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewDataLoader$DataConfig;I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    .line 313
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mModelListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setModelListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter$ModelListener;)V

    .line 314
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsEasyMode:Z

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setEasyMode(Z)V

    .line 317
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryApp:Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    .line 319
    const-string v1, "Gallery_Performance"

    const-string v2, "EventViewState onCreate End"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    move v1, v2

    .line 292
    goto/16 :goto_1

    .line 307
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDataProxy:Lcom/sec/android/gallery3d/data/DataManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentTopSetPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    goto :goto_2
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 635
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMenu:Landroid/view/Menu;

    .line 636
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 637
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 625
    const-string v0, "Gallery_Performance"

    const-string v1, "EventViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->onStop()V

    .line 629
    :cond_0
    const-string v0, "Gallery_Performance"

    const-string v1, "EventViewState onDestroy End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 630
    return-void
.end method

.method public onDirty()V
    .locals 2

    .prologue
    .line 1078
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-nez v1, :cond_1

    .line 1087
    :cond_0
    :goto_0
    return-void

    .line 1082
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getSource()Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 1083
    .local v0, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 1084
    check-cast v1, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->isNeedUpdateCluster()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1085
    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    .end local v0    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;->updateMediaSet()V

    goto :goto_0
.end method

.method public onMWLayoutChanged()V
    .locals 2

    .prologue
    .line 1121
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMenu:Landroid/view/Menu;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->updateMenuOperation(Landroid/view/Menu;)V

    .line 1122
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 664
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 665
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 666
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    .line 667
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_1

    .line 668
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 669
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->onBackPressed()V

    .line 676
    :cond_1
    :goto_0
    return-void

    .line 671
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 672
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 600
    const-string v0, "Gallery_Performance"

    const-string v1, "EventViewState onPause Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 602
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 603
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-eqz v0, :cond_0

    .line 604
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->onPause()V

    .line 607
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 608
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_1

    .line 609
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->saveCurrentScrollInfo()V

    .line 610
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->pause()V

    .line 613
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShrinkOption:I

    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "EVENT_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    if-eqz v0, :cond_2

    .line 616
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setGenericMotionFocus(I)V

    .line 617
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setGenericMotionTitleFocus(I)V

    .line 619
    :cond_2
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 620
    const-string v0, "Gallery_Performance"

    const-string v1, "EventViewState onPause End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 8
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 641
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 642
    iget-boolean v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsDeleteMode:Z

    if-eqz v4, :cond_2

    .line 643
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v1

    .line 644
    .local v1, "isSelected":Z
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-interface {p1}, Landroid/view/Menu;->size()I

    move-result v3

    .local v3, "n":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 645
    invoke-interface {p1, v0}, Landroid/view/Menu;->getItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 646
    .local v2, "item":Landroid/view/MenuItem;
    invoke-interface {v2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    const v5, 0x7f0f027b

    if-ne v4, v5, :cond_1

    .line 647
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 648
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 649
    if-eqz v1, :cond_0

    .line 650
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 644
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 652
    :cond_0
    invoke-interface {v2, v7}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 654
    :cond_1
    invoke-interface {v2, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 658
    .end local v0    # "i":I
    .end local v1    # "isSelected":Z
    .end local v2    # "item":Landroid/view/MenuItem;
    .end local v3    # "n":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v4, :cond_3

    .line 659
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 660
    :cond_3
    return-void
.end method

.method public onRefresh()V
    .locals 2

    .prologue
    .line 1142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_0

    .line 1143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->refreshView(Z)V

    .line 1144
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v1, 0x0

    const/4 v2, -0x1

    const/4 v8, 0x1

    .line 525
    const-string v0, "Gallery_Performance"

    const-string v3, "EventViewState onResume Start"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 526
    const/4 v6, -0x1

    .line 530
    .local v6, "initialCode":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEmergencyMode(Landroid/app/Activity;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_0
    move v0, v8

    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsEasyMode:Z

    .line 531
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-boolean v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsEasyMode:Z

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setEasyMode(Z)V

    .line 535
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerEventView;->getThumbSizeType(I)I

    move-result v7

    .line 536
    .local v7, "type":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setInitThumbType(I)V

    .line 538
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SLIDESHOW_SETTINGS"

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShowSlideshowsettingsMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 541
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-nez v0, :cond_1

    .line 542
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_1

    .line 543
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume()V

    .line 547
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->setLaunchMode()V

    .line 548
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 549
    const/4 v4, 0x1

    .line 550
    .local v4, "option":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_3

    .line 551
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;

    if-eqz v0, :cond_2

    .line 552
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->cancel(Z)Z

    .line 554
    :cond_2
    new-instance v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;

    invoke-direct {v0, p0, v9}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;Lcom/sec/samsung/gallery/view/eventview/EventViewState$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;

    .line 555
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mUpdateSelectionModeTask:Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$UpdateSelectionModeTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 560
    :cond_3
    :goto_1
    or-int/lit8 v4, v4, 0x40

    .line 561
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v0, :cond_a

    .line 562
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->getCodeForMediaItem(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v6

    .line 563
    if-ne v6, v2, :cond_4

    .line 564
    const-string v0, "EventViewState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialCode = -1!!, re-calculate initialCode with albumIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", itemIndex = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumItemIndex:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    iget v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumIndex:I

    shl-int/lit8 v0, v0, 0x10

    iget v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mAlbumItemIndex:I

    or-int v6, v0, v1

    .line 567
    :cond_4
    iget v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mShrinkOption:I

    or-int/2addr v4, v0

    .line 571
    :goto_2
    if-ltz v6, :cond_5

    .line 572
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setFirstIndex(I)V

    .line 574
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->setFirstLoadingCount(I)V

    .line 575
    new-instance v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaItem:Lcom/sec/android/gallery3d/data/MediaItem;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;-><init>(Landroid/content/Context;ILcom/sec/android/gallery3d/data/MediaItem;ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    .line 576
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v1, p0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 577
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;->onResume()V

    .line 579
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 581
    new-instance v0, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeviewcommon/ComposeViewBeam;->setBeamListener()V

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 584
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v0, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setSlideShowMode(Z)V

    .line 586
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 587
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryFeature;->checkUseStoryAlbum(Landroid/content/pm/PackageManager;)V

    .line 589
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->onDirty()V

    .line 591
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 593
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-static {v0, v1, v9}, Lcom/sec/android/gallery3d/util/GalleryUtils;->checkViewStateByFilterBySetting(Landroid/content/Context;Lcom/sec/android/gallery3d/app/StateManager;Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 595
    const-string v0, "Gallery_Performance"

    const-string v1, "EventViewState onResume End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    return-void

    .end local v4    # "option":I
    .end local v7    # "type":I
    :cond_8
    move v0, v1

    .line 530
    goto/16 :goto_0

    .line 558
    .restart local v7    # "type":I
    :cond_9
    const/4 v4, 0x0

    .restart local v4    # "option":I
    goto/16 :goto_1

    .line 569
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeViewConfig:Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;

    iget v6, v0, Lcom/sec/samsung/gallery/view/eventview/EventViewState$ComposeViewConfig;->mPrevCenterObject:I

    goto :goto_2
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 8
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 1091
    sparse-switch p1, :sswitch_data_0

    .line 1111
    :cond_0
    :goto_0
    return-void

    .line 1093
    :sswitch_0
    const/4 v6, 0x3

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v7, v2, v6

    const/4 v6, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v2, v6

    const/4 v6, 0x2

    aput-object p3, v2, v6

    .line 1096
    .local v2, "params":[Ljava/lang/Object;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v7, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v6, v7, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 1099
    .end local v2    # "params":[Ljava/lang/Object;
    :sswitch_1
    if-eqz p3, :cond_0

    .line 1100
    const-string v6, "selectedItems"

    invoke-virtual {p3, v6}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v5

    check-cast v5, Ljava/util/ArrayList;

    .line 1101
    .local v5, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 1102
    .local v1, "mDataManager":Lcom/sec/android/gallery3d/data/DataManager;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    .line 1103
    .local v3, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 1104
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/net/Uri;

    .line 1105
    .local v4, "uri":Landroid/net/Uri;
    const/4 v6, 0x0

    invoke-virtual {v1, v4, v6}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_1

    .line 1107
    .end local v4    # "uri":Landroid/net/Uri;
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventAlbumMgr:Lcom/sec/android/gallery3d/data/EventAlbumManager;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6, v7, v3}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->prepareCreateEventDialog(Landroid/content/Context;Lcom/sec/android/gallery3d/ui/SelectionManager;)V

    goto :goto_0

    .line 1091
    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_0
        0x701 -> :sswitch_1
    .end sparse-switch
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 4
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    .line 325
    .local v0, "glComposeView":Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
    const-string v1, "EventViewState"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initialzieView = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEventSetAdapter:Lcom/sec/samsung/gallery/view/adapter/ComposeEventSetAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V

    .line 328
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$5;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;)V

    .line 351
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$6;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$6;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemLongClickListener;)V

    .line 378
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$7;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$7;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;)V

    .line 392
    const/4 v1, 0x0

    new-instance v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$8;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 418
    const/4 v1, 0x2

    new-instance v2, Lcom/sec/samsung/gallery/view/eventview/EventViewState$9;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$9;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;)V

    .line 444
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$10;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$10;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnKeyListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnKeyListener;)V

    .line 464
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$11;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$11;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnHoverListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnHoverListener;)V

    .line 472
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$12;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$12;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnSwitchViewListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnSwitchViewListener;)V

    .line 495
    new-instance v1, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState$13;-><init>(Lcom/sec/samsung/gallery/view/eventview/EventViewState;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V

    .line 521
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 10
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    const/4 v9, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 704
    move-object v1, p2

    check-cast v1, Lcom/sec/samsung/gallery/core/Event;

    .line 705
    .local v1, "ev":Lcom/sec/samsung/gallery/core/Event;
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/core/Event;->getType()I

    move-result v2

    .line 707
    .local v2, "eventType":I
    const-string v4, "EventViewState"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Event update ["

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_DELETE_SELECTION_MODE:I

    if-ne v2, v4, :cond_1

    .line 710
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mIsDeleteMode:Z

    .line 711
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->enterSelectionMode(Z)V

    .line 712
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    .line 765
    :cond_0
    :goto_0
    return-void

    .line 713
    :cond_1
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_DELETE_DIALOG:I

    if-ne v2, v4, :cond_2

    .line 714
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->showDeleteDialog()V

    goto :goto_0

    .line 715
    :cond_2
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_SELECT_ALL:I

    if-ne v2, v4, :cond_3

    .line 716
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->selectAll()V

    goto :goto_0

    .line 717
    :cond_3
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_UNSELECT_ALL:I

    if-ne v2, v4, :cond_4

    .line 718
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->unselectAll()V

    goto :goto_0

    .line 719
    :cond_4
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_EXIT_SELECTION_MODE:I

    if-ne v2, v4, :cond_5

    .line 720
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->exitSelectionMode()V

    goto :goto_0

    .line 721
    :cond_5
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_SHOW_SLIDESHOW_SETTING:I

    if-ne v2, v4, :cond_9

    .line 722
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isIncomingCall(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 723
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v5, 0x7f0e0066

    invoke-static {v4, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0

    .line 726
    :cond_6
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSlideSettingPopup:Z

    if-nez v4, :cond_7

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExternalDisplayAvailable(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 727
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v4, v7}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 728
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 729
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v5, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v5, v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 730
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 731
    new-instance v4, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    const-string v5, "SLIDE_SHOW_SETTING_VIEW"

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v4, v5, v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;-><init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    .line 732
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mSlideShowSettingsMediator:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;

    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 737
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    invoke-virtual {v4, v8}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;->setSlideShowMode(Z)V

    goto :goto_0

    .line 734
    :cond_8
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "PREPARE_SLIDE_SHOW_DATA"

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1

    .line 738
    :cond_9
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_NEW_EVENT_ALBUM:I

    if-ne v2, v4, :cond_a

    .line 739
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->handleNewEventAlbum()V

    goto/16 :goto_0

    .line 740
    :cond_a
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_CAMERA_MODE:I

    if-ne v2, v4, :cond_b

    .line 741
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "START_CAMERA"

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 742
    :cond_b
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SELECTION_MODE:I

    if-ne v2, v4, :cond_c

    .line 743
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->enterSelectionMode(Z)V

    .line 744
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->updateCountOnActionBar()V

    goto/16 :goto_0

    .line 745
    :cond_c
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    if-ne v2, v4, :cond_d

    .line 746
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 747
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mMediaSetForCopy:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v6, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mCurrentMediaSetIndex:I

    const/4 v7, 0x5

    invoke-virtual {v4, v5, v6, v8, v7}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    goto/16 :goto_0

    .line 749
    :cond_d
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_SCAN_NEARBY_DEVICES:I

    if-ne v2, v4, :cond_e

    .line 751
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->scanForNearbyProviderDevices(Landroid/app/Activity;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 752
    :catch_0
    move-exception v0

    .line 753
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v4, "EventViewState"

    const-string v5, "NullPointer Exception, maybe nearby device is not initiated yet"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 755
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_e
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_LAST_SHARE_APP:I

    if-ne v2, v4, :cond_f

    .line 756
    new-array v3, v8, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v4, v3, v7

    .line 759
    .local v3, "params":[Ljava/lang/Object;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v5, "LAST_SHARE_APP"

    invoke-virtual {v4, v5, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 760
    .end local v3    # "params":[Ljava/lang/Object;
    :cond_f
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_SEARCH_MODE:I

    if-ne v2, v4, :cond_10

    .line 761
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchViewState;

    invoke-virtual {v4, v5, v9}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 762
    :cond_10
    sget v4, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTER_GALLERY_SEARCH_MODE:I

    if-ne v2, v4, :cond_0

    .line 763
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/eventview/EventViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    const-class v5, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-virtual {v4, v5, v9}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto/16 :goto_0
.end method
