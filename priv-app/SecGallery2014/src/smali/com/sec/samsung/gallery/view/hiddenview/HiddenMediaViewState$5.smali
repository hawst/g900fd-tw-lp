.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V
    .locals 7
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 218
    packed-switch p2, :pswitch_data_0

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 220
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 221
    .local v1, "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 222
    .local v2, "lastIndex":I
    add-int v3, v1, v2

    div-int/lit8 v0, v3, 0x2

    .line 223
    .local v0, "bgIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 224
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # setter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1202(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)I

    .line 225
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mBgImageIndex:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlGridView;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 230
    .end local v0    # "bgIndex":I
    .end local v1    # "firstIndex":I
    .end local v2    # "lastIndex":I
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 231
    .restart local v1    # "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 232
    .restart local v2    # "lastIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$5;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$100(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setActiveWindow(II)V

    goto :goto_0

    .line 218
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
