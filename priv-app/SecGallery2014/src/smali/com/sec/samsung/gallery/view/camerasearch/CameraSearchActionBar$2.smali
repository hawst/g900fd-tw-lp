.class Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;
.super Ljava/lang/Object;
.source "CameraSearchActionBar.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView$OnSuggestionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->setOnSuggestionListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSuggestionClick(I)Z
    .locals 4
    .param p1, "arg0"    # I

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/database/MatrixCursor;->moveToPosition(I)Z

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mSearchView:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$700(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->mCursor:Landroid/database/MatrixCursor;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;->access$600(Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchActionBar;)Landroid/database/MatrixCursor;

    move-result-object v2

    const-string v3, "name"

    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/database/MatrixCursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/camerasearch/CameraSearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public onSuggestionSelect(I)Z
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method
