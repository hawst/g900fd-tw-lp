.class public Lcom/sec/samsung/gallery/view/help/HelpViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "HelpViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field public static final HELP_AIR_ALBUM:I = 0x7

.field protected static final HELP_AIR_BUTTON:I = 0xb

.field public static final HELP_AIR_COMPLETE:I = 0xa

.field public static final HELP_AIR_HOVER:I = 0x8

.field public static final HELP_AIR_HOVER_NEXT:I = 0x9

.field protected static final HELP_ANIMATION_COUNT:I = 0x5

.field protected static final HELP_ANIMATION_HEAD_START_OFFSET:I = 0xc8

.field public static final HELP_DETAIL:I = 0x2

.field public static final HELP_FACE_DETAIL_LIST:I = 0x5

.field public static final HELP_FACE_DETAIL_NAME:I = 0x6

.field public static final HELP_FACE_DETAIL_TAP:I = 0x4

.field public static final HELP_FACE_PHOTO:I = 0x3

.field protected static final HELP_LIST_SCROLLING:I = 0xc

.field public static final HELP_PHOTO:I = 0x1

.field public static final HELP_PHOTONOTE:I = 0xd

.field public static final HELP_TIME:I = 0x0

.field private static final TAG:Ljava/lang/String; = "HelpViewState"

.field static mHelpPluginResources:Landroid/content/res/Resources;

.field protected static mPointX:F

.field protected static mPointY:F


# instance fields
.field protected mBG:Landroid/widget/ImageView;

.field protected mBubbleAnimation:Landroid/view/animation/Animation;

.field protected mBubbleHeadAnimation:Landroid/view/animation/Animation;

.field mConfiguration:Landroid/content/res/Configuration;

.field protected mCurrentPointAnimation:I

.field protected mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field protected mHead:Landroid/widget/ImageView;

.field mHeight:I

.field protected mHelpFaceListView:Landroid/view/View;

.field protected mHelpLayoutView:Landroid/view/View;

.field protected mHelpMode:I

.field protected mHelpMotionView:Landroid/view/View;

.field protected mHelpPopup:Landroid/widget/RelativeLayout;

.field protected mHoverEnterTime:J

.field protected mHoverModeOn:Z

.field protected mHoverView:Landroid/widget/ImageView;

.field protected mInflater:Landroid/view/LayoutInflater;

.field protected mItem:Landroid/widget/ImageView;

.field protected mLayoutParams:Landroid/view/WindowManager$LayoutParams;

.field protected mParentView:Landroid/widget/RelativeLayout;

.field protected mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

.field protected mPointAnimations:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/animation/Animation;",
            ">;"
        }
    .end annotation
.end field

.field protected mRelativeLayout:Landroid/widget/RelativeLayout;

.field protected mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

.field protected mRelativeLayoutToolBar:Landroid/widget/FrameLayout;

.field protected mResources:Landroid/content/res/Resources;

.field protected mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field protected mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field protected mTap:Landroid/widget/ImageView;

.field protected mText:Landroid/widget/TextView;

.field protected mVibrator:Landroid/os/Vibrator;

.field protected mViewMode:I

.field mWidth:I

.field protected mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPluginResources:Landroid/content/res/Resources;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 49
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 58
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    .line 59
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    .line 60
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpLayoutView:Landroid/view/View;

    .line 61
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpFaceListView:Landroid/view/View;

    .line 62
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpMotionView:Landroid/view/View;

    .line 63
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    .line 64
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    .line 65
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    .line 66
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    .line 67
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mItem:Landroid/widget/ImageView;

    .line 68
    iput v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mViewMode:I

    .line 69
    iput v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpMode:I

    .line 70
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHoverView:Landroid/widget/ImageView;

    .line 72
    iput v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHeight:I

    .line 73
    iput v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mWidth:I

    .line 74
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHoverModeOn:Z

    .line 75
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHoverEnterTime:J

    .line 77
    iput v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    .line 78
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 79
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleHeadAnimation:Landroid/view/animation/Animation;

    .line 106
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 107
    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    .line 403
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/help/HelpViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewState;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method private playHaptic()V
    .locals 4

    .prologue
    .line 344
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    if-nez v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    .line 347
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v0}, Landroid/os/Vibrator;->cancel()V

    .line 349
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v0, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 351
    :cond_1
    return-void
.end method

.method private playSound()V
    .locals 2

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-nez v0, :cond_0

    .line 337
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 339
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    if-eqz v0, :cond_1

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/utils/SoundUtils;->playSoundResource(I)V

    .line 341
    :cond_1
    return-void
.end method

.method public static setPointInfo(FF)V
    .locals 0
    .param p0, "x"    # F
    .param p1, "y"    # F

    .prologue
    .line 424
    sput p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointX:F

    .line 425
    sput p1, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointY:F

    .line 426
    return-void
.end method


# virtual methods
.method protected animationInit(Landroid/content/Context;)V
    .locals 3
    .param p1, "mActivity"    # Landroid/content/Context;

    .prologue
    .line 368
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    .line 370
    const v1, 0x7f040015

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 371
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 372
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 374
    const v1, 0x7f040016

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 376
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 378
    const v1, 0x7f040017

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 379
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 380
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 382
    const v1, 0x7f040018

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 383
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 384
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    const v1, 0x7f040019

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 387
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimationListener:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 388
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 389
    return-void
.end method

.method public getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "resourceName"    # Ljava/lang/String;

    .prologue
    .line 430
    sget-object v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPluginResources:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpResources(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPluginResources:Landroid/content/res/Resources;

    .line 433
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPluginResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_1

    .line 434
    sget-object v0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPluginResources:Landroid/content/res/Resources;

    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/help/GalleryHelp;->getHelpDrawable(Landroid/content/res/Resources;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 437
    :goto_0
    return-object v0

    .line 436
    :cond_1
    const-string v0, "HelpViewState"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getHelpDrawable : not find resource!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 437
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 246
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 355
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/app/ActivityState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 356
    const-string v0, "HelpViewState"

    const-string v1, "onConfigChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 358
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 359
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 362
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startTapAnimation(I)V

    .line 363
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 13
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v1, -0x1

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v8

    .line 115
    .local v8, "win":Landroid/view/Window;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v2, "window"

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mWindowManager:Landroid/view/WindowManager;

    .line 116
    invoke-virtual {v8}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v9

    .line 117
    .local v9, "winParams":Landroid/view/WindowManager$LayoutParams;
    iget v0, v9, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, v9, Landroid/view/WindowManager$LayoutParams;->multiWindowFlags:I

    .line 118
    invoke-virtual {v8, v9}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 119
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/4 v3, 0x2

    const/16 v4, 0x418

    const/4 v5, -0x3

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    .line 123
    new-instance v0, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v2, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 125
    if-eqz p1, :cond_0

    const-string v0, "Help"

    invoke-virtual {p1, v0, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpMode:I

    .line 127
    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v6, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 128
    .local v6, "params":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0175

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mInflater:Landroid/view/LayoutInflater;

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006f

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpLayoutView:Landroid/view/View;

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006e

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpFaceListView:Landroid/view/View;

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mInflater:Landroid/view/LayoutInflater;

    const v1, 0x7f03006d

    invoke-virtual {v0, v1, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    .line 134
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mLayoutParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v0, v1, v2}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpLayoutView:Landroid/view/View;

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpFaceListView:Landroid/view/View;

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mRelativeLayoutForCustomTap:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v12}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpLayoutView:Landroid/view/View;

    invoke-virtual {v0, v10}, Landroid/view/View;->setVisibility(I)V

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpFaceListView:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    .line 141
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0130

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0129

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f013b

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    .line 144
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0128

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    .line 145
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f013a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    .line 146
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0f0134

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHoverView:Landroid/widget/ImageView;

    .line 147
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mResources:Landroid/content/res/Resources;

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 150
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string/jumbo v1, "vibrator"

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Vibrator;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mVibrator:Landroid/os/Vibrator;

    .line 152
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v7

    .line 153
    .local v7, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lcom/sec/android/gallery3d/app/StateManager;->setHelpMode(Z)V

    .line 155
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 156
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->animationInit(Landroid/content/Context;)V

    .line 157
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setOnClickListener()V

    .line 158
    return-void

    .end local v6    # "params":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v7    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :cond_0
    move v0, v10

    .line 125
    goto/16 :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 226
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 227
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 220
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 221
    .local v0, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/StateManager;->setHelpMode(Z)V

    .line 222
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 236
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    .line 241
    :goto_0
    return-void

    .line 239
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e02da

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 216
    :cond_0
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 232
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 204
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 205
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startTapAnimation(I)V

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpLayoutView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 207
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mParentView:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->invalidate()V

    .line 208
    return-void
.end method

.method public onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 0
    .param p1, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 256
    return-void
.end method

.method protected setHelpView()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 259
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getLcdRect(Landroid/content/Context;)Landroid/graphics/Rect;

    move-result-object v0

    .line 260
    .local v0, "LCDSize":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHeight:I

    .line 261
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mWidth:I

    .line 263
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    if-nez v1, :cond_0

    .line 264
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0130

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    .line 265
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    if-nez v1, :cond_1

    .line 266
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0129

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHelpPopup:Landroid/widget/RelativeLayout;

    .line 267
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    if-nez v1, :cond_2

    .line 268
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f013b

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    .line 269
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    if-nez v1, :cond_3

    .line 270
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f0128

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    .line 271
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    if-nez v1, :cond_4

    .line 272
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v2, 0x7f0f013a

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    .line 274
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageView;->measure(II)V

    .line 275
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    invoke-virtual {v1, v3, v3}, Landroid/widget/ImageView;->measure(II)V

    .line 277
    return-void
.end method

.method protected setOnClickListener()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 163
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBG:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 199
    :cond_1
    return-void
.end method

.method protected showCompletedPopup()V
    .locals 4

    .prologue
    .line 441
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f013c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    .line 442
    .local v0, "completePopup":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f040002

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 443
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 444
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0e02dc

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 445
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 446
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 447
    return-void
.end method

.method protected soundAndVibrator()V
    .locals 2

    .prologue
    .line 316
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getModeAirViewSoundAndHaptic(Landroid/content/Context;)I

    move-result v0

    .line 318
    .local v0, "mode":I
    packed-switch v0, :pswitch_data_0

    .line 333
    :goto_0
    return-void

    .line 320
    :pswitch_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->playSound()V

    .line 321
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->playHaptic()V

    goto :goto_0

    .line 324
    :pswitch_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->playSound()V

    goto :goto_0

    .line 327
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->playHaptic()V

    goto :goto_0

    .line 318
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected startBubbleAnimation()V
    .locals 8

    .prologue
    const-wide/16 v2, 0x12c

    const/4 v0, 0x4

    .line 280
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mText:Landroid/widget/TextView;

    .line 281
    .local v6, "text":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mHead:Landroid/widget/ImageView;

    .line 283
    .local v7, "head":Landroid/widget/ImageView;
    if-eqz v6, :cond_0

    .line 284
    invoke-virtual {v6, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    :cond_0
    if-eqz v7, :cond_1

    .line 286
    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 287
    :cond_1
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;JJLandroid/widget/TextView;Landroid/widget/ImageView;)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState$2;->start()Landroid/os/CountDownTimer;

    .line 307
    return-void
.end method

.method protected startFinishActivity(JJ)V
    .locals 7
    .param p1, "millisInFuture"    # J
    .param p3, "countDownInterval"    # J

    .prologue
    .line 392
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpViewState$3;

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/help/HelpViewState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewState;JJ)V

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState$3;->start()Landroid/os/CountDownTimer;

    .line 401
    return-void
.end method

.method protected startTapAnimation(I)V
    .locals 3
    .param p1, "setIndex"    # I

    .prologue
    .line 310
    iput p1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    .line 311
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 312
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mTap:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mPointAnimations:Ljava/util/List;

    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewState;->mCurrentPointAnimation:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/animation/Animation;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 313
    :cond_0
    return-void
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 251
    return-void
.end method
