.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;
.super Ljava/lang/Object;
.source "MoreInfoItemUserTag.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView$Result;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->runUserTagEditView(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)V
    .locals 0

    .prologue
    .line 390
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 407
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    const/4 v1, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$902(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .line 408
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextHasFocus:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->requestFocus()Z

    .line 411
    :cond_0
    return-void
.end method

.method public done(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "data":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v0, 0x1

    .line 393
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->getOriginalTags()Ljava/util/ArrayList;

    move-result-object v1

    .line 394
    .local v1, "original":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mAdapters:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;

    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoBaseAdapter;->changeDataSet(Ljava/util/ArrayList;)V

    .line 395
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    if-eqz v2, :cond_1

    .line 396
    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->containsAll(Ljava/util/Collection;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 397
    .local v0, "changed":Z
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditListener:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;

    invoke-interface {v2, v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItem$EditListener;->onEdit(Z)V

    .line 399
    .end local v0    # "changed":Z
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    const/4 v3, 0x0

    # setter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mUserTagEditView:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$902(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoUserTagEditView;

    .line 400
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditTextHasFocus:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 401
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$7;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->mEditText:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;->access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag;)Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoCustomEditText;->requestFocus()Z

    .line 403
    :cond_2
    return-void

    .line 396
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
