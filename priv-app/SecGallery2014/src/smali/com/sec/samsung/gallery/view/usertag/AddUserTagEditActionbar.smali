.class public Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
.super Ljava/lang/Object;
.source "AddUserTagEditActionbar.java"


# static fields
.field public static ACTIONBAR_BG_TYPE_DEFAULT:I = 0x0

.field public static ACTIONBAR_BG_TYPE_SHADOW:I = 0x0

.field public static final STYLE_BLACK:I = 0x1

.field public static final STYLE_DEFAULT:I = 0x0

.field public static final STYLE_TRANSPARENT:I = 0x2


# instance fields
.field customView:Landroid/view/View;

.field private mActivity:Landroid/app/Activity;

.field private mEnabledDoneBtn:Z

.field private mMainActionBar:Landroid/app/ActionBar;

.field private mMenu:Landroid/view/Menu;

.field protected mPopupMenu:Landroid/widget/ListPopupWindow;

.field private mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

.field private mSelectionButton:Landroid/widget/Button;

.field private mStyle:I

.field private mTitle:Ljava/lang/String;

.field protected mUsingSelectionDropDown:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->ACTIONBAR_BG_TYPE_DEFAULT:I

    .line 30
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->ACTIONBAR_BG_TYPE_SHADOW:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;

    .line 40
    iput v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mStyle:I

    .line 41
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mEnabledDoneBtn:Z

    .line 42
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAddTagSelectionDropDown:Z

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    .line 49
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    .line 50
    invoke-virtual {p1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    .line 51
    iput v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mStyle:I

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$1;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 83
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setActionBarLayout()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/app/ActionBar;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setPopUpMenu(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)Landroid/widget/Button;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Landroid/widget/Button;)Landroid/widget/Button;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;
    .param p1, "x1"    # Landroid/widget/Button;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;

    return-object p1
.end method

.method private setActionBarLayout()V
    .locals 6

    .prologue
    const v5, 0x7f11002a

    .line 219
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200e6

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 222
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mStyle:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 223
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/Context;->setTheme(I)V

    .line 224
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f02003c

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 228
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    .line 231
    const/16 v0, 0x10

    .line 232
    .local v0, "flags":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 233
    return-void

    .line 226
    .end local v0    # "flags":I
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->getThemedContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/content/Context;->setTheme(I)V

    goto :goto_0
.end method

.method private setPopUpMenu(Ljava/lang/String;)V
    .locals 21
    .param p1, "buttonTitle"    # Ljava/lang/String;

    .prologue
    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    move-object/from16 v18, v0

    const v19, 0x7f0f0018

    invoke-virtual/range {v18 .. v19}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/Button;

    .line 98
    .local v16, "selectAllButton":Landroid/widget/Button;
    if-nez v16, :cond_0

    .line 205
    :goto_0
    return-void

    .line 101
    :cond_0
    sget v18, Lcom/sec/android/gallery3d/util/GalleryFeature;->mSystemUiVisibility:I

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setSystemUiVisibility(I)V

    .line 102
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setHoverPopupType(I)V

    .line 104
    new-instance v18, Landroid/widget/ListPopupWindow;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    invoke-direct/range {v18 .. v19}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    .line 105
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 106
    .local v15, "res":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    const v19, 0x7f0d0006

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v19

    move/from16 v0, v19

    float-to-int v0, v0

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 108
    const/16 v18, 0x2

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    const v19, 0x7f0e0048

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    const/16 v18, 0x1

    const v19, 0x7f0e0049

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v19

    aput-object v19, v17, v18

    .line 109
    .local v17, "strs":[Ljava/lang/String;
    invoke-virtual/range {v16 .. v16}, Landroid/widget/Button;->getPaint()Landroid/text/TextPaint;

    move-result-object v9

    .line 110
    .local v9, "paint":Landroid/text/TextPaint;
    const/4 v4, 0x0

    .line 111
    .local v4, "contentWidth":F
    move-object/from16 v3, v17

    .local v3, "arr$":[Ljava/lang/String;
    array-length v7, v3

    .local v7, "len$":I
    const/4 v6, 0x0

    .local v6, "i$":I
    :goto_1
    if-ge v6, v7, :cond_1

    aget-object v8, v3, v6

    .line 112
    .local v8, "menuOption":Ljava/lang/String;
    invoke-virtual {v9, v8}, Landroid/text/TextPaint;->measureText(Ljava/lang/String;)F

    move-result v18

    move/from16 v0, v18

    invoke-static {v4, v0}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 111
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 114
    .end local v8    # "menuOption":Ljava/lang/String;
    :cond_1
    const/4 v10, 0x0

    .line 116
    .local v10, "popupItemWidth":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v18

    if-eqz v18, :cond_5

    .line 117
    const v18, 0x7f0d0006

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v12, v0

    .line 119
    .local v12, "popupMenuWidth":I
    const v18, 0x7f0d025c

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 122
    .local v11, "popupMenuItemPaddings":I
    float-to-int v0, v4

    move/from16 v18, v0

    mul-int/lit8 v19, v11, 0x2

    add-int v10, v18, v19

    .line 124
    if-ge v10, v12, :cond_2

    .line 125
    move v10, v12

    .line 145
    .end local v12    # "popupMenuWidth":I
    :cond_2
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/widget/ListPopupWindow;->setWidth(I)V

    .line 147
    if-eqz p1, :cond_3

    .line 148
    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_3
    new-instance v18, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$2;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$2;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 166
    const v18, 0x7f0d0259

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 167
    .local v5, "horizontalOffset":I
    if-eqz v5, :cond_4

    .line 168
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Landroid/widget/ListPopupWindow;->setHorizontalOffset(I)V

    .line 170
    :cond_4
    new-instance v18, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    move-object/from16 v19, v0

    const v20, 0x7f0300cc

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v1, v2, v10}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;-><init>(Landroid/content/Context;II)V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setMenuEnable(ZZ)V

    .line 173
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 174
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    const/16 v19, 0x1

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$3;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$3;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setSelectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$4;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$4;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    invoke-virtual/range {v18 .. v19}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setUnselectAllClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    move-object/from16 v18, v0

    new-instance v19, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$5;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$5;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;)V

    invoke-virtual/range {v18 .. v19}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto/16 :goto_0

    .line 128
    .end local v5    # "horizontalOffset":I
    .end local v11    # "popupMenuItemPaddings":I
    :cond_5
    const v18, 0x7f0d0006

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v14, v0

    .line 130
    .local v14, "popupMenuWidthMin":I
    const v18, 0x7f0d0257

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v13, v0

    .line 132
    .local v13, "popupMenuWidthMax":I
    const v18, 0x7f0d025c

    move/from16 v0, v18

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v18

    move/from16 v0, v18

    float-to-int v11, v0

    .line 135
    .restart local v11    # "popupMenuItemPaddings":I
    mul-int/lit8 v18, v11, 0x2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    add-float v18, v18, v4

    move/from16 v0, v18

    float-to-int v10, v0

    .line 137
    if-ge v10, v14, :cond_6

    .line 138
    move v10, v14

    .line 140
    :cond_6
    if-le v10, v13, :cond_2

    .line 141
    move v10, v13

    goto/16 :goto_2
.end method

.method private setTextOnlyButton(Landroid/view/Menu;IZ)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # I
    .param p3, "isTextOnly"    # Z

    .prologue
    .line 308
    invoke-interface {p1, p2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 309
    .local v0, "menuItem":Landroid/view/MenuItem;
    if-nez v0, :cond_0

    .line 316
    :goto_0
    return-void

    .line 312
    :cond_0
    if-eqz p3, :cond_1

    .line 313
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    goto :goto_0

    .line 315
    :cond_1
    invoke-interface {v0}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public getActionBar()Landroid/app/ActionBar;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    return-object v0
.end method

.method protected initActionBarBg(I)V
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 236
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02003c

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 238
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->ACTIONBAR_BG_TYPE_SHADOW:I

    if-ne p1, v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200ab

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected initDisplayOptions()V
    .locals 3

    .prologue
    .line 244
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    if-nez v1, :cond_0

    .line 255
    :goto_0
    return-void

    .line 248
    :cond_0
    const/4 v0, 0x0

    .line 249
    .local v0, "options":I
    const/16 v0, 0xc

    .line 250
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-nez v1, :cond_1

    .line 251
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    const/16 v2, 0xc

    invoke-virtual {v1, v0, v2}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 254
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 280
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 281
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120041

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 282
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMenu:Landroid/view/Menu;

    .line 283
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mEnabledDoneBtn:Z

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->updateDoneButton(Z)V

    .line 284
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 1
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 287
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 293
    .local v0, "id":I
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 271
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 277
    :cond_0
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;Z)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "hideDoneButton"    # Z

    .prologue
    const v2, 0x7f0f0025

    .line 296
    if-eqz p2, :cond_0

    .line 297
    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 298
    .local v0, "menuItem":Landroid/view/MenuItem;
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 301
    .end local v0    # "menuItem":Landroid/view/MenuItem;
    :goto_0
    return-void

    .line 300
    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, p1, v2, v1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->setTextOnlyButton(Landroid/view/Menu;IZ)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mUsingSelectionDropDown:Z

    if-eqz v0, :cond_1

    .line 259
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    const v1, 0x7f0f0018

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;

    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectionButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    .line 267
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->initDisplayOptions()V

    .line 268
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected processSelectAllMenuItemClick(I)V
    .locals 1
    .param p1, "itemId"    # I

    .prologue
    .line 208
    packed-switch p1, :pswitch_data_0

    .line 216
    :goto_0
    return-void

    .line 210
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->selectAll()V

    goto :goto_0

    .line 213
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    check-cast v0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagListActivity;->unSelectAll()V

    goto :goto_0

    .line 208
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mActivity:Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar$6;-><init>(Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 335
    return-void
.end method

.method public updateBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 304
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 305
    return-void
.end method

.method public updateDoneButton(Z)V
    .locals 3
    .param p1, "enable"    # Z

    .prologue
    .line 338
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMenu:Landroid/view/Menu;

    if-nez v1, :cond_1

    .line 345
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mMenu:Landroid/view/Menu;

    const v2, 0x7f0f0025

    invoke-interface {v1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 341
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 342
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mEnabledDoneBtn:Z

    .line 343
    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected updatePopupMenuItemVisibility(ZI)V
    .locals 4
    .param p1, "isSelectAll"    # Z
    .param p2, "quantitySelectedAlbum"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mPopupMenu:Landroid/widget/ListPopupWindow;

    if-eqz v3, :cond_1

    .line 87
    if-lez p2, :cond_2

    move v0, v1

    .line 89
    .local v0, "isUnsellectAll":Z
    :goto_0
    if-eqz p1, :cond_0

    if-nez p2, :cond_0

    .line 90
    const/4 p1, 0x0

    .line 92
    :cond_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/usertag/AddUserTagEditActionbar;->mSelectAllMenuAdapter:Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;

    if-nez p1, :cond_3

    :goto_1
    invoke-virtual {v3, v1, v0}, Lcom/sec/samsung/gallery/view/SelectAllMenuAdapter;->setMenuEnable(ZZ)V

    .line 94
    .end local v0    # "isUnsellectAll":Z
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 87
    goto :goto_0

    .restart local v0    # "isUnsellectAll":Z
    :cond_3
    move v1, v2

    .line 92
    goto :goto_1
.end method
