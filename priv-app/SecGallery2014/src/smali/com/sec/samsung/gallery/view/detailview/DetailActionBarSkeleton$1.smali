.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;
.super Ljava/lang/Object;
.source "DetailActionBarSkeleton.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;)V
    .locals 0

    .prologue
    .line 23
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 26
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->access$000(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 28
    .local v0, "customView":Landroid/view/View;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->access$100(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 29
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->access$200(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;)Landroid/app/ActionBar;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setNavigationMode(I)V

    .line 30
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->mMainActionBar:Landroid/app/ActionBar;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;)Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 31
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    sget v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->ACTIONBAR_BG_TYPE_SHADOW:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->initActionBarBg(I)V

    .line 32
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;->initDisplayOtpions()V

    .line 33
    return-void
.end method
