.class public Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;
.super Ljava/lang/Object;
.source "SecMMCodecInterface.java"


# static fields
.field private static mInstance:Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;


# instance fields
.field private secmmfd:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-string v0, "SecMMCodec"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static decodeByteArray([BII)Landroid/graphics/Bitmap;
    .locals 16
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 121
    const/4 v15, 0x0

    .line 122
    .local v15, "mBm_t":Landroid/graphics/Bitmap;
    const/4 v2, 0x2

    new-array v7, v2, [I

    .line 123
    .local v7, "tmp":[I
    const-wide/16 v12, 0x0

    .line 124
    .local v12, "secmmfd":J
    if-eqz p0, :cond_1

    if-lez p2, :cond_1

    move/from16 v0, p1

    move/from16 v1, p2

    if-ge v0, v1, :cond_1

    .line 125
    const-string v2, "nofile"

    move-object/from16 v0, p0

    array-length v5, v0

    const/4 v6, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-static/range {v2 .. v7}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativedecinstance(Ljava/lang/String;[BIII[I)J

    move-result-wide v12

    .line 126
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-eqz v2, :cond_0

    .line 127
    move-object/from16 v0, p0

    array-length v11, v0

    const/4 v14, 0x1

    move-object/from16 v9, p0

    move/from16 v10, p1

    invoke-static/range {v9 .. v15}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeDecodeByteArray([BIIJILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 134
    :goto_0
    return-object v2

    .line 130
    :cond_0
    invoke-static/range {p0 .. p2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 134
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 103
    const-wide/16 v2, 0x0

    .line 104
    .local v2, "secmmfd":J
    const/4 v0, 0x0

    .line 105
    .local v0, "mBm":Landroid/graphics/Bitmap;
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 106
    .local v1, "tmp":[I
    if-eqz p0, :cond_0

    if-lez p2, :cond_0

    if-ge p1, p2, :cond_0

    .line 107
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeCreateFds()J

    move-result-wide v2

    .line 108
    array-length v4, p0

    invoke-static {v2, v3, p0, p1, v4}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativecopybytebuffer(J[BII)I

    .line 109
    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativegetImageinfo(J[I)I

    .line 110
    invoke-static {v2, v3, v1, p3}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->doDecode(J[ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 111
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativefreeFds(J)I

    .line 112
    const-wide/16 v2, 0x0

    .line 113
    if-nez v0, :cond_0

    .line 114
    invoke-static {p0, p1, p2, p3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 117
    :cond_0
    return-object v0
.end method

.method public static decodeByteArrayBB([BIILandroid/graphics/BitmapFactory$Options;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 17
    .param p0, "data"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p4, "bm"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 139
    const-wide/16 v12, 0x0

    .line 140
    .local v12, "secmmfd":J
    const/4 v2, 0x2

    new-array v7, v2, [I

    .line 141
    .local v7, "tmp":[I
    if-eqz p0, :cond_4

    if-lez p2, :cond_4

    move/from16 v0, p1

    move/from16 v1, p2

    if-ge v0, v1, :cond_4

    .line 142
    const-string v2, "nofile"

    move-object/from16 v0, p0

    array-length v5, v0

    const/4 v6, 0x0

    move-object/from16 v3, p0

    move/from16 v4, p1

    invoke-static/range {v2 .. v7}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativedecinstance(Ljava/lang/String;[BIII[I)J

    move-result-wide v12

    .line 143
    const-wide/16 v2, 0x0

    cmp-long v2, v12, v2

    if-eqz v2, :cond_0

    .line 144
    const/4 v2, 0x0

    aget v16, v7, v2

    .line 145
    .local v16, "img_wid":I
    const/4 v2, 0x1

    aget v8, v7, v2

    .line 146
    .local v8, "img_hei":I
    move-object/from16 v0, p3

    iget-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v2, :cond_1

    .line 147
    move/from16 v0, v16

    move-object/from16 v1, p3

    iput v0, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 148
    move-object/from16 v0, p3

    iput v8, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 171
    .end local v8    # "img_hei":I
    .end local v16    # "img_wid":I
    .end local p4    # "bm":Ljava/nio/ByteBuffer;
    :cond_0
    :goto_0
    return-object p4

    .line 151
    .restart local v8    # "img_hei":I
    .restart local v16    # "img_wid":I
    .restart local p4    # "bm":Ljava/nio/ByteBuffer;
    :cond_1
    move-object/from16 v0, p3

    iget v14, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 152
    .local v14, "samplesize":I
    if-nez v14, :cond_2

    .line 153
    const/4 v14, 0x1

    .line 155
    :cond_2
    add-int v2, v16, v14

    add-int/lit8 v2, v2, -0x1

    div-int v16, v2, v14

    .line 156
    add-int v2, v8, v14

    add-int/lit8 v2, v2, -0x1

    div-int v8, v2, v14

    .line 157
    if-eqz p4, :cond_3

    .line 158
    invoke-virtual/range {p4 .. p4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v2

    mul-int v3, v16, v8

    mul-int/lit8 v3, v3, 0x4

    if-ge v2, v3, :cond_3

    .line 159
    const-string v2, "SecMM"

    const-string v3, "SecMMCodec DecodeBytearray Input Bitmap Erraneous\n"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 163
    :cond_3
    move-object/from16 v0, p0

    array-length v11, v0

    move-object/from16 v9, p0

    move/from16 v10, p1

    move-object/from16 v15, p4

    invoke-static/range {v9 .. v15}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeDecodeByteArrayBB([BIIJILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p4

    goto :goto_0

    .line 171
    .end local v8    # "img_hei":I
    .end local v14    # "samplesize":I
    .end local v16    # "img_wid":I
    :cond_4
    const/16 p4, 0x0

    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "pathName"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 176
    const/4 v6, 0x0

    .line 177
    .local v6, "mBm_t":Landroid/graphics/Bitmap;
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 178
    .local v5, "tmp":[I
    const-wide/16 v8, 0x0

    .line 179
    .local v8, "secmmfd":J
    if-eqz p0, :cond_0

    move-object v0, p0

    move v3, v2

    .line 180
    invoke-static/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativedecinstance(Ljava/lang/String;[BIII[I)J

    move-result-wide v8

    .line 181
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-eqz v0, :cond_1

    .line 182
    invoke-static {p0, v8, v9, v4, v6}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeDecodeFile(Ljava/lang/String;JILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 188
    :cond_0
    :goto_0
    return-object v1

    .line 185
    :cond_1
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public static decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 193
    const-wide/16 v2, 0x0

    .line 194
    .local v2, "secmmfd":J
    const/4 v0, 0x0

    .line 195
    .local v0, "mBm":Landroid/graphics/Bitmap;
    const/4 v4, 0x2

    new-array v1, v4, [I

    .line 196
    .local v1, "tmp":[I
    if-eqz p0, :cond_0

    .line 197
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeCreateFds()J

    move-result-wide v2

    .line 198
    invoke-static {v2, v3, p0}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativecopyfilename(JLjava/lang/String;)I

    .line 199
    invoke-static {v2, v3, v1}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativegetImageinfo(J[I)I

    .line 200
    invoke-static {v2, v3, v1, p1}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->doDecode(J[ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 201
    invoke-static {v2, v3}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativefreeFds(J)I

    .line 202
    const-wide/16 v2, 0x0

    .line 203
    if-nez v0, :cond_0

    .line 204
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 207
    :cond_0
    return-object v0
.end method

.method public static decodeFileBB(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 12
    .param p0, "pathName"    # Ljava/lang/String;
    .param p1, "opts"    # Landroid/graphics/BitmapFactory$Options;
    .param p2, "bm"    # Ljava/nio/ByteBuffer;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 211
    const-wide/16 v10, 0x0

    .line 212
    .local v10, "secmmfd":J
    const/4 v0, 0x2

    new-array v5, v0, [I

    .line 213
    .local v5, "tmp":[I
    const/4 v1, 0x0

    move-object v0, p0

    move v3, v2

    invoke-static/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativedecinstance(Ljava/lang/String;[BIII[I)J

    move-result-wide v10

    .line 214
    const-wide/16 v0, 0x0

    cmp-long v0, v10, v0

    if-eqz v0, :cond_3

    .line 215
    aget v8, v5, v2

    .line 216
    .local v8, "img_wid":I
    aget v7, v5, v4

    .line 217
    .local v7, "img_hei":I
    iget-boolean v0, p1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v0, :cond_0

    .line 218
    iput v8, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 219
    iput v7, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    move-object v6, p2

    .line 240
    .end local v7    # "img_hei":I
    .end local v8    # "img_wid":I
    .end local p2    # "bm":Ljava/nio/ByteBuffer;
    .local v6, "bm":Ljava/nio/ByteBuffer;
    :goto_0
    return-object v6

    .line 222
    .end local v6    # "bm":Ljava/nio/ByteBuffer;
    .restart local v7    # "img_hei":I
    .restart local v8    # "img_wid":I
    .restart local p2    # "bm":Ljava/nio/ByteBuffer;
    :cond_0
    iget v9, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 223
    .local v9, "samplesize":I
    if-nez v9, :cond_1

    .line 224
    const/4 v9, 0x1

    .line 226
    :cond_1
    add-int v0, v8, v9

    add-int/lit8 v0, v0, -0x1

    div-int v8, v0, v9

    .line 227
    add-int v0, v7, v9

    add-int/lit8 v0, v0, -0x1

    div-int v7, v0, v9

    .line 228
    if-eqz p2, :cond_2

    .line 229
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    mul-int v1, v8, v7

    mul-int/lit8 v1, v1, 0x4

    if-ge v0, v1, :cond_2

    .line 230
    const-string v0, "SecMM"

    const-string v1, "SecMMCodec DecodeBytearray Input Bitmap Erraneous\n"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    move-object v6, p2

    .line 231
    .end local p2    # "bm":Ljava/nio/ByteBuffer;
    .restart local v6    # "bm":Ljava/nio/ByteBuffer;
    goto :goto_0

    .line 234
    .end local v6    # "bm":Ljava/nio/ByteBuffer;
    .restart local p2    # "bm":Ljava/nio/ByteBuffer;
    :cond_2
    invoke-static {p0, v10, v11, v9, p2}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeDecodeFileBB(Ljava/lang/String;JILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object p2

    move-object v6, p2

    .line 235
    .end local p2    # "bm":Ljava/nio/ByteBuffer;
    .restart local v6    # "bm":Ljava/nio/ByteBuffer;
    goto :goto_0

    .end local v6    # "bm":Ljava/nio/ByteBuffer;
    .end local v7    # "img_hei":I
    .end local v8    # "img_wid":I
    .end local v9    # "samplesize":I
    .restart local p2    # "bm":Ljava/nio/ByteBuffer;
    :cond_3
    move-object v6, p2

    .line 240
    .end local p2    # "bm":Ljava/nio/ByteBuffer;
    .restart local v6    # "bm":Ljava/nio/ByteBuffer;
    goto :goto_0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;

    .prologue
    .line 247
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "fd"    # Ljava/io/FileDescriptor;
    .param p1, "outPadding"    # Landroid/graphics/Rect;
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 253
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I

    .prologue
    .line 265
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 259
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;
    .param p1, "value"    # Landroid/util/TypedValue;
    .param p2, "is"    # Ljava/io/InputStream;
    .param p3, "pad"    # Landroid/graphics/Rect;
    .param p4, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 271
    invoke-static {p0, p1, p2, p3, p4}, Landroid/graphics/BitmapFactory;->decodeResourceStream(Landroid/content/res/Resources;Landroid/util/TypedValue;Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;

    .prologue
    .line 277
    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "is"    # Ljava/io/InputStream;
    .param p1, "outPadding"    # Landroid/graphics/Rect;
    .param p2, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    .line 283
    invoke-static {p0, p1, p2}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private static doDecode(J[ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 10
    .param p0, "secmmfd"    # J
    .param p2, "tmp"    # [I
    .param p3, "opts"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/4 v8, -0x1

    .line 66
    const/4 v2, 0x0

    .line 67
    .local v2, "mBm":Landroid/graphics/Bitmap;
    const/4 v5, 0x0

    aget v1, p2, v5

    .line 68
    .local v1, "img_wid":I
    const/4 v5, 0x1

    aget v0, p2, v5

    .line 69
    .local v0, "img_hei":I
    const-wide/16 v6, 0x0

    cmp-long v5, p0, v6

    if-nez v5, :cond_0

    move-object v3, v2

    .line 100
    .end local v2    # "mBm":Landroid/graphics/Bitmap;
    .local v3, "mBm":Landroid/graphics/Bitmap;
    :goto_0
    return-object v3

    .line 71
    .end local v3    # "mBm":Landroid/graphics/Bitmap;
    .restart local v2    # "mBm":Landroid/graphics/Bitmap;
    :cond_0
    if-eq v1, v8, :cond_1

    if-eq v0, v8, :cond_1

    .line 72
    iget-boolean v5, p3, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    if-eqz v5, :cond_2

    .line 73
    iput v1, p3, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    .line 74
    iput v0, p3, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    .line 75
    iget-object v2, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_1
    :goto_1
    move-object v3, v2

    .line 100
    .end local v2    # "mBm":Landroid/graphics/Bitmap;
    .restart local v3    # "mBm":Landroid/graphics/Bitmap;
    goto :goto_0

    .line 77
    .end local v3    # "mBm":Landroid/graphics/Bitmap;
    .restart local v2    # "mBm":Landroid/graphics/Bitmap;
    :cond_2
    iget v4, p3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 78
    .local v4, "samplesize":I
    if-nez v4, :cond_3

    .line 79
    const/4 v4, 0x1

    .line 81
    :cond_3
    add-int v5, v1, v4

    add-int/lit8 v5, v5, -0x1

    div-int v1, v5, v4

    .line 82
    add-int v5, v0, v4

    add-int/lit8 v5, v5, -0x1

    div-int v0, v5, v4

    .line 83
    iget-object v5, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v5, :cond_6

    .line 84
    iget-object v2, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    .line 85
    iget-object v5, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    if-ne v5, v1, :cond_4

    iget-object v5, p3, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-eq v5, v0, :cond_5

    .line 86
    :cond_4
    const-string v5, "SecMM"

    const-string v6, "SecMMCodec DecodeBytearray Input Bitmap Erraneous\n"

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    const/4 v2, 0x0

    .line 92
    :cond_5
    :goto_2
    if-eqz v2, :cond_1

    .line 94
    invoke-static {p0, p1, v2}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativelockBitmap(JLandroid/graphics/Bitmap;)I

    .line 95
    invoke-static {p0, p1, v4}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeDecode(JI)I

    .line 96
    invoke-static {v2}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeunlockBitmap(Landroid/graphics/Bitmap;)I

    goto :goto_1

    .line 90
    :cond_6
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->nativeCreateBitmap(II)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_2
.end method

.method public static getInstance()Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;
    .locals 2

    .prologue
    .line 55
    sget-object v0, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->mInstance:Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    if-nez v0, :cond_1

    .line 56
    const-class v1, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    monitor-enter v1

    .line 57
    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->mInstance:Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;-><init>()V

    sput-object v0, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->mInstance:Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    .line 60
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;->mInstance:Lcom/sec/samsung/gallery/decoder/SecMMCodecInterface;

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private static native nativeCreateBitmap(II)Landroid/graphics/Bitmap;
.end method

.method private static native nativeCreateFds()J
.end method

.method private static native nativeDecode(JI)I
.end method

.method private static native nativeDecodeByteArray([BIIJILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeByteArrayBB([BIIJILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
.end method

.method private static native nativeDecodeFile(Ljava/lang/String;JILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
.end method

.method private static native nativeDecodeFileBB(Ljava/lang/String;JILjava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
.end method

.method private static native nativecopybytebuffer(J[BII)I
.end method

.method private static native nativecopyfilename(JLjava/lang/String;)I
.end method

.method private static native nativedecinstance(Ljava/lang/String;[BIII[I)J
.end method

.method private static native nativefreeFds(J)I
.end method

.method private static native nativegetImageinfo(J[I)I
.end method

.method private static native nativelockBitmap(JLandroid/graphics/Bitmap;)I
.end method

.method private static native nativeunlockBitmap(Landroid/graphics/Bitmap;)I
.end method
