.class public Lcom/sec/samsung/gallery/decoder/AnimationInterface;
.super Ljava/lang/Object;
.source "AnimationInterface.java"


# instance fields
.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

.field private mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

.field private mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

.field private mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 41
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseXiv:Z

    if-eqz v0, :cond_1

    .line 43
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getXIVInterface()Lcom/sec/samsung/gallery/decoder/XIVInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 45
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseTileAlphaBlending:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-static {}, Lcom/sec/android/gallery3d/ui/ImageRotation;->getImageRotation()Lcom/sec/android/gallery3d/ui/ImageRotation;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    .line 48
    invoke-static {}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->create()Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    .line 49
    new-instance v0, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    goto :goto_0
.end method


# virtual methods
.method public applyInterpolationCurve(IF)F
    .locals 4
    .param p1, "kind"    # I
    .param p2, "progress"    # F

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v1, v3, v2, p2, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getProgress(FFFI)F

    move-result v1

    .line 197
    :goto_0
    return v1

    .line 175
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    if-eqz v1, :cond_1

    const/4 v1, 0x3

    if-ne p1, v1, :cond_1

    .line 176
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    invoke-virtual {v1, v3, v2, p2}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->getProgress(FFF)F

    move-result v1

    goto :goto_0

    .line 178
    :cond_1
    sub-float v0, v2, p2

    .line 180
    .local v0, "f":F
    sub-float p2, v2, v0

    move v1, p2

    .line 197
    goto :goto_0
.end method

.method public centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V
    .locals 12
    .param p1, "tiv"    # Lcom/sec/android/gallery3d/ui/TileImageView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "r"    # Landroid/graphics/Rect;
    .param p4, "level"    # I
    .param p5, "size"    # I
    .param p6, "mScale"    # F
    .param p7, "offsetX"    # I
    .param p8, "offsetY"    # I
    .param p9, "mScreenNail"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p10, "mImageWidth"    # I
    .param p11, "mImageHeight"    # I

    .prologue
    .line 309
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move/from16 v11, p11

    invoke-virtual/range {v0 .. v11}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->centerDrawTiles(Lcom/sec/android/gallery3d/ui/TileImageView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Landroid/graphics/Rect;IIFIILcom/sec/android/gallery3d/ui/ScreenNail;II)V

    .line 312
    :cond_0
    return-void
.end method

.method public checkAnimation(Z)V
    .locals 1
    .param p1, "isMoreAnim"    # Z

    .prologue
    .line 142
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->checkAnimation(Z)V

    .line 145
    :cond_0
    return-void
.end method

.method public checkSliding()V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->checkSliding()V

    .line 287
    :cond_0
    return-void
.end method

.method public drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z
    .locals 9
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "realOffsetX"    # I
    .param p3, "realOffsetY"    # I
    .param p4, "imageWidth"    # I
    .param p5, "imageHeight"    # I
    .param p6, "scale"    # F
    .param p7, "backUp"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p8, "level"    # I

    .prologue
    .line 315
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawOnCenter(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIIIFLcom/sec/android/gallery3d/ui/ScreenNail;I)Z

    move-result v0

    .line 318
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V
    .locals 10
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "isNext"    # Z
    .param p3, "backup"    # Lcom/sec/android/gallery3d/ui/ScreenNail;
    .param p4, "cx"    # I
    .param p5, "cy"    # I
    .param p6, "drawW"    # I
    .param p7, "drawH"    # I
    .param p8, "rotation"    # I
    .param p9, "diff"    # I

    .prologue
    .line 347
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-virtual/range {v0 .. v9}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawOnScreenNail(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;ZLcom/sec/android/gallery3d/ui/ScreenNail;IIIIII)V

    .line 350
    :cond_0
    return-void
.end method

.method public drawTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)Z
    .locals 6
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "level"    # I
    .param p3, "size"    # I
    .param p4, "mRealOffsetX"    # I
    .param p5, "mRealOffsetY"    # I

    .prologue
    .line 302
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->drawTiles(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;IIII)Z

    move-result v0

    .line 305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAlphablendingAnimationInterface()Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;
    .locals 1

    .prologue
    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    .line 376
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getAnimationDuration(II)I
    .locals 1
    .param p1, "kind"    # I
    .param p2, "time"    # I

    .prologue
    .line 158
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 159
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getAnimationDuration(II)I

    move-result p2

    .line 168
    .end local p2    # "time":I
    :cond_0
    :goto_0
    return p2

    .line 160
    .restart local p2    # "time":I
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    if-eqz v0, :cond_0

    .line 161
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 163
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;->getSlideAnimationTime(I)I

    move-result p2

    goto :goto_0

    .line 161
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public getCurrentPhotoIndex()I
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->getCurrentPhotoIndex()I

    move-result v0

    .line 256
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initAlphaBlendingAnimation()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->init()V

    .line 80
    :cond_0
    return-void
.end method

.method public isAnimating()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isAnimating()Z

    move-result v0

    .line 90
    :goto_0
    return v0

    .line 85
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-eqz v0, :cond_1

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ImageRotation;->isAnimationStarted()Z

    move-result v0

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isImageChanging()Z
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->isImageChanging()Z

    move-result v0

    .line 342
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLevelChanged(I)Z
    .locals 1
    .param p1, "currLevel"    # I

    .prologue
    .line 353
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->isLevelChanged(I)Z

    move-result v0

    .line 356
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPanorama()Z
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->isPanorama()Z

    move-result v0

    .line 363
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isScrolling()Z
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->isScrolling()Z

    move-result v0

    .line 334
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isUseAnimation(I)Z
    .locals 2
    .param p1, "animKind"    # I

    .prologue
    const/4 v0, 0x0

    .line 148
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v1, :cond_1

    .line 149
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->isUseXIVAnimation(I)Z

    move-result v0

    .line 153
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mSlidingInterface:Lcom/sec/samsung/gallery/decoder/SlidingAnimationInterface;

    if-eqz v1, :cond_0

    .line 151
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isZoomState(I)Z
    .locals 1
    .param p1, "level"    # I

    .prologue
    .line 323
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->isZoomState(I)Z

    move-result v0

    .line 326
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveTo(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 261
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->moveTo(I)V

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 262
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->moveTo(I)V

    goto :goto_0
.end method

.method public onDoubleTapZoomBegin()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onDoubleTapZoomBegin()V

    .line 226
    :cond_0
    return-void
.end method

.method public onDoubleTapZoomEnd()V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onDoubleTapZoomEnd()V

    .line 232
    :cond_0
    return-void
.end method

.method public onScale(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onScale(F)V

    .line 238
    :cond_0
    return-void
.end method

.method public onScaleBegin(F)V
    .locals 1
    .param p1, "scale"    # F

    .prologue
    .line 241
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onScaleBegin(F)V

    .line 244
    :cond_0
    return-void
.end method

.method public onScaleEnd()V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->onScaleEnd()V

    .line 250
    :cond_0
    return-void
.end method

.method public postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V
    .locals 1
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p3, "isActive"    # Z

    .prologue
    .line 213
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 214
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->postRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-virtual {v0, p1, p2, p3}, Lcom/sec/android/gallery3d/ui/ImageRotation;->endRotation(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)V

    goto :goto_0
.end method

.method public preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z
    .locals 1
    .param p1, "photoView"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 202
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1, p2}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->preRender(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v0

    .line 209
    :goto_0
    return v0

    .line 204
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-eqz v0, :cond_1

    .line 206
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/ImageRotation;->startRotation(Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)Z

    move-result v0

    goto :goto_0

    .line 209
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public prepareAlphablendingAnimation()V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->prepareAlphablendingAnimation()V

    .line 370
    :cond_0
    return-void
.end method

.method public prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLjava/lang/Object;)Z
    .locals 6
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;
    .param p2, "isCW"    # Z
    .param p3, "cb"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 55
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v5, :cond_1

    .line 56
    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v4, p1, p2, p3}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->prepareAnimation(Lcom/sec/android/gallery3d/ui/PhotoView;ZLjava/lang/Object;)V

    .line 73
    :cond_0
    :goto_0
    return v3

    .line 58
    :cond_1
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-eqz v5, :cond_3

    .line 60
    const/4 v0, 0x0

    .line 61
    .local v0, "imageBounds":Landroid/graphics/Rect;
    if-eqz p1, :cond_2

    .line 62
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v1

    .line 63
    .local v1, "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    if-eqz v1, :cond_2

    .line 64
    invoke-virtual {v1, v4}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v0

    .line 66
    .end local v1    # "positionController":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_2
    if-eqz v0, :cond_0

    .line 67
    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v5, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v5

    int-to-float v2, v4

    .line 68
    .local v2, "width":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-virtual {v4, v2, p2}, Lcom/sec/android/gallery3d/ui/ImageRotation;->init(FZ)V

    goto :goto_0

    .end local v0    # "imageBounds":Landroid/graphics/Rect;
    .end local v2    # "width":F
    :cond_3
    move v3, v4

    .line 73
    goto :goto_0
.end method

.method public setImageChanging(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 290
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->setImageChanging(Z)V

    .line 293
    :cond_0
    return-void
.end method

.method public setMultiViewState(Z)V
    .locals 1
    .param p1, "state"    # Z

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setMultiViewState(Z)V

    .line 139
    :cond_0
    return-void
.end method

.method public setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    goto :goto_0
.end method

.method public setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V
    .locals 1
    .param p1, "view"    # Lcom/sec/android/gallery3d/ui/PhotoView;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    goto :goto_0
.end method

.method public setScrolling(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->setScrolling(Z)V

    .line 299
    :cond_0
    return-void
.end method

.method public startAnimation()V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->startAnimation()V

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mImageRotation:Lcom/sec/android/gallery3d/ui/ImageRotation;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/ImageRotation;->prepare()V

    goto :goto_0
.end method

.method public switchToNext()V
    .locals 1

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->switchToNext()V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->switchToNext()V

    goto :goto_0
.end method

.method public switchToPrev()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->switchToPrev()V

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->switchToPrev()V

    goto :goto_0
.end method

.method public unsetPhotoDataAdapter()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->unsetPhotoDataAdapter()V

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->unsetPhotoDataAdapter()V

    goto :goto_0
.end method

.method public unsetPhotoView()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mXIVInterface:Lcom/sec/samsung/gallery/decoder/XIVInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/XIVInterface;->unsetPhotoView()V

    .line 125
    :cond_0
    :goto_0
    return-void

    .line 122
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->mAlphablendingAnimationInterface:Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AlphablendingAnimationInterface;->unsetPhotoView()V

    goto :goto_0
.end method
