.class Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;
.super Ljava/lang/Object;
.source "HelpAirCommandState.java"

# interfaces
.implements Lcom/samsung/android/airbutton/AirButtonImpl$OnStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->setAirButtonListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method public onHide()V
    .locals 0

    .prologue
    .line 73
    return-void
.end method

.method public onShow()V
    .locals 8

    .prologue
    const-wide/16 v6, 0xbb8

    .line 53
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    const v3, 0x7f0f013c

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object v0, v2

    check-cast v0, Landroid/widget/TextView;

    .line 55
    .local v0, "completePopup":Landroid/widget/TextView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->access$100(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f040002

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 57
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 61
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->access$200(Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    const v3, 0x7f0e02dc

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 62
    .local v1, "text":Ljava/lang/String;
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->mHelpPopup:Landroid/widget/RelativeLayout;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 68
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState$1;->this$0:Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;

    invoke-virtual {v2, v6, v7, v6, v7}, Lcom/sec/samsung/gallery/view/help/HelpAirCommandState;->startFinishActivity(JJ)V

    .line 69
    return-void
.end method
