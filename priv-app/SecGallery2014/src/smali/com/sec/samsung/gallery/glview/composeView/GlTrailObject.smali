.class public Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
.super Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;
.source "GlTrailObject.java"


# instance fields
.field private mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

.field mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

.field mSegPre:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1
    .param p1, "compView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 16
    iget-object v0, p1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;-><init>(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 17
    return-void
.end method

.method public static linkTrail(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "objs":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;>;"
    if-nez p0, :cond_1

    .line 28
    :cond_0
    return-void

    .line 22
    :cond_1
    const/4 v1, 0x1

    .local v1, "i":I
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 23
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 24
    .local v0, "cur":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 25
    .local v3, "pre":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    if-eqz v3, :cond_2

    iput-object v0, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 26
    :cond_2
    if-eqz v0, :cond_3

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegPre:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 22
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static linkTrail([Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;)V
    .locals 5
    .param p0, "objs"    # [Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .prologue
    .line 31
    if-nez p0, :cond_1

    .line 39
    :cond_0
    return-void

    .line 33
    :cond_1
    const/4 v1, 0x1

    .local v1, "i":I
    array-length v2, p0

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 34
    aget-object v0, p0, v1

    .line 35
    .local v0, "cur":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    add-int/lit8 v4, v1, -0x1

    aget-object v3, p0, v4

    .line 36
    .local v3, "pre":Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;
    if-eqz v3, :cond_2

    iput-object v0, v3, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 37
    :cond_2
    if-eqz v0, :cond_3

    iput-object v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegPre:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    .line 33
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getTrailLatency()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v0, v0

    goto :goto_0
.end method

.method public setTrailLatency(I)V
    .locals 5
    .param p1, "latency"    # I

    .prologue
    .line 42
    if-gez p1, :cond_1

    .line 48
    :cond_0
    return-void

    .line 44
    :cond_1
    new-array v1, p1, [Lcom/sec/android/gallery3d/glcore/GlPos2D;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    .line 45
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 46
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getX()F

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getY()F

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlPos2D;-><init>(FF)V

    aput-object v2, v1, v0

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateInitPosition()V
    .locals 3

    .prologue
    .line 55
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-nez v1, :cond_1

    .line 63
    :cond_0
    return-void

    .line 59
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getX()F

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v1, v1, v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getY()F

    move-result v2

    iput v2, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public updateTrail(FF)V
    .locals 8
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 66
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v2, v2

    if-nez v2, :cond_3

    .line 67
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v2, :cond_1

    .line 68
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getX()F

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->updateTrail(FF)V

    .line 70
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegPre:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v2, :cond_2

    .line 71
    invoke-virtual {p0, p1, p2, v5, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(FFFI)V

    .line 88
    :cond_2
    :goto_0
    return-void

    .line 73
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v2, :cond_4

    .line 74
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v2, v3

    .line 75
    .local v1, "lastPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegNext:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    iget v3, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    iget v4, v1, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    invoke-virtual {v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->updateTrail(FF)V

    .line 78
    .end local v1    # "lastPos":Lcom/sec/android/gallery3d/glcore/GlPos2D;
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    array-length v2, v2

    add-int/lit8 v0, v2, -0x1

    .local v0, "i":I
    :goto_1
    if-lez v0, :cond_5

    .line 79
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 80
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    iput v3, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 78
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 83
    :cond_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v6

    iput p1, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mX:F

    .line 84
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mLatency:[Lcom/sec/android/gallery3d/glcore/GlPos2D;

    aget-object v2, v2, v6

    iput p2, v2, Lcom/sec/android/gallery3d/glcore/GlPos2D;->mY:F

    .line 85
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->mSegPre:Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;

    if-eqz v2, :cond_2

    .line 86
    invoke-virtual {p0, p1, p2, v5, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlTrailObject;->setPos(FFFI)V

    goto :goto_0
.end method
