.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->genericMotionCancel()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$2000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;

    move-result-object v0

    const/4 v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlGridView;->setFocusIndex(IZ)V

    .line 309
    return-void
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p2, "position"    # I

    .prologue
    .line 299
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;->setFocusOnText(Z)V

    .line 300
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->genericMotionEnter(I)V
    invoke-static {v0, p2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$1900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;I)V

    .line 301
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mGridView:Lcom/sec/samsung/gallery/glview/GlGridView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$900(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/glview/GlGridView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->setFocusIndex(IZ)V

    .line 302
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mHiddenAlbumAdapter:Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$200(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/adapter/EditHiddenMediaAdapter;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$10;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$300(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v2

    invoke-virtual {v0, v1, p2, v2}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/Object;ILcom/sec/android/gallery3d/ui/SelectionManager;)V

    .line 303
    return-void
.end method
