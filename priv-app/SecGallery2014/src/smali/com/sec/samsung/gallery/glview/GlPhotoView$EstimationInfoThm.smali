.class Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;
.super Ljava/lang/Object;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EstimationInfoThm"
.end annotation


# instance fields
.field firstX:F

.field firstY:F

.field firstZ:F

.field gapH:F

.field gapW:F

.field hCount:I

.field objH:F

.field objW:F

.field scroll:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 933
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$1;

    .prologue
    .line 933
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    return-void
.end method


# virtual methods
.method public getObject(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 7
    .param p1, "index"    # I

    .prologue
    .line 944
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->objW:F

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->objH:F

    invoke-direct {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 946
    .local v1, "glObject":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->hCount:I

    rem-int v0, p1, v3

    .line 947
    .local v0, "col":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->hCount:I

    div-int v2, p1, v3

    .line 948
    .local v2, "row":I
    int-to-float v3, v0

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->gapW:F

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstX:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstY:F

    int-to-float v5, v2

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->gapH:F

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->scroll:F

    add-float/2addr v4, v5

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstZ:F

    invoke-virtual {v1, v3, v4, v5}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 949
    return-object v1
.end method

.method public saveState()V
    .locals 1

    .prologue
    .line 953
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->objW:F

    .line 954
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->objH:F

    .line 955
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapW:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->gapW:F

    .line 956
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->gapH:F

    .line 957
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartX:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstX:F

    .line 958
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartY:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstY:F

    .line 959
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemStartZ:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->firstZ:F

    .line 960
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mItemHVisibleCnt:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->hCount:I

    .line 961
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThumbCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemScroller;->mScroll:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$EstimationInfoThm;->scroll:F

    .line 962
    return-void
.end method
