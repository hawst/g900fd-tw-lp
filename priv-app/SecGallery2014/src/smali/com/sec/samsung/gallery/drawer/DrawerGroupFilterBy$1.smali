.class Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;
.super Landroid/os/Handler;
.source "DrawerGroupFilterBy.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;Landroid/os/Looper;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Looper;

    .prologue
    .line 76
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 79
    iget v0, p1, Landroid/os/Message;->what:I

    .line 80
    .local v0, "what":I
    packed-switch v0, :pswitch_data_0

    .line 96
    :goto_0
    return-void

    .line 82
    :pswitch_0
    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MSG_NOTIFY_DATA_SET_CHANGED"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 86
    :pswitch_1
    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "MSG_NOTIFY_DATA_SET_CHANGED_ADD_FILTERBY"

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    # invokes: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->isContainViewFilterTypeOfLocal()Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$100(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->addFilterBy()V

    .line 93
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    iget-object v1, v1, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/drawer/IDrawerCallback;->onUpdate()V

    goto :goto_0

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy$1;->this$0:Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;

    # getter for: Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->mFilterbyGroup:Lcom/sec/samsung/gallery/drawer/Group;
    invoke-static {v1}, Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;->access$200(Lcom/sec/samsung/gallery/drawer/DrawerGroupFilterBy;)Lcom/sec/samsung/gallery/drawer/Group;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/Group;->removeChildren()V

    goto :goto_1

    .line 80
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
