.class final Lcom/sec/samsung/gallery/util/DownloadUtil$1;
.super Landroid/content/BroadcastReceiver;
.source "DownloadUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/util/DownloadUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 63
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.DOWNLOAD_COMPLETE"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 65
    const-string v4, "DownloadUtil"

    const-string v5, "Notification DOWNLOAD_COMPLETE"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const-string v4, "extra_download_id"

    const-wide/16 v6, -0x1

    invoke-virtual {p2, v4, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 68
    .local v2, "downloadedId":J
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mIDArray:Ljava/util/ArrayList;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$000()Ljava/util/ArrayList;

    move-result-object v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 69
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/util/DownloadUtil;->checkDownloadStatus(Landroid/content/Context;J)V

    .line 71
    :cond_0
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->smIsReceiverRegistered:Z
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$100()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 72
    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mHandlerDownloadInfo:Landroid/os/Handler;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$300()Landroid/os/Handler;

    move-result-object v4

    # getter for: Lcom/sec/samsung/gallery/util/DownloadUtil;->mRunnableDownloadCheck:Ljava/lang/Runnable;
    invoke-static {}, Lcom/sec/samsung/gallery/util/DownloadUtil;->access$200()Ljava/lang/Runnable;

    move-result-object v5

    const-wide/16 v6, 0x1388

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    .end local v2    # "downloadedId":J
    :cond_1
    :goto_0
    return-void

    .line 73
    :cond_2
    const-string v4, "android.intent.action.DOWNLOAD_NOTIFICATION_CLICKED"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 74
    const-string v4, "DownloadUtil"

    const-string v5, "Notification clicked"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    :try_start_0
    new-instance v4, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW_DOWNLOADS"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x10000000

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v1

    .line 79
    .local v1, "e":Landroid/content/ActivityNotFoundException;
    const-string v4, "DownloadUtil"

    const-string v5, "Activity Not Found"

    invoke-static {v4, v5}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {v1}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method
