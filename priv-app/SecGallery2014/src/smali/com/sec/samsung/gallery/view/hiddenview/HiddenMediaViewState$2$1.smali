.class Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;
.super Ljava/lang/Object;
.source "HiddenMediaViewState.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->handleMediaList(Ljava/util/Set;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;->this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;->this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$400(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissDialogs()V

    .line 137
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;->this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # getter for: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$500(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v0

    sget-object v1, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_MULTIPLE_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;->this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitViewState()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$600(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    .line 142
    :goto_0
    return-void

    .line 140
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2$1;->this$1:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState$2;->this$0:Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;

    # invokes: Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->exitSelectionMode()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;->access$000(Lcom/sec/samsung/gallery/view/hiddenview/HiddenMediaViewState;)V

    goto :goto_0
.end method
