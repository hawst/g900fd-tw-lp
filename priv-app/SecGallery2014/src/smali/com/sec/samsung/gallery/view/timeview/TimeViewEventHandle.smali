.class Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;
.super Ljava/lang/Object;
.source "TimeViewEventHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$1;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;,
        Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;
    }
.end annotation


# instance fields
.field private mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private final mTimeViewMultiPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;

.field private final mTimeViewPersonPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;

.field private final mTimeViewPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;

.field private final mTimeViewState:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;


# direct methods
.method constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/view/timeview/TimeViewState;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "albumViewState"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .prologue
    const/4 v1, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 30
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewState:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;

    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewMultiPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;

    .line 33
    new-instance v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;-><init>(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewPersonPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewState:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getActionBarManager()Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewState:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 36
    return-void
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;)Lcom/sec/samsung/gallery/view/timeview/TimeViewState;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewState:Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;)Lcom/sec/android/gallery3d/ui/SelectionManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    return-object v0
.end method


# virtual methods
.method disableView()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;->disableView()V

    .line 60
    return-void
.end method

.method initializeView()V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;->initializeView()V

    .line 56
    return-void
.end method

.method onBackPressed()V
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;->onBackPressed()V

    .line 68
    return-void
.end method

.method onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)V
    .locals 1
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "view"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "position"    # I
    .param p4, "subPosition"    # I

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)V

    .line 64
    return-void
.end method

.method setMode(Lcom/sec/samsung/gallery/core/LaunchModeType;)V
    .locals 2
    .param p1, "mode"    # Lcom/sec/samsung/gallery/core/LaunchModeType;

    .prologue
    .line 39
    sget-object v0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$1;->$SwitchMap$com$sec$samsung$gallery$core$LaunchModeType:[I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/core/LaunchModeType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 52
    :goto_0
    return-void

    .line 41
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    goto :goto_0

    .line 44
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewMultiPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewMultiPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    goto :goto_0

    .line 47
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mTimeViewPersonPick:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$TimeViewPersonPick;

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle;->mMode:Lcom/sec/samsung/gallery/view/timeview/TimeViewEventHandle$ITimeViewMode;

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
