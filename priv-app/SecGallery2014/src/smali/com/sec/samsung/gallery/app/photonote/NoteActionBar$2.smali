.class Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;
.super Ljava/lang/Object;
.source "NoteActionBar.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 170
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 171
    .local v0, "buttonId":I
    const v1, 0x7f0f014d

    if-eq v0, v1, :cond_0

    const v1, 0x7f0f0148

    if-ne v0, v1, :cond_2

    .line 172
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->finish_note(Z)V

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 173
    :cond_2
    const v1, 0x7f0f014e

    if-eq v0, v1, :cond_3

    const v1, 0x7f0f0146

    if-ne v0, v1, :cond_1

    .line 174
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar$2;->this$0:Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;

    # getter for: Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->mView:Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;->access$000(Lcom/sec/samsung/gallery/app/photonote/NoteActionBar;)Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/app/photonote/NoteSpenSurfaceView;->onSave()V

    goto :goto_0
.end method
