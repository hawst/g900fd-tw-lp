.class public Lcom/sec/samsung/gallery/hlistview/widget/HListView;
.super Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.source "HListView.java"


# annotations
.annotation runtime Landroid/widget/RemoteViews$RemoteView;
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/hlistview/widget/HListView$1;,
        Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;,
        Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;,
        Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    }
.end annotation


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "HListView"

.field private static final MAX_SCROLL_FACTOR:F = 0.33f

.field private static final MIN_SCROLL_PREVIEW_PIXELS:I = 0x2

.field static final NO_POSITION:I = -0x1


# instance fields
.field private mAreAllItemsSelectable:Z

.field private final mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

.field mDivider:Landroid/graphics/drawable/Drawable;

.field private mDividerIsOpaque:Z

.field private mDividerPaint:Landroid/graphics/Paint;

.field mDividerWidth:I

.field private mFocusSelector:Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

.field private mFooterDividersEnabled:Z

.field private mFooterViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mHeaderDividersEnabled:Z

.field private mHeaderViewInfos:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCacheColorOpaque:Z

.field private mItemsCanFocus:Z

.field mMeasureWithChild:I

.field mOverScrollFooter:Landroid/graphics/drawable/Drawable;

.field mOverScrollHeader:Landroid/graphics/drawable/Drawable;

.field private final mTempRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 148
    const v0, 0x7f010010

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 152
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 112
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    .line 113
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iput-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    .line 128
    iput-boolean v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAreAllItemsSelectable:Z

    .line 130
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    .line 133
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    iput-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    .line 138
    new-instance v6, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    const/4 v7, 0x0

    invoke-direct {v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/HListView$1;)V

    iput-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    .line 154
    sget-object v6, Lcom/sec/android/gallery3d/R$styleable;->HListView:[I

    invoke-virtual {p1, p2, v6, p3, v8}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 156
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v8}, Landroid/content/res/TypedArray;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v3

    .line 157
    .local v3, "entries":[Ljava/lang/CharSequence;
    if-eqz v3, :cond_0

    .line 158
    new-instance v6, Landroid/widget/ArrayAdapter;

    const v7, 0x1090003

    invoke-direct {v6, p1, v7, v3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 161
    :cond_0
    invoke-virtual {v0, v9}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 162
    .local v1, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v1, :cond_1

    .line 164
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 167
    :cond_1
    const/4 v6, 0x5

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    .line 168
    .local v5, "osHeader":Landroid/graphics/drawable/Drawable;
    if-eqz v5, :cond_2

    .line 169
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V

    .line 172
    :cond_2
    const/4 v6, 0x6

    invoke-virtual {v0, v6}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 173
    .local v4, "osFooter":Landroid/graphics/drawable/Drawable;
    if-eqz v4, :cond_3

    .line 174
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V

    .line 178
    :cond_3
    const/4 v6, 0x2

    invoke-virtual {v0, v6, v8}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    .line 179
    .local v2, "dividerWidth":I
    if-eqz v2, :cond_4

    .line 180
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setDividerWidth(I)V

    .line 183
    :cond_4
    const/4 v6, 0x3

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderDividersEnabled:Z

    .line 184
    const/4 v6, 0x4

    invoke-virtual {v0, v6, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    iput-boolean v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterDividersEnabled:Z

    .line 185
    const/4 v6, 0x7

    const/4 v7, -0x1

    invoke-virtual {v0, v6, v7}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v6

    iput v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMeasureWithChild:I

    .line 187
    const-string v6, "HListView"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mMeasureWithChild: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMeasureWithChild:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 190
    return-void
.end method

.method private addViewAfter(Landroid/view/View;I)Landroid/view/View;
    .locals 8
    .param p1, "theView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x0

    .line 3056
    add-int/lit8 v2, p2, 0x1

    .line 3057
    .local v2, "belowPosition":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    invoke-virtual {p0, v2, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v1

    .line 3058
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v0

    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    add-int v3, v0, v4

    .line 3059
    .local v3, "edgeOfNewChild":I
    const/4 v4, 0x1

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    aget-boolean v7, v0, v6

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setupChild(Landroid/view/View;IIZIZZ)V

    .line 3060
    return-object v1
.end method

.method private addViewBefore(Landroid/view/View;I)Landroid/view/View;
    .locals 8
    .param p1, "theView"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 3048
    add-int/lit8 v2, p2, -0x1

    .line 3049
    .local v2, "abovePosition":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    invoke-virtual {p0, v2, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v1

    .line 3050
    .local v1, "view":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    sub-int v3, v0, v5

    .line 3051
    .local v3, "edgeOfNewChild":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    aget-boolean v7, v0, v4

    move-object v0, p0

    move v6, v4

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setupChild(Landroid/view/View;IIZIZZ)V

    .line 3052
    return-object v1
.end method

.method private adjustViewsLeftOrRight()V
    .locals 6

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v1

    .line 206
    .local v1, "childCount":I
    if-lez v1, :cond_2

    .line 209
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v3, :cond_3

    .line 212
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 213
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int v2, v3, v4

    .line 214
    .local v2, "delta":I
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-eqz v3, :cond_0

    .line 217
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    sub-int/2addr v2, v3

    .line 219
    :cond_0
    if-gez v2, :cond_1

    .line 221
    const/4 v2, 0x0

    .line 239
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 240
    neg-int v3, v2

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 243
    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "delta":I
    :cond_2
    return-void

    .line 225
    :cond_3
    add-int/lit8 v3, v1, -0x1

    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 226
    .restart local v0    # "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v5

    sub-int v2, v3, v4

    .line 228
    .restart local v2    # "delta":I
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v3, v1

    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-ge v3, v4, :cond_4

    .line 231
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    add-int/2addr v2, v3

    .line 234
    :cond_4
    if-lez v2, :cond_1

    .line 235
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private amountToScroll(II)I
    .locals 12
    .param p1, "direction"    # I
    .param p2, "nextSelectedPosition"    # I

    .prologue
    .line 2662
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v10

    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    sub-int v5, v10, v11

    .line 2663
    .local v5, "listRight":I
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v10, Landroid/graphics/Rect;->left:I

    .line 2665
    .local v4, "listLeft":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    .line 2667
    .local v7, "numChildren":I
    const/16 v10, 0x82

    if-ne p1, v10, :cond_5

    .line 2668
    add-int/lit8 v3, v7, -0x1

    .line 2669
    .local v3, "indexToMakeVisible":I
    const/4 v10, -0x1

    if-eq p2, v10, :cond_0

    .line 2670
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    sub-int v3, p2, v10

    .line 2673
    :cond_0
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int v8, v10, v3

    .line 2674
    .local v8, "positionToMakeVisible":I
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 2676
    .local v9, "viewToMakeVisible":Landroid/view/View;
    move v2, v5

    .line 2677
    .local v2, "goalRight":I
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v10, v10, -0x1

    if-ge v8, v10, :cond_1

    .line 2678
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v10

    sub-int/2addr v2, v10

    .line 2681
    :cond_1
    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v10

    if-gt v10, v2, :cond_2

    .line 2683
    const/4 v10, 0x0

    .line 2729
    .end local v2    # "goalRight":I
    :goto_0
    return v10

    .line 2686
    .restart local v2    # "goalRight":I
    :cond_2
    const/4 v10, -0x1

    if-eq p2, v10, :cond_3

    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int v10, v2, v10

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getMaxScrollAmount()I

    move-result v11

    if-lt v10, v11, :cond_3

    .line 2689
    const/4 v10, 0x0

    goto :goto_0

    .line 2692
    :cond_3
    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v10

    sub-int v0, v10, v2

    .line 2694
    .local v0, "amountToScroll":I
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v10, v7

    iget v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-ne v10, v11, :cond_4

    .line 2696
    add-int/lit8 v10, v7, -0x1

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v10

    sub-int v6, v10, v5

    .line 2697
    .local v6, "max":I
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2700
    .end local v6    # "max":I
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getMaxScrollAmount()I

    move-result v10

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    goto :goto_0

    .line 2702
    .end local v0    # "amountToScroll":I
    .end local v2    # "goalRight":I
    .end local v3    # "indexToMakeVisible":I
    .end local v8    # "positionToMakeVisible":I
    .end local v9    # "viewToMakeVisible":Landroid/view/View;
    :cond_5
    const/4 v3, 0x0

    .line 2703
    .restart local v3    # "indexToMakeVisible":I
    const/4 v10, -0x1

    if-eq p2, v10, :cond_6

    .line 2704
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    sub-int v3, p2, v10

    .line 2706
    :cond_6
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int v8, v10, v3

    .line 2707
    .restart local v8    # "positionToMakeVisible":I
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 2708
    .restart local v9    # "viewToMakeVisible":Landroid/view/View;
    move v1, v4

    .line 2709
    .local v1, "goalLeft":I
    if-lez v8, :cond_7

    .line 2710
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v10

    add-int/2addr v1, v10

    .line 2712
    :cond_7
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v10

    if-lt v10, v1, :cond_8

    .line 2714
    const/4 v10, 0x0

    goto :goto_0

    .line 2717
    :cond_8
    const/4 v10, -0x1

    if-eq p2, v10, :cond_9

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v10

    sub-int/2addr v10, v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getMaxScrollAmount()I

    move-result v11

    if-lt v10, v11, :cond_9

    .line 2720
    const/4 v10, 0x0

    goto :goto_0

    .line 2723
    :cond_9
    invoke-virtual {v9}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int v0, v1, v10

    .line 2724
    .restart local v0    # "amountToScroll":I
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-nez v10, :cond_a

    .line 2726
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v10

    sub-int v6, v4, v10

    .line 2727
    .restart local v6    # "max":I
    invoke-static {v0, v6}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2729
    .end local v6    # "max":I
    :cond_a
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getMaxScrollAmount()I

    move-result v10

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v10

    goto/16 :goto_0
.end method

.method private amountToScrollToNewFocus(ILandroid/view/View;I)I
    .locals 4
    .param p1, "direction"    # I
    .param p2, "newFocus"    # Landroid/view/View;
    .param p3, "positionOfNewFocus"    # I

    .prologue
    .line 2923
    const/4 v0, 0x0

    .line 2924
    .local v0, "amountToScroll":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2925
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2926
    const/16 v2, 0x21

    if-ne p1, v2, :cond_1

    .line 2927
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_0

    .line 2928
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v3

    .line 2929
    if-lez p3, :cond_0

    .line 2930
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v2

    add-int/2addr v0, v2

    .line 2942
    :cond_0
    :goto_0
    return v0

    .line 2934
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v1, v2, v3

    .line 2935
    .local v1, "listRight":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    if-le v2, v1, :cond_0

    .line 2936
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v1

    .line 2937
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    if-ge p3, v2, :cond_0

    .line 2938
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0
.end method

.method private arrowScrollFocused(I)Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;
    .locals 17
    .param p1, "direction"    # I

    .prologue
    .line 2818
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getSelectedView()Landroid/view/View;

    move-result-object v12

    .line 2820
    .local v12, "selectedView":Landroid/view/View;
    if-eqz v12, :cond_2

    invoke-virtual {v12}, Landroid/view/View;->hasFocus()Z

    move-result v14

    if-eqz v14, :cond_2

    .line 2821
    invoke-virtual {v12}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v8

    .line 2822
    .local v8, "oldFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v14

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v14, v0, v8, v1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v7

    .line 2847
    .end local v8    # "oldFocus":Landroid/view/View;
    .local v7, "newFocus":Landroid/view/View;
    :goto_0
    if-eqz v7, :cond_c

    .line 2848
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionOfNewFocus(Landroid/view/View;)I

    move-result v9

    .line 2852
    .local v9, "positionOfNewFocus":I
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    const/4 v15, -0x1

    if-eq v14, v15, :cond_a

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-eq v9, v14, :cond_a

    .line 2853
    invoke-direct/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePositionOnScreen(I)I

    move-result v11

    .line 2854
    .local v11, "selectablePosition":I
    const/4 v14, -0x1

    if-eq v11, v14, :cond_a

    const/16 v14, 0x82

    move/from16 v0, p1

    if-ne v0, v14, :cond_0

    if-lt v11, v9, :cond_1

    :cond_0
    const/16 v14, 0x21

    move/from16 v0, p1

    if-ne v0, v14, :cond_a

    if-le v11, v9, :cond_a

    .line 2857
    :cond_1
    const/4 v14, 0x0

    .line 2879
    .end local v9    # "positionOfNewFocus":I
    .end local v11    # "selectablePosition":I
    :goto_1
    return-object v14

    .line 2824
    .end local v7    # "newFocus":Landroid/view/View;
    :cond_2
    const/16 v14, 0x82

    move/from16 v0, p1

    if-ne v0, v14, :cond_6

    .line 2825
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-lez v14, :cond_3

    const/4 v3, 0x1

    .line 2826
    .local v3, "leftFadingEdgeShowing":Z
    :goto_2
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v15, v14, Landroid/graphics/Rect;->left:I

    if-eqz v3, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v14

    :goto_3
    add-int v4, v15, v14

    .line 2828
    .local v4, "listLeft":I
    if-eqz v12, :cond_5

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v14

    if-le v14, v4, :cond_5

    invoke-virtual {v12}, Landroid/view/View;->getLeft()I

    move-result v13

    .line 2832
    .local v13, "xSearchPoint":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v13, v15, v13, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 2844
    .end local v3    # "leftFadingEdgeShowing":Z
    .end local v4    # "listLeft":I
    :goto_5
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual {v14, v0, v15, v1}, Landroid/view/FocusFinder;->findNextFocusFromRect(Landroid/view/ViewGroup;Landroid/graphics/Rect;I)Landroid/view/View;

    move-result-object v7

    .restart local v7    # "newFocus":Landroid/view/View;
    goto :goto_0

    .line 2825
    .end local v7    # "newFocus":Landroid/view/View;
    .end local v13    # "xSearchPoint":I
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 2826
    .restart local v3    # "leftFadingEdgeShowing":Z
    :cond_4
    const/4 v14, 0x0

    goto :goto_3

    .restart local v4    # "listLeft":I
    :cond_5
    move v13, v4

    .line 2828
    goto :goto_4

    .line 2834
    .end local v3    # "leftFadingEdgeShowing":Z
    .end local v4    # "listLeft":I
    :cond_6
    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v15

    add-int/2addr v14, v15

    add-int/lit8 v14, v14, -0x1

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-ge v14, v15, :cond_7

    const/4 v10, 0x1

    .line 2836
    .local v10, "rightFadingEdgeShowing":Z
    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->right:I

    sub-int v15, v14, v15

    if-eqz v10, :cond_8

    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getArrowScrollPreviewLength()I

    move-result v14

    :goto_7
    sub-int v5, v15, v14

    .line 2838
    .local v5, "listRight":I
    if-eqz v12, :cond_9

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v14

    if-ge v14, v5, :cond_9

    invoke-virtual {v12}, Landroid/view/View;->getRight()I

    move-result v13

    .line 2842
    .restart local v13    # "xSearchPoint":I
    :goto_8
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    const/4 v15, 0x0

    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-virtual {v14, v13, v15, v13, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_5

    .line 2834
    .end local v5    # "listRight":I
    .end local v10    # "rightFadingEdgeShowing":Z
    .end local v13    # "xSearchPoint":I
    :cond_7
    const/4 v10, 0x0

    goto :goto_6

    .line 2836
    .restart local v10    # "rightFadingEdgeShowing":Z
    :cond_8
    const/4 v14, 0x0

    goto :goto_7

    .restart local v5    # "listRight":I
    :cond_9
    move v13, v5

    .line 2838
    goto :goto_8

    .line 2861
    .end local v5    # "listRight":I
    .end local v10    # "rightFadingEdgeShowing":Z
    .restart local v7    # "newFocus":Landroid/view/View;
    .restart local v9    # "positionOfNewFocus":I
    :cond_a
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v1, v7, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->amountToScrollToNewFocus(ILandroid/view/View;I)I

    move-result v2

    .line 2863
    .local v2, "focusScroll":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getMaxScrollAmount()I

    move-result v6

    .line 2864
    .local v6, "maxScrollAmount":I
    if-ge v2, v6, :cond_b

    .line 2866
    move/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2867
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    invoke-virtual {v14, v9, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;->populate(II)V

    .line 2868
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    goto/16 :goto_1

    .line 2869
    :cond_b
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->distanceToView(Landroid/view/View;)I

    move-result v14

    if-ge v14, v6, :cond_c

    .line 2874
    move/from16 v0, p1

    invoke-virtual {v7, v0}, Landroid/view/View;->requestFocus(I)Z

    .line 2875
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    invoke-virtual {v14, v9, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;->populate(II)V

    .line 2876
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mArrowScrollFocusResult:Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    goto/16 :goto_1

    .line 2879
    .end local v2    # "focusScroll":I
    .end local v6    # "maxScrollAmount":I
    .end local v9    # "positionOfNewFocus":I
    :cond_c
    const/4 v14, 0x0

    goto/16 :goto_1
.end method

.method private arrowScrollImpl(I)Z
    .locals 11
    .param p1, "direction"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v10, -0x1

    const/4 v9, 0x0

    .line 2438
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    if-gtz v7, :cond_1

    .line 2512
    :cond_0
    :goto_0
    return v9

    .line 2442
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2443
    .local v6, "selectedView":Landroid/view/View;
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    .line 2445
    .local v5, "selectedPos":I
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePositionOnScreen(I)I

    move-result v4

    .line 2446
    .local v4, "nextSelectedPosition":I
    invoke-direct {p0, p1, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->amountToScroll(II)I

    move-result v0

    .line 2449
    .local v0, "amountToScroll":I
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_b

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->arrowScrollFocused(I)Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;

    move-result-object v1

    .line 2450
    .local v1, "focusResult":Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;
    :goto_1
    if-eqz v1, :cond_2

    .line 2451
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;->getSelectedPosition()I

    move-result v4

    .line 2452
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;->getAmountToScroll()I

    move-result v0

    .line 2455
    :cond_2
    if-eqz v1, :cond_c

    move v3, v8

    .line 2456
    .local v3, "needToRedraw":Z
    :goto_2
    if-eq v4, v10, :cond_4

    .line 2457
    if-eqz v1, :cond_d

    move v7, v8

    :goto_3
    invoke-direct {p0, v6, p1, v4, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->handleNewSelectionChange(Landroid/view/View;IIZ)V

    .line 2458
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectedPositionInt(I)V

    .line 2459
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setNextSelectedPositionInt(I)V

    .line 2460
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getSelectedView()Landroid/view/View;

    move-result-object v6

    .line 2461
    move v5, v4

    .line 2462
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_3

    if-nez v1, :cond_3

    .line 2465
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 2466
    .local v2, "focused":Landroid/view/View;
    if-eqz v2, :cond_3

    .line 2467
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2470
    .end local v2    # "focused":Landroid/view/View;
    :cond_3
    const/4 v3, 0x1

    .line 2471
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkSelectionChanged()V

    .line 2474
    :cond_4
    if-lez v0, :cond_5

    .line 2475
    const/16 v7, 0x21

    if-ne p1, v7, :cond_e

    .end local v0    # "amountToScroll":I
    :goto_4
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->scrollListItemsBy(I)V

    .line 2476
    const/4 v3, 0x1

    .line 2481
    :cond_5
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    if-eqz v7, :cond_7

    if-nez v1, :cond_7

    if-eqz v6, :cond_7

    invoke-virtual {v6}, Landroid/view/View;->hasFocus()Z

    move-result v7

    if-eqz v7, :cond_7

    .line 2482
    invoke-virtual {v6}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v2

    .line 2483
    .restart local v2    # "focused":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 2485
    invoke-direct {p0, v2, p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->distanceToView(Landroid/view/View;)I

    move-result v7

    if-lez v7, :cond_7

    .line 2486
    :cond_6
    invoke-virtual {v2}, Landroid/view/View;->clearFocus()V

    .line 2491
    .end local v2    # "focused":Landroid/view/View;
    :cond_7
    if-ne v4, v10, :cond_8

    if-eqz v6, :cond_8

    invoke-direct {p0, v6, p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 2492
    const/4 v6, 0x0

    .line 2493
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->hideSelector()V

    .line 2497
    iput v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mResurrectToPosition:I

    .line 2500
    :cond_8
    if-eqz v3, :cond_0

    .line 2501
    if-eqz v6, :cond_9

    .line 2502
    invoke-virtual {p0, v5, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionSelector(ILandroid/view/View;)V

    .line 2503
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v7

    iput v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedLeft:I

    .line 2505
    :cond_9
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->awakenScrollBars()Z

    move-result v7

    if-nez v7, :cond_a

    .line 2506
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 2508
    :cond_a
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V

    move v9, v8

    .line 2509
    goto/16 :goto_0

    .line 2449
    .end local v1    # "focusResult":Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;
    .end local v3    # "needToRedraw":Z
    .restart local v0    # "amountToScroll":I
    :cond_b
    const/4 v1, 0x0

    goto/16 :goto_1

    .restart local v1    # "focusResult":Lcom/sec/samsung/gallery/hlistview/widget/HListView$ArrowScrollFocusResult;
    :cond_c
    move v3, v9

    .line 2455
    goto/16 :goto_2

    .restart local v3    # "needToRedraw":Z
    :cond_d
    move v7, v9

    .line 2457
    goto/16 :goto_3

    .line 2475
    :cond_e
    neg-int v0, v0

    goto :goto_4
.end method

.method private clearRecycledState(Ljava/util/ArrayList;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 503
    .local p1, "infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;>;"
    if-eqz p1, :cond_1

    .line 504
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 506
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 507
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    iget-object v0, v4, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    .line 508
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 509
    .local v3, "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-eqz v3, :cond_0

    .line 510
    const/4 v4, 0x0

    iput-boolean v4, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->recycledHeaderFooter:Z

    .line 506
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 514
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_1
    return-void
.end method

.method private commonKey(IILandroid/view/KeyEvent;)Z
    .locals 10
    .param p1, "keyCode"    # I
    .param p2, "count"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/16 v8, 0x82

    const/16 v7, 0x21

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2139
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsAttached:Z

    if-nez v5, :cond_1

    .line 2272
    :cond_0
    :goto_0
    return v3

    .line 2143
    :cond_1
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataChanged:Z

    if-eqz v5, :cond_2

    .line 2144
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->layoutChildren()V

    .line 2147
    :cond_2
    sget v5, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xb

    if-lt v5, v6, :cond_0

    .line 2151
    const/4 v2, 0x0

    .line 2152
    .local v2, "handled":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 2154
    .local v0, "action":I
    if-eq v0, v4, :cond_3

    .line 2155
    sparse-switch p1, :sswitch_data_0

    .line 2257
    :cond_3
    :goto_1
    if-eqz v2, :cond_1e

    move v3, v4

    .line 2258
    goto :goto_0

    .line 2157
    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2158
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v2

    .line 2159
    if-nez v2, :cond_3

    move v1, p2

    .line 2160
    .end local p2    # "count":I
    .local v1, "count":I
    :goto_2
    add-int/lit8 p2, v1, -0x1

    .end local v1    # "count":I
    .restart local p2    # "count":I
    if-lez v1, :cond_3

    .line 2161
    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->arrowScroll(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2162
    const/4 v2, 0x1

    move v1, p2

    .end local p2    # "count":I
    .restart local v1    # "count":I
    goto :goto_2

    .line 2168
    .end local v1    # "count":I
    .restart local p2    # "count":I
    :cond_4
    invoke-virtual {p3, v9}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2169
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    move v2, v4

    :goto_3
    goto :goto_1

    :cond_6
    move v2, v3

    goto :goto_3

    .line 2174
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2175
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v2

    .line 2176
    if-nez v2, :cond_3

    move v1, p2

    .line 2177
    .end local p2    # "count":I
    .restart local v1    # "count":I
    :goto_4
    add-int/lit8 p2, v1, -0x1

    .end local v1    # "count":I
    .restart local p2    # "count":I
    if-lez v1, :cond_3

    .line 2178
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->arrowScroll(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2179
    const/4 v2, 0x1

    move v1, p2

    .end local p2    # "count":I
    .restart local v1    # "count":I
    goto :goto_4

    .line 2185
    .end local v1    # "count":I
    .restart local p2    # "count":I
    :cond_7
    invoke-virtual {p3, v9}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2186
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_8

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_8
    move v2, v4

    :goto_5
    goto :goto_1

    :cond_9
    move v2, v3

    goto :goto_5

    .line 2191
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2192
    const/16 v5, 0x11

    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->handleHorizontalFocusWithinListItem(I)Z

    move-result v2

    goto :goto_1

    .line 2197
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2198
    const/16 v5, 0x42

    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->handleHorizontalFocusWithinListItem(I)Z

    move-result v2

    goto :goto_1

    .line 2204
    :sswitch_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2205
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v2

    .line 2206
    if-nez v2, :cond_3

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    if-nez v5, :cond_3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v5

    if-lez v5, :cond_3

    .line 2208
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->keyPressed()V

    .line 2209
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 2216
    :sswitch_5
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_d

    .line 2217
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->pageScroll(I)Z

    move-result v5

    if-eqz v5, :cond_c

    :cond_a
    move v2, v4

    .line 2221
    :cond_b
    :goto_6
    const/4 v2, 0x1

    .line 2222
    goto/16 :goto_1

    :cond_c
    move v2, v3

    .line 2217
    goto :goto_6

    .line 2218
    :cond_d
    invoke-virtual {p3, v4}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2219
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->pageScroll(I)Z

    move-result v5

    if-eqz v5, :cond_f

    :cond_e
    move v2, v4

    :goto_7
    goto :goto_6

    :cond_f
    move v2, v3

    goto :goto_7

    .line 2225
    :sswitch_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_12

    .line 2226
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_10

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->pageScroll(I)Z

    move-result v5

    if-eqz v5, :cond_11

    :cond_10
    move v2, v4

    :goto_8
    goto/16 :goto_1

    :cond_11
    move v2, v3

    goto :goto_8

    .line 2227
    :cond_12
    invoke-virtual {p3, v9}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2228
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_13

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_14

    :cond_13
    move v2, v4

    :goto_9
    goto/16 :goto_1

    :cond_14
    move v2, v3

    goto :goto_9

    .line 2233
    :sswitch_7
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_17

    .line 2234
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_15

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->pageScroll(I)Z

    move-result v5

    if-eqz v5, :cond_16

    :cond_15
    move v2, v4

    :goto_a
    goto/16 :goto_1

    :cond_16
    move v2, v3

    goto :goto_a

    .line 2235
    :cond_17
    invoke-virtual {p3, v9}, Landroid/view/KeyEvent;->hasModifiers(I)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2236
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_18

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_19

    :cond_18
    move v2, v4

    :goto_b
    goto/16 :goto_1

    :cond_19
    move v2, v3

    goto :goto_b

    .line 2241
    :sswitch_8
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2242
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_1a

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_1b

    :cond_1a
    move v2, v4

    :goto_c
    goto/16 :goto_1

    :cond_1b
    move v2, v3

    goto :goto_c

    .line 2247
    :sswitch_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->hasNoModifiers()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2248
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resurrectSelectionIfNeeded()Z

    move-result v5

    if-nez v5, :cond_1c

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fullScroll(I)Z

    move-result v5

    if-eqz v5, :cond_1d

    :cond_1c
    move v2, v4

    :goto_d
    goto/16 :goto_1

    :cond_1d
    move v2, v3

    goto :goto_d

    .line 2261
    :cond_1e
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 2263
    :pswitch_0
    invoke-super {p0, p1, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto/16 :goto_0

    .line 2266
    :pswitch_1
    invoke-super {p0, p1, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v3

    goto/16 :goto_0

    .line 2269
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v3

    goto/16 :goto_0

    .line 2155
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x17 -> :sswitch_4
        0x3e -> :sswitch_5
        0x42 -> :sswitch_4
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_8
        0x7b -> :sswitch_9
    .end sparse-switch

    .line 2261
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private correctTooSmall(I)V
    .locals 11
    .param p1, "childCount"    # I

    .prologue
    .line 1447
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-nez v8, :cond_2

    if-lez p1, :cond_2

    .line 1450
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1453
    .local v1, "firstChild":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1456
    .local v2, "firstLeft":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v8, Landroid/graphics/Rect;->left:I

    .line 1459
    .local v7, "start":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->right:I

    sub-int v0, v8, v9

    .line 1463
    .local v0, "end":I
    sub-int v6, v2, v7

    .line 1464
    .local v6, "leftOffset":I
    add-int/lit8 v8, p1, -0x1

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1465
    .local v3, "lastChild":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v5

    .line 1466
    .local v5, "lastRight":I
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v8, p1

    add-int/lit8 v4, v8, -0x1

    .line 1470
    .local v4, "lastPosition":I
    if-lez v6, :cond_2

    .line 1471
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-lt v4, v8, :cond_0

    if-le v5, v0, :cond_3

    .line 1472
    :cond_0
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v4, v8, :cond_1

    .line 1474
    sub-int v8, v5, v0

    invoke-static {v6, v8}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 1477
    :cond_1
    neg-int v8, v6

    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 1478
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ge v4, v8, :cond_2

    .line 1481
    add-int/lit8 v8, v4, 0x1

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v9

    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    add-int/2addr v9, v10

    invoke-direct {p0, v8, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 1483
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 1490
    .end local v0    # "end":I
    .end local v1    # "firstChild":Landroid/view/View;
    .end local v2    # "firstLeft":I
    .end local v3    # "lastChild":Landroid/view/View;
    .end local v4    # "lastPosition":I
    .end local v5    # "lastRight":I
    .end local v6    # "leftOffset":I
    .end local v7    # "start":I
    :cond_2
    :goto_0
    return-void

    .line 1485
    .restart local v0    # "end":I
    .restart local v1    # "firstChild":Landroid/view/View;
    .restart local v2    # "firstLeft":I
    .restart local v3    # "lastChild":Landroid/view/View;
    .restart local v4    # "lastPosition":I
    .restart local v5    # "lastRight":I
    .restart local v6    # "leftOffset":I
    .restart local v7    # "start":I
    :cond_3
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    if-ne v4, v8, :cond_2

    .line 1486
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    goto :goto_0
.end method

.method private correctTooWide(I)V
    .locals 10
    .param p1, "childCount"    # I

    .prologue
    .line 1398
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v7, p1

    add-int/lit8 v4, v7, -0x1

    .line 1399
    .local v4, "lastPosition":I
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v7, v7, -0x1

    if-ne v4, v7, :cond_2

    if-lez p1, :cond_2

    .line 1402
    add-int/lit8 v7, p1, -0x1

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1405
    .local v3, "lastChild":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v5

    .line 1408
    .local v5, "lastRight":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v7

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->right:I

    sub-int v0, v7, v8

    .line 1412
    .local v0, "end":I
    sub-int v6, v0, v5

    .line 1413
    .local v6, "rightOffset":I
    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1414
    .local v1, "firstChild":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1418
    .local v2, "firstLeft":I
    if-lez v6, :cond_2

    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-gtz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    if-ge v2, v7, :cond_2

    .line 1419
    :cond_0
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-nez v7, :cond_1

    .line 1421
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 1424
    :cond_1
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 1425
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-lez v7, :cond_2

    .line 1428
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v8

    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    sub-int/2addr v8, v9

    invoke-direct {p0, v7, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    .line 1430
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 1435
    .end local v0    # "end":I
    .end local v1    # "firstChild":Landroid/view/View;
    .end local v2    # "firstLeft":I
    .end local v3    # "lastChild":Landroid/view/View;
    .end local v5    # "lastRight":I
    .end local v6    # "rightOffset":I
    :cond_2
    return-void
.end method

.method private distanceToView(Landroid/view/View;)I
    .locals 4
    .param p1, "descendant"    # Landroid/view/View;

    .prologue
    .line 2953
    const/4 v0, 0x0

    .line 2954
    .local v0, "distance":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p1, v2}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 2955
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2956
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v1, v2, v3

    .line 2957
    .local v1, "listRight":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    if-ge v2, v3, :cond_1

    .line 2958
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sub-int v0, v2, v3

    .line 2962
    :cond_0
    :goto_0
    return v0

    .line 2959
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-le v2, v1, :cond_0

    .line 2960
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v1

    goto :goto_0
.end method

.method private fillBeforeAndAfter(Landroid/view/View;I)V
    .locals 3
    .param p1, "sel"    # Landroid/view/View;
    .param p2, "position"    # I

    .prologue
    .line 772
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 773
    .local v0, "dividerWidth":I
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v1, :cond_0

    .line 774
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    .line 775
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 776
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 782
    :goto_0
    return-void

    .line 778
    :cond_0
    add-int/lit8 v1, p2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v2

    add-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 779
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 780
    add-int/lit8 v1, p2, -0x1

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-direct {p0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    goto :goto_0
.end method

.method private fillFromLeft(I)Landroid/view/View;
    .locals 2
    .param p1, "nextLeft"    # I

    .prologue
    .line 721
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 722
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 723
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-gez v0, :cond_0

    .line 724
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 726
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private fillFromMiddle(II)Landroid/view/View;
    .locals 9
    .param p1, "childrenLeft"    # I
    .param p2, "childrenRight"    # I

    .prologue
    const/4 v3, 0x1

    .line 740
    sub-int v8, p2, p1

    .line 742
    .local v8, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->reconcileSelectedPosition()I

    move-result v1

    .line 744
    .local v1, "position":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v2, p1

    move v5, v3

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v6

    .line 745
    .local v6, "sel":Landroid/view/View;
    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 747
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    .line 748
    .local v7, "selWidth":I
    if-gt v7, v8, :cond_0

    .line 749
    sub-int v0, v8, v7

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v6, v0}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 752
    :cond_0
    invoke-direct {p0, v6, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 754
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v0, :cond_1

    .line 755
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooWide(I)V

    .line 760
    :goto_0
    return-object v6

    .line 757
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooSmall(I)V

    goto :goto_0
.end method

.method private fillFromSelection(III)Landroid/view/View;
    .locals 14
    .param p1, "selectedLeft"    # I
    .param p2, "childrenLeft"    # I
    .param p3, "childrenRight"    # I

    .prologue
    .line 797
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v7

    .line 798
    .local v7, "fadingEdgeLength":I
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    .line 802
    .local v2, "selectedPosition":I
    move/from16 v0, p2

    invoke-direct {p0, v0, v7, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeftSelectionPixel(III)I

    move-result v8

    .line 803
    .local v8, "leftSelectionPixel":I
    move/from16 v0, p3

    invoke-direct {p0, v0, v7, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRightSelectionPixel(III)I

    move-result v10

    .line 805
    .local v10, "rightSelectionPixel":I
    const/4 v4, 0x1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v1, Landroid/graphics/Rect;->top:I

    const/4 v6, 0x1

    move-object v1, p0

    move v3, p1

    invoke-direct/range {v1 .. v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v11

    .line 808
    .local v11, "sel":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v1

    if-le v1, v10, :cond_1

    .line 811
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v13, v1, v8

    .line 815
    .local v13, "spaceBefore":I
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v12, v1, v10

    .line 816
    .local v12, "spaceAfter":I
    invoke-static {v13, v12}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 819
    .local v9, "offset":I
    neg-int v1, v9

    invoke-virtual {v11, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 835
    .end local v9    # "offset":I
    .end local v12    # "spaceAfter":I
    .end local v13    # "spaceBefore":I
    :cond_0
    :goto_0
    invoke-direct {p0, v11, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    .line 837
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v1, :cond_2

    .line 838
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooWide(I)V

    .line 843
    :goto_1
    return-object v11

    .line 820
    :cond_1
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v1

    if-ge v1, v8, :cond_0

    .line 823
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v13, v8, v1

    .line 827
    .restart local v13    # "spaceBefore":I
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v1

    sub-int v12, v10, v1

    .line 828
    .restart local v12    # "spaceAfter":I
    invoke-static {v13, v12}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 831
    .restart local v9    # "offset":I
    invoke-virtual {v11, v9}, Landroid/view/View;->offsetLeftAndRight(I)V

    goto :goto_0

    .line 840
    .end local v9    # "offset":I
    .end local v12    # "spaceAfter":I
    .end local v13    # "spaceBefore":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooSmall(I)V

    goto :goto_1
.end method

.method private fillLeft(II)Landroid/view/View;
    .locals 9
    .param p1, "pos"    # I
    .param p2, "nextRight"    # I

    .prologue
    const/4 v3, 0x0

    .line 689
    const/4 v8, 0x0

    .line 691
    .local v8, "selectedView":Landroid/view/View;
    const/4 v7, 0x0

    .line 696
    .local v7, "end":I
    :goto_0
    if-le p2, v7, :cond_2

    if-ltz p1, :cond_2

    .line 698
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-ne p1, v0, :cond_1

    const/4 v5, 0x1

    .line 699
    .local v5, "selected":Z
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v6

    .line 700
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    sub-int p2, v0, v1

    .line 701
    if-eqz v5, :cond_0

    .line 702
    move-object v8, v6

    .line 704
    :cond_0
    add-int/lit8 p1, p1, -0x1

    .line 705
    goto :goto_0

    .end local v5    # "selected":Z
    .end local v6    # "child":Landroid/view/View;
    :cond_1
    move v5, v3

    .line 698
    goto :goto_1

    .line 707
    :cond_2
    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 708
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setVisibleRangeHint(II)V

    .line 709
    return-object v8
.end method

.method private fillRight(II)Landroid/view/View;
    .locals 9
    .param p1, "pos"    # I
    .param p2, "nextLeft"    # I

    .prologue
    const/4 v3, 0x1

    .line 654
    const/4 v8, 0x0

    .line 656
    .local v8, "selectedView":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v1

    sub-int v7, v0, v1

    .line 661
    .local v7, "end":I
    :goto_0
    if-ge p2, v7, :cond_2

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-ge p1, v0, :cond_2

    .line 663
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-ne p1, v0, :cond_1

    move v5, v3

    .line 664
    .local v5, "selected":Z
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v6

    .line 666
    .local v6, "child":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v0

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    add-int p2, v0, v1

    .line 667
    if-eqz v5, :cond_0

    .line 668
    move-object v8, v6

    .line 670
    :cond_0
    add-int/lit8 p1, p1, 0x1

    .line 671
    goto :goto_0

    .line 663
    .end local v5    # "selected":Z
    .end local v6    # "child":Landroid/view/View;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 673
    :cond_2
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setVisibleRangeHint(II)V

    .line 674
    return-object v8
.end method

.method private fillSpecific(II)Landroid/view/View;
    .locals 11
    .param p1, "position"    # I
    .param p2, "left"    # I

    .prologue
    const/4 v3, 0x1

    .line 1350
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-ne p1, v0, :cond_1

    move v5, v3

    .line 1351
    .local v5, "tempIsSelected":Z
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v10

    .line 1353
    .local v10, "temp":Landroid/view/View;
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 1358
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 1359
    .local v9, "dividerWidth":I
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v0, :cond_2

    .line 1360
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v9

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    move-result-object v7

    .line 1362
    .local v7, "before":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 1363
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v9

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    move-result-object v6

    .line 1364
    .local v6, "after":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v8

    .line 1365
    .local v8, "childCount":I
    if-lez v8, :cond_0

    .line 1366
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooWide(I)V

    .line 1379
    :cond_0
    :goto_1
    if-eqz v5, :cond_3

    .line 1384
    .end local v10    # "temp":Landroid/view/View;
    :goto_2
    return-object v10

    .line 1350
    .end local v5    # "tempIsSelected":Z
    .end local v6    # "after":Landroid/view/View;
    .end local v7    # "before":Landroid/view/View;
    .end local v8    # "childCount":I
    .end local v9    # "dividerWidth":I
    :cond_1
    const/4 v5, 0x0

    goto :goto_0

    .line 1369
    .restart local v5    # "tempIsSelected":Z
    .restart local v9    # "dividerWidth":I
    .restart local v10    # "temp":Landroid/view/View;
    :cond_2
    add-int/lit8 v0, p1, 0x1

    invoke-virtual {v10}, Landroid/view/View;->getRight()I

    move-result v1

    add-int/2addr v1, v9

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    move-result-object v6

    .line 1371
    .restart local v6    # "after":Landroid/view/View;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 1372
    add-int/lit8 v0, p1, -0x1

    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int/2addr v1, v9

    invoke-direct {p0, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    move-result-object v7

    .line 1373
    .restart local v7    # "before":Landroid/view/View;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v8

    .line 1374
    .restart local v8    # "childCount":I
    if-lez v8, :cond_0

    .line 1375
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooSmall(I)V

    goto :goto_1

    .line 1381
    :cond_3
    if-eqz v7, :cond_4

    move-object v10, v7

    .line 1382
    goto :goto_2

    :cond_4
    move-object v10, v6

    .line 1384
    goto :goto_2
.end method

.method private findAccessibilityFocusedChild(Landroid/view/View;)Landroid/view/View;
    .locals 2
    .param p1, "focusedView"    # Landroid/view/View;

    .prologue
    .line 1783
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1784
    .local v0, "viewParent":Landroid/view/ViewParent;
    :goto_0
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_0

    if-eq v0, p0, :cond_0

    move-object p1, v0

    .line 1785
    check-cast p1, Landroid/view/View;

    .line 1786
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 1788
    :cond_0
    instance-of v1, v0, Landroid/view/View;

    if-nez v1, :cond_1

    .line 1789
    const/4 p1, 0x0

    .line 1791
    .end local p1    # "focusedView":Landroid/view/View;
    :cond_1
    return-object p1
.end method

.method private getArrowScrollPreviewLength()I
    .locals 2

    .prologue
    .line 2648
    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method private getLeftSelectionPixel(III)I
    .locals 1
    .param p1, "childrenLeft"    # I
    .param p2, "fadingEdgeLength"    # I
    .param p3, "selectedPosition"    # I

    .prologue
    .line 878
    move v0, p1

    .line 879
    .local v0, "leftSelectionPixel":I
    if-lez p3, :cond_0

    .line 880
    add-int/2addr v0, p2

    .line 882
    :cond_0
    return v0
.end method

.method private getRightSelectionPixel(III)I
    .locals 2
    .param p1, "childrenRight"    # I
    .param p2, "fadingEdgeLength"    # I
    .param p3, "selectedPosition"    # I

    .prologue
    .line 858
    move v0, p1

    .line 859
    .local v0, "rightSelectionPixel":I
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    if-eq p3, v1, :cond_0

    .line 860
    sub-int/2addr v0, p2

    .line 862
    :cond_0
    return v0
.end method

.method private handleHorizontalFocusWithinListItem(I)Z
    .locals 7
    .param p1, "direction"    # I

    .prologue
    .line 2371
    const/16 v5, 0x11

    if-eq p1, v5, :cond_0

    const/16 v5, 0x42

    if-eq p1, v5, :cond_0

    .line 2372
    new-instance v5, Ljava/lang/IllegalArgumentException;

    const-string v6, "direction must be one of {View.FOCUS_LEFT, View.FOCUS_RIGHT}"

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 2376
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v3

    .line 2377
    .local v3, "numChildren":I
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    if-eqz v5, :cond_2

    if-lez v3, :cond_2

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    const/4 v6, -0x1

    if-eq v5, v6, :cond_2

    .line 2378
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getSelectedView()Landroid/view/View;

    move-result-object v4

    .line 2379
    .local v4, "selectedView":Landroid/view/View;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->hasFocus()Z

    move-result v5

    if-eqz v5, :cond_2

    instance-of v5, v4, Landroid/view/ViewGroup;

    if-eqz v5, :cond_2

    .line 2382
    invoke-virtual {v4}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 2383
    .local v0, "currentFocus":Landroid/view/View;
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v5

    check-cast v4, Landroid/view/ViewGroup;

    .end local v4    # "selectedView":Landroid/view/View;
    invoke-virtual {v5, v4, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v2

    .line 2385
    .local v2, "nextFocus":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 2387
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 2388
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2389
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v2, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 2390
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v2, p1, v5}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 2391
    const/4 v5, 0x1

    .line 2405
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :goto_0
    return v5

    .line 2398
    .restart local v0    # "currentFocus":Landroid/view/View;
    .restart local v2    # "nextFocus":Landroid/view/View;
    :cond_1
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRootView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-virtual {v6, v5, v0, p1}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 2400
    .local v1, "globalNextFocus":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 2401
    invoke-direct {p0, v1, p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v5

    goto :goto_0

    .line 2405
    .end local v0    # "currentFocus":Landroid/view/View;
    .end local v1    # "globalNextFocus":Landroid/view/View;
    .end local v2    # "nextFocus":Landroid/view/View;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private handleNewSelectionChange(Landroid/view/View;IIZ)V
    .locals 10
    .param p1, "selectedView"    # Landroid/view/View;
    .param p2, "direction"    # I
    .param p3, "newSelectedPosition"    # I
    .param p4, "newFocusAssigned"    # Z

    .prologue
    .line 2532
    const/4 v8, -0x1

    if-ne p3, v8, :cond_0

    .line 2533
    new-instance v8, Ljava/lang/IllegalArgumentException;

    const-string v9, "newSelectedPosition needs to be valid"

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 2543
    :cond_0
    const/4 v0, 0x0

    .line 2544
    .local v0, "leftSelected":Z
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    sub-int v7, v8, v9

    .line 2545
    .local v7, "selectedIndex":I
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    sub-int v3, p3, v8

    .line 2547
    .local v3, "nextSelectedIndex":I
    const/16 v8, 0x21

    if-ne p2, v8, :cond_3

    .line 2548
    move v2, v3

    .line 2549
    .local v2, "leftViewIndex":I
    move v6, v7

    .line 2550
    .local v6, "rightViewIndex":I
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2551
    .local v1, "leftView":Landroid/view/View;
    move-object v5, p1

    .line 2552
    .local v5, "rightView":Landroid/view/View;
    const/4 v0, 0x1

    .line 2560
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v4

    .line 2563
    .local v4, "numChildren":I
    if-eqz v1, :cond_1

    .line 2564
    if-nez p4, :cond_4

    if-eqz v0, :cond_4

    const/4 v8, 0x1

    :goto_1
    invoke-virtual {v1, v8}, Landroid/view/View;->setSelected(Z)V

    .line 2565
    invoke-direct {p0, v1, v2, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureAndAdjustRight(Landroid/view/View;II)V

    .line 2569
    :cond_1
    if-eqz v5, :cond_2

    .line 2570
    if-nez p4, :cond_5

    if-nez v0, :cond_5

    const/4 v8, 0x1

    :goto_2
    invoke-virtual {v5, v8}, Landroid/view/View;->setSelected(Z)V

    .line 2571
    invoke-direct {p0, v5, v6, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureAndAdjustRight(Landroid/view/View;II)V

    .line 2573
    :cond_2
    return-void

    .line 2554
    .end local v1    # "leftView":Landroid/view/View;
    .end local v2    # "leftViewIndex":I
    .end local v4    # "numChildren":I
    .end local v5    # "rightView":Landroid/view/View;
    .end local v6    # "rightViewIndex":I
    :cond_3
    move v2, v7

    .line 2555
    .restart local v2    # "leftViewIndex":I
    move v6, v3

    .line 2556
    .restart local v6    # "rightViewIndex":I
    move-object v1, p1

    .line 2557
    .restart local v1    # "leftView":Landroid/view/View;
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .restart local v5    # "rightView":Landroid/view/View;
    goto :goto_0

    .line 2564
    .restart local v4    # "numChildren":I
    :cond_4
    const/4 v8, 0x0

    goto :goto_1

    .line 2570
    :cond_5
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private isDirectChildHeaderOrFooter(Landroid/view/View;)Z
    .locals 7
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    .line 1801
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    .line 1802
    .local v1, "headers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;>;"
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1803
    .local v4, "numHeaders":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_1

    .line 1804
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    iget-object v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    if-ne p1, v5, :cond_0

    move v5, v6

    .line 1815
    :goto_1
    return v5

    .line 1803
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1808
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    .line 1809
    .local v0, "footers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;>;"
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 1810
    .local v3, "numFooters":I
    const/4 v2, 0x0

    :goto_2
    if-ge v2, v3, :cond_3

    .line 1811
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    iget-object v5, v5, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    if-ne p1, v5, :cond_2

    move v5, v6

    .line 1812
    goto :goto_1

    .line 1810
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1815
    :cond_3
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p1, "child"    # Landroid/view/View;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 2903
    if-ne p1, p2, :cond_1

    .line 2908
    :cond_0
    :goto_0
    return v1

    .line 2907
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2908
    .local v0, "theParent":Landroid/view/ViewParent;
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    check-cast v0, Landroid/view/View;

    .end local v0    # "theParent":Landroid/view/ViewParent;
    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private lookForSelectablePositionOnScreen(I)I
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v6, -0x1

    .line 2766
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 2767
    .local v1, "firstPosition":I
    const/16 v7, 0x82

    if-ne p1, v7, :cond_5

    .line 2768
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-eq v7, v6, :cond_1

    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v5, v7, 0x1

    .line 2771
    .local v5, "startPos":I
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_2

    move v4, v6

    .line 2806
    :cond_0
    :goto_1
    return v4

    .end local v5    # "startPos":I
    :cond_1
    move v5, v1

    .line 2768
    goto :goto_0

    .line 2774
    .restart local v5    # "startPos":I
    :cond_2
    if-ge v5, v1, :cond_3

    .line 2775
    move v5, v1

    .line 2778
    :cond_3
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLastVisiblePosition()I

    move-result v3

    .line 2779
    .local v3, "lastVisiblePos":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2780
    .local v0, "adapter":Landroid/widget/ListAdapter;
    move v4, v5

    .local v4, "pos":I
    :goto_2
    if-gt v4, v3, :cond_b

    .line 2781
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_4

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_0

    .line 2780
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2787
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    .end local v3    # "lastVisiblePos":I
    .end local v4    # "pos":I
    .end local v5    # "startPos":I
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v2, v7, -0x1

    .line 2788
    .local v2, "last":I
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-eq v7, v6, :cond_7

    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v5, v7, -0x1

    .line 2791
    .restart local v5    # "startPos":I
    :goto_3
    if-ltz v5, :cond_6

    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v7}, Landroid/widget/ListAdapter;->getCount()I

    move-result v7

    if-lt v5, v7, :cond_8

    :cond_6
    move v4, v6

    .line 2792
    goto :goto_1

    .line 2788
    .end local v5    # "startPos":I
    :cond_7
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v1

    add-int/lit8 v5, v7, -0x1

    goto :goto_3

    .line 2794
    .restart local v5    # "startPos":I
    :cond_8
    if-le v5, v2, :cond_9

    .line 2795
    move v5, v2

    .line 2798
    :cond_9
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 2799
    .restart local v0    # "adapter":Landroid/widget/ListAdapter;
    move v4, v5

    .restart local v4    # "pos":I
    :goto_4
    if-lt v4, v1, :cond_b

    .line 2800
    invoke-interface {v0, v4}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v7

    if-eqz v7, :cond_a

    sub-int v7, v4, v1

    invoke-virtual {p0, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v7

    if-eqz v7, :cond_0

    .line 2799
    :cond_a
    add-int/lit8 v4, v4, -0x1

    goto :goto_4

    .end local v2    # "last":I
    :cond_b
    move v4, v6

    .line 2806
    goto :goto_1
.end method

.method private makeAndAddView(IIZIZ)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "x"    # I
    .param p3, "flow"    # Z
    .param p4, "childrenTop"    # I
    .param p5, "selected"    # Z

    .prologue
    .line 1837
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataChanged:Z

    if-nez v0, :cond_0

    .line 1839
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->getActiveView(I)Landroid/view/View;

    move-result-object v1

    .line 1840
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1843
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setupChild(Landroid/view/View;IIZIZZ)V

    move-object v8, v1

    .line 1855
    .end local v1    # "child":Landroid/view/View;
    .local v8, "child":Landroid/view/View;
    :goto_0
    return-object v8

    .line 1850
    .end local v8    # "child":Landroid/view/View;
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v1

    .line 1853
    .restart local v1    # "child":Landroid/view/View;
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    const/4 v2, 0x0

    aget-boolean v7, v0, v2

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setupChild(Landroid/view/View;IIZIZZ)V

    move-object v8, v1

    .line 1855
    .end local v1    # "child":Landroid/view/View;
    .restart local v8    # "child":Landroid/view/View;
    goto :goto_0
.end method

.method private measureAndAdjustRight(Landroid/view/View;II)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "childIndex"    # I
    .param p3, "numChildren"    # I

    .prologue
    .line 2586
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v1

    .line 2587
    .local v1, "oldWidth":I
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureItem(Landroid/view/View;)V

    .line 2588
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    if-eq v3, v1, :cond_0

    .line 2590
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->relayoutMeasuredItem(Landroid/view/View;)V

    .line 2593
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    sub-int v2, v3, v1

    .line 2594
    .local v2, "widthDelta":I
    add-int/lit8 v0, p2, 0x1

    .local v0, "i":I
    :goto_0
    if-ge v0, p3, :cond_0

    .line 2595
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 2594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2598
    .end local v0    # "i":I
    .end local v2    # "widthDelta":I
    :cond_0
    return-void
.end method

.method private measureItem(Landroid/view/View;)V
    .locals 8
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    .line 2607
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .line 2608
    .local v3, "p":Landroid/view/ViewGroup$LayoutParams;
    if-nez v3, :cond_0

    .line 2609
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    .end local v3    # "p":Landroid/view/ViewGroup$LayoutParams;
    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 2614
    .restart local v3    # "p":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeightMeasureSpec:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v5, v6

    iget v6, v3, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v5, v6}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 2615
    .local v0, "childHeightSpec":I
    iget v2, v3, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 2617
    .local v2, "lpWidth":I
    if-lez v2, :cond_1

    .line 2618
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 2622
    .local v1, "childWidthSpec":I
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 2623
    return-void

    .line 2620
    .end local v1    # "childWidthSpec":I
    :cond_1
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .restart local v1    # "childWidthSpec":I
    goto :goto_0
.end method

.method private measureScrapChildWidth(Landroid/view/View;II)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "heightMeasureSpec"    # I

    .prologue
    const/4 v6, 0x0

    .line 1150
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 1151
    .local v3, "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-nez v3, :cond_0

    .line 1152
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    .end local v3    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    check-cast v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 1153
    .restart local v3    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    invoke-virtual {p1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1155
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v4, p2}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v4

    iput v4, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    .line 1156
    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->forceAdd:Z

    .line 1158
    iget-object v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v5

    iget v5, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->height:I

    invoke-static {p3, v4, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 1159
    .local v0, "childHeightSpec":I
    iget v2, v3, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->width:I

    .line 1161
    .local v2, "lpWidth":I
    if-lez v2, :cond_1

    .line 1162
    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 1166
    .local v1, "childWidthSpec":I
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 1167
    return-void

    .line 1164
    .end local v1    # "childWidthSpec":I
    :cond_1
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .restart local v1    # "childWidthSpec":I
    goto :goto_0
.end method

.method private moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;
    .locals 20
    .param p1, "oldSel"    # Landroid/view/View;
    .param p2, "newSel"    # Landroid/view/View;
    .param p3, "delta"    # I
    .param p4, "childrenLeft"    # I
    .param p5, "childrenRight"    # I

    .prologue
    .line 926
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v10

    .line 927
    .local v10, "fadingEdgeLength":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    move/from16 v17, v0

    .line 931
    .local v17, "selectedPosition":I
    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, v17

    invoke-direct {v0, v1, v10, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeftSelectionPixel(III)I

    move-result v12

    .line 932
    .local v12, "leftSelectionPixel":I
    move-object/from16 v0, p0

    move/from16 v1, p4

    move/from16 v2, v17

    invoke-direct {v0, v1, v10, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRightSelectionPixel(III)I

    move-result v15

    .line 934
    .local v15, "rightSelectionPixel":I
    if-lez p3, :cond_2

    .line 947
    add-int/lit8 v4, v17, -0x1

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v5

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object p1

    .line 949
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 952
    .local v9, "dividerWidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getRight()I

    move-result v3

    add-int v5, v3, v9

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x1

    move-object/from16 v3, p0

    move/from16 v4, v17

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v16

    .line 955
    .local v16, "sel":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v3

    if-le v3, v15, :cond_0

    .line 958
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v19, v3, v12

    .line 961
    .local v19, "spaceBefore":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int v18, v3, v15

    .line 964
    .local v18, "spaceAfter":I
    sub-int v3, p5, p4

    div-int/lit8 v11, v3, 0x2

    .line 965
    .local v11, "halfHorizontalSpace":I
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 966
    .local v14, "offset":I
    invoke-static {v14, v11}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 969
    neg-int v3, v14

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 971
    neg-int v3, v14

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 975
    .end local v11    # "halfHorizontalSpace":I
    .end local v14    # "offset":I
    .end local v18    # "spaceAfter":I
    .end local v19    # "spaceBefore":I
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v3, :cond_1

    .line 976
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v3, v3, -0x2

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v4, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    .line 977
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 978
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v4, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 1048
    .end local v9    # "dividerWidth":I
    :goto_0
    return-object v16

    .line 980
    .restart local v9    # "dividerWidth":I
    :cond_1
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v3, v3, 0x1

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v4

    add-int/2addr v4, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 981
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    .line 982
    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    add-int/lit8 v3, v3, -0x2

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v4

    sub-int/2addr v4, v9

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    goto :goto_0

    .line 984
    .end local v9    # "dividerWidth":I
    .end local v16    # "sel":Landroid/view/View;
    :cond_2
    if-gez p3, :cond_5

    .line 996
    if-eqz p2, :cond_4

    .line 998
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getLeft()I

    move-result v5

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x1

    move-object/from16 v3, p0

    move/from16 v4, v17

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v16

    .line 1006
    .restart local v16    # "sel":Landroid/view/View;
    :goto_1
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v3

    if-ge v3, v12, :cond_3

    .line 1008
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v19, v12, v3

    .line 1011
    .restart local v19    # "spaceBefore":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int v18, v15, v3

    .line 1014
    .restart local v18    # "spaceAfter":I
    sub-int v3, p5, p4

    div-int/lit8 v11, v3, 0x2

    .line 1015
    .restart local v11    # "halfHorizontalSpace":I
    move/from16 v0, v19

    move/from16 v1, v18

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 1016
    .restart local v14    # "offset":I
    invoke-static {v14, v11}, Ljava/lang/Math;->min(II)I

    move-result v14

    .line 1019
    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1023
    .end local v11    # "halfHorizontalSpace":I
    .end local v14    # "offset":I
    .end local v18    # "spaceAfter":I
    .end local v19    # "spaceBefore":I
    :cond_3
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto :goto_0

    .line 1002
    .end local v16    # "sel":Landroid/view/View;
    :cond_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x1

    move-object/from16 v3, p0

    move/from16 v4, v17

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v16

    .restart local v16    # "sel":Landroid/view/View;
    goto :goto_1

    .line 1026
    .end local v16    # "sel":Landroid/view/View;
    :cond_5
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v5

    .line 1031
    .local v5, "oldLeft":I
    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v7, v3, Landroid/graphics/Rect;->top:I

    const/4 v8, 0x1

    move-object/from16 v3, p0

    move/from16 v4, v17

    invoke-direct/range {v3 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->makeAndAddView(IIZIZ)Landroid/view/View;

    move-result-object v16

    .line 1034
    .restart local v16    # "sel":Landroid/view/View;
    move/from16 v0, p4

    if-ge v5, v0, :cond_6

    .line 1037
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v13

    .line 1038
    .local v13, "newRight":I
    add-int/lit8 v3, p4, 0x14

    if-ge v13, v3, :cond_6

    .line 1040
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int v3, p4, v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1045
    .end local v13    # "newRight":I
    :cond_6
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move/from16 v2, v17

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillBeforeAndAfter(Landroid/view/View;I)V

    goto/16 :goto_0
.end method

.method private positionOfNewFocus(Landroid/view/View;)I
    .locals 5
    .param p1, "newFocus"    # Landroid/view/View;

    .prologue
    .line 2888
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v2

    .line 2889
    .local v2, "numChildren":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2890
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2891
    .local v0, "child":Landroid/view/View;
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isViewAncestorOf(Landroid/view/View;Landroid/view/View;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2892
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v3, v1

    return v3

    .line 2889
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2895
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "newFocus is not a child of any of the children of the list!"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method private relayoutMeasuredItem(Landroid/view/View;)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 2632
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 2633
    .local v5, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    .line 2635
    .local v4, "h":I
    iget-object v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v6, Landroid/graphics/Rect;->top:I

    .line 2636
    .local v3, "childTop":I
    add-int v0, v3, v4

    .line 2638
    .local v0, "childBottom":I
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 2639
    .local v1, "childLeft":I
    add-int v2, v1, v5

    .line 2641
    .local v2, "childRight":I
    invoke-virtual {p1, v1, v3, v2, v0}, Landroid/view/View;->layout(IIII)V

    .line 2642
    return-void
.end method

.method private removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/View;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 321
    .local p2, "where":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;>;"
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 322
    .local v2, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 323
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    .line 324
    .local v1, "info":Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    iget-object v3, v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    if-ne v3, p1, :cond_1

    .line 325
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 329
    .end local v1    # "info":Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    :cond_0
    return-void

    .line 322
    .restart local v1    # "info":Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private scrollListItemsBy(I)V
    .locals 12
    .param p1, "amount"    # I

    .prologue
    const/4 v11, 0x0

    .line 2972
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 2974
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    sub-int v6, v9, v10

    .line 2975
    .local v6, "listRight":I
    iget-object v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v9, Landroid/graphics/Rect;->left:I

    .line 2976
    .local v5, "listLeft":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .line 2978
    .local v8, "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    if-gez p1, :cond_3

    .line 2982
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    .line 2983
    .local v7, "numChildren":I
    add-int/lit8 v9, v7, -0x1

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2984
    .local v1, "last":Landroid/view/View;
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v9

    if-ge v9, v6, :cond_0

    .line 2985
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v9, v7

    add-int/lit8 v3, v9, -0x1

    .line 2986
    .local v3, "lastVisiblePosition":I
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v9, v9, -0x1

    if-ge v3, v9, :cond_0

    .line 2987
    invoke-direct {p0, v1, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addViewAfter(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 2988
    add-int/lit8 v7, v7, 0x1

    .line 2992
    goto :goto_0

    .line 2997
    .end local v3    # "lastVisiblePosition":I
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v9

    if-ge v9, v6, :cond_1

    .line 2998
    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v9

    sub-int v9, v6, v9

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 3002
    :cond_1
    invoke-virtual {p0, v11}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3003
    .local v0, "first":Landroid/view/View;
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v9

    if-ge v9, v5, :cond_7

    .line 3004
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 3005
    .local v4, "layoutParams":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    iget v9, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 3006
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->detachViewFromParent(Landroid/view/View;)V

    .line 3007
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-virtual {v8, v0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3011
    :goto_2
    invoke-virtual {p0, v11}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3012
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    goto :goto_1

    .line 3009
    :cond_2
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_2

    .line 3016
    .end local v0    # "first":Landroid/view/View;
    .end local v1    # "last":Landroid/view/View;
    .end local v4    # "layoutParams":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .end local v7    # "numChildren":I
    :cond_3
    invoke-virtual {p0, v11}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3019
    .restart local v0    # "first":Landroid/view/View;
    :goto_3
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v9

    if-le v9, v5, :cond_4

    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-lez v9, :cond_4

    .line 3020
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-direct {p0, v0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addViewBefore(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 3021
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/lit8 v9, v9, -0x1

    iput v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    goto :goto_3

    .line 3026
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v9

    if-le v9, v5, :cond_5

    .line 3027
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v9

    sub-int v9, v5, v9

    invoke-virtual {p0, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetChildrenLeftAndRight(I)V

    .line 3030
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v9

    add-int/lit8 v2, v9, -0x1

    .line 3031
    .local v2, "lastIndex":I
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3034
    .restart local v1    # "last":Landroid/view/View;
    :goto_4
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v9

    if-le v9, v6, :cond_7

    .line 3035
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 3036
    .restart local v4    # "layoutParams":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    iget v9, v4, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 3037
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->detachViewFromParent(Landroid/view/View;)V

    .line 3038
    iget v9, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v9, v2

    invoke-virtual {v8, v1, v9}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 3042
    :goto_5
    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3043
    goto :goto_4

    .line 3040
    :cond_6
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->removeViewInLayout(Landroid/view/View;)V

    goto :goto_5

    .line 3045
    .end local v2    # "lastIndex":I
    .end local v4    # "layoutParams":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_7
    return-void
.end method

.method private setupChild(Landroid/view/View;IIZIZZ)V
    .locals 22
    .param p1, "child"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "x"    # I
    .param p4, "flowDown"    # Z
    .param p5, "childrenTop"    # I
    .param p6, "selected"    # Z
    .param p7, "recycled"    # Z

    .prologue
    .line 1877
    if-eqz p6, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->shouldShowSelector()Z

    move-result v19

    if-eqz v19, :cond_9

    const/4 v11, 0x1

    .line 1878
    .local v11, "isSelected":Z
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isSelected()Z

    move-result v19

    move/from16 v0, v19

    if-eq v11, v0, :cond_a

    const/16 v17, 0x1

    .line 1879
    .local v17, "updateChildSelected":Z
    :goto_1
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTouchMode:I

    .line 1880
    .local v13, "mode":I
    if-lez v13, :cond_b

    const/16 v19, 0x3

    move/from16 v0, v19

    if-ge v13, v0, :cond_b

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMotionPosition:I

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, p2

    if-ne v0, v1, :cond_b

    const/4 v10, 0x1

    .line 1881
    .local v10, "isPressed":Z
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isPressed()Z

    move-result v19

    move/from16 v0, v19

    if-eq v10, v0, :cond_c

    const/16 v16, 0x1

    .line 1882
    .local v16, "updateChildPressed":Z
    :goto_3
    if-eqz p7, :cond_0

    if-nez v17, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isLayoutRequested()Z

    move-result v19

    if-eqz v19, :cond_d

    :cond_0
    const/4 v14, 0x1

    .line 1886
    .local v14, "needToMeasure":Z
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    check-cast v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 1887
    .local v15, "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    if-nez v15, :cond_1

    .line 1888
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v15

    .end local v15    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    check-cast v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    .line 1890
    .restart local v15    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->getItemViewType(I)I

    move-result v19

    move/from16 v0, v19

    iput v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    .line 1892
    if-eqz p7, :cond_2

    iget-boolean v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->forceAdd:Z

    move/from16 v19, v0

    if-eqz v19, :cond_3

    :cond_2
    iget-boolean v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->recycledHeaderFooter:Z

    move/from16 v19, v0

    if-eqz v19, :cond_f

    iget v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    move/from16 v19, v0

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_f

    .line 1893
    :cond_3
    if-eqz p4, :cond_e

    const/16 v19, -0x1

    :goto_5
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2, v15}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 1902
    :goto_6
    if-eqz v17, :cond_4

    .line 1903
    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/view/View;->setSelected(Z)V

    .line 1906
    :cond_4
    if-eqz v16, :cond_5

    .line 1907
    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/view/View;->setPressed(Z)V

    .line 1910
    :cond_5
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mChoiceMode:I

    move/from16 v19, v0

    if-eqz v19, :cond_6

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v19, v0

    if-eqz v19, :cond_6

    .line 1911
    move-object/from16 v0, p1

    instance-of v0, v0, Landroid/widget/Checkable;

    move/from16 v19, v0

    if-eqz v19, :cond_12

    move-object/from16 v19, p1

    .line 1912
    check-cast v19, Landroid/widget/Checkable;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v20

    invoke-interface/range {v19 .. v20}, Landroid/widget/Checkable;->setChecked(Z)V

    .line 1918
    :cond_6
    :goto_7
    if-eqz v14, :cond_14

    .line 1919
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeightMeasureSpec:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v21, v0

    add-int v20, v20, v21

    iget v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->height:I

    move/from16 v21, v0

    invoke-static/range {v19 .. v21}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v5

    .line 1920
    .local v5, "childHeightSpec":I
    iget v12, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->width:I

    .line 1922
    .local v12, "lpWidth":I
    if-lez v12, :cond_13

    .line 1923
    const/high16 v19, 0x40000000    # 2.0f

    move/from16 v0, v19

    invoke-static {v12, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 1927
    .local v8, "childWidthSpec":I
    :goto_8
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v5}, Landroid/view/View;->measure(II)V

    .line 1932
    .end local v5    # "childHeightSpec":I
    .end local v8    # "childWidthSpec":I
    .end local v12    # "lpWidth":I
    :goto_9
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v18

    .line 1933
    .local v18, "w":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    .line 1934
    .local v9, "h":I
    if-eqz p4, :cond_15

    move/from16 v6, p3

    .line 1936
    .local v6, "childLeft":I
    :goto_a
    if-eqz v14, :cond_16

    .line 1937
    add-int v4, p5, v9

    .line 1938
    .local v4, "childBottom":I
    add-int v7, v6, v18

    .line 1939
    .local v7, "childRight":I
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-virtual {v0, v6, v1, v7, v4}, Landroid/view/View;->layout(IIII)V

    .line 1945
    .end local v4    # "childBottom":I
    .end local v7    # "childRight":I
    :goto_b
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingStarted:Z

    move/from16 v19, v0

    if-eqz v19, :cond_7

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->isDrawingCacheEnabled()Z

    move-result v19

    if-nez v19, :cond_7

    .line 1946
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    .line 1949
    :cond_7
    sget v19, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_8

    .line 1950
    if-eqz p7, :cond_8

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->scrappedFromPosition:I

    move/from16 v19, v0

    move/from16 v0, v19

    move/from16 v1, p2

    if-eq v0, v1, :cond_8

    .line 1951
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 1954
    :cond_8
    return-void

    .line 1877
    .end local v6    # "childLeft":I
    .end local v9    # "h":I
    .end local v10    # "isPressed":Z
    .end local v11    # "isSelected":Z
    .end local v13    # "mode":I
    .end local v14    # "needToMeasure":Z
    .end local v15    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    .end local v16    # "updateChildPressed":Z
    .end local v17    # "updateChildSelected":Z
    .end local v18    # "w":I
    :cond_9
    const/4 v11, 0x0

    goto/16 :goto_0

    .line 1878
    .restart local v11    # "isSelected":Z
    :cond_a
    const/16 v17, 0x0

    goto/16 :goto_1

    .line 1880
    .restart local v13    # "mode":I
    .restart local v17    # "updateChildSelected":Z
    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 1881
    .restart local v10    # "isPressed":Z
    :cond_c
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 1882
    .restart local v16    # "updateChildPressed":Z
    :cond_d
    const/4 v14, 0x0

    goto/16 :goto_4

    .line 1893
    .restart local v14    # "needToMeasure":Z
    .restart local v15    # "p":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;
    :cond_e
    const/16 v19, 0x0

    goto/16 :goto_5

    .line 1895
    :cond_f
    const/16 v19, 0x0

    move/from16 v0, v19

    iput-boolean v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->forceAdd:Z

    .line 1896
    iget v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    move/from16 v19, v0

    const/16 v20, -0x2

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 1897
    const/16 v19, 0x1

    move/from16 v0, v19

    iput-boolean v0, v15, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->recycledHeaderFooter:Z

    .line 1899
    :cond_10
    if-eqz p4, :cond_11

    const/16 v19, -0x1

    :goto_c
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v15, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    goto/16 :goto_6

    :cond_11
    const/16 v19, 0x0

    goto :goto_c

    .line 1913
    :cond_12
    sget v19, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v20, 0xb

    move/from16 v0, v19

    move/from16 v1, v20

    if-lt v0, v1, :cond_6

    .line 1914
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->setActivated(Z)V

    goto/16 :goto_7

    .line 1925
    .restart local v5    # "childHeightSpec":I
    .restart local v12    # "lpWidth":I
    :cond_13
    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-static/range {v19 .. v20}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .restart local v8    # "childWidthSpec":I
    goto/16 :goto_8

    .line 1929
    .end local v5    # "childHeightSpec":I
    .end local v8    # "childWidthSpec":I
    .end local v12    # "lpWidth":I
    :cond_14
    invoke-virtual/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->cleanupLayoutState(Landroid/view/View;)V

    goto/16 :goto_9

    .line 1934
    .restart local v9    # "h":I
    .restart local v18    # "w":I
    :cond_15
    sub-int v6, p3, v18

    goto/16 :goto_a

    .line 1941
    .restart local v6    # "childLeft":I
    :cond_16
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v19

    sub-int v19, v6, v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1942
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v19

    sub-int v19, p5, v19

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/view/View;->offsetTopAndBottom(I)V

    goto/16 :goto_b
.end method

.method private showingLeftFadingEdge()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 520
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int v0, v2, v3

    .line 521
    .local v0, "listLeft":I
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-gtz v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v2

    if-le v2, v0, :cond_1

    :cond_0
    const/4 v1, 0x1

    :cond_1
    return v1
.end method

.method private showingRightFadingEdge()Z
    .locals 6

    .prologue
    .line 528
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    .line 529
    .local v0, "childCount":I
    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v3

    .line 530
    .local v3, "rightOfRightChild":I
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v4, v0

    add-int/lit8 v1, v4, -0x1

    .line 532
    .local v1, "lastVisiblePosition":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    sub-int v2, v4, v5

    .line 534
    .local v2, "listRight":I
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v4, v4, -0x1

    if-lt v1, v4, :cond_0

    if-ge v3, v2, :cond_1

    :cond_0
    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addFooterView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 377
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 378
    return-void
.end method

.method public addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "isSelectable"    # Z

    .prologue
    .line 352
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;-><init>()V

    .line 353
    .local v0, "info":Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    iput-object p1, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    .line 354
    iput-object p2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->data:Ljava/lang/Object;

    .line 355
    iput-boolean p3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->isSelectable:Z

    .line 356
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 360
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 361
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;->onChanged()V

    .line 363
    :cond_0
    return-void
.end method

.method public addHeaderView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 290
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 291
    return-void
.end method

.method public addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "data"    # Ljava/lang/Object;
    .param p3, "isSelectable"    # Z

    .prologue
    .line 261
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;

    if-nez v1, :cond_0

    .line 262
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot add header view to list -- setAdapter has already been called."

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 266
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;-><init>()V

    .line 267
    .local v0, "info":Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;
    iput-object p1, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->view:Landroid/view/View;

    .line 268
    iput-object p2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->data:Ljava/lang/Object;

    .line 269
    iput-boolean p3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FixedViewInfo;->isSelectable:Z

    .line 270
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_1

    .line 275
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;->onChanged()V

    .line 277
    :cond_1
    return-void
.end method

.method arrowScroll(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v2, 0x0

    .line 2418
    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mInLayout:Z

    .line 2419
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->arrowScrollImpl(I)Z

    move-result v0

    .line 2420
    .local v0, "handled":Z
    if-eqz v0, :cond_0

    .line 2421
    invoke-static {p1}, Landroid/view/SoundEffectConstants;->getContantForFocusDirection(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->playSoundEffect(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2425
    :cond_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mInLayout:Z

    return v0

    .end local v0    # "handled":Z
    :catchall_0
    move-exception v1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mInLayout:Z

    throw v1
.end method

.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 1958
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 34
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 3151
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingStarted:Z

    move/from16 v32, v0

    if-eqz v32, :cond_0

    .line 3152
    const/16 v32, 0x1

    move/from16 v0, v32

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingActive:Z

    .line 3156
    :cond_0
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 3157
    .local v9, "dividerWidth":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    move-object/from16 v27, v0

    .line 3158
    .local v27, "overscrollHeader":Landroid/graphics/drawable/Drawable;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    move-object/from16 v26, v0

    .line 3159
    .local v26, "overscrollFooter":Landroid/graphics/drawable/Drawable;
    if-eqz v27, :cond_9

    const/4 v12, 0x1

    .line 3160
    .local v12, "drawOverscrollHeader":Z
    :goto_0
    if-eqz v26, :cond_a

    const/4 v11, 0x1

    .line 3161
    .local v11, "drawOverscrollFooter":Z
    :goto_1
    if-lez v9, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    move-object/from16 v32, v0

    if-eqz v32, :cond_b

    const/4 v10, 0x1

    .line 3163
    .local v10, "drawDividers":Z
    :goto_2
    if-nez v10, :cond_1

    if-nez v12, :cond_1

    if-eqz v11, :cond_10

    .line 3165
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    .line 3166
    .local v6, "bounds":Landroid/graphics/Rect;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingTop()I

    move-result v32

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->top:I

    .line 3167
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getBottom()I

    move-result v32

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getTop()I

    move-result v33

    sub-int v32, v32, v33

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingBottom()I

    move-result v33

    sub-int v32, v32, v33

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->bottom:I

    .line 3169
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v8

    .line 3170
    .local v8, "count":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v19

    .line 3171
    .local v19, "headerCount":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    move/from16 v22, v0

    .line 3172
    .local v22, "itemCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    move-object/from16 v32, v0

    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->size()I

    move-result v32

    sub-int v32, v22, v32

    add-int/lit8 v18, v32, -0x1

    .line 3173
    .local v18, "footerLimit":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderDividersEnabled:Z

    move/from16 v20, v0

    .line 3174
    .local v20, "headerDividers":Z
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterDividersEnabled:Z

    move/from16 v17, v0

    .line 3175
    .local v17, "footerDividers":Z
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    move/from16 v16, v0

    .line 3176
    .local v16, "first":I
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAreAllItemsSelectable:Z

    .line 3177
    .local v5, "areAllItemsSelectable":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3182
    .local v4, "adapter":Landroid/widget/ListAdapter;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isOpaque()Z

    move-result v32

    if-eqz v32, :cond_c

    invoke-super/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isOpaque()Z

    move-result v32

    if-nez v32, :cond_c

    const/4 v15, 0x1

    .line 3184
    .local v15, "fillForMissingDividers":Z
    :goto_3
    if-eqz v15, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    move-object/from16 v32, v0

    if-nez v32, :cond_2

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsCacheColorOpaque:Z

    move/from16 v32, v0

    if-eqz v32, :cond_2

    .line 3185
    new-instance v32, Landroid/graphics/Paint;

    invoke-direct/range {v32 .. v32}, Landroid/graphics/Paint;-><init>()V

    move-object/from16 v0, v32

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    .line 3186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    move-object/from16 v32, v0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getCacheColorHint()I

    move-result v33

    invoke-virtual/range {v32 .. v33}, Landroid/graphics/Paint;->setColor(I)V

    .line 3188
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    move-object/from16 v28, v0

    .line 3190
    .local v28, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x0

    .line 3191
    .local v13, "effectivePaddingLeft":I
    const/4 v14, 0x0

    .line 3197
    .local v14, "effectivePaddingRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v32

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v33

    sub-int v32, v32, v33

    sub-int v32, v32, v14

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v33

    add-int v24, v32, v33

    .line 3198
    .local v24, "listRight":I
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    move/from16 v32, v0

    if-nez v32, :cond_11

    .line 3199
    const/16 v29, 0x0

    .line 3202
    .local v29, "right":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v30

    .line 3203
    .local v30, "scrollX":I
    if-lez v8, :cond_3

    if-gez v30, :cond_3

    .line 3204
    if-eqz v12, :cond_d

    .line 3205
    const/16 v32, 0x0

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3206
    move/from16 v0, v30

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3207
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 3215
    :cond_3
    :goto_4
    const/16 v21, 0x0

    .local v21, "i":I
    :goto_5
    move/from16 v0, v21

    if-ge v0, v8, :cond_f

    .line 3216
    if-nez v20, :cond_4

    add-int v32, v16, v21

    move/from16 v0, v32

    move/from16 v1, v19

    if-lt v0, v1, :cond_8

    :cond_4
    if-nez v17, :cond_5

    add-int v32, v16, v21

    move/from16 v0, v32

    move/from16 v1, v18

    if-ge v0, v1, :cond_8

    .line 3218
    :cond_5
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 3219
    .local v7, "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getRight()I

    move-result v29

    .line 3222
    if-eqz v10, :cond_8

    move/from16 v0, v29

    move/from16 v1, v24

    if-ge v0, v1, :cond_8

    if-eqz v11, :cond_6

    add-int/lit8 v32, v8, -0x1

    move/from16 v0, v21

    move/from16 v1, v32

    if-eq v0, v1, :cond_8

    .line 3224
    :cond_6
    if-nez v5, :cond_7

    add-int v32, v16, v21

    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v32

    if-eqz v32, :cond_e

    add-int/lit8 v32, v8, -0x1

    move/from16 v0, v21

    move/from16 v1, v32

    if-eq v0, v1, :cond_7

    add-int v32, v16, v21

    add-int/lit8 v32, v32, 0x1

    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v32

    if-eqz v32, :cond_e

    .line 3226
    :cond_7
    move/from16 v0, v29

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3227
    add-int v32, v29, v9

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3228
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v21

    invoke-virtual {v0, v1, v6, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    .line 3215
    .end local v7    # "child":Landroid/view/View;
    :cond_8
    :goto_6
    add-int/lit8 v21, v21, 0x1

    goto :goto_5

    .line 3159
    .end local v4    # "adapter":Landroid/widget/ListAdapter;
    .end local v5    # "areAllItemsSelectable":Z
    .end local v6    # "bounds":Landroid/graphics/Rect;
    .end local v8    # "count":I
    .end local v10    # "drawDividers":Z
    .end local v11    # "drawOverscrollFooter":Z
    .end local v12    # "drawOverscrollHeader":Z
    .end local v13    # "effectivePaddingLeft":I
    .end local v14    # "effectivePaddingRight":I
    .end local v15    # "fillForMissingDividers":Z
    .end local v16    # "first":I
    .end local v17    # "footerDividers":Z
    .end local v18    # "footerLimit":I
    .end local v19    # "headerCount":I
    .end local v20    # "headerDividers":Z
    .end local v21    # "i":I
    .end local v22    # "itemCount":I
    .end local v24    # "listRight":I
    .end local v28    # "paint":Landroid/graphics/Paint;
    .end local v29    # "right":I
    .end local v30    # "scrollX":I
    :cond_9
    const/4 v12, 0x0

    goto/16 :goto_0

    .line 3160
    .restart local v12    # "drawOverscrollHeader":Z
    :cond_a
    const/4 v11, 0x0

    goto/16 :goto_1

    .line 3161
    .restart local v11    # "drawOverscrollFooter":Z
    :cond_b
    const/4 v10, 0x0

    goto/16 :goto_2

    .line 3182
    .restart local v4    # "adapter":Landroid/widget/ListAdapter;
    .restart local v5    # "areAllItemsSelectable":Z
    .restart local v6    # "bounds":Landroid/graphics/Rect;
    .restart local v8    # "count":I
    .restart local v10    # "drawDividers":Z
    .restart local v16    # "first":I
    .restart local v17    # "footerDividers":Z
    .restart local v18    # "footerLimit":I
    .restart local v19    # "headerCount":I
    .restart local v20    # "headerDividers":Z
    .restart local v22    # "itemCount":I
    :cond_c
    const/4 v15, 0x0

    goto/16 :goto_3

    .line 3208
    .restart local v13    # "effectivePaddingLeft":I
    .restart local v14    # "effectivePaddingRight":I
    .restart local v15    # "fillForMissingDividers":Z
    .restart local v24    # "listRight":I
    .restart local v28    # "paint":Landroid/graphics/Paint;
    .restart local v29    # "right":I
    .restart local v30    # "scrollX":I
    :cond_d
    if-eqz v10, :cond_3

    .line 3209
    const/16 v32, 0x0

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3210
    neg-int v0, v9

    move/from16 v32, v0

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3211
    const/16 v32, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v32

    invoke-virtual {v0, v1, v6, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    goto/16 :goto_4

    .line 3229
    .restart local v7    # "child":Landroid/view/View;
    .restart local v21    # "i":I
    :cond_e
    if-eqz v15, :cond_8

    .line 3230
    move/from16 v0, v29

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3231
    add-int v32, v29, v9

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3232
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_6

    .line 3238
    .end local v7    # "child":Landroid/view/View;
    :cond_f
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v32

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v33

    add-int v25, v32, v33

    .line 3239
    .local v25, "overFooterBottom":I
    if-eqz v11, :cond_10

    add-int v32, v16, v8

    move/from16 v0, v32

    move/from16 v1, v22

    if-ne v0, v1, :cond_10

    move/from16 v0, v25

    move/from16 v1, v29

    if-le v0, v1, :cond_10

    .line 3241
    move/from16 v0, v29

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3242
    move/from16 v0, v25

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3243
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 3298
    .end local v4    # "adapter":Landroid/widget/ListAdapter;
    .end local v5    # "areAllItemsSelectable":Z
    .end local v6    # "bounds":Landroid/graphics/Rect;
    .end local v8    # "count":I
    .end local v13    # "effectivePaddingLeft":I
    .end local v14    # "effectivePaddingRight":I
    .end local v15    # "fillForMissingDividers":Z
    .end local v16    # "first":I
    .end local v17    # "footerDividers":Z
    .end local v18    # "footerLimit":I
    .end local v19    # "headerCount":I
    .end local v20    # "headerDividers":Z
    .end local v21    # "i":I
    .end local v22    # "itemCount":I
    .end local v24    # "listRight":I
    .end local v25    # "overFooterBottom":I
    .end local v28    # "paint":Landroid/graphics/Paint;
    .end local v29    # "right":I
    .end local v30    # "scrollX":I
    :cond_10
    :goto_7
    invoke-super/range {p0 .. p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 3299
    return-void

    .line 3248
    .restart local v4    # "adapter":Landroid/widget/ListAdapter;
    .restart local v5    # "areAllItemsSelectable":Z
    .restart local v6    # "bounds":Landroid/graphics/Rect;
    .restart local v8    # "count":I
    .restart local v13    # "effectivePaddingLeft":I
    .restart local v14    # "effectivePaddingRight":I
    .restart local v15    # "fillForMissingDividers":Z
    .restart local v16    # "first":I
    .restart local v17    # "footerDividers":Z
    .restart local v18    # "footerLimit":I
    .restart local v19    # "headerCount":I
    .restart local v20    # "headerDividers":Z
    .restart local v22    # "itemCount":I
    .restart local v24    # "listRight":I
    .restart local v28    # "paint":Landroid/graphics/Paint;
    :cond_11
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v30

    .line 3250
    .restart local v30    # "scrollX":I
    if-lez v8, :cond_12

    if-eqz v12, :cond_12

    .line 3251
    move/from16 v0, v30

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3252
    const/16 v32, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v32

    invoke-virtual/range {v32 .. v32}, Landroid/view/View;->getLeft()I

    move-result v32

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3253
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v27

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    .line 3256
    :cond_12
    if-eqz v12, :cond_17

    const/16 v31, 0x1

    .line 3257
    .local v31, "start":I
    :goto_8
    move/from16 v21, v31

    .restart local v21    # "i":I
    :goto_9
    move/from16 v0, v21

    if-ge v0, v8, :cond_19

    .line 3258
    if-nez v20, :cond_13

    add-int v32, v16, v21

    move/from16 v0, v32

    move/from16 v1, v19

    if-lt v0, v1, :cond_16

    :cond_13
    if-nez v17, :cond_14

    add-int v32, v16, v21

    move/from16 v0, v32

    move/from16 v1, v18

    if-ge v0, v1, :cond_16

    .line 3260
    :cond_14
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 3261
    .restart local v7    # "child":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getLeft()I

    move-result v23

    .line 3263
    .local v23, "left":I
    move/from16 v0, v23

    if-le v0, v13, :cond_16

    .line 3264
    if-nez v5, :cond_15

    add-int v32, v16, v21

    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v32

    if-eqz v32, :cond_18

    add-int/lit8 v32, v8, -0x1

    move/from16 v0, v21

    move/from16 v1, v32

    if-eq v0, v1, :cond_15

    add-int v32, v16, v21

    add-int/lit8 v32, v32, 0x1

    move/from16 v0, v32

    invoke-interface {v4, v0}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v32

    if-eqz v32, :cond_18

    .line 3266
    :cond_15
    sub-int v32, v23, v9

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3267
    move/from16 v0, v23

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3272
    add-int/lit8 v32, v21, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v32

    invoke-virtual {v0, v1, v6, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    .line 3257
    .end local v7    # "child":Landroid/view/View;
    .end local v23    # "left":I
    :cond_16
    :goto_a
    add-int/lit8 v21, v21, 0x1

    goto :goto_9

    .line 3256
    .end local v21    # "i":I
    .end local v31    # "start":I
    :cond_17
    const/16 v31, 0x0

    goto :goto_8

    .line 3273
    .restart local v7    # "child":Landroid/view/View;
    .restart local v21    # "i":I
    .restart local v23    # "left":I
    .restart local v31    # "start":I
    :cond_18
    if-eqz v15, :cond_16

    .line 3274
    sub-int v32, v23, v9

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3275
    move/from16 v0, v23

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3276
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_a

    .line 3282
    .end local v7    # "child":Landroid/view/View;
    .end local v23    # "left":I
    :cond_19
    if-lez v8, :cond_10

    if-lez v30, :cond_10

    .line 3283
    if-eqz v11, :cond_1a

    .line 3284
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v3

    .line 3285
    .local v3, "absListRight":I
    iput v3, v6, Landroid/graphics/Rect;->left:I

    .line 3286
    add-int v32, v3, v30

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3287
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V

    goto/16 :goto_7

    .line 3288
    .end local v3    # "absListRight":I
    :cond_1a
    if-eqz v10, :cond_10

    .line 3289
    move/from16 v0, v24

    iput v0, v6, Landroid/graphics/Rect;->left:I

    .line 3290
    add-int v32, v24, v9

    move/from16 v0, v32

    iput v0, v6, Landroid/graphics/Rect;->right:I

    .line 3291
    const/16 v32, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v32

    invoke-virtual {v0, v1, v6, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V

    goto/16 :goto_7
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2109
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v1

    .line 2110
    .local v1, "handled":Z
    if-nez v1, :cond_0

    .line 2112
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v0

    .line 2113
    .local v0, "focused":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    .line 2116
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {p0, v2, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    .line 2119
    .end local v0    # "focused":Landroid/view/View;
    :cond_0
    return v1
.end method

.method protected drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "drawingTime"    # J

    .prologue
    .line 3303
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->drawChild(Landroid/graphics/Canvas;Landroid/view/View;J)Z

    move-result v0

    .line 3304
    .local v0, "more":Z
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingActive:Z

    if-eqz v1, :cond_0

    .line 3305
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingActive:Z

    .line 3307
    :cond_0
    return v0
.end method

.method drawDivider(Landroid/graphics/Canvas;Landroid/graphics/Rect;I)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bounds"    # Landroid/graphics/Rect;
    .param p3, "childIndex"    # I

    .prologue
    .line 3323
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 3329
    :goto_0
    return-void

    .line 3325
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 3327
    .local v0, "divider":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3328
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method drawOverscrollFooter(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 3133
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v1

    .line 3135
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 3136
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 3138
    iget v2, p3, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v3

    .line 3139
    .local v0, "span":I
    if-ge v0, v1, :cond_0

    .line 3140
    iget v2, p3, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v1

    iput v2, p3, Landroid/graphics/Rect;->right:I

    .line 3143
    :cond_0
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3144
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3146
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 3147
    return-void
.end method

.method drawOverscrollHeader(Landroid/graphics/Canvas;Landroid/graphics/drawable/Drawable;Landroid/graphics/Rect;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;
    .param p3, "bounds"    # Landroid/graphics/Rect;

    .prologue
    .line 3116
    invoke-virtual {p2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v1

    .line 3118
    .local v1, "width":I
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 3119
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 3121
    iget v2, p3, Landroid/graphics/Rect;->right:I

    iget v3, p3, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v3

    .line 3122
    .local v0, "span":I
    if-ge v0, v1, :cond_0

    .line 3123
    iget v2, p3, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v1

    iput v2, p3, Landroid/graphics/Rect;->left:I

    .line 3126
    :cond_0
    invoke-virtual {p2, p3}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 3127
    invoke-virtual {p2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 3129
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 3130
    return-void
.end method

.method protected fillGap(Z)V
    .locals 6
    .param p1, "down"    # Z

    .prologue
    .line 622
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    .line 623
    .local v0, "count":I
    if-eqz p1, :cond_1

    .line 624
    const/4 v1, 0x0

    .line 628
    .local v1, "paddingLeft":I
    if-lez v0, :cond_0

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    add-int v3, v4, v5

    .line 629
    .local v3, "startOffset":I
    :goto_0
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v4, v0

    invoke-direct {p0, v4, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillRight(II)Landroid/view/View;

    .line 630
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooWide(I)V

    .line 640
    .end local v1    # "paddingLeft":I
    :goto_1
    return-void

    .end local v3    # "startOffset":I
    .restart local v1    # "paddingLeft":I
    :cond_0
    move v3, v1

    .line 628
    goto :goto_0

    .line 632
    .end local v1    # "paddingLeft":I
    :cond_1
    const/4 v2, 0x0

    .line 636
    .local v2, "paddingRight":I
    if-lez v0, :cond_2

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v4

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    sub-int v3, v4, v5

    .line 637
    .restart local v3    # "startOffset":I
    :goto_2
    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/lit8 v4, v4, -0x1

    invoke-direct {p0, v4, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    .line 638
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->correctTooSmall(I)V

    goto :goto_1

    .line 636
    .end local v3    # "startOffset":I
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v4

    sub-int v3, v4, v2

    goto :goto_2
.end method

.method protected findMotionCol(I)I
    .locals 4
    .param p1, "x"    # I

    .prologue
    .line 1318
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    .line 1319
    .local v0, "childCount":I
    if-lez v0, :cond_3

    .line 1320
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v3, :cond_1

    .line 1321
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 1322
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1323
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v3

    if-gt p1, v3, :cond_0

    .line 1324
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v3, v1

    .line 1336
    .end local v1    # "i":I
    .end local v2    # "v":Landroid/view/View;
    :goto_1
    return v3

    .line 1321
    .restart local v1    # "i":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1328
    .end local v1    # "i":I
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    add-int/lit8 v1, v0, -0x1

    .restart local v1    # "i":I
    :goto_2
    if-ltz v1, :cond_3

    .line 1329
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1330
    .restart local v2    # "v":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt p1, v3, :cond_2

    .line 1331
    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v3, v1

    goto :goto_1

    .line 1328
    :cond_2
    add-int/lit8 v1, v1, -0x1

    goto :goto_2

    .line 1336
    .end local v1    # "i":I
    .end local v2    # "v":Landroid/view/View;
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method fullScroll(I)Z
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v4, 0x1

    .line 2330
    const/4 v0, 0x0

    .line 2331
    .local v0, "moved":Z
    const/16 v2, 0x21

    if-ne p1, v2, :cond_3

    .line 2332
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-eqz v2, :cond_1

    .line 2333
    const/4 v2, 0x0

    invoke-virtual {p0, v2, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 2334
    .local v1, "position":I
    if-ltz v1, :cond_0

    .line 2335
    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 2336
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionInt(I)V

    .line 2337
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V

    .line 2339
    :cond_0
    const/4 v0, 0x1

    .line 2353
    .end local v1    # "position":I
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->awakenScrollBars()Z

    move-result v2

    if-nez v2, :cond_2

    .line 2354
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->awakenScrollBars()Z

    .line 2355
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 2358
    :cond_2
    return v0

    .line 2341
    :cond_3
    const/16 v2, 0x82

    if-ne p1, v2, :cond_1

    .line 2342
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v3, v3, -0x1

    if-ge v2, v3, :cond_1

    .line 2343
    iget v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v1

    .line 2344
    .restart local v1    # "position":I
    if-ltz v1, :cond_4

    .line 2345
    const/4 v2, 0x3

    iput v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 2346
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionInt(I)V

    .line 2347
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V

    .line 2349
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getCheckItemIds()[J
    .locals 11
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 3550
    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v8}, Landroid/widget/ListAdapter;->hasStableIds()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 3551
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getCheckedItemIds()[J

    move-result-object v5

    .line 3580
    :cond_0
    :goto_0
    return-object v5

    .line 3556
    :cond_1
    iget v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mChoiceMode:I

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v8, :cond_3

    .line 3557
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCheckStates:Landroid/util/SparseBooleanArray;

    .line 3558
    .local v7, "states":Landroid/util/SparseBooleanArray;
    invoke-virtual {v7}, Landroid/util/SparseBooleanArray;->size()I

    move-result v3

    .line 3559
    .local v3, "count":I
    new-array v5, v3, [J

    .line 3560
    .local v5, "ids":[J
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3562
    .local v0, "adapter":Landroid/widget/ListAdapter;
    const/4 v1, 0x0

    .line 3563
    .local v1, "checkedCount":I
    const/4 v4, 0x0

    .local v4, "i":I
    move v2, v1

    .end local v1    # "checkedCount":I
    .local v2, "checkedCount":I
    :goto_1
    if-ge v4, v3, :cond_2

    .line 3564
    invoke-virtual {v7, v4}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 3565
    add-int/lit8 v1, v2, 0x1

    .end local v2    # "checkedCount":I
    .restart local v1    # "checkedCount":I
    invoke-virtual {v7, v4}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    invoke-interface {v0, v8}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v8

    aput-wide v8, v5, v2

    .line 3563
    :goto_2
    add-int/lit8 v4, v4, 0x1

    move v2, v1

    .end local v1    # "checkedCount":I
    .restart local v2    # "checkedCount":I
    goto :goto_1

    .line 3571
    :cond_2
    if-eq v2, v3, :cond_0

    .line 3574
    new-array v6, v2, [J

    .line 3575
    .local v6, "result":[J
    invoke-static {v5, v10, v6, v10, v2}, Ljava/lang/System;->arraycopy([JI[JII)V

    move-object v5, v6

    .line 3577
    goto :goto_0

    .line 3580
    .end local v0    # "adapter":Landroid/widget/ListAdapter;
    .end local v2    # "checkedCount":I
    .end local v3    # "count":I
    .end local v4    # "i":I
    .end local v5    # "ids":[J
    .end local v6    # "result":[J
    .end local v7    # "states":Landroid/util/SparseBooleanArray;
    :cond_3
    new-array v5, v10, [J

    goto :goto_0

    .restart local v0    # "adapter":Landroid/widget/ListAdapter;
    .restart local v2    # "checkedCount":I
    .restart local v3    # "count":I
    .restart local v4    # "i":I
    .restart local v5    # "ids":[J
    .restart local v7    # "states":Landroid/util/SparseBooleanArray;
    :cond_4
    move v1, v2

    .end local v2    # "checkedCount":I
    .restart local v1    # "checkedCount":I
    goto :goto_2
.end method

.method public getDivider()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getDividerWidth()I
    .locals 1

    .prologue
    .line 3365
    iget v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    return v0
.end method

.method public getFooterViewsCount()I
    .locals 1

    .prologue
    .line 382
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemsCanFocus()Z
    .locals 1

    .prologue
    .line 3080
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    return v0
.end method

.method public getMaxScrollAmount()I
    .locals 3

    .prologue
    .line 196
    const v0, 0x3ea8f5c3    # 0.33f

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    return v0
.end method

.method public getOverscrollFooter()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 3447
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getOverscrollHeader()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 3428
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public isOpaque()Z
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 3085
    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mCachingActive:Z

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsCacheColorOpaque:Z

    if-eqz v5, :cond_0

    iget-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerIsOpaque:Z

    if-nez v5, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isOpaque()Z

    move-result v5

    if-eqz v5, :cond_4

    :cond_1
    const/4 v4, 0x1

    .line 3086
    .local v4, "retValue":Z
    :goto_0
    if-eqz v4, :cond_3

    .line 3088
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v2, v5, Landroid/graphics/Rect;->left:I

    .line 3089
    .local v2, "listLeft":I
    :goto_1
    invoke-virtual {p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 3090
    .local v0, "first":Landroid/view/View;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    if-le v5, v2, :cond_6

    :cond_2
    move v4, v6

    .line 3099
    .end local v0    # "first":Landroid/view/View;
    .end local v2    # "listLeft":I
    .end local v4    # "retValue":Z
    :cond_3
    :goto_2
    return v4

    :cond_4
    move v4, v6

    .line 3085
    goto :goto_0

    .line 3088
    .restart local v4    # "retValue":Z
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingLeft()I

    move-result v2

    goto :goto_1

    .line 3093
    .restart local v0    # "first":Landroid/view/View;
    .restart local v2    # "listLeft":I
    :cond_6
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v7

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    :goto_3
    sub-int v3, v7, v5

    .line 3094
    .local v3, "listRight":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 3095
    .local v1, "last":Landroid/view/View;
    if-eqz v1, :cond_7

    invoke-virtual {v1}, Landroid/view/View;->getRight()I

    move-result v5

    if-ge v5, v3, :cond_3

    :cond_7
    move v4, v6

    .line 3096
    goto :goto_2

    .line 3093
    .end local v1    # "last":Landroid/view/View;
    .end local v3    # "listRight":I
    :cond_8
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingRight()I

    move-result v5

    goto :goto_3
.end method

.method protected layoutChildren()V
    .locals 26

    .prologue
    .line 1494
    move-object/from16 v0, p0

    iget-boolean v8, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    .line 1495
    .local v8, "blockLayoutRequests":Z
    if-nez v8, :cond_0

    .line 1496
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    .line 1502
    :try_start_0
    invoke-super/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->layoutChildren()V

    .line 1504
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 1506
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_1

    .line 1507
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resetList()V

    .line 1508
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1770
    if-nez v8, :cond_0

    .line 1771
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    .line 1774
    :cond_0
    :goto_0
    return-void

    .line 1512
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v6, v2, Landroid/graphics/Rect;->left:I

    .line 1513
    .local v6, "childrenLeft":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getRight()I

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getLeft()I

    move-result v24

    sub-int v2, v2, v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    sub-int v7, v2, v24

    .line 1515
    .local v7, "childrenRight":I
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v10

    .line 1516
    .local v10, "childCount":I
    const/16 v19, 0x0

    .line 1517
    .local v19, "index":I
    const/4 v5, 0x0

    .line 1520
    .local v5, "delta":I
    const/4 v3, 0x0

    .line 1521
    .local v3, "oldSel":Landroid/view/View;
    const/16 v20, 0x0

    .line 1522
    .local v20, "oldFirst":Landroid/view/View;
    const/4 v4, 0x0

    .line 1524
    .local v4, "newSel":Landroid/view/View;
    const/4 v14, 0x0

    .line 1531
    .local v14, "focusLayoutRestoreView":Landroid/view/View;
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_0

    .line 1546
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    move/from16 v24, v0

    sub-int v19, v2, v24

    .line 1547
    if-ltz v19, :cond_2

    move/from16 v0, v19

    if-ge v0, v10, :cond_2

    .line 1548
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1552
    :cond_2
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    .line 1554
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    if-ltz v2, :cond_3

    .line 1555
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    move/from16 v24, v0

    sub-int v5, v2, v24

    .line 1559
    :cond_3
    add-int v2, v19, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1562
    :cond_4
    :goto_1
    :pswitch_0
    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataChanged:Z

    .line 1563
    .local v11, "dataChanged":Z
    if-eqz v11, :cond_5

    .line 1564
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->handleDataChanged()V

    .line 1569
    :cond_5
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-nez v2, :cond_6

    .line 1570
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resetList()V

    .line 1571
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1770
    if-nez v8, :cond_0

    .line 1771
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    goto/16 :goto_0

    .line 1533
    .end local v11    # "dataChanged":Z
    :pswitch_1
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    move/from16 v24, v0

    sub-int v19, v2, v24

    .line 1534
    if-ltz v19, :cond_4

    move/from16 v0, v19

    if-ge v0, v10, :cond_4

    .line 1535
    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    goto :goto_1

    .line 1573
    .restart local v11    # "dataChanged":Z
    :cond_6
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v24, v0

    invoke-interface/range {v24 .. v24}, Landroid/widget/ListAdapter;->getCount()I

    move-result v24

    move/from16 v0, v24

    if-eq v2, v0, :cond_8

    .line 1574
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v24, Ljava/lang/StringBuilder;

    invoke-direct/range {v24 .. v24}, Ljava/lang/StringBuilder;-><init>()V

    const-string v25, "The content of the adapter has changed but ListView did not receive a notification. Make sure the content of your adapter is not modified from a background thread, but only from the UI thread. [in ListView("

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getId()I

    move-result v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ", "

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ") with Adapter("

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v24

    const-string v25, ")]"

    invoke-virtual/range {v24 .. v25}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v0, v24

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1770
    .end local v3    # "oldSel":Landroid/view/View;
    .end local v4    # "newSel":Landroid/view/View;
    .end local v5    # "delta":I
    .end local v6    # "childrenLeft":I
    .end local v7    # "childrenRight":I
    .end local v10    # "childCount":I
    .end local v11    # "dataChanged":Z
    .end local v14    # "focusLayoutRestoreView":Landroid/view/View;
    .end local v19    # "index":I
    .end local v20    # "oldFirst":Landroid/view/View;
    :catchall_0
    move-exception v2

    if-nez v8, :cond_7

    .line 1771
    const/16 v24, 0x0

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    :cond_7
    throw v2

    .line 1581
    .restart local v3    # "oldSel":Landroid/view/View;
    .restart local v4    # "newSel":Landroid/view/View;
    .restart local v5    # "delta":I
    .restart local v6    # "childrenLeft":I
    .restart local v7    # "childrenRight":I
    .restart local v10    # "childCount":I
    .restart local v11    # "dataChanged":Z
    .restart local v14    # "focusLayoutRestoreView":Landroid/view/View;
    .restart local v19    # "index":I
    .restart local v20    # "oldFirst":Landroid/view/View;
    :cond_8
    :try_start_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectedPositionInt(I)V

    .line 1585
    move-object/from16 v0, p0

    iget v12, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 1586
    .local v12, "firstPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    move-object/from16 v22, v0

    .line 1589
    .local v22, "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    const/4 v13, 0x0

    .line 1593
    .local v13, "focusLayoutRestoreDirectChild":Landroid/view/View;
    if-eqz v11, :cond_9

    .line 1594
    const/16 v18, 0x0

    .local v18, "i":I
    :goto_2
    move/from16 v0, v18

    if-ge v0, v10, :cond_a

    .line 1595
    move-object/from16 v0, p0

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    add-int v24, v12, v18

    move-object/from16 v0, v22

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 1594
    add-int/lit8 v18, v18, 0x1

    goto :goto_2

    .line 1598
    .end local v18    # "i":I
    :cond_9
    move-object/from16 v0, v22

    invoke-virtual {v0, v10, v12}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->fillActiveViews(II)V

    .line 1605
    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v17

    .line 1606
    .local v17, "focusedChild":Landroid/view/View;
    if-eqz v17, :cond_d

    .line 1611
    if-eqz v11, :cond_b

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isDirectChildHeaderOrFooter(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1612
    :cond_b
    move-object/from16 v13, v17

    .line 1614
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->findFocus()Landroid/view/View;

    move-result-object v14

    .line 1615
    if-eqz v14, :cond_c

    .line 1617
    invoke-virtual {v14}, Landroid/view/View;->onStartTemporaryDetach()V

    .line 1620
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestFocus()Z

    .line 1635
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->detachAllViewsFromParent()V

    .line 1636
    invoke-virtual/range {v22 .. v22}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->removeSkippedScrap()V

    .line 1638
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    packed-switch v2, :pswitch_data_1

    .line 1665
    if-nez v10, :cond_17

    .line 1666
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-nez v2, :cond_16

    .line 1667
    const/4 v2, 0x0

    const/16 v24, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v21

    .line 1668
    .local v21, "position":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectedPositionInt(I)V

    .line 1669
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillFromLeft(I)Landroid/view/View;

    move-result-object v23

    .line 1688
    .end local v6    # "childrenLeft":I
    .end local v21    # "position":I
    .local v23, "sel":Landroid/view/View;
    :goto_3
    invoke-virtual/range {v22 .. v22}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->scrapActiveViews()V

    .line 1690
    if-eqz v23, :cond_1f

    .line 1693
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    if-eqz v2, :cond_1e

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_1e

    .line 1694
    move-object/from16 v0, v23

    if-ne v0, v13, :cond_e

    if-eqz v14, :cond_e

    invoke-virtual {v14}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-nez v2, :cond_f

    :cond_e
    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->requestFocus()Z

    move-result v2

    if-eqz v2, :cond_1c

    :cond_f
    const/4 v15, 0x1

    .line 1697
    .local v15, "focusWasTaken":Z
    :goto_4
    if-nez v15, :cond_1d

    .line 1701
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v16

    .line 1702
    .local v16, "focused":Landroid/view/View;
    if-eqz v16, :cond_10

    .line 1703
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->clearFocus()V

    .line 1705
    :cond_10
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionSelector(ILandroid/view/View;)V

    .line 1713
    .end local v15    # "focusWasTaken":Z
    .end local v16    # "focused":Landroid/view/View;
    :goto_5
    invoke-virtual/range {v23 .. v23}, Landroid/view/View;->getLeft()I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedLeft:I

    .line 1748
    :cond_11
    :goto_6
    if-eqz v14, :cond_12

    invoke-virtual {v14}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    if-eqz v2, :cond_12

    .line 1750
    invoke-virtual {v14}, Landroid/view/View;->onFinishTemporaryDetach()V

    .line 1753
    :cond_12
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 1754
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataChanged:Z

    .line 1755
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    if-eqz v2, :cond_13

    .line 1756
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->post(Ljava/lang/Runnable;)Z

    .line 1757
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScrollAfterLayout:Ljava/lang/Runnable;

    .line 1759
    :cond_13
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNeedSync:Z

    .line 1760
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setNextSelectedPositionInt(I)V

    .line 1762
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->updateScrollIndicators()V

    .line 1764
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-lez v2, :cond_14

    .line 1765
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkSelectionChanged()V

    .line 1768
    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1770
    if-nez v8, :cond_0

    .line 1771
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mBlockLayoutRequests:Z

    goto/16 :goto_0

    .line 1640
    .end local v23    # "sel":Landroid/view/View;
    .restart local v6    # "childrenLeft":I
    :pswitch_2
    if-eqz v4, :cond_15

    .line 1641
    :try_start_4
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillFromSelection(III)Landroid/view/View;

    move-result-object v23

    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1643
    .end local v23    # "sel":Landroid/view/View;
    :cond_15
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillFromMiddle(II)Landroid/view/View;

    move-result-object v23

    .line 1645
    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1647
    .end local v23    # "sel":Landroid/view/View;
    :pswitch_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSyncPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSpecificLeft:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v23

    .line 1648
    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1650
    .end local v23    # "sel":Landroid/view/View;
    :pswitch_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    move-result-object v23

    .line 1651
    .restart local v23    # "sel":Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    goto/16 :goto_3

    .line 1654
    .end local v23    # "sel":Landroid/view/View;
    :pswitch_5
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 1655
    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillFromLeft(I)Landroid/view/View;

    move-result-object v23

    .line 1656
    .restart local v23    # "sel":Landroid/view/View;
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->adjustViewsLeftOrRight()V

    goto/16 :goto_3

    .line 1659
    .end local v23    # "sel":Landroid/view/View;
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->reconcileSelectedPosition()I

    move-result v2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSpecificLeft:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-direct {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v23

    .line 1660
    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .end local v23    # "sel":Landroid/view/View;
    :pswitch_7
    move-object/from16 v2, p0

    .line 1662
    invoke-direct/range {v2 .. v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->moveSelection(Landroid/view/View;Landroid/view/View;III)Landroid/view/View;

    move-result-object v23

    .line 1663
    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1671
    .end local v23    # "sel":Landroid/view/View;
    :cond_16
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    const/16 v24, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v21

    .line 1672
    .restart local v21    # "position":I
    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectedPositionInt(I)V

    .line 1673
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillLeft(II)Landroid/view/View;

    move-result-object v23

    .line 1674
    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1676
    .end local v21    # "position":I
    .end local v23    # "sel":Landroid/view/View;
    :cond_17
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-ltz v2, :cond_19

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v2, v0, :cond_19

    .line 1677
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-nez v3, :cond_18

    .end local v6    # "childrenLeft":I
    :goto_7
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v23

    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .end local v23    # "sel":Landroid/view/View;
    .restart local v6    # "childrenLeft":I
    :cond_18
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v6

    goto :goto_7

    .line 1678
    :cond_19
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-ge v2, v0, :cond_1b

    .line 1679
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    if-nez v20, :cond_1a

    .end local v6    # "childrenLeft":I
    :goto_8
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v23

    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .end local v23    # "sel":Landroid/view/View;
    .restart local v6    # "childrenLeft":I
    :cond_1a
    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v6

    goto :goto_8

    .line 1681
    :cond_1b
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->fillSpecific(II)Landroid/view/View;

    move-result-object v23

    .restart local v23    # "sel":Landroid/view/View;
    goto/16 :goto_3

    .line 1694
    .end local v6    # "childrenLeft":I
    :cond_1c
    const/4 v15, 0x0

    goto/16 :goto_4

    .line 1707
    .restart local v15    # "focusWasTaken":Z
    :cond_1d
    const/4 v2, 0x0

    move-object/from16 v0, v23

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 1708
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V

    goto/16 :goto_5

    .line 1711
    .end local v15    # "focusWasTaken":Z
    :cond_1e
    const/4 v2, -0x1

    move-object/from16 v0, p0

    move-object/from16 v1, v23

    invoke-virtual {v0, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionSelector(ILandroid/view/View;)V

    goto/16 :goto_5

    .line 1715
    :cond_1f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTouchMode:I

    if-lez v2, :cond_21

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTouchMode:I

    const/16 v24, 0x3

    move/from16 v0, v24

    if-ge v2, v0, :cond_21

    .line 1716
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMotionPosition:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    move/from16 v24, v0

    sub-int v2, v2, v24

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v9

    .line 1717
    .local v9, "child":Landroid/view/View;
    if-eqz v9, :cond_20

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMotionPosition:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionSelector(ILandroid/view/View;)V

    .line 1725
    .end local v9    # "child":Landroid/view/View;
    :cond_20
    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_11

    if-eqz v14, :cond_11

    .line 1726
    invoke-virtual {v14}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_6

    .line 1719
    :cond_21
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedLeft:I

    .line 1720
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->setEmpty()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_9

    .line 1531
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 1638
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_6
        :pswitch_3
        :pswitch_7
    .end packed-switch
.end method

.method protected lookForSelectablePosition(IZ)I
    .locals 4
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    const/4 v2, -0x1

    .line 2056
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 2057
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2083
    :cond_0
    :goto_0
    return v2

    .line 2061
    :cond_1
    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    .line 2062
    .local v1, "count":I
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAreAllItemsSelectable:Z

    if-nez v3, :cond_4

    .line 2063
    if-eqz p2, :cond_2

    .line 2064
    const/4 v3, 0x0

    invoke-static {v3, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 2065
    :goto_1
    if-ge p1, v1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2066
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    .line 2069
    :cond_2
    add-int/lit8 v3, v1, -0x1

    invoke-static {p1, v3}, Ljava/lang/Math;->min(II)I

    move-result p1

    .line 2070
    :goto_2
    if-ltz p1, :cond_3

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2071
    add-int/lit8 p1, p1, -0x1

    goto :goto_2

    .line 2075
    :cond_3
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 2078
    goto :goto_0

    .line 2080
    :cond_4
    if-ltz p1, :cond_0

    if-ge p1, v1, :cond_0

    move v2, p1

    .line 2083
    goto :goto_0
.end method

.method public measureChild(Landroid/view/View;)[I
    .locals 4
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 1170
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureItem(Landroid/view/View;)V

    .line 1172
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    .line 1173
    .local v1, "w":I
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    .line 1175
    .local v0, "h":I
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    aput v0, v2, v3

    return-object v2
.end method

.method final measureWidthOfChildren(IIIII)I
    .locals 13
    .param p1, "heightMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxWidth"    # I
    .param p5, "disallowPartialChildPosition"    # I

    .prologue
    .line 1210
    const-string v10, "HListView"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "measureWidthOfChildren, from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " to "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move/from16 v0, p3

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1212
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 1213
    .local v1, "adapter":Landroid/widget/ListAdapter;
    if-nez v1, :cond_1

    .line 1214
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    add-int v6, v10, v11

    .line 1266
    :cond_0
    :goto_0
    return v6

    .line 1218
    :cond_1
    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    add-int v9, v10, v11

    .line 1219
    .local v9, "returnedWidth":I
    iget v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    if-lez v10, :cond_6

    iget-object v10, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v10, :cond_6

    iget v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 1222
    .local v3, "dividerWidth":I
    :goto_1
    const/4 v6, 0x0

    .line 1227
    .local v6, "prevWidthWithoutPartialChild":I
    const/4 v10, -0x1

    move/from16 v0, p3

    if-ne v0, v10, :cond_2

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v10

    add-int/lit8 p3, v10, -0x1

    .line 1228
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .line 1229
    .local v7, "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->recycleOnMeasure()Z

    move-result v8

    .line 1230
    .local v8, "recyle":Z
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    .line 1232
    .local v5, "isScrap":[Z
    move v4, p2

    .local v4, "i":I
    :goto_2
    move/from16 v0, p3

    if-gt v4, v0, :cond_9

    .line 1233
    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v2

    .line 1235
    .local v2, "child":Landroid/view/View;
    invoke-direct {p0, v2, v4, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureScrapChildWidth(Landroid/view/View;II)V

    .line 1237
    if-lez v4, :cond_3

    .line 1239
    add-int/2addr v9, v3

    .line 1243
    :cond_3
    if-eqz v8, :cond_4

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v10

    check-cast v10, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    iget v10, v10, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v7, v10}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 1244
    const/4 v10, -0x1

    invoke-virtual {v7, v2, v10}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 1247
    :cond_4
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v9, v10

    .line 1249
    move/from16 v0, p4

    if-lt v9, v0, :cond_7

    .line 1252
    if-ltz p5, :cond_5

    move/from16 v0, p5

    if-le v4, v0, :cond_5

    if-lez v6, :cond_5

    move/from16 v0, p4

    if-ne v9, v0, :cond_0

    :cond_5
    move/from16 v6, p4

    goto :goto_0

    .line 1219
    .end local v2    # "child":Landroid/view/View;
    .end local v3    # "dividerWidth":I
    .end local v4    # "i":I
    .end local v5    # "isScrap":[Z
    .end local v6    # "prevWidthWithoutPartialChild":I
    .end local v7    # "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    .end local v8    # "recyle":Z
    :cond_6
    const/4 v3, 0x0

    goto :goto_1

    .line 1259
    .restart local v2    # "child":Landroid/view/View;
    .restart local v3    # "dividerWidth":I
    .restart local v4    # "i":I
    .restart local v5    # "isScrap":[Z
    .restart local v6    # "prevWidthWithoutPartialChild":I
    .restart local v7    # "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    .restart local v8    # "recyle":Z
    :cond_7
    if-ltz p5, :cond_8

    move/from16 v0, p5

    if-lt v4, v0, :cond_8

    .line 1260
    move v6, v9

    .line 1232
    :cond_8
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .end local v2    # "child":Landroid/view/View;
    :cond_9
    move v6, v9

    .line 1266
    goto :goto_0
.end method

.method final measureWithLargeChildren(IIIIII)[I
    .locals 17
    .param p1, "heightMeasureSpec"    # I
    .param p2, "startPosition"    # I
    .param p3, "endPosition"    # I
    .param p4, "maxWidth"    # I
    .param p5, "maxHeight"    # I
    .param p6, "disallowPartialChildPosition"    # I

    .prologue
    .line 1270
    const-string v13, "HListView"

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "measureWithLargeChildren, from "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p2

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " to "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move/from16 v0, p3

    invoke-virtual {v14, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1272
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 1273
    .local v2, "adapter":Landroid/widget/ListAdapter;
    if-nez v2, :cond_0

    .line 1274
    const/4 v13, 0x2

    new-array v13, v13, [I

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v16, v0

    add-int v15, v15, v16

    aput v15, v13, v14

    const/4 v14, 0x1

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v15, v15, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    move/from16 v16, v0

    add-int v15, v15, v16

    aput v15, v13, v14

    .line 1312
    :goto_0
    return-object v13

    .line 1278
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    add-int v12, v13, v14

    .line 1279
    .local v12, "returnedWidth":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    add-int v11, v13, v14

    .line 1281
    .local v11, "returnedHeight":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    if-lez v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    if-eqz v13, :cond_3

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 1283
    .local v6, "dividerWidth":I
    :goto_1
    const/4 v5, 0x0

    .line 1284
    .local v5, "childWidth":I
    const/4 v4, 0x0

    .line 1290
    .local v4, "childHeight":I
    const/4 v13, -0x1

    move/from16 v0, p3

    if-ne v0, v13, :cond_1

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v13

    add-int/lit8 p3, v13, -0x1

    .line 1291
    :cond_1
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    .line 1292
    .local v9, "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->recycleOnMeasure()Z

    move-result v10

    .line 1293
    .local v10, "recyle":Z
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    .line 1295
    .local v8, "isScrap":[Z
    move/from16 v7, p2

    .local v7, "i":I
    :goto_2
    move/from16 v0, p3

    if-gt v7, v0, :cond_4

    .line 1296
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v3

    .line 1298
    .local v3, "child":Landroid/view/View;
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v0, v3, v7, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureScrapChildWidth(Landroid/view/View;II)V

    .line 1301
    if-eqz v10, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    iget v13, v13, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v9, v13}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1302
    const/4 v13, -0x1

    invoke-virtual {v9, v3, v13}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 1305
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    add-int/2addr v13, v6

    invoke-static {v5, v13}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 1306
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    invoke-static {v4, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1295
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    .line 1281
    .end local v3    # "child":Landroid/view/View;
    .end local v4    # "childHeight":I
    .end local v5    # "childWidth":I
    .end local v6    # "dividerWidth":I
    .end local v7    # "i":I
    .end local v8    # "isScrap":[Z
    .end local v9    # "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    .end local v10    # "recyle":Z
    :cond_3
    const/4 v6, 0x0

    goto :goto_1

    .line 1309
    .restart local v4    # "childHeight":I
    .restart local v5    # "childWidth":I
    .restart local v6    # "dividerWidth":I
    .restart local v7    # "i":I
    .restart local v8    # "isScrap":[Z
    .restart local v9    # "recycleBin":Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;
    .restart local v10    # "recyle":Z
    :cond_4
    add-int/2addr v12, v5

    .line 1310
    add-int/2addr v11, v4

    .line 1312
    const/4 v13, 0x2

    new-array v13, v13, [I

    const/4 v14, 0x0

    move/from16 v0, p4

    invoke-static {v12, v0}, Ljava/lang/Math;->min(II)I

    move-result v15

    aput v15, v13, v14

    const/4 v14, 0x1

    move/from16 v0, p5

    invoke-static {v11, v0}, Ljava/lang/Math;->min(II)I

    move-result v15

    aput v15, v13, v14

    goto/16 :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 3508
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onFinishInflate()V

    .line 3510
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v0

    .line 3511
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 3512
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 3513
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->addHeaderView(Landroid/view/View;)V

    .line 3512
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3515
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->removeAllViews()V

    .line 3517
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 15
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 3452
    invoke-super/range {p0 .. p3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 3454
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 3455
    .local v2, "adapter":Landroid/widget/ListAdapter;
    const/4 v5, -0x1

    .line 3456
    .local v5, "closetChildIndex":I
    const/4 v4, 0x0

    .line 3457
    .local v4, "closestChildLeft":I
    if-eqz v2, :cond_3

    if-eqz p1, :cond_3

    if-eqz p3, :cond_3

    .line 3458
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v12

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollY()I

    move-result v13

    move-object/from16 v0, p3

    invoke-virtual {v0, v12, v13}, Landroid/graphics/Rect;->offset(II)V

    .line 3462
    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v12

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v13

    iget v14, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v13, v14

    if-ge v12, v13, :cond_0

    .line 3463
    const/4 v12, 0x0

    iput v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 3464
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->layoutChildren()V

    .line 3469
    :cond_0
    iget-object v11, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mTempRect:Landroid/graphics/Rect;

    .line 3470
    .local v11, "otherRect":Landroid/graphics/Rect;
    const v9, 0x7fffffff

    .line 3471
    .local v9, "minDistance":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v3

    .line 3472
    .local v3, "childCount":I
    iget v7, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    .line 3474
    .local v7, "firstPosition":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v3, :cond_3

    .line 3476
    add-int v12, v7, v8

    invoke-interface {v2, v12}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v12

    if-nez v12, :cond_2

    .line 3474
    :cond_1
    :goto_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 3480
    :cond_2
    invoke-virtual {p0, v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    .line 3481
    .local v10, "other":Landroid/view/View;
    invoke-virtual {v10, v11}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 3482
    invoke-virtual {p0, v10, v11}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 3483
    move-object/from16 v0, p3

    move/from16 v1, p2

    invoke-static {v0, v11, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v6

    .line 3485
    .local v6, "distance":I
    if-ge v6, v9, :cond_1

    .line 3486
    move v9, v6

    .line 3487
    move v5, v8

    .line 3488
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v4

    goto :goto_1

    .line 3493
    .end local v3    # "childCount":I
    .end local v6    # "distance":I
    .end local v7    # "firstPosition":I
    .end local v8    # "i":I
    .end local v9    # "minDistance":I
    .end local v10    # "other":Landroid/view/View;
    .end local v11    # "otherRect":Landroid/graphics/Rect;
    :cond_3
    if-ltz v5, :cond_4

    .line 3494
    iget v12, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    add-int/2addr v12, v5

    invoke-virtual {p0, v12, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionFromLeft(II)V

    .line 3498
    :goto_2
    return-void

    .line 3496
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestLayout()V

    goto :goto_2
.end method

.method public onGlobalLayout()V
    .locals 0

    .prologue
    .line 3597
    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .prologue
    .line 3585
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 3586
    const-class v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 3587
    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 3592
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 3593
    const-class v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 3594
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2124
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "repeatCount"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2129
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 2134
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 21
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1089
    invoke-super/range {p0 .. p2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onMeasure(II)V

    .line 1091
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v20

    .line 1092
    .local v20, "widthMode":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v18

    .line 1093
    .local v18, "heightMode":I
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v6

    .line 1094
    .local v6, "widthSize":I
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 1096
    .local v7, "heightSize":I
    const/16 v17, 0x0

    .line 1097
    .local v17, "childWidth":I
    const/4 v15, 0x0

    .line 1098
    .local v15, "childHeight":I
    const/16 v16, 0x0

    .line 1100
    .local v16, "childState":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_6

    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    .line 1102
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-lez v2, :cond_2

    if-eqz v20, :cond_0

    if-nez v18, :cond_2

    .line 1103
    :cond_0
    const-string v2, "HListView"

    const-string v3, "let\'s measure a scrap child"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1105
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsScrap:[Z

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v14

    .line 1107
    .local v14, "child":Landroid/view/View;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v14, v2, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureScrapChildWidth(Landroid/view/View;II)V

    .line 1109
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    .line 1110
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredHeight()I

    move-result v15

    .line 1112
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_1

    .line 1113
    invoke-virtual {v14}, Landroid/view/View;->getMeasuredState()I

    move-result v2

    move/from16 v0, v16

    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->combineMeasuredStates(II)I

    move-result v16

    .line 1116
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->recycleOnMeasure()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v14}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;

    iget v2, v2, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$LayoutParams;->viewType:I

    invoke-virtual {v3, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    const/4 v3, -0x1

    invoke-virtual {v2, v14, v3}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->addScrapView(Landroid/view/View;I)V

    .line 1121
    .end local v14    # "child":Landroid/view/View;
    :cond_2
    if-nez v18, :cond_7

    .line 1122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v2, v3

    add-int/2addr v2, v15

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalScrollbarHeight()I

    move-result v3

    add-int v7, v2, v3

    .line 1135
    :cond_3
    :goto_1
    if-nez v20, :cond_4

    .line 1136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v3

    add-int v2, v2, v17

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    add-int v6, v2, v3

    .line 1139
    :cond_4
    const/high16 v2, -0x80000000

    move/from16 v0, v20

    if-ne v0, v2, :cond_5

    .line 1140
    const/4 v10, 0x0

    const/4 v11, -0x1

    const/4 v13, -0x1

    move-object/from16 v8, p0

    move/from16 v9, p2

    move v12, v6

    invoke-virtual/range {v8 .. v13}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureWidthOfChildren(IIIII)I

    move-result v6

    .line 1145
    :cond_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v6, v7}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setMeasuredDimension(II)V

    .line 1146
    move/from16 v0, p2

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeightMeasureSpec:I

    .line 1147
    return-void

    .line 1100
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    goto/16 :goto_0

    .line 1123
    :cond_7
    const/high16 v2, -0x80000000

    move/from16 v0, v18

    if-ne v0, v2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-lez v2, :cond_8

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMeasureWithChild:I

    const/4 v3, -0x1

    if-le v2, v3, :cond_8

    .line 1126
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMeasureWithChild:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mMeasureWithChild:I

    const/4 v8, -0x1

    move-object/from16 v2, p0

    move/from16 v3, p2

    invoke-virtual/range {v2 .. v8}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->measureWithLargeChildren(IIIIII)[I

    move-result-object v19

    .line 1127
    .local v19, "result":[I
    const/4 v2, 0x1

    aget v7, v19, v2

    .line 1129
    goto :goto_1

    .line 1130
    .end local v19    # "result":[I
    :cond_8
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    .line 1131
    const/high16 v2, -0x1000000

    and-int v2, v2, v16

    or-int/2addr v7, v2

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 7
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1070
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v5

    if-lez v5, :cond_1

    .line 1071
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getFocusedChild()Landroid/view/View;

    move-result-object v2

    .line 1072
    .local v2, "focusedChild":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 1073
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFirstPosition:I

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->indexOfChild(Landroid/view/View;)I

    move-result v6

    add-int v0, v5, v6

    .line 1074
    .local v0, "childPosition":I
    invoke-virtual {v2}, Landroid/view/View;->getRight()I

    move-result v1

    .line 1075
    .local v1, "childRight":I
    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingLeft()I

    move-result v6

    sub-int v6, p1, v6

    sub-int v6, v1, v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 1076
    .local v4, "offset":I
    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int v3, v5, v4

    .line 1077
    .local v3, "left":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFocusSelector:Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

    if-nez v5, :cond_0

    .line 1078
    new-instance v5, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/HListView;Lcom/sec/samsung/gallery/hlistview/widget/HListView$1;)V

    iput-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFocusSelector:Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

    .line 1080
    :cond_0
    iget-object v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFocusSelector:Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

    invoke-virtual {v5, v0, v3}, Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;->setup(II)Lcom/sec/samsung/gallery/hlistview/widget/HListView$FocusSelector;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->post(Ljava/lang/Runnable;)Z

    .line 1083
    .end local v0    # "childPosition":I
    .end local v1    # "childRight":I
    .end local v2    # "focusedChild":Landroid/view/View;
    .end local v3    # "left":I
    .end local v4    # "offset":I
    :cond_1
    invoke-super {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->onSizeChanged(IIII)V

    .line 1084
    return-void
.end method

.method pageScroll(I)Z
    .locals 8
    .param p1, "direction"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 2284
    const/4 v1, -0x1

    .line 2285
    .local v1, "nextPage":I
    const/4 v0, 0x0

    .line 2287
    .local v0, "down":Z
    const/16 v5, 0x21

    if-ne p1, v5, :cond_4

    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v5, v5, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 2293
    :cond_0
    :goto_0
    if-ltz v1, :cond_5

    .line 2294
    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 2295
    .local v2, "position":I
    if-ltz v2, :cond_5

    .line 2296
    const/4 v4, 0x4

    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 2297
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    add-int/2addr v4, v5

    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSpecificLeft:I

    .line 2299
    if-eqz v0, :cond_1

    iget v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v5

    sub-int/2addr v4, v5

    if-le v2, v4, :cond_1

    .line 2300
    const/4 v4, 0x3

    iput v4, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 2303
    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 2304
    iput v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 2307
    :cond_2
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionInt(I)V

    .line 2308
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invokeOnItemScrollListener()V

    .line 2309
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->awakenScrollBars()Z

    move-result v4

    if-nez v4, :cond_3

    .line 2310
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 2317
    .end local v2    # "position":I
    :cond_3
    :goto_1
    return v3

    .line 2288
    :cond_4
    const/16 v5, 0x82

    if-ne p1, v5, :cond_0

    .line 2289
    iget v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v5, v5, -0x1

    iget v6, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v7

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, -0x1

    invoke-static {v5, v6}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 2290
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v3, v4

    .line 2317
    goto :goto_1
.end method

.method protected recycleOnMeasure()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
        category = "list"
    .end annotation

    .prologue
    .line 1184
    const/4 v0, 0x1

    return v0
.end method

.method public removeFooterView(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 393
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 394
    const/4 v0, 0x0

    .line 395
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;->removeFooter(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 397
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;->onChanged()V

    .line 399
    :cond_0
    const/4 v0, 0x1

    .line 401
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V

    .line 404
    .end local v0    # "result":Z
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public removeHeaderView(Landroid/view/View;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 306
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_2

    .line 307
    const/4 v0, 0x0

    .line 308
    .local v0, "result":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;->removeHeader(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 309
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 310
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;->onChanged()V

    .line 312
    :cond_0
    const/4 v0, 0x1

    .line 314
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-direct {p0, p1, v1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->removeFixedViewInfo(Landroid/view/View;Ljava/util/ArrayList;)V

    .line 317
    .end local v0    # "result":Z
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 15
    .param p1, "child"    # Landroid/view/View;
    .param p2, "rect"    # Landroid/graphics/Rect;
    .param p3, "immediate"    # Z

    .prologue
    .line 541
    move-object/from16 v0, p2

    iget v8, v0, Landroid/graphics/Rect;->left:I

    .line 544
    .local v8, "rectLeftWithinChild":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getLeft()I

    move-result v13

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Rect;->offset(II)V

    .line 545
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollX()I

    move-result v13

    neg-int v13, v13

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getScrollY()I

    move-result v14

    neg-int v14, v14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13, v14}, Landroid/graphics/Rect;->offset(II)V

    .line 547
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getWidth()I

    move-result v12

    .line 548
    .local v12, "width":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v6

    .line 549
    .local v6, "listUnfadedLeft":I
    add-int v7, v6, v12

    .line 550
    .local v7, "listUnfadedRight":I
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getHorizontalFadingEdgeLength()I

    move-result v4

    .line 552
    .local v4, "fadingEdge":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->showingLeftFadingEdge()Z

    move-result v13

    if-eqz v13, :cond_1

    .line 554
    iget v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    if-gtz v13, :cond_0

    if-le v8, v4, :cond_1

    .line 555
    :cond_0
    add-int/2addr v6, v4

    .line 559
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildCount()I

    move-result v1

    .line 560
    .local v1, "childCount":I
    add-int/lit8 v13, v1, -0x1

    invoke-virtual {p0, v13}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getRight()I

    move-result v9

    .line 562
    .local v9, "rightOfRightChild":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->showingRightFadingEdge()Z

    move-result v13

    if-eqz v13, :cond_3

    .line 564
    iget v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    iget v14, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v14, v14, -0x1

    if-lt v13, v14, :cond_2

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    sub-int v14, v9, v4

    if-ge v13, v14, :cond_3

    .line 566
    :cond_2
    sub-int/2addr v7, v4

    .line 570
    :cond_3
    const/4 v11, 0x0

    .line 572
    .local v11, "scrollXDelta":I
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    if-le v13, v7, :cond_7

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->left:I

    if-le v13, v6, :cond_7

    .line 577
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v13

    if-le v13, v12, :cond_6

    .line 579
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v13, v6

    add-int/2addr v11, v13

    .line 586
    :goto_0
    sub-int v3, v9, v7

    .line 587
    .local v3, "distanceToRight":I
    invoke-static {v11, v3}, Ljava/lang/Math;->min(II)I

    move-result v11

    .line 607
    .end local v3    # "distanceToRight":I
    :cond_4
    :goto_1
    if-eqz v11, :cond_9

    const/4 v10, 0x1

    .line 608
    .local v10, "scroll":Z
    :goto_2
    if-eqz v10, :cond_5

    .line 609
    neg-int v13, v11

    invoke-direct {p0, v13}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->scrollListItemsBy(I)V

    .line 610
    const/4 v13, -0x1

    move-object/from16 v0, p1

    invoke-virtual {p0, v13, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->positionSelector(ILandroid/view/View;)V

    .line 611
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTop()I

    move-result v13

    iput v13, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedLeft:I

    .line 612
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 614
    :cond_5
    return v10

    .line 582
    .end local v10    # "scroll":Z
    :cond_6
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v13, v7

    add-int/2addr v11, v13

    goto :goto_0

    .line 588
    :cond_7
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->left:I

    if-ge v13, v6, :cond_4

    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    if-ge v13, v7, :cond_4

    .line 593
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Rect;->width()I

    move-result v13

    if-le v13, v12, :cond_8

    .line 595
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->right:I

    sub-int v13, v7, v13

    sub-int/2addr v11, v13

    .line 602
    :goto_3
    const/4 v13, 0x0

    invoke-virtual {p0, v13}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getChildAt(I)Landroid/view/View;

    move-result-object v13

    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v5

    .line 603
    .local v5, "left":I
    sub-int v2, v5, v6

    .line 604
    .local v2, "deltaToLeft":I
    invoke-static {v11, v2}, Ljava/lang/Math;->max(II)I

    move-result v11

    goto :goto_1

    .line 598
    .end local v2    # "deltaToLeft":I
    .end local v5    # "left":I
    :cond_8
    move-object/from16 v0, p2

    iget v13, v0, Landroid/graphics/Rect;->left:I

    sub-int v13, v6, v13

    sub-int/2addr v11, v13

    goto :goto_3

    .line 607
    :cond_9
    const/4 v10, 0x0

    goto :goto_2
.end method

.method protected resetList()V
    .locals 1

    .prologue
    .line 494
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->clearRecycledState(Ljava/util/ArrayList;)V

    .line 495
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->clearRecycledState(Ljava/util/ArrayList;)V

    .line 497
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->resetList()V

    .line 499
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 500
    return-void
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 78
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 6
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 434
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    if-eqz v1, :cond_0

    .line 435
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 438
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->resetList()V

    .line 439
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->clear()V

    .line 441
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-gtz v1, :cond_1

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_3

    .line 442
    :cond_1
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterViewInfos:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v3, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HeaderViewListAdapter;-><init>(Ljava/util/ArrayList;Ljava/util/ArrayList;Landroid/widget/ListAdapter;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 447
    :goto_0
    const/4 v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOldSelectedPosition:I

    .line 448
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOldSelectedColId:J

    .line 451
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 453
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_5

    .line 454
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->areAllItemsEnabled()Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAreAllItemsSelectable:Z

    .line 455
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOldItemCount:I

    .line 456
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    .line 457
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkFocus()V

    .line 459
    new-instance v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    .line 460
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDataSetObserver:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;

    invoke-interface {v1, v2}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 462
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mRecycler:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;

    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$RecycleBin;->setViewTypeCount(I)V

    .line 465
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mStackFromRight:Z

    if-eqz v1, :cond_4

    .line 466
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v4}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v0

    .line 470
    .local v0, "position":I
    :goto_1
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectedPositionInt(I)V

    .line 471
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setNextSelectedPositionInt(I)V

    .line 473
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemCount:I

    if-nez v1, :cond_2

    .line 475
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkSelectionChanged()V

    .line 484
    .end local v0    # "position":I
    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestLayout()V

    .line 485
    return-void

    .line 444
    :cond_3
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    goto :goto_0

    .line 468
    :cond_4
    invoke-virtual {p0, v4, v5}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result v0

    .restart local v0    # "position":I
    goto :goto_1

    .line 478
    .end local v0    # "position":I
    :cond_5
    iput-boolean v5, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAreAllItemsSelectable:Z

    .line 479
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkFocus()V

    .line 481
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->checkSelectionChanged()V

    goto :goto_2
.end method

.method public setCacheColorHint(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 3104
    ushr-int/lit8 v1, p1, 0x18

    const/16 v2, 0xff

    if-ne v1, v2, :cond_2

    const/4 v0, 0x1

    .line 3105
    .local v0, "opaque":Z
    :goto_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mIsCacheColorOpaque:Z

    .line 3106
    if-eqz v0, :cond_1

    .line 3107
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    .line 3108
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    .line 3110
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 3112
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setCacheColorHint(I)V

    .line 3113
    return-void

    .line 3104
    .end local v0    # "opaque":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setDivider(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "divider"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x0

    .line 3348
    const-string v1, "HListView"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDivider: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3350
    if-eqz p1, :cond_2

    .line 3351
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 3355
    :goto_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDivider:Landroid/graphics/drawable/Drawable;

    .line 3356
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getOpacity()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerIsOpaque:Z

    .line 3357
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestLayout()V

    .line 3358
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3359
    return-void

    .line 3353
    :cond_2
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    goto :goto_0
.end method

.method public setDividerWidth(I)V
    .locals 3
    .param p1, "width"    # I

    .prologue
    .line 3376
    const-string v0, "HListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDividerWidth: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3377
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mDividerWidth:I

    .line 3378
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestLayout()V

    .line 3379
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3380
    return-void
.end method

.method public setFooterDividersEnabled(Z)V
    .locals 0
    .param p1, "footerDividersEnabled"    # Z

    .prologue
    .line 3406
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mFooterDividersEnabled:Z

    .line 3407
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3408
    return-void
.end method

.method public setHeaderDividersEnabled(Z)V
    .locals 0
    .param p1, "headerDividersEnabled"    # Z

    .prologue
    .line 3392
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderDividersEnabled:Z

    .line 3393
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3394
    return-void
.end method

.method public setItemsCanFocus(Z)V
    .locals 1
    .param p1, "itemsCanFocus"    # Z

    .prologue
    .line 3070
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mItemsCanFocus:Z

    .line 3071
    if-nez p1, :cond_0

    .line 3072
    const/high16 v0, 0x60000

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setDescendantFocusability(I)V

    .line 3074
    :cond_0
    return-void
.end method

.method public setOverscrollFooter(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "footer"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 3439
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollFooter:Landroid/graphics/drawable/Drawable;

    .line 3440
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3441
    return-void
.end method

.method public setOverscrollHeader(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "header"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 3418
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mOverScrollHeader:Landroid/graphics/drawable/Drawable;

    .line 3419
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->getScrollX()I

    move-result v0

    if-gez v0, :cond_0

    .line 3420
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->invalidate()V

    .line 3422
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1970
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelectionFromLeft(II)V

    .line 1971
    return-void
.end method

.method public setSelectionAfterHeaderView()V
    .locals 2

    .prologue
    .line 2091
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mHeaderViewInfos:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 2092
    .local v0, "count":I
    if-lez v0, :cond_0

    .line 2093
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    .line 2104
    :goto_0
    return-void

    .line 2097
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    .line 2098
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setSelection(I)V

    goto :goto_0

    .line 2100
    :cond_1
    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNextSelectedPosition:I

    .line 2101
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    goto :goto_0
.end method

.method public setSelectionFromLeft(II)V
    .locals 2
    .param p1, "position"    # I
    .param p2, "x"    # I

    .prologue
    .line 1983
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    .line 2010
    :cond_0
    :goto_0
    return-void

    .line 1987
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1988
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->lookForSelectablePosition(IZ)I

    move-result p1

    .line 1989
    if-ltz p1, :cond_2

    .line 1990
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setNextSelectedPositionInt(I)V

    .line 1996
    :cond_2
    :goto_1
    if-ltz p1, :cond_0

    .line 1997
    const/4 v0, 0x4

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mLayoutMode:I

    .line 1998
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSpecificLeft:I

    .line 2000
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mNeedSync:Z

    if-eqz v0, :cond_3

    .line 2001
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSyncPosition:I

    .line 2002
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, p1}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSyncColId:J

    .line 2005
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v0, :cond_4

    .line 2006
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2008
    :cond_4
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->requestLayout()V

    goto :goto_0

    .line 1993
    :cond_5
    iput p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mResurrectToPosition:I

    goto :goto_1
.end method

.method public setSelectionInt(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 2020
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->setNextSelectedPositionInt(I)V

    .line 2021
    const/4 v0, 0x0

    .line 2023
    .local v0, "awakeScrollbars":Z
    iget v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mSelectedPosition:I

    .line 2025
    .local v1, "selectedPosition":I
    if-ltz v1, :cond_0

    .line 2026
    add-int/lit8 v2, v1, -0x1

    if-ne p1, v2, :cond_3

    .line 2027
    const/4 v0, 0x1

    .line 2033
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    if-eqz v2, :cond_1

    .line 2034
    iget-object v2, p0, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->mPositionScroller:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$PositionScroller;->stop()V

    .line 2037
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->layoutChildren()V

    .line 2039
    if-eqz v0, :cond_2

    .line 2040
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/hlistview/widget/HListView;->awakenScrollBars()Z

    .line 2042
    :cond_2
    return-void

    .line 2028
    :cond_3
    add-int/lit8 v2, v1, 0x1

    if-ne p1, v2, :cond_0

    .line 2029
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public smoothScrollByOffset(I)V
    .locals 0
    .param p1, "offset"    # I

    .prologue
    .line 904
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollByOffset(I)V

    .line 905
    return-void
.end method

.method public smoothScrollToPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 893
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->smoothScrollToPosition(I)V

    .line 894
    return-void
.end method
