.class Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;
.super Ljava/lang/Object;
.source "RecommendDropboxDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/16 v4, 0xa

    const/4 v3, 0x4

    const/4 v2, 0x3

    const/4 v1, 0x2

    .line 90
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ge v0, v1, :cond_1

    .line 92
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setNextTime(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$300(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->getDropboxFlag()I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$400(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v1, :cond_2

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v3}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    goto :goto_0

    .line 97
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->getDropboxFlag()I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$400(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 98
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    goto :goto_0

    .line 100
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v2, :cond_5

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->getDropboxFlag()I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$400(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v1, :cond_4

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    goto :goto_0

    .line 103
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->getDropboxFlag()I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$400(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    goto :goto_0

    .line 106
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->mTriggerType:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$000(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;)I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment$2;->this$1:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog$RecommendDropboxDialogFragment;->this$0:Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;

    const/16 v1, 0x14

    # invokes: Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->setDropboxFlag(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;->access$100(Lcom/sec/samsung/gallery/view/common/RecommendDropboxDialog;I)V

    goto :goto_0
.end method
