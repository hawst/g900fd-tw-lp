.class public final Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;
.super Ljava/lang/Object;
.source "ShowBarManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/ShowBarManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "SHOWBAR_TYPE"
.end annotation


# static fields
.field public static TYPE_COVER:I

.field public static TYPE_DEFAULT:I

.field public static TYPE_MOREINFO:I

.field public static TYPE_MOTION_HELP:I

.field public static TYPE_MOTION_PREVIEW:I

.field public static TYPE_MOTION_TUTORIAL:I

.field public static TYPE_SLIDESHOW_SETTING:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_DEFAULT:I

    .line 18
    const/4 v0, 0x1

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_COVER:I

    .line 19
    const/4 v0, 0x2

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_HELP:I

    .line 20
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_PREVIEW:I

    .line 21
    const/4 v0, 0x4

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOTION_TUTORIAL:I

    .line 22
    const/4 v0, 0x5

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_MOREINFO:I

    .line 23
    const/4 v0, 0x6

    sput v0, Lcom/sec/samsung/gallery/view/detailview/ShowBarManager$SHOWBAR_TYPE;->TYPE_SLIDESHOW_SETTING:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
