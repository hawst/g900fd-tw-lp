.class public Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;
.super Lcom/android/internal/app/ChooserActivity;
.source "GalleryChooserActivity.java"


# static fields
.field private static final MSG_PRIVATE_MODE_CHANGE:I = 0x0

.field private static final PERSONAL_PAGE_OFF:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

.field private static final PERSONAL_PAGE_ON:Ljava/lang/String; = "com.samsung.android.intent.action.PRIVATE_MODE_ON"

.field private static final REMOTE_SHARE_SERVICE_ID:I = 0x2

.field private static final TAG:Ljava/lang/String;

.field private static mIsRemoteShareServiceDownloaded:Z

.field private static mIsRemoteShareServiceDownloadedChecked:Z


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

.field private privateMode:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    const-class v0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->TAG:Ljava/lang/String;

    .line 43
    sput-boolean v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloaded:Z

    .line 44
    sput-boolean v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloadedChecked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/android/internal/app/ChooserActivity;-><init>()V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z

    .line 48
    new-instance v0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$1;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$1;-><init>(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mHandler:Landroid/os/Handler;

    .line 105
    new-instance v0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity$2;-><init>(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z

    return v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z

    return p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method public static isRemoteShareServiceDownloaded(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 70
    sget-boolean v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloadedChecked:Z

    if-nez v1, :cond_0

    .line 71
    const/4 v1, 0x2

    invoke-static {p0, v1}, Lcom/samsung/android/coreapps/sdk/easysignup/EasySignUpManager;->getSupportedFeatures(Landroid/content/Context;I)I

    move-result v0

    .line 72
    .local v0, "retVal":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 73
    const/4 v1, 0x0

    sput-boolean v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloaded:Z

    .line 77
    :goto_0
    sput-boolean v2, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloadedChecked:Z

    .line 79
    .end local v0    # "retVal":I
    :cond_0
    sget-boolean v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloaded:Z

    return v1

    .line 75
    .restart local v0    # "retVal":I
    :cond_1
    sput-boolean v2, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mIsRemoteShareServiceDownloaded:Z

    goto :goto_0
.end method

.method private registerReceiver()V
    .locals 2

    .prologue
    .line 83
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 84
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 87
    new-instance v0, Landroid/content/IntentFilter;

    .end local v0    # "filter":Landroid/content/IntentFilter;
    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 88
    .restart local v0    # "filter":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.android.intent.action.PRIVATE_MODE_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 89
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 90
    return-void
.end method

.method private unregisterReceiver()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 93
    sget-object v1, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->TAG:Ljava/lang/String;

    const-string/jumbo v2, "unregisterReceiver"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 96
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->mPrivateModeReceiver:Landroid/content/BroadcastReceiver;

    throw v1
.end method


# virtual methods
.method public exitShareVia()V
    .locals 0

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->finish()V

    .line 127
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 64
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isSecretModeOn()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->privateMode:Z

    .line 65
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->registerReceiver()V

    .line 66
    invoke-super {p0, p1}, Lcom/android/internal/app/ChooserActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    return-void
.end method

.method protected onDestroy()V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/GalleryChooserActivity;->unregisterReceiver()V

    .line 122
    invoke-super {p0}, Lcom/android/internal/app/ChooserActivity;->onDestroy()V

    .line 123
    return-void
.end method
