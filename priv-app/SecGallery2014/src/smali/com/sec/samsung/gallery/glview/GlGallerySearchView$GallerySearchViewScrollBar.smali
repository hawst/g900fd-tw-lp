.class public Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;
.super Lcom/sec/samsung/gallery/glview/GlScrollBar;
.source "GlGallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GallerySearchViewScrollBar"
.end annotation


# static fields
.field public static final SCROLL_MOVEMENT_HORIZONTAL:I = 0x1

.field public static final SCROLL_MOVEMENT_VERTICAL:I


# instance fields
.field private mMinFaceLength:F

.field private mScrollBarDefZ:F

.field private mScrollBarHeightSpace:F

.field private mScrollBarTop:F

.field private mScrollBarWidthSpace:F

.field private mTrackLength:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V
    .locals 1
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F

    .prologue
    .line 1289
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .line 1290
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 1282
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarDefZ:F

    .line 1287
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mMinFaceLength:F

    .line 1292
    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V
    .locals 7
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "textureWidth"    # I

    .prologue
    .line 1295
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;-><init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 1297
    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V
    .locals 1
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;
    .param p3, "width"    # F
    .param p4, "height"    # F
    .param p5, "textureWidth"    # I
    .param p6, "textureHeight"    # I

    .prologue
    .line 1299
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    .line 1301
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    .line 1282
    const/high16 v0, -0x3bb80000    # -800.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarDefZ:F

    .line 1287
    const/high16 v0, 0x42480000    # 50.0f

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mMinFaceLength:F

    .line 1302
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->initScrollBar()V

    .line 1304
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mWidthSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarWidthSpace:F

    .line 1305
    iget v0, p2, Lcom/sec/android/gallery3d/glcore/GlLayer;->mHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarHeightSpace:F

    .line 1307
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarHeightSpace:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    .line 1308
    return-void
.end method

.method private getHeight(F)F
    .locals 8
    .param p1, "range"    # F

    .prologue
    const/4 v4, 0x0

    .line 1365
    sget v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->THRESHOLD:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarHeightSpace:F

    mul-float v0, v5, v6

    .line 1366
    .local v0, "T":F
    cmpl-float v5, p1, v0

    if-lez v5, :cond_1

    .line 1367
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mMinFaceLength:F

    .line 1375
    :cond_0
    :goto_0
    return v3

    .line 1370
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mMinFaceLength:F

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    sub-float/2addr v5, v6

    div-float v1, v5, v0

    .line 1371
    .local v1, "a":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    .line 1372
    .local v2, "b":F
    mul-float v5, v1, p1

    add-float v3, v5, v2

    .line 1373
    .local v3, "result":F
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    sub-float/2addr v5, v3

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    const/high16 v7, 0x41400000    # 12.0f

    div-float/2addr v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 1374
    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    const/high16 v6, 0x41200000    # 10.0f

    div-float/2addr v5, v6

    sub-float/2addr v3, v5

    .line 1375
    :cond_2
    cmpl-float v5, p1, v4

    if-gtz v5, :cond_0

    move v3, v4

    goto :goto_0
.end method


# virtual methods
.method protected initScrollBar()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1311
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setEmptyFill(Z)V

    .line 1312
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseWhiteTheme:Z

    if-eqz v0, :cond_0

    .line 1313
    const v0, -0x5e5c5b

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setEmptyFillColor(I)V

    .line 1314
    const v0, -0x706d6a

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setBorderColor(I)V

    .line 1319
    :goto_0
    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setBorderWidth(F)V

    .line 1320
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setBorderVisible(Z)V

    .line 1321
    return-void

    .line 1316
    :cond_0
    const v0, -0xca9889

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setEmptyFillColor(I)V

    .line 1317
    const v0, -0xbb8372

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setBorderColor(I)V

    goto :goto_0
.end method

.method public reset(FFF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F
    .param p3, "top"    # F

    .prologue
    .line 1326
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarWidthSpace:F

    .line 1327
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarHeightSpace:F

    .line 1328
    iput p3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarTop:F

    .line 1329
    return-void
.end method

.method public setDefZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 1333
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarDefZ:F

    .line 1334
    return-void
.end method

.method public setMaxHeight(F)V
    .locals 0
    .param p1, "maxHeight"    # F

    .prologue
    .line 1356
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mTrackLength:F

    .line 1357
    return-void
.end method

.method public setMinHeight(F)V
    .locals 0
    .param p1, "minHeight"    # F

    .prologue
    .line 1361
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mMinFaceLength:F

    .line 1362
    return-void
.end method

.method public update(FFF)V
    .locals 10
    .param p1, "scroll"    # F
    .param p2, "min"    # F
    .param p3, "max"    # F

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    .line 1338
    sub-float v5, p3, p2

    .line 1339
    .local v5, "range":F
    invoke-direct {p0, v5}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->getHeight(F)F

    move-result v4

    .line 1340
    .local v4, "height":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewScrollbarWidth()I

    move-result v8

    int-to-float v8, v8

    mul-float v3, v7, v8

    .line 1341
    .local v3, "glFaceThickness":F
    invoke-virtual {p0, v3, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setSize(FF)V

    .line 1343
    const/4 v7, 0x0

    cmpg-float v7, v5, v7

    if-gtz v7, :cond_0

    .line 1352
    :goto_0
    return-void

    .line 1347
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarHeightSpace:F

    .line 1348
    .local v0, "H":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarTop:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mBottom:F

    add-float/2addr v7, v8

    add-float/2addr v7, v4

    sub-float/2addr v7, v0

    div-float v1, v7, v5

    .line 1349
    .local v1, "a":F
    neg-float v7, v0

    div-float/2addr v7, v9

    div-float v8, v4, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mBottom:F

    add-float/2addr v7, v8

    mul-float v8, p3, v1

    sub-float v2, v7, v8

    .line 1350
    .local v2, "b":F
    mul-float v7, v1, p1

    add-float v6, v7, v2

    .line 1351
    .local v6, "y":F
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarWidthSpace:F

    div-float/2addr v7, v9

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mWidth:F

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->mScrollBarDefZ:F

    const/high16 v9, 0x3f800000    # 1.0f

    add-float/2addr v8, v9

    invoke-virtual {p0, v7, v6, v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$GallerySearchViewScrollBar;->setPos(FFF)V

    goto :goto_0
.end method
