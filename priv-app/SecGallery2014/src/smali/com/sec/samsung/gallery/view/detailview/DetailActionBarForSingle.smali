.class Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;
.super Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;
.source "DetailActionBarForSingle.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

.field private mIsKnox2Enabled:Z

.field private final mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 2
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarSkeleton;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 26
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mIsKnox2Enabled:Z

    .line 30
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setHomeButtonEnabled(Z)V

    .line 32
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayHomeAsUpEnabled(Z)V

    .line 34
    :cond_0
    new-instance v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .line 36
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, v1, p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    .line 37
    invoke-static {p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isKnoxEnabledStateOfKnox2(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mIsKnox2Enabled:Z

    .line 38
    return-void
.end method

.method private getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 3

    .prologue
    .line 106
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v1

    if-lez v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 108
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v1, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    if-eqz v1, :cond_0

    .line 109
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 112
    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSetAsMenuVisibility(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 116
    if-nez p1, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const v1, 0x7f0f029a

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 122
    .local v0, "item":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 123
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v1, :cond_0

    .line 124
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method


# virtual methods
.method public isManualRotateIconEnabled()Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->isRotationMenuItemEnabled()Z

    move-result v0

    .line 134
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 3
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v2, 0x7f0f0298

    .line 55
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 56
    .local v0, "inflater":Landroid/view/MenuInflater;
    const v1, 0x7f120013

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 57
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v1, :cond_0

    .line 58
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->setRotationMenuItem(Landroid/view/MenuItem;)V

    .line 62
    :goto_0
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->setCameraButtonVisibility(Landroid/view/Menu;Z)V

    .line 63
    return-void

    .line 60
    :cond_0
    invoke-interface {p1, v2}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method protected onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 3
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    const/4 v2, 0x1

    .line 86
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 88
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Lcom/sec/android/gallery3d/data/MediaItem;Z)V

    goto :goto_0

    .line 91
    :pswitch_1
    sget-object v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->TAG:Ljava/lang/String;

    const-string v1, "change player selected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_CHANGE_PLAYER:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->notifyObservers(Ljava/lang/Object;)V

    goto :goto_0

    .line 95
    :pswitch_2
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->handleScreenRotation()V

    goto :goto_0

    .line 100
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mEditModeHelper:Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    goto :goto_0

    .line 86
    :pswitch_data_0
    .packed-switch 0x7f0f0298
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 42
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onPause()V

    .line 45
    :cond_0
    return-void
.end method

.method protected onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v4, 0x7f0f0299

    .line 67
    const-wide/16 v2, 0x0

    .line 68
    .local v2, "supportedOperations":J
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->getCurrentPhoto()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 69
    .local v0, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 70
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v2

    .line 72
    :cond_0
    invoke-static {p1, v2, v3}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->updateMenuOperation(Landroid/view/Menu;J)V

    .line 73
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->setSetAsMenuVisibility(Landroid/view/Menu;)V

    .line 75
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHideChangePlayerText:Z

    if-eqz v1, :cond_1

    .line 76
    const/4 v1, 0x2

    invoke-static {p1, v4, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemShowAsAction(Landroid/view/Menu;II)V

    .line 79
    :cond_1
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mIsKnox2Enabled:Z

    if-eqz v1, :cond_2

    .line 80
    const/4 v1, 0x0

    invoke-static {p1, v4, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 82
    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 48
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mAddAutoRotationIcon:Z

    if-eqz v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForSingle;->mScreenOrientation:Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewScreenOrientation;->onResume()V

    .line 51
    :cond_0
    return-void
.end method
