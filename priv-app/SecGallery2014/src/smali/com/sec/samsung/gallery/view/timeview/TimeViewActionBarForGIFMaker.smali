.class public Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;
.super Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;
.source "TimeViewActionBarForGIFMaker.java"


# static fields
.field private static final MEDIA_TYPE_VIDEO:I = 0x4


# direct methods
.method protected constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 30
    return-void
.end method

.method private isAvalableProceedStoryAlbum(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 3
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v0, 0x0

    .line 91
    instance-of v1, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/cloud/CloudImage;

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v1, :cond_1

    .line 92
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    .line 97
    :cond_1
    :goto_0
    return v0

    .line 95
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private startCreateGIF()V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "START_GIF_MAKER"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 88
    return-void
.end method


# virtual methods
.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 7
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 61
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 83
    :goto_0
    :pswitch_0
    return-void

    .line 63
    :pswitch_1
    const/16 v0, 0x14

    .line 64
    .local v0, "MaxCount":I
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mCount:I

    if-le v2, v0, :cond_0

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0114

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 67
    .local v1, "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 71
    .end local v1    # "text":Ljava/lang/String;
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mCount:I

    if-ge v2, v4, :cond_1

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0116

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 74
    .restart local v1    # "text":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 78
    .end local v1    # "text":Ljava/lang/String;
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->startCreateGIF()V

    goto :goto_0

    .line 81
    .end local v0    # "MaxCount":I
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->onBackPressed()V

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x7f0f0022
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 11
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v10, 0x0

    .line 34
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForMultiPick;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 35
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v4

    .line 36
    .local v4, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v3, 0x0

    .line 37
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v1, 0x1

    .line 38
    .local v1, "bShowDone":Z
    const/16 v0, 0x14

    .line 39
    .local v0, "MaxCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_1

    .line 40
    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 41
    .restart local v3    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v3, :cond_0

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    if-le v6, v0, :cond_2

    .line 42
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0e0114

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 44
    .local v5, "text":Ljava/lang/String;
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v6, v5}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 45
    const v6, 0x7f0f0022

    invoke-static {p1, v6, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 57
    .end local v5    # "text":Ljava/lang/String;
    :cond_1
    :goto_1
    return-void

    .line 50
    :cond_2
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/view/timeview/TimeViewActionBarForGIFMaker;->isAvalableProceedStoryAlbum(Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v1

    .line 52
    if-nez v1, :cond_3

    .line 53
    const v6, 0x7f0f0024

    invoke-static {p1, v6, v10}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    goto :goto_1

    .line 39
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
