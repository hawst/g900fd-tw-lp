.class public Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;
.super Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;
.source "MediaNewItemAdapter.java"


# static fields
.field private static final CACHE_SIZE:I = 0x40


# instance fields
.field protected final mContext:Landroid/content/Context;

.field private mFocusedIndex:I

.field private mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

.field private final mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;-><init>(Landroid/content/Context;)V

    .line 27
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mFocusedIndex:I

    .line 33
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 36
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSize:I

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumThumbnailProxy()Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .line 38
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 39
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setCacheSize(I)V

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 41
    return-void
.end method

.method private drawDeleteButton(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 7
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x0

    .line 205
    invoke-virtual {p1, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 207
    .local v0, "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 208
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f020478

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 209
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 210
    .restart local v0    # "checkBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 211
    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 212
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 213
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 214
    const/4 v2, 0x3

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 216
    .end local v1    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v0, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 217
    return-void
.end method

.method private drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v2, 0x5

    .line 191
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 192
    .local v0, "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mFocusedIndex:I

    if-ne v1, p2, :cond_2

    .line 193
    if-nez v0, :cond_0

    .line 194
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 195
    .restart local v0    # "focusBoxView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    const v1, 0x7f020291

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 196
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 198
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 202
    :cond_1
    :goto_0
    return-void

    .line 199
    :cond_2
    if-eqz v0, :cond_1

    .line 200
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;)V
    .locals 3
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;

    .prologue
    const/4 v2, 0x1

    .line 157
    invoke-virtual {p1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 158
    .local v0, "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-nez v0, :cond_0

    .line 159
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 160
    .restart local v0    # "frameView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {p1, v0, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 162
    :cond_0
    const v1, 0x7f0200a4

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageResource(I)V

    .line 163
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 164
    return-void
.end method

.method private drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 10
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v9, 0x3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v8, 0x2

    .line 167
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 168
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v6

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    instance-of v6, v2, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v6, :cond_2

    move v1, v4

    .line 170
    .local v1, "isVideo":Z
    :goto_0
    invoke-virtual {p1, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->findViewByID(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .line 172
    .local v3, "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    if-eqz v1, :cond_3

    .line 173
    if-nez v3, :cond_0

    .line 174
    invoke-static {}, Lcom/sec/android/gallery3d/util/ResourceManager;->getInstance()Lcom/sec/android/gallery3d/util/ResourceManager;

    move-result-object v4

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f020308

    invoke-virtual {v4, v6, v7}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 175
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .end local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 176
    .restart local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    invoke-virtual {v3, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 177
    invoke-virtual {v3, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setAlign(II)V

    .line 178
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v6

    invoke-virtual {v3, v4, v6}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setSize(II)V

    .line 179
    invoke-virtual {v3, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setFitMode(I)V

    .line 180
    invoke-virtual {p1, v3, v9}, Lcom/sec/android/gallery3d/glcore/GlImageView;->addChild(Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 182
    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    invoke-virtual {v3, v5}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    .line 186
    :cond_1
    :goto_1
    return-void

    .end local v1    # "isVideo":Z
    .end local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_2
    move v1, v5

    .line 168
    goto :goto_0

    .line 183
    .restart local v1    # "isVideo":Z
    .restart local v3    # "videoPlayView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_3
    if-eqz v3, :cond_1

    .line 184
    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method private setBitmap(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V
    .locals 5
    .param p1, "imageView"    # Lcom/sec/android/gallery3d/glcore/GlImageView;
    .param p2, "position"    # I

    .prologue
    .line 220
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 221
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_0

    .line 244
    :goto_0
    return-void

    .line 223
    :cond_0
    const/4 v3, 0x0

    .line 224
    .local v3, "rotation":I
    const/4 v0, 0x0

    .line 226
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-ltz p2, :cond_1

    .line 227
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v4, p2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 230
    :cond_1
    if-nez v0, :cond_2

    .line 231
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getIndexOfItemFromPhotoView(Lcom/sec/android/gallery3d/data/MediaItem;)I

    move-result v1

    .line 232
    .local v1, "index":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    invoke-virtual {v4, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getThumbnailBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 234
    .end local v1    # "index":I
    :cond_2
    if-nez v0, :cond_3

    .line 235
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->readyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v4}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 237
    :cond_3
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v3

    .line 238
    if-nez v3, :cond_4

    .line 239
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 241
    :cond_4
    invoke-virtual {p1, v0, v3}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setImageBitmap(Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method


# virtual methods
.method public add(Lcom/sec/android/gallery3d/data/MediaObject;)V
    .locals 2
    .param p1, "mediaObject"    # Lcom/sec/android/gallery3d/data/MediaObject;

    .prologue
    .line 57
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v1

    .line 58
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 59
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 60
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSize:I

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSize:I

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setSize(I)V

    .line 62
    return-void

    .line 59
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected addContentListener()V
    .locals 0

    .prologue
    .line 148
    return-void
.end method

.method public addList(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 48
    .local p1, "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-nez p1, :cond_1

    .line 54
    :cond_0
    return-void

    .line 51
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 52
    .local v1, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->add(Lcom/sec/android/gallery3d/data/MediaObject;)V

    goto :goto_0
.end method

.method public clear()V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->removeAll()V

    .line 89
    return-void
.end method

.method public contains(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->isSelected(Lcom/sec/android/gallery3d/data/MediaObject;)Z

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 93
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v1

    .line 94
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    monitor-exit v1

    return v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getIndexOfItemFromPhotoView(Lcom/sec/android/gallery3d/data/MediaItem;)I
    .locals 10
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 247
    const/4 v1, -0x1

    .line 248
    .local v1, "detect":I
    if-nez p1, :cond_0

    move v2, v1

    .line 267
    .end local v1    # "detect":I
    .local v2, "detect":I
    :goto_0
    return v2

    .line 251
    .end local v2    # "detect":I
    .restart local v1    # "detect":I
    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    .line 252
    .local v4, "groupId":J
    const/4 v3, 0x0

    .local v3, "i":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getCount()I

    move-result v0

    .local v0, "count":I
    :goto_1
    if-ge v3, v0, :cond_1

    .line 253
    iget-object v7, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    invoke-virtual {v7, v3}, Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v6

    .line 254
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const-wide/16 v8, 0x0

    cmp-long v7, v4, v8

    if-lez v7, :cond_2

    .line 255
    if-eqz v6, :cond_3

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v8

    cmp-long v7, v8, v4

    if-nez v7, :cond_3

    .line 256
    move v1, v3

    .end local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    :goto_2
    move v2, v1

    .line 267
    .end local v1    # "detect":I
    .restart local v2    # "detect":I
    goto :goto_0

    .line 260
    .end local v2    # "detect":I
    .restart local v1    # "detect":I
    .restart local v6    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_2
    if-eqz v6, :cond_3

    if-ne v6, p1, :cond_3

    .line 261
    move v1, v3

    .line 262
    goto :goto_2

    .line 252
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method public getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 100
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v1

    .line 101
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 102
    const/4 v0, 0x0

    monitor-exit v1

    .line 105
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->get(I)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    monitor-exit v1

    goto :goto_0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    return-object v0
.end method

.method public getItemImage(II)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I
    .param p2, "type"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Lcom/sec/android/gallery3d/glcore/GlView;
    .param p3, "parentLayer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    .line 117
    if-nez p2, :cond_0

    .line 118
    new-instance v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 122
    .local v0, "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :goto_0
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->drawFrame(Lcom/sec/android/gallery3d/glcore/GlImageView;)V

    .line 123
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->drawVideoPlayIcon(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 124
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->drawFocusBox(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 125
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->drawDeleteButton(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 126
    invoke-direct {p0, v0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->setBitmap(Lcom/sec/android/gallery3d/glcore/GlImageView;I)V

    .line 128
    return-object v0

    .end local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    :cond_0
    move-object v0, p2

    .line 120
    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlImageView;

    .restart local v0    # "imageView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    goto :goto_0
.end method

.method protected loadData()Z
    .locals 1

    .prologue
    .line 272
    const/4 v0, 0x1

    return v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause()V

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 140
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resume()V

    .line 134
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 144
    return-void
.end method

.method public remove(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 70
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelSlotImage(I)V

    .line 71
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->freeSlotContent(I)V

    .line 73
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 74
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    monitor-enter v2

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSelectionManager:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->remove(Lcom/sec/android/gallery3d/data/MediaObject;)V

    .line 76
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSize:I

    .line 78
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    iget v2, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mSize:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setSize(I)V

    .line 79
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method protected removeContentListener()V
    .locals 0

    .prologue
    .line 152
    return-void
.end method

.method public setMediaItemAdapter(Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;)V
    .locals 0
    .param p1, "mediaItemAdapter"    # Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/MediaNewItemAdapter;->mMediaItemAdapter:Lcom/sec/samsung/gallery/view/adapter/MediaItemAdapter;

    .line 45
    return-void
.end method
