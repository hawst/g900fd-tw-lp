.class public Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;
.super Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;
.source "GallerySearchUserTagScrollViewHorizontalStyle.java"


# instance fields
.field private mLastLinearLayout:Landroid/widget/LinearLayout;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;-><init>(Landroid/content/Context;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    .line 25
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    .line 21
    return-void
.end method

.method private makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V
    .locals 24
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p3, "lineCount"    # I
    .param p4, "isMultiTag"    # Z
    .param p5, "isExpanded"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;",
            ">;IZZ)V"
        }
    .end annotation

    .prologue
    .line 90
    .local p2, "tagList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;>;"
    sget-boolean v20, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v20, :cond_2

    .line 91
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    .line 92
    .local v17, "tagListLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/LinearLayout;>;"
    const/4 v10, 0x0

    .line 93
    .local v10, "lastLine":Landroid/widget/LinearLayout;
    const/16 v19, 0x0

    .line 95
    .local v19, "totalWidth":I
    if-eqz p2, :cond_1

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v20

    if-nez v20, :cond_1

    .line 96
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 97
    .local v5, "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v20

    const/16 v21, -0x1

    const/16 v22, -0x2

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeEmptyLineLinearLayout(Landroid/content/Context;III)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 99
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 101
    if-eqz p5, :cond_1

    .line 102
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 103
    .local v16, "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    new-instance v15, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;

    move-object/from16 v0, v16

    invoke-direct {v15, v5, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 105
    .local v15, "tagButton":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    invoke-virtual/range {v16 .. v16}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isEnabled()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setEnabled(Z)V

    .line 106
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 107
    invoke-virtual/range {v16 .. v16}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isSelected()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 108
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->setContentDescription(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 113
    invoke-virtual {v15}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->getExpectedWidth()I

    move-result v7

    .line 114
    .local v7, "expectedWidth":I
    add-int v19, v19, v7

    .line 116
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->MAX_WIDTH_FOR_A_LINE:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-le v0, v1, :cond_0

    .line 117
    move/from16 v19, v7

    .line 119
    const/16 v20, -0x1

    const/16 v21, -0x2

    const/16 v22, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v21

    move/from16 v3, v22

    invoke-virtual {v0, v5, v1, v2, v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeEmptyLineLinearLayout(Landroid/content/Context;III)Landroid/widget/LinearLayout;

    move-result-object v10

    .line 121
    move-object/from16 v0, v17

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_0
    invoke-virtual {v10, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 128
    .end local v5    # "context":Landroid/content/Context;
    .end local v7    # "expectedWidth":I
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v15    # "tagButton":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    .end local v16    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :cond_1
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .restart local v9    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v20

    if-eqz v20, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/widget/LinearLayout;

    .line 129
    .local v13, "linearLayout":Landroid/widget/LinearLayout;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_1

    .line 134
    .end local v9    # "i$":Ljava/util/Iterator;
    .end local v10    # "lastLine":Landroid/widget/LinearLayout;
    .end local v13    # "linearLayout":Landroid/widget/LinearLayout;
    .end local v17    # "tagListLines":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/widget/LinearLayout;>;"
    .end local v19    # "totalWidth":I
    :cond_2
    if-gtz p3, :cond_4

    .line 173
    :cond_3
    :goto_2
    return-void

    .line 137
    :cond_4
    move/from16 v0, p3

    new-array v14, v0, [Landroid/widget/LinearLayout;

    .line 138
    .local v14, "multiTagLine":[Landroid/widget/LinearLayout;
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v18

    .line 139
    .local v18, "tagListSize":I
    if-eqz p4, :cond_5

    .line 140
    const/16 v20, 0x8

    move/from16 v0, v18

    move/from16 v1, v20

    if-gt v0, v1, :cond_5

    .line 141
    const/16 p3, 0x1

    .line 142
    const/16 p4, 0x0

    .line 145
    :cond_5
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_3
    move/from16 v0, p3

    if-ge v8, v0, :cond_8

    .line 146
    new-instance v20, Landroid/widget/LinearLayout;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v21

    invoke-direct/range {v20 .. v21}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    aput-object v20, v14, v8

    .line 147
    const/16 v20, 0x2

    move/from16 v0, p3

    move/from16 v1, v20

    if-lt v0, v1, :cond_7

    .line 148
    invoke-virtual/range {p1 .. p1}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v20

    check-cast v20, Landroid/view/ViewGroup;

    invoke-virtual/range {v20 .. v20}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/widget/LinearLayout$LayoutParams;

    .line 149
    .local v12, "layoutParentParams":Landroid/widget/LinearLayout$LayoutParams;
    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v20, -0x1

    const/16 v21, -0x2

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v11, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 151
    .local v11, "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    if-lez v8, :cond_6

    .line 152
    iget v0, v12, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    move/from16 v20, v0

    iget v0, v12, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    move/from16 v21, v0

    add-int v20, v20, v21

    move/from16 v0, v20

    iput v0, v11, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    .line 154
    :cond_6
    aget-object v20, v14, v8

    move-object/from16 v0, v20

    invoke-virtual {v0, v11}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 156
    .end local v11    # "layoutParams":Landroid/widget/LinearLayout$LayoutParams;
    .end local v12    # "layoutParentParams":Landroid/widget/LinearLayout$LayoutParams;
    :cond_7
    aget-object v20, v14, v8

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 157
    aget-object v20, v14, v8

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    .line 145
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 161
    :cond_8
    const/4 v8, 0x0

    :goto_4
    move/from16 v0, v18

    if-ge v8, v0, :cond_3

    .line 162
    :try_start_0
    move-object/from16 v0, p2

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .line 163
    .restart local v16    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    new-instance v15, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;

    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    invoke-direct {v15, v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 164
    .restart local v15    # "tagButton":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    move-object/from16 v0, p0

    invoke-virtual {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 165
    invoke-virtual/range {v16 .. v16}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->isSelected()Z

    move-result v20

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setSelected(Z)V

    .line 166
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v15, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->setContentDescription(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V

    .line 167
    rem-int v20, v8, p3

    aget-object v20, v14, v20

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 161
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 169
    .end local v15    # "tagButton":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    .end local v16    # "tagData":Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;
    :catch_0
    move-exception v6

    .line 170
    .local v6, "e":Ljava/lang/IndexOutOfBoundsException;
    const-string v20, "GallerySearchUserTagScrollViewHorizontalStyle"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "Invalid index = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    const-string/jumbo v22, "size = "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v22

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2
.end method

.method private setContentDescription(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;)V
    .locals 5
    .param p1, "tagButton"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;
    .param p2, "tagData"    # Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;

    .prologue
    const v2, 0x7f0e02b2

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 206
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsId()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 213
    :goto_0
    return-void

    .line 208
    :cond_0
    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTtsText()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 211
    :cond_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagData;->getTransTagText()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchTagButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public isLastRowFocus()Z
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->hasFocus()Z

    move-result v0

    .line 202
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected makeEmptyLineLinearLayout(Landroid/content/Context;III)Landroid/widget/LinearLayout;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "width"    # I
    .param p3, "height"    # I
    .param p4, "orientation"    # I

    .prologue
    .line 177
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 178
    .local v0, "ll":Landroid/widget/LinearLayout;
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, p2, p3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 180
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 181
    invoke-virtual {v0, p4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 183
    return-object v0
.end method

.method protected refreshTagList()V
    .locals 11

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x1

    .line 33
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagLine:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 34
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagLine:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 35
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagLine:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 36
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagLine:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagLine:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViewsInLayout()V

    .line 39
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeAllViews()V

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLastLinearLayout:Landroid/widget/LinearLayout;

    .line 42
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagList:Ljava/util/List;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_a

    .line 43
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->addView(Landroid/view/View;)V

    .line 45
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagLine:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagList:Ljava/util/List;

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->DEFAULT_TAG_MAX_LINE_COUNT:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mExpandStates:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V

    .line 51
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagList:Ljava/util/List;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->addView(Landroid/view/View;)V

    .line 54
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagLine:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagList:Ljava/util/List;

    iget v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->DEFAULT_TAG_MAX_LINE_COUNT:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mExpandStates:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V

    .line 60
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagList:Ljava/util/List;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_c

    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_4

    .line 62
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->addView(Landroid/view/View;)V

    .line 63
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagLine:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagList:Ljava/util/List;

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->FILTER_TAG_MAX_LINE_COUNT:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mExpandStates:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V

    .line 69
    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagList:Ljava/util/List;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_6

    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->addView(Landroid/view/View;)V

    .line 72
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagLine:Landroid/widget/LinearLayout;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagList:Ljava/util/List;

    iget v8, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->DEFAULT_TAG_MAX_LINE_COUNT:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mExpandStates:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    move-object v5, p0

    invoke-direct/range {v5 .. v10}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V

    .line 78
    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserTagTagList:Ljava/util/List;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserTagTagList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_e

    .line 79
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_8

    .line 80
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->addView(Landroid/view/View;)V

    .line 81
    :cond_8
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagLine:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserTagTagList:Ljava/util/List;

    iget v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->FILTER_TAG_MAX_LINE_COUNT:I

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mExpandStates:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->makeMultiLineFilterTag(Landroid/view/ViewGroup;Ljava/util/List;IZZ)V

    .line 87
    :cond_9
    :goto_4
    return-void

    .line 47
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mTimeTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    .line 56
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mEventTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeView(Landroid/view/View;)V

    goto/16 :goto_1

    .line 65
    :cond_c
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeView(Landroid/view/View;)V

    goto/16 :goto_2

    .line 74
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_7

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mPersonTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeView(Landroid/view/View;)V

    goto :goto_3

    .line 83
    :cond_e
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_9

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->removeView(Landroid/view/View;)V

    goto :goto_4
.end method

.method public setLoctionTagVisibleMode(II)V
    .locals 1
    .param p1, "visibility"    # I
    .param p2, "lineCount"    # I

    .prologue
    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mLocationTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 195
    iput p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->DEFAULT_TAG_MAX_LINE_COUNT:I

    .line 196
    return-void
.end method

.method public setUserDefTagVisibleMode(II)V
    .locals 1
    .param p1, "visibility"    # I
    .param p2, "lineCount"    # I

    .prologue
    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->mUserDefTagRootView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 189
    iput p2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchUserTagScrollViewHorizontalStyle;->FILTER_TAG_MAX_LINE_COUNT:I

    .line 190
    return-void
.end method
