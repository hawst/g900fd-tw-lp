.class Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;
.super Landroid/widget/BaseAdapter;
.source "EditCategoryDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CategoryListAdapter"
.end annotation


# instance fields
.field private mCategoryStringId:[Ljava/lang/Object;

.field private mCategoryStrings:[Ljava/lang/Object;

.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 96
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mContext:Landroid/content/Context;

    .line 97
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStrings:[Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->CATEGORY_STRING_MAP:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStringId:[Ljava/lang/Object;

    .line 99
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStrings:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 108
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStrings:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 113
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 118
    if-nez p2, :cond_0

    .line 119
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f0300d8

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    move-object v0, p2

    .line 121
    check-cast v0, Landroid/widget/CheckedTextView;

    .line 122
    .local v0, "cTextView":Landroid/widget/CheckedTextView;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStringId:[Ljava/lang/Object;

    aget-object v2, v2, p1

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 124
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->this$0:Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;

    # getter for: Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->mItem:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;->access$100(Lcom/sec/samsung/gallery/view/common/EditCategoryDialog;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStrings:[Ljava/lang/Object;

    aget-object v2, v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    .line 125
    .local v1, "check":Z
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/common/EditCategoryDialog$CategoryListAdapter;->mCategoryStrings:[Ljava/lang/Object;

    aget-object v2, v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/CheckedTextView;->setTag(Ljava/lang/Object;)V

    .line 126
    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 127
    return-object p2
.end method
