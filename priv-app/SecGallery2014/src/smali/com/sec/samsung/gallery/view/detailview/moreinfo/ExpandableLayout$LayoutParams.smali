.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "ExpandableLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public final horizontalSpacing:I

.field public final itemHeight:I

.field public final verticalSpacing:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "horizontalSpacing"    # I
    .param p2, "verticalSpacing"    # I
    .param p3, "itemHeight"    # I

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0, v0, v0}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 37
    iput p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->horizontalSpacing:I

    .line 38
    iput p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->verticalSpacing:I

    .line 39
    iput p3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/ExpandableLayout$LayoutParams;->itemHeight:I

    .line 40
    return-void
.end method
