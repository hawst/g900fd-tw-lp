.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->updateNewAlbumActionBar()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 1118
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1120
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/view/Menu;

    move-result-object v3

    const v4, 0x7f0f02de

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v3, v4, v0}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1121
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mMenu:Landroid/view/Menu;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Landroid/view/Menu;

    move-result-object v0

    const v3, 0x7f0f0022

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$7;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mNewAlbumSelectionProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getNumberOfMarkedAsSelected()I

    move-result v4

    if-gtz v4, :cond_1

    :goto_1
    invoke-static {v0, v3, v1}, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->setMenuItemVisibility(Landroid/view/Menu;IZ)V

    .line 1122
    return-void

    :cond_0
    move v0, v2

    .line 1120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 1121
    goto :goto_1
.end method
