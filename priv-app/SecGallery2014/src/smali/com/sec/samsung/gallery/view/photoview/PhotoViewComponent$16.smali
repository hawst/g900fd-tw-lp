.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)V
    .locals 0

    .prologue
    .line 2728
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V
    .locals 3
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "status"    # I
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I

    .prologue
    const/4 v0, 0x1

    .line 2732
    sparse-switch p2, :sswitch_data_0

    .line 2757
    :goto_0
    :sswitch_0
    return-void

    .line 2734
    :sswitch_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoViewState:Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3000(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    move-result-object v1

    iput p3, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;->mCurrentLayout:I

    .line 2735
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iget-boolean v1, v1, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mIsCategoryTagType:Z

    if-eqz v1, :cond_0

    .line 2736
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    const-string/jumbo v1, "timeViewColCnt"

    invoke-static {v0, v1, p3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0

    .line 2739
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2740
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const-string v2, "photoViewColCnt"

    invoke-static {v1, v2, p3}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    .line 2743
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1200(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const-string v2, "is_split_view_expanded"

    if-ne p4, v0, :cond_2

    :goto_1
    invoke-static {v1, v2, v0}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 2751
    :sswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->addToNewAlbumList(I)V

    goto :goto_0

    .line 2754
    :sswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$16;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # invokes: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->showItemCopyMoveDialog(I)V
    invoke-static {v0, p3}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$3100(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;I)V

    goto :goto_0

    .line 2732
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0xa -> :sswitch_0
        0xb -> :sswitch_2
        0xc -> :sswitch_3
    .end sparse-switch
.end method
