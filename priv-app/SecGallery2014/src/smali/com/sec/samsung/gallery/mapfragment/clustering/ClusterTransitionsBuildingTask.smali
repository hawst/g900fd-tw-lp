.class public Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;
.super Landroid/os/AsyncTask;
.source "ClusterTransitionsBuildingTask.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;,
        Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;",
        "Ljava/lang/Void;",
        "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;",
        ">;"
    }
.end annotation


# instance fields
.field private final host:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;)V
    .locals 0
    .param p1, "host"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->host:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;

    .line 21
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;
    .locals 7
    .param p1, "args"    # [Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;

    .prologue
    const/4 v6, 0x1

    .line 28
    new-instance v4, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;

    invoke-direct {v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;-><init>()V

    .line 29
    .local v4, "result":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;
    if-eqz p1, :cond_1

    array-length v5, p1

    if-ne v5, v6, :cond_1

    .line 30
    invoke-static {v6}, Landroid/os/Process;->setThreadPriority(I)V

    .line 32
    const/4 v5, 0x0

    aget-object v0, p1, v5

    .line 33
    .local v0, "arg":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;

    iget-object v5, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;->projection:Lcom/google/android/gms/maps/Projection;

    iget-object v6, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;->previousClusters:Ljava/util/ArrayList;

    invoke-direct {v1, v5, v6}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;-><init>(Lcom/google/android/gms/maps/Projection;Ljava/util/ArrayList;)V

    .line 34
    .local v1, "ctb":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;
    iget-object v5, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;->currentClusters:Ljava/util/ArrayList;

    if-eqz v5, :cond_0

    .line 35
    iget-object v5, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;->currentClusters:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 36
    .local v2, "currentClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->add(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    goto :goto_0

    .line 39
    .end local v2    # "currentClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v3    # "i$":Ljava/util/Iterator;
    :cond_0
    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->build()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    move-result-object v5

    iput-object v5, v4, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;->clusterTransitions:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    .line 41
    const/16 v5, 0xa

    invoke-static {v5}, Landroid/os/Process;->setThreadPriority(I)V

    .line 43
    .end local v0    # "arg":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;
    .end local v1    # "ctb":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;
    :cond_1
    return-object v4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, [Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->doInBackground([Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Argument;)Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;)V
    .locals 1
    .param p1, "result"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->host:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->host:Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Host;->onClusterTransitionsBuildingTaskPostExecute(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;)V

    .line 55
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 15
    check-cast p1, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask;->onPostExecute(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsBuildingTask$Result;)V

    return-void
.end method
