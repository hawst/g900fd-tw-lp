.class Lcom/sec/samsung/gallery/controller/DetailModel$1;
.super Ljava/lang/Object;
.source "DetailModel.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/DetailsAddressResolver$AddressResolvingListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/DetailModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/DetailModel;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/controller/DetailModel;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/DetailModel$1;->this$0:Lcom/sec/samsung/gallery/controller/DetailModel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddressAvailable(Ljava/lang/String;)V
    .locals 7
    .param p1, "address"    # Ljava/lang/String;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel$1;->this$0:Lcom/sec/samsung/gallery/controller/DetailModel;

    # getter for: Lcom/sec/samsung/gallery/controller/DetailModel;->contentsList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/DetailModel;->access$200(Lcom/sec/samsung/gallery/controller/DetailModel;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/DetailModel$1;->this$0:Lcom/sec/samsung/gallery/controller/DetailModel;

    # getter for: Lcom/sec/samsung/gallery/controller/DetailModel;->mLocationIndex:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/DetailModel;->access$000(Lcom/sec/samsung/gallery/controller/DetailModel;)I

    move-result v1

    const-string v2, "%s: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/DetailModel$1;->this$0:Lcom/sec/samsung/gallery/controller/DetailModel;

    # getter for: Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/DetailModel;->access$100(Lcom/sec/samsung/gallery/controller/DetailModel;)Landroid/content/Context;

    move-result-object v5

    const/16 v6, 0x9

    invoke-static {v5, v6}, Lcom/sec/android/gallery3d/ui/DetailsHelper;->getDetailsName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 370
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/DetailModel$1;->this$0:Lcom/sec/samsung/gallery/controller/DetailModel;

    # getter for: Lcom/sec/samsung/gallery/controller/DetailModel;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/DetailModel;->access$100(Lcom/sec/samsung/gallery/controller/DetailModel;)Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "SHOW_DETAILS_DIALOG"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;)V

    .line 371
    return-void
.end method
