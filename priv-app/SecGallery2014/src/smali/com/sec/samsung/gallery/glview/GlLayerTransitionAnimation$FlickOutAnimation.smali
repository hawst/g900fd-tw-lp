.class Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;
.super Ljava/lang/Object;
.source "GlLayerTransitionAnimation.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$IApplyTransform;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FlickOutAnimation"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V
    .locals 0

    .prologue
    .line 165
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$1;

    .prologue
    .line 165
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;-><init>(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)V

    return-void
.end method


# virtual methods
.method public applyTransform(F)V
    .locals 9
    .param p1, "ratio"    # F

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 168
    sub-float v1, v7, p1

    .line 169
    .local v1, "inRatio":F
    mul-float v6, v1, v1

    sub-float v5, v7, v6

    .line 170
    .local v5, "proRatio":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F

    move-result v6

    mul-float v4, v6, v5

    .line 171
    .local v4, "oldDelta":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mDeltaX:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$400(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)F

    move-result v6

    neg-float v6, v6

    sub-float/2addr v7, v5

    mul-float v2, v6, v7

    .line 175
    .local v2, "newDelta":F
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldCount:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$500(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v6

    if-ge v0, v6, :cond_0

    .line 176
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$600(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 177
    .local v3, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mOldObjHnd:[I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$800(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v6

    aget v6, v6, v0

    invoke-virtual {v3, v6, v4, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    .end local v3    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewCount:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$900(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)I

    move-result v6

    if-ge v0, v6, :cond_1

    .line 180
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjList:Ljava/util/ArrayList;
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1000(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 181
    .restart local v3    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation$FlickOutAnimation;->this$0:Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;

    # getter for: Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->mNewObjHnd:[I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;->access$1200(Lcom/sec/samsung/gallery/glview/GlLayerTransitionAnimation;)[I

    move-result-object v6

    aget v6, v6, v0

    invoke-virtual {v3, v6, v2, v8, v8}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(IFFF)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 183
    .end local v3    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_1
    return-void
.end method
