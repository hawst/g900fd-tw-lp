.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;
.super Ljava/lang/Object;
.source "SlideShowSettingView.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 251
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    if-eqz p2, :cond_1

    .line 252
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    .line 253
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-interface {v2, v3}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 254
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->requestFocusFromTouch()Z

    .line 291
    :cond_0
    :goto_0
    return-void

    .line 255
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    if-eqz p2, :cond_2

    .line 256
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    .line 257
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-interface {v2, v3}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 258
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    if-eqz p2, :cond_3

    .line 259
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    .line 260
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-interface {v2, v3}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 261
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-eqz p2, :cond_4

    .line 262
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setNormalBackground()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)V

    .line 263
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filterClickListener:Landroid/view/View$OnClickListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-interface {v2, v3}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0

    .line 264
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->filter_linear_layout:Landroid/widget/LinearLayout;

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz p2, :cond_0

    .line 265
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->access$100(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "SLIDE_SHOW_FILTER_PREFS"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 267
    .local v1, "preferences":Landroid/content/SharedPreferences;
    const-string v2, "FILTER"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 269
    .local v0, "filter":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->clearFocus()V

    .line 270
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->clearFocus()V

    .line 271
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->clearFocus()V

    .line 272
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->clearFocus()V

    .line 274
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 276
    :pswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->grayscale_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->requestFocus()Z

    goto/16 :goto_0

    .line 279
    :pswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->simple_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->requestFocus()Z

    goto/16 :goto_0

    .line 282
    :pswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->sepia_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->requestFocus()Z

    goto/16 :goto_0

    .line 285
    :pswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView$5;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->vintage_image:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->requestFocus()Z

    goto/16 :goto_0

    .line 274
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
