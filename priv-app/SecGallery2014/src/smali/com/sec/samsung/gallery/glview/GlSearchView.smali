.class public Lcom/sec/samsung/gallery/glview/GlSearchView;
.super Lcom/sec/samsung/gallery/glview/GlAbsListView;
.source "GlSearchView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;,
        Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;,
        Lcom/sec/samsung/gallery/glview/GlSearchView$EstimationInfo;
    }
.end annotation


# static fields
.field private static ALBUM_ITEM_MAX:I = 0x0

.field private static CLIP_STEP_LAND:I = 0x0

.field private static CLIP_STEP_PORT:I = 0x0

.field private static MAX_KEYWORD_ROW_COUNT_LAND:I = 0x0

.field private static MAX_KEYWORD_ROW_COUNT_PORT:I = 0x0

.field private static SPLIT_RATIO_LAND:F = 0.0f

.field private static SPLIT_RATIO_PORT:F = 0.0f

.field public static final STATUS_COLUMN_PORT:I = 0x2

.field public static final STATUS_COLUMN_WIDE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "GlSearchView"


# instance fields
.field private mAnimStep:I

.field private mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

.field private mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

.field private mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

.field private mDeletingIndex:I

.field public mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field private mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

.field private mItemClipRect:Landroid/graphics/Rect;

.field private mKeyboardKeywordFocused:Z

.field private mKeywordClipRect:Landroid/graphics/Rect;

.field private mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

.field private mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field protected mKeywordScroll:F

.field private mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

.field public mListenerGenericMotionKeyword:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field private mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field private mListenerKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

.field private mListenerKeywordClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

.field private mListenerKeywordMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

.field private mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

.field private mPitchFact:F

.field private mPressedUpper:Z

.field private mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

.field public mSPKeyString:Ljava/lang/String;

.field private mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

.field private mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;

.field private mUsePitchBoundary:Z

.field private mViewMode:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const v1, 0x3e99999a    # 0.3f

    .line 35
    const/16 v0, 0x1e

    sput v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->ALBUM_ITEM_MAX:I

    .line 36
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_PORT:I

    .line 37
    const/4 v0, 0x2

    sput v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_LAND:I

    .line 38
    sput v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->SPLIT_RATIO_LAND:F

    .line 39
    sput v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->SPLIT_RATIO_PORT:F

    .line 41
    const/16 v0, 0x5a

    sput v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_PORT:I

    .line 42
    const/16 v0, 0x4b

    sput v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_LAND:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 70
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;-><init>(Landroid/content/Context;)V

    .line 44
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 45
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 46
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 49
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemClipRect:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlSearchView$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    .line 54
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordClipRect:Landroid/graphics/Rect;

    .line 55
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    .line 56
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    .line 61
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    .line 65
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mUsePitchBoundary:Z

    .line 66
    const-string v0, "searchViewMode"

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSPKeyString:Ljava/lang/String;

    .line 359
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 390
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    .line 425
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$4;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeywordMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 441
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$5;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    .line 456
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$6;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$6;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeywordClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 479
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$7;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$7;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    .line 502
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$8;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$8;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerGenericMotionKeyword:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 527
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$9;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$9;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    .line 642
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$10;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$10;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    .line 764
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$11;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$11;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 778
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$12;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$12;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 72
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    .line 73
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$ItemScroller;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlSearchView$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    .line 75
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 76
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-direct {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    .line 77
    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/glview/GlSearchView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->startKeywordDeleteAnimation(I)V

    return-void
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I

    return v0
.end method

.method static synthetic access$1102(Lcom/sec/samsung/gallery/glview/GlSearchView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I

    return p1
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    return-object v0
.end method

.method static synthetic access$1202(Lcom/sec/samsung/gallery/glview/GlSearchView;Lcom/sec/samsung/gallery/glview/GlThumbObject;)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .prologue
    .line 29
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    return-object p1
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/glview/GlSearchView;)[Lcom/sec/samsung/gallery/glview/GlBaseObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlRepositionAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadKeywords()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/glview/GlSearchView;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setClipRect()V

    return-void
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    return v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/glview/GlSearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getKeywordCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2200()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_LAND:I

    return v0
.end method

.method static synthetic access$2300()I
    .locals 1

    .prologue
    .line 29
    sget v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_PORT:I

    return v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/glview/GlSearchView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mUsePitchBoundary:Z

    return v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/glview/GlSearchView;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F

    return v0
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/glview/GlSearchView;F)F
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # F

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPitchFact:F

    return p1
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/glview/GlBlurObject;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    return-object v0
.end method

.method static synthetic access$702(Lcom/sec/samsung/gallery/glview/GlSearchView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/glview/GlSearchView;)Lcom/sec/samsung/gallery/view/utils/SoundUtils;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/glview/GlSearchView;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;

    .prologue
    .line 29
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeletingIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/samsung/gallery/glview/GlSearchView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlSearchView;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeletingIndex:I

    return p1
.end method

.method private convAnimResize()V
    .locals 11

    .prologue
    .line 617
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v7

    .line 618
    .local v5, "objCount":I
    new-array v1, v5, [Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 619
    .local v1, "glObject":[Lcom/sec/android/gallery3d/glcore/GlObject;
    new-array v4, v5, [I

    .line 620
    .local v4, "indexes":[I
    const/4 v6, 0x0

    .line 621
    .local v6, "validCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_0

    .line 622
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v2, v7, v3

    check-cast v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 623
    .local v2, "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-lt v3, v7, :cond_1

    .line 633
    .end local v2    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 634
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;-><init>(Landroid/content/Context;)V

    .line 635
    .local v0, "convSetAnim":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iget-object v10, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iget v10, v10, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    invoke-virtual {v0, v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setSrc([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 636
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mObjEstimatorEx:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-virtual {v0, v1, v4, v7, v6}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setDst([Lcom/sec/android/gallery3d/glcore/GlObject;[ILcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;I)V

    .line 637
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mConvSetListener:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;

    invoke-virtual {v0, v7}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->setAnimationDoneListener(Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnAnimationDoneListener;)V

    .line 638
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;->start()V

    .line 639
    const/4 v7, 0x0

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimType:I

    .line 640
    return-void

    .line 625
    .end local v0    # "convSetAnim":Lcom/sec/samsung/gallery/glview/GlConvSetAnimation;
    .restart local v2    # "glObjectCur":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    if-ge v7, v8, :cond_2

    .line 626
    const/4 v7, 0x1

    invoke-virtual {v2, v7}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setVisibility(Z)V

    .line 627
    aput-object v2, v1, v6

    .line 628
    iget v7, v2, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    aput v7, v4, v6

    .line 629
    add-int/lit8 v6, v6, 0x1

    .line 621
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method

.method private createKeywordRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 8

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 209
    sget v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->ALBUM_ITEM_MAX:I

    new-array v6, v1, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 211
    .local v6, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->ALBUM_ITEM_MAX:I

    if-ge v7, v1, :cond_0

    .line 212
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_NORMAL:I

    sget v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_CANVAS_TINY:I

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 213
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    aput-object v0, v6, v7

    .line 214
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeywordMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 215
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeywordClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 216
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerGenericMotionKeyword:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 211
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 218
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    return-object v6
.end method

.method private createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 8

    .prologue
    const/16 v4, 0x140

    const/high16 v2, 0x3f800000    # 1.0f

    .line 226
    sget v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->ALBUM_ITEM_MAX:I

    new-array v6, v1, [Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 228
    .local v6, "glThumbSet":[Lcom/sec/samsung/gallery/glview/GlThumbObject;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    sget v1, Lcom/sec/samsung/gallery/glview/GlSearchView;->ALBUM_ITEM_MAX:I

    if-ge v7, v1, :cond_0

    .line 229
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move v3, v2

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlThumbObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFII)V

    .line 230
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    aput-object v0, v6, v7

    .line 231
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItemMove:Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setMoveListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;)V

    .line 232
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClickListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;)V

    .line 233
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setGenericMotionListener(Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;)V

    .line 228
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 235
    .end local v0    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    return-object v6
.end method

.method private freeRectangles()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v1, :cond_0

    .line 182
    :goto_0
    return-void

    .line 171
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 172
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 173
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v2, v1, v0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 175
    :cond_1
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 177
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 178
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 179
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aput-object v2, v1, v0

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 181
    :cond_2
    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    goto :goto_0
.end method

.method private getHorizontalCount()V
    .locals 3

    .prologue
    .line 686
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewColumnItemCount(IZ)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    .line 689
    :cond_0
    return-void
.end method

.method private getKeywordCount()I
    .locals 1

    .prologue
    .line 793
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    .line 794
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getKeyWordCount()I

    move-result v0

    .line 796
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 724
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    if-ge v1, v2, :cond_1

    .line 725
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v0, v2, v1

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 726
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mIndex:I

    if-ne v2, p1, :cond_0

    .line 729
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :goto_1
    return-object v0

    .line 724
    .restart local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 729
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private loadItems(Z)V
    .locals 5
    .param p1, "resizing"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 279
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    .line 280
    .local v0, "count":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimType:I

    if-eqz v2, :cond_2

    .line 281
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput-boolean v1, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mInitVisible:Z

    .line 284
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->reset()V

    .line 285
    if-eqz p1, :cond_3

    .line 286
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    invoke-virtual {v1, v2, v0, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    .line 290
    :goto_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mScrollRange:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 291
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mScroll:F

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 292
    if-lez v0, :cond_0

    .line 293
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getFirstVisiblePosition()I

    move-result v2

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getLastVisiblePosition()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->updateVisiableRange(II)V

    .line 294
    :cond_0
    return-void

    .end local v0    # "count":I
    :cond_1
    move v0, v1

    .line 279
    goto :goto_0

    .line 283
    .restart local v0    # "count":I
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput-boolean v3, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mInitVisible:Z

    goto :goto_1

    .line 288
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    div-int/2addr v3, v4

    int-to-float v3, v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemHVisibleCnt:I

    mul-int/2addr v3, v4

    invoke-virtual {v2, v3, v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->resetSet(IIZ)V

    goto :goto_2
.end method

.method private loadKeywords()V
    .locals 4

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->reset()V

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordScroll:F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemGapH:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemHVisibleCnt:I

    mul-int/2addr v1, v2

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getKeywordCount()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->resetSet(IIZ)V

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScrollRange:F

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setRange(FF)V

    .line 201
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScroll:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 202
    return-void
.end method

.method private pushCurrentListInfo()V
    .locals 5

    .prologue
    .line 696
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    sub-int/2addr v3, v4

    add-int/lit8 v2, v3, 0x1

    .line 697
    .local v2, "visibleCount":I
    if-gtz v2, :cond_0

    .line 718
    :goto_0
    return-void

    .line 700
    :cond_0
    new-instance v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    invoke-direct {v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;-><init>()V

    .line 701
    .local v1, "listInfo":Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    .line 702
    iput v2, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjCount:I

    .line 703
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mSelected:I

    .line 704
    new-array v3, v2, [I

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    .line 705
    new-array v3, v2, [Lcom/sec/android/gallery3d/glcore/GlObject;

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 706
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    if-ge v0, v2, :cond_1

    .line 707
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mIndexs:[I

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v4, v0

    aput v4, v3, v0

    .line 708
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mFirst:I

    add-int/2addr v4, v0

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v4

    aput-object v4, v3, v0

    .line 706
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 710
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 711
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    and-int/2addr v3, v4

    if-nez v3, :cond_2

    .line 712
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->remove()V

    .line 710
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 714
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mObjEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    iput-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    .line 715
    iget-object v3, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjectEstimator:Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;

    invoke-interface {v3}, Lcom/sec/samsung/gallery/glview/GlConvSetAnimation$OnObjectEstimaiton;->saveState()V

    .line 716
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v4, v1, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3, v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->addObjectNotToDetach([Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 717
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v3, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->saveStatus(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private reload()V
    .locals 0

    .prologue
    .line 566
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadKeywords()V

    .line 567
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadItems()V

    .line 568
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setClipRect()V

    .line 569
    return-void
.end method

.method private setBoundaryEffect()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 297
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mUsePitchBoundary:Z

    if-eqz v2, :cond_0

    .line 313
    :goto_0
    return-void

    .line 300
    :cond_0
    const/16 v0, 0x64

    .line 302
    .local v0, "boundThick":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    if-nez v2, :cond_1

    .line 303
    new-instance v2, Lcom/sec/samsung/gallery/glview/GlBlurObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v5, 0x2

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/GlBlurObject;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    .line 304
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const v3, 0x7f0203fa

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setResourceImage(I)V

    .line 306
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v1

    .line 307
    .local v1, "paddingTop":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v2, v6, v6, v1, v6}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setPadding(IIII)V

    .line 309
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/16 v3, 0x88

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    invoke-virtual {v2, v6, v3, v4, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 310
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/4 v3, 0x1

    const/16 v4, 0x18

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->setAlignAndScale(IIII)V

    .line 312
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    const/4 v3, 0x0

    invoke-virtual {v2, v6, v3}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->show(IF)V

    goto :goto_0
.end method

.method private setClipRect()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 239
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v0

    .line 240
    .local v0, "actionbarHeightLand":I
    move v1, v0

    .line 241
    .local v1, "actionbarHeightPort":I
    const/4 v2, 0x5

    .line 243
    .local v2, "gap":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemClipRect:Landroid/graphics/Rect;

    if-nez v7, :cond_1

    .line 269
    :cond_0
    return-void

    .line 246
    :cond_1
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    if-le v7, v8, :cond_2

    .line 247
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemRowCount:I

    sget v8, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_LAND:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 248
    .local v5, "rowCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemClipRect:Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v9, v0

    sget v10, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_LAND:I

    mul-int/2addr v10, v5

    sub-int/2addr v9, v10

    invoke-virtual {v7, v11, v11, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 249
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v7, v0

    sget v8, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_LAND:I

    mul-int/2addr v8, v5

    sub-int/2addr v7, v8

    add-int/lit8 v6, v7, 0x5

    .line 250
    .local v6, "top":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordClipRect:Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v9, v6

    sub-int/2addr v9, v0

    invoke-virtual {v7, v11, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 258
    :goto_0
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v7, v7

    if-ge v4, v7, :cond_3

    .line 259
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v7, v4

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 260
    .local v3, "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v3, v12}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClippingEnabled(Z)V

    .line 261
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemClipRect:Landroid/graphics/Rect;

    iput-object v7, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClipRect:Landroid/graphics/Rect;

    .line 258
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 252
    .end local v3    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    .end local v4    # "i":I
    .end local v5    # "rowCount":I
    .end local v6    # "top":I
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemRowCount:I

    sget v8, Lcom/sec/samsung/gallery/glview/GlSearchView;->MAX_KEYWORD_ROW_COUNT_PORT:I

    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 253
    .restart local v5    # "rowCount":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemClipRect:Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v9, v1

    sget v10, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_PORT:I

    mul-int/2addr v10, v5

    sub-int/2addr v9, v10

    invoke-virtual {v7, v11, v11, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    .line 254
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v7, v1

    sget v8, Lcom/sec/samsung/gallery/glview/GlSearchView;->CLIP_STEP_PORT:I

    mul-int/2addr v8, v5

    sub-int/2addr v7, v8

    add-int/lit8 v6, v7, 0x5

    .line 255
    .restart local v6    # "top":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordClipRect:Landroid/graphics/Rect;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    sub-int/2addr v9, v6

    sub-int/2addr v9, v1

    invoke-virtual {v7, v11, v6, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0

    .line 264
    .restart local v4    # "i":I
    :cond_3
    const/4 v4, 0x0

    :goto_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v7, v7

    if-ge v4, v7, :cond_0

    .line 265
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v7, v4

    check-cast v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 266
    .restart local v3    # "glThumb":Lcom/sec/samsung/gallery/glview/GlThumbObject;
    invoke-virtual {v3, v12}, Lcom/sec/samsung/gallery/glview/GlThumbObject;->setClippingEnabled(Z)V

    .line 267
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordClipRect:Landroid/graphics/Rect;

    iput-object v7, v3, Lcom/sec/samsung/gallery/glview/GlThumbObject;->mClipRect:Landroid/graphics/Rect;

    .line 264
    add-int/lit8 v4, v4, 0x1

    goto :goto_2
.end method

.method private setScaleFactor(ZI)V
    .locals 0
    .param p1, "wide"    # Z
    .param p2, "colCount"    # I

    .prologue
    .line 579
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHorizontalCount()V

    .line 580
    return-void
.end method

.method private startKeywordDeleteAnimation(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 522
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->getObject(I)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlThumbObject;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    .line 523
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThumbDeleting:Lcom/sec/samsung/gallery/glview/GlThumbObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->startAnimation(Lcom/sec/android/gallery3d/glcore/GlObject;)V

    .line 524
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I

    .line 525
    return-void
.end method

.method private switchKeyboardFocus(Z)V
    .locals 4
    .param p1, "toKeyword"    # Z

    .prologue
    const/4 v3, 0x0

    .line 809
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    .line 810
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v2, :cond_1

    .line 811
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 812
    .local v0, "focusedIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    .line 813
    const/4 v1, 0x0

    .line 819
    .local v1, "focusedPart":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v2, :cond_0

    .line 820
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-interface {v2, v3, v0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 822
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->update(I)V

    .line 823
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    .line 824
    return-void

    .line 815
    .end local v0    # "focusedIndex":I
    .end local v1    # "focusedPart":I
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 816
    .restart local v0    # "focusedIndex":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iput v0, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    .line 817
    const/4 v1, 0x1

    .restart local v1    # "focusedPart":I
    goto :goto_0
.end method


# virtual methods
.method protected doResize()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 584
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->pushCurrentListInfo()V

    .line 585
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->restoreStatus()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    .line 587
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

    if-nez v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 592
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    if-eqz v0, :cond_1

    .line 593
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWideMode:Z

    if-eqz v0, :cond_2

    .line 594
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    invoke-interface {v0, v1, v4, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 599
    :cond_1
    :goto_1
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimType:I

    .line 600
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mLockPos:Z

    .line 601
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 602
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->reset()V

    .line 603
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 605
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadItems(Z)V

    .line 606
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setClipRect()V

    .line 607
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->convAnimResize()V

    goto :goto_0

    .line 596
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    invoke-interface {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;->onStatusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    goto :goto_1
.end method

.method public getColumnCount()I
    .locals 1

    .prologue
    .line 1148
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    return v0
.end method

.method protected isScreenLocked()Z
    .locals 1

    .prologue
    .line 802
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAnimStep:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 803
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isScreenLocked()Z

    move-result v0

    goto :goto_0
.end method

.method protected loadItems()V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadItems(Z)V

    .line 274
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 2
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    const/4 v1, 0x1

    .line 552
    if-ne p1, v1, :cond_0

    .line 553
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->reload()V

    .line 563
    :goto_0
    return-void

    .line 554
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_2

    .line 555
    if-ne p4, v1, :cond_1

    .line 556
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->update(I)V

    goto :goto_0

    .line 558
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, p3}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    goto :goto_0

    .line 561
    :cond_2
    invoke-super/range {p0 .. p5}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onCommand(ILjava/lang/Object;III)V

    goto :goto_0
.end method

.method protected onCreate()V
    .locals 8

    .prologue
    const-wide/16 v6, 0xbb8

    const/4 v5, 0x1

    .line 81
    invoke-virtual {p0, v5}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setClearByColor(Z)V

    .line 82
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidthSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSPKeyString:Ljava/lang/String;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewDefaultMode()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadIntKey(Landroid/content/Context;Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailViewMode(I)V

    .line 87
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewShowSplitModeCount()I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailViewMode(I)V

    .line 88
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHorizontalCount()V

    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->freeRectangles()V

    .line 92
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->createKeywordRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iput-boolean v5, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mInitVisible:Z

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 96
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 97
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 99
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->createRectangles()[Lcom/sec/samsung/gallery/glview/GlThumbObject;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .line 100
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->register([Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    const v1, 0x3f19999a    # 0.6f

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setFactors(FF)V

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setDuration(J)V

    .line 103
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItem:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setBoundaryAnimationListener(Lcom/sec/samsung/gallery/glview/GlBoundaryAnim$GlBoundaryAnimListener;)V

    .line 104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlDeleteAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 107
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 109
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerDelete:Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlRepositionAnim;->setAnimationListener(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;)V

    .line 110
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 112
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setBoundaryEffect()V

    .line 114
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSoundUtils()Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSoundUtil:Lcom/sec/samsung/gallery/view/utils/SoundUtils;

    .line 116
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getKeywordCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x6

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->setScrollStep(I)V

    .line 117
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mScroll:F

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 118
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 126
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->freeRectangles()V

    .line 127
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDeleteAnim:Lcom/sec/samsung/gallery/glview/GlDeleteAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 128
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mReposAnim:Lcom/sec/samsung/gallery/glview/GlRepositionAnim;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 129
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mBoundaryEffect:Lcom/sec/samsung/gallery/glview/GlBlurObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBlurObject;->destroy()V

    goto :goto_0
.end method

.method protected onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 828
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 903
    :goto_0
    return v0

    .line 832
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemCount:I

    if-nez v0, :cond_2

    .line 833
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0

    .line 834
    :cond_2
    const/16 v0, 0x15

    if-eq p1, v0, :cond_3

    const/16 v0, 0x16

    if-eq p1, v0, :cond_3

    const/16 v0, 0x13

    if-eq p1, v0, :cond_3

    const/16 v0, 0x14

    if-eq p1, v0, :cond_3

    const/16 v0, 0x17

    if-eq p1, v0, :cond_3

    const/16 v0, 0x42

    if-eq p1, v0, :cond_3

    const/16 v0, 0x3d

    if-ne p1, v0, :cond_4

    .line 837
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->stop()V

    .line 839
    :cond_4
    sparse-switch p1, :sswitch_data_0

    move v0, v2

    .line 903
    goto :goto_0

    .line 841
    :sswitch_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v0, :cond_6

    .line 842
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    if-nez v0, :cond_5

    .line 843
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->onFocusChange(I)V

    move v0, v2

    .line 844
    goto :goto_0

    .line 846
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 848
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-nez v0, :cond_7

    .line 849
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->switchKeyboardFocus(Z)V

    move v0, v1

    .line 853
    goto :goto_0

    .line 851
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 855
    :sswitch_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v0, :cond_a

    .line 856
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemCount:I

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_9

    .line 857
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->switchKeyboardFocus(Z)V

    :cond_8
    move v0, v1

    .line 864
    goto :goto_0

    .line 859
    :cond_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->moveTo(I)Z

    move-result v0

    goto :goto_0

    .line 861
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_8

    .line 862
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v4}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 866
    :sswitch_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v0, :cond_c

    .line 867
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->firstRowFocused()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 868
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->onFocusChange(I)V

    move v0, v2

    .line 869
    goto/16 :goto_0

    .line 871
    :cond_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 873
    :cond_c
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->firstRowFocused()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 874
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->switchKeyboardFocus(Z)V

    move v0, v1

    .line 878
    goto/16 :goto_0

    .line 876
    :cond_d
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 880
    :sswitch_3
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v0, :cond_11

    .line 881
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->lastRowFocused()Z

    move-result v0

    if-eqz v0, :cond_10

    .line 882
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getGenericMotionKeywordFocus()I

    move-result v0

    if-ne v0, v3, :cond_f

    .line 883
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->onFocusChange(I)V

    :cond_e
    :goto_1
    move v0, v1

    .line 892
    goto/16 :goto_0

    .line 885
    :cond_f
    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->switchKeyboardFocus(Z)V

    goto :goto_1

    .line 887
    :cond_10
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 889
    :cond_11
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlScroller;->lastRowFocused()Z

    move-result v0

    if-nez v0, :cond_e

    .line 890
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlScroller;->moveTo(I)Z

    move-result v0

    goto/16 :goto_0

    .line 894
    :sswitch_4
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeyboardKeywordFocused:Z

    if-eqz v0, :cond_13

    .line 895
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    if-gez v0, :cond_12

    move v0, v1

    goto/16 :goto_0

    .line 896
    :cond_12
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerKeywordClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mKeywordCtrl:Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlSearchView$KeywordScroller;->mItemFocused:I

    aget-object v2, v2, v3

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;->onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    :goto_2
    move v0, v1

    .line 901
    goto/16 :goto_0

    .line 898
    :cond_13
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    if-gez v0, :cond_14

    move v0, v1

    goto/16 :goto_0

    .line 899
    :cond_14
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mListenerItemClick:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemFocused:I

    aget-object v2, v2, v3

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;->onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    goto :goto_2

    .line 839
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onMoved(II)Z
    .locals 4
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    const/4 v3, 0x1

    .line 331
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    :goto_0
    return v3

    .line 333
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z

    if-eqz v0, :cond_1

    .line 334
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0

    .line 336
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p2

    int-to-float v1, v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setMovement(F)V

    goto :goto_0
.end method

.method protected onPressed(II)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v3, 0x1

    .line 317
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 326
    :goto_0
    return v3

    .line 320
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidth:I

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    if-le v1, v2, :cond_1

    sget v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->SPLIT_RATIO_LAND:F

    .line 321
    .local v0, "ratio":F
    :goto_1
    int-to-float v1, p2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 322
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z

    goto :goto_0

    .line 320
    .end local v0    # "ratio":F
    :cond_1
    sget v0, Lcom/sec/samsung/gallery/glview/GlSearchView;->SPLIT_RATIO_PORT:F

    goto :goto_1

    .line 324
    .restart local v0    # "ratio":F
    :cond_2
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z

    goto :goto_0
.end method

.method protected onReleased(IIII)Z
    .locals 3
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v2, 0x1

    .line 348
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->isScreenLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    :goto_0
    return v2

    .line 351
    :cond_0
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mPressedUpper:Z

    if-eqz v0, :cond_1

    .line 352
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnimKeyword:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    goto :goto_0

    .line 354
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    neg-int v1, p4

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->startFling(F)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)V
    .locals 4
    .param p1, "detector"    # Landroid/view/ScaleGestureDetector;

    .prologue
    const/4 v3, 0x0

    .line 657
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mLockPos:Z

    if-eqz v2, :cond_1

    .line 675
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    .line 661
    .local v1, "scaleFactor":F
    const v2, 0x3f8147ae    # 1.01f

    cmpl-float v2, v1, v2

    if-lez v2, :cond_2

    .line 662
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailViewMode(I)V

    .line 669
    :goto_1
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    .line 670
    .local v0, "oldColCount":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHorizontalCount()V

    .line 671
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mColCnt:I

    if-eq v2, v0, :cond_0

    .line 672
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    .line 673
    const/4 v2, 0x6

    invoke-virtual {p0, v2, v3, v3, v3}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    goto :goto_0

    .line 663
    .end local v0    # "oldColCount":I
    :cond_2
    const v2, 0x3f7d70a4    # 0.99f

    cmpg-float v2, v1, v2

    if-gez v2, :cond_0

    .line 664
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setThumbnailViewMode(I)V

    goto :goto_1
.end method

.method protected onScrolled(IIII)Z
    .locals 2
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    const/4 v1, 0x0

    .line 343
    invoke-virtual {p0, p1, p2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->onMoved(II)Z

    move-result v0

    invoke-virtual {p0, v1, v1, v1, v1}, Lcom/sec/samsung/gallery/glview/GlSearchView;->onReleased(IIII)Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method protected resetLayout()V
    .locals 5

    .prologue
    .line 186
    invoke-super {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mWidthSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mHeightSpace:F

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHeight()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->setDimensionUtil(FFII)V

    .line 188
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->getHorizontalCount()V

    .line 189
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mFocused:I

    .line 190
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadKeywords()V

    .line 191
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->loadItems()V

    .line 192
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setClipRect()V

    .line 193
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setBoundaryEffect()V

    .line 194
    return-void
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 3
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V

    .line 138
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_1

    .line 140
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlSearchView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlSearchView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlSearchView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setCommand(IIII)V

    .line 163
    :cond_1
    return-void
.end method

.method public setAttrs(II)V
    .locals 1
    .param p1, "attr"    # I
    .param p2, "parm"    # I

    .prologue
    const/4 v0, 0x1

    .line 572
    if-ne p1, v0, :cond_1

    .line 573
    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setScaleFactor(ZI)V

    .line 576
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    .line 575
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/sec/samsung/gallery/glview/GlSearchView;->setScaleFactor(ZI)V

    goto :goto_0
.end method

.method public setThumbnailPitchRate(FLcom/sec/samsung/gallery/glview/GlScroller;)V
    .locals 6
    .param p1, "rate"    # F
    .param p2, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 1152
    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-nez v4, :cond_1

    .line 1170
    :cond_0
    return-void

    .line 1156
    :cond_1
    iget v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    iget-object v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v5

    if-le v4, v5, :cond_2

    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v4

    .line 1157
    .local v1, "count":I
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 1158
    iget-object v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v2

    .line 1159
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_3

    .line 1157
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1156
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    iget v1, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVisibleCnt:I

    goto :goto_0

    .line 1162
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVVisibleCnt:I

    div-int v0, v4, v5

    .line 1164
    .local v0, "col":I
    if-eqz v0, :cond_4

    iget v4, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemCount:I

    iget v5, p2, Lcom/sec/samsung/gallery/glview/GlScroller;->mItemVVisibleCnt:I

    div-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    if-nez v4, :cond_5

    .line 1165
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2

    .line 1167
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2
.end method

.method public setThumbnailViewMode(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 678
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewShowSplitModeCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewModeCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-le p1, v0, :cond_1

    .line 683
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    .line 682
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mSPKeyString:Ljava/lang/String;

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlSearchView;->mViewMode:I

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method
