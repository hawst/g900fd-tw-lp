.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;
.super Lcom/sec/android/gallery3d/ui/GLView;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V
    .locals 0

    .prologue
    .line 505
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/ui/GLView;-><init>()V

    return-void
.end method

.method private renderImageNote(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;F)V
    .locals 2
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "angle"    # F

    .prologue
    .line 678
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v0

    .line 679
    .local v0, "imageNoteView":Lcom/sec/android/gallery3d/ui/ImageNoteView;
    if-eqz v0, :cond_1

    .line 680
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->save(I)V

    .line 681
    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->renderRotateAnim(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;F)V

    .line 682
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 683
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p2, v1

    if-ltz v1, :cond_0

    .line 684
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 686
    :cond_0
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->restore()V

    .line 688
    :cond_1
    return-void
.end method


# virtual methods
.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/DragEvent;

    .prologue
    const/4 v3, 0x0

    .line 886
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v4

    if-nez v4, :cond_1

    .line 904
    :cond_0
    :goto_0
    return v3

    .line 888
    :cond_1
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipData()Landroid/content/ClipData;

    move-result-object v0

    .line 889
    .local v0, "clipData":Landroid/content/ClipData;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ClipData;->getItemCount()I

    move-result v4

    if-lez v4, :cond_0

    .line 891
    invoke-virtual {v0, v3}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v1

    .line 892
    .local v1, "item":Landroid/content/ClipData$Item;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 895
    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "cropUri"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1}, Landroid/view/DragEvent;->getClipDescription()Landroid/content/ClipDescription;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ClipDescription;->getLabel()Ljava/lang/CharSequence;

    move-result-object v4

    const-string v5, "selectedUri"

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 898
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v5

    invoke-virtual {v1}, Landroid/content/ClipData$Item;->getUri()Landroid/net/Uri;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Lcom/sec/android/gallery3d/data/Path;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 899
    .local v2, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_3

    .line 900
    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5000()Ljava/lang/String;

    move-result-object v4

    const-string v5, "mediaItem is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 903
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v3

    const v4, 0x7f0e02e4

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 904
    const/4 v3, 0x1

    goto :goto_0
.end method

.method public dispatchHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 867
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 868
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HEBT"

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHoverGestureDetector:Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/glcore/GlHoverGestureDetector;->dispatchHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 875
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mLockTouchEvent:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 876
    const/4 v0, 0x0

    .line 880
    :goto_0
    return v0

    .line 877
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 878
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->removeView()V

    .line 880
    :cond_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onGenericMotionCancel()V
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOnGenericMotionListener:Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$5200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/sec/android/gallery3d/ui/OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 910
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 8
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const-wide/16 v6, 0xc8

    const-wide/16 v4, 0x10

    const/16 v3, 0x68

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 697
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    if-nez v2, :cond_1

    .line 815
    :cond_0
    :goto_0
    return v0

    .line 700
    :cond_1
    sparse-switch p1, :sswitch_data_0

    .line 813
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 814
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    goto :goto_0

    .line 703
    :sswitch_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 712
    :cond_2
    invoke-virtual {p2}, Landroid/view/KeyEvent;->startTracking()V

    move v0, v1

    .line 713
    goto :goto_0

    .line 717
    :sswitch_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 718
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 719
    goto :goto_0

    .line 721
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 722
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 723
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 724
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto/16 :goto_0

    .line 729
    :sswitch_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBarsForAShortTime()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 730
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    iget-boolean v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDragDownFromSelectAll:Z

    if-nez v2, :cond_4

    .line 731
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 738
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 733
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    iput-boolean v0, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mDragDownFromSelectAll:Z

    .line 734
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 735
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setFocusable(Z)V

    .line 736
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->requestFocus()Z

    goto :goto_1

    .line 740
    :sswitch_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 741
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 742
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 743
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 744
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->previousImage()Z

    move v0, v1

    .line 745
    goto/16 :goto_0

    .line 750
    :sswitch_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 751
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->showBarsForAShortTime()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)V

    .line 752
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    .line 753
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resetToFullView()V

    .line 754
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->nextImage()Z

    move v0, v1

    .line 755
    goto/16 :goto_0

    .line 761
    :sswitch_5
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 764
    :cond_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-nez v0, :cond_6

    .line 765
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v2, 0x3f866666    # 1.05f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 766
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0x66

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 767
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0x65

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    move v0, v1

    .line 769
    goto/16 :goto_0

    .line 771
    :sswitch_6
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 774
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/high16 v2, 0x3fa00000    # 1.25f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 775
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 776
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 777
    goto/16 :goto_0

    .line 780
    :sswitch_7
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 783
    :cond_8
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-nez v0, :cond_9

    .line 784
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v2, 0x3f733333    # 0.95f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 785
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0x65

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 786
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/16 v2, 0x66

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_9
    move v0, v1

    .line 788
    goto/16 :goto_0

    .line 790
    :sswitch_8
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v2

    if-nez v2, :cond_0

    .line 793
    :cond_a
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const/high16 v2, 0x3f400000    # 0.75f

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 794
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 795
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 796
    goto/16 :goto_0

    .line 798
    :sswitch_9
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->toggleScale()V

    .line 799
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 800
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v3, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    .line 801
    goto/16 :goto_0

    .line 803
    :sswitch_a
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v2

    instance-of v2, v2, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    if-eqz v2, :cond_0

    .line 804
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getAction()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailActionBarForNormal;->showDeleteDialog()V

    move v0, v1

    .line 806
    goto/16 :goto_0

    .line 811
    :sswitch_b
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/gallery3d/ui/PhotoView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0

    .line 700
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x15 -> :sswitch_3
        0x16 -> :sswitch_4
        0x17 -> :sswitch_0
        0x25 -> :sswitch_5
        0x2b -> :sswitch_7
        0x42 -> :sswitch_0
        0x70 -> :sswitch_a
        0x71 -> :sswitch_b
        0x72 -> :sswitch_b
        0xa8 -> :sswitch_5
        0xa9 -> :sswitch_7
        0x117 -> :sswitch_8
        0x118 -> :sswitch_6
        0x119 -> :sswitch_9
    .end sparse-switch
.end method

.method public onKeyLongPress(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 847
    const/16 v2, 0xa8

    if-eq p1, v2, :cond_0

    const/16 v2, 0x25

    if-eq p1, v2, :cond_0

    const/16 v2, 0xa9

    if-eq p1, v2, :cond_0

    const/16 v2, 0x2b

    if-ne p1, v2, :cond_1

    .line 862
    :cond_0
    :goto_0
    return v1

    .line 850
    :cond_1
    const/16 v2, 0x42

    if-eq p1, v2, :cond_2

    const/16 v2, 0x17

    if-ne p1, v2, :cond_4

    .line 851
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4102(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    .line 852
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 853
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x4

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v2

    if-nez v2, :cond_3

    .line 855
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/FilmStripView;->enterSelectionMode()V

    .line 856
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onLongTap(I)V

    .line 857
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto :goto_0

    .line 860
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 862
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_4
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyLongPress(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto/16 :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 821
    const/16 v1, 0x118

    if-eq p1, v1, :cond_0

    const/16 v1, 0x117

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa8

    if-eq p1, v1, :cond_0

    const/16 v1, 0x25

    if-eq p1, v1, :cond_0

    const/16 v1, 0xa9

    if-eq p1, v1, :cond_0

    const/16 v1, 0x2b

    if-ne p1, v1, :cond_3

    .line 824
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    const-wide/16 v2, 0x10

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 825
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    .line 842
    :cond_1
    :goto_0
    return v0

    .line 827
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->endScale()V

    goto :goto_0

    .line 829
    :cond_3
    const/16 v1, 0x42

    if-eq p1, v1, :cond_4

    const/16 v1, 0x17

    if-ne p1, v1, :cond_8

    .line 830
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->playCurrentMedia(Z)Z
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    move-result v1

    if-nez v1, :cond_1

    .line 832
    :cond_5
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 833
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->isLongPressKey:Z
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4102(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    goto :goto_0

    .line 836
    :cond_6
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->isFocusable()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v0

    if-ne v0, v3, :cond_8

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 839
    :cond_7
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mCurrentIndex:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$4200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->onSingleTapUp(I)V

    .line 842
    :cond_8
    invoke-super {p0, p1, p2}, Lcom/sec/android/gallery3d/ui/GLView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 26
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 508
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v21

    const/16 v22, 0x0

    const/16 v23, 0x0

    sub-int v24, p4, p2

    sub-int v25, p5, p3

    invoke-virtual/range {v21 .. v25}, Lcom/sec/android/gallery3d/ui/PhotoView;->layout(IIII)V

    .line 511
    const/4 v14, 0x0

    .line 513
    .local v14, "modifiedGapForFilmStripView":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v21

    if-eqz v21, :cond_0

    sget-boolean v21, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-nez v21, :cond_0

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v21

    if-eqz v21, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v21

    if-eqz v21, :cond_0

    .line 515
    const/16 v14, 0x30

    .line 519
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v21

    if-eqz v21, :cond_2

    .line 520
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v15

    .line 522
    .local v15, "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    :try_start_0
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/ui/PositionController;->getUseViewSize()Z

    move-result v21

    if-nez v21, :cond_6

    .line 523
    new-instance v16, Landroid/graphics/Rect;

    const/16 v21, 0x0

    move/from16 v0, v21

    invoke-virtual {v15, v0}, Lcom/sec/android/gallery3d/ui/PositionController;->getPosition(I)Landroid/graphics/Rect;

    move-result-object v21

    move-object/from16 v0, v16

    move-object/from16 v1, v21

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 524
    .local v16, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v21

    if-eqz v21, :cond_1

    .line 526
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getActionBar()Landroid/app/ActionBar;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/app/ActionBar;->getHeight()I

    move-result v5

    .line 527
    .local v5, "actionBarHeight":I
    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v0, v5, :cond_1

    .line 528
    move-object/from16 v0, v16

    iput v5, v0, Landroid/graphics/Rect;->top:I

    .line 531
    .end local v5    # "actionBarHeight":I
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v21

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->top:I

    move/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v23

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v24, v0

    move/from16 v0, v24

    int-to-float v0, v0

    move/from16 v24, v0

    move-object/from16 v0, v16

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-float v0, v0

    move/from16 v25, v0

    move-object/from16 v0, v23

    move/from16 v1, v24

    move/from16 v2, v25

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getCorrectRightFromPhotoView(FFI)I

    move-result v23

    move-object/from16 v0, v21

    move/from16 v1, p2

    move/from16 v2, v22

    move/from16 v3, v23

    move/from16 v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->layout(IIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 540
    .end local v15    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    .end local v16    # "r":Landroid/graphics/Rect;
    :cond_2
    :goto_0
    const/4 v11, 0x0

    .line 541
    .local v11, "filmStripHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v21

    if-eqz v21, :cond_3

    .line 542
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$PhotoPage;

    move-result-object v8

    .line 543
    .local v8, "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v21

    sub-int v22, p4, p2

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/gallery3d/ui/FilmStripView;->measure(II)V

    .line 546
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/FilmStripView;->getMeasuredHeight()I

    move-result v11

    .line 547
    sub-int v18, p4, p2

    .line 548
    .local v18, "width":I
    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripContentSize:I

    move/from16 v21, v0

    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$PhotoPage;->filmstripMidMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static/range {v22 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v22

    mul-int v9, v21, v22

    .line 550
    .local v9, "contentWidth":I
    move/from16 v0, v18

    if-ge v0, v9, :cond_7

    move/from16 v6, v18

    .line 552
    .local v6, "barSize":I
    :goto_1
    const/4 v7, 0x2

    .line 553
    .local v7, "barStart":I
    add-int/lit8 v13, v18, -0x4

    .line 555
    .local v13, "maxBarSize":I
    if-lt v6, v13, :cond_8

    .line 556
    move v6, v13

    .line 561
    :goto_2
    sget-boolean v21, Lcom/sec/android/gallery3d/ui/FilmStripView;->mUse3DFilmStrip:Z

    if-eqz v21, :cond_9

    .line 562
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v21

    const/16 v22, 0x0

    sub-int v23, p5, p3

    sub-int v23, v23, v11

    sub-int v23, v23, v14

    sub-int v24, p4, p2

    sub-int v25, p5, p3

    sub-int v25, v25, v14

    invoke-virtual/range {v21 .. v25}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    .line 574
    .end local v6    # "barSize":I
    .end local v7    # "barStart":I
    .end local v8    # "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    .end local v9    # "contentWidth":I
    .end local v13    # "maxBarSize":I
    .end local v18    # "width":I
    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v21

    sub-int v22, p5, p3

    sub-int v22, v22, v11

    sub-int v22, v22, v14

    invoke-virtual/range {v21 .. v22}, Lcom/sec/android/gallery3d/ui/PhotoView;->setAboveFilmStripHeight(I)V

    .line 575
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;

    move-result-object v21

    if-eqz v21, :cond_4

    .line 576
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/Config$SideMirror;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$SideMirror;

    move-result-object v8

    .line 577
    .local v8, "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;

    move-result-object v21

    sub-int v22, p4, p2

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->measure(II)V

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->getMeasuredHeight()I

    move-result v11

    .line 581
    sub-int v18, p4, p2

    .line 582
    .restart local v18    # "width":I
    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorContentSize:I

    move/from16 v21, v0

    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorMidMargin:I

    move/from16 v22, v0

    add-int v21, v21, v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/FilterDeleteSet;
    invoke-static/range {v22 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/data/FilterDeleteSet;

    move-result-object v22

    invoke-virtual/range {v22 .. v22}, Lcom/sec/android/gallery3d/data/FilterDeleteSet;->getMediaItemCount()I

    move-result v22

    mul-int v9, v21, v22

    .line 584
    .restart local v9    # "contentWidth":I
    move/from16 v0, v18

    if-ge v0, v9, :cond_a

    move/from16 v6, v18

    .line 586
    .restart local v6    # "barSize":I
    :goto_4
    const/4 v7, 0x2

    .line 587
    .restart local v7    # "barStart":I
    add-int/lit8 v13, v18, -0x4

    .line 589
    .restart local v13    # "maxBarSize":I
    if-lt v6, v13, :cond_b

    .line 590
    move v6, v13

    .line 595
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorX:F
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v19, v0

    .line 596
    .local v19, "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v20

    .line 597
    .local v20, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2200(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_c

    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidthLand:I

    move/from16 v17, v0

    .line 598
    .local v17, "w":I
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorView:Lcom/sec/android/gallery3d/ui/SideMirrorView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorView;

    move-result-object v21

    add-int v22, v19, v17

    sub-int v23, p5, p3

    sub-int v23, v23, v14

    sub-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SideMirrorView;->layout(IIII)V

    .line 602
    .end local v6    # "barSize":I
    .end local v7    # "barStart":I
    .end local v8    # "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    .end local v9    # "contentWidth":I
    .end local v13    # "maxBarSize":I
    .end local v17    # "w":I
    .end local v18    # "width":I
    .end local v19    # "x":I
    .end local v20    # "y":I
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    move-result-object v21

    if-eqz v21, :cond_5

    .line 603
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    move-result-object v21

    sub-int v22, p4, p2

    const/high16 v23, 0x40000000    # 2.0f

    invoke-static/range {v22 .. v23}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v22

    const/16 v23, 0x0

    invoke-virtual/range {v21 .. v23}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->measure(II)V

    .line 607
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-static/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/Config$SideMirror;->get(Landroid/content/Context;)Lcom/sec/android/gallery3d/app/Config$SideMirror;

    move-result-object v8

    .line 608
    .restart local v8    # "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->getMeasuredHeight()I

    move-result v11

    .line 609
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorX:F
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v21

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v19, v0

    .line 610
    .restart local v19    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeightPixel()I

    move-result v20

    .line 611
    .restart local v20    # "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v21

    move-object/from16 v0, v21

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v21, v0

    const/16 v22, 0x2

    move/from16 v0, v21

    move/from16 v1, v22

    if-ne v0, v1, :cond_d

    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidthLand:I

    move/from16 v17, v0

    .line 612
    .restart local v17    # "w":I
    :goto_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorMotionView:Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;

    move-result-object v21

    add-int v22, v19, v17

    sub-int v23, p5, p3

    sub-int v23, v23, v11

    move-object/from16 v0, v21

    move/from16 v1, v19

    move/from16 v2, v20

    move/from16 v3, v22

    move/from16 v4, v23

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/sec/android/gallery3d/ui/SideMirrorMotionView;->layout(IIII)V

    .line 615
    .end local v8    # "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    .end local v17    # "w":I
    .end local v19    # "x":I
    .end local v20    # "y":I
    :cond_5
    return-void

    .line 533
    .end local v11    # "filmStripHeight":I
    .restart local v15    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    :cond_6
    :try_start_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->requestLayout()V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 535
    :catch_0
    move-exception v10

    .line 536
    .local v10, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v10}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto/16 :goto_0

    .end local v10    # "e":Ljava/lang/NullPointerException;
    .end local v15    # "posCtrl":Lcom/sec/android/gallery3d/ui/PositionController;
    .local v8, "config":Lcom/sec/android/gallery3d/app/Config$PhotoPage;
    .restart local v9    # "contentWidth":I
    .restart local v11    # "filmStripHeight":I
    .restart local v18    # "width":I
    :cond_7
    move v6, v9

    .line 550
    goto/16 :goto_1

    .line 558
    .restart local v6    # "barSize":I
    .restart local v7    # "barStart":I
    .restart local v13    # "maxBarSize":I
    :cond_8
    sub-int v21, v18, v6

    div-int/lit8 v7, v21, 0x2

    goto/16 :goto_2

    .line 565
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1300(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    const v22, 0x7f0d0113

    invoke-virtual/range {v21 .. v22}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 567
    .local v12, "filmstripBottomPadding":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static {v0, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1402(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 568
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    sub-int v22, p5, p3

    sub-int v22, v22, v11

    sub-int v22, v22, v14

    add-int v22, v22, v12

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static/range {v21 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1502(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 569
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    add-int v22, v7, v6

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static/range {v21 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1602(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 570
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    sub-int v22, p5, p3

    sub-int v22, v22, v14

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static/range {v21 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1702(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;I)I

    .line 571
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v21, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static/range {v21 .. v21}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v22, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static/range {v22 .. v22}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v23, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static/range {v23 .. v23}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v23

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v24, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static/range {v24 .. v24}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    move-object/from16 v25, v0

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static/range {v25 .. v25}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v25

    invoke-virtual/range {v21 .. v25}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    goto/16 :goto_3

    .end local v6    # "barSize":I
    .end local v7    # "barStart":I
    .end local v12    # "filmstripBottomPadding":I
    .end local v13    # "maxBarSize":I
    .local v8, "config":Lcom/sec/android/gallery3d/app/Config$SideMirror;
    :cond_a
    move v6, v9

    .line 584
    goto/16 :goto_4

    .line 592
    .restart local v6    # "barSize":I
    .restart local v7    # "barStart":I
    .restart local v13    # "maxBarSize":I
    :cond_b
    sub-int v21, v18, v6

    div-int/lit8 v7, v21, 0x2

    goto/16 :goto_5

    .line 597
    .restart local v19    # "x":I
    .restart local v20    # "y":I
    :cond_c
    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidth:I

    move/from16 v17, v0

    goto/16 :goto_6

    .line 611
    .end local v6    # "barSize":I
    .end local v7    # "barStart":I
    .end local v9    # "contentWidth":I
    .end local v13    # "maxBarSize":I
    .end local v18    # "width":I
    :cond_d
    iget v0, v8, Lcom/sec/android/gallery3d/app/Config$SideMirror;->sidemirrorBgWidth:I

    move/from16 v17, v0

    goto/16 :goto_7
.end method

.method public render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 14
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    const/4 v13, 0x0

    const/high16 v7, 0x43340000    # 180.0f

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v11, 0x0

    .line 620
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 621
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mOutOfFocusShotHelpView:Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/common/OutOfFocusShotHelpView;->resetLayout()V

    .line 623
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mImageNoteView:Lcom/sec/android/gallery3d/ui/ImageNoteView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/ImageNoteView;

    move-result-object v3

    .line 624
    .local v3, "imageNoteView":Lcom/sec/android/gallery3d/ui/ImageNoteView;
    if-eqz v3, :cond_3

    .line 625
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->getRotateAnim()Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v4

    .line 626
    .local v4, "rotateAnim":Lcom/sec/android/gallery3d/anim/FloatAnimation;
    if-eqz v4, :cond_2

    .line 627
    const/4 v0, 0x0

    .line 628
    .local v0, "angle":F
    monitor-enter v4

    .line 629
    :try_start_0
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->get()F

    move-result v0

    .line 630
    invoke-direct {p0, p1, v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->renderImageNote(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;F)V

    .line 631
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 632
    cmpl-float v6, v0, v7

    if-ltz v6, :cond_1

    .line 633
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    const/4 v7, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2802(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Z)Z

    .line 634
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/ImageNoteView;->startImageNote()V

    .line 675
    .end local v0    # "angle":F
    .end local v4    # "rotateAnim":Lcom/sec/android/gallery3d/anim/FloatAnimation;
    :cond_1
    :goto_0
    return-void

    .line 631
    .restart local v0    # "angle":F
    .restart local v4    # "rotateAnim":Lcom/sec/android/gallery3d/anim/FloatAnimation;
    :catchall_0
    move-exception v6

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v6

    .line 638
    .end local v0    # "angle":F
    :cond_2
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mIsStartPhotoNote:Z
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2800(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 639
    invoke-direct {p0, p1, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->renderImageNote(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;F)V

    goto :goto_0

    .line 643
    .end local v4    # "rotateAnim":Lcom/sec/android/gallery3d/anim/FloatAnimation;
    :cond_3
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v6

    if-eqz v6, :cond_7

    .line 644
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->get()F

    move-result v7

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6, v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F

    .line 646
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v6

    cmpg-float v6, v6, v12

    if-gez v6, :cond_4

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 647
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v7

    sub-int/2addr v6, v7

    int-to-float v2, v6

    .line 648
    .local v2, "gap":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v6

    int-to-float v6, v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v7

    mul-float/2addr v7, v2

    sub-float v5, v6, v7

    .line 649
    .local v5, "top":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v6, v2

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v7

    mul-float/2addr v7, v2

    sub-float v1, v6, v7

    .line 651
    .local v1, "bottom":F
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v7

    float-to-int v8, v5

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v9

    float-to-int v10, v1

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    .line 654
    .end local v1    # "bottom":F
    .end local v2    # "gap":F
    .end local v5    # "top":F
    :cond_4
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v6

    cmpl-float v6, v6, v12

    if-ltz v6, :cond_6

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    if-eqz v6, :cond_6

    .line 655
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->forceStop()V

    .line 656
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 657
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v6, v13}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2902(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/anim/FloatAnimation;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 658
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6, v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F

    .line 659
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    .line 674
    :cond_5
    :goto_1
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/ui/GLView;->render(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    goto/16 :goto_0

    .line 660
    :cond_6
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v6

    cmpl-float v6, v6, v11

    if-lez v6, :cond_5

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->isActive()Z

    move-result v6

    if-nez v6, :cond_5

    .line 661
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3100(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->removeAnimation(Lcom/sec/android/gallery3d/anim/Animation;)V

    .line 662
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSAnim:Lcom/sec/android/gallery3d/anim/FloatAnimation;
    invoke-static {v6, v13}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2902(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;Lcom/sec/android/gallery3d/anim/FloatAnimation;)Lcom/sec/android/gallery3d/anim/FloatAnimation;

    .line 663
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 664
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    goto :goto_1

    .line 667
    :cond_7
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v6

    cmpl-float v6, v6, v11

    if-gtz v6, :cond_8

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)F

    move-result v6

    cmpg-float v6, v6, v12

    if-gez v6, :cond_5

    .line 668
    :cond_8
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    if-eqz v6, :cond_9

    .line 669
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1000(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v6

    iget-object v7, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSLeft:I
    invoke-static {v7}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1400(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSTop:I
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1500(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSRight:I
    invoke-static {v9}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1600(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v9

    iget-object v10, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFSBottom:I
    invoke-static {v10}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$1700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)I

    move-result v10

    invoke-virtual {v6, v7, v8, v9, v10}, Lcom/sec/android/gallery3d/ui/FilmStripView;->layout(IIII)V

    .line 670
    :cond_9
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$1;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mFilmStripCurrentProgress:F
    invoke-static {v6, v11}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$3002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F

    goto/16 :goto_1
.end method

.method protected renderBackground(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V
    .locals 0
    .param p1, "view"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;

    .prologue
    .line 692
    invoke-interface {p1}, Lcom/sec/android/gallery3d/glrenderer/GLCanvas;->clearBuffer()V

    .line 693
    return-void
.end method
