.class public Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;
.super Ljava/lang/Object;
.source "GlComposeBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewConfig"
.end annotation


# instance fields
.field public mAlbumList:Z

.field public mHideIconMinLevel:Z

.field public mInitialLevel:I

.field public mIsRTL:Z

.field public mIsSplitView:Z

.field public mIsSplitViewExpanded:Z

.field public mItemSizeScale:F

.field public mLandRatio:F

.field public mMaxLevel:I

.field public mMaxObject:I

.field public mMinLevel:I

.field public mPortRatio:F

.field public mPosCtrl:[Ljava/lang/Object;

.field public mPrevCenterObject:I

.field public mPrevScroll:F

.field public mTopGroupTitle:Z

.field public mUseEnlargeAnim:Z

.field public mUseGroupSelect:Z

.field public mUseGroupTitle:Z

.field public mUseItemSelect:Z

.field public mUseLayoutChange:Z

.field public mUsePenSelectInPickMode:Z

.field public mUseQuickScroll:Z

.field public mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 321
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 330
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mAlbumList:Z

    .line 331
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mHideIconMinLevel:Z

    .line 335
    const/16 v0, 0xd0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMaxObject:I

    .line 337
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMinLevel:I

    .line 338
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mMaxLevel:I

    .line 339
    iput v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevCenterObject:I

    .line 340
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mPrevScroll:F

    .line 341
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mUseQuickScroll:Z

    .line 342
    iput-boolean v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitViewExpanded:Z

    .line 343
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsSplitView:Z

    .line 344
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mIsRTL:Z

    .line 345
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    return-void
.end method


# virtual methods
.method public isTimeViewStateConfig()Z
    .locals 3

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    .local v0, "isTimeViewStateConfig":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eqz v1, :cond_1

    .line 350
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mViewTabType:Lcom/sec/samsung/gallery/core/TabTagType;

    sget-object v2, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_FACE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v1, v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 352
    :cond_1
    :goto_0
    return v0

    .line 350
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
