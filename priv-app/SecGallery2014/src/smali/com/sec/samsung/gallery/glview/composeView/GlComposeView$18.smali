.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/accessibility/GlComposeViewAccessibility$AccessibilityNodeInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private groupMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1

    .prologue
    .line 2077
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2079
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->groupMap:Ljava/util/HashMap;

    return-void
.end method


# virtual methods
.method public getOject(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 5
    .param p1, "id"    # I

    .prologue
    .line 2112
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->groupMap:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 2113
    .local v1, "groupId":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 2114
    const/4 v0, 0x0

    .line 2125
    :cond_0
    :goto_0
    return-object v0

    .line 2115
    :cond_1
    const/4 v0, 0x0

    .line 2116
    .local v0, "glThumb":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_2

    .line 2117
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getGroupObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    move-result-object v2

    .line 2118
    .local v2, "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v2, :cond_0

    .line 2119
    iget-object v0, v2, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 2120
    iput p1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    goto :goto_0

    .line 2123
    .end local v2    # "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    :cond_2
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    goto :goto_0
.end method

.method public update(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 11
    .param p1, "info"    # Landroid/view/accessibility/AccessibilityNodeInfo;

    .prologue
    .line 2083
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->groupMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 2084
    const/4 v5, 0x0

    .local v5, "j":I
    :goto_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v7}, Landroid/util/SparseArray;->size()I

    move-result v7

    if-gt v5, v7, :cond_4

    .line 2085
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v7, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    .line 2086
    .local v6, "obj":Ljava/lang/Object;
    instance-of v7, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    if-nez v7, :cond_1

    .line 2084
    :cond_0
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_1
    move-object v3, v6

    .line 2088
    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;

    .line 2089
    .local v3, "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    if-eqz v3, :cond_0

    .line 2090
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v7

    shl-int/lit8 v1, v7, 0x10

    .line 2091
    .local v1, "groupFirstElement":I
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroup:[Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v8

    aget-object v7, v7, v8

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupInfo;->mCount:I

    add-int v2, v1, v7

    .line 2092
    .local v2, "groupLastElement":I
    iget-object v7, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v7, :cond_0

    .line 2094
    iget-object v7, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    add-int/lit8 v8, v2, 0x1

    iput v8, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 2095
    iget-object v7, v3, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->mTitleObj:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getVisibility()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 2096
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->groupMap:Ljava/util/HashMap;

    add-int/lit8 v8, v2, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Ljava/lang/Integer;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;->getIndex()I

    move-result v10

    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2097
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    add-int/lit8 v8, v2, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 2099
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveStart:I

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v4

    .local v4, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGrpActiveEnd:I

    invoke-static {v2, v7}, Ljava/lang/Math;->min(II)I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 2100
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v7, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->getObjectIndex(I)Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-result-object v0

    .line 2101
    .local v0, "glObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    if-eqz v0, :cond_3

    iget-object v7, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-ne v7, v3, :cond_3

    .line 2102
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->groupMap:Ljava/util/HashMap;

    iget v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    new-instance v9, Ljava/lang/Integer;

    const/4 v10, -0x1

    invoke-direct {v9, v10}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2103
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$18;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v8, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    invoke-virtual {p1, v7, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 2099
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 2108
    .end local v0    # "glObject":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .end local v1    # "groupFirstElement":I
    .end local v2    # "groupLastElement":I
    .end local v3    # "groupObject":Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$GroupObject;
    .end local v4    # "i":I
    .end local v6    # "obj":Ljava/lang/Object;
    :cond_4
    return-void
.end method
