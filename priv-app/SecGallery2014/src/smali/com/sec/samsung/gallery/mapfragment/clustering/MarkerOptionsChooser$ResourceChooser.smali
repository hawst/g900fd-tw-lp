.class public Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
.source "MarkerOptionsChooser.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ResourceChooser"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;
    }
.end annotation


# instance fields
.field private final hostRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;)V
    .locals 1
    .param p1, "host"    # Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser;-><init>()V

    .line 81
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser;->hostRef:Ljava/lang/ref/WeakReference;

    .line 82
    return-void
.end method


# virtual methods
.method public choose(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 3
    .param p1, "markerOptions"    # Lcom/google/android/gms/maps/model/MarkerOptions;
    .param p2, "clusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 86
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser;->hostRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;

    .line 87
    .local v0, "host":Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;
    if-eqz v0, :cond_0

    .line 88
    invoke-interface {v0, p2}, Lcom/sec/samsung/gallery/mapfragment/clustering/MarkerOptionsChooser$ResourceChooser$Host;->getIconResource(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)I

    move-result v1

    .line 89
    .local v1, "resource":I
    invoke-static {v1}, Lcom/google/android/gms/maps/model/BitmapDescriptorFactory;->fromResource(I)Lcom/google/android/gms/maps/model/BitmapDescriptor;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->icon(Lcom/google/android/gms/maps/model/BitmapDescriptor;)Lcom/google/android/gms/maps/model/MarkerOptions;

    .line 91
    .end local v1    # "resource":I
    :cond_0
    return-void
.end method
