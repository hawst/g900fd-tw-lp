.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;
.super Ljava/lang/Object;
.source "SlideShowViewState.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 769
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v4

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_0
    if-eq v4, v3, :cond_0

    .line 770
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMultiWindow:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1100(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v1

    :goto_1
    # setter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z
    invoke-static {v4, v3}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1002(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)Z

    .line 771
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v4

    if-nez v4, :cond_3

    :goto_2
    invoke-virtual {v3, v1}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setIndicatorVisibility(Z)V

    .line 772
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$6;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mIsFullScreen:Z
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1000(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v2

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->setFullScreenMode(Z)V
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$1200(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 777
    :cond_0
    :goto_3
    return-void

    :cond_1
    move v3, v2

    .line 769
    goto :goto_0

    :cond_2
    move v3, v2

    .line 770
    goto :goto_1

    :cond_3
    move v1, v2

    .line 771
    goto :goto_2

    .line 774
    :catch_0
    move-exception v0

    .line 775
    .local v0, "e":Ljava/lang/Exception;
    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$300()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
