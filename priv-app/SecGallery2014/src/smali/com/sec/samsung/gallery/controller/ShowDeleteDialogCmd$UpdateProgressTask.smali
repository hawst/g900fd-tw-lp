.class Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;
.super Landroid/os/AsyncTask;
.source "ShowDeleteDialogCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateProgressTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$1;

    .prologue
    .line 389
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;-><init>(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 389
    check-cast p1, [Ljava/lang/Integer;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 12
    .param p1, "arg"    # [Ljava/lang/Integer;

    .prologue
    const-wide/16 v10, 0x1

    const/4 v8, 0x0

    .line 393
    aget-object v3, p1, v8

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 395
    .local v2, "totalSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 397
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 405
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 406
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v3

    invoke-virtual {v3, v10, v11, v8}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 407
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;
    invoke-static {v3}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$000(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v10, v11, v4}, Lcom/sec/samsung/gallery/view/common/CustomProgressDialog;->increaseProgress(JZ)V

    .line 395
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 398
    :catch_0
    move-exception v0

    .line 399
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 400
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 401
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    .line 402
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    const-wide/16 v4, 0xf

    # setter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J
    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$602(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;J)J

    .line 403
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    iget-object v3, v3, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mContext:Landroid/content/Context;

    const-string v4, "AverageDeleteDbOperationTime"

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd$UpdateProgressTask;->this$0:Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->mElapsedTime:J
    invoke-static {v5}, Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;->access$600(Lcom/sec/samsung/gallery/controller/ShowDeleteDialogCmd;)J

    move-result-wide v6

    invoke-static {v3, v4, v6, v7}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->saveState(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_1

    .line 410
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    const/4 v3, 0x0

    return-object v3
.end method
