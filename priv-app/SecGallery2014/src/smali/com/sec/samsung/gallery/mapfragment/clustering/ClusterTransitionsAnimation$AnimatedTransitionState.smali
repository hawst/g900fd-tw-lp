.class Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;
.super Ljava/lang/Object;
.source "ClusterTransitionsAnimation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "AnimatedTransitionState"
.end annotation


# instance fields
.field private final transitions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;",
            ">;"
        }
    .end annotation
.end field

.field private value:F


# direct methods
.method private constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 83
    .local p1, "transitions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->transitions:Ljava/util/ArrayList;

    .line 85
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/ArrayList;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$1;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/ArrayList;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$1;

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;-><init>(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;)[Lcom/google/android/gms/maps/model/LatLng;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->getPositions()[Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    return-object v0
.end method

.method private getPositions()[Lcom/google/android/gms/maps/model/LatLng;
    .locals 28

    .prologue
    .line 97
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->transitions:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    move/from16 v0, v19

    new-array v10, v0, [Lcom/google/android/gms/maps/model/LatLng;

    .line 98
    .local v10, "positions":[Lcom/google/android/gms/maps/model/LatLng;
    const/4 v7, 0x0

    .line 99
    .local v7, "i":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->transitions:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    .local v9, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    .line 100
    .local v18, "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->getOriginClusterRelevantInputPoints()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v11

    .line 101
    .local v11, "start":Lcom/google/android/gms/maps/model/LatLng;
    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->getDestinationClusterPoint()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getMapPosition()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    .line 102
    .local v6, "end":Lcom/google/android/gms/maps/model/LatLng;
    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->value:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v22, v0

    iget-wide v0, v6, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v24, v0

    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->latitude:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    mul-double v22, v22, v24

    add-double v2, v20, v22

    .line 104
    .local v2, "currentLat":D
    invoke-virtual/range {v18 .. v18}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->spans180Meridian()Z

    move-result v19

    if-nez v19, :cond_0

    .line 105
    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->value:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v22, v0

    iget-wide v0, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v24, v0

    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v26, v0

    sub-double v24, v24, v26

    mul-double v22, v22, v24

    add-double v4, v20, v22

    .line 119
    .local v4, "currentLon":D
    :goto_1
    add-int/lit8 v8, v7, 0x1

    .end local v7    # "i":I
    .local v8, "i":I
    new-instance v19, Lcom/google/android/gms/maps/model/LatLng;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    aput-object v19, v10, v7

    move v7, v8

    .line 120
    .end local v8    # "i":I
    .restart local v7    # "i":I
    goto :goto_0

    .line 114
    .end local v4    # "currentLon":D
    :cond_0
    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmpg-double v19, v20, v22

    if-gez v19, :cond_1

    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    const-wide v22, 0x4076800000000000L    # 360.0

    add-double v16, v20, v22

    .line 115
    .local v16, "shiftedStartLon":D
    :goto_2
    iget-wide v0, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    const-wide/16 v22, 0x0

    cmpg-double v19, v20, v22

    if-gez v19, :cond_2

    iget-wide v0, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v20, v0

    const-wide v22, 0x4076800000000000L    # 360.0

    add-double v14, v20, v22

    .line 116
    .local v14, "shiftedEndLon":D
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->value:F

    move/from16 v19, v0

    move/from16 v0, v19

    float-to-double v0, v0

    move-wide/from16 v20, v0

    sub-double v22, v14, v16

    mul-double v20, v20, v22

    add-double v12, v16, v20

    .line 117
    .local v12, "shiftedCurrentLon":D
    const-wide v20, 0x4076800000000000L    # 360.0

    sub-double v4, v12, v20

    .restart local v4    # "currentLon":D
    goto :goto_1

    .line 114
    .end local v4    # "currentLon":D
    .end local v12    # "shiftedCurrentLon":D
    .end local v14    # "shiftedEndLon":D
    .end local v16    # "shiftedStartLon":D
    :cond_1
    iget-wide v0, v11, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    move-wide/from16 v16, v0

    goto :goto_2

    .line 115
    .restart local v16    # "shiftedStartLon":D
    :cond_2
    iget-wide v14, v6, Lcom/google/android/gms/maps/model/LatLng;->longitude:D

    goto :goto_3

    .line 121
    .end local v2    # "currentLat":D
    .end local v6    # "end":Lcom/google/android/gms/maps/model/LatLng;
    .end local v11    # "start":Lcom/google/android/gms/maps/model/LatLng;
    .end local v16    # "shiftedStartLon":D
    .end local v18    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    :cond_3
    return-object v10
.end method


# virtual methods
.method public getTransitions()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->transitions:Ljava/util/ArrayList;

    return-object v0
.end method

.method public setValue(F)V
    .locals 0
    .param p1, "value"    # F

    .prologue
    .line 89
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitionsAnimation$AnimatedTransitionState;->value:F

    .line 90
    return-void
.end method
