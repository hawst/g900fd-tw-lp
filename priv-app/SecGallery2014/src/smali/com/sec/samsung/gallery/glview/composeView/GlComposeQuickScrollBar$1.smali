.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;
.super Ljava/lang/Object;
.source "GlComposeQuickScrollBar.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)V
    .locals 0

    .prologue
    .line 418
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    .line 435
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setFocusBorderVisible(Z)V

    .line 436
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarObj:Lcom/sec/android/gallery3d/glcore/GlObject;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->access$300(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mScrollBarView:Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->access$200(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 437
    const/4 v0, 0x1

    return v0
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const v4, 0x7f0e04bc

    const/4 v3, 0x1

    .line 425
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    invoke-virtual {v1, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->setFocusBorderVisible(Z)V

    .line 426
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseQuickScrollTypeWinset:Z

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 430
    .local v0, "description":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(Ljava/lang/String;)V

    .line 431
    return v3

    .line 429
    .end local v0    # "description":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mGroupTitle:Ljava/lang/String;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->access$100(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar$1;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;->access$000(Lcom/sec/samsung/gallery/glview/composeView/GlComposeQuickScrollBar;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "description":Ljava/lang/String;
    goto :goto_0
.end method
