.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "ItemSetScroller"
.end annotation


# instance fields
.field private mAlbumPitchRate:F

.field private mFullVisibleCnt:I

.field public mWidthL:F

.field paddingBottom:F

.field paddingLeft:F

.field paddingRight:F

.field paddingTop:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 1

    .prologue
    .line 3390
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    .line 3393
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mAlbumPitchRate:F

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    .prologue
    .line 3390
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I

    return v0
.end method


# virtual methods
.method public getAlbumPitchRate()F
    .locals 1

    .prologue
    .line 3549
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mAlbumPitchRate:F

    return v0
.end method

.method public onAnimation(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 3520
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 3521
    return-void
.end method

.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 3484
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 3485
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 3487
    :cond_0
    return-void
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 3491
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mSplitSx:F
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1502(Lcom/sec/samsung/gallery/glview/GlPhotoView;F)F

    .line 3492
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    if-eqz v0, :cond_0

    .line 3493
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 3494
    :cond_0
    return-void
.end method

.method public onScroll()V
    .locals 5

    .prologue
    .line 3504
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    if-ltz v2, :cond_4

    .line 3505
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumCtrl:Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;

    move-result-object v2

    iget v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    int-to-float v3, v3

    mul-float v1, v2, v3

    .line 3506
    .local v1, "topScrollBorder":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    sub-float v2, v1, v2

    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v3

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v3

    add-float v0, v2, v3

    .line 3509
    .local v0, "botScrollBorder":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpl-float v2, v2, v1

    if-gtz v2, :cond_0

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpg-float v2, v2, v0

    if-gez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    if-eqz v2, :cond_3

    :cond_1
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScroll:F

    cmpl-float v2, v2, v0

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    if-nez v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsFixedAlbumVisible:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getVisibility()Z

    move-result v2

    if-nez v2, :cond_4

    .line 3513
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateFixedAlbum()V

    .line 3515
    .end local v0    # "botScrollBorder":F
    .end local v1    # "topScrollBorder":F
    :cond_4
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->updateArrowObject()V

    .line 3516
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 3498
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 3499
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 3500
    :cond_0
    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 3401
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumLeftPadding(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->paddingLeft:F

    .line 3402
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumLeftPadding(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->paddingRight:F

    .line 3403
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->paddingTop:F

    .line 3404
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->paddingBottom:F

    .line 3406
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumBackgroundWidth(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mWidthL:F

    .line 3407
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidth(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    .line 3408
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeight(I)F

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    .line 3409
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemW:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumHorizontalGap(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapW:F

    .line 3410
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemH:F

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumVerticalGap(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    .line 3412
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumColumnItemCount(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemHVisibleCnt:I

    .line 3413
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumRowItemCount(I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVVisibleCnt:I

    .line 3414
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVVisibleCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVVisibleCnt:I

    .line 3415
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemHVisibleCnt:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVVisibleCnt:I

    mul-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVisibleCnt:I

    .line 3417
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mWidthSpace:F

    neg-float v0, v0

    div-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailWidth(I)F

    move-result v1

    div-float/2addr v1, v3

    add-float/2addr v0, v1

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumLeftPadding(I)F

    move-result v1

    add-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartX:F

    .line 3418
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    div-float/2addr v0, v3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumThumbnailHeight(I)F

    move-result v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumTopPadding(I)F

    move-result v2

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartY:F

    .line 3420
    sget v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemStartZ:F

    .line 3422
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getScreenHeight()F

    move-result v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->paddingTop:F

    add-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    .line 3423
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mHeightSpace:F

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    div-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mFullVisibleCnt:I

    .line 3424
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemGapH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVStart:F

    .line 3425
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVisibleH:F

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollVEnd:F

    .line 3426
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mScrollOffset:I

    .line 3427
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v1

    invoke-virtual {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->resetScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    .line 3428
    return-void
.end method

.method public setAlbumPitchRate(F)V
    .locals 6
    .param p1, "rate"    # F

    .prologue
    .line 3524
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->getAlbumPitchRate()F

    move-result v4

    cmpl-float v4, p1, v4

    if-nez v4, :cond_1

    .line 3546
    :cond_0
    return-void

    .line 3527
    :cond_1
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mAlbumPitchRate:F

    .line 3528
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    .line 3532
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVisibleCnt:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v5, v5

    if-le v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v1, v4

    .line 3533
    .local v1, "count":I
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v1, :cond_0

    .line 3534
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v2

    .line 3535
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_3

    .line 3533
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3532
    .end local v1    # "count":I
    .end local v2    # "i":I
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemVisibleCnt:I

    goto :goto_0

    .line 3538
    .restart local v1    # "count":I
    .restart local v2    # "i":I
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRowCnt:I

    div-int v0, v4, v5

    .line 3540
    .local v0, "col":I
    if-eqz v0, :cond_4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemCount:I

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRowCnt:I

    div-int/2addr v4, v5

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v0

    if-nez v4, :cond_5

    .line 3541
    :cond_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2

    .line 3543
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getAlbumViewEndAffordanceRowDegree(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, p1

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_2
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 8
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x40000000    # 2.0f

    .line 3468
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 3469
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    .line 3470
    .local v0, "mTextObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v0, :cond_0

    .line 3473
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getWidth(Z)F

    move-result v3

    neg-float v3, v3

    div-float v1, v3, v6

    .line 3474
    .local v1, "x":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxXOffset(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidth(I)F

    move-result v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    add-float/2addr v1, v3

    .line 3475
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getHeight(Z)F

    move-result v3

    neg-float v3, v3

    div-float v2, v3, v6

    .line 3476
    .local v2, "y":F
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxYOffset(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    .line 3477
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setPos(FFF)V

    .line 3478
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxWidth(I)F

    move-result v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mViewMode:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumNameTextboxHeight(I)F

    move-result v4

    invoke-virtual {v0, v3, v4}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setSize(FF)V

    .line 3480
    .end local v1    # "x":F
    .end local v2    # "y":F
    :cond_0
    return-void
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 10
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 3436
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_2

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v0, :cond_2

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->mItemCount:I

    if-ge v0, v1, :cond_2

    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 3438
    iget v0, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-eqz v0, :cond_0

    .line 3442
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iput v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mRatio:F

    .line 3443
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    iput v2, v0, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;->mAspectRatioCorrection:F

    .line 3445
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThisListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    .line 3446
    .local v7, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 3448
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v9

    .line 3449
    .local v9, "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    if-eqz v9, :cond_1

    .line 3450
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/glview/GlTextObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    .line 3452
    .local v6, "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3453
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getChildTextObject()Lcom/sec/samsung/gallery/glview/GlTextObject;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/sec/samsung/gallery/glview/GlTextObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 3456
    .end local v6    # "glTextView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAlbumFocused:I

    iget v1, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ne v0, v1, :cond_3

    .line 3457
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$ItemSetScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    iput-object v0, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderObjecSel:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .line 3458
    const/4 v8, 0x1

    .line 3462
    .local v8, "selected":Z
    :goto_0
    check-cast p1, Lcom/sec/samsung/gallery/glview/GlSplitObject;

    .end local p1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-virtual {p1, v8, v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setSelected(ZZ)V

    .line 3464
    .end local v7    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v8    # "selected":Z
    .end local v9    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    :cond_2
    return-void

    .line 3460
    .restart local v7    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    .restart local v9    # "textObj":Lcom/sec/samsung/gallery/glview/GlTextObject;
    .restart local p1    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_3
    const/4 v8, 0x0

    .restart local v8    # "selected":Z
    goto :goto_0
.end method
