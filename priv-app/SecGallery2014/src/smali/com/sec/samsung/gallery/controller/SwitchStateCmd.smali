.class public Lcom/sec/samsung/gallery/controller/SwitchStateCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "SwitchStateCmd.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 5
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 16
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 18
    .local v3, "params":[Ljava/lang/Object;
    const/4 v4, 0x0

    aget-object v2, v3, v4

    check-cast v2, Ljava/lang/Class;

    .line 19
    .local v2, "klass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/sec/android/gallery3d/app/ActivityState;>;"
    const/4 v4, 0x1

    aget-object v1, v3, v4

    check-cast v1, Landroid/os/Bundle;

    .line 20
    .local v1, "bundle":Landroid/os/Bundle;
    const/4 v4, 0x2

    aget-object v0, v3, v4

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 21
    .local v0, "activity":Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v4

    invoke-virtual {v4, v2, v1}, Lcom/sec/android/gallery3d/app/StateManager;->switchState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 22
    return-void
.end method
