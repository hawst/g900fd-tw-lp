.class Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;
.super Ljava/lang/Object;
.source "SlideShowViewState.java"

# interfaces
.implements Lcom/samsung/android/motion/MRListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionListener(Lcom/samsung/android/motion/MREvent;)V
    .locals 2
    .param p1, "motionEvent"    # Lcom/samsung/android/motion/MREvent;

    .prologue
    .line 306
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # invokes: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->isMusicEnabled()Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$500(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/motion/MREvent;->getMotion()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->isAudioPathEarjack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 308
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->mMusicPlayer:Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;->access$600(Lcom/sec/samsung/gallery/view/slideshowview/SlideShowViewState;)Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowview/MusicPlayer;->pauseMusic()V

    .line 311
    :cond_0
    return-void
.end method
