.class public Lcom/sec/samsung/gallery/glview/GlAbsListView;
.super Lcom/sec/android/gallery3d/glcore/GlLayer;
.source "GlAbsListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;,
        Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;
    }
.end annotation


# static fields
.field protected static final CMD_NOTIFY_UPDATE:I = 0x3

.field protected static final CMD_REQ_LOAD:I = 0x1

.field protected static final CMD_RESET_LAYOUT:I = 0x9

.field protected static final CMD_RESIZE_REQ:I = 0x6

.field protected static final CMD_SCALE_UNLOCK:I = 0x8

.field protected static final CMD_SCROLL_DONE:I = 0x2

.field protected static final CMD_START_CONV_ANIM:I = 0x5

.field protected static final CMD_UPDATE_ALL:I = 0x7

.field protected static final CMD_UPDATE_ITEM:I = 0x4

.field public static DEF_DISTANCE:F = 0.0f

.field protected static DEF_FOV:F = 0.0f

.field public static LIST_MODE_NEW_ALBUM:I = 0x0

.field public static LIST_MODE_NEW_ALBUM_EX:I = 0x0

.field public static LIST_MODE_NORMAL:I = 0x0

.field public static LIST_MODE_SELECT:I = 0x0

.field protected static LOCK_FLING_TIMEOUT:J = 0x0L

.field public static final MAX_GENERIC_MOTION:I = 0x4

.field public static SAVE_OBJINFO_FORANIM:I = 0x0

.field public static SCROLL_STATE_FLING:I = 0x0

.field public static SCROLL_STATE_IDLE:I = 0x0

.field public static SCROLL_STATE_TOUCH_SCROLL:I = 0x0

.field public static final STATUS_COLUMN_PORT:I = 0x2

.field public static final STATUS_COLUMN_WIDE:I = 0x1

.field public static final STATUS_REORDER_CANCEL:I = 0x5

.field public static final STATUS_REORDER_DONE:I = 0x4

.field public static final STATUS_REORDER_START:I = 0x3

.field private static final TAG:Ljava/lang/String; = "GlAbsListView"


# instance fields
.field protected mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field protected mAnimStatus:I

.field protected mAnimType:I

.field protected mColCnt:I

.field protected mContext:Landroid/content/Context;

.field protected mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

.field protected mFlags:I

.field protected mFocused:I

.field protected mGl:Ljavax/microedition/khronos/opengles/GL10;

.field protected mGlObjects:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

.field protected mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

.field public mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field public mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

.field protected mLockFlingAtMaxZoomLevel:Z

.field protected mLockPos:Z

.field protected mMode:I

.field protected mModeObj:Ljava/lang/Object;

.field protected mModeParm:I

.field protected mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

.field protected mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

.field protected mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

.field protected mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

.field protected mOnHoverListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;

.field protected mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

.field protected mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

.field protected mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

.field protected mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

.field protected mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;

.field protected mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

.field protected mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

.field protected mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

.field protected mPrevListInfo:Lcom/sec/samsung/gallery/glview/GlAbsListView$AbsListInfo;

.field protected mResult:I

.field protected mResultObj:Ljava/lang/Object;

.field protected mSx:F

.field protected mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

.field protected mTransitionAnimationType:I

.field protected mUpdateUpperBorder:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 23
    sput v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_IDLE:I

    .line 24
    sput v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_TOUCH_SCROLL:I

    .line 25
    sput v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    .line 27
    sput v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    .line 28
    sput v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_SELECT:I

    .line 29
    sput v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NEW_ALBUM:I

    .line 30
    const/4 v0, 0x3

    sput v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NEW_ALBUM_EX:I

    .line 42
    sput v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SAVE_OBJINFO_FORANIM:I

    .line 44
    const/high16 v0, 0x41f00000    # 30.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_FOV:F

    .line 45
    const/high16 v0, 0x44480000    # 800.0f

    sput v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    .line 49
    const-wide/16 v0, 0x64

    sput-wide v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LOCK_FLING_TIMEOUT:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlLayer;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFlags:I

    .line 71
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mSx:F

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mTransitionAnimationType:I

    .line 96
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    .line 546
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$2;-><init>(Lcom/sec/samsung/gallery/glview/GlAbsListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mListenerGenericMotion:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 566
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$3;-><init>(Lcom/sec/samsung/gallery/glview/GlAbsListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mListenerGenericMotionEx:Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;

    .line 103
    const-string v0, "GlAbsListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "GlAbsListView = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    iput-object p0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .line 105
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mContext:Landroid/content/Context;

    .line 106
    sget v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->LIST_MODE_NORMAL:I

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mMode:I

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 108
    return-void
.end method

.method protected static isAlmostEquals(FF)Z
    .locals 2
    .param p0, "a"    # F
    .param p1, "b"    # F

    .prologue
    .line 358
    sub-float v0, p0, p1

    .line 359
    .local v0, "diff":F
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    neg-float v0, v0

    .end local v0    # "diff":F
    :cond_0
    const v1, 0x3c23d70a    # 0.01f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected doResize()V
    .locals 3

    .prologue
    .line 473
    const-string v0, "GlAbsListView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Resize not supported for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 474
    return-void
.end method

.method public enableScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .locals 3
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    const/4 v2, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 490
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 496
    :cond_0
    :goto_0
    return-object v0

    .line 491
    :cond_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    .line 492
    .local v0, "scrollBar":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    if-nez v0, :cond_0

    .line 493
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlScrollBar;

    .end local v0    # "scrollBar":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    invoke-direct {v0, p0, v1, v1, v2}, Lcom/sec/samsung/gallery/glview/GlScrollBar;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FFI)V

    .line 494
    .restart local v0    # "scrollBar":Lcom/sec/samsung/gallery/glview/GlScrollBar;
    invoke-virtual {p1, v0, v2}, Lcom/sec/samsung/gallery/glview/GlScroller;->setScrollBarObject(Lcom/sec/samsung/gallery/glview/GlScrollBar;Z)V

    goto :goto_0
.end method

.method public getActionBarHeight()I
    .locals 2

    .prologue
    .line 130
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v0

    .line 131
    .local v0, "height":I
    return v0
.end method

.method public getAdapter()Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    return-object v0
.end method

.method protected getColCntThreshold()[F
    .locals 1

    .prologue
    .line 531
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getDistance()F
    .locals 1

    .prologue
    .line 421
    sget v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    return v0
.end method

.method protected getFOV()F
    .locals 1

    .prologue
    .line 417
    sget v0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_FOV:F

    return v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-nez v0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 322
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleFirst:I

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 1

    .prologue
    .line 326
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    if-nez v0, :cond_0

    .line 327
    const/4 v0, 0x0

    .line 328
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/GlScroller;->mVisibleLast:I

    goto :goto_0
.end method

.method protected getMaxColCnt(I)I
    .locals 5
    .param p1, "initColCnt"    # I

    .prologue
    .line 516
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v3

    int-to-float v0, v3

    .line 517
    .local v0, "fullScreenWidth":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getColCntThreshold()[F

    move-result-object v2

    .line 520
    .local v2, "threshold":[F
    if-nez v2, :cond_1

    .line 527
    .end local p1    # "initColCnt":I
    :cond_0
    :goto_0
    return p1

    .line 522
    .restart local p1    # "initColCnt":I
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    array-length v3, v2

    if-ge v1, v3, :cond_0

    .line 523
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mWidth:I

    int-to-float v3, v3

    aget v4, v2, v1

    mul-float/2addr v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 524
    add-int/lit8 p1, v1, 0x1

    goto :goto_0

    .line 522
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getMaxVisibleCount()I
    .locals 1

    .prologue
    .line 332
    const/4 v0, 0x0

    return v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 354
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mMode:I

    return v0
.end method

.method public final getOnItemClickListener()Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    return-object v0
.end method

.method public getScroll()F
    .locals 1

    .prologue
    .line 484
    const/4 v0, 0x0

    return v0
.end method

.method public getScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;)Lcom/sec/samsung/gallery/glview/GlScrollBar;
    .locals 1
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;

    .prologue
    .line 512
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    goto :goto_0
.end method

.method public getScrollPosition()I
    .locals 1

    .prologue
    .line 340
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFocused:I

    return v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 336
    const/4 v0, -0x1

    return v0
.end method

.method public getSourceView()Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    return-object v0
.end method

.method public isDragEnabled()Z
    .locals 1

    .prologue
    .line 543
    const/4 v0, 0x0

    return v0
.end method

.method protected isScreenLocked()Z
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mLockPos:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isCreated()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected loadItems()V
    .locals 0

    .prologue
    .line 470
    return-void
.end method

.method protected onCommand(ILjava/lang/Object;III)V
    .locals 1
    .param p1, "cmd"    # I
    .param p2, "obj"    # Ljava/lang/Object;
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I
    .param p5, "parm3"    # I

    .prologue
    .line 442
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 443
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onDataSetChanged()V

    .line 453
    :cond_0
    :goto_0
    return-void

    .line 444
    :cond_1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 445
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->onScrollDone()V

    goto :goto_0

    .line 446
    :cond_2
    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    .line 447
    invoke-virtual {p0, p3}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->updateItem(I)V

    goto :goto_0

    .line 448
    :cond_3
    const/4 v0, 0x6

    if-ne p1, v0, :cond_4

    .line 449
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->doResize()V

    goto :goto_0

    .line 450
    :cond_4
    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    .line 451
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mLockFlingAtMaxZoomLevel:Z

    goto :goto_0
.end method

.method protected onDataSetChanged()V
    .locals 0

    .prologue
    .line 457
    return-void
.end method

.method public onGenericMotionCancel()V
    .locals 2

    .prologue
    .line 165
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 166
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 165
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aget-object v1, v1, v0

    invoke-interface {v1, p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    goto :goto_1

    .line 170
    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0
    .param p1, "changeSize"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 426
    return-void
.end method

.method protected onResume()V
    .locals 0

    .prologue
    .line 111
    return-void
.end method

.method protected onScrollDone()V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    sget v1, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_IDLE:I

    invoke-interface {v0, p0, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 462
    :cond_0
    return-void
.end method

.method public onSurfaceChanged(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->setMultiWindow(Landroid/content/Context;)V

    .line 436
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->isCreated()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->resetLayout()V

    .line 439
    :cond_0
    return-void
.end method

.method public reconstructView(I)V
    .locals 0
    .param p1, "animation"    # I

    .prologue
    .line 116
    return-void
.end method

.method public refreshView(Z)V
    .locals 0
    .param p1, "reload"    # Z

    .prologue
    .line 121
    return-void
.end method

.method public reloadObjects()V
    .locals 0

    .prologue
    .line 587
    return-void
.end method

.method protected resetLayout()V
    .locals 8

    .prologue
    const/16 v7, 0x303

    const/4 v6, 0x0

    .line 375
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFOV()F

    move-result v2

    .line 376
    .local v2, "fov":F
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getDistance()F

    move-result v1

    .line 378
    .local v1, "distance":F
    const/16 v4, 0x10

    new-array v3, v4, [F

    .line 379
    .local v3, "mProjM":[F
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mHeight:I

    int-to-float v5, v5

    div-float v0, v4, v5

    .line 381
    .local v0, "aspect":F
    invoke-virtual {p0, v2, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->setDefaultDistance(FF)V

    .line 383
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v4, :cond_0

    .line 384
    const-string v4, "GlAbsListView"

    const-string v5, "resetLayout : gl root is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 414
    :goto_0
    return-void

    .line 389
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGLContext()Ljavax/microedition/khronos/opengles/GL11;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    .line 390
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    if-nez v4, :cond_1

    .line 391
    const-string v4, "GlAbsListView"

    const-string v5, "resetLayout : gl root context is null"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 395
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0xb60

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glDisable(I)V

    .line 396
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0xbe2

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 397
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0xde1

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glEnable(I)V

    .line 398
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUsePremultipleAlphaBlend:Z

    if-eqz v4, :cond_2

    .line 399
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/4 v5, 0x1

    invoke-interface {v4, v5, v7}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    .line 404
    :goto_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0x1701

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    .line 405
    invoke-static {v3, v6}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 406
    const/high16 v4, 0x3f800000    # 1.0f

    const/high16 v5, 0x44fa0000    # 2000.0f

    invoke-static {v3, v2, v0, v4, v5}, Lcom/sec/android/gallery3d/glcore/TUtils;->glhPerspectivef2([FFFFF)V

    .line 408
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getGlConfig()Lcom/sec/android/gallery3d/glcore/GlConfig;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/sec/android/gallery3d/glcore/GlConfig;->setProspectMatrix([F)V

    .line 410
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v4, v3, v6}, Ljavax/microedition/khronos/opengles/GL10;->glLoadMatrixf([FI)V

    .line 412
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0x1700

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/opengles/GL10;->glMatrixMode(I)V

    goto :goto_0

    .line 401
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    const/16 v5, 0x302

    invoke-interface {v4, v5, v7}, Ljavax/microedition/khronos/opengles/GL10;->glBlendFunc(II)V

    goto :goto_1
.end method

.method public resetScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;F)V
    .locals 3
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p2, "topPadding"    # F

    .prologue
    .line 500
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 501
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mWidthSpace:F

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mHeightSpace:F

    invoke-virtual {v0, v1, v2, p2}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->reset(FFF)V

    .line 503
    :cond_0
    return-void
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->unregisterObserver(Ljava/lang/Object;)V

    .line 138
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v0, :cond_1

    .line 140
    new-instance v0, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/glview/GlAbsListView$1;-><init>(Lcom/sec/samsung/gallery/glview/GlAbsListView;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDataSetObserver:Lcom/sec/samsung/gallery/glview/GContentObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->registerObserver(Lcom/sec/samsung/gallery/glview/GContentObserver;)V

    .line 155
    :cond_1
    return-void
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;I)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .param p2, "option"    # I

    .prologue
    .line 158
    return-void
.end method

.method public setAirMotionImageFling(I)V
    .locals 0
    .param p1, "speed"    # I

    .prologue
    .line 535
    return-void
.end method

.method public setAttrs(II)V
    .locals 0
    .param p1, "attr"    # I
    .param p2, "parm"    # I

    .prologue
    .line 290
    return-void
.end method

.method public setDefaultDistance(FF)V
    .locals 6
    .param p1, "fov"    # F
    .param p2, "distance"    # F

    .prologue
    .line 365
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFov:F

    .line 366
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDistance:F

    .line 367
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    if-nez v1, :cond_0

    .line 372
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v1, v1, Lcom/sec/android/gallery3d/glcore/GlRootView;->mWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget v2, v2, Lcom/sec/android/gallery3d/glcore/GlRootView;->mHeight:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 370
    .local v0, "aspect":F
    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mDistance:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFov:F

    float-to-double v2, v2

    const-wide v4, 0x400921fafc8b007aL    # 3.141592

    mul-double/2addr v2, v4

    const-wide v4, 0x4076800000000000L    # 360.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->tan(D)D

    move-result-wide v2

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mHeightSpace:F

    .line 371
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mHeightSpace:F

    mul-float/2addr v1, v0

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mWidthSpace:F

    goto :goto_0
.end method

.method public setDragEnable(Z)V
    .locals 0
    .param p1, "b"    # Z

    .prologue
    .line 539
    return-void
.end method

.method public setFlags(IZ)V
    .locals 2
    .param p1, "flag"    # I
    .param p2, "set"    # Z

    .prologue
    .line 283
    if-eqz p2, :cond_0

    .line 284
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFlags:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFlags:I

    .line 287
    :goto_0
    return-void

    .line 286
    :cond_0
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFlags:I

    xor-int/lit8 v1, p1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFlags:I

    goto :goto_0
.end method

.method public setFocusIndex(IZ)V
    .locals 0
    .param p1, "index"    # I
    .param p2, "isAlbum"    # Z

    .prologue
    .line 481
    return-void
.end method

.method public setLayerTransitionAnimation(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 298
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mTransitionAnimationType:I

    .line 299
    return-void
.end method

.method public setMode(IILjava/lang/Object;)V
    .locals 0
    .param p1, "mode"    # I
    .param p2, "parm"    # I
    .param p3, "obj"    # Ljava/lang/Object;

    .prologue
    .line 348
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mMode:I

    .line 349
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mModeParm:I

    .line 350
    iput-object p3, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mModeObj:Ljava/lang/Object;

    .line 351
    return-void
.end method

.method public setOnDropItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnDropItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnDropItemClickListener;

    .line 194
    return-void
.end method

.method public setOnFocusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    .prologue
    .line 209
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    .line 210
    return-void
.end method

.method public setOnGenericMotionListener(ILcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    .prologue
    const/4 v3, 0x4

    .line 218
    if-lt p1, v3, :cond_0

    .line 219
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setOnGenericMotionListener : index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", max = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/GlAbsListView$OnGenericMotionListener;

    aput-object p2, v0, p1

    .line 222
    return-void
.end method

.method public setOnHoverListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    .prologue
    .line 185
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnHoverListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListener;

    .line 186
    return-void
.end method

.method public setOnHoverListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;

    .prologue
    .line 189
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnHoverListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnHoverListenerEx;

    .line 190
    return-void
.end method

.method public setOnItemClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListener;

    .line 178
    return-void
.end method

.method public setOnItemClickListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    .prologue
    .line 205
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnItemClickListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemClickListenerEx;

    .line 206
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    .line 182
    return-void
.end method

.method public setOnKeyListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnKeyListenner:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnKeyListener;

    .line 226
    return-void
.end method

.method public setOnPenSelectionListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;

    .prologue
    .line 228
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnPenSelectionListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnPenSelectionListener;

    .line 229
    return-void
.end method

.method public setOnScrollListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    .prologue
    .line 173
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    .line 174
    return-void
.end method

.method public setOnScrollListenerEx(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    .prologue
    .line 201
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnScrollListenerEx:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    .line 202
    return-void
.end method

.method public setOnStatusChangedListener(Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    .prologue
    .line 213
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mOnStatusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnStatusChangedListener;

    .line 214
    return-void
.end method

.method public setResult(ILjava/lang/Object;)V
    .locals 0
    .param p1, "parm"    # I
    .param p2, "obj"    # Ljava/lang/Object;

    .prologue
    .line 293
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mResult:I

    .line 294
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mResultObj:Ljava/lang/Object;

    .line 295
    return-void
.end method

.method public setScrollBarMaxHeight(Lcom/sec/samsung/gallery/glview/GlScroller;F)V
    .locals 1
    .param p1, "scroller"    # Lcom/sec/samsung/gallery/glview/GlScroller;
    .param p2, "max"    # F

    .prologue
    .line 506
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 507
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->getScrollBar()Lcom/sec/samsung/gallery/glview/GlScrollBar;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/sec/samsung/gallery/glview/GlScrollBar;->setMaxHeight(F)V

    .line 509
    :cond_0
    return-void
.end method

.method public setScrollPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 344
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mFocused:I

    .line 345
    return-void
.end method

.method protected updateItem(I)V
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 465
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlAbsListView;->mItemCtrl:Lcom/sec/samsung/gallery/glview/GlScroller;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlScroller;->update(I)V

    .line 466
    return-void
.end method
