.class public Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;
.super Lcom/sec/samsung/gallery/view/help/HelpViewState;
.source "HelpViewPictureState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;


# static fields
.field protected static final TAG:Ljava/lang/String; = "HelpViewPictureState"


# instance fields
.field contentDescription:Ljava/lang/String;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;-><init>()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->contentDescription:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->setViewViewingPictures()V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->onClickViewingPictures()V

    return-void
.end method

.method private onClickViewingPictures()V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    .line 333
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    if-nez v3, :cond_1

    .line 334
    iput v4, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    .line 338
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->setHelpView()V

    .line 339
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    if-ne v3, v6, :cond_0

    .line 340
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 341
    const/4 v3, 0x0

    invoke-super {p0, v3}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startTapAnimation(I)V

    .line 343
    :cond_0
    iget v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    if-ne v3, v4, :cond_1

    .line 346
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/help_2.jpg"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v1

    .line 348
    .local v1, "file":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/sec/android/gallery3d/data/DataManager;->findPathByUri(Landroid/net/Uri;Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    .line 349
    .local v2, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    if-nez v2, :cond_2

    .line 350
    const-string v3, "HelpViewPictureState"

    const-string v4, " itemPath is null"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    .end local v1    # "file":Ljava/lang/String;
    .end local v2    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    :goto_0
    return-void

    .line 353
    .restart local v1    # "file":Ljava/lang/String;
    .restart local v2    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_2
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 354
    .local v0, "data":Landroid/os/Bundle;
    const-string v3, "Help"

    iget v4, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMode:I

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 355
    const-string v3, "KEY_MEDIA_SET_PATH"

    const-string v4, "/uri/help/mediaset"

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    const-string v3, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 357
    const-string v3, "KEY_ITEM_POSITION"

    invoke-virtual {v0, v3, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 359
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->setDetailViewViewingPictures()V

    .line 361
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    const-class v4, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-virtual {v3, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private refreshDetailViewingPictures(I)V
    .locals 4
    .param p1, "orientation"    # I

    .prologue
    .line 409
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 410
    .local v0, "r":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v3, 0x7f0f01c4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 411
    .local v1, "text":Landroid/widget/TextView;
    if-nez v1, :cond_0

    .line 419
    :goto_0
    return-void

    .line 413
    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 414
    const v2, 0x7f0d0197

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 418
    :goto_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 416
    :cond_1
    const v2, 0x7f0d0198

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_1
.end method

.method private setDetailViewViewingPictures()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    const/4 v10, 0x4

    .line 367
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v9, "layout_inflater"

    invoke-virtual {v8, v9}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 370
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f0300a2

    const/4 v9, 0x0

    invoke-virtual {v3, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    .line 371
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v9, 0x7f0f01c2

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 372
    .local v5, "tap":Landroid/widget/ImageView;
    if-eqz v5, :cond_0

    .line 373
    invoke-virtual {v5, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 374
    :cond_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v9, 0x7f0f01c4

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 375
    .local v7, "text_pan_text":Landroid/widget/TextView;
    if-eqz v7, :cond_1

    .line 376
    invoke-virtual {v7, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 378
    :cond_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v9, 0x7f0f01c5

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 379
    .local v2, "head":Landroid/widget/ImageView;
    if-eqz v2, :cond_2

    .line 380
    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 382
    :cond_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v9, 0x7f0f01c6

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 383
    .local v6, "text":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    const v9, 0x7f0f01c3

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 384
    .local v1, "cue_img":Landroid/widget/ImageView;
    if-eqz v6, :cond_3

    if-eqz v1, :cond_3

    .line 385
    invoke-virtual {v1, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 386
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 387
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v8, :cond_4

    .line 388
    const-string v8, "gallery_help_cue_swipe"

    invoke-super {p0, v8}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 392
    :goto_0
    const v8, 0x7f0e02c3

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 393
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 394
    .local v4, "r":Landroid/content/res/Resources;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    const/4 v9, 0x1

    if-ne v8, v9, :cond_5

    .line 395
    const v8, 0x7f0d0197

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 402
    .end local v4    # "r":Landroid/content/res/Resources;
    :cond_3
    :goto_1
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    new-instance v10, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v10, v11, v11}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9, v10}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 404
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const/high16 v9, 0x7f040000

    invoke-static {v8, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 405
    .local v0, "animation":Landroid/view/animation/Animation;
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMotionView:Landroid/view/View;

    invoke-virtual {v8, v0}, Landroid/view/View;->setAnimation(Landroid/view/animation/Animation;)V

    .line 406
    return-void

    .line 390
    .end local v0    # "animation":Landroid/view/animation/Animation;
    :cond_4
    const v8, 0x7f0202d0

    invoke-virtual {v1, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 397
    .restart local v4    # "r":Landroid/content/res/Resources;
    :cond_5
    const v8, 0x7f0d0198

    invoke-virtual {v4, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_1
.end method

.method private setViewViewingPictures()V
    .locals 19

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    .line 92
    .local v14, "res":Landroid/content/res/Resources;
    const v16, 0x7f0d00c6

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v12

    .line 93
    .local v12, "popup_x":I
    const v16, 0x7f0d0167

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 94
    .local v7, "margin":I
    const v16, 0x7f0d0168

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 95
    .local v8, "marginBottom":I
    const v16, 0x7f0d0169

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    .line 96
    .local v9, "marginLeft":I
    const v16, 0x7f0d016a

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 97
    .local v10, "marginLeftAdditional":I
    const v16, 0x7f0d016b

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 98
    .local v11, "marginTop":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    .line 100
    .local v5, "locale":Ljava/lang/String;
    const/4 v3, 0x0

    .line 102
    .local v3, "contentDescription":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    if-nez v16, :cond_0

    .line 103
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0f0133

    invoke-virtual/range {v16 .. v17}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v16

    check-cast v16, Landroid/widget/ImageView;

    check-cast v16, Landroid/widget/ImageView;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    .line 105
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getActionBarSize(Landroid/content/Context;)I

    move-result v2

    .line 106
    .local v2, "actionBarHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v16

    move-object/from16 v0, v16

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    move/from16 v16, v0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_10

    .line 107
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    if-nez v16, :cond_b

    .line 108
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1

    .line 109
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(I)V

    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/widget/TextView;->measure(II)V

    .line 112
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 113
    const v16, 0x7f0d01ad

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_5

    .line 114
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    sub-int v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v17

    div-int/lit8 v17, v17, 0x2

    sub-int v15, v16, v17

    .line 116
    .local v15, "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    neg-int v0, v15

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 117
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 119
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    sub-int v17, v17, v18

    add-int v17, v17, v15

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 140
    .end local v15    # "x":I
    :goto_0
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v16, :cond_7

    .line 141
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_tap"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_head"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_album_land"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 152
    :goto_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v16

    if-nez v16, :cond_a

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 154
    const v16, 0x7f0d0170

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_9

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    add-int v17, v2, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    mul-int/lit8 v17, v10, 0x2

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x5

    mul-int/lit8 v17, v17, 0x2

    sub-int v17, v17, v7

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 158
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0xa

    mul-int/lit8 v17, v17, 0x6

    sub-int v17, v17, v8

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 169
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_2

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 171
    .local v13, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0199

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setMaxWidth(I)V

    .line 324
    .end local v13    # "r":Landroid/content/res/Resources;
    :cond_2
    :goto_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    if-eqz v16, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_4

    .line 325
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 326
    .local v4, "description":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 327
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 329
    .end local v4    # "description":Ljava/lang/String;
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 330
    return-void

    .line 122
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_6

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01af

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 125
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b0

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 127
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01ae

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 128
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01ad

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 129
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b1

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_0

    .line 131
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b9

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01ba

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 135
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01bc

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01bb

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 137
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b2

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_0

    .line 145
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202ef

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202e6

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_8

    .line 148
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202ce

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 150
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202cd

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_1

    .line 160
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0171

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 161
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0170

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 162
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0172

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 163
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0172

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_2

    .line 166
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 174
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_f

    .line 175
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_c

    .line 176
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/widget/TextView;->measure(II)V

    .line 179
    :cond_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 180
    const v16, 0x7f0d01b3

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_d

    .line 181
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    sub-int v16, v16, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v17

    div-int/lit8 v17, v17, 0x2

    sub-int v15, v16, v17

    .line 182
    .restart local v15    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    neg-int v0, v15

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 183
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40000000    # 2.0f

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 184
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 185
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40400000    # 3.0f

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 186
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x3

    add-int v17, v17, v18

    add-int v17, v17, v15

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 196
    .end local v15    # "x":I
    :goto_4
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v16, :cond_e

    .line 197
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_photo_land"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 200
    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    add-int v17, v2, v7

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 201
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x5

    sub-int v17, v17, v9

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 202
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x5

    mul-int/lit8 v17, v17, 0x2

    div-int/lit8 v18, v9, 0x2

    add-int v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 203
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    mul-int/lit8 v18, v7, 0x14

    sub-int v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_3

    .line 188
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b5

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    neg-int v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b6

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 192
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b4

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 193
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b3

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 194
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_4

    .line 199
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202e3

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_5

    .line 204
    :cond_f
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 205
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpLayoutView:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x80

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpLayoutView:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3

    .line 209
    :cond_10
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    if-nez v16, :cond_1e

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_11

    .line 211
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(I)V

    .line 212
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    int-to-float v0, v12

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 215
    const v16, 0x7f0d01a5

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_17

    .line 216
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x2

    mul-int/lit8 v18, v18, 0x3

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 217
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    mul-int/lit8 v18, v18, 0x7

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 230
    :goto_6
    const v16, 0x7f0d01a6

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_19

    .line 231
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40800000    # 4.0f

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 232
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 243
    :goto_7
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v16, :cond_1b

    .line 244
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_tap"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_head"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_album"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 256
    :goto_8
    const v16, 0x7f0d01aa

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-eqz v16, :cond_13

    const-string v16, "ar"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string v16, "fa"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string/jumbo v16, "sv"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string v16, "et"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string/jumbo v16, "vi"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string/jumbo v16, "th"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_12

    const-string v16, "lo"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_13

    .line 260
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    add-int/lit8 v17, v12, 0x46

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 261
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    add-int v17, v17, v18

    sub-int v17, v17, v12

    add-int/lit8 v17, v17, 0x50

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01aa

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 264
    :cond_13
    const v16, 0x7f0d01ab

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-eqz v16, :cond_14

    const-string v16, "nl"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "pt"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "gl"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "es"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "fr"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "it"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "hu"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    const-string v16, "ru"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-nez v16, :cond_15

    :cond_14
    const-string v16, "ml"

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_16

    .line 268
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01ac

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    sub-int v17, v12, v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 269
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01ab

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 271
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    const v16, 0x7f0d016d

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-nez v16, :cond_1d

    .line 273
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    add-int v17, v2, v11

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 274
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    int-to-float v0, v10

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 275
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    mul-int/lit8 v17, v17, 0x2

    sub-int v17, v17, v7

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 276
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x6

    mul-int/lit8 v17, v17, 0x2

    div-int/lit8 v18, v8, 0x4

    add-int v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    .line 283
    :goto_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_2

    .line 284
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    .line 285
    .restart local v13    # "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0199

    move/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto/16 :goto_3

    .line 219
    .end local v13    # "r":Landroid/content/res/Resources;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_18

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01a5

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 221
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d00c7

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 222
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01a8

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    goto/16 :goto_6

    .line 224
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0173

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 225
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d0174

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 226
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01a9

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    goto/16 :goto_6

    .line 234
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_1a

    .line 235
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01a7

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 236
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01a6

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    goto/16 :goto_7

    .line 238
    :cond_1a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d01c4

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 239
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01c5

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    goto/16 :goto_7

    .line 248
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202ef

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 249
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202e6

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 251
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/sec/samsung/gallery/util/SettingsUtil;->getEasyMode(Landroid/content/Context;)Z

    move-result v16

    if-nez v16, :cond_1c

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202cb

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8

    .line 254
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202cc

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_8

    .line 278
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d016e

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 279
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d016d

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 280
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d016f

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d016f

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_9

    .line 288
    :cond_1e
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    const/16 v17, 0x1

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_23

    .line 289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_1f

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/TextView;->setText(I)V

    .line 290
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-object/from16 v16, v0

    const v17, 0x7f0e02c2

    invoke-virtual/range {v16 .. v17}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 291
    const v16, 0x7f0d0163

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-eqz v16, :cond_20

    .line 292
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    const v17, 0x7f0d0163

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    .line 297
    :goto_a
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    const/high16 v18, 0x40800000    # 4.0f

    div-float v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setY(F)V

    .line 298
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v18

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 299
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    div-int/lit8 v18, v18, 0x3

    mul-int/lit8 v18, v18, 0x2

    add-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 300
    const v16, 0x7f0d01b7

    move/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    if-eqz v16, :cond_21

    .line 301
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0d01b7

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 305
    :goto_b
    sget-boolean v16, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDownloadableHelp:Z

    if-eqz v16, :cond_22

    .line 306
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const-string v17, "gallery_help_photo"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-super {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->getHelpDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v17

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 309
    :goto_c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    add-int v17, v2, v7

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setY(F)V

    .line 310
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    mul-int/lit8 v18, v9, 0x2

    sub-int v17, v17, v18

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    .line 311
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x3

    mul-int/lit8 v17, v17, 0x2

    mul-int/lit8 v18, v9, 0x2

    add-int v17, v17, v18

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setMinimumWidth(I)V

    .line 312
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHeight:I

    move/from16 v16, v0

    div-int/lit8 v16, v16, 0x8

    mul-int/lit8 v16, v16, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mResources:Landroid/content/res/Resources;

    move-object/from16 v17, v0

    const v18, 0x7f0d01c0

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v17

    add-int v6, v16, v17

    .line 317
    .local v6, "mItemHeight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setMinimumHeight(I)V

    goto/16 :goto_3

    .line 295
    .end local v6    # "mItemHeight":I
    :cond_20
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpPopup:Landroid/widget/RelativeLayout;

    move-object/from16 v16, v0

    int-to-float v0, v12

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/RelativeLayout;->setX(F)V

    goto/16 :goto_a

    .line 303
    :cond_21
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHead:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mWidth:I

    move/from16 v17, v0

    div-int/lit8 v17, v17, 0x2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v18

    add-int v17, v17, v18

    sub-int v17, v17, v12

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setX(F)V

    goto/16 :goto_b

    .line 308
    :cond_22
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    move-object/from16 v16, v0

    const v17, 0x7f0202e2

    invoke-virtual/range {v16 .. v17}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_c

    .line 318
    :cond_23
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    move/from16 v16, v0

    const/16 v17, 0x2

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_2

    .line 319
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpLayoutView:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x80

    const/16 v18, 0x0

    invoke-virtual/range {v16 .. v18}, Landroid/view/View;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 320
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpLayoutView:Landroid/view/View;

    move-object/from16 v16, v0

    const/16 v17, 0x4

    invoke-virtual/range {v16 .. v17}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_3
.end method


# virtual methods
.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 423
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 424
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 425
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->refreshDetailViewingPictures(I)V

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_1

    .line 428
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 430
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x0

    .line 44
    invoke-super {p0, p1, p2}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 45
    const-string v0, "Help"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mHelpMode:I

    .line 46
    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-super {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->animationInit(Landroid/content/Context;)V

    .line 49
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->startBubbleAnimation()V

    .line 50
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->setOnClickListener()V

    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 53
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 434
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 435
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e02da

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 439
    :goto_0
    return-void

    .line 437
    :cond_0
    invoke-super {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 84
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onPause()V

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->pause()V

    .line 87
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const v1, 0x7f0e0073

    .line 71
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->onResume()V

    .line 72
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mViewMode:I

    if-nez v0, :cond_1

    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->resume(I)V

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setTitle(I)V

    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setDrawerEnable(Z)V

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpTimeViewActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 80
    :cond_1
    return-void
.end method

.method protected setHelpView()V
    .locals 6

    .prologue
    .line 57
    invoke-super {p0}, Lcom/sec/samsung/gallery/view/help/HelpViewState;->setHelpView()V

    .line 58
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 59
    .local v0, "locale":Ljava/lang/String;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 60
    .local v1, "res":Landroid/content/res/Resources;
    const-string v4, "ar"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "fa"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "iw"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    const-wide/16 v2, 0x1e

    .line 61
    .local v2, "rtlDelay":J
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    new-instance v5, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$1;

    invoke-direct {v5, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V

    invoke-virtual {v4, v5, v2, v3}, Landroid/widget/ImageView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 67
    return-void

    .line 60
    .end local v2    # "rtlDelay":J
    :cond_1
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method protected setOnClickListener()V
    .locals 2

    .prologue
    .line 442
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 443
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSoundEffectsEnabled(Z)V

    .line 444
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mBG:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 454
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mText:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setImportantForAccessibility(I)V

    .line 457
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 458
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mItem:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$3;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 467
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;->mTap:Landroid/widget/ImageView;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$4;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/help/HelpViewPictureState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpViewPictureState;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    :cond_3
    return-void
.end method
