.class Lcom/sec/samsung/gallery/glview/GlGridView$12;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1148
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHoverEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 3
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v2, 0x0

    .line 1150
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mPenSelectionObj:Lcom/sec/android/gallery3d/glcore/GlObject;
    invoke-static {v1, p1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1302(Lcom/sec/samsung/gallery/glview/GlGridView;Lcom/sec/android/gallery3d/glcore/GlObject;)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object v1, p1

    .line 1151
    check-cast v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v0, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1152
    .local v0, "index":I
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-nez v1, :cond_1

    :cond_0
    move v1, v2

    .line 1161
    :goto_0
    return v1

    .line 1154
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->isDrawerVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    .line 1155
    goto :goto_0

    .line 1156
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1500(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1500(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->isIdle()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    .line 1157
    goto :goto_0

    .line 1158
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mIsDragEnabled:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$900(Lcom/sec/samsung/gallery/glview/GlGridView;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    .line 1159
    goto :goto_0

    .line 1160
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlGridView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v2

    invoke-virtual {v1, p1, v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->requestStart(Lcom/sec/android/gallery3d/glcore/GlObject;Lcom/sec/android/gallery3d/data/MediaObject;I)V

    .line 1161
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public onHoverExit(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v0, 0x1

    .line 1171
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1172
    const/4 v0, 0x0

    .line 1175
    :goto_0
    return v0

    .line 1174
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    goto :goto_0
.end method

.method public onHoverMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    const/4 v0, 0x0

    .line 1165
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1166
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$12;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mHoverCtrl:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$1400(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->isIdleState()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 1167
    :cond_0
    return v0
.end method
