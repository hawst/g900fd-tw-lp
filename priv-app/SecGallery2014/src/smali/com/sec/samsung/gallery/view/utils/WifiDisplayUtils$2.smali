.class Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;
.super Landroid/content/BroadcastReceiver;
.source "WifiDisplayUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;->this$0:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 258
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 259
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receiver got : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 262
    const-string v2, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/WifiDisplayStatus;

    .line 264
    .local v1, "status":Landroid/hardware/display/WifiDisplayStatus;
    # getter for: Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "wifi display status changed! Scanstate : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", ActiveDisplayState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;->this$0:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    # setter for: Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;
    invoke-static {v2, v1}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->access$202(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;Landroid/hardware/display/WifiDisplayStatus;)Landroid/hardware/display/WifiDisplayStatus;

    .line 267
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils$2;->this$0:Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;

    # invokes: Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->applyState()V
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;->access$300(Lcom/sec/samsung/gallery/view/utils/WifiDisplayUtils;)V

    .line 269
    .end local v1    # "status":Landroid/hardware/display/WifiDisplayStatus;
    :cond_0
    return-void
.end method
