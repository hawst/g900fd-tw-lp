.class Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;
.super Landroid/os/AsyncTask;
.source "MoreInfoItemUserTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "FaceNameLoadTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/util/ArrayList",
        "<",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)V
    .locals 0

    .prologue
    .line 499
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private removeLookupKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 524
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 525
    const-string v1, "profile/Me"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 526
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->access$1000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0e00c0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 531
    :goto_0
    return-object v1

    .line 528
    :cond_0
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 529
    .local v0, "split":[Ljava/lang/String;
    const/4 v1, 0x1

    aget-object v1, v0, v1

    goto :goto_0

    .line 531
    .end local v0    # "split":[Ljava/lang/String;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->doInBackground([Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 11
    .param p1, "params"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Object;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 503
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 504
    .local v1, "buffer":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    const/4 v10, 0x0

    aget-object v2, p1, v10

    check-cast v2, Landroid/content/Context;

    .line 505
    .local v2, "context":Landroid/content/Context;
    const/4 v10, 0x1

    aget-object v6, p1, v10

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 506
    .local v6, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getFaces()[Lcom/sec/android/gallery3d/data/Face;

    move-result-object v4

    .line 507
    .local v4, "faces":[Lcom/sec/android/gallery3d/data/Face;
    if-eqz v4, :cond_0

    array-length v10, v4

    if-gtz v10, :cond_1

    .line 520
    :cond_0
    return-object v1

    .line 511
    :cond_1
    move-object v0, v4

    .local v0, "arr$":[Lcom/sec/android/gallery3d/data/Face;
    array-length v7, v0

    .local v7, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v7, :cond_0

    aget-object v3, v0, v5

    .line 512
    .local v3, "face":Lcom/sec/android/gallery3d/data/Face;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Face;->getPersonId()Ljava/lang/String;

    move-result-object v9

    .line 513
    .local v9, "personId":Ljava/lang/String;
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-static {v2, v10}, Lcom/sec/samsung/gallery/access/face/PersonList;->getName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v8

    .line 514
    .local v8, "name":Ljava/lang/String;
    invoke-direct {p0, v8}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->removeLookupKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 515
    if-eqz v8, :cond_2

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    if-lez v10, :cond_2

    .line 516
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 517
    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 511
    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 499
    check-cast p1, Ljava/util/ArrayList;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->onPostExecute(Ljava/util/ArrayList;)V

    return-void
.end method

.method protected onPostExecute(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 536
    .local p1, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->access$1100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Ljava/util/ArrayList;

    move-result-object v1

    monitor-enter v1

    .line 538
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->access$1100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 539
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->mFaceNames:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->access$1100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 540
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 541
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter$FaceNameLoadTask;->this$0:Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/MoreInfoItemUserTag$FaceAdapter;->onDataSetChanged()V

    .line 543
    :cond_0
    return-void

    .line 540
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
