.class public interface abstract Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;
.super Ljava/lang/Object;
.source "GlComposeBaseView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnPenSelectionListener"
.end annotation


# virtual methods
.method public abstract isCheckAvailable()V
.end method

.method public abstract onPenSelection(IIIZ)Z
.end method

.method public abstract prePenSelectionCheck(III)Z
.end method
