.class Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;
.super Lcom/sec/android/gallery3d/ui/SynchronizedHandler;
.source "HelpDetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/android/gallery3d/ui/GLRoot;)V
    .locals 0
    .param p2, "x0"    # Lcom/sec/android/gallery3d/ui/GLRoot;

    .prologue
    .line 466
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-direct {p0, p2}, Lcom/sec/android/gallery3d/ui/SynchronizedHandler;-><init>(Lcom/sec/android/gallery3d/ui/GLRoot;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "message"    # Landroid/os/Message;

    .prologue
    const-wide/16 v2, 0xa

    const/16 v1, 0x157c

    .line 469
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 513
    :cond_0
    :goto_0
    return-void

    .line 473
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 516
    :pswitch_0
    new-instance v0, Ljava/lang/AssertionError;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(I)V

    throw v0

    .line 475
    :pswitch_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_4

    .line 476
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$2900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 477
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 478
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)V

    goto :goto_0

    .line 481
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->hideBars()V

    goto :goto_0

    .line 485
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # invokes: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$3000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)V

    goto :goto_0

    .line 489
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->hideBars()V

    goto :goto_0

    .line 494
    :pswitch_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setUserInteractionShowMode()V

    goto :goto_0

    .line 498
    :pswitch_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 499
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v1, 0x3f866666    # 1.05f

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 500
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 505
    :pswitch_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->getPositionController()Lcom/sec/android/gallery3d/ui/PositionController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PositionController;->isInScale()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    const v1, 0x3f733333    # 0.95f

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/PhotoView;->startScale(F)V

    .line 507
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 512
    :pswitch_5
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;->this$0:Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->endScale()V

    goto/16 :goto_0

    .line 473
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method
