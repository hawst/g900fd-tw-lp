.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;
.super Ljava/lang/Object;
.source "GallerySearchActionBar.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->initializeSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 3
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 234
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$900(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # invokes: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setSuggestionAdapter(Ljava/lang/String;)V
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_ENTERKEYWORD_SEARCH:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mOldFilterText:Ljava/lang/String;
    invoke-static {v0, p1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$902(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;Ljava/lang/String;)Ljava/lang/String;

    .line 239
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 244
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->access$1100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->setLastQueryText(Ljava/lang/String;)V

    .line 246
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$2;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_SEARCH:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/core/Event;->setData(Ljava/lang/Object;)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 248
    const/4 v0, 0x0

    .line 250
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
