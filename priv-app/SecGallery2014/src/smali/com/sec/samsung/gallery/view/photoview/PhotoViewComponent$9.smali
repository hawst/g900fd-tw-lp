.class Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;
.super Ljava/lang/Object;
.source "PhotoViewComponent.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->startDetailViewState(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

.field final synthetic val$bundle:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 1485
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->val$bundle:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 1488
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mPhotoView:Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$1800(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;->setClickEnabled(Z)V

    .line 1489
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->lockRenderThread()V

    .line 1490
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$600(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    const-class v1, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->val$bundle:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 1491
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent$9;->this$0:Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;

    # getter for: Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;->access$2300(Lcom/sec/samsung/gallery/view/photoview/PhotoViewComponent;)Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->unlockRenderThread()V

    .line 1492
    return-void
.end method
