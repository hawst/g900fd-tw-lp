.class public Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "KnoxModeReceiverCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 29
    new-instance v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;)Lcom/sec/android/gallery3d/data/DataManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    return-object v0
.end method

.method public static registerKnoxModeReceiver(Landroid/content/Context;Lcom/sec/samsung/gallery/core/GalleryFacade;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "galleryFacade"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 99
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseKnoxMode:Z

    if-nez v1, :cond_0

    .line 106
    :goto_0
    return-void

    .line 102
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    aput-object v2, v0, v1

    .line 105
    .local v0, "params":[Ljava/lang/Object;
    const-string v1, "KNOX_MODE_RECEIVER"

    invoke-virtual {p1, v1, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private registerReceiver(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 76
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 77
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 78
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 79
    const-string v1, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 80
    const-string v1, "com.sec.knox.container.FileRelayExist"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 81
    const-string v1, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 83
    return-void
.end method

.method public static unregisterKnoxModeReceiver(Landroid/content/Context;Lcom/sec/samsung/gallery/core/GalleryFacade;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "galleryFacade"    # Lcom/sec/samsung/gallery/core/GalleryFacade;

    .prologue
    .line 109
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseKnoxMode:Z

    if-nez v1, :cond_0

    .line 116
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    aput-object v2, v0, v1

    .line 115
    .local v0, "params":[Ljava/lang/Object;
    const-string v1, "KNOX_MODE_RECEIVER"

    invoke-virtual {p1, v1, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private unregisterReceiver(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 86
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    sget-object v1, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->TAG:Ljava/lang/String;

    const-string v2, "catch IllegalArgumentException and ignore it"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    goto :goto_0

    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mKNOXProgressReceiver:Landroid/content/BroadcastReceiver;

    throw v1
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 4
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 65
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/Object;

    move-object v2, v3

    check-cast v2, [Ljava/lang/Object;

    .line 66
    .local v2, "params":[Ljava/lang/Object;
    const/4 v3, 0x0

    aget-object v1, v2, v3

    check-cast v1, Landroid/content/Context;

    .line 67
    .local v1, "context":Landroid/content/Context;
    const/4 v3, 0x1

    aget-object v0, v2, v3

    check-cast v0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    .local v0, "cmdType":Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;
    move-object v3, v1

    .line 68
    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 69
    sget-object v3, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->REGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    if-ne v0, v3, :cond_1

    .line 70
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->registerReceiver(Landroid/content/Context;)V

    .line 73
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    sget-object v3, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;->UNREGISTER_RECEIVER:Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd$CmdType;

    if-ne v0, v3, :cond_0

    .line 72
    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/KnoxModeReceiverCmd;->unregisterReceiver(Landroid/content/Context;)V

    goto :goto_0
.end method
