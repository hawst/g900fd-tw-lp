.class public Lcom/sec/samsung/gallery/model/ThumbnailProxy;
.super Ljava/lang/Object;
.source "ThumbnailProxy.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;,
        Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    }
.end annotation


# static fields
.field private static final MSG_BITMAP_READY:I = 0x2

.field private static final MSG_REQUEST_NONACTIVE_IMAGES:I = 0x3

.field private static final MSG_SIZE_CHANGED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "ThumbnailProxy"


# instance fields
.field private mActiveEnd:I

.field private mActiveMarker:J

.field private mActiveRequestCount:I

.field private mActiveStart:I

.field private mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

.field private mCacheSize:I

.field private mContentEnd:I

.field private mContentStart:I

.field private mContext:Landroid/content/Context;

.field private mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

.field private final mHandler:Landroid/os/Handler;

.field private mIsActive:Z

.field private mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

.field private final mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

.field private mNonActiveRequestCount:I

.field private mScrollState:I

.field private mSize:I

.field private mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

.field private mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 195
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z

    .line 39
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    .line 40
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    .line 42
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    .line 43
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    .line 45
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .line 46
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .line 48
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    .line 51
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mScrollState:I

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    .line 55
    iput-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    .line 58
    iput-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    .line 60
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$1;-><init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    .line 667
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$3;-><init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    .line 198
    iput-object p2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContext:Landroid/content/Context;

    .line 199
    new-instance v0, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;

    invoke-virtual {p2}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$2;-><init>(Lcom/sec/samsung/gallery/model/ThumbnailProxy;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;

    .line 218
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateSlotContent(I)V

    return-void
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->contentSizeChanged(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    return v0
.end method

.method static synthetic access$1402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    return p1
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->procBitmapReady(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->requestNonactiveImages()V

    return-void
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    return v0
.end method

.method static synthetic access$302(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    return p1
.end method

.method static synthetic access$402(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    return p1
.end method

.method static synthetic access$502(Lcom/sec/samsung/gallery/model/ThumbnailProxy;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I

    .prologue
    .line 26
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    return p1
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    return v0
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/model/ThumbnailProxy;IIZ)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # Z

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setContentWindow(IIZ)V

    return-void
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/model/ThumbnailProxy;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateAllImageRequests()V

    return-void
.end method

.method private cancelNonactiveImages()V
    .locals 5

    .prologue
    .line 461
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 464
    .local v1, "range":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 465
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelSlotImage(I)V

    .line 466
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelSlotImage(I)V

    .line 464
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 468
    :cond_0
    return-void
.end method

.method private cancelUpdateImages()V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->pause()V

    .line 437
    :cond_0
    return-void
.end method

.method private checkStartUpdateImages()V
    .locals 4

    .prologue
    .line 440
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    if-eqz v0, :cond_1

    .line 449
    :cond_0
    :goto_0
    return-void

    .line 442
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mScrollState:I

    if-nez v0, :cond_0

    .line 445
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v0, :cond_0

    .line 446
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->setItemArray([Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;II)V

    goto :goto_0
.end method

.method private contentSizeChanged(I)V
    .locals 6
    .param p1, "size"    # I

    .prologue
    const/4 v5, 0x0

    .line 313
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    .line 314
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    add-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v3, v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v4, v4

    sub-int/2addr v3, v4

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-static {v2, v5, v3}, Lcom/sec/android/gallery3d/common/Utils;->clamp(III)I

    move-result v1

    .line 316
    .local v1, "contentStart":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v2, v2

    add-int/2addr v2, v1

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 317
    .local v0, "contentEnd":I
    const-string v2, "ThumbnailProxy"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "contentSizeChanged: Old start = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", end = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", New start = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", end = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", size = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 320
    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    .line 321
    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    .line 323
    invoke-direct {p0, v1, v0, v5}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setContentWindow(IIZ)V

    .line 324
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 325
    return-void
.end method

.method private isEqual(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 8
    .param p1, "mediaItem1"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "mediaItem2"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    const/4 v3, 0x0

    .line 563
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    move v2, v3

    .line 579
    :goto_0
    return v2

    .line 566
    :cond_1
    if-ne p1, p2, :cond_3

    .line 567
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 568
    .local v0, "filePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDrmUtil()Lcom/sec/android/gallery3d/util/ImageDRMUtil;

    move-result-object v1

    .line 570
    .local v1, "imageDRMUtil":Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    if-eqz v1, :cond_2

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/util/ImageDRMUtil;->isValidRights(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v3

    .line 572
    goto :goto_0

    .line 573
    :cond_2
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v4

    invoke-virtual {p2}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v6

    cmp-long v2, v4, v6

    if-nez v2, :cond_3

    .line 575
    const/4 v2, 0x1

    goto :goto_0

    .end local v0    # "filePath":Ljava/lang/String;
    .end local v1    # "imageDRMUtil":Lcom/sec/android/gallery3d/util/ImageDRMUtil;
    :cond_3
    move v2, v3

    .line 579
    goto :goto_0
.end method

.method private prepareSlotContent(I)V
    .locals 10
    .param p1, "slotIndex"    # I

    .prologue
    .line 544
    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 545
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v0, v0

    rem-int v8, p1, v0

    .line 546
    .local v8, "index":I
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    aget-object v6, v0, v8

    .line 547
    .local v6, "displayMediaItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v6, :cond_1

    .line 548
    iget v0, v6, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->mIndex:I

    if-ne v0, p1, :cond_0

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isBitmapReady()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->isEqual(Lcom/sec/android/gallery3d/data/MediaItem;Lcom/sec/android/gallery3d/data/MediaItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "displayMediaItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .end local v8    # "index":I
    :goto_0
    return-void

    .line 554
    .restart local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .restart local v6    # "displayMediaItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .restart local v8    # "index":I
    :cond_0
    invoke-virtual {v6}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->recycle()V

    .line 556
    :cond_1
    iget-object v9, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v5

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;Lcom/sec/android/gallery3d/data/MediaItem;II)V

    aput-object v0, v9, v8
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 557
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v6    # "displayMediaItem":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .end local v8    # "index":I
    :catch_0
    move-exception v7

    .line 558
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private procBitmapReady(I)V
    .locals 6
    .param p1, "index"    # I

    .prologue
    .line 641
    :try_start_0
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    if-lt p1, v1, :cond_2

    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    if-ge p1, v1, :cond_2

    .line 642
    iget-wide v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    const/4 v1, 0x1

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    sub-int v4, p1, v4

    shl-int/2addr v1, v4

    xor-int/lit8 v1, v1, -0x1

    int-to-long v4, v1

    and-long/2addr v2, v4

    iput-wide v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    .line 643
    iget-wide v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 644
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    if-eqz v1, :cond_0

    .line 645
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    sub-int/2addr v2, v3

    invoke-interface {v1, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;->onActiveBitmapComplete(I)V

    .line 648
    :cond_0
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    .line 649
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    if-nez v1, :cond_1

    .line 650
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 651
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 665
    :cond_1
    :goto_0
    return-void

    .line 653
    :cond_2
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    if-lez v1, :cond_1

    .line 654
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    .line 655
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    if-nez v1, :cond_1

    .line 656
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    if-eqz v1, :cond_3

    .line 657
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;->onContentBitmapComplete()V

    .line 659
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->checkStartUpdateImages()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 662
    :catch_0
    move-exception v0

    .line 663
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private requestNonactiveImages()V
    .locals 5

    .prologue
    .line 420
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    sub-int/2addr v3, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 423
    .local v1, "range":I
    const/4 v2, 0x0

    iput v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    .line 424
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 425
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    add-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->requestSlotImage(I)V

    .line 426
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v0

    invoke-direct {p0, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->requestSlotImage(I)V

    .line 424
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 428
    :cond_0
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    if-nez v2, :cond_1

    .line 429
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->checkStartUpdateImages()V

    .line 431
    :cond_1
    return-void
.end method

.method private requestSlotImage(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    .line 400
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 402
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 404
    .local v0, "item":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-nez v0, :cond_2

    .line 405
    const-string v1, "ThumbnailProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "failed to request slot image. display media item is null : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v3, v3

    rem-int v3, p1, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 409
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getThumbType()I

    move-result v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v2

    if-eq v1, v2, :cond_3

    .line 410
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->setThumbType(I)V

    .line 411
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestNewImage()V

    .line 415
    :goto_1
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isRequestInProgress()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 416
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mNonActiveRequestCount:I

    goto :goto_0

    .line 413
    :cond_3
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestImage()V

    goto :goto_1
.end method

.method private resetOutOfActivieThumbnail(II)V
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 296
    if-ltz p1, :cond_0

    if-gez p2, :cond_2

    .line 297
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 301
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetOutOfActivieThumbnail(II)V

    .line 302
    return-void

    .line 298
    :cond_2
    if-lt p1, p2, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    goto :goto_0
.end method

.method private declared-synchronized setContentWindow(IIZ)V
    .locals 5
    .param p1, "contentStart"    # I
    .param p2, "contentEnd"    # I
    .param p3, "needSetAdapter"    # Z

    .prologue
    .line 496
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    if-ne p1, v2, :cond_1

    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p2, v2, :cond_1

    .line 538
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 499
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z

    if-nez v2, :cond_2

    .line 500
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .line 501
    iput p2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .line 502
    if-eqz p3, :cond_0

    .line 503
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    invoke-virtual {v2, p1, p2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->setActiveWindow(IIII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 496
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 508
    :cond_2
    :try_start_2
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    if-ge p1, v2, :cond_3

    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    if-lt v2, p2, :cond_6

    .line 509
    :cond_3
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .local v1, "n":I
    :goto_1
    if-ge v0, v1, :cond_4

    .line 510
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->freeSlotContent(I)V

    .line 509
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 512
    :cond_4
    if-eqz p3, :cond_5

    .line 513
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    invoke-virtual {v2, p1, p2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->setActiveWindow(IIII)V

    .line 515
    :cond_5
    move v0, p1

    :goto_2
    if-ge v0, p2, :cond_b

    .line 516
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 515
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 519
    .end local v0    # "i":I
    .end local v1    # "n":I
    :cond_6
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .restart local v0    # "i":I
    :goto_3
    if-ge v0, p1, :cond_7

    .line 520
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->freeSlotContent(I)V

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 522
    :cond_7
    move v0, p2

    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .restart local v1    # "n":I
    :goto_4
    if-ge v0, v1, :cond_8

    .line 523
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->freeSlotContent(I)V

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 525
    :cond_8
    if-eqz p3, :cond_9

    .line 526
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    invoke-virtual {v2, p1, p2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->setActiveWindow(IIII)V

    .line 528
    :cond_9
    move v0, p1

    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    :goto_5
    if-ge v0, v1, :cond_a

    .line 529
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 528
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 531
    :cond_a
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    :goto_6
    if-ge v0, p2, :cond_b

    .line 532
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 531
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 536
    :cond_b
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .line 537
    iput p2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private updateAllImageRequests()V
    .locals 8

    .prologue
    .line 367
    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v5, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    if-ne v4, v5, :cond_0

    .line 397
    :goto_0
    return-void

    .line 370
    :cond_0
    const/4 v4, 0x0

    iput v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    .line 371
    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    .line 373
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 374
    .local v0, "data":[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    .local v1, "i":I
    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    .local v3, "n":I
    :goto_1
    if-ge v1, v3, :cond_5

    .line 375
    array-length v4, v0

    rem-int v4, v1, v4

    aget-object v2, v0, v4

    .line 376
    .local v2, "item":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-nez v2, :cond_2

    .line 374
    :cond_1
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 379
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v4, v4, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getThumbType()I

    move-result v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v5

    if-eq v4, v5, :cond_4

    .line 380
    iget-object v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->setThumbType(I)V

    .line 381
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestNewImage()V

    .line 385
    :goto_3
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isValid()Z

    move-result v4

    if-nez v4, :cond_3

    .line 386
    iget-wide v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    const/4 v6, 0x1

    iget v7, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    sub-int v7, v1, v7

    shl-int/2addr v6, v7

    int-to-long v6, v6

    or-long/2addr v4, v6

    iput-wide v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    .line 388
    :cond_3
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isRequestInProgress()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 389
    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    goto :goto_2

    .line 383
    :cond_4
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestImage()V

    goto :goto_3

    .line 392
    .end local v2    # "item":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    :cond_5
    iget v4, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    if-nez v4, :cond_6

    .line 393
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->requestNonactiveImages()V

    goto :goto_0

    .line 395
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelNonactiveImages()V

    goto :goto_0
.end method

.method private updateSlotContent(I)V
    .locals 14
    .param p1, "slotIndex"    # I

    .prologue
    .line 328
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->isActiveSlot(I)Z

    move-result v8

    .line 329
    .local v8, "isActive":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 330
    .local v3, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    iget-object v6, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 331
    .local v6, "data":[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    array-length v1, v6

    rem-int v7, p1, v1

    .line 332
    .local v7, "index":I
    aget-object v9, v6, v7

    .line 333
    .local v9, "original":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v3, :cond_1

    if-eqz v9, :cond_1

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getVersion()J

    move-result-wide v10

    sget-wide v12, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->DEFAULT_VERSION:J

    cmp-long v1, v10, v12

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v10

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->getVersion()J

    move-result-wide v12

    cmp-long v1, v10, v12

    if-nez v1, :cond_1

    .line 336
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->notifyDataSetChanged(II)V

    .line 364
    :cond_0
    :goto_0
    return-void

    .line 339
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/adapter/MediaSetAdapter;

    if-eqz v1, :cond_7

    if-eqz v3, :cond_7

    const-wide/16 v10, 0x4

    invoke-virtual {v3, v10, v11}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v5, 0x2

    .line 341
    .local v5, "mType":I
    :goto_1
    new-instance v0, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mListenerBitmap:Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;-><init>(Landroid/content/Context;Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem$OnBitmapAvailableListener;Lcom/sec/android/gallery3d/data/MediaItem;II)V

    .line 343
    .local v0, "update":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v3, :cond_2

    if-eqz v0, :cond_2

    .line 344
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getDataVersion()J

    move-result-wide v10

    invoke-virtual {v0, v10, v11}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->setVersion(J)V

    .line 345
    :cond_2
    aput-object v0, v6, v7

    .line 346
    if-eqz v9, :cond_4

    .line 347
    if-eqz v8, :cond_3

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->isRequestInProgress()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 348
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    .line 350
    :cond_3
    invoke-virtual {v9}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->recycle()V

    .line 352
    :cond_4
    if-eqz v8, :cond_8

    .line 353
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    if-nez v1, :cond_5

    .line 354
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelNonactiveImages()V

    .line 356
    :cond_5
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    .line 357
    if-eqz v0, :cond_6

    .line 358
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestImage()V

    .line 359
    :cond_6
    iget-wide v10, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    const/4 v1, 0x1

    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    sub-int v2, p1, v2

    shl-int/2addr v1, v2

    int-to-long v12, v1

    or-long/2addr v10, v12

    iput-wide v10, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveMarker:J

    goto :goto_0

    .line 339
    .end local v0    # "update":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .end local v5    # "mType":I
    :cond_7
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getThumbSize()I

    move-result v5

    goto :goto_1

    .line 361
    .restart local v0    # "update":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .restart local v5    # "mType":I
    :cond_8
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveRequestCount:I

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 362
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->requestImage()V

    goto :goto_0
.end method


# virtual methods
.method public cancelSlotImage(I)V
    .locals 3
    .param p1, "slotIndex"    # I

    .prologue
    .line 452
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    if-lt p1, v1, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    if-lt p1, v1, :cond_1

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 454
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v2, v2

    rem-int v2, p1, v2

    aget-object v0, v1, v2

    .line 455
    .local v0, "item":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v0, :cond_0

    .line 457
    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->cancelImageRequest()V

    goto :goto_0
.end method

.method public declared-synchronized freeSlotContent(I)V
    .locals 4
    .param p1, "slotIndex"    # I

    .prologue
    .line 583
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 584
    .local v0, "data":[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    iget-object v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    array-length v3, v3

    rem-int v1, p1, v3

    .line 585
    .local v1, "index":I
    aget-object v2, v0, v1

    .line 586
    .local v2, "original":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    if-eqz v2, :cond_0

    .line 587
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;->recycle()V

    .line 588
    const/4 v3, 0x0

    aput-object v3, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    :cond_0
    monitor-exit p0

    return-void

    .line 583
    .end local v0    # "data":[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .end local v1    # "index":I
    .end local v2    # "original":Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method public getAdapter()Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    return-object v0
.end method

.method public getBitmap(I)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->getBitmap(I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getCacheSize()I
    .locals 1

    .prologue
    .line 260
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    return v0
.end method

.method public getDispItemList()[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    return-object v0
.end method

.method public isActiveSlot(I)Z
    .locals 1
    .param p1, "slotIndex"    # I

    .prologue
    .line 309
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    if-lt p1, v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 605
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->pause(Z)V

    .line 606
    return-void
.end method

.method public declared-synchronized pause(Z)V
    .locals 4
    .param p1, "keepActiveThumbnail"    # Z

    .prologue
    .line 609
    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z

    .line 610
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 611
    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelSlotImage(I)V

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 613
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v2, :cond_1

    .line 614
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->stop()V

    .line 617
    :cond_1
    if-eqz p1, :cond_2

    .line 618
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    invoke-direct {p0, v2, v3}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetOutOfActivieThumbnail(II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 623
    :goto_1
    monitor-exit p0

    return-void

    .line 621
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 609
    .end local v0    # "i":I
    .end local v1    # "n":I
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method

.method public reloadRectOfAllFaces()V
    .locals 3

    .prologue
    .line 676
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    if-ge v0, v2, :cond_1

    .line 677
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 678
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v2, :cond_0

    .line 679
    check-cast v1, Lcom/sec/android/gallery3d/data/LocalImage;

    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/LocalImage;->loadRectOfAllFaces()V

    .line 676
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 682
    :cond_1
    return-void
.end method

.method public replaceChangedSlot(I)V
    .locals 0
    .param p1, "index"    # I

    .prologue
    .line 305
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 306
    return-void
.end method

.method public resetAllThumbnails()V
    .locals 1

    .prologue
    .line 291
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetAllThumbnails()V

    .line 292
    return-void
.end method

.method public resetContentWindow()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 265
    return-void
.end method

.method public resume()V
    .locals 3

    .prologue
    .line 593
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z

    .line 594
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .local v0, "i":I
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 595
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 594
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 597
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateAllImageRequests()V

    .line 599
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v2, :cond_1

    .line 600
    iget-object v2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-virtual {v2}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->start()V

    .line 602
    :cond_1
    return-void
.end method

.method public declared-synchronized setActiveWindow(II)V
    .locals 2
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 272
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 278
    :goto_0
    monitor-exit p0

    return-void

    .line 274
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->loadContentData(Z)V

    .line 275
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelUpdateImages()V

    .line 277
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0, p1, p2}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->setActiveWindow(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 272
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V
    .locals 2
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .prologue
    .line 230
    if-nez p1, :cond_0

    .line 231
    const-string v0, "ThumbnailProxy"

    const-string v1, "Input argument adapter is null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    :goto_0
    return-void

    .line 235
    .restart local p1    # "adapter":Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v0, :cond_1

    .line 236
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-virtual {v0}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->resetItemArray()V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    invoke-virtual {v0, p1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 239
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .line 240
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getCount()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    .line 241
    check-cast p1, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;

    .end local p1    # "adapter":Lcom/sec/samsung/gallery/glview/GlBaseAdapter;
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-virtual {p1, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter;->setModelListener(Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;)V

    goto :goto_0
.end method

.method public setBitmapStateChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;)V
    .locals 0
    .param p1, "bitmapStateChangedListener"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    .prologue
    .line 287
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mBitmapStateChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$BitmapStateChangedListener;

    .line 288
    return-void
.end method

.method public setCacheSize(I)V
    .locals 1
    .param p1, "cacheSize"    # I

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 254
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetContentWindow()V

    .line 255
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    .line 256
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    new-array v0, v0, [Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mDisplayMediaItems:[Lcom/sec/samsung/gallery/view/image_manager/DisplayMediaItem;

    .line 257
    return-void
.end method

.method public setCurrentScrollState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 632
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mScrollState:I

    .line 633
    iget v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mScrollState:I

    if-nez v0, :cond_0

    .line 634
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->checkStartUpdateImages()V

    .line 637
    :goto_0
    return-void

    .line 636
    :cond_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->cancelUpdateImages()V

    goto :goto_0
.end method

.method public setEnableVideoWall()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;

    iget-object v0, v0, Lcom/sec/android/gallery3d/app/GalleryAppImpl;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    .line 247
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mVideoWallMgr:Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;

    iget-object v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    invoke-virtual {v0, v1}, Lcom/sec/android/app/videoplayer/videowall/VideoWallMgr;->setAdapter(Lcom/sec/samsung/gallery/glview/GlBaseAdapter;)V

    .line 250
    :cond_0
    return-void
.end method

.method public declared-synchronized setInitActiveWindow(II)V
    .locals 4
    .param p1, "start"    # I
    .param p2, "end"    # I

    .prologue
    .line 472
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mIsActive:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_1

    .line 493
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 476
    :cond_1
    if-eq p1, p2, :cond_0

    .line 479
    :try_start_1
    iget v1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    add-int/2addr v1, p1

    if-lt p2, v1, :cond_2

    .line 480
    const-string v1, "ThumbnailProxy"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setInitActiveWindow : error (max ActiveWindow = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mCacheSize:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "), start = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", end = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 472
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 484
    :cond_2
    :try_start_2
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveStart:I

    .line 485
    iput p2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mActiveEnd:I

    .line 487
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentStart:I

    .line 488
    iput p2, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mContentEnd:I

    .line 489
    move v0, p1

    .local v0, "i":I
    :goto_1
    if-ge v0, p2, :cond_3

    .line 490
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->prepareSlotContent(I)V

    .line 489
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 492
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->updateAllImageRequests()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public setSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 225
    iput p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mSize:I

    .line 226
    return-void
.end method

.method public setWindowContentChangedListener(Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;)V
    .locals 0
    .param p1, "windowContentChangedListener"    # Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    .prologue
    .line 282
    iput-object p1, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;

    .line 283
    return-void
.end method

.method public declared-synchronized stop()V
    .locals 2

    .prologue
    .line 626
    monitor-enter p0

    :try_start_0
    const-string v0, "ThumbnailProxy"

    const-string/jumbo v1, "stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->resetAllThumbnails()V

    .line 628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->mWindowContentChangedListener:Lcom/sec/samsung/gallery/model/ThumbnailProxy$WindowContentChangedListener;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 629
    monitor-exit p0

    return-void

    .line 626
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
