.class Lcom/sec/samsung/gallery/glview/GlGridView$8;
.super Ljava/lang/Object;
.source "GlGridView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGridView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlGridView;)V
    .locals 0

    .prologue
    .line 1008
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 1014
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGridView;->onDrag(II)Z
    invoke-static {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$700(Lcom/sec/samsung/gallery/glview/GlGridView;II)Z

    move-result v0

    .line 1017
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0, p2, p3}, Lcom/sec/samsung/gallery/glview/GlGridView;->onMoved(II)Z

    move-result v0

    goto :goto_0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    iget v1, v1, Lcom/sec/samsung/gallery/glview/GlGridView;->mHeight:I

    invoke-virtual {v0, p2, v1}, Lcom/sec/samsung/gallery/glview/GlGridView;->onPressed(II)Z

    move-result v0

    return v0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    .line 1022
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseReOrderAlbums:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->isDragging()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1023
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGridView;->mReorderHelper:Lcom/sec/samsung/gallery/glview/GlReorderHelper;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlGridView;->access$600(Lcom/sec/samsung/gallery/glview/GlGridView;)Lcom/sec/samsung/gallery/glview/GlReorderHelper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/GlReorderHelper;->onDragEnd()Z

    .line 1024
    const/4 v0, 0x1

    .line 1026
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGridView$8;->this$0:Lcom/sec/samsung/gallery/glview/GlGridView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/sec/samsung/gallery/glview/GlGridView;->onReleased(IIII)Z

    move-result v0

    goto :goto_0
.end method
