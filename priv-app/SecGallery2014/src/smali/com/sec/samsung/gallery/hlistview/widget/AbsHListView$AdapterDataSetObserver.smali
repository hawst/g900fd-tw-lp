.class public Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;
.super Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;
.source "AbsHListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "AdapterDataSetObserver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/sec/samsung/gallery/hlistview/widget/AdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">.AdapterDataSetObserver;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 0

    .prologue
    .line 5250
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$AdapterDataSetObserver;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;-><init>(Lcom/sec/samsung/gallery/hlistview/widget/AdapterView;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic clearSavedState()V
    .locals 0

    .prologue
    .line 5250
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->clearSavedState()V

    return-void
.end method

.method public onChanged()V
    .locals 2

    .prologue
    .line 5254
    const-string v0, "AbsListView"

    const-string v1, "onChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5255
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->onChanged()V

    .line 5256
    return-void
.end method

.method public onInvalidated()V
    .locals 2

    .prologue
    .line 5260
    const-string v0, "AbsListView"

    const-string v1, "onInvalidated"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 5261
    invoke-super {p0}, Lcom/sec/samsung/gallery/hlistview/widget/AdapterView$AdapterDataSetObserver;->onInvalidated()V

    .line 5262
    return-void
.end method
