.class abstract Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;
.super Ljava/lang/Object;
.source "AbstractDrawerGroup.java"


# instance fields
.field protected mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field protected mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/samsung/gallery/drawer/IDrawerCallback;)V
    .locals 0
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .param p2, "callback"    # Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    iput-object p1, p0, Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 13
    iput-object p2, p0, Lcom/sec/samsung/gallery/drawer/AbstractDrawerGroup;->mCallback:Lcom/sec/samsung/gallery/drawer/IDrawerCallback;

    .line 14
    return-void
.end method


# virtual methods
.method public abstract initialize()V
.end method

.method public abstract onDestroy()V
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method
