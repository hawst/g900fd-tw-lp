.class Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;
.super Ljava/lang/Object;
.source "GlHoverController.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlMoveListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)V
    .locals 0

    .prologue
    .line 1087
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMove(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 1
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I

    .prologue
    .line 1094
    const/4 v0, 0x1

    return v0
.end method

.method public onPress(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 2
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    .line 1089
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlHandler;->removeMessage(I)V

    .line 1090
    const/4 v0, 0x1

    return v0
.end method

.method public onRelease(Lcom/sec/android/gallery3d/glcore/GlObject;IIII)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "dx"    # I
    .param p3, "dy"    # I
    .param p4, "speedX"    # I
    .param p5, "speedY"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1098
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mTotalCount:I
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1800(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)I

    move-result v0

    const/16 v1, 0x9

    if-lt v0, v1, :cond_1

    .line 1099
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # setter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mNeedDelay:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1702(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;Z)Z

    .line 1100
    int-to-long v0, p2

    const-wide/16 v2, -0x50

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 1101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v4, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    .line 1109
    :goto_0
    return v5

    .line 1103
    :cond_0
    int-to-long v0, p2

    const-wide/16 v2, 0x50

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 1104
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->access$1600(Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;)Lcom/sec/android/gallery3d/glcore/GlHandler;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v4, v4, v4}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto :goto_0

    .line 1108
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController$7;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;

    invoke-virtual {v0, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlHoverController;->exitFromHoverView(Z)V

    goto :goto_0
.end method
