.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;
.super Ljava/lang/Object;
.source "GallerySearchActionBar.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchView$OnVoiceRecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->initializeSearchView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVoiceRecognitionStarted()V
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    invoke-static {}, Lcom/sec/samsung/gallery/core/Event$Builder;->Create()Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    sget v2, Lcom/sec/samsung/gallery/core/Event;->EVENT_PRESS_VOICE_SEARCH:I

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/Event;->setType(I)Lcom/sec/samsung/gallery/core/Event;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->notifyObservers(Ljava/lang/Object;)V

    .line 269
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar$4;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchActionBar;->setVoiceSearchState(Z)V

    .line 270
    return-void
.end method
