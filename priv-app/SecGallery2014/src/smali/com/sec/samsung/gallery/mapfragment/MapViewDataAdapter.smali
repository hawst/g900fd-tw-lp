.class public Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
.super Landroid/widget/BaseAdapter;
.source "MapViewDataAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private final mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field protected final mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "inputSetPath"    # Ljava/lang/String;
    .param p3, "mediaFilterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 36
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter$1;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    .line 45
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mContext:Landroid/content/Context;

    .line 47
    check-cast p1, Landroid/app/Activity;

    .end local p1    # "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/app/Activity;->getApplication()Landroid/app/Application;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v1}, Lcom/sec/android/gallery3d/app/GalleryApp;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 48
    if-nez p2, :cond_0

    .line 49
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object p2

    .line 53
    :cond_0
    if-eqz p3, :cond_2

    .line 54
    invoke-virtual {p2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 60
    .local v0, "newPath":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 61
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_1

    .line 62
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 63
    :cond_1
    return-void

    .line 57
    .end local v0    # "newPath":Ljava/lang/String;
    :cond_2
    move-object v0, p2

    .restart local v0    # "newPath":Ljava/lang/String;
    goto :goto_0
.end method


# virtual methods
.method public addSourceContentListener()V
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->addContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 68
    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "arg0"    # I

    .prologue
    .line 103
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getMediaItemList(I)Ljava/util/ArrayList;
    .locals 4
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 93
    .local v0, "mediaList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v2, p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v1

    .line 94
    .local v1, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v1, :cond_0

    .line 95
    const/4 v2, 0x0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v0

    .line 97
    :cond_0
    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # Landroid/view/View;
    .param p3, "arg2"    # Landroid/view/ViewGroup;

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method public reloadData()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->reload()J

    .line 79
    :cond_0
    return-void
.end method

.method public removeSourceContentListener()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mInputMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->mSourceListener:Lcom/sec/android/gallery3d/data/ContentListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/MediaSet;->removeContentListener(Lcom/sec/android/gallery3d/data/ContentListener;)V

    .line 73
    :cond_0
    return-void
.end method
