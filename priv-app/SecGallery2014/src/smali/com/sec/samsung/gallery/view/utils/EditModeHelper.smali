.class public Lcom/sec/samsung/gallery/view/utils/EditModeHelper;
.super Ljava/lang/Object;
.source "EditModeHelper.java"


# static fields
.field public static final MAX_DEFAULT_SELECTION_NUMBER:I = 0x64

.field public static final MAX_DOWNLOAD_SELECTION_NUMBER:I = 0x32

.field public static final MAX_EDIT_SELECTION_NUMBER:I = 0x8

.field public static final MAX_MOTION_PICTURE_LIST_NUMBER:I = 0x14

.field public static final MIN_DEFAULT_SELECTION_NUMBER:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private final mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 88
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 89
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/utils/EditModeHelper;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private dismissSlideShowSettings()V
    .locals 2

    .prologue
    .line 812
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$2;

    invoke-direct {v1, p0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$2;-><init>(Lcom/sec/samsung/gallery/view/utils/EditModeHelper;)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 819
    return-void
.end method

.method private getImageToEdit()Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 268
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v2

    .line 269
    .local v2, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v1, v3

    .line 284
    :cond_0
    :goto_0
    return-object v1

    .line 274
    :cond_1
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaItem;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 280
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x4

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v4

    sget-object v5, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_SNS:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v4, v5, :cond_0

    :cond_2
    move-object v1, v3

    .line 282
    goto :goto_0

    .line 275
    .end local v1    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v0}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    move-object v1, v3

    .line 277
    goto :goto_0
.end method

.method private getMediaSetArray(Lcom/sec/android/gallery3d/data/MediaSet;I)[Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "mCurrentMediaSetIndex"    # I

    .prologue
    .line 442
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v1

    .line 443
    .local v1, "listSize":I
    new-array v2, v1, [Lcom/sec/android/gallery3d/data/MediaSet;

    .line 445
    .local v2, "mediaSetArray":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSetCount()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 446
    invoke-virtual {p1, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v3

    aput-object v3, v2, v0

    .line 445
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :cond_0
    return-object v2
.end method


# virtual methods
.method public CopyToEvent(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 149
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 152
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "COPY_TO_EVENT"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public chooseShareDialog()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 835
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseAdvancedSoundSceneShare:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/SoundSceneCmd;->isASoundSceneFileSelected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/gallery3d/golf/GolfMgr;->isGolfFileSelected(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/Play3DTourCmd;->isSelected3DTour(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 837
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    .line 840
    :goto_0
    return-void

    .line 839
    :cond_2
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showShareDialog(Z)V

    goto :goto_0
.end method

.method public copyToClipboard()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 106
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 107
    .local v1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 114
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 110
    .local v0, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 113
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "COPY_TO_CLIPBOARD"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public copyToClipboard(Ljava/lang/String;)V
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 117
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 120
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "COPY_TO_CLIPBOARD"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 121
    return-void
.end method

.method public dismissDialogs()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 457
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRenameDialog(Z)V

    .line 458
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showPhotoEditorDialog(Z)V

    .line 459
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Z)V

    .line 460
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseChooserActivity:Z

    if-nez v0, :cond_0

    .line 461
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showShareDialog(Z)V

    .line 462
    :cond_0
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showImageVideoConversionShareDialog(ZZ)V

    .line 463
    invoke-virtual {p0, v1, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDeleteDialog(ZZ)V

    .line 464
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showDetailsDialog(Z)V

    .line 465
    invoke-virtual {p0, v2, v1, v1, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V

    .line 466
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumDialog(Z)V

    .line 467
    invoke-virtual {p0, v1, v2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V

    .line 468
    invoke-virtual {p0, v2, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showNewAlbumCopyMoveDialog(Ljava/lang/String;Z)V

    .line 469
    invoke-virtual {p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->CopyToEvent(Z)V

    .line 470
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->dismissSlideShowSettings()V

    .line 471
    return-void
.end method

.method public download()V
    .locals 21

    .prologue
    .line 378
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v8

    .line 379
    .local v8, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 380
    .local v4, "copiedList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-static {v4, v8}, Ljava/util/Collections;->copy(Ljava/util/List;Ljava/util/List;)V

    .line 382
    const/4 v13, 0x1

    new-array v11, v13, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v4, v11, v13

    .line 386
    .local v11, "params":[Ljava/lang/Object;
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 387
    .local v10, "nearbyList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 388
    .local v12, "sLinkList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v3, "cloudList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v9

    .local v9, "n":I
    :goto_0
    if-ge v6, v9, :cond_5

    .line 391
    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 393
    .local v7, "item":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    if-eqz v13, :cond_1

    move-object v5, v7

    .line 394
    check-cast v5, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;

    .line 395
    .local v5, "device":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    const/4 v13, 0x0

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->getMediaItemCount()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v5, v13, v0}, Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 390
    .end local v5    # "device":Lcom/sec/android/gallery3d/remote/nearby/NearbyDevice;
    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 396
    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_1
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/nearby/NearbyImage;

    if-eqz v13, :cond_2

    .line 397
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 398
    .restart local v7    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/slink/SLinkImage;

    if-eqz v13, :cond_3

    .line 399
    invoke-interface {v12, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 401
    :cond_3
    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/cloud/CloudMediaItem;

    if-nez v13, :cond_4

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    if-nez v13, :cond_4

    instance-of v13, v7, Lcom/sec/android/gallery3d/remote/sns/SNSImage;

    if-eqz v13, :cond_0

    .line 403
    :cond_4
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v7    # "item":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-interface {v3, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 408
    :cond_5
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_7

    .line 409
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v13, v10}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getStorageShortageInByte(Landroid/content/Context;Ljava/util/List;)J

    move-result-wide v14

    .line 410
    .local v14, "storageShortage":J
    const-wide/16 v16, 0x0

    cmp-long v13, v14, v16

    if-lez v13, :cond_6

    .line 411
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v13, v14, v15}, Lcom/sec/android/gallery3d/remote/nearby/NearbyUtils;->getStorageAlertDialog(Landroid/content/Context;J)Landroid/app/AlertDialog;

    move-result-object v2

    .line 413
    .local v2, "alertDialog":Landroid/app/AlertDialog;
    invoke-virtual {v2}, Landroid/app/AlertDialog;->show()V

    .line 439
    .end local v2    # "alertDialog":Landroid/app/AlertDialog;
    .end local v14    # "storageShortage":J
    :goto_2
    return-void

    .line 417
    .restart local v14    # "storageShortage":J
    :cond_6
    const/4 v13, 0x0

    aput-object v10, v11, v13

    .line 418
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v16, "DOWNLOAD_NEARBY"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v11}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 421
    .end local v14    # "storageShortage":J
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v13, v12}, Lcom/sec/android/gallery3d/remote/slink/SLinkManager;->downloadFiles(Landroid/content/Context;Ljava/util/List;)V

    .line 423
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v13

    const/16 v16, 0x32

    move/from16 v0, v16

    if-le v13, v0, :cond_8

    .line 424
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    move-object/from16 v16, v0

    const v17, 0x7f0e0114

    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x32

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v18, v19

    invoke-virtual/range {v16 .. v18}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-static {v13, v0}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_2

    .line 429
    :cond_8
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_9

    .line 430
    const/4 v13, 0x0

    aput-object v3, v11, v13

    .line 431
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v16, "DOWNLOAD_CLOUDBY"

    move-object/from16 v0, v16

    invoke-virtual {v13, v0, v11}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 438
    :cond_9
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v16, "EXIT_SELECTION_MODE"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v13, v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2
.end method

.method public getSupportedOperationForSelectedAlbumItem()J
    .locals 24

    .prologue
    .line 644
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v13

    .line 645
    .local v13, "selectedMediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v13, :cond_0

    invoke-virtual {v13}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v19

    if-eqz v19, :cond_2

    .line 646
    :cond_0
    const-wide/16 v16, 0x0

    .line 763
    :cond_1
    :goto_0
    return-wide v16

    .line 648
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v18

    .line 649
    .local v18, "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    const-wide/16 v16, -0x1

    .line 650
    .local v16, "supportedOperation":J
    const/4 v2, 0x0

    .line 651
    .local v2, "count":I
    const/4 v10, 0x0

    .line 652
    .local v10, "itemCount":I
    const/4 v9, 0x0

    .line 653
    .local v9, "isSupportSlideShow":Z
    const/4 v5, 0x1

    .line 654
    .local v5, "isSupportMoveToKNOX":Z
    const/4 v7, 0x1

    .line 656
    .local v7, "isSupportRemoveFromKNOX":Z
    const/4 v8, 0x1

    .line 657
    .local v8, "isSupportSStudio":Z
    const/4 v6, 0x0

    .line 658
    .local v6, "isSupportMoveToSecretBox":Z
    const/4 v4, 0x0

    .line 660
    .local v4, "isLocalMediaOnly":Z
    monitor-enter v13

    .line 661
    :try_start_0
    invoke-virtual {v13}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v19

    if-eqz v19, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 662
    .local v12, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v0, v12, Lcom/sec/android/gallery3d/data/MediaSet;

    move/from16 v19, v0

    if-nez v19, :cond_5

    .line 663
    instance-of v0, v12, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v19, v0

    if-eqz v19, :cond_3

    .line 666
    invoke-virtual {v12}, Lcom/sec/android/gallery3d/data/MediaObject;->getSupportedOperations()J

    move-result-wide v14

    .line 667
    .local v14, "support":J
    if-eqz v5, :cond_4

    .line 668
    const-wide/32 v20, 0x2000000

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-nez v19, :cond_4

    .line 669
    const/4 v5, 0x0

    .line 672
    :cond_4
    if-eqz v7, :cond_3

    .line 673
    const-wide/32 v20, 0x1000000

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-nez v19, :cond_3

    .line 674
    const/4 v7, 0x0

    goto :goto_1

    .line 679
    .end local v14    # "support":J
    :cond_5
    move-object v0, v12

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    move-object v11, v0

    .line 680
    .local v11, "mediaAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v0, v11, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    move/from16 v19, v0

    if-nez v19, :cond_6

    if-nez v4, :cond_6

    .line 681
    const/4 v4, 0x1

    .line 684
    :cond_6
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getSupportedOperations()J

    move-result-wide v14

    .line 685
    .restart local v14    # "support":J
    if-nez v9, :cond_7

    .line 686
    const-wide v20, 0x400000000000L

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-lez v19, :cond_b

    const/4 v9, 0x1

    .line 688
    :cond_7
    :goto_2
    if-eqz v5, :cond_8

    .line 689
    const-wide/32 v20, 0x2000000

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-nez v19, :cond_8

    .line 690
    const/4 v5, 0x0

    .line 693
    :cond_8
    if-eqz v7, :cond_9

    .line 694
    const-wide/32 v20, 0x1000000

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-nez v19, :cond_9

    .line 695
    const/4 v7, 0x0

    .line 699
    :cond_9
    sget-boolean v19, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v19, :cond_a

    .line 700
    if-nez v6, :cond_a

    .line 701
    const-wide/32 v20, 0x40000000

    and-long v20, v20, v14

    const-wide/16 v22, 0x0

    cmp-long v19, v20, v22

    if-eqz v19, :cond_a

    .line 702
    const/4 v6, 0x1

    .line 721
    :cond_a
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v19

    add-int v10, v10, v19

    .line 722
    and-long v16, v16, v14

    .line 723
    add-int/lit8 v2, v2, 0x1

    .line 724
    goto/16 :goto_1

    .line 686
    :cond_b
    const/4 v9, 0x0

    goto :goto_2

    .line 725
    .end local v11    # "mediaAlbum":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v12    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v14    # "support":J
    :cond_c
    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 728
    sget-object v19, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    if-eq v0, v1, :cond_d

    .line 729
    const-wide v20, -0x4000000000001L

    and-long v16, v16, v20

    .line 733
    :cond_d
    const/16 v19, 0x1

    move/from16 v0, v19

    if-le v2, v0, :cond_e

    .line 734
    const-wide v20, -0x4a00000000000001L    # -1.368455531567204E-48

    and-long v16, v16, v20

    .line 739
    :cond_e
    if-nez v5, :cond_f

    .line 740
    const-wide/32 v20, -0x2000001

    and-long v16, v16, v20

    .line 742
    :cond_f
    if-nez v7, :cond_10

    .line 743
    const-wide/32 v20, -0x1000001

    and-long v16, v16, v20

    .line 750
    :cond_10
    if-eqz v8, :cond_11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    move-object/from16 v19, v0

    invoke-static/range {v19 .. v19}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->getSelectionItemMaxLimit(Landroid/content/Context;)I

    move-result v19

    move/from16 v0, v19

    if-le v10, v0, :cond_12

    .line 751
    :cond_11
    const-wide/32 v20, -0x200001

    and-long v16, v16, v20

    .line 754
    :cond_12
    if-eqz v6, :cond_13

    if-nez v4, :cond_13

    .line 755
    const-wide/32 v20, 0x40000000

    or-long v16, v16, v20

    .line 756
    const-wide/32 v20, -0x20000001

    and-long v16, v16, v20

    .line 759
    :cond_13
    sget v19, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    move/from16 v0, v19

    if-le v10, v0, :cond_1

    .line 760
    const-wide/32 v20, -0x800001

    and-long v16, v16, v20

    goto/16 :goto_0

    .line 725
    .end local v3    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v19

    :try_start_1
    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v19
.end method

.method public getSupportedOperationForSelectedItem()J
    .locals 34

    .prologue
    .line 474
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v28

    if-eqz v28, :cond_1

    .line 475
    const-wide/16 v24, 0x0

    .line 640
    :cond_0
    :goto_0
    return-wide v24

    .line 478
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType()Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v26

    .line 479
    .local v26, "tagType":Lcom/sec/samsung/gallery/core/TabTagType;
    const-wide/16 v24, -0x1

    .line 480
    .local v24, "supportedOperation":J
    const/4 v3, 0x0

    .line 481
    .local v3, "hadBrokenImage":Z
    const/4 v2, 0x0

    .line 482
    .local v2, "count":I
    const/16 v17, 0x0

    .line 483
    .local v17, "selectionContainsPicasaVideo":I
    const/16 v22, 0x1

    .line 484
    .local v22, "supportBurstShot":Z
    const/16 v23, 0x1

    .line 485
    .local v23, "supportMotionPicture":Z
    const/4 v12, 0x0

    .line 486
    .local v12, "isSupportSlideShow":Z
    const/4 v11, 0x1

    .line 487
    .local v11, "isSupportShare":Z
    const/16 v18, 0x1

    .line 488
    .local v18, "showLastShare":Z
    const/16 v19, 0x1

    .line 489
    .local v19, "supportAddTag":Z
    const/4 v10, 0x0

    .line 490
    .local v10, "isSupportMoveToSecretBox":Z
    const/4 v9, 0x0

    .line 491
    .local v9, "isLocalMediaOnly":Z
    const/16 v27, 0x0

    .line 492
    .local v27, "unconfirmedLocalFaceItemCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v29

    monitor-enter v29

    .line 493
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-object/from16 v28, v0

    invoke-virtual/range {v28 .. v28}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v14

    .line 494
    .local v14, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const-wide/16 v4, -0x1

    .line 497
    .local v4, "groupId":J
    const/16 v28, 0x0

    move/from16 v0, v28

    invoke-virtual {v14, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 498
    .local v15, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v15, :cond_2

    instance-of v0, v15, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v28, v0

    if-eqz v28, :cond_2

    .line 499
    check-cast v15, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v15    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-virtual {v15}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    .line 502
    :cond_2
    invoke-virtual {v14}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    .local v6, "i$":Ljava/util/Iterator;
    :cond_3
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v28

    if-eqz v28, :cond_18

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 503
    .local v16, "mediaobj":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz v16, :cond_3

    move-object/from16 v0, v16

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move/from16 v28, v0

    if-eqz v28, :cond_3

    .line 507
    move-object/from16 v0, v16

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    move/from16 v28, v0

    if-nez v28, :cond_4

    if-nez v9, :cond_4

    .line 508
    const/4 v9, 0x1

    .line 511
    :cond_4
    move-object/from16 v0, v16

    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    move-object v13, v0

    .line 512
    .local v13, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    add-int/lit8 v2, v2, 0x1

    .line 513
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getSupportedOperations()J

    move-result-wide v20

    .line 514
    .local v20, "support":J
    if-nez v12, :cond_5

    .line 515
    const-wide v30, 0x400000000000L

    and-long v30, v30, v20

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-lez v28, :cond_14

    const/4 v12, 0x1

    .line 518
    :cond_5
    :goto_2
    if-eqz v11, :cond_7

    .line 519
    const-wide/16 v30, 0x4

    and-long v30, v30, v20

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-lez v28, :cond_15

    const/4 v8, 0x1

    .line 520
    .local v8, "isItemSharable":Z
    :goto_3
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->isDrm()Z

    move-result v28

    if-eqz v28, :cond_16

    if-eqz v8, :cond_16

    const/4 v7, 0x1

    .line 521
    .local v7, "isDrmNotForwardLock":Z
    :goto_4
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v28

    if-eqz v28, :cond_6

    if-eqz v7, :cond_17

    :cond_6
    if-eqz v8, :cond_17

    const/4 v11, 0x1

    .line 524
    .end local v7    # "isDrmNotForwardLock":Z
    .end local v8    # "isItemSharable":Z
    :cond_7
    :goto_5
    instance-of v0, v13, Lcom/sec/android/gallery3d/remote/picasa/PicasaImage;

    move/from16 v28, v0

    if-eqz v28, :cond_8

    .line 525
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v28

    const/16 v30, 0x4

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_8

    .line 526
    add-int/lit8 v17, v17, 0x1

    .line 529
    :cond_8
    instance-of v0, v13, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    move/from16 v28, v0

    if-eqz v28, :cond_9

    .line 530
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemType()I

    move-result v28

    if-nez v28, :cond_9

    .line 531
    add-int/lit8 v27, v27, 0x1

    .line 535
    :cond_9
    const-wide/high16 v30, 0x1000000000000000L

    and-long v30, v30, v20

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-nez v28, :cond_a

    .line 536
    const/16 v19, 0x0

    .line 539
    :cond_a
    sget-boolean v28, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v28, :cond_b

    .line 540
    if-nez v10, :cond_b

    .line 541
    const-wide/32 v30, 0x40000000

    and-long v30, v30, v20

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-eqz v28, :cond_b

    .line 542
    const/4 v10, 0x1

    .line 547
    :cond_b
    and-long v24, v24, v20

    .line 549
    const-wide v30, 0x100000000000L

    and-long v30, v30, v24

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-eqz v28, :cond_c

    invoke-virtual {v14}, Ljava/util/LinkedList;->size()I

    move-result v28

    const/16 v30, 0x1

    move/from16 v0, v28

    move/from16 v1, v30

    if-ne v0, v1, :cond_d

    .line 551
    :cond_c
    const/16 v22, 0x0

    .line 554
    :cond_d
    const-wide v30, 0x200000000L

    and-long v30, v30, v24

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-eqz v28, :cond_e

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v30

    cmp-long v28, v4, v30

    if-eqz v28, :cond_f

    .line 556
    :cond_e
    const/16 v23, 0x0

    .line 559
    :cond_f
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v28

    if-nez v28, :cond_10

    const-wide/16 v30, 0x10

    move-wide/from16 v0, v30

    invoke-virtual {v13, v0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->hasAttribute(J)Z

    move-result v28

    if-eqz v28, :cond_11

    .line 560
    :cond_10
    const/16 v18, 0x0

    .line 563
    :cond_11
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->getGroupId()J

    move-result-wide v4

    .line 566
    if-nez v3, :cond_12

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v28

    if-eqz v28, :cond_12

    .line 567
    const/4 v3, 0x1

    .line 568
    const-wide v30, 0x5ffffffffffffdd5L    # 2.6815615859883542E154

    and-long v24, v24, v30

    .line 572
    :cond_12
    const-wide/32 v30, 0x2000000

    and-long v30, v30, v24

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-nez v28, :cond_13

    .line 573
    const-wide/32 v30, -0x2000001

    and-long v24, v24, v30

    .line 575
    :cond_13
    const-wide/32 v30, 0x1000000

    and-long v30, v30, v24

    const-wide/16 v32, 0x0

    cmp-long v28, v30, v32

    if-nez v28, :cond_3

    .line 576
    const-wide/32 v30, -0x1000001

    and-long v24, v24, v30

    goto/16 :goto_1

    .line 515
    :cond_14
    const/4 v12, 0x0

    goto/16 :goto_2

    .line 519
    :cond_15
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 520
    .restart local v8    # "isItemSharable":Z
    :cond_16
    const/4 v7, 0x0

    goto/16 :goto_4

    .line 521
    .restart local v7    # "isDrmNotForwardLock":Z
    :cond_17
    const/4 v11, 0x0

    goto/16 :goto_5

    .line 579
    .end local v7    # "isDrmNotForwardLock":Z
    .end local v8    # "isItemSharable":Z
    .end local v13    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v16    # "mediaobj":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v20    # "support":J
    :cond_18
    monitor-exit v29
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 582
    sget-object v28, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_19

    sget-object v28, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    if-eq v0, v1, :cond_19

    .line 583
    const-wide v28, -0x4038000000000001L    # -0.18749999999999997

    and-long v24, v24, v28

    .line 588
    :cond_19
    const/16 v28, 0x1

    move/from16 v0, v28

    if-le v2, v0, :cond_24

    .line 589
    const-wide v28, -0x4000000000000029L    # -1.999999999999991

    and-long v24, v24, v28

    .line 591
    const/16 v28, 0x8

    move/from16 v0, v28

    if-le v2, v0, :cond_1a

    .line 592
    const-wide/16 v28, -0x201

    and-long v24, v24, v28

    .line 595
    :cond_1a
    sget v28, Lcom/sec/samsung/gallery/view/utils/MenuHelper;->SHARE_MAX_ITEM_LIMIT:I

    move/from16 v0, v28

    if-le v2, v0, :cond_1b

    .line 596
    const-wide/32 v28, -0x800001

    and-long v24, v24, v28

    .line 599
    :cond_1b
    sget-boolean v28, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMobilePrint:Z

    if-nez v28, :cond_1c

    .line 600
    const-wide v28, 0x7fffffffffffffffL

    and-long v24, v24, v28

    .line 606
    :cond_1c
    if-nez v22, :cond_1d

    .line 607
    const-wide v28, -0x100000000001L

    and-long v24, v24, v28

    .line 609
    :cond_1d
    if-nez v23, :cond_1e

    .line 610
    const-wide v28, -0x200000001L

    and-long v24, v24, v28

    .line 612
    :cond_1e
    if-eqz v12, :cond_1f

    .line 613
    const-wide v28, 0x400000000000L

    or-long v24, v24, v28

    .line 615
    :cond_1f
    if-eqz v11, :cond_25

    if-gtz v17, :cond_25

    .line 616
    const-wide/16 v28, 0x4

    or-long v24, v24, v28

    .line 617
    if-eqz v18, :cond_20

    .line 618
    const-wide/32 v28, 0x8000000

    or-long v24, v24, v28

    .line 623
    :cond_20
    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    move-object/from16 v28, v0

    invoke-static/range {v28 .. v28}, Lcom/sec/samsung/gallery/controller/SStudioCmd;->getSelectionItemMaxLimit(Landroid/content/Context;)I

    move-result v28

    move/from16 v0, v28

    if-le v2, v0, :cond_21

    .line 624
    const-wide/32 v28, -0x200001

    and-long v24, v24, v28

    .line 627
    :cond_21
    if-lez v27, :cond_22

    .line 628
    const-wide/high16 v28, 0x400000000000000L

    or-long v24, v24, v28

    .line 631
    :cond_22
    if-nez v19, :cond_23

    .line 632
    const-wide v28, -0x1000000000000001L    # -3.1050361846014175E231

    and-long v24, v24, v28

    .line 635
    :cond_23
    if-eqz v10, :cond_0

    if-nez v9, :cond_0

    .line 636
    const-wide/32 v28, 0x40000000

    or-long v24, v24, v28

    .line 637
    const-wide/32 v28, -0x20000001

    and-long v24, v24, v28

    goto/16 :goto_0

    .line 579
    .end local v4    # "groupId":J
    .end local v6    # "i$":Ljava/util/Iterator;
    .end local v14    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :catchall_0
    move-exception v28

    :try_start_1
    monitor-exit v29
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v28

    .line 602
    .restart local v4    # "groupId":J
    .restart local v6    # "i$":Ljava/util/Iterator;
    .restart local v14    # "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    :cond_24
    if-nez v2, :cond_1c

    .line 603
    const-wide/16 v24, 0x0

    goto/16 :goto_0

    .line 620
    :cond_25
    const-wide/16 v28, -0x5

    and-long v24, v24, v28

    goto :goto_6
.end method

.method public hideAlbums(ZZZ)V
    .locals 3
    .param p1, "hide"    # Z
    .param p2, "show"    # Z
    .param p3, "delete"    # Z

    .prologue
    .line 156
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 159
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "HIDE_ALBUMS"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 160
    return-void
.end method

.method public imageEdit()V
    .locals 9

    .prologue
    .line 231
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getImageToEdit()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v1

    .line 232
    .local v1, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v1, :cond_0

    .line 245
    :goto_0
    return-void

    .line 235
    :cond_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    const-string v7, "EDIT"

    iget-object v8, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v6, v7, v8}, Lcom/sec/android/gallery3d/util/GalleryUtils;->insertLog(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;)V

    .line 237
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 238
    .local v5, "selectedItemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    iget-object v6, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v3

    .line 239
    .local v3, "mediaList":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    invoke-virtual {v3}, Ljava/util/LinkedList;->size()I

    move-result v4

    .local v4, "n":I
    :goto_1
    if-ge v0, v4, :cond_1

    .line 240
    invoke-virtual {v3, v0}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 241
    .local v2, "media":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 239
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 244
    .end local v2    # "media":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    const/4 v6, 0x0

    invoke-virtual {v3, v6}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/data/MediaItem;->getMimeType()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->imageEdit(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public imageEdit(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4
    .param p2, "mimeType"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 248
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    aput-object p1, v0, v2

    const/4 v1, 0x2

    aput-object p2, v0, v1

    const/4 v1, 0x3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 251
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "IMAGE_EDIT"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 252
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "EXIT_SELECTION_MODE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 253
    return-void
.end method

.method public isAvailableProceedEditAndCollage(Lcom/sec/android/gallery3d/data/MediaItem;)Z
    .locals 1
    .param p1, "item"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 256
    instance-of v0, p1, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->isBroken()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public makeMotionPicture(Ljava/util/ArrayList;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "burstShotList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 291
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "MAKE_MOTION_PICTURE"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 292
    return-void
.end method

.method public moveToKNOX(ZZ)V
    .locals 3
    .param p1, "move"    # Z
    .param p2, "remove"    # Z

    .prologue
    .line 170
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 173
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "MOVE_TO_KNOX"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 174
    return-void
.end method

.method public moveToSecretbox(ZZ)V
    .locals 3
    .param p1, "hide"    # Z
    .param p2, "show"    # Z

    .prologue
    .line 163
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 166
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "MOVE_TO_SECRETBOX"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 167
    return-void
.end method

.method public printImage()V
    .locals 4

    .prologue
    .line 137
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 138
    .local v0, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 146
    :goto_0
    return-void

    .line 141
    :cond_0
    const/4 v2, 0x1

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v1, v2

    .line 145
    .local v1, "params":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "PRINT_IMAGE"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public rotate(I)V
    .locals 3
    .param p1, "degree"    # I

    .prologue
    .line 177
    sget-boolean v1, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGalleryRotate:Z

    if-nez v1, :cond_0

    .line 184
    :goto_0
    return-void

    .line 180
    :cond_0
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    .line 183
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "ROTATE_FILES"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public showAddUserTagDialog(IZ)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "bShow"    # Z

    .prologue
    .line 843
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 844
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "ADD_USER_TAG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 845
    return-void
.end method

.method public showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZI)V
    .locals 6
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "currentMediaSetIndex"    # I
    .param p3, "show"    # Z
    .param p4, "operationId"    # I

    .prologue
    .line 312
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZILjava/lang/String;)V

    .line 313
    return-void
.end method

.method public showAlbumListDialogToCopyMove(Lcom/sec/android/gallery3d/data/MediaSet;IZILjava/lang/String;)V
    .locals 8
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "currentMediaSetIndex"    # I
    .param p3, "show"    # Z
    .param p4, "operationId"    # I
    .param p5, "topSetPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 297
    if-eqz p3, :cond_0

    .line 298
    invoke-direct {p0, p1, p2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->getMediaSetArray(Lcom/sec/android/gallery3d/data/MediaSet;I)[Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 299
    .local v0, "mediaSetArray":[Lcom/sec/android/gallery3d/data/MediaSet;
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v1, v3

    aput-object v0, v1, v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object p5, v1, v7

    .line 307
    .end local v0    # "mediaSetArray":[Lcom/sec/android/gallery3d/data/MediaSet;
    .local v1, "params":[Ljava/lang/Object;
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SHOW_COPY_MOVE_ALBUM_LIST_DIALOG"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 308
    return-void

    .line 303
    .end local v1    # "params":[Ljava/lang/Object;
    :cond_0
    const/4 v2, 0x5

    new-array v1, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v1, v3

    const/4 v2, 0x0

    aput-object v2, v1, v4

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    aput-object p5, v1, v7

    .restart local v1    # "params":[Ljava/lang/Object;
    goto :goto_0
.end method

.method public showDeleteDialog(ZZ)V
    .locals 4
    .param p1, "isSelectAll"    # Z
    .param p2, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 92
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 95
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_DELETE_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 96
    return-void
.end method

.method public showDetailsDialog(Z)V
    .locals 8
    .param p1, "show"    # Z

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 204
    if-eqz p1, :cond_3

    .line 205
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 207
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-nez v1, :cond_0

    .line 228
    .end local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :goto_0
    return-void

    .line 210
    .restart local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    :cond_0
    const/4 v0, 0x0

    .line 211
    .local v0, "content":Ljava/lang/Object;
    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSCamDetails:Z

    if-eqz v3, :cond_1

    .line 212
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->getMediaForDetails()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 215
    .end local v0    # "content":Ljava/lang/Object;
    :cond_1
    if-nez v0, :cond_2

    .line 216
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->getContentsForDetailsDialog()Ljava/util/ArrayList;

    move-result-object v0

    .line 219
    :cond_2
    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v2, v4

    aput-object v0, v2, v5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    .line 227
    .end local v1    # "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    .local v2, "params":[Ljava/lang/Object;
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "SHOW_DETAILS_DIALOG"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 223
    .end local v2    # "params":[Ljava/lang/Object;
    :cond_3
    new-array v2, v7, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v2, v4

    const/4 v3, 0x0

    aput-object v3, v2, v5

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v6

    .restart local v2    # "params":[Ljava/lang/Object;
    goto :goto_1
.end method

.method public showImageVideoConversionShareDialog(ZZ)V
    .locals 3
    .param p1, "show"    # Z
    .param p2, "type"    # Z

    .prologue
    .line 805
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 808
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_IMAGE_VIDEO_CONVERSION_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 809
    return-void
.end method

.method public showNewAlbumCopyMoveDialog(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "albumPath"    # Ljava/lang/String;
    .param p2, "show"    # Z

    .prologue
    .line 316
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    const/4 v1, 0x2

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 320
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "SHOW_NEW_ALBUM_COPY_MOVE_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 322
    return-void
.end method

.method public showNewAlbumDialog(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 371
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 374
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_NEW_ALBUM_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 375
    return-void
.end method

.method public showPhotoEditorDialog(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 99
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object v3, v0, v1

    const/4 v1, 0x2

    aput-object v3, v0, v1

    const/4 v1, 0x3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 102
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "IMAGE_EDIT"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 103
    return-void
.end method

.method public showRemoveConfirmTagDialog(ZLcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;)V
    .locals 5
    .param p1, "show"    # Z
    .param p2, "action"    # Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    .prologue
    .line 789
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v1

    .line 791
    .local v1, "currentViewState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-nez v1, :cond_0

    .line 792
    sget-object v3, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->TAG:Ljava/lang/String;

    const-string v4, "currentViewState is null!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 802
    :goto_0
    return-void

    .line 795
    :cond_0
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/ActivityState;->getContentsForFaceTag()Ljava/util/ArrayList;

    move-result-object v0

    .line 796
    .local v0, "contentList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    sget-object v3, Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;->CONFIRM_TAG:Lcom/sec/samsung/gallery/controller/StartFaceTagCmd$FaceTagAction;

    if-eq p2, v3, :cond_1

    .line 797
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/sec/android/gallery3d/app/ActivityState;->setFaceTagParam(Landroid/content/Intent;)V

    .line 798
    :cond_1
    const/4 v3, 0x4

    new-array v2, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    aput-object p2, v2, v3

    const/4 v3, 0x3

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 801
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "START_FACE_TAG"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public showRenameDialog(Z)V
    .locals 5
    .param p1, "show"    # Z

    .prologue
    const/4 v4, 0x0

    .line 187
    const/4 v0, 0x0

    .line 188
    .local v0, "media":Lcom/sec/android/gallery3d/data/MediaObject;
    if-eqz p1, :cond_0

    .line 189
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v1

    .line 190
    .local v1, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 191
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 195
    .end local v1    # "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    .restart local v0    # "media":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v0, v2, v3

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    .line 199
    .local v2, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "SHOW_RENAME_DIALOG"

    invoke-virtual {v3, v4, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 200
    return-void
.end method

.method public showSetAsDialog(Lcom/sec/android/gallery3d/data/MediaItem;Z)V
    .locals 6
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "show"    # Z

    .prologue
    .line 330
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    sget-boolean v3, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSecretBox:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    const-string v4, "private_move_do_not_show"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcom/sec/samsung/gallery/util/SharedPreferenceManager;->loadBooleanKey(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    .line 332
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 333
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f030022

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 335
    .local v1, "checkView":Landroid/view/View;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 336
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f0e0064

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e01f2

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f0e00db

    new-instance v5, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;

    invoke-direct {v5, p0, v1}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper$1;-><init>(Lcom/sec/samsung/gallery/view/utils/EditModeHelper;Landroid/view/View;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog;->show()V

    .line 347
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "checkView":Landroid/view/View;
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-virtual {p0, p2}, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->showSetAsDialog(Z)V

    goto :goto_0
.end method

.method public showSetAsDialog(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 350
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 353
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_SET_AS_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 354
    return-void
.end method

.method public showSetAsDialog(ZLjava/util/ArrayList;)V
    .locals 4
    .param p1, "show"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "uris":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    const/4 v3, 0x0

    .line 357
    const/4 v1, 0x4

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    aput-object p2, v0, v1

    .line 360
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_SET_AS_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 361
    return-void
.end method

.method public showShareDialog(Z)V
    .locals 4
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 364
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    .line 367
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "SHOW_SHARE_DIALOG"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 368
    return-void
.end method

.method public startCollageMaker()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 260
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v1, v0, v3

    const/4 v1, 0x1

    sget-object v2, Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;->START_COLLAGE_MAKER:Lcom/sec/samsung/gallery/controller/StartCollageCmd$CollageCmdType;

    aput-object v2, v0, v1

    .line 263
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "START_COLLAGE_APP"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 264
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v2, "EXIT_SELECTION_MODE"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 265
    return-void
.end method

.method public startContactActivity(I)V
    .locals 5
    .param p1, "requestCode"    # I

    .prologue
    .line 767
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.GET_CONTENT"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 768
    .local v1, "intent":Landroid/content/Intent;
    const-string/jumbo v3, "vnd.android.cursor.item/contact"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 770
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v3

    const-string v4, "CscFeature_Contact_ReplacePackageAs"

    invoke-virtual {v3, v4}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 772
    .local v2, "strContactPackageName":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 773
    :cond_0
    const-string v2, "com.android.contacts"

    .line 776
    :cond_1
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 779
    const-string v3, "additional"

    const-string v4, "include-profile"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 781
    :try_start_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3, v1, p1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 786
    :goto_0
    return-void

    .line 782
    :catch_0
    move-exception v0

    .line 783
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    const v4, 0x7f0e0062

    invoke-static {v3, v4}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 784
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public startGifMaker()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 124
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 125
    .local v0, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 134
    :goto_0
    return-void

    .line 128
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 130
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v1, v4

    const/4 v3, 0x1

    aput-object v2, v1, v3

    .line 133
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "START_GIF_MAKER"

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public startMagicShotStudio()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 822
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 823
    .local v0, "list":Ljava/util/LinkedList;, "Ljava/util/LinkedList<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v3

    if-eq v3, v5, :cond_1

    .line 832
    :cond_0
    :goto_0
    return-void

    .line 826
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 827
    .local v2, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/net/Uri;>;"
    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaObject;->getContentUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mContext:Landroid/content/Context;

    aput-object v3, v1, v4

    aput-object v2, v1, v5

    .line 831
    .local v1, "params":[Ljava/lang/Object;
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/utils/EditModeHelper;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v4, "MAGIC_SHOT_STUDIO"

    invoke-virtual {v3, v4, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0
.end method
