.class Lcom/sec/samsung/gallery/glview/GlButtonView$2;
.super Ljava/lang/Object;
.source "GlButtonView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlMovementDetector$GlMoveDetectorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/glview/GlButtonView;->initMoveDetectorListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlButtonView;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 116
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$500(Lcom/sec/samsung/gallery/glview/GlButtonView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v1, v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$502(Lcom/sec/samsung/gallery/glview/GlButtonView;Z)Z

    .line 118
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->playSoundOnClickThumb()V

    .line 119
    const/4 v0, 0x0

    .line 120
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$800(Lcom/sec/samsung/gallery/glview/GlButtonView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mClickListener:Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;
    invoke-static {v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$800(Lcom/sec/samsung/gallery/glview/GlButtonView;)Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/GlButtonView;->mGlObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;->onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 124
    .end local v0    # "handled":Z
    :cond_0
    return v0
.end method

.method public onGenericMotionTouch(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public onLongClick(II)Z
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public onMove(II)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public onPress(II)Z
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 93
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    const/4 v1, 0x1

    # setter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$502(Lcom/sec/samsung/gallery/glview/GlButtonView;Z)Z

    .line 94
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$700(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mPressedDrawableId:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$200(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 95
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$500(Lcom/sec/samsung/gallery/glview/GlButtonView;)Z

    move-result v0

    return v0
.end method

.method public onRelease(IIII)Z
    .locals 5
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "speedX"    # I
    .param p4, "speedY"    # I

    .prologue
    const/4 v4, 0x0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v0}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$500(Lcom/sec/samsung/gallery/glview/GlButtonView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->isPressed:Z
    invoke-static {v0, v4}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$502(Lcom/sec/samsung/gallery/glview/GlButtonView;Z)Z

    .line 86
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlButtonView;->mResMgr:Lcom/sec/android/gallery3d/util/ResourceManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mResources:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$600(Lcom/sec/samsung/gallery/glview/GlButtonView;)Landroid/content/res/Resources;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/GlButtonView$2;->this$0:Lcom/sec/samsung/gallery/glview/GlButtonView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlButtonView;->mNormalDrawableId:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/GlButtonView;->access$000(Lcom/sec/samsung/gallery/glview/GlButtonView;)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/util/ResourceManager;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlButtonView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 88
    :cond_0
    return v4
.end method

.method public onScroll(IIII)Z
    .locals 1
    .param p1, "dx"    # I
    .param p2, "dy"    # I
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method
