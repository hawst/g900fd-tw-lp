.class Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GlBgExpandAnim"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 3854
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/glview/GlPhotoView;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/glview/GlPhotoView$1;

    .prologue
    .line 3854
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;-><init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 8
    .param p1, "ratio"    # F

    .prologue
    .line 3856
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getSourceX()F

    move-result v0

    .line 3857
    .local v0, "sx":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getSourceY()F

    move-result v1

    .line 3858
    .local v1, "sy":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getTargetX()F

    move-result v2

    .line 3859
    .local v2, "tx":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getTargetY()F

    move-result v3

    .line 3860
    .local v3, "ty":F
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v4

    sub-float v5, v2, v0

    mul-float/2addr v5, p1

    add-float/2addr v5, v0

    sub-float v6, v3, v1

    mul-float/2addr v6, p1

    add-float/2addr v6, v1

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFolderBg:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3800(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->getTargetZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgObject;->setPos(FFF)V

    .line 3863
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSourceX()F

    move-result v0

    .line 3864
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSourceY()F

    move-result v1

    .line 3865
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetX()F

    move-result v2

    .line 3866
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetY()F

    move-result v3

    .line 3867
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    sub-float v5, v2, v0

    mul-float/2addr v5, p1

    add-float/2addr v5, v0

    sub-float v6, v3, v1

    mul-float/2addr v6, p1

    add-float/2addr v6, v1

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mGlArrow:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 3870
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseFixScrollingAlbum:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 3871
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getSourceX()F

    move-result v0

    .line 3872
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getSourceY()F

    move-result v1

    .line 3873
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getTargetX()F

    move-result v2

    .line 3874
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getTargetY()F

    move-result v3

    .line 3875
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    sub-float v5, v2, v0

    mul-float/2addr v5, p1

    add-float/2addr v5, v0

    sub-float v6, v3, v1

    mul-float/2addr v6, p1

    add-float/2addr v6, v1

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumObject:Lcom/sec/samsung/gallery/glview/GlSplitObject;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->getTargetZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlSplitObject;->setPos(FFF)V

    .line 3878
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSourceX()F

    move-result v0

    .line 3879
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getSourceY()F

    move-result v1

    .line 3880
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetX()F

    move-result v2

    .line 3881
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetY()F

    move-result v3

    .line 3882
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v4}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v4

    sub-float v5, v2, v0

    mul-float/2addr v5, p1

    add-float/2addr v5, v0

    sub-float v6, v3, v1

    mul-float/2addr v6, p1

    add-float/2addr v6, v1

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlBgExpandAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFixedAlbumSeparator:Lcom/sec/samsung/gallery/glview/GlBaseObject;
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$3600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlBaseObject;

    move-result-object v7

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getTargetZ()F

    move-result v7

    invoke-virtual {v4, v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 3885
    :cond_0
    return-void
.end method
