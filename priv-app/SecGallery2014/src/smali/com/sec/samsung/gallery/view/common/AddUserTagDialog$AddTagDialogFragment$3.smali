.class Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$3;
.super Ljava/lang/Object;
.source "AddUserTagDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$3;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 10
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "spanned"    # Landroid/text/Spanned;
    .param p5, "dstStart"    # I
    .param p6, "dstEnd"    # I

    .prologue
    .line 185
    sub-int v8, p3, p2

    const/16 v9, 0x80

    if-le v8, v9, :cond_0

    .line 186
    add-int/lit16 p3, p2, 0x80

    .line 187
    :cond_0
    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    .line 188
    .local v5, "origTxt":Ljava/lang/String;
    move-object v6, v5

    .line 189
    .local v6, "validTxt":Ljava/lang/String;
    const/4 v2, 0x0

    .line 191
    .local v2, "invalidFlag":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->INVALID_CHAR:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_3

    .line 192
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    .line 193
    .local v7, "validTxtLength":I
    const/4 v3, 0x0

    .local v3, "j":I
    :goto_1
    if-ge v3, v7, :cond_2

    .line 194
    sget-object v8, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog;->INVALID_CHAR:[Ljava/lang/String;

    aget-object v8, v8, v0

    invoke-virtual {v6, v8}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    .line 195
    .local v1, "index":I
    if-ltz v1, :cond_1

    .line 196
    const/4 v2, 0x1

    .line 198
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_1

    .line 201
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x0

    invoke-virtual {v6, v9, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    add-int/lit8 v9, v1, 0x1

    invoke-virtual {v6, v9}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    .line 193
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 191
    .end local v1    # "index":I
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 226
    .end local v3    # "j":I
    .end local v7    # "validTxtLength":I
    :cond_3
    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v8

    sub-int v9, p6, p5

    sub-int/2addr v8, v9

    rsub-int/lit8 v4, v8, 0x40

    .line 227
    .local v4, "keep":I
    if-gtz v4, :cond_4

    .line 231
    :cond_4
    if-eqz v2, :cond_5

    .line 232
    iget-object v8, p0, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment$3;->this$1:Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;

    # getter for: Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->mInvalidToast:Landroid/widget/Toast;
    invoke-static {v8}, Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;->access$500(Lcom/sec/samsung/gallery/view/common/AddUserTagDialog$AddTagDialogFragment;)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 235
    .end local v6    # "validTxt":Ljava/lang/String;
    :goto_2
    return-object v6

    .restart local v6    # "validTxt":Ljava/lang/String;
    :cond_5
    const/4 v6, 0x0

    goto :goto_2
.end method
