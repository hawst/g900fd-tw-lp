.class Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;
.super Ljava/lang/Object;
.source "HiddenMediaAdapter.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateContent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;)V
    .locals 0
    .param p2, "info"    # Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    .prologue
    .line 553
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 554
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    .line 555
    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 550
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 559
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$200(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    .line 560
    .local v2, "oldSize":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    .line 562
    .local v1, "newSize":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$200(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 563
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mCoverData:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$300(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 564
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mData:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$200(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->data:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 565
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    # getter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mCoverData:Ljava/util/List;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$300(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;)Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    iget-object v5, v5, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->covers:Ljava/util/List;

    invoke-interface {v4, v5}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 566
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->mUpdateInfo:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;

    iget-wide v6, v5, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateInfo;->version:J

    # setter for: Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mSourceVersion:J
    invoke-static {v4, v6, v7}, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->access$002(Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;J)J

    .line 568
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v4, :cond_2

    .line 569
    if-eqz v1, :cond_0

    if-eq v2, v1, :cond_1

    .line 570
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v4, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 572
    :cond_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget-object v3, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mReloadTask:Lcom/sec/samsung/gallery/view/adapter/ReloadTask;

    .line 573
    .local v3, "reloadTask":Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget v0, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mThumbnailActiveStart:I

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget v4, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mThumbnailActiveEnd:I

    if-ge v0, v4, :cond_2

    .line 574
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/adapter/ReloadTask;->isActive()Z

    move-result v4

    if-nez v4, :cond_3

    .line 580
    .end local v0    # "i":I
    .end local v3    # "reloadTask":Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
    :cond_2
    const/4 v4, 0x0

    return-object v4

    .line 576
    .restart local v0    # "i":I
    .restart local v3    # "reloadTask":Lcom/sec/samsung/gallery/view/adapter/ReloadTask;
    :cond_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/adapter/HiddenMediaAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v4, v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onWindowContentChanged(I)V

    .line 573
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
