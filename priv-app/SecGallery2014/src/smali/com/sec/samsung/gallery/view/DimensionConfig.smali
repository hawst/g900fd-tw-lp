.class public Lcom/sec/samsung/gallery/view/DimensionConfig;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/view/DimensionConfig$QuickScroll;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewVI;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewVI;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$EventViewDimension;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$DetailViewDimension;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$ThumbnailViewDimension;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumViewDimension;,
        Lcom/sec/samsung/gallery/view/DimensionConfig$ScreenDimension;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 694
    return-void
.end method

.method static synthetic access$000(Landroid/content/res/Resources;II)I
    .locals 1
    .param p0, "x0"    # Landroid/content/res/Resources;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 10
    invoke-static {p0, p1, p2}, Lcom/sec/samsung/gallery/view/DimensionConfig;->getIntFromTypeArray(Landroid/content/res/Resources;II)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Landroid/content/res/Resources;III)[I
    .locals 1
    .param p0, "x0"    # Landroid/content/res/Resources;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 10
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/DimensionConfig;->getFullArraryFromTypeArray(Landroid/content/res/Resources;III)[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Landroid/content/res/Resources;III)[[I
    .locals 1
    .param p0, "x0"    # Landroid/content/res/Resources;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 10
    invoke-static {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/view/DimensionConfig;->get2DFullArraryFromTypeArray(Landroid/content/res/Resources;III)[[I

    move-result-object v0

    return-object v0
.end method

.method private static get2DFullArraryFromTypeArray(Landroid/content/res/Resources;III)[[I
    .locals 10
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "land"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v8, -0x1

    .line 656
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v6

    .line 657
    .local v6, "typedArray":Landroid/content/res/TypedArray;
    new-array v5, p3, [[I

    .line 658
    .local v5, "tempArray":[[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_1

    .line 659
    sget-object v7, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v7, v7, v1

    mul-int/lit8 v7, v7, 0x2

    new-array v7, v7, [I

    aput-object v7, v5, v1

    .line 660
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    sget-object v7, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v7, v7, v1

    if-ge v2, v7, :cond_0

    .line 661
    aget-object v7, v5, v1

    aput v8, v7, v2

    .line 660
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 658
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 664
    .end local v2    # "j":I
    :cond_1
    const/4 v4, 0x0

    .line 666
    .local v4, "startIndex":I
    if-nez p2, :cond_2

    .line 667
    const/4 v4, 0x0

    .line 679
    :goto_2
    const/4 v1, 0x0

    :goto_3
    if-ge v1, p3, :cond_6

    .line 680
    :try_start_0
    sget-object v7, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v7, v7, v1

    mul-int/lit8 v3, v7, 0x2

    .line 681
    .local v3, "modeCount":I
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_4
    if-ge v2, v3, :cond_5

    .line 682
    aget-object v7, v5, v1

    add-int v8, v4, v2

    const/4 v9, -0x1

    invoke-virtual {v6, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v8

    aput v8, v7, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 668
    .end local v2    # "j":I
    .end local v3    # "modeCount":I
    :cond_2
    const/4 v7, 0x1

    if-ne p2, v7, :cond_4

    .line 669
    const/4 v1, 0x0

    :goto_5
    if-ge v1, p3, :cond_3

    .line 670
    sget-object v7, Lcom/sec/samsung/gallery/view/DimensionConfig$AlbumTimeDimension;->MODE_CHILD_COUNT:[I

    aget v7, v7, v1

    add-int/2addr v4, v7

    .line 669
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 672
    :cond_3
    mul-int/lit8 v4, v4, 0x2

    goto :goto_2

    .line 674
    :cond_4
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    .line 691
    :goto_6
    return-object v5

    .line 684
    .restart local v2    # "j":I
    .restart local v3    # "modeCount":I
    :cond_5
    add-int/2addr v4, v3

    .line 679
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 689
    .end local v2    # "j":I
    .end local v3    # "modeCount":I
    :cond_6
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_6

    .line 686
    :catch_0
    move-exception v0

    .line 687
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 689
    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_6

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v7

    invoke-virtual {v6}, Landroid/content/res/TypedArray;->recycle()V

    throw v7
.end method

.method private static getFullArraryFromTypeArray(Landroid/content/res/Resources;III)[I
    .locals 7
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "land"    # I
    .param p3, "count"    # I

    .prologue
    const/4 v5, -0x1

    .line 629
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 630
    .local v4, "typedArray":Landroid/content/res/TypedArray;
    new-array v3, p3, [I

    .line 631
    .local v3, "tempArray":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_0

    aput v5, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 632
    :cond_0
    const/4 v2, 0x0

    .line 634
    .local v2, "startIndex":I
    if-nez p2, :cond_1

    .line 635
    const/4 v2, 0x0

    .line 644
    :goto_1
    const/4 v1, 0x0

    :goto_2
    if-ge v1, p3, :cond_3

    .line 645
    add-int v5, v2, v1

    const/4 v6, -0x1

    :try_start_0
    invoke-virtual {v4, v5, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v5

    aput v5, v3, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 644
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 636
    :cond_1
    const/4 v5, 0x1

    if-ne p2, v5, :cond_2

    .line 637
    move v2, p3

    goto :goto_1

    .line 639
    :cond_2
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 652
    :goto_3
    return-object v3

    .line 650
    :cond_3
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_3

    .line 647
    :catch_0
    move-exception v0

    .line 648
    .local v0, "e":Ljava/lang/Exception;
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 650
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_3

    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    throw v5
.end method

.method private static getIntFromTypeArray(Landroid/content/res/Resources;II)I
    .locals 3
    .param p0, "r"    # Landroid/content/res/Resources;
    .param p1, "id"    # I
    .param p2, "index"    # I

    .prologue
    .line 622
    invoke-virtual {p0, p1}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 623
    .local v0, "typedArray":Landroid/content/res/TypedArray;
    const/4 v2, 0x0

    invoke-virtual {v0, p2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    .line 624
    .local v1, "value":I
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 625
    return v1
.end method
