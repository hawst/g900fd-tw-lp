.class Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;
.super Landroid/os/AsyncTask;
.source "StartCategoryTagCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateCategoryTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final CHANGE_CATEGORY:I = 0x2

.field private static final REMOVE_FROM_CATEGORY:I = 0x1


# instance fields
.field private action:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

.field private clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private editType:Ljava/lang/String;

.field private final mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

.field private mediaSets:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;"
        }
    .end annotation
.end field

.field private newType:Ljava/lang/String;

.field private parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private removeType:Ljava/lang/String;

.field private selectedPaths:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

.field private updateCategoryType:I

.field private uriList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;)V
    .locals 1
    .param p2, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p3, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p7, "removeType"    # Ljava/lang/String;
    .param p8, "action"    # Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;",
            ")V"
        }
    .end annotation

    .prologue
    .line 354
    .local p4, "mediaSets":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaSet;>;"
    .local p5, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local p6, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 337
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 355
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->updateCategoryType:I

    .line 356
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 357
    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 358
    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->mediaSets:Ljava/util/ArrayList;

    .line 359
    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->selectedPaths:Ljava/util/ArrayList;

    .line 360
    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->uriList:Ljava/util/ArrayList;

    .line 361
    iput-object p7, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->removeType:Ljava/lang/String;

    .line 362
    iput-object p8, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->action:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    .line 363
    return-void
.end method

.method public constructor <init>(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 1
    .param p2, "editType"    # Ljava/lang/String;
    .param p3, "newType"    # Ljava/lang/String;
    .param p4, "clusterAlbumSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p5, "parentMediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/gallery3d/data/Path;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 365
    .local p6, "selectedPaths":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/Path;>;"
    .local p7, "uriList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 337
    new-instance v0, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    .line 366
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->updateCategoryType:I

    .line 367
    iput-object p2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->editType:Ljava/lang/String;

    .line 368
    iput-object p3, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->newType:Ljava/lang/String;

    .line 369
    iput-object p4, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 370
    iput-object p5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 371
    iput-object p6, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->selectedPaths:Ljava/util/ArrayList;

    .line 372
    iput-object p7, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->uriList:Ljava/util/ArrayList;

    .line 373
    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 4
    .param p1, "arg0"    # [Ljava/lang/Void;

    .prologue
    const/4 v2, 0x0

    .line 384
    iget v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->updateCategoryType:I

    packed-switch v0, :pswitch_data_0

    .line 404
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    .line 386
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbumSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->removeType:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->deleteCategoryType(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->uriList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->removeType:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->deleteCategoryTypeArray(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 391
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/CategoryAlbum;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_2

    .line 392
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->uriList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->removeType:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->deleteCategoryTypeArray(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 394
    :cond_2
    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "remove from category clusterAlbumSet cast error"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 395
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 399
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->uriList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->editType:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->newType:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/access/dcm/DCMInterface;->chanageCategoryTypeArray(Landroid/content/Context;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 384
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 335
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 7
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x3

    .line 409
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 410
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->closeProgressDialog()V

    .line 412
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 445
    :goto_0
    return-void

    .line 415
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 416
    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error : remove from category failed by DCM !!!"

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 420
    :cond_1
    iget v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->updateCategoryType:I

    packed-switch v0, :pswitch_data_0

    .line 444
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->requestUpdateScreenByCategoryChange(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    goto :goto_0

    .line 422
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_2

    .line 423
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->selectedPaths:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 425
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v6

    .line 426
    .local v6, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/sec/android/gallery3d/app/StateManager;->setReloadRequiredOnResume(Z)V

    .line 428
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->action:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_DETAIL_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->action:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    sget-object v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;->REMOVE_PEOPLE_TAG:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$CategoryAction;

    if-ne v0, v1, :cond_2

    .line 429
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/sec/android/gallery3d/app/GalleryApp;->setRefreshOperation(I)V

    goto :goto_1

    .line 435
    .end local v6    # "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    :pswitch_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    instance-of v0, v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v0, :cond_2

    .line 436
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->clusterAlbumSet:Lcom/sec/android/gallery3d/data/MediaSet;

    check-cast v0, Lcom/sec/android/gallery3d/data/ClusterAlbum;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->selectedPaths:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->parentMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/gallery3d/data/ClusterAlbum;->updateOperation(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/util/ArrayList;ILjava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;)V

    .line 437
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    # getter for: Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v0}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->access$000(Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAndroidContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0e048d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;II)V

    goto :goto_1

    .line 420
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 335
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 377
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 378
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->mProgressDialogHelper:Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    iget-object v1, v1, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd$UpdateCategoryTask;->this$0:Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;

    iget-object v3, v3, Lcom/sec/samsung/gallery/controller/StartCategoryTagCmd;->mContext:Landroid/content/Context;

    const v5, 0x7f0e00ca

    invoke-virtual {v3, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    move v5, v4

    move-object v6, v2

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/view/common/ProgressDialogHelper;->showProgressDialog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLandroid/content/DialogInterface$OnCancelListener;)V

    .line 380
    return-void
.end method
