.class Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;
.super Landroid/widget/ArrayAdapter;
.source "AlbumTabBar.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->initTabForPhone()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field final synthetic this$1:Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

.field final synthetic val$options:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$3;Landroid/content/Context;II[Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p2, "x0"    # Landroid/content/Context;
    .param p3, "x1"    # I
    .param p4, "x2"    # I
    .param p5, "x3"    # [Ljava/lang/String;

    .prologue
    .line 197
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->this$1:Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

    iput-object p6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->val$options:[Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 198
    new-instance v0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1$1;-><init>(Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 215
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 216
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 217
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->this$1:Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mMainActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v3

    if-ne p1, v3, :cond_1

    const v1, 0x7f0e02b2

    .line 219
    .local v1, "resId":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->this$1:Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    iget-object v3, v3, Lcom/sec/samsung/gallery/view/AlbumTabBar;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->val$options:[Ljava/lang/String;

    aget-object v6, v6, p1

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 221
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 223
    .end local v0    # "description":Ljava/lang/String;
    .end local v1    # "resId":I
    :cond_0
    return-object v2

    .line 217
    :cond_1
    const v1, 0x7f0e02b3

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x0

    .line 228
    const/4 v3, 0x0

    .line 230
    .local v3, "v":Landroid/view/View;
    if-nez p2, :cond_2

    .line 231
    :try_start_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 240
    :goto_0
    if-eqz p3, :cond_1

    if-eqz v3, :cond_1

    .line 241
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 242
    .local v0, "childParams":Landroid/view/ViewGroup$LayoutParams;
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    .line 243
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v4, 0x10

    iput v4, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 244
    invoke-virtual {v3, v5}, Landroid/view/View;->setFocusable(Z)V

    .line 245
    if-eqz v2, :cond_0

    .line 246
    invoke-virtual {p3, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 248
    :cond_0
    const/4 v4, 0x0

    invoke-virtual {p3, v4}, Landroid/view/ViewGroup;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 249
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->mOnGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {p3, v4}, Landroid/view/ViewGroup;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 251
    .end local v0    # "childParams":Landroid/view/ViewGroup$LayoutParams;
    .end local v2    # "params":Landroid/widget/LinearLayout$LayoutParams;
    :cond_1
    return-object v3

    .line 233
    :cond_2
    move-object v3, p2

    goto :goto_0

    .line 234
    :catch_0
    move-exception v1

    .line 235
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    .line 236
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/AlbumTabBar$3$1;->this$1:Lcom/sec/samsung/gallery/view/AlbumTabBar$3;

    iget-object v4, v4, Lcom/sec/samsung/gallery/view/AlbumTabBar$3;->this$0:Lcom/sec/samsung/gallery/view/AlbumTabBar;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/view/AlbumTabBar;->onTabChanged()V

    .line 237
    invoke-super {p0, v5, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_0
.end method
