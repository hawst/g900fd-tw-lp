.class Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;
.super Ljava/lang/Object;
.source "HelpListScrollingState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/view/help/HelpScrollView$OnScrollChanged;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->setViewListScrolling()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

.field final synthetic val$scroll:Lcom/sec/samsung/gallery/view/help/HelpScrollView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;Lcom/sec/samsung/gallery/view/help/HelpScrollView;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iput-object p2, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->val$scroll:Lcom/sec/samsung/gallery/view/help/HelpScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollChanged(II)V
    .locals 8
    .param p1, "l"    # I
    .param p2, "t"    # I

    .prologue
    const-wide/16 v6, 0xbb8

    const/4 v5, 0x0

    const/4 v4, 0x4

    .line 121
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->val$scroll:Lcom/sec/samsung/gallery/view/help/HelpScrollView;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->getMaxScroll()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    if-lt p2, v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->IsShowCompletePopup:Z
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->access$300(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 122
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    const/4 v2, 0x1

    # setter for: Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->IsShowCompletePopup:Z
    invoke-static {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->access$302(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;Z)Z

    .line 123
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->val$scroll:Lcom/sec/samsung/gallery/view/help/HelpScrollView;

    invoke-virtual {v1, v5}, Lcom/sec/samsung/gallery/view/help/HelpScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 124
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mTap:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 125
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 127
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->access$400(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v1

    const v2, 0x7f0f013c

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Landroid/widget/TextView;

    .line 130
    .local v0, "completePopup":Landroid/widget/TextView;
    const v1, 0x7f0e02dc

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 131
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    # getter for: Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    invoke-static {v2}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->access$500(Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    move-result-object v2

    const v3, 0x7f040002

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mBubbleAnimation:Landroid/view/animation/Animation;

    .line 133
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mBubbleAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    .line 134
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 136
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->clearAnimation()V

    .line 137
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    iget-object v1, v1, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->mHelpPopup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    .line 139
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState$4;->this$0:Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;

    invoke-virtual {v1, v6, v7, v6, v7}, Lcom/sec/samsung/gallery/view/help/HelpListScrollingState;->startFinishActivity(JJ)V

    .line 141
    .end local v0    # "completePopup":Landroid/widget/TextView;
    :cond_0
    return-void
.end method
