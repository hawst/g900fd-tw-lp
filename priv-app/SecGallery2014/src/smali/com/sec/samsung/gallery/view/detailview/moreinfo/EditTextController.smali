.class public Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;
.super Ljava/lang/Object;
.source "EditTextController.java"


# static fields
.field public static final INVALID_CHAR:[Ljava/lang/String;


# instance fields
.field private final MAX_INPUT_LENGTH:I

.field private final mContext:Landroid/content/Context;

.field private final mEditText:Landroid/widget/EditText;

.field private mInvalidToast:Landroid/widget/Toast;

.field private mLengthFilter:Landroid/text/InputFilter$LengthFilter;

.field private mLongTextToast:Landroid/widget/Toast;

.field private mTextCheckFilter:Landroid/text/InputFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 13
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "\\"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "/"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ":"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "*"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "?"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "\""

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "<"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, ">"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "|"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "\n"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->INVALID_CHAR:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/EditText;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "editText"    # Landroid/widget/EditText;

    .prologue
    const/16 v1, 0x40

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput v1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->MAX_INPUT_LENGTH:I

    .line 31
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$1;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mTextCheckFilter:Landroid/text/InputFilter;

    .line 79
    new-instance v0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController$2;-><init>(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;I)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLengthFilter:Landroid/text/InputFilter$LengthFilter;

    .line 24
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mContext:Landroid/content/Context;

    .line 25
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mEditText:Landroid/widget/EditText;

    .line 26
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mEditText:Landroid/widget/EditText;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mTextCheckFilter:Landroid/text/InputFilter;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLengthFilter:Landroid/text/InputFilter$LengthFilter;

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mInvalidToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mInvalidToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLongTextToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$202(Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 12
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/moreinfo/EditTextController;->mLongTextToast:Landroid/widget/Toast;

    return-object p1
.end method
