.class Lcom/sec/samsung/gallery/glview/GlPhotoView$15;
.super Ljava/lang/Object;
.source "GlPhotoView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlLongClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 2042
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onLongClick(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 11
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v10, 0x4

    const v8, 0x7f0e0107

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2044
    move-object v5, p1

    check-cast v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v1, v5, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 2046
    .local v1, "index":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mRClickable:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1600(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v5

    if-nez v5, :cond_0

    move v5, v6

    .line 2096
    :goto_0
    return v5

    .line 2048
    :cond_0
    const/4 v0, 0x0

    .line 2049
    .local v0, "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    instance-of v5, v5, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    if-eqz v5, :cond_7

    .line 2050
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v0, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    .end local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    check-cast v0, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;

    .line 2051
    .restart local v0    # "editAdapter":Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;
    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/EditMediaItemAdapter;->getItem(I)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v2

    .line 2052
    .local v2, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v2, :cond_1

    move v5, v7

    .line 2053
    goto :goto_0

    .line 2056
    :cond_1
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalImage;

    if-eqz v5, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v5

    if-eqz v5, :cond_3

    :cond_2
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFilteredItems:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1800()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 2057
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v5, v7

    .line 2058
    goto :goto_0

    .line 2061
    :cond_3
    instance-of v5, v2, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v5, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v5

    if-eqz v5, :cond_5

    :cond_4
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForVideoAlbum:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1900()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 2062
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v5, v7

    .line 2063
    goto :goto_0

    .line 2066
    :cond_5
    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v5

    if-eq v5, v10, :cond_6

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/data/MediaItem;->isGolf()Z

    move-result v5

    if-eqz v5, :cond_7

    :cond_6
    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->misFilteredForStoryAlbum:Z
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2000()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 2067
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v5, v8}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    move v5, v7

    .line 2068
    goto :goto_0

    .line 2072
    .end local v2    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_7
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    if-eqz v5, :cond_8

    .line 2073
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mOnItemLongClickListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v9

    invoke-interface {v5, v8, v9, v1}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnItemLongClickListener;->onItemLongClick(Lcom/sec/samsung/gallery/glview/GlAbsListView;Lcom/sec/android/gallery3d/glcore/GlView;I)V

    .line 2075
    :cond_8
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-virtual {v5, v10, v1, v7, v7}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->setCommand(IIII)V

    .line 2076
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsDragEnabled:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2200(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mResult:I

    const/4 v8, 0x2

    if-ne v5, v8, :cond_b

    .line 2077
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-boolean v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mIsExpanded:Z

    if-eqz v5, :cond_9

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragNDropInMultiwindow:Z

    if-eqz v5, :cond_b

    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMultiWindow:Z

    if-eqz v5, :cond_b

    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v5

    if-eqz v5, :cond_b

    .line 2078
    :cond_9
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_a

    .line 2079
    const/4 v4, 0x0

    .line 2080
    .local v4, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mNewAlbumMode:Z
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2300(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 2081
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    .line 2085
    :goto_1
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getTotalSelectedItems()I

    move-result v3

    .line 2086
    .local v3, "numberOfSelectedItems":I
    if-ne v3, v6, :cond_d

    .line 2087
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v8, 0x7f0e0294

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-virtual {v5, v8, v9}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    .line 2092
    .end local v3    # "numberOfSelectedItems":I
    .end local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_a
    :goto_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->clearBoundarEffect()V
    invoke-static {v5}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$900(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V

    .line 2093
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlPhotoView;->startDragAnim(Lcom/sec/android/gallery3d/glcore/GlObject;II)V
    invoke-static {v5, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$2400(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/android/gallery3d/glcore/GlObject;II)V

    :cond_b
    move v5, v6

    .line 2096
    goto/16 :goto_0

    .line 2083
    .restart local v4    # "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    :cond_c
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$15;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getNewAlbumSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v4

    goto :goto_1

    .line 2089
    .restart local v3    # "numberOfSelectedItems":I
    :cond_d
    invoke-static {}, Lcom/sec/samsung/gallery/util/TTSUtil;->getInstance()Lcom/sec/samsung/gallery/util/TTSUtil;

    move-result-object v5

    const v8, 0x7f0e0295

    new-array v9, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v9, v7

    invoke-virtual {v5, v8, v9}, Lcom/sec/samsung/gallery/util/TTSUtil;->speak(I[Ljava/lang/Object;)V

    goto :goto_2
.end method
