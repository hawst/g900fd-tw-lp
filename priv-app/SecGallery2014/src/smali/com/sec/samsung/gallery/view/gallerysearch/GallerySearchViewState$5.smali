.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;
.super Ljava/lang/Object;
.source "GallerySearchViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V
    .locals 7
    .param p1, "view"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 368
    packed-switch p2, :pswitch_data_0

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 370
    :pswitch_0
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 371
    .local v1, "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 372
    .local v2, "lastIndex":I
    add-int v3, v1, v2

    div-int/lit8 v0, v3, 0x2

    .line 373
    .local v0, "bgIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$2000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I

    move-result v3

    if-eq v3, v0, :cond_0

    .line 374
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # setter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I
    invoke-static {v3, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$2002(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;I)I

    .line 375
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mSearchView:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$1700(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mGallerySearchItemAdapter:Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mBgImageIndex:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$2000(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)I

    move-result v5

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getItemImage(II)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setNextImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 380
    .end local v0    # "bgIndex":I
    .end local v1    # "firstIndex":I
    .end local v2    # "lastIndex":I
    :pswitch_1
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getFirstVisiblePosition()I

    move-result v1

    .line 381
    .restart local v1    # "firstIndex":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->getLastVisiblePosition()I

    move-result v2

    .line 382
    .restart local v2    # "lastIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState$5;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;

    # getter for: Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->mPhotoThumbnailProxy:Lcom/sec/samsung/gallery/model/ThumbnailProxy;
    invoke-static {v3}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;->access$2100(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchViewState;)Lcom/sec/samsung/gallery/model/ThumbnailProxy;

    move-result-object v3

    invoke-virtual {v3, v1, v2}, Lcom/sec/samsung/gallery/model/ThumbnailProxy;->setActiveWindow(II)V

    goto :goto_0

    .line 368
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
