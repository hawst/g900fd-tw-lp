.class Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;
.super Ljava/lang/Object;
.source "LargeImage.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/GLRoot$OnGLIdleListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/decoder/LargeImage;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LargeImageTileUploader"
.end annotation


# instance fields
.field mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/decoder/LargeImage;)V
    .locals 2

    .prologue
    .line 413
    iput-object p1, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 414
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/decoder/LargeImage;Lcom/sec/samsung/gallery/decoder/LargeImage$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/decoder/LargeImage;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/decoder/LargeImage$1;

    .prologue
    .line 413
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;-><init>(Lcom/sec/samsung/gallery/decoder/LargeImage;)V

    return-void
.end method


# virtual methods
.method public onGLIdle(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;Z)Z
    .locals 8
    .param p1, "canvas"    # Lcom/sec/android/gallery3d/glrenderer/GLCanvas;
    .param p2, "renderRequested"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 421
    if-nez p1, :cond_0

    .line 461
    :goto_0
    return v4

    .line 424
    :cond_0
    const/16 v1, 0x32

    .line 425
    .local v1, "quota":I
    const/4 v2, 0x0

    .line 426
    .local v2, "tile":Lcom/sec/samsung/gallery/decoder/LargeImageTile;
    :goto_1
    if-lez v1, :cond_1

    .line 427
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$1200(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    move-result-object v5

    monitor-enter v5

    .line 428
    :try_start_0
    iget-object v6, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;
    invoke-static {v6}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$1200(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    move-result-object v6

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->pop()Lcom/sec/samsung/gallery/decoder/LargeImageTile;

    move-result-object v2

    .line 429
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 430
    if-nez v2, :cond_4

    .line 451
    :cond_1
    :goto_2
    if-nez v2, :cond_2

    .line 452
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->mActive:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 453
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$500(Lcom/sec/samsung/gallery/decoder/LargeImage;)I

    move-result v5

    if-nez v5, :cond_3

    .line 454
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadQueue:Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$1200(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileQueue;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 455
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    iput-boolean v3, v5, Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadingComplete:Z

    .line 456
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$1300(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 457
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # getter for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$1300(Lcom/sec/samsung/gallery/decoder/LargeImage;)Lcom/sec/android/gallery3d/ui/PhotoView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/ui/PhotoView;->invalidate()V

    .line 461
    :cond_3
    if-eqz v2, :cond_9

    :goto_3
    move v4, v3

    goto :goto_0

    .line 429
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 432
    :cond_4
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->isContentValid()Z

    move-result v5

    if-nez v5, :cond_6

    .line 433
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    iget-object v6, v5, Lcom/sec/samsung/gallery/decoder/LargeImage;->mDecodeLock:Ljava/lang/Object;

    monitor-enter v6

    .line 434
    :try_start_2
    iget v5, v2, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    const/4 v7, 0x4

    if-eq v5, v7, :cond_8

    .line 435
    invoke-virtual {v2}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->isLoaded()Z

    move-result v0

    .line 436
    .local v0, "hasBeenLoaded":Z
    iget v5, v2, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->mTileState:I

    const/16 v7, 0x8

    if-ne v5, v7, :cond_7

    move v5, v3

    :goto_4
    invoke-static {v5}, Lcom/sec/android/gallery3d/common/Utils;->assertTrue(Z)V

    .line 437
    invoke-virtual {v2, p1}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->updateContent(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;)V

    .line 438
    if-nez v0, :cond_5

    .line 439
    const/4 v5, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, p1, v5, v7}, Lcom/sec/samsung/gallery/decoder/LargeImageTile;->draw(Lcom/sec/android/gallery3d/glrenderer/GLCanvas;II)V

    .line 446
    :cond_5
    add-int/lit8 v1, v1, -0x1

    .line 447
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 449
    .end local v0    # "hasBeenLoaded":Z
    :cond_6
    iget-object v5, p0, Lcom/sec/samsung/gallery/decoder/LargeImage$LargeImageTileUploader;->this$0:Lcom/sec/samsung/gallery/decoder/LargeImage;

    # operator-- for: Lcom/sec/samsung/gallery/decoder/LargeImage;->mUploadTileCount:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/decoder/LargeImage;->access$510(Lcom/sec/samsung/gallery/decoder/LargeImage;)I

    goto :goto_1

    .restart local v0    # "hasBeenLoaded":Z
    :cond_7
    move v5, v4

    .line 436
    goto :goto_4

    .line 442
    .end local v0    # "hasBeenLoaded":Z
    :cond_8
    :try_start_3
    const-string v5, "TEST"

    const-string v7, "Tile is already recycled"

    invoke-static {v5, v7}, Lcom/sec/android/gallery3d/ui/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const/4 v2, 0x0

    .line 444
    monitor-exit v6

    goto :goto_2

    .line 447
    :catchall_1
    move-exception v3

    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v3

    :cond_9
    move v3, v4

    .line 461
    goto :goto_3
.end method
