.class public Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;
.super Ljava/lang/Object;
.source "GlPenSelectController.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;,
        Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "GlPenSelectController"

.field private static final TYPE_ALBUMVIEW:I = 0x1

.field private static final TYPE_EVENTVIEW:I = 0x3

.field private static final TYPE_PHOTOVIEW:I = 0x2

.field private static final TYPE_TIMEVIEW:I


# instance fields
.field private mBaseAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

.field private mBottomStartAlbumIndex:I

.field private mBottomStartItemIndex:I

.field private mComposeBaseAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

.field private mContext:Landroid/content/Context;

.field private mDirection:I

.field private mEnd:Landroid/graphics/PointF;

.field private mEndAlbumIndex:I

.field private mEndItemIndex:I

.field private mIndexList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

.field private mLayerType:I

.field private mLeftStartAlbumIndex:I

.field private mLeftStartItemIndex:I

.field private mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

.field private mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

.field private mRightStartAlbumIndex:I

.field private mRightStartItemIndex:I

.field private mSelectedObjList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;",
            ">;"
        }
    .end annotation
.end field

.field private mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

.field private mStart:Landroid/graphics/PointF;

.field private mStartAlbumIndex:I

.field private mStartItemIndex:I

.field private mTopStartAlbumIndex:I

.field private mTopStartItemIndex:I

.field private mVibrator:Landroid/os/Vibrator;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/sec/android/gallery3d/glcore/GlLayer;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layer"    # Lcom/sec/android/gallery3d/glcore/GlLayer;

    .prologue
    const/4 v0, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 37
    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    .line 67
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mContext:Landroid/content/Context;

    .line 68
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    .line 69
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    .line 71
    instance-of v0, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeAlbumView;

    if-eqz v0, :cond_1

    .line 72
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    .line 78
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->initAttribute()V

    .line 79
    return-void

    .line 73
    :cond_1
    instance-of v0, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposePhotoView;

    if-eqz v0, :cond_2

    .line 74
    const/4 v0, 0x2

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    goto :goto_0

    .line 75
    :cond_2
    instance-of v0, p2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeEventView;

    if-eqz v0, :cond_0

    .line 76
    const/4 v0, 0x3

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    goto :goto_0
.end method

.method private changeObjPositionInAlbumView()V
    .locals 7

    .prologue
    .line 908
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v0

    .line 913
    .local v0, "columnCount":I
    if-gtz v0, :cond_1

    .line 944
    :cond_0
    :goto_0
    return-void

    .line 916
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    if-gez v5, :cond_2

    .line 917
    const/4 v4, -0x1

    .line 918
    .local v4, "startQuotient":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    mul-int v6, v0, v4

    sub-int v3, v5, v6

    .line 923
    .local v3, "start":I
    :goto_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    if-gez v5, :cond_3

    .line 924
    const/4 v2, -0x1

    .line 925
    .local v2, "endQuotient":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    mul-int v6, v0, v2

    sub-int v1, v5, v6

    .line 931
    .local v1, "end":I
    :goto_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 932
    mul-int v5, v0, v2

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 933
    mul-int v5, v0, v4

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    goto :goto_0

    .line 920
    .end local v1    # "end":I
    .end local v2    # "endQuotient":I
    .end local v3    # "start":I
    .end local v4    # "startQuotient":I
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    rem-int v3, v5, v0

    .line 921
    .restart local v3    # "start":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    div-int v4, v5, v0

    .restart local v4    # "startQuotient":I
    goto :goto_1

    .line 927
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    rem-int v1, v5, v0

    .line 928
    .restart local v1    # "end":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    div-int v2, v5, v0

    .restart local v2    # "endQuotient":I
    goto :goto_2

    .line 934
    :cond_4
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 935
    mul-int v5, v0, v2

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 936
    mul-int v5, v0, v4

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    goto :goto_0

    .line 937
    :cond_5
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_6

    .line 938
    mul-int v5, v0, v4

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 939
    mul-int v5, v0, v2

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    goto :goto_0

    .line 940
    :cond_6
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_0

    .line 941
    mul-int v5, v0, v4

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 942
    mul-int v5, v0, v2

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    goto :goto_0
.end method

.method private changeObjPositionInPhotoView()V
    .locals 7

    .prologue
    .line 1156
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v0

    .line 1161
    .local v0, "columnCount":I
    if-gtz v0, :cond_1

    .line 1192
    :cond_0
    :goto_0
    return-void

    .line 1164
    :cond_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    if-gez v5, :cond_2

    .line 1165
    const/4 v4, -0x1

    .line 1166
    .local v4, "startQuotient":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    mul-int v6, v0, v4

    sub-int v3, v5, v6

    .line 1171
    .local v3, "start":I
    :goto_1
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    if-gez v5, :cond_3

    .line 1172
    const/4 v2, -0x1

    .line 1173
    .local v2, "endQuotient":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    mul-int v6, v0, v2

    sub-int v1, v5, v6

    .line 1179
    .local v1, "end":I
    :goto_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_4

    .line 1180
    mul-int v5, v0, v2

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 1181
    mul-int v5, v0, v4

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 1168
    .end local v1    # "end":I
    .end local v2    # "endQuotient":I
    .end local v3    # "start":I
    .end local v4    # "startQuotient":I
    :cond_2
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    rem-int v3, v5, v0

    .line 1169
    .restart local v3    # "start":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    div-int v4, v5, v0

    .restart local v4    # "startQuotient":I
    goto :goto_1

    .line 1175
    :cond_3
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    rem-int v1, v5, v0

    .line 1176
    .restart local v1    # "end":I
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    div-int v2, v5, v0

    .restart local v2    # "endQuotient":I
    goto :goto_2

    .line 1182
    :cond_4
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    .line 1183
    mul-int v5, v0, v2

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 1184
    mul-int v5, v0, v4

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 1185
    :cond_5
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_6

    .line 1186
    mul-int v5, v0, v4

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 1187
    mul-int v5, v0, v2

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 1188
    :cond_6
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v6, 0x4

    if-ne v5, v6, :cond_0

    .line 1189
    mul-int v5, v0, v4

    add-int/2addr v5, v3

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 1190
    mul-int v5, v0, v2

    add-int/2addr v5, v1

    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0
.end method

.method private checkDirection()I
    .locals 4

    .prologue
    .line 165
    const/4 v0, 0x0

    .line 167
    .local v0, "direction":I
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStart:Landroid/graphics/PointF;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEnd:Landroid/graphics/PointF;

    if-nez v2, :cond_1

    :cond_0
    move v1, v0

    .line 184
    .end local v0    # "direction":I
    .local v1, "direction":I
    :goto_0
    return v1

    .line 171
    .end local v1    # "direction":I
    .restart local v0    # "direction":I
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStart:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEnd:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_3

    .line 172
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStart:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEnd:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_2

    .line 173
    const/4 v0, 0x4

    :goto_1
    move v1, v0

    .line 184
    .end local v0    # "direction":I
    .restart local v1    # "direction":I
    goto :goto_0

    .line 175
    .end local v1    # "direction":I
    .restart local v0    # "direction":I
    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    .line 178
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStart:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEnd:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v2, v2, v3

    if-gez v2, :cond_4

    .line 179
    const/4 v0, 0x3

    goto :goto_1

    .line 181
    :cond_4
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private checkDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)D
    .locals 10
    .param p1, "point1"    # Landroid/graphics/PointF;
    .param p2, "point2"    # Landroid/graphics/PointF;

    .prologue
    .line 188
    iget v6, p2, Landroid/graphics/PointF;->x:F

    iget v7, p1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v6, v7

    float-to-double v0, v6

    .line 189
    .local v0, "deltaX":D
    iget v6, p2, Landroid/graphics/PointF;->y:F

    iget v7, p1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v7

    float-to-double v2, v6

    .line 190
    .local v2, "deltaY":D
    mul-double v6, v0, v0

    mul-double v8, v2, v2

    add-double/2addr v6, v8

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    .line 191
    .local v4, "distance":D
    return-wide v4
.end method

.method private checkSelectionModeInAlbumView()V
    .locals 11

    .prologue
    .line 832
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v0

    .line 834
    .local v0, "columnCount":I
    if-gtz v0, :cond_1

    .line 868
    :cond_0
    return-void

    .line 837
    :cond_1
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setObjPositionInAlbumView()V

    .line 838
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->changeObjPositionInAlbumView()V

    .line 840
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    rem-int v6, v7, v0

    .line 841
    .local v6, "start":I
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    rem-int v2, v7, v0

    .line 842
    .local v2, "end":I
    if-gez v6, :cond_2

    .line 843
    add-int/2addr v6, v0

    .line 844
    :cond_2
    if-gez v2, :cond_3

    .line 845
    add-int/2addr v2, v0

    .line 847
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .local v3, "i":I
    :goto_0
    iget v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    add-int/lit8 v7, v7, 0x1

    if-ge v3, v7, :cond_6

    .line 848
    rem-int v1, v3, v0

    .line 849
    .local v1, "current":I
    if-gez v1, :cond_4

    .line 850
    add-int/2addr v1, v0

    .line 852
    :cond_4
    if-gt v6, v1, :cond_5

    if-lt v2, v1, :cond_5

    .line 853
    invoke-direct {p0, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v4

    .line 855
    .local v4, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v4, :cond_5

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v7

    if-lez v7, :cond_5

    .line 856
    new-instance v5, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/4 v7, 0x0

    invoke-direct {v5, p0, v7}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 857
    .local v5, "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    iput v3, v5, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 858
    const/4 v7, -0x1

    iput v7, v5, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 859
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 847
    .end local v4    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v5    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 864
    .end local v1    # "current":I
    :cond_6
    const/4 v3, 0x0

    :goto_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v3, v7, :cond_0

    .line 865
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v8, v7, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v10, 0x0

    invoke-direct {p0, v8, v7, v9, v10}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->onPenSelection(IIIZ)V

    .line 864
    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private checkSelectionModeInPhotoView(Landroid/graphics/Rect;II)V
    .locals 16
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "firstItem"    # I
    .param p3, "lastItem"    # I

    .prologue
    .line 1027
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v1

    .line 1028
    .local v1, "columnCount":I
    if-gtz v1, :cond_1

    .line 1111
    :cond_0
    return-void

    .line 1031
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setObjPositionInPhotoView()Z

    move-result v9

    .line 1032
    .local v9, "result":Z
    if-nez v9, :cond_8

    .line 1034
    move/from16 v4, p2

    .local v4, "i":I
    :goto_0
    move/from16 v0, p3

    if-gt v4, v0, :cond_5

    .line 1035
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v13

    if-nez v13, :cond_3

    .line 1034
    :cond_2
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1038
    :cond_3
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 1039
    .local v10, "rx":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v13

    invoke-virtual {v13, v10}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 1041
    move-object/from16 v0, p1

    invoke-static {v0, v10}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v13

    if-nez v13, :cond_4

    move-object/from16 v0, p1

    invoke-virtual {v0, v10}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v13

    if-eqz v13, :cond_2

    .line 1042
    :cond_4
    const v13, 0xffff

    and-int v6, v4, v13

    .line 1043
    .local v6, "itemIndex":I
    new-instance v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 1044
    .local v8, "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    iput v13, v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 1045
    iput v6, v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 1046
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1050
    .end local v6    # "itemIndex":I
    .end local v8    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    .end local v10    # "rx":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1051
    .local v11, "size":I
    add-int/lit8 v4, v11, -0x1

    :goto_2
    if-ltz v4, :cond_7

    .line 1052
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v14, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->prePenSelectionCheck(III)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 1054
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1051
    :cond_6
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 1057
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1058
    const/4 v4, 0x0

    :goto_3
    if-ge v4, v11, :cond_0

    .line 1059
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v14, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v13, v11, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->onPenSelection(IIIZ)V

    .line 1058
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 1065
    .end local v4    # "i":I
    .end local v11    # "size":I
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->changeObjPositionInPhotoView()V

    .line 1067
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    rem-int v12, v13, v1

    .line 1068
    .local v12, "start":I
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    rem-int v3, v13, v1

    .line 1069
    .local v3, "end":I
    if-gez v12, :cond_9

    .line 1070
    add-int/2addr v12, v1

    .line 1071
    :cond_9
    if-gez v3, :cond_a

    .line 1072
    add-int/2addr v3, v1

    .line 1074
    :cond_a
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .restart local v4    # "i":I
    :goto_4
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    add-int/lit8 v13, v13, 0x1

    if-ge v4, v13, :cond_d

    .line 1075
    rem-int v2, v4, v1

    .line 1076
    .local v2, "current":I
    if-gez v2, :cond_b

    .line 1077
    add-int/2addr v2, v1

    .line 1079
    :cond_b
    if-gt v12, v2, :cond_c

    if-lt v3, v2, :cond_c

    .line 1080
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mComposeBaseAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    invoke-virtual {v13, v14, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getItem(II)Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v7

    .line 1082
    .local v7, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mComposeBaseAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getCount(I)I

    move-result v13

    if-ge v4, v13, :cond_c

    .line 1083
    if-eqz v7, :cond_c

    .line 1084
    new-instance v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v13}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 1085
    .restart local v8    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    move-object/from16 v0, p0

    iget v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    iput v13, v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 1086
    iput v4, v8, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 1087
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1074
    .end local v7    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v8    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 1093
    .end local v2    # "current":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1094
    .restart local v11    # "size":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;

    invoke-virtual {v13}, Lcom/sec/android/gallery3d/ui/SelectionManager;->inSelectionMode()Z

    move-result v13

    if-eqz v13, :cond_10

    .line 1095
    add-int/lit8 v4, v11, -0x1

    :goto_5
    if-ltz v4, :cond_f

    .line 1096
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v15, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    invoke-interface {v14, v15, v13, v11}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;->prePenSelectionCheck(III)Z

    move-result v13

    if-eqz v13, :cond_e

    .line 1098
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 1095
    :cond_e
    add-int/lit8 v4, v4, -0x1

    goto :goto_5

    .line 1101
    :cond_f
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 1104
    :cond_10
    const/4 v4, 0x0

    :goto_6
    if-ge v4, v11, :cond_0

    .line 1105
    const/4 v5, 0x0

    .line 1106
    .local v5, "isLast":Z
    add-int/lit8 v13, v11, -0x1

    if-ne v4, v13, :cond_11

    .line 1107
    const/4 v5, 0x1

    .line 1108
    :cond_11
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v15, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v13, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    invoke-interface {v14, v15, v13, v11, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;->onPenSelection(IIIZ)Z

    .line 1104
    add-int/lit8 v4, v4, 0x1

    goto :goto_6
.end method

.method private getColumnCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 141
    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    .line 142
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-nez v1, :cond_2

    .line 148
    :cond_1
    :goto_0
    return v0

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mGroupLineCount:I

    goto :goto_0

    .line 146
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mPhotoLineCount:I

    goto :goto_0
.end method

.method private getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 135
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    if-nez v0, :cond_1

    .line 136
    :cond_0
    const/4 v0, 0x0

    .line 137
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;->mItemCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ObjectControl;->mActiveObject:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/glcore/GlObject;

    goto :goto_0
.end method

.method private getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mComposeBaseAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    return-object v0
.end method

.method private onPenSelection(IIIZ)V
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I
    .param p4, "isLast"    # Z

    .prologue
    .line 161
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;->onPenSelection(IIIZ)Z

    .line 162
    return-void
.end method

.method private playHaptic()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 255
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string/jumbo v3, "spen_feedback_haptic_pen_gesture"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v1, :cond_0

    move v0, v1

    .line 258
    .local v0, "isHapticFeedbackEnabled":Z
    :cond_0
    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mContext:Landroid/content/Context;

    const-string v2, "android.permission.VIBRATE"

    invoke-virtual {v1, v2}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 260
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    if-nez v1, :cond_1

    .line 261
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "vibrator"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Vibrator;

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    .line 263
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->hasVibrator()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 264
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    invoke-virtual {v1}, Landroid/os/Vibrator;->cancel()V

    .line 265
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mVibrator:Landroid/os/Vibrator;

    const-wide/16 v2, 0xc

    invoke-virtual {v1, v2, v3}, Landroid/os/Vibrator;->vibrate(J)V

    .line 268
    :cond_2
    return-void
.end method

.method private prePenSelectionCheck(III)Z
    .locals 1
    .param p1, "albumIndex"    # I
    .param p2, "itemIndex"    # I
    .param p3, "selectedCnt"    # I

    .prologue
    .line 157
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;->prePenSelectionCheck(III)Z

    move-result v0

    return v0
.end method

.method private searchEndObjectInAlbumView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z
    .locals 13
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "firstItem"    # I
    .param p4, "lastItem"    # I

    .prologue
    .line 759
    const/4 v4, -0x1

    .line 760
    .local v4, "maxColumn":I
    const/4 v5, -0x1

    .line 762
    .local v5, "maxRaw":I
    const/16 v6, 0x2710

    .line 763
    .local v6, "minColumn":I
    const/16 v7, 0x2710

    .line 765
    .local v7, "minRaw":I
    const/4 v1, -0x1

    .line 766
    .local v1, "column":I
    const/4 v8, -0x1

    .line 767
    .local v8, "raw":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v2

    .line 768
    .local v2, "columnCount":I
    const/4 v10, -0x1

    .line 769
    .local v10, "resultRaw":I
    const/4 v9, -0x1

    .line 770
    .local v9, "resultColumn":I
    if-gtz v2, :cond_0

    .line 771
    const/4 v11, 0x0

    .line 825
    :goto_0
    return v11

    .line 773
    :cond_0
    invoke-virtual/range {p0 .. p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkObjPosIn(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 774
    const/4 v11, 0x1

    goto :goto_0

    .line 776
    :cond_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-nez v11, :cond_2

    .line 777
    const/4 v11, 0x0

    goto :goto_0

    .line 779
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v11

    if-ge v3, v11, :cond_f

    .line 781
    iget-object v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Integer;

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    shr-int/lit8 v0, v11, 0x10

    .line 783
    .local v0, "albumIndex":I
    rem-int v1, v0, v2

    .line 784
    div-int v8, v0, v2

    .line 786
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x1

    if-eq v11, v12, :cond_3

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x4

    if-ne v11, v12, :cond_9

    .line 787
    :cond_3
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x4

    if-ne v11, v12, :cond_7

    .line 788
    if-ge v5, v8, :cond_4

    .line 789
    move v5, v8

    .line 791
    :cond_4
    move v10, v5

    .line 799
    :goto_2
    if-ge v4, v1, :cond_5

    .line 800
    move v4, v1

    .line 802
    :cond_5
    move v9, v4

    .line 779
    :cond_6
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 793
    :cond_7
    if-le v7, v8, :cond_8

    .line 794
    move v7, v8

    .line 796
    :cond_8
    move v10, v7

    goto :goto_2

    .line 804
    :cond_9
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x2

    if-eq v11, v12, :cond_a

    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_6

    .line 805
    :cond_a
    iget v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v12, 0x3

    if-ne v11, v12, :cond_d

    .line 806
    if-ge v5, v8, :cond_b

    .line 807
    move v5, v8

    .line 809
    :cond_b
    move v10, v5

    .line 817
    :goto_4
    if-le v6, v1, :cond_c

    .line 818
    move v6, v1

    .line 820
    :cond_c
    move v9, v6

    goto :goto_3

    .line 811
    :cond_d
    if-le v7, v8, :cond_e

    .line 812
    move v7, v8

    .line 814
    :cond_e
    move v10, v7

    goto :goto_4

    .line 824
    .end local v0    # "albumIndex":I
    :cond_f
    mul-int v11, v10, v2

    add-int/2addr v11, v9

    iput v11, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 825
    const/4 v11, 0x1

    goto :goto_0
.end method

.method private setObjPositionInAlbumView()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 871
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v1

    .line 872
    .local v1, "columnCount":I
    if-gtz v1, :cond_1

    .line 904
    :cond_0
    :goto_0
    return-void

    .line 875
    :cond_1
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    if-ne v3, v4, :cond_0

    if-eqz v1, :cond_0

    .line 876
    const/4 v0, 0x0

    .line 877
    .local v0, "column":I
    const/4 v2, 0x0

    .line 878
    .local v2, "raw":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    if-eq v3, v4, :cond_6

    .line 879
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    rem-int v0, v3, v1

    .line 881
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-eq v3, v4, :cond_2

    .line 882
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    div-int v2, v3, v1

    .line 902
    :goto_1
    mul-int v3, v1, v2

    add-int/2addr v3, v0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    goto :goto_0

    .line 883
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    if-eq v3, v4, :cond_5

    .line 884
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_3

    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v4, 0x4

    if-ne v3, v4, :cond_4

    .line 885
    :cond_3
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 887
    :cond_4
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 889
    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    div-int/2addr v3, v1

    add-int/lit8 v2, v3, 0x2

    goto :goto_1

    .line 891
    :cond_6
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    if-eq v3, v4, :cond_7

    .line 892
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    rem-int v0, v3, v1

    .line 893
    const/4 v2, -0x1

    goto :goto_1

    .line 894
    :cond_7
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-eq v3, v4, :cond_8

    .line 895
    add-int/lit8 v0, v1, -0x1

    .line 896
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 898
    :cond_8
    add-int/lit8 v0, v1, -0x1

    .line 899
    const/4 v2, 0x2

    goto :goto_1
.end method

.method private setObjPositionInPhotoView()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 1114
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v1

    .line 1115
    .local v1, "columnCount":I
    if-gtz v1, :cond_1

    .line 1151
    :cond_0
    :goto_0
    return v3

    .line 1118
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    if-ne v4, v5, :cond_3

    .line 1119
    const/4 v0, 0x0

    .line 1120
    .local v0, "column":I
    const/4 v2, 0x0

    .line 1122
    .local v2, "raw":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    if-eq v4, v5, :cond_0

    .line 1127
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    if-eq v3, v5, :cond_5

    .line 1128
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    rem-int v0, v3, v1

    .line 1130
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    if-eq v3, v5, :cond_4

    .line 1131
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    div-int v2, v3, v1

    .line 1149
    :goto_1
    mul-int v3, v1, v2

    add-int/2addr v3, v0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 1151
    .end local v0    # "column":I
    .end local v2    # "raw":I
    :cond_3
    const/4 v3, 0x1

    goto :goto_0

    .line 1133
    .restart local v0    # "column":I
    .restart local v2    # "raw":I
    :cond_4
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    div-int/2addr v3, v1

    add-int/lit8 v2, v3, 0x2

    goto :goto_1

    .line 1135
    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    if-eq v3, v5, :cond_6

    .line 1136
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    rem-int v0, v3, v1

    .line 1137
    const/4 v2, -0x1

    goto :goto_1

    .line 1138
    :cond_6
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    if-eq v3, v5, :cond_7

    .line 1139
    add-int/lit8 v0, v1, -0x1

    .line 1140
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 1141
    :cond_7
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    if-eq v3, v5, :cond_8

    .line 1142
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    rem-int v0, v3, v1

    .line 1143
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 1145
    :cond_8
    add-int/lit8 v0, v1, -0x1

    .line 1146
    const/4 v2, 0x2

    goto :goto_1
.end method


# virtual methods
.method protected changeObjPositionInTimeView()V
    .locals 8

    .prologue
    .line 711
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v0

    .line 712
    .local v0, "columnCount":I
    if-gtz v0, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 715
    :cond_1
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    if-gez v6, :cond_2

    .line 716
    const/4 v4, -0x1

    .line 717
    .local v4, "startQuotient":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    mul-int v7, v0, v4

    sub-int v3, v6, v7

    .line 722
    .local v3, "start":I
    :goto_1
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    if-gez v6, :cond_3

    .line 723
    const/4 v2, -0x1

    .line 724
    .local v2, "endQuotient":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    mul-int v7, v0, v2

    sub-int v1, v6, v7

    .line 730
    .local v1, "end":I
    :goto_2
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v7, 0x1

    if-ne v6, v7, :cond_4

    .line 731
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 732
    .local v5, "tmpAlbumIndex":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 733
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 735
    mul-int v6, v0, v2

    add-int/2addr v6, v3

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 736
    mul-int v6, v0, v4

    add-int/2addr v6, v1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 719
    .end local v1    # "end":I
    .end local v2    # "endQuotient":I
    .end local v3    # "start":I
    .end local v4    # "startQuotient":I
    .end local v5    # "tmpAlbumIndex":I
    :cond_2
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    rem-int v3, v6, v0

    .line 720
    .restart local v3    # "start":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    div-int v4, v6, v0

    .restart local v4    # "startQuotient":I
    goto :goto_1

    .line 726
    :cond_3
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    rem-int v1, v6, v0

    .line 727
    .restart local v1    # "end":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    div-int v2, v6, v0

    .restart local v2    # "endQuotient":I
    goto :goto_2

    .line 738
    :cond_4
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v7, 0x2

    if-ne v6, v7, :cond_5

    .line 739
    iget v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 740
    .restart local v5    # "tmpAlbumIndex":I
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 741
    iput v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 743
    mul-int v6, v0, v2

    add-int/2addr v6, v1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 744
    mul-int v6, v0, v4

    add-int/2addr v6, v3

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 745
    .end local v5    # "tmpAlbumIndex":I
    :cond_5
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v7, 0x3

    if-ne v6, v7, :cond_6

    .line 746
    mul-int v6, v0, v4

    add-int/2addr v6, v1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 747
    mul-int v6, v0, v2

    add-int/2addr v6, v3

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0

    .line 748
    :cond_6
    iget v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_0

    .line 749
    mul-int v6, v0, v4

    add-int/2addr v6, v3

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 750
    mul-int v6, v0, v2

    add-int/2addr v6, v1

    iput v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    goto :goto_0
.end method

.method protected checkObjPosIn(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z
    .locals 5
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "firstItem"    # I
    .param p4, "lastItem"    # I

    .prologue
    .line 196
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    .line 198
    .local v2, "rx":Landroid/graphics/Rect;
    move v0, p3

    .local v0, "i":I
    :goto_0
    if-gt v0, p4, :cond_4

    .line 199
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v1

    .line 200
    .local v1, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v1, :cond_1

    .line 198
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 203
    :cond_1
    iget v3, p1, Landroid/graphics/PointF;->x:F

    float-to-int v3, v3

    iget v4, p1, Landroid/graphics/PointF;->y:F

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 204
    const v3, 0xffff

    and-int/2addr v3, v0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    .line 205
    shr-int/lit8 v3, v0, 0x10

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 206
    const/4 v3, 0x1

    .line 214
    .end local v1    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :goto_2
    return v3

    .line 209
    .restart local v1    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 210
    invoke-static {p2, v2}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-virtual {p2, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 211
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 214
    .end local v1    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_4
    const/4 v3, 0x0

    goto :goto_2
.end method

.method protected checkSelectionModeInTimeView(Landroid/graphics/Rect;II)V
    .locals 22
    .param p1, "r"    # Landroid/graphics/Rect;
    .param p2, "firstItem"    # I
    .param p3, "lastItem"    # I

    .prologue
    .line 500
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v6

    .line 501
    .local v6, "columnCount":I
    if-gtz v6, :cond_1

    .line 652
    :cond_0
    return-void

    .line 504
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->setObjPositionInTimeView()Z

    move-result v15

    .line 505
    .local v15, "result":Z
    if-nez v15, :cond_8

    .line 508
    move/from16 v9, p2

    .local v9, "i":I
    :goto_0
    move/from16 v0, p3

    if-gt v9, v0, :cond_5

    .line 509
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v19

    if-nez v19, :cond_3

    .line 508
    :cond_2
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 512
    :cond_3
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 513
    .local v16, "rx":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 515
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v19

    if-nez v19, :cond_4

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 516
    :cond_4
    const v19, 0xffff

    and-int v10, v9, v19

    .line 517
    .local v10, "itemIndex":I
    shr-int/lit8 v5, v9, 0x10

    .line 518
    .local v5, "albumIndex":I
    new-instance v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 519
    .local v14, "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    iput v5, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 520
    iput v10, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 521
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 525
    .end local v5    # "albumIndex":I
    .end local v10    # "itemIndex":I
    .end local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    .end local v16    # "rx":Landroid/graphics/Rect;
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 526
    .local v17, "size":I
    add-int/lit8 v9, v17, -0x1

    :goto_2
    if-ltz v9, :cond_7

    .line 527
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->prePenSelectionCheck(III)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 529
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 526
    :cond_6
    add-int/lit8 v9, v9, -0x1

    goto :goto_2

    .line 532
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 533
    const/4 v9, 0x0

    :goto_3
    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    .line 534
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    move/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->onPenSelection(IIIZ)V

    .line 533
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 540
    .end local v9    # "i":I
    .end local v17    # "size":I
    :cond_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->changeObjPositionInTimeView()V

    .line 542
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    move/from16 v19, v0

    rem-int v18, v19, v6

    .line 543
    .local v18, "start":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    move/from16 v19, v0

    rem-int v8, v19, v6

    .line 544
    .local v8, "end":I
    if-gez v18, :cond_9

    .line 545
    add-int v18, v18, v6

    .line 546
    :cond_9
    if-gez v8, :cond_a

    .line 547
    add-int/2addr v8, v6

    .line 549
    :cond_a
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_10

    .line 550
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v13

    .line 552
    .local v13, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v13, :cond_0

    .line 555
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .restart local v9    # "i":I
    :goto_4
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    if-ge v9, v0, :cond_d

    .line 556
    rem-int v7, v9, v6

    .line 557
    .local v7, "current":I
    if-gez v7, :cond_b

    .line 558
    add-int/2addr v7, v6

    .line 560
    :cond_b
    move/from16 v0, v18

    if-gt v0, v7, :cond_c

    if-lt v8, v7, :cond_c

    .line 561
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v13, v9, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v12

    .line 562
    .local v12, "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v12, :cond_c

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_c

    .line 563
    new-instance v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 564
    .restart local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    iput v0, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 565
    iput v9, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 566
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 555
    .end local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_c
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    .line 570
    .end local v7    # "current":I
    :cond_d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 571
    .restart local v17    # "size":I
    add-int/lit8 v9, v17, -0x1

    :goto_5
    if-ltz v9, :cond_f

    .line 572
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->prePenSelectionCheck(III)Z

    move-result v19

    if-eqz v19, :cond_e

    .line 574
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 571
    :cond_e
    add-int/lit8 v9, v9, -0x1

    goto :goto_5

    .line 578
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 579
    const/4 v9, 0x0

    :goto_6
    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    .line 580
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    move/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->onPenSelection(IIIZ)V

    .line 579
    add-int/lit8 v9, v9, 0x1

    goto :goto_6

    .line 584
    .end local v9    # "i":I
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    .end local v17    # "size":I
    :cond_10
    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .restart local v9    # "i":I
    :goto_7
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    if-ge v9, v0, :cond_1b

    .line 585
    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getSubMediaSet(I)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v13

    .line 586
    .restart local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    if-nez v13, :cond_12

    .line 584
    :cond_11
    add-int/lit8 v9, v9, 0x1

    goto :goto_7

    .line 589
    :cond_12
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v9, v0, :cond_15

    .line 590
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .local v11, "j":I
    :goto_8
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_11

    .line 591
    rem-int v7, v11, v6

    .line 592
    .restart local v7    # "current":I
    if-gez v7, :cond_13

    .line 593
    add-int/2addr v7, v6

    .line 595
    :cond_13
    move/from16 v0, v18

    if-gt v0, v7, :cond_14

    if-lt v8, v7, :cond_14

    .line 596
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v13, v11, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v12

    .line 597
    .restart local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v12, :cond_14

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_14

    .line 598
    new-instance v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 599
    .restart local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    iput v9, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 600
    iput v11, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 601
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 590
    .end local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_14
    add-int/lit8 v11, v11, 0x1

    goto :goto_8

    .line 605
    .end local v7    # "current":I
    .end local v11    # "j":I
    :cond_15
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    move/from16 v19, v0

    move/from16 v0, v19

    if-ne v9, v0, :cond_18

    .line 606
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_9
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    move/from16 v19, v0

    add-int/lit8 v19, v19, 0x1

    move/from16 v0, v19

    if-ge v11, v0, :cond_11

    .line 607
    rem-int v7, v11, v6

    .line 608
    .restart local v7    # "current":I
    if-gez v7, :cond_16

    .line 609
    add-int/2addr v7, v6

    .line 611
    :cond_16
    move/from16 v0, v18

    if-gt v0, v7, :cond_17

    if-lt v8, v7, :cond_17

    .line 612
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v13, v11, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v12

    .line 613
    .restart local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v12, :cond_17

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_17

    .line 614
    new-instance v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 615
    .restart local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    iput v9, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 616
    iput v11, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 617
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    .end local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_17
    add-int/lit8 v11, v11, 0x1

    goto :goto_9

    .line 622
    .end local v7    # "current":I
    .end local v11    # "j":I
    :cond_18
    const/4 v11, 0x0

    .restart local v11    # "j":I
    :goto_a
    invoke-virtual {v13}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v19

    move/from16 v0, v19

    if-ge v11, v0, :cond_11

    .line 623
    rem-int v7, v11, v6

    .line 624
    .restart local v7    # "current":I
    if-gez v7, :cond_19

    .line 625
    add-int/2addr v7, v6

    .line 627
    :cond_19
    move/from16 v0, v18

    if-gt v0, v7, :cond_1a

    if-lt v8, v7, :cond_1a

    .line 628
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v13, v11, v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v12

    .line 629
    .restart local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    if-eqz v12, :cond_1a

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v19

    if-lez v19, :cond_1a

    .line 630
    new-instance v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    const/16 v19, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v14, v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;-><init>(Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$1;)V

    .line 631
    .restart local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    iput v9, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    .line 632
    iput v11, v14, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    .line 633
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 622
    .end local v12    # "media":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v14    # "op":Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;
    :cond_1a
    add-int/lit8 v11, v11, 0x1

    goto :goto_a

    .line 639
    .end local v7    # "current":I
    .end local v11    # "j":I
    .end local v13    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 640
    .restart local v17    # "size":I
    add-int/lit8 v9, v17, -0x1

    :goto_b
    if-ltz v9, :cond_1d

    .line 641
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->prePenSelectionCheck(III)Z

    move-result v19

    if-eqz v19, :cond_1c

    .line 643
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 640
    :cond_1c
    add-int/lit8 v9, v9, -0x1

    goto :goto_b

    .line 646
    :cond_1d
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 647
    const/4 v9, 0x0

    :goto_c
    move/from16 v0, v17

    if-ge v9, v0, :cond_0

    .line 648
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->albumIndex:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;

    move-object/from16 v0, v19

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController$objPosition;->itemIndex:I

    move/from16 v19, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v17

    move/from16 v4, v21

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->onPenSelection(IIIZ)V

    .line 647
    add-int/lit8 v9, v9, 0x1

    goto :goto_c
.end method

.method public destroy()V
    .locals 0

    .prologue
    .line 82
    return-void
.end method

.method public exitFromPenSelectionView()V
    .locals 0

    .prologue
    .line 1196
    return-void
.end method

.method public initAttribute()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 85
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 86
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 87
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 88
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    .line 90
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    .line 91
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    .line 92
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    .line 93
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    .line 94
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    .line 95
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    .line 96
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartAlbumIndex:I

    .line 97
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    .line 99
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    .line 101
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mSelectedObjList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 108
    :cond_1
    return-void
.end method

.method protected searchEndObjectInPhotoView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z
    .locals 14
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "firstItem"    # I
    .param p4, "lastItem"    # I

    .prologue
    .line 949
    const/4 v0, -0x1

    .line 952
    .local v0, "albumIndex":I
    const/4 v5, -0x1

    .line 953
    .local v5, "maxColumn":I
    const/4 v6, -0x1

    .line 955
    .local v6, "maxRaw":I
    const/16 v7, 0x2710

    .line 956
    .local v7, "minColumn":I
    const/16 v8, 0x2710

    .line 958
    .local v8, "minRaw":I
    const/4 v1, -0x1

    .line 959
    .local v1, "column":I
    const/4 v9, -0x1

    .line 960
    .local v9, "raw":I
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v2

    .line 961
    .local v2, "columnCount":I
    const/4 v11, -0x1

    .line 962
    .local v11, "resultRaw":I
    const/4 v10, -0x1

    .line 963
    .local v10, "resultColumn":I
    if-gtz v2, :cond_0

    .line 964
    const/4 v12, 0x0

    .line 1020
    :goto_0
    return v12

    .line 966
    :cond_0
    invoke-virtual/range {p0 .. p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkObjPosIn(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 967
    const/4 v12, 0x1

    goto :goto_0

    .line 969
    :cond_1
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-nez v12, :cond_2

    .line 970
    const/4 v12, 0x0

    goto :goto_0

    .line 972
    :cond_2
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v3, v12, :cond_f

    .line 974
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    shr-int/lit8 v0, v12, 0x10

    .line 975
    iget-object v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    const v13, 0xffff

    and-int v4, v12, v13

    .line 977
    .local v4, "itemIndex":I
    rem-int v1, v4, v2

    .line 978
    div-int v9, v4, v2

    .line 980
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_3

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_9

    .line 981
    :cond_3
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x4

    if-ne v12, v13, :cond_7

    .line 982
    if-ge v6, v9, :cond_4

    .line 983
    move v6, v9

    .line 985
    :cond_4
    move v11, v6

    .line 993
    :goto_2
    if-ge v5, v1, :cond_5

    .line 994
    move v5, v1

    .line 996
    :cond_5
    move v10, v5

    .line 972
    :cond_6
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 987
    :cond_7
    if-le v8, v9, :cond_8

    .line 988
    move v8, v9

    .line 990
    :cond_8
    move v11, v8

    goto :goto_2

    .line 998
    :cond_9
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x2

    if-eq v12, v13, :cond_a

    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_6

    .line 999
    :cond_a
    iget v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    const/4 v13, 0x3

    if-ne v12, v13, :cond_d

    .line 1000
    if-ge v6, v9, :cond_b

    .line 1001
    move v6, v9

    .line 1003
    :cond_b
    move v11, v6

    .line 1011
    :goto_4
    if-le v7, v1, :cond_c

    .line 1012
    move v7, v1

    .line 1014
    :cond_c
    move v10, v7

    goto :goto_3

    .line 1005
    :cond_d
    if-le v8, v9, :cond_e

    .line 1006
    move v8, v9

    .line 1008
    :cond_e
    move v11, v8

    goto :goto_4

    .line 1018
    .end local v4    # "itemIndex":I
    :cond_f
    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 1019
    mul-int v12, v11, v2

    add-int/2addr v12, v10

    iput v12, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    .line 1020
    const/4 v12, 0x1

    goto/16 :goto_0
.end method

.method protected searchEndObjectInTimeView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z
    .locals 19
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "firstItem"    # I
    .param p4, "lastItem"    # I

    .prologue
    .line 380
    const/4 v8, -0x1

    .line 381
    .local v8, "maxColumn":I
    const/4 v9, -0x1

    .line 382
    .local v9, "maxRaw":I
    const/4 v7, -0x1

    .line 384
    .local v7, "maxAlbumIndex":I
    const/16 v11, 0x2710

    .line 385
    .local v11, "minColumn":I
    const/16 v12, 0x2710

    .line 386
    .local v12, "minRaw":I
    const/16 v10, 0x2710

    .line 388
    .local v10, "minAlbumIndex":I
    const/4 v3, -0x1

    .line 389
    .local v3, "column":I
    const/4 v13, -0x1

    .line 390
    .local v13, "raw":I
    const/4 v14, -0x1

    .line 391
    .local v14, "resultAlbumIndex":I
    const/16 v16, -0x1

    .line 392
    .local v16, "resultRaw":I
    const/4 v15, -0x1

    .line 394
    .local v15, "resultColumn":I
    invoke-direct/range {p0 .. p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v4

    .line 395
    .local v4, "columnCount":I
    if-gtz v4, :cond_0

    .line 396
    const/16 v17, 0x0

    .line 492
    :goto_0
    return v17

    .line 398
    :cond_0
    invoke-virtual/range {p0 .. p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkObjPosIn(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 399
    const/16 v17, 0x1

    goto :goto_0

    .line 401
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    if-nez v17, :cond_2

    .line 402
    const/16 v17, 0x0

    goto :goto_0

    .line 404
    :cond_2
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ge v5, v0, :cond_13

    .line 406
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    shr-int/lit8 v2, v17, 0x10

    .line 407
    .local v2, "albumIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mIndexList:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/lang/Integer;

    invoke-virtual/range {v17 .. v17}, Ljava/lang/Integer;->intValue()I

    move-result v17

    const v18, 0xffff

    and-int v6, v17, v18

    .line 409
    .local v6, "itemIndex":I
    rem-int v3, v6, v4

    .line 410
    div-int v13, v6, v4

    .line 412
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_b

    .line 413
    :cond_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_8

    .line 414
    if-ge v7, v2, :cond_7

    .line 415
    move v7, v2

    .line 416
    const/4 v9, -0x1

    .line 418
    if-ge v9, v13, :cond_4

    .line 419
    move v9, v13

    .line 426
    :cond_4
    :goto_2
    move v14, v7

    .line 427
    move/from16 v16, v9

    .line 445
    :goto_3
    if-ge v8, v3, :cond_5

    .line 446
    move v8, v3

    .line 448
    :cond_5
    move v15, v8

    .line 404
    :cond_6
    :goto_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    .line 421
    :cond_7
    if-ne v7, v2, :cond_4

    .line 422
    if-ge v9, v13, :cond_4

    .line 423
    move v9, v13

    goto :goto_2

    .line 429
    :cond_8
    if-le v10, v2, :cond_a

    .line 430
    move v10, v2

    .line 431
    const/16 v12, 0x2710

    .line 433
    if-le v12, v13, :cond_9

    .line 434
    move v12, v13

    .line 441
    :cond_9
    :goto_5
    move v14, v10

    .line 442
    move/from16 v16, v12

    goto :goto_3

    .line 436
    :cond_a
    if-ne v10, v2, :cond_9

    .line 437
    if-le v12, v13, :cond_9

    .line 438
    move v12, v13

    goto :goto_5

    .line 450
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x2

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_6

    .line 451
    :cond_c
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    move/from16 v17, v0

    const/16 v18, 0x3

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_10

    .line 452
    if-ge v7, v2, :cond_f

    .line 453
    move v7, v2

    .line 454
    const/4 v9, -0x1

    .line 456
    if-ge v9, v13, :cond_d

    .line 457
    move v9, v13

    .line 464
    :cond_d
    :goto_6
    move v14, v7

    .line 465
    move/from16 v16, v9

    .line 483
    :goto_7
    if-le v11, v3, :cond_e

    .line 484
    move v11, v3

    .line 486
    :cond_e
    move v15, v11

    goto :goto_4

    .line 459
    :cond_f
    if-ne v7, v2, :cond_d

    .line 460
    if-ge v9, v13, :cond_d

    .line 461
    move v9, v13

    goto :goto_6

    .line 467
    :cond_10
    if-le v10, v2, :cond_12

    .line 468
    move v10, v2

    .line 469
    const/16 v12, 0x2710

    .line 471
    if-le v12, v13, :cond_11

    .line 472
    move v12, v13

    .line 479
    :cond_11
    :goto_8
    move v14, v10

    .line 480
    move/from16 v16, v12

    goto :goto_7

    .line 474
    :cond_12
    if-ne v10, v2, :cond_11

    .line 475
    if-le v12, v13, :cond_11

    .line 476
    move v12, v13

    goto :goto_8

    .line 490
    .end local v2    # "albumIndex":I
    .end local v6    # "itemIndex":I
    :cond_13
    move-object/from16 v0, p0

    iput v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndAlbumIndex:I

    .line 491
    mul-int v17, v16, v4

    add-int v17, v17, v15

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEndItemIndex:I

    .line 492
    const/16 v17, 0x1

    goto/16 :goto_0
.end method

.method protected searchFirstObject(Landroid/graphics/PointF;II)Z
    .locals 22
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "firstItem"    # I
    .param p3, "lastItem"    # I

    .prologue
    .line 273
    new-instance v19, Landroid/graphics/Rect;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Rect;-><init>()V

    .line 274
    .local v19, "rx":Landroid/graphics/Rect;
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 275
    .local v18, "rTop":Landroid/graphics/Rect;
    new-instance v16, Landroid/graphics/Rect;

    invoke-direct/range {v16 .. v16}, Landroid/graphics/Rect;-><init>()V

    .line 276
    .local v16, "rLeft":Landroid/graphics/Rect;
    new-instance v15, Landroid/graphics/Rect;

    invoke-direct {v15}, Landroid/graphics/Rect;-><init>()V

    .line 277
    .local v15, "rBottom":Landroid/graphics/Rect;
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    .line 279
    .local v17, "rRight":Landroid/graphics/Rect;
    new-instance v14, Landroid/graphics/PointF;

    invoke-direct {v14}, Landroid/graphics/PointF;-><init>()V

    .line 281
    .local v14, "objPoint":Landroid/graphics/PointF;
    const-wide v12, 0x40b3880000000000L    # 5000.0

    .line 282
    .local v12, "minTopDistance":D
    const-wide v8, 0x40b3880000000000L    # 5000.0

    .line 283
    .local v8, "minLeftDistance":D
    const-wide v6, 0x40b3880000000000L    # 5000.0

    .line 284
    .local v6, "minBottomDistance":D
    const-wide v10, 0x40b3880000000000L    # 5000.0

    .line 287
    .local v10, "minRightDistance":D
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 288
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 289
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0xa

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 290
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 292
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 293
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0xa

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 294
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 295
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v16

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 297
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, -0xa

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->left:I

    .line 298
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->top:I

    .line 299
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->right:I

    .line 300
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayHeight(Landroid/content/Context;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v15, Landroid/graphics/Rect;->bottom:I

    .line 302
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 303
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 304
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayer:Lcom/sec/android/gallery3d/glcore/GlLayer;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/glcore/GlLayer;->getGlRoot()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDisplayWidth(Landroid/content/Context;)I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    const/high16 v21, 0x3f800000    # 1.0f

    mul-float v20, v20, v21

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->right:I

    .line 305
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    add-int/lit8 v20, v20, 0xa

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 307
    move/from16 v4, p2

    .local v4, "i":I
    :goto_0
    move/from16 v0, p3

    if-gt v4, v0, :cond_a

    .line 308
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getItem(I)Lcom/sec/android/gallery3d/glcore/GlObject;

    move-result-object v5

    .line 309
    .local v5, "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    if-nez v5, :cond_1

    .line 307
    :cond_0
    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 312
    :cond_1
    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    move/from16 v20, v0

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v20, v0

    move-object/from16 v0, p1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move/from16 v21, v0

    move/from16 v0, v21

    float-to-int v0, v0

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->checkPosIn(II)Z

    move-result v20

    if-eqz v20, :cond_2

    .line 313
    const v20, 0xffff

    and-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 314
    shr-int/lit8 v20, v4, 0x10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 315
    const/16 v20, 0x1

    .line 372
    .end local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :goto_2
    return v20

    .line 318
    .restart local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_2
    move-object/from16 v0, v19

    invoke-virtual {v5, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->getGlObjectRect(Landroid/graphics/Rect;)V

    .line 320
    invoke-static/range {v18 .. v19}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v20

    if-nez v20, :cond_3

    invoke-virtual/range {v18 .. v19}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 321
    :cond_3
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->x:F

    .line 322
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerY()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->y:F

    .line 324
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)D

    move-result-wide v2

    .line 325
    .local v2, "calcDistance":D
    cmpg-double v20, v2, v12

    if-gtz v20, :cond_4

    .line 326
    move-wide v12, v2

    .line 328
    const v20, 0xffff

    and-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    .line 329
    shr-int/lit8 v20, v4, 0x10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    .line 333
    .end local v2    # "calcDistance":D
    :cond_4
    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v20

    if-nez v20, :cond_5

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 334
    :cond_5
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->x:F

    .line 335
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerY()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->y:F

    .line 337
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)D

    move-result-wide v2

    .line 338
    .restart local v2    # "calcDistance":D
    cmpg-double v20, v2, v8

    if-gtz v20, :cond_6

    .line 339
    move-wide v8, v2

    .line 341
    const v20, 0xffff

    and-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    .line 342
    shr-int/lit8 v20, v4, 0x10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    .line 346
    .end local v2    # "calcDistance":D
    :cond_6
    move-object/from16 v0, v19

    invoke-static {v15, v0}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v20

    if-nez v20, :cond_7

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 347
    :cond_7
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->x:F

    .line 348
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerY()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->y:F

    .line 350
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)D

    move-result-wide v2

    .line 351
    .restart local v2    # "calcDistance":D
    cmpg-double v20, v2, v6

    if-gtz v20, :cond_8

    .line 352
    move-wide v6, v2

    .line 354
    const v20, 0xffff

    and-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    .line 355
    shr-int/lit8 v20, v4, 0x10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    .line 359
    .end local v2    # "calcDistance":D
    :cond_8
    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v20

    if-nez v20, :cond_9

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v20

    if-eqz v20, :cond_0

    .line 360
    :cond_9
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerX()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->x:F

    .line 361
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Rect;->centerY()I

    move-result v20

    move/from16 v0, v20

    int-to-float v0, v0

    move/from16 v20, v0

    move/from16 v0, v20

    iput v0, v14, Landroid/graphics/PointF;->y:F

    .line 363
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v14}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkDistance(Landroid/graphics/PointF;Landroid/graphics/PointF;)D

    move-result-wide v2

    .line 364
    .restart local v2    # "calcDistance":D
    cmpg-double v20, v2, v10

    if-gtz v20, :cond_0

    .line 365
    move-wide v10, v2

    .line 367
    const v20, 0xffff

    and-int v20, v20, v4

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    .line 368
    shr-int/lit8 v20, v4, 0x10

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartAlbumIndex:I

    goto/16 :goto_1

    .line 372
    .end local v2    # "calcDistance":D
    .end local v5    # "obj":Lcom/sec/android/gallery3d/glcore/GlObject;
    :cond_a
    const/16 v20, 0x1

    goto/16 :goto_2
.end method

.method public searchObject(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z
    .locals 6
    .param p1, "point"    # Landroid/graphics/PointF;
    .param p2, "r"    # Landroid/graphics/Rect;
    .param p3, "firstVisibleItem"    # I
    .param p4, "lastVisibleItem"    # I

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 218
    const/4 v0, 0x0

    .line 220
    .local v0, "result":Z
    if-nez p2, :cond_4

    .line 221
    invoke-virtual {p0, p1, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchFirstObject(Landroid/graphics/PointF;II)Z

    move-result v0

    .line 237
    :cond_0
    if-eqz p2, :cond_3

    .line 238
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-nez v2, :cond_9

    .line 239
    invoke-virtual {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkSelectionModeInTimeView(Landroid/graphics/Rect;II)V

    .line 247
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    if-eqz v2, :cond_2

    .line 248
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    invoke-interface {v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;->isCheckAvailable()V

    .line 249
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->playHaptic()V

    :cond_3
    move v1, v0

    .line 251
    .end local v0    # "result":Z
    .local v1, "result":Z
    :goto_1
    return v1

    .line 223
    .end local v1    # "result":Z
    .restart local v0    # "result":Z
    :cond_4
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-nez v2, :cond_6

    .line 224
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchEndObjectInTimeView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v0

    .line 233
    :cond_5
    :goto_2
    if-nez v0, :cond_0

    move v1, v0

    .line 234
    .end local v0    # "result":Z
    .restart local v1    # "result":Z
    goto :goto_1

    .line 225
    .end local v1    # "result":Z
    .restart local v0    # "result":Z
    :cond_6
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v3, :cond_7

    .line 226
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchEndObjectInAlbumView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v0

    goto :goto_2

    .line 227
    :cond_7
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v4, :cond_8

    .line 228
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchEndObjectInPhotoView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v0

    goto :goto_2

    .line 229
    :cond_8
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v5, :cond_5

    .line 230
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->searchEndObjectInAlbumView(Landroid/graphics/PointF;Landroid/graphics/Rect;II)Z

    move-result v0

    goto :goto_2

    .line 240
    :cond_9
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v3, :cond_a

    .line 241
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkSelectionModeInAlbumView()V

    goto :goto_0

    .line 242
    :cond_a
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v4, :cond_b

    .line 243
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkSelectionModeInPhotoView(Landroid/graphics/Rect;II)V

    goto :goto_0

    .line 244
    :cond_b
    iget v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLayerType:I

    if-ne v2, v5, :cond_1

    .line 245
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkSelectionModeInAlbumView()V

    goto :goto_0
.end method

.method public setAdapter(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;)V
    .locals 0
    .param p1, "adapter"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .prologue
    .line 123
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mComposeBaseAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    .line 124
    return-void
.end method

.method public setDirection()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->checkDirection()I

    move-result v0

    iput v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mDirection:I

    .line 120
    return-void
.end method

.method public setEndPoint(Landroid/graphics/PointF;)V
    .locals 0
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mEnd:Landroid/graphics/PointF;

    .line 116
    return-void
.end method

.method protected setObjPositionInTimeView()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 655
    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->getColumnCount()I

    move-result v1

    .line 656
    .local v1, "columnCount":I
    if-gtz v1, :cond_1

    .line 702
    :cond_0
    :goto_0
    return v3

    .line 659
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    if-ne v4, v5, :cond_4

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    if-ne v4, v5, :cond_4

    .line 660
    const/4 v0, 0x0

    .line 661
    .local v0, "column":I
    const/4 v2, 0x0

    .line 663
    .local v2, "raw":I
    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartAlbumIndex:I

    if-ne v4, v5, :cond_2

    iget v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mRightStartItemIndex:I

    if-eq v4, v5, :cond_0

    .line 670
    :cond_2
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    if-eq v3, v5, :cond_6

    .line 671
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartItemIndex:I

    rem-int v0, v3, v1

    .line 673
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-eq v3, v5, :cond_5

    .line 674
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 676
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    div-int v2, v3, v1

    .line 700
    :cond_3
    :goto_1
    mul-int v3, v1, v2

    add-int/2addr v3, v0

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartItemIndex:I

    .line 702
    .end local v0    # "column":I
    .end local v2    # "raw":I
    :cond_4
    const/4 v3, 0x1

    goto :goto_0

    .line 678
    .restart local v0    # "column":I
    .restart local v2    # "raw":I
    :cond_5
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mTopStartAlbumIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 679
    const/4 v2, -0x1

    goto :goto_1

    .line 681
    :cond_6
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    if-eq v3, v5, :cond_8

    .line 682
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartItemIndex:I

    rem-int v0, v3, v1

    .line 684
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-eq v3, v5, :cond_7

    .line 685
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 687
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    div-int v2, v3, v1

    goto :goto_1

    .line 689
    :cond_7
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mBottomStartAlbumIndex:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    .line 690
    const/4 v2, -0x1

    goto :goto_1

    .line 692
    :cond_8
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    if-eq v3, v5, :cond_3

    .line 693
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    rem-int v0, v3, v1

    .line 694
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartItemIndex:I

    div-int v2, v3, v1

    .line 695
    iget v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mLeftStartAlbumIndex:I

    iput v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStartAlbumIndex:I

    goto :goto_1
.end method

.method public setPenSelectionListener(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    .prologue
    .line 127
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPenSelectionListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnPenSelectionListener;

    .line 128
    return-void
.end method

.method public setStartPoint(Landroid/graphics/PointF;)V
    .locals 0
    .param p1, "point"    # Landroid/graphics/PointF;

    .prologue
    .line 111
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mStart:Landroid/graphics/PointF;

    .line 112
    return-void
.end method

.method public setposCtrl(Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;)V
    .locals 0
    .param p1, "posCtrl"    # Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .prologue
    .line 131
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlPenSelectController;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    .line 132
    return-void
.end method
