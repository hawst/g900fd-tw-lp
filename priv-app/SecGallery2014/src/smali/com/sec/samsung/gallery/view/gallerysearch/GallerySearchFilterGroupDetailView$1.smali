.class Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;
.super Landroid/database/DataSetObserver;
.source "GallerySearchFilterGroupDetailView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 5

    .prologue
    .line 38
    invoke-super {p0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 39
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->removeAllViews()V

    .line 41
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->getCount()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 42
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-virtual {v2, v1, v3, v4}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 44
    .local v0, "child":Landroid/view/View;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->mAdapter:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 45
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->addView(Landroid/view/View;)V

    .line 41
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView$1;->this$0:Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/gallerysearch/GallerySearchFilterGroupDetailView;->requestLayout()V

    .line 51
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 55
    invoke-super {p0}, Landroid/database/DataSetObserver;->onInvalidated()V

    .line 56
    return-void
.end method
