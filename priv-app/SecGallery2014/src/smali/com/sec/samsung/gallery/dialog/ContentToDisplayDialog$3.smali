.class Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;
.super Ljava/lang/Object;
.source "ContentToDisplayDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->setupListView(Landroid/widget/ListView;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

.field final synthetic val$listView:Landroid/widget/ListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;Landroid/widget/ListView;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    iput-object p2, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->val$listView:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private isAllItemsSelected()Z
    .locals 5

    .prologue
    .line 335
    const/4 v0, 0x1

    .local v0, "i":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .local v1, "n":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 336
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 337
    .local v2, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v3, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    const/4 v3, 0x0

    .line 342
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :goto_1
    return v3

    .line 335
    .restart local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    .end local v2    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_1
    const/4 v3, 0x1

    goto :goto_1
.end method

.method private updateListItem(Z)V
    .locals 6
    .param p1, "isAllContentSelected"    # Z

    .prologue
    .line 318
    const/4 v1, 0x1

    .local v1, "i":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .local v2, "n":I
    :goto_0
    if-ge v1, v2, :cond_3

    .line 319
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    check-cast v4, Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    .line 321
    .local v0, "checkbox":Landroid/widget/CheckedTextView;
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    .line 322
    .local v3, "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    if-eqz p1, :cond_1

    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 323
    if-eqz v0, :cond_0

    .line 324
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 325
    :cond_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 318
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 327
    :cond_1
    if-eqz v0, :cond_2

    .line 328
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 329
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    xor-int/lit8 v5, v5, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_1

    .line 332
    .end local v0    # "checkbox":Landroid/widget/CheckedTextView;
    .end local v3    # "vtype":Lcom/sec/samsung/gallery/core/ViewByFilterType;
    :cond_3
    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 9
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v8, 0x42

    const/16 v7, 0x17

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 276
    if-eq p2, v8, :cond_0

    if-ne p2, v7, :cond_8

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-ne v4, v5, :cond_8

    .line 278
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getSelectedItemPosition()I

    move-result v2

    .line 279
    .local v2, "position":I
    if-gez v2, :cond_2

    .line 314
    .end local v2    # "position":I
    :cond_1
    :goto_0
    return v6

    .line 281
    .restart local v2    # "position":I
    :cond_2
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v3

    .line 282
    .local v3, "viewType":I
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->val$listView:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getSelectedView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckedTextView;

    .line 283
    .local v1, "ctv":Landroid/widget/CheckedTextView;
    if-eqz v1, :cond_3

    iget-object v7, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mValidOptions:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$300(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v4

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->isViewByOptionEnabled(I)Z
    invoke-static {v7, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$400(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 284
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_1
    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->setChecked(Z)V

    .line 285
    const v4, 0x8000

    invoke-virtual {v1, v4}, Landroid/widget/CheckedTextView;->sendAccessibilityEvent(I)V

    .line 286
    invoke-virtual {v1}, Landroid/widget/CheckedTextView;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    .line 287
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v3}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 293
    :cond_3
    :goto_2
    if-nez v2, :cond_6

    .line 295
    sget-object v4, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    iget-object v5, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$000(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->isOptionSelected(I)Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->updateListItem(Z)V

    .line 309
    :goto_3
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # invokes: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->updateSaveButton()V
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$500(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)V

    goto :goto_0

    :cond_4
    move v4, v6

    .line 284
    goto :goto_1

    .line 289
    :cond_5
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    xor-int/lit8 v5, v3, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_2

    .line 298
    :cond_6
    invoke-direct {p0}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->isAllItemsSelected()Z

    move-result v0

    .line 299
    .local v0, "allItemsChecked":Z
    if-eqz v0, :cond_7

    .line 301
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    # |= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$076(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    .line 307
    :goto_4
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v4

    check-cast v4, Landroid/app/AlertDialog;

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/ListView;->invalidateViews()V

    goto :goto_3

    .line 303
    :cond_7
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    sget-object v5, Lcom/sec/samsung/gallery/core/ViewByFilterType;->ALL:Lcom/sec/samsung/gallery/core/ViewByFilterType;

    invoke-virtual {v5}, Lcom/sec/samsung/gallery/core/ViewByFilterType;->getIndex()I

    move-result v5

    xor-int/lit8 v5, v5, -0x1

    # &= operator for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mCurrentViewType:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$072(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;I)I

    goto :goto_4

    .line 310
    .end local v0    # "allItemsChecked":Z
    .end local v1    # "ctv":Landroid/widget/CheckedTextView;
    .end local v2    # "position":I
    .end local v3    # "viewType":I
    :cond_8
    if-eq p2, v8, :cond_9

    if-ne p2, v7, :cond_1

    :cond_9
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    if-nez v4, :cond_1

    .line 312
    iget-object v4, p0, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog$3;->this$0:Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;

    # getter for: Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;->access$200(Lcom/sec/samsung/gallery/dialog/ContentToDisplayDialog;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->playSoundKeyClick(Landroid/content/Context;)V

    goto/16 :goto_0
.end method
