.class public final enum Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;
.super Ljava/lang/Enum;
.source "HideRootViewCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/HideRootViewCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RootViewVisibility"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

.field public static final enum HIDE:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

.field public static final enum NOT_INITIALIZED:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

.field public static final enum SHOW:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

.field public static final enum SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    const-string v1, "NOT_INITIALIZED"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->NOT_INITIALIZED:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    new-instance v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->HIDE:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    new-instance v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v4}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    new-instance v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    const-string v1, "SHOW_WITH_DELAY"

    invoke-direct {v0, v1, v5}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    .line 19
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->NOT_INITIALIZED:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->HIDE:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    aput-object v1, v0, v3

    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    aput-object v1, v0, v4

    sget-object v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    aput-object v1, v0, v5

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->$VALUES:[Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 19
    const-class v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->$VALUES:[Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    return-object v0
.end method
