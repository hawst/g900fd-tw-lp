.class public Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;
.super Lcom/sec/android/gallery3d/glcore/GlAnimationBase;
.source "GlPhotoView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlPhotoView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "GlFadeOutAnim"
.end annotation


# static fields
.field public static final ANIM_TYPE_1:I = 0x1

.field public static final ANIM_TYPE_2:I = 0x2

.field private static final Z_MOVE:F = 300.0f


# instance fields
.field private mAnimType:I

.field private mCount:I

.field private mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mPosZ:[F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlPhotoView;)V
    .locals 0

    .prologue
    .line 1062
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    invoke-direct {p0}, Lcom/sec/android/gallery3d/glcore/GlAnimationBase;-><init>()V

    return-void
.end method


# virtual methods
.method protected applyTransform(F)V
    .locals 5
    .param p1, "ratio"    # F

    .prologue
    const/4 v4, 0x0

    .line 1090
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_1

    .line 1102
    :cond_0
    return-void

    .line 1093
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_0

    .line 1094
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-nez v1, :cond_3

    .line 1093
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1097
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mPosZ:[F

    aget v2, v2, v0

    const/high16 v3, 0x43960000    # 300.0f

    mul-float/2addr v3, p1

    add-float/2addr v2, v3

    const/4 v3, 0x4

    invoke-virtual {v1, v4, v4, v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFFI)V

    .line 1098
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setAlpha(F)V

    .line 1099
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mAnimType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_2

    .line 1100
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    const/high16 v2, 0x42b40000    # 90.0f

    mul-float/2addr v2, p1

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->setYaw(F)V

    goto :goto_1
.end method

.method protected onCancel()V
    .locals 0

    .prologue
    .line 1105
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->onStop()V

    .line 1106
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1109
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    if-nez v1, :cond_0

    .line 1120
    :goto_0
    return-void

    .line 1111
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_2

    .line 1112
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 1113
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 1114
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aput-object v3, v1, v0

    .line 1111
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1117
    :cond_2
    iput-object v3, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1118
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlPhotoView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1100(Lcom/sec/samsung/gallery/glview/GlPhotoView;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView;->removeAnimation(Lcom/sec/android/gallery3d/glcore/GlAnimationBase;)V

    .line 1119
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->this$0:Lcom/sec/samsung/gallery/glview/GlPhotoView;

    # setter for: Lcom/sec/samsung/gallery/glview/GlPhotoView;->mFadeOutAnim:Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;
    invoke-static {v1, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView;->access$1102(Lcom/sec/samsung/gallery/glview/GlPhotoView;Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;)Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;

    goto :goto_0
.end method

.method public startAnimation([Lcom/sec/android/gallery3d/glcore/GlObject;I)V
    .locals 4
    .param p1, "objects"    # [Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "animType"    # I

    .prologue
    .line 1073
    if-nez p1, :cond_0

    .line 1087
    :goto_0
    return-void

    .line 1076
    :cond_0
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 1077
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mAnimType:I

    .line 1078
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    array-length v1, v1

    iput v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mCount:I

    .line 1079
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mCount:I

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mPosZ:[F

    .line 1081
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mCount:I

    if-ge v0, v1, :cond_2

    .line 1082
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 1083
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mPosZ:[F

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->mObjects:[Lcom/sec/android/gallery3d/glcore/GlObject;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/glcore/GlObject;->getZ()F

    move-result v2

    aput v2, v1, v0

    .line 1081
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1085
    :cond_2
    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v2, v3}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->setDuration(J)V

    .line 1086
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlPhotoView$GlFadeOutAnim;->start()V

    goto :goto_0
.end method
