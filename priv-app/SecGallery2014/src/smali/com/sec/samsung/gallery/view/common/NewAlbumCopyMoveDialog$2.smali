.class Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;
.super Ljava/lang/Object;
.source "NewAlbumCopyMoveDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->createDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->putAppUseLog()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->access$000(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V

    .line 75
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    sget v1, Lcom/sec/samsung/gallery/core/Event;->EVENT_COPY_FILES:I

    # invokes: Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->handleOnClick(I)V
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->access$100(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;I)V

    .line 76
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDragAndDropInExpandSplitView:Z

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog$2;->this$0:Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;

    # invokes: Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->expandSplitView()V
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;->access$200(Lcom/sec/samsung/gallery/view/common/NewAlbumCopyMoveDialog;)V

    .line 78
    :cond_0
    return-void
.end method
