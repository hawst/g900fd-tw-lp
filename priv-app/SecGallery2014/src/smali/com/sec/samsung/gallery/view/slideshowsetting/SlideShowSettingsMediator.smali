.class public Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;
.super Lorg/puremvc/java/patterns/mediator/Mediator;
.source "SlideShowSettingsMediator.java"

# interfaces
.implements Lcom/sec/android/gallery3d/app/GalleryEvent$OnConfigurationChangedListener;
.implements Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;
.implements Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;
.implements Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;


# static fields
.field public static final REQUEST_CODE_FOR_MUSIC:I = 0x9


# instance fields
.field private isOnSoundPick:Z

.field private mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private mIsEnabled:Z

.field private mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "mediatorName"    # Ljava/lang/String;
    .param p2, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lorg/puremvc/java/patterns/mediator/Mediator;-><init>(Ljava/lang/String;Ljava/lang/Object;)V

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isOnSoundPick:Z

    .line 31
    invoke-static {p2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 32
    iput-object p2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 33
    return-void
.end method

.method private getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->getViewComponent()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    return-object v0
.end method

.method private reloadSetting()V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->reloadSetting()V

    .line 86
    :cond_0
    return-void
.end method

.method private showSlideshowSetting()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->closeOptionsMenu()V

    .line 69
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->registerListeners()V

    .line 70
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    .line 74
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setContext(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    .line 75
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->reloadSetting()V

    .line 76
    return-void
.end method


# virtual methods
.method public dismissSlideShowSettingsView()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "SLIDE_SHOW_SETTING_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 94
    :cond_0
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->unregisterListeners()V

    .line 95
    return-void
.end method

.method public handleNotification(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 3
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 49
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getName()Ljava/lang/String;

    move-result-object v0

    .line 50
    .local v0, "notiName":Ljava/lang/String;
    const-string v2, "SLIDESHOW_PICK_MUSIC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 51
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->startSoundPick()Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isOnSoundPick:Z

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v2, "DISSMISS_SLIDE_SHOW_SETTINGS_DIALOG"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 54
    const-string v2, "MEDIA_EJECT"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 57
    const-string v2, "SLIDESHOW_SETUP_MUSIC"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 58
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 59
    .local v1, "ringtoneUri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v2, v1}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->setupRingtoneChoice(Landroid/net/Uri;)V

    goto :goto_0

    .line 61
    .end local v1    # "ringtoneUri":Landroid/net/Uri;
    :cond_2
    const-string v2, "SLIDESHOW_MUSIC_UPDATE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 62
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    if-eqz v2, :cond_0

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v2}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->reloadSlideShowSettings()V

    goto :goto_0
.end method

.method public isSlideshowSetiingEnable()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mIsEnabled:Z

    return v0
.end method

.method public listNotificationInterests()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "SLIDESHOW_PICK_MUSIC"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DISSMISS_SLIDE_SHOW_SETTINGS_DIALOG"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SLIDESHOW_SETUP_MUSIC"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "SLIDESHOW_MUSIC_UPDATE"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "MEDIA_EJECT"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 101
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 131
    sget-boolean v0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsActionbar;->misSlideshowRunning:Z

    if-eqz v0, :cond_0

    .line 132
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->setSlideshowSettingEnable(Z)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->stopCurrentMusic()V

    .line 134
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isSlideshowSetiingEnable()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 125
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->reloadSlideShowSettings()V

    .line 127
    :cond_0
    return-void
.end method

.method public onStateResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "request"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 139
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->isOnSoundPick:Z

    .line 141
    const/16 v1, 0x9

    if-ne p1, v1, :cond_0

    .line 142
    const/4 v1, 0x3

    new-array v0, v1, [Ljava/lang/Object;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v1, v0, v2

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    aput-object p3, v0, v1

    .line 145
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "ADD_SLIDESHOW_MUSIC"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 147
    .end local v0    # "params":[Ljava/lang/Object;
    :cond_0
    return-void
.end method

.method public registerListeners()V
    .locals 2

    .prologue
    .line 104
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 105
    .local v0, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    .line 106
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->registerOnPauseListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;)V

    .line 107
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->registerOnResumeListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;)V

    .line 108
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->registerOnStateResultListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;)V

    .line 110
    :cond_0
    return-void
.end method

.method public resetLayout()V
    .locals 2

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mIsEnabled:Z

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 169
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->reloadSetting()V

    .line 171
    :cond_0
    return-void
.end method

.method public setSlideshowSettingEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 154
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mIsEnabled:Z

    if-ne v0, p1, :cond_0

    .line 165
    :goto_0
    return-void

    .line 156
    :cond_0
    if-eqz p1, :cond_1

    .line 157
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->showSlideshowSetting()V

    .line 164
    :goto_1
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mIsEnabled:Z

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mSettingView:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingView;->destroyView()V

    .line 162
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->dismissSlideShowSettingsView()V

    goto :goto_1
.end method

.method public unregisterListeners()V
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideShowSettingsMediator;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 114
    .local v0, "state":Lcom/sec/android/gallery3d/app/ActivityState;
    if-eqz v0, :cond_0

    .line 115
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->unregisterOnPauseListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnPauseListener;)V

    .line 116
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->unregisterOnResumeListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnResumeListener;)V

    .line 117
    invoke-virtual {v0, p0}, Lcom/sec/android/gallery3d/app/ActivityState;->unregisterOnStateResultListener(Lcom/sec/android/gallery3d/app/GalleryEvent$OnStateResultListener;)V

    .line 119
    :cond_0
    return-void
.end method
