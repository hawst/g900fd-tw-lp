.class public Lcom/sec/samsung/gallery/controller/HideRootViewCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "HideRootViewCmd.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mVisibility:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    .line 23
    sget-object v0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->NOT_INITIALIZED:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->mVisibility:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    return-void
.end method

.method private setWindowOpaque(Landroid/app/Activity;J)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "delay"    # J

    .prologue
    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$1;

    invoke-direct {v1, p0, p1}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/HideRootViewCmd;Landroid/app/Activity;)V

    invoke-virtual {v0, v1, p2, p3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 58
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 10
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v9, 0x0

    .line 27
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Ljava/lang/Object;

    move-object v4, v6

    check-cast v4, [Ljava/lang/Object;

    .line 28
    .local v4, "params":[Ljava/lang/Object;
    aget-object v0, v4, v9

    check-cast v0, Landroid/app/Activity;

    .line 29
    .local v0, "activity":Landroid/app/Activity;
    const/4 v6, 0x1

    aget-object v5, v4, v6

    check-cast v5, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    .line 31
    .local v5, "visibility":Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->mVisibility:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    if-ne v6, v5, :cond_1

    .line 49
    :cond_0
    :goto_0
    return-void

    .line 34
    :cond_1
    iput-object v5, p0, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->mVisibility:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    .line 36
    const v6, 0x7f0f00bf

    invoke-virtual {v0, v6}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 37
    .local v1, "glRootView":Landroid/view/View;
    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->HIDE:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    if-ne v5, v6, :cond_2

    .line 38
    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    const/4 v6, 0x4

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 41
    :cond_2
    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    if-eq v5, v6, :cond_3

    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    if-ne v5, v6, :cond_0

    .line 43
    :cond_3
    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    sget-object v6, Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;->SHOW_WITH_DELAY:Lcom/sec/samsung/gallery/controller/HideRootViewCmd$RootViewVisibility;

    if-ne v5, v6, :cond_4

    const-wide/16 v2, 0x1f4

    .line 46
    .local v2, "delay":J
    :goto_1
    invoke-direct {p0, v0, v2, v3}, Lcom/sec/samsung/gallery/controller/HideRootViewCmd;->setWindowOpaque(Landroid/app/Activity;J)V

    .line 47
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 45
    .end local v2    # "delay":J
    :cond_4
    const-wide/16 v2, 0x0

    goto :goto_1
.end method
