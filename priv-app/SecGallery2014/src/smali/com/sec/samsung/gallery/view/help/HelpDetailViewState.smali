.class public Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "HelpDetailViewState.java"

# interfaces
.implements Lcom/sec/android/gallery3d/ui/PhotoView$Listener;


# static fields
.field private static final DOUBLE_TAP_TIMEOUT:I

.field private static final HIDE_BARS_TIMEOUT:I = 0xdac

.field private static final HIDE_BARS_TIMEOUT_FOR_SPC:I = 0x157c

.field public static final KEY_MIME_TYPE:Ljava/lang/String; = "mime-type"

.field private static final MSG_END_SCALE:I = 0x9

.field private static final MSG_HIDE_BARS:I = 0x1

.field private static final MSG_SET_BARS_DELAY:I = 0x8

.field private static final MSG_SMOOTH_ZOOM_IN:I = 0x6

.field private static final MSG_SMOOTH_ZOOM_OUT:I = 0x7

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

.field private mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

.field private mCurrentIndex:I

.field private mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

.field private mDisableAutoHideBars:Z

.field private final mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

.field private mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

.field private mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

.field private mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mHandler:Landroid/os/Handler;

.field private mHelpMode:I

.field private mIsActive:Z

.field private mIsInteracting:Z

.field private mIsMenuVisible:Z

.field protected mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

.field private mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field private mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

.field private final mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field private mPhotoDataAdapterListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

.field private mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

.field private final mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

.field private mShowBars:Z

.field private final mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-class v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->TAG:Ljava/lang/String;

    .line 61
    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    sput v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->DOUBLE_TAP_TIMEOUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 58
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 80
    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    .line 81
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    .line 82
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    .line 83
    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mDisableAutoHideBars:Z

    .line 86
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 89
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 92
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$1;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoDataAdapterListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    .line 154
    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    .line 156
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$2;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    .line 350
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$3;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    .line 363
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$4;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    .line 393
    new-instance v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$5;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/FilmStripView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V

    return-void
.end method

.method static synthetic access$102(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 58
    iput p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaSet;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/glcore/GlRootView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V

    return-void
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
    .param p1, "x1"    # Z

    .prologue
    .line 58
    iput-boolean p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsInteracting:Z

    return p1
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    return v0
.end method

.method static synthetic access$2600()I
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->DOUBLE_TAP_TIMEOUT:I

    return v0
.end method

.method static synthetic access$2700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    return v0
.end method

.method static synthetic access$2800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
    .param p1, "x1"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V

    return-void
.end method

.method static synthetic access$3000(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;
    .param p1, "x1"    # I

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V

    return-void
.end method

.method static synthetic access$3100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/samsung/gallery/core/GalleryFacade;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z

    return v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->finishDetailView()V

    return-void
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/ui/PhotoView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;)Lcom/sec/android/gallery3d/data/MediaItem;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    return-object v0
.end method

.method private enableDnieMode(ZZ)V
    .locals 6
    .param p1, "isEnable"    # Z
    .param p2, "delayed"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 752
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 753
    .local v0, "handler":Landroid/os/Handler;
    const/4 v2, 0x2

    new-array v1, v2, [Ljava/lang/Object;

    if-eqz p1, :cond_0

    move v2, v3

    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v1, v3

    .line 757
    .local v1, "params":[Ljava/lang/Object;
    if-eqz p2, :cond_1

    .line 758
    new-instance v2, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;

    invoke-direct {v2, p0, p1, v1}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$7;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;ZLjava/lang/Object;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 769
    :goto_1
    return-void

    .end local v1    # "params":[Ljava/lang/Object;
    :cond_0
    move v2, v4

    .line 753
    goto :goto_0

    .line 766
    .restart local v1    # "params":[Ljava/lang/Object;
    :cond_1
    sget-object v2, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "enableDnieMode ["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] with no delay"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 767
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v3, "SET_DNIE_MODE"

    invoke-virtual {v2, v3, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private finishDetailView()V
    .locals 1

    .prologue
    .line 748
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 749
    return-void
.end method

.method private initFilmStripView()V
    .locals 7

    .prologue
    .line 417
    new-instance v0, Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

    iget v4, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/gallery3d/ui/FilmStripView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/util/KeyBoardManager;IZLcom/sec/android/gallery3d/app/AlbumReloader;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    .line 419
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripListener:Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setListener(Lcom/sec/android/gallery3d/ui/FilmStripView$Listener;)V

    .line 420
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 421
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 422
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setStartIndex(I)V

    .line 424
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 425
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 426
    return-void
.end method

.method private postGallery(Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;)V
    .locals 3
    .param p1, "cmd"    # Lcom/sec/samsung/gallery/controller/PostGalleryCmd$PostGalleryCmdType;

    .prologue
    .line 894
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    .line 897
    .local v0, "params":[Ljava/lang/Object;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    const-string v2, "POST_GALLERY"

    invoke-virtual {v1, v2, v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 898
    return-void
.end method

.method private refreshHidingMessage(I)V
    .locals 4
    .param p1, "timeout"    # I

    .prologue
    const/4 v1, 0x1

    .line 686
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 688
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionTutorialNone(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 697
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_2

    .line 691
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->isSelectionMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 694
    :cond_2
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsMenuVisible:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsInteracting:Z

    if-eqz v0, :cond_4

    :cond_3
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 695
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private setFullScreenMode(Z)V
    .locals 4
    .param p1, "isFullMode"    # Z

    .prologue
    .line 657
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-eqz v2, :cond_1

    .line 676
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    sget-boolean v2, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v2, :cond_2

    .line 661
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v2}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionTutorialNone(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 664
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getMultiWindow()Lcom/sec/android/gallery3d/app/GalleryMultiWindow;

    move-result-object v0

    .line 665
    .local v0, "mw":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/GalleryMultiWindow;->isFullScreenMode()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 669
    .end local v0    # "mw":Lcom/sec/android/gallery3d/app/GalleryMultiWindow;
    :cond_2
    const-class v2, Landroid/view/View;

    const-string v3, "SYSTEM_UI_FLAG_REMOVE_NAVIGATION"

    invoke-static {v2, v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getDeclaredIntField(Ljava/lang/Class;Ljava/lang/String;)I

    move-result v1

    .line 671
    .local v1, "systemUiFlagRemoveNavigation":I
    if-eqz p1, :cond_3

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 672
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0

    .line 674
    :cond_3
    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/16 v3, 0x200

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    goto :goto_0
.end method

.method private showBarsForAShortTime()V
    .locals 1

    .prologue
    .line 700
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBars()V

    .line 701
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-eqz v0, :cond_0

    .line 702
    const/16 v0, 0x157c

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V

    .line 706
    :goto_0
    return-void

    .line 704
    :cond_0
    const/16 v0, 0xdac

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V

    goto :goto_0
.end method

.method private updateCurrentPhoto(Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 2
    .param p1, "photo"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 614
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/ui/FilmStripView;->setFocusIndex(I)V

    .line 618
    :cond_0
    if-nez p1, :cond_1

    .line 623
    :goto_0
    return-void

    .line 621
    :cond_1
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentPhoto:Lcom/sec/android/gallery3d/data/MediaItem;

    .line 622
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->updateMenuOperations()V

    goto :goto_0
.end method

.method private updateMenuOperations()V
    .locals 1

    .prologue
    .line 807
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 808
    return-void
.end method


# virtual methods
.method public getSelectonModeBarHeight()I
    .locals 1

    .prologue
    .line 890
    const/4 v0, 0x0

    return v0
.end method

.method public hideBars()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 640
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mDisableAutoHideBars:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/AbstractActionBarView;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ActionBar;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isExploreByTouchEnabled(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 654
    :cond_1
    :goto_0
    return-void

    .line 646
    :cond_2
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    .line 647
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 648
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    if-nez v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setFullScreenMode(Z)V

    .line 649
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->hide()V

    .line 651
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->hide()V

    goto :goto_0
.end method

.method public isLongPressFinished()Z
    .locals 1

    .prologue
    .line 865
    const/4 v0, 0x0

    return v0
.end method

.method public onActionBarAllowed(Z)V
    .locals 0
    .param p1, "allowed"    # Z

    .prologue
    .line 829
    return-void
.end method

.method public onActionBarWanted()V
    .locals 0

    .prologue
    .line 833
    return-void
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 741
    sget-object v0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->TAG:Ljava/lang/String;

    const-string v1, "onBackPressed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 742
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setFullScreenMode(Z)V

    .line 743
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->onBackPressed()V

    .line 744
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->finishDetailView()V

    .line 745
    return-void
.end method

.method public onCommitDeleteImage()V
    .locals 0

    .prologue
    .line 849
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "configuration"    # Landroid/content/res/Configuration;

    .prologue
    .line 773
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 774
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, p1}, Lcom/sec/android/gallery3d/ui/PhotoView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 776
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getScreenSize(Landroid/content/Context;)Landroid/graphics/Point;

    move-result-object v0

    .line 777
    .local v0, "size":Landroid/graphics/Point;
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v3, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/gallery3d/ui/PhotoView;->setScreenSize(II)V

    .line 779
    .end local v0    # "size":Landroid/graphics/Point;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 780
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 11
    .param p1, "data"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 430
    const-string v1, "Gallery_Performance"

    const-string v2, "HelpDetailViewState onCreate Start"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    .line 432
    new-instance v1, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 433
    new-instance v1, Lcom/sec/android/gallery3d/util/KeyBoardManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2, v7}, Lcom/sec/android/gallery3d/util/KeyBoardManager;-><init>(Lcom/sec/android/gallery3d/app/GalleryContext;Z)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mKeyBoardManager:Lcom/sec/android/gallery3d/util/KeyBoardManager;

    .line 434
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGlRootView()Lcom/sec/android/gallery3d/glcore/GlRootView;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 435
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->clearCanvas()V

    .line 437
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    .line 438
    invoke-direct {p0, v7}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setFullScreenMode(Z)V

    .line 439
    if-eqz p1, :cond_0

    .line 440
    const-string v1, "Help"

    invoke-virtual {p1, v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    .line 442
    :cond_0
    new-instance v1, Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    .line 443
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/ui/PhotoView;->setListener(Lcom/sec/android/gallery3d/ui/PhotoView$Listener;)V

    .line 444
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mUserInteractionListener:Lcom/sec/android/gallery3d/ui/UserInteractionListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setUserInteractionListener(Lcom/sec/android/gallery3d/ui/UserInteractionListener;)V

    .line 445
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mOnSystemGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    .line 447
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/GLView;->addComponent(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 449
    if-eqz p1, :cond_4

    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 450
    .local v10, "setPathString":Ljava/lang/String;
    :goto_0
    if-eqz p1, :cond_1

    const-string v1, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/gallery3d/data/Path;->fromString(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/Path;

    move-result-object v4

    .line 451
    .local v4, "itemPath":Lcom/sec/android/gallery3d/data/Path;
    :cond_1
    if-eqz v10, :cond_2

    .line 452
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v1, v10}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaObject(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v1

    check-cast v1, Lcom/sec/android/gallery3d/data/MediaSet;

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    .line 455
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v1, :cond_3

    .line 456
    if-eqz p1, :cond_5

    const-string v1, "KEY_ITEM_POSITION"

    invoke-virtual {p1, v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    :goto_1
    iput v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    .line 457
    new-instance v1, Lcom/sec/android/gallery3d/app/AlbumReloader;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    invoke-direct {v1, v2, v3}, Lcom/sec/android/gallery3d/app/AlbumReloader;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/data/MediaSet;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    .line 458
    new-instance v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v3, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

    iget v5, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mCurrentIndex:I

    const/4 v6, -0x1

    iget-object v9, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    move v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Lcom/sec/android/gallery3d/ui/PhotoView;Lcom/sec/android/gallery3d/data/MediaSet;Lcom/sec/android/gallery3d/data/Path;IIZZLcom/sec/android/gallery3d/app/AlbumReloader;)V

    .line 460
    .local v0, "adapter":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->initFilmStripView()V

    .line 461
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoDataAdapterListener:Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;->setDataListener(Lcom/sec/android/gallery3d/app/PhotoDataAdapter$DataListener;)V

    .line 462
    iput-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    .line 463
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setModel(Lcom/sec/android/gallery3d/ui/PhotoView$Model;)V

    .line 466
    .end local v0    # "adapter":Lcom/sec/android/gallery3d/app/PhotoDataAdapter;
    :cond_3
    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGLRoot()Lcom/sec/android/gallery3d/ui/GLRoot;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState$6;-><init>(Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;Lcom/sec/android/gallery3d/ui/GLRoot;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    .line 521
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getAnimationInterface()Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    .line 522
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setHelpMode(I)V

    .line 523
    const-string v1, "Gallery_Performance"

    const-string v2, "HelpDetailViewState onCreate End"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 524
    return-void

    .end local v4    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .end local v10    # "setPathString":Ljava/lang/String;
    :cond_4
    move-object v10, v4

    .line 449
    goto :goto_0

    .restart local v4    # "itemPath":Lcom/sec/android/gallery3d/data/Path;
    .restart local v10    # "setPathString":Ljava/lang/String;
    :cond_5
    move v1, v7

    .line 456
    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 784
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 785
    return-void
.end method

.method public onCurrentImageUpdated()V
    .locals 0

    .prologue
    .line 837
    return-void
.end method

.method public onDeleteImage(Lcom/sec/android/gallery3d/data/Path;I)V
    .locals 0
    .param p1, "path"    # Lcom/sec/android/gallery3d/data/Path;
    .param p2, "offset"    # I

    .prologue
    .line 841
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 594
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onDestroy Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_0

    .line 596
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->destroy()V

    .line 599
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 600
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 602
    :cond_1
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseHWKey:Z

    if-nez v0, :cond_2

    .line 603
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setSystemUiVisibility(I)V

    .line 606
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v0, :cond_3

    .line 607
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->destroy()V

    .line 609
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mGalleryFacade:Lcom/sec/samsung/gallery/core/GalleryFacade;

    const-string v1, "PHOTO_PAGE"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 610
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onDestroy End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 611
    return-void
.end method

.method public onDown()V
    .locals 0

    .prologue
    .line 874
    return-void
.end method

.method public onFilmModeChanged(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 853
    return-void
.end method

.method public onFullScreenChanged(Z)V
    .locals 0
    .param p1, "full"    # Z

    .prologue
    .line 825
    return-void
.end method

.method public onLongPress()V
    .locals 0

    .prologue
    .line 870
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "menuItem"    # Landroid/view/MenuItem;

    .prologue
    .line 801
    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHelpMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 802
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v1, 0x7f0e02da

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 804
    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 568
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onPause Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 569
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onPause()V

    .line 570
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z

    .line 572
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryApp;

    invoke-interface {v0, v2}, Lcom/sec/android/gallery3d/app/GalleryApp;->setIsPhotoPage(Z)V

    .line 573
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    if-eqz v0, :cond_0

    .line 574
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->pause()V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 577
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->pause()V

    .line 580
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->pause()V

    .line 581
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->pause()V

    .line 582
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 583
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 585
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->unsetPhotoView()V

    .line 586
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->unsetPhotoDataAdapter()V

    .line 588
    invoke-direct {p0, v2, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->enableDnieMode(ZZ)V

    .line 589
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onPause End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 590
    return-void
.end method

.method public onPictureCenter(Z)V
    .locals 0
    .param p1, "isCamera"    # Z

    .prologue
    .line 857
    return-void
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 789
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-nez v1, :cond_1

    .line 797
    :cond_0
    :goto_0
    return-void

    .line 792
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->getCurrentMediaItem()Lcom/sec/android/gallery3d/data/MediaItem;

    move-result-object v0

    .line 793
    .local v0, "current":Lcom/sec/android/gallery3d/data/MediaItem;
    if-eqz v0, :cond_0

    .line 796
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 0

    .prologue
    .line 816
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 528
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onResume Start"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    invoke-super {p0}, Lcom/sec/android/gallery3d/app/ActivityState;->onResume()V

    .line 530
    iput-boolean v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z

    .line 531
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;

    invoke-virtual {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setContentPane(Lcom/sec/android/gallery3d/ui/GLView;)V

    .line 533
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    if-eqz v0, :cond_0

    .line 534
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;->resume()V

    .line 536
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_1

    .line 537
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->resume()V

    .line 538
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    .line 540
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    if-eqz v0, :cond_2

    .line 541
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAlbumReloader:Lcom/sec/android/gallery3d/app/AlbumReloader;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AlbumReloader;->resume()V

    .line 544
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/PhotoView;->resume()V

    .line 545
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 547
    invoke-direct {p0, v2, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->enableDnieMode(ZZ)V

    .line 548
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setPhotoView(Lcom/sec/android/gallery3d/ui/PhotoView;)V

    .line 549
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    instance-of v0, v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    if-eqz v0, :cond_4

    .line 550
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mModel:Lcom/sec/samsung/gallery/view/detailview/DetailViewState$Model;

    check-cast v0, Lcom/sec/android/gallery3d/app/PhotoDataAdapter;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setPhotoDataAdapter(Lcom/sec/android/gallery3d/app/PhotoDataAdapter;)V

    .line 551
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setMultiViewState(Z)V

    .line 556
    :goto_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_3

    .line 557
    const/16 v0, 0xdac

    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->refreshHidingMessage(I)V

    .line 560
    :cond_3
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v1, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;

    iget-object v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/view/help/HelpDetailActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 561
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBars()V

    .line 563
    const-string v0, "Gallery_Performance"

    const-string v1, "HelpDetailViewState onResume End"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 564
    return-void

    .line 553
    :cond_4
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mAnimationInterface:Lcom/sec/samsung/gallery/decoder/AnimationInterface;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/decoder/AnimationInterface;->setMultiViewState(Z)V

    goto :goto_0
.end method

.method public onScaleMin()Z
    .locals 1

    .prologue
    .line 878
    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 820
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->onUserInteractionTap()V

    .line 821
    return-void
.end method

.method public onSizeChanged()V
    .locals 0

    .prologue
    .line 885
    return-void
.end method

.method public onUndoBarVisibilityChanged(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 861
    return-void
.end method

.method public onUndoDeleteImage()V
    .locals 0

    .prologue
    .line 845
    return-void
.end method

.method public onUserInteractionTap()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 709
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/util/MotionUtils;->isMotionTutorialNone(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 721
    :cond_0
    :goto_0
    return-void

    .line 712
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 713
    sget-boolean v0, Lcom/sec/android/gallery3d/util/GalleryFeature;->mIsSPCEnabled:Z

    if-nez v0, :cond_2

    .line 714
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 715
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    sget v1, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->DOUBLE_TAP_TIMEOUT:I

    int-to-long v2, v1

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 718
    :cond_2
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setUserInteractionShowMode()V

    goto :goto_0
.end method

.method public removeHidingMessage()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 679
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 681
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xdac

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 683
    :cond_0
    return-void
.end method

.method public setUserInteractionShowMode()V
    .locals 2

    .prologue
    .line 724
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mIsActive:Z

    if-nez v0, :cond_1

    .line 737
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    if-eqz v0, :cond_2

    .line 729
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mDisableAutoHideBars:Z

    .line 730
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->hideBars()V

    .line 731
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 732
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0

    .line 735
    :cond_2
    invoke-direct {p0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->showBarsForAShortTime()V

    goto :goto_0
.end method

.method public showBars()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 626
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    if-eqz v1, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 629
    :cond_1
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    .line 630
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mPhotoView:Lcom/sec/android/gallery3d/ui/PhotoView;

    iget-boolean v2, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/ui/PhotoView;->setShowBarState(Z)V

    .line 631
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mShowBars:Z

    if-nez v1, :cond_2

    :goto_1
    invoke-direct {p0, v0}, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->setFullScreenMode(Z)V

    .line 632
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->show()V

    .line 634
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    if-eqz v0, :cond_0

    .line 635
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/help/HelpDetailViewState;->mFilmStripView:Lcom/sec/android/gallery3d/ui/FilmStripView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/FilmStripView;->show()V

    goto :goto_0

    .line 631
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 812
    return-void
.end method
