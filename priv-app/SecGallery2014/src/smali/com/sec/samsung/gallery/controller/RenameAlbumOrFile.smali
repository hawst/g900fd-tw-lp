.class public Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "RenameAlbumOrFile.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private getCurrentSelectedItem()Lcom/sec/android/gallery3d/data/MediaObject;
    .locals 3

    .prologue
    .line 75
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v1

    .line 76
    .local v1, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v1}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v0

    .line 77
    .local v0, "selectedItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/data/MediaObject;

    return-object v2
.end method

.method private getExtension(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "srcFilePath"    # Ljava/lang/String;

    .prologue
    .line 129
    const/16 v1, 0x2e

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 130
    .local v0, "extension":Ljava/lang/String;
    return-object v0
.end method

.method private getMediaItemProviderOperation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/content/ContentProviderOperation;
    .locals 4
    .param p1, "newFileFullPath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 179
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 181
    .local v0, "args":Landroid/content/ContentValues;
    const/4 v2, 0x0

    const-string v3, "."

    invoke-virtual {p2, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {p2, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "fileNameWithNoExtenison":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 183
    const-string v2, "_data"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 184
    const-string v2, "_display_name"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    const-string/jumbo v2, "title"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :goto_0
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    return-object v2

    .line 187
    :cond_0
    const-string v2, "_data"

    invoke-virtual {v0, v2, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    const-string v2, "_display_name"

    invoke-virtual {v0, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    const-string/jumbo v2, "title"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private getSubItemList(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;
    .locals 4
    .param p1, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            ")",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaSet;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/Path;->toString()Ljava/lang/String;

    move-result-object v2

    .line 82
    .local v2, "setPath":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    check-cast v3, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    .line 83
    .local v0, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaItemList(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 85
    .local v1, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    return-object v1
.end method

.method private handleException(Ljava/lang/Exception;I)V
    .locals 1
    .param p1, "e"    # Ljava/lang/Exception;
    .param p2, "resId"    # I

    .prologue
    .line 236
    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    .line 237
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/sec/android/gallery3d/common/Utils;->showToast(Landroid/content/Context;I)V

    .line 238
    return-void
.end method

.method private renameAlbum(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;)V
    .locals 11
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .param p2, "newAlbumName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 89
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getSubItemList(Lcom/sec/android/gallery3d/data/MediaSet;)Ljava/util/List;

    move-result-object v3

    .line 93
    .local v3, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    invoke-interface {v3, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/gallery3d/data/MediaItem;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v7

    .line 95
    .local v7, "srcFilePath":Ljava/lang/String;
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    if-eqz v7, :cond_0

    .line 98
    const/4 v6, 0x0

    .line 99
    .local v6, "srcAlbumPath":Ljava/lang/String;
    const/4 v1, 0x0

    .line 100
    .local v1, "dstAlbumPath":Ljava/lang/String;
    const/4 v5, 0x0

    .line 101
    .local v5, "srcAlbum":Ljava/io/File;
    const/16 v8, 0x2f

    invoke-virtual {v7, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v4

    .line 102
    .local v4, "pos":I
    if-ltz v4, :cond_2

    .line 103
    invoke-virtual {v7, v9, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 104
    new-instance v5, Ljava/io/File;

    .end local v5    # "srcAlbum":Ljava/io/File;
    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 106
    .restart local v5    # "srcAlbum":Ljava/io/File;
    :cond_2
    if-eqz v5, :cond_0

    .line 107
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "/"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 108
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 109
    .local v0, "dstAlbum":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 110
    new-instance v8, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "dst album already exists : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/sec/samsung/gallery/util/DestAlreadyExistException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 112
    :cond_3
    invoke-virtual {v5, v0}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v2

    .line 113
    .local v2, "isSuccess":Z
    if-nez v2, :cond_4

    .line 114
    new-instance v8, Ljava/io/IOException;

    const-string v9, "renameAlbum() failed"

    invoke-direct {v8, v9}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 116
    :cond_4
    invoke-direct {p0, v1, p2, p1, v3}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->updateAlbumDB(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V

    goto :goto_0
.end method

.method private renameFile(Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;)V
    .locals 4
    .param p1, "media"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "newFileName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 121
    invoke-virtual {p1}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v1

    .line 122
    .local v1, "srcFilePath":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "dstFilePath":Ljava/lang/String;
    new-instance v2, Lcom/sec/samsung/gallery/util/FileUtil;

    iget-object v3, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/util/FileUtil;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, v0}, Lcom/sec/samsung/gallery/util/FileUtil;->rename(Ljava/lang/String;Ljava/lang/String;)V

    .line 125
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, v1}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2, p1}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->updateItemDB(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 126
    return-void
.end method

.method private updateAlbumDB(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaSet;Ljava/util/List;)V
    .locals 22
    .param p1, "newFileFullPath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;
    .param p3, "mediaSet"    # Lcom/sec/android/gallery3d/data/MediaSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/sec/android/gallery3d/data/MediaSet;",
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaItem;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 135
    .local p4, "itemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    const-string v17, "external"

    invoke-static/range {v17 .. v17}, Landroid/provider/MediaStore$Files;->getContentUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 137
    .local v5, "baseUri":Landroid/net/Uri;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    .line 138
    .local v6, "cr":Landroid/content/ContentResolver;
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 140
    .local v4, "args":Landroid/content/ContentValues;
    const-string v17, "_data"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v17, "_display_name"

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/16 v17, 0x0

    const-string v18, "/"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v18

    move-object/from16 v0, p1

    move/from16 v1, v17

    move/from16 v2, v18

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v16

    .line 144
    .local v16, "parentPath":Ljava/lang/String;
    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v12, v0, [Ljava/lang/String;

    const/16 v17, 0x0

    aput-object v16, v12, v17

    .line 148
    .local v12, "mSelectionArgs":[Ljava/lang/String;
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 150
    .local v15, "operationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v17

    const-string v18, "_data = ?"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v12}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v14

    .line 153
    .local v14, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual {v15, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 154
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 155
    .local v11, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v9

    .line 156
    .local v9, "fileOldPath":Ljava/lang/String;
    const-string v17, "/"

    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v17

    add-int/lit8 v17, v17, 0x1

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v18

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v9, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v8

    .line 158
    .local v8, "fileOldName":Ljava/lang/String;
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-direct/range {v17 .. v17}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    const-string v18, "/"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 159
    .local v13, "newFilePath":Ljava/lang/String;
    move-object/from16 v0, p0

    invoke-direct {v0, v13, v8, v11}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getMediaItemProviderOperation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/content/ContentProviderOperation;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 163
    .end local v8    # "fileOldName":Ljava/lang/String;
    .end local v9    # "fileOldPath":Ljava/lang/String;
    .end local v11    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v13    # "newFilePath":Ljava/lang/String;
    :cond_0
    :try_start_0
    invoke-virtual {v5}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v6, v0, v15}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 164
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    new-instance v18, Landroid/content/Intent;

    const-string v19, "android.intent.action.MEDIA_SCAN"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "file://"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    invoke-direct/range {v18 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual/range {v17 .. v18}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/util/GalleryUtils;->scanExternalStorage(Landroid/content/Context;)V

    .line 170
    sget-boolean v17, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v17, :cond_1

    .line 171
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    move-object/from16 v17, v0

    check-cast v17, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual/range {v17 .. v17}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v17

    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/android/gallery3d/data/EventAlbumManager;

    move-result-object v3

    .line 172
    .local v3, "EventAlbumMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    const/16 v17, 0x1

    const-string v18, "images_event_album"

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 173
    const/16 v17, 0x0

    const-string/jumbo v18, "video_event_album"

    move/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v3, v0, v1}, Lcom/sec/android/gallery3d/data/EventAlbumManager;->updateLocalDB(ZLjava/lang/String;)Ljava/util/List;

    .line 175
    .end local v3    # "EventAlbumMgr":Lcom/sec/android/gallery3d/data/EventAlbumManager;
    :cond_1
    return-void

    .line 165
    :catch_0
    move-exception v7

    .line 166
    .local v7, "e":Ljava/lang/Exception;
    invoke-virtual {v7}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method private updateEventItem(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 6
    .param p1, "newFileFullPath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 214
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseEventView:Z

    if-eqz v4, :cond_0

    .line 215
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 218
    .local v3, "values":Landroid/content/ContentValues;
    const/4 v4, 0x0

    const-string v5, "."

    invoke-virtual {p2, v5}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 219
    .local v0, "fileNameWithNoExtenison":Ljava/lang/String;
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getMediaType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 220
    const-string v2, "images_event_album"

    .line 221
    .local v2, "table":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 222
    .local v1, "selection":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 223
    const-string/jumbo v4, "title"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getGalleryApplication()Lcom/sec/android/gallery3d/app/GalleryApp;

    move-result-object v4

    invoke-static {v4}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->getInstance(Lcom/sec/android/gallery3d/app/GalleryApp;)Lcom/sec/samsung/gallery/util/LocalDatabaseManager;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v3, v1, v5}, Lcom/sec/samsung/gallery/util/LocalDatabaseManager;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 233
    .end local v0    # "fileNameWithNoExtenison":Ljava/lang/String;
    .end local v1    # "selection":Ljava/lang/String;
    .end local v2    # "table":Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    return-void

    .line 225
    .restart local v0    # "fileNameWithNoExtenison":Ljava/lang/String;
    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_1
    const-string/jumbo v2, "video_event_album"

    .line 226
    .restart local v2    # "table":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getItemId()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 227
    .restart local v1    # "selection":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {v3, v4, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const-string/jumbo v4, "title"

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateItemDB(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)V
    .locals 9
    .param p1, "newFileFullPath"    # Ljava/lang/String;
    .param p2, "newFileName"    # Ljava/lang/String;
    .param p3, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 197
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 198
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v3, "operationList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentProviderOperation;>;"
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getMediaItemProviderOperation(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 201
    .local v2, "operation":Landroid/content/ContentProviderOperation;
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 204
    :try_start_0
    invoke-virtual {p3}, Lcom/sec/android/gallery3d/data/MediaItem;->getContentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4, v3}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    .line 205
    invoke-direct {p0, p1, p2, p3}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->updateEventItem(Ljava/lang/String;Ljava/lang/String;Lcom/sec/android/gallery3d/data/MediaItem;)V

    .line 206
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MEDIA_SCAN"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "file://"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v4, v5}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    :goto_0
    return-void

    .line 207
    :catch_0
    move-exception v1

    .line 208
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 209
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/gallery3d/util/GalleryUtils;->scanExternalStorage(Landroid/content/Context;)V

    goto :goto_0
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 8
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const v7, 0x7f0e010f

    const/4 v5, 0x0

    .line 51
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/Object;

    move-object v3, v4

    check-cast v3, [Ljava/lang/Object;

    .line 52
    .local v3, "params":[Ljava/lang/Object;
    aget-object v4, v3, v5

    check-cast v4, Landroid/content/Context;

    iput-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    .line 53
    const/4 v4, 0x1

    aget-object v2, v3, v4

    check-cast v2, Ljava/lang/String;

    .line 56
    .local v2, "newName":Ljava/lang/String;
    :try_start_0
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->getCurrentSelectedItem()Lcom/sec/android/gallery3d/data/MediaObject;

    move-result-object v0

    .line 58
    .local v0, "currentSelectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    instance-of v4, v0, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v4, :cond_0

    .line 59
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaSet;

    .end local v0    # "currentSelectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->renameAlbum(Lcom/sec/android/gallery3d/data/MediaSet;Ljava/lang/String;)V

    .line 64
    :goto_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->mContext:Landroid/content/Context;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "EXIT_SELECTION_MODE"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    :goto_1
    return-void

    .line 61
    .restart local v0    # "currentSelectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_0
    check-cast v0, Lcom/sec/android/gallery3d/data/MediaItem;

    .end local v0    # "currentSelectedItem":Lcom/sec/android/gallery3d/data/MediaObject;
    invoke-direct {p0, v0, v2}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->renameFile(Lcom/sec/android/gallery3d/data/MediaItem;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/sec/samsung/gallery/util/DestAlreadyExistException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 65
    :catch_0
    move-exception v1

    .line 66
    .local v1, "e":Lcom/sec/samsung/gallery/util/DestAlreadyExistException;
    const v4, 0x7f0e0110

    invoke-direct {p0, v1, v4}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->handleException(Ljava/lang/Exception;I)V

    goto :goto_1

    .line 67
    .end local v1    # "e":Lcom/sec/samsung/gallery/util/DestAlreadyExistException;
    :catch_1
    move-exception v1

    .line 68
    .local v1, "e":Ljava/io/IOException;
    invoke-direct {p0, v1, v7}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->handleException(Ljava/lang/Exception;I)V

    goto :goto_1

    .line 69
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 70
    .local v1, "e":Ljava/lang/StringIndexOutOfBoundsException;
    invoke-direct {p0, v1, v7}, Lcom/sec/samsung/gallery/controller/RenameAlbumOrFile;->handleException(Ljava/lang/Exception;I)V

    goto :goto_1
.end method
