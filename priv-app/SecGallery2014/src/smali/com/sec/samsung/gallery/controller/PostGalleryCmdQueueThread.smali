.class Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;
.super Ljava/lang/Thread;
.source "PostGalleryCmdQueueThread.java"


# instance fields
.field private mActive:Z

.field private final mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

.field private final mRunnableList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V
    .locals 1
    .param p1, "activity"    # Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .prologue
    .line 16
    const-class v0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 12
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mRunnableList:Ljava/util/ArrayList;

    .line 13
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mActive:Z

    .line 17
    iput-object p1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    .line 18
    return-void
.end method


# virtual methods
.method public addRunnable(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "runnable"    # Ljava/lang/Runnable;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mRunnableList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 22
    return-void
.end method

.method public run()V
    .locals 3

    .prologue
    .line 36
    :goto_0
    iget-boolean v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mActive:Z

    if-eqz v1, :cond_1

    .line 37
    monitor-enter p0

    .line 38
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mRunnableList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    invoke-static {p0}, Lcom/sec/android/gallery3d/common/Utils;->waitWithoutInterrupt(Ljava/lang/Object;)V

    .line 40
    monitor-exit p0

    goto :goto_0

    .line 42
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 44
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mRunnableList:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 46
    .local v0, "runnable":Ljava/lang/Runnable;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 48
    .end local v0    # "runnable":Ljava/lang/Runnable;
    :cond_1
    return-void
.end method

.method public declared-synchronized startAllRunnablesInQueue()V
    .locals 1

    .prologue
    .line 25
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 26
    monitor-exit p0

    return-void

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized terminate()V
    .locals 1

    .prologue
    .line 30
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/sec/samsung/gallery/controller/PostGalleryCmdQueueThread;->mActive:Z

    .line 31
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 32
    monitor-exit p0

    return-void

    .line 30
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
