.class Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;
.super Ljava/lang/Object;
.source "AlbumViewState.java"

# interfaces
.implements Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnStatusChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->onViewInitialize(Lcom/sec/android/gallery3d/glcore/GlLayer;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)V
    .locals 0

    .prologue
    .line 584
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onStatusChange(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;III)V
    .locals 4
    .param p1, "parentView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;
    .param p2, "status"    # I
    .param p3, "parm1"    # I
    .param p4, "parm2"    # I

    .prologue
    .line 588
    packed-switch p2, :pswitch_data_0

    .line 600
    :goto_0
    :pswitch_0
    return-void

    .line 593
    :pswitch_1
    const-string v1, "AlbumViewState"

    const-string/jumbo v2, "startReorder STATUS_REORDER_DONE"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    shr-int/lit8 v2, p3, 0x10

    const v3, 0xffff

    and-int/2addr v3, p3

    # invokes: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->handleReorderAlbums(II)Z
    invoke-static {v1, v2, v3}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4700(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;II)Z

    move-result v0

    .line 595
    .local v0, "isChanged":Z
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4800(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    instance-of v1, v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    if-eqz v1, :cond_0

    .line 596
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$4900(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->getActionBarView()Lcom/sec/samsung/gallery/view/AbstractActionBarView;

    move-result-object v1

    check-cast v1, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/view/albumview/AlbumActionBarForReorder;->setIsOrderChanged(Z)V

    .line 597
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState$8;->this$0:Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    # getter for: Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;
    invoke-static {v1}, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;->access$5000(Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->updateActionbarButton()V

    goto :goto_0

    .line 588
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
