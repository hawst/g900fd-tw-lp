.class Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;
.super Ljava/lang/Object;
.source "SlideshowDefaultMusicDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)V
    .locals 0

    .prologue
    .line 196
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 200
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/app/AlertDialog;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v1

    .line 202
    .local v1, "mListView":Landroid/widget/ListView;
    if-eqz v1, :cond_0

    .line 203
    :try_start_0
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    iget-object v5, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mMusicListDialog:Landroid/app/AlertDialog;
    invoke-static {v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$200(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/app/AlertDialog;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/ListView;->getCheckedItemPosition()I

    move-result v5

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->index:I
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$602(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;I)I

    .line 204
    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;

    .line 205
    .local v2, "myAdpater":Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->index:I
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$600(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;

    .line 206
    .local v3, "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;->getUri()Landroid/net/Uri;

    move-result-object v5

    # setter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;
    invoke-static {v4, v5}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$402(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 211
    .end local v2    # "myAdpater":Lcom/sec/samsung/gallery/view/slideshowview/MusicListAdapter;
    .end local v3    # "selectedItem":Lcom/sec/samsung/gallery/view/slideshowview/MusicItemBuilder$MusicItem;
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 212
    iget-object v4, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$300(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/content/Context;

    move-result-object v4

    check-cast v4, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v4}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v4

    const-string v5, "SLIDESHOW_SETUP_MUSIC"

    iget-object v6, p0, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog$3;->this$0:Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;

    # getter for: Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->mSelectedMusicUri:Landroid/net/Uri;
    invoke-static {v6}, Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;->access$400(Lcom/sec/samsung/gallery/view/slideshowsetting/SlideshowDefaultMusicDialog;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Lcom/sec/samsung/gallery/core/GalleryFacade;->sendNotification(Ljava/lang/String;Ljava/lang/Object;)V

    .line 214
    return-void

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
