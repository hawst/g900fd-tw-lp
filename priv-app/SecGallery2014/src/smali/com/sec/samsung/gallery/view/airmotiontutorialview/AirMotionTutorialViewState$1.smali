.class Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;
.super Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;
.source "AirMotionTutorialViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)V
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlAirMotionDetector$AirMotionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onNext()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$100(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->moveImage(Z)V

    .line 61
    :cond_1
    return-void
.end method

.method public onPrevious()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlRootView;->hasWindowFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 49
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mPopupView:Landroid/view/View;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$000(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState$1;->this$0:Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;

    # getter for: Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->mView:Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;->access$100(Lcom/sec/samsung/gallery/view/airmotiontutorialview/AirMotionTutorialViewState;)Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/GlAirMotionTutorialView;->moveImage(Z)V

    .line 52
    :cond_1
    return-void
.end method
