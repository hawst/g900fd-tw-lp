.class Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;
.super Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;
.source "Clusterkraf.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ShowAllClustersClusteringTaskHost"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$BaseClusteringTaskHost;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$1;

    .prologue
    .line 561
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    return-void
.end method


# virtual methods
.method protected onCurrentClustersSet(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;)V
    .locals 2
    .param p1, "result"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusteringTask$Result;

    .prologue
    .line 565
    # getter for: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->options:Lcom/sec/samsung/gallery/mapfragment/clustering/Options;
    invoke-static {}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$600()Lcom/sec/samsung/gallery/mapfragment/clustering/Options;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Options;->getProcessingListener()Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;

    move-result-object v0

    .line 566
    .local v0, "processingListener":Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;
    if-eqz v0, :cond_0

    .line 567
    invoke-interface {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ProcessingListener;->onClusteringFinished()V

    .line 569
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf$ShowAllClustersClusteringTaskHost;->this$0:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    # invokes: Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->showAllClusters()V
    invoke-static {v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->access$2000(Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)V

    .line 570
    return-void
.end method
