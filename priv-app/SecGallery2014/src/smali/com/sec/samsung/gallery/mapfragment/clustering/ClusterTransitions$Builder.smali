.class Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;
.super Ljava/lang/Object;
.source "ClusterTransitions.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Builder"
.end annotation


# instance fields
.field private final animatedTransitions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;",
            ">;"
        }
    .end annotation
.end field

.field private final previousClusterPoints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field

.field private final projectionRef:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/google/android/gms/maps/Projection;",
            ">;"
        }
    .end annotation
.end field

.field private final stationaryTransitions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/google/android/gms/maps/Projection;Ljava/util/ArrayList;)V
    .locals 1
    .param p1, "projection"    # Lcom/google/android/gms/maps/Projection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/maps/Projection;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "previousClusterPoints":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->animatedTransitions:Ljava/util/ArrayList;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->stationaryTransitions:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->projectionRef:Ljava/lang/ref/WeakReference;

    .line 28
    iput-object p2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->previousClusterPoints:Ljava/util/ArrayList;

    .line 29
    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->animatedTransitions:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;

    .prologue
    .line 18
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->stationaryTransitions:Ljava/util/ArrayList;

    return-object v0
.end method

.method private getTransition(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    .locals 3
    .param p1, "relevantInputPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .prologue
    .line 58
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->animatedTransitions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    .line 59
    .local v0, "at":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->destinationContains(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->originContains(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    .end local v0    # "at":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method add(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V
    .locals 8
    .param p1, "currentClusterPoint"    # Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .prologue
    .line 32
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->projectionRef:Ljava/lang/ref/WeakReference;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->projectionRef:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/maps/Projection;

    move-object v5, v7

    .line 33
    .local v5, "projection":Lcom/google/android/gms/maps/Projection;
    :goto_0
    if-eqz p1, :cond_5

    if-eqz v5, :cond_5

    .line 34
    const/4 v0, 0x0

    .line 35
    .local v0, "animated":Z
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->previousClusterPoints:Ljava/util/ArrayList;

    if-eqz v7, :cond_4

    .line 36
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->previousClusterPoints:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;

    .line 37
    .local v3, "previousClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->getPointsInCluster()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;

    .line 38
    .local v4, "previousInputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;->containsInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 39
    invoke-direct {p0, v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->getTransition(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    move-result-object v6

    .line 40
    .local v6, "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    if-eqz v6, :cond_3

    .line 41
    invoke-virtual {v6, v4}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;->addOriginClusterRelevantInputPoint(Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;)V

    goto :goto_1

    .line 32
    .end local v0    # "animated":Z
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "previousClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v4    # "previousInputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .end local v5    # "projection":Lcom/google/android/gms/maps/Projection;
    .end local v6    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    :cond_2
    const/4 v5, 0x0

    goto :goto_0

    .line 43
    .restart local v0    # "animated":Z
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "previousClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .restart local v4    # "previousInputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .restart local v5    # "projection":Lcom/google/android/gms/maps/Projection;
    .restart local v6    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    :cond_3
    new-instance v6, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;

    .end local v6    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    invoke-direct {v6, v5, v3, v4, p1}, Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;-><init>(Lcom/google/android/gms/maps/Projection;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;)V

    .line 44
    .restart local v6    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->animatedTransitions:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 45
    const/4 v0, 0x1

    goto :goto_1

    .line 51
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "previousClusterPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterPoint;
    .end local v4    # "previousInputPoint":Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;
    .end local v6    # "transition":Lcom/sec/samsung/gallery/mapfragment/clustering/AnimatedTransition;
    :cond_4
    if-nez v0, :cond_5

    .line 52
    iget-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;->stationaryTransitions:Ljava/util/ArrayList;

    invoke-virtual {v7, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 55
    .end local v0    # "animated":Z
    :cond_5
    return-void
.end method

.method build()Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions;-><init>(Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$Builder;Lcom/sec/samsung/gallery/mapfragment/clustering/ClusterTransitions$1;)V

    return-object v0
.end method
