.class public Lcom/sec/samsung/gallery/util/SecretboxOperations;
.super Landroid/os/AsyncTask;
.source "SecretboxOperations.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/util/SecretboxOperations$1;,
        Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;,
        Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# static fields
.field public static final OP_MOVE_TO_SECRETBOX:I = 0x0

.field public static final OP_REMOVE_FROM_SECRETBOX:I = 0x1

.field private static final mComparator:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field

.field public static mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

.field private mOperationId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/sec/samsung/gallery/util/SecretboxOperations$MediaObjectComparator;-><init>(Lcom/sec/samsung/gallery/util/SecretboxOperations$1;)V

    sput-object v0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mComparator:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(ILcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;Landroid/content/Context;)V
    .locals 1
    .param p1, "operationId"    # I
    .param p2, "onSecretboxListener"    # Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 44
    iput p1, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOperationId:I

    .line 45
    sput-object p2, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    .line 46
    iput-object p3, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mContext:Landroid/content/Context;

    .line 47
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    .line 48
    return-void
.end method

.method private getDstAlbumPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5
    .param p1, "folderPath"    # Ljava/lang/String;
    .param p2, "dstAlbumName"    # Ljava/lang/String;

    .prologue
    .line 121
    const/4 v2, 0x0

    .line 122
    .local v2, "dstAlbum":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    .local v1, "desAlbumParent":Ljava/io/File;
    new-instance v2, Ljava/io/File;

    .end local v2    # "dstAlbum":Ljava/io/File;
    invoke-direct {v2, v1, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 125
    .restart local v2    # "dstAlbum":Ljava/io/File;
    const/4 v0, 0x1

    .line 126
    .local v0, "count":I
    :goto_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 127
    new-instance v2, Ljava/io/File;

    .end local v2    # "dstAlbum":Ljava/io/File;
    invoke-static {p2, v0}, Lcom/sec/android/gallery3d/common/Utils;->addPostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v1, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 128
    .restart local v2    # "dstAlbum":Ljava/io/File;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 130
    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v4

    if-nez v4, :cond_1

    .line 131
    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    .line 133
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    .line 134
    .local v3, "dstAlbumPath":Ljava/lang/String;
    return-object v3
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 14
    .param p1, "args"    # [Ljava/lang/Void;

    .prologue
    const/4 v13, 0x0

    .line 58
    const/4 v8, 0x0

    .line 59
    .local v8, "result":Z
    const/4 v10, 0x0

    .line 60
    .local v10, "successCount":I
    iget-object v11, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mContext:Landroid/content/Context;

    check-cast v11, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v9

    .line 61
    .local v9, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v9}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v11

    invoke-direct {v5, v11}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 62
    .local v5, "mediaList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaObject;>;"
    sget-object v11, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mComparator:Ljava/util/Comparator;

    invoke-static {v5, v11}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 64
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/sec/android/gallery3d/data/MediaObject;

    .line 65
    .local v6, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    iget v11, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOperationId:I

    if-nez v11, :cond_6

    .line 66
    instance-of v11, v6, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v11, :cond_4

    move-object v7, v6

    .line 67
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 68
    .local v7, "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v11, v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v11, :cond_3

    .line 69
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v11

    invoke-virtual {v7, v13, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 70
    .local v4, "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v11, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    if-eqz v11, :cond_0

    .line 71
    sget-object v11, Lcom/sec/android/gallery3d/util/GalleryUtils;->SECRETBOX_PATH:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->getDstAlbumPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 72
    .local v0, "dstAlbumPath":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v1, v11, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_0

    .line 73
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 74
    .local v3, "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v3}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_2

    .line 75
    const/4 v8, 0x0

    .line 80
    :goto_2
    if-eqz v8, :cond_1

    .line 81
    add-int/lit8 v10, v10, 0x1

    .line 72
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .line 77
    :cond_2
    sget-object v11, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    invoke-interface {v11, v3, v0}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;)Z

    move-result v8

    goto :goto_2

    .line 86
    .end local v0    # "dstAlbumPath":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    :cond_3
    const/4 v8, 0x0

    goto :goto_0

    .line 88
    .end local v7    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_4
    instance-of v11, v6, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    if-eqz v11, :cond_0

    move-object v11, v6

    .line 89
    check-cast v11, Lcom/sec/android/gallery3d/data/LocalMediaItem;

    invoke-virtual {v11}, Lcom/sec/android/gallery3d/data/LocalMediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v11}, Lcom/sec/android/gallery3d/util/MediaSetUtils;->isSecretBoxPath(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 90
    const/4 v8, 0x0

    .line 95
    :goto_3
    if-eqz v8, :cond_0

    .line 96
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 92
    :cond_5
    sget-object v11, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    const/4 v12, 0x0

    invoke-interface {v11, v6, v12}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;)Z

    move-result v8

    goto :goto_3

    .line 99
    :cond_6
    iget v11, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOperationId:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_0

    .line 100
    instance-of v11, v6, Lcom/sec/android/gallery3d/data/MediaSet;

    if-eqz v11, :cond_0

    move-object v7, v6

    .line 101
    check-cast v7, Lcom/sec/android/gallery3d/data/MediaSet;

    .line 102
    .restart local v7    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    instance-of v11, v7, Lcom/sec/android/gallery3d/data/LocalMergeAlbum;

    if-eqz v11, :cond_0

    .line 103
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItemCount()I

    move-result v11

    invoke-virtual {v7, v13, v11}, Lcom/sec/android/gallery3d/data/MediaSet;->getMediaItem(II)Ljava/util/ArrayList;

    move-result-object v4

    .line 104
    .restart local v4    # "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    sget-object v11, Lcom/sec/android/gallery3d/util/MediaSetUtils;->RESTORE_PATH:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-direct {p0, v11, v12}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->getDstAlbumPath(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    .restart local v0    # "dstAlbumPath":Ljava/lang/String;
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v11

    add-int/lit8 v1, v11, -0x1

    .restart local v1    # "i":I
    :goto_4
    if-ltz v1, :cond_0

    .line 106
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 107
    .restart local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    sget-object v11, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    invoke-interface {v11, v3, v0}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->handleOperation(Lcom/sec/android/gallery3d/data/MediaObject;Ljava/lang/String;)Z

    move-result v8

    .line 108
    if-eqz v8, :cond_7

    .line 109
    add-int/lit8 v10, v10, 0x1

    .line 105
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_4

    .line 117
    .end local v0    # "dstAlbumPath":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "mediaItem":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v4    # "mediaItemList":Ljava/util/List;, "Ljava/util/List<Lcom/sec/android/gallery3d/data/MediaItem;>;"
    .end local v6    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    .end local v7    # "mediaSet":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_8
    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    return-object v11
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->doInBackground([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 140
    sget-object v0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    if-eqz v0, :cond_0

    .line 141
    sget-object v0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mOnSecretboxListener:Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/util/SecretboxOperations$OnSecretboxListener;->onCompleted(I)V

    .line 143
    :cond_0
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 144
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 24
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/sec/samsung/gallery/util/SecretboxOperations;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/sec/samsung/gallery/util/SecretboxOperations;->mDataManager:Lcom/sec/android/gallery3d/data/DataManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/gallery3d/data/DataManager;->setChangeNotifierActive(Z)V

    .line 53
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 54
    return-void
.end method
