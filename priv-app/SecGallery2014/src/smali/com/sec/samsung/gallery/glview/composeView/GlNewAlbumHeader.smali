.class public Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;
.super Ljava/lang/Object;
.source "GlNewAlbumHeader.java"


# static fields
.field private static final DURATION:I = 0x190

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActive:Z

.field private mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mBgRect:Landroid/graphics/RectF;

.field private mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

.field private mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

.field private mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

.field private mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

.field private mOccupationHeight:F

.field private mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

.field private mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

.field private mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->TAG:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 1
    .param p1, "composeView"    # Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-direct {v0}, Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    .line 34
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    .line 35
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    .line 39
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    .line 40
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase;

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    .line 41
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDimensionUtil()Lcom/sec/samsung/gallery/util/DimensionUtil;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    .line 42
    return-void
.end method


# virtual methods
.method public animateAppear()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/high16 v2, 0x3f800000    # 1.0f

    .line 180
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 182
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableScaleAnim(Z)V

    .line 183
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAlphaAnim(Z)V

    .line 184
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceScale(F)V

    .line 185
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceAlpha(F)V

    .line 186
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetScale(F)V

    .line 187
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetAlpha(F)V

    .line 188
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 189
    return-void
.end method

.method public animateDisappear()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    .line 192
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->resetTransformAnimation()V

    .line 194
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableScaleAnim(Z)V

    .line 195
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEnableAlphaAnim(Z)V

    .line 196
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceScale(F)V

    .line 197
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSourceAlpha(F)V

    .line 198
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const v1, 0x3ecccccd    # 0.4f

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetScale(F)V

    .line 199
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setTargetAlpha(F)V

    .line 200
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    const/4 v1, 0x0

    const-wide/16 v2, 0x190

    const-wide/16 v4, 0x0

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mInterpolator:Lcom/sec/android/gallery3d/glcore/glparts/GlInterpolatorSqr;

    invoke-virtual/range {v0 .. v6}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->startTransAnim(Lcom/sec/android/gallery3d/glcore/GlAnimationBase$GlAnimationListener;JJLcom/sec/android/gallery3d/glcore/GlInterpolator;)V

    .line 201
    return-void
.end method

.method public clean()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    if-nez v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    .line 85
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 86
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 87
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->remove()V

    .line 88
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 89
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->remove()V

    .line 90
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 91
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->remove()V

    .line 92
    iput-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    goto :goto_0
.end method

.method public getHeaderObject()Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mOccupationHeight:F

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize(Z)V
    .locals 10
    .param p1, "doAnimation"    # Z

    .prologue
    const/4 v9, 0x0

    const/4 v4, 0x1

    const/4 v8, 0x0

    const/high16 v6, 0x43af0000    # 350.0f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 48
    iput-boolean v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    .line 49
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 50
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFill(Z)V

    .line 52
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0051

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setEmptyFillColor(I)V

    .line 56
    :cond_0
    new-instance v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v4, v4, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mReferRatioForNewAlbum:F

    mul-float/2addr v4, v6

    float-to-int v4, v4

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mReferRatioForNewAlbum:F

    mul-float/2addr v5, v6

    float-to-int v5, v5

    invoke-direct {v2, v3, v4, v5}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;II)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 57
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v4}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 58
    .local v0, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 59
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 61
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v2, v3, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 62
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v5, v5, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsW:I

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v6, v6, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleCanvsH:I

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 63
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    check-cast v2, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/4 v3, -0x2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    invoke-virtual {v2, v3, v4, v8, v9}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 64
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 65
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 67
    new-instance v2, Lcom/sec/android/gallery3d/glcore/GlObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {v2, v3, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlObject;-><init>(Lcom/sec/android/gallery3d/glcore/GlLayer;FF)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    .line 68
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    new-instance v3, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    const/4 v5, 0x5

    const/4 v6, 0x3

    invoke-direct {v3, v4, v5, v6}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {v2, v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 69
    new-instance v1, Lcom/sec/android/gallery3d/glcore/GlImageView;

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;-><init>(Landroid/content/Context;)V

    .line 70
    .local v1, "imgView":Lcom/sec/android/gallery3d/glcore/GlImageView;
    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0052

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/glcore/GlImageView;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 71
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v1}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 72
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->addChild(Lcom/sec/android/gallery3d/glcore/GlObject;)I

    .line 74
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->resetLayout()V

    .line 75
    if-eqz p1, :cond_1

    .line 76
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->animateAppear()V

    .line 78
    :cond_1
    return-void
.end method

.method public resetLayout()V
    .locals 18

    .prologue
    .line 98
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mActive:Z

    if-nez v13, :cond_0

    .line 136
    :goto_0
    return-void

    .line 101
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    invoke-virtual {v13, v14}, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->getNewAlbumHeaderRect(Landroid/graphics/RectF;)V

    .line 102
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v13, v13, Landroid/graphics/RectF;->bottom:F

    move-object/from16 v0, p0

    iput v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mOccupationHeight:F

    .line 103
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v13, v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 104
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBackObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->left:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v15, v15, Landroid/graphics/RectF;->top:F

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mDefDistance:F

    move/from16 v16, v0

    move/from16 v0, v16

    neg-float v0, v0

    move/from16 v16, v0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 106
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v1, v13, Landroid/graphics/RectF;->bottom:F

    .line 107
    .local v1, "bh":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mItemW:F

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v14, v14, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mViewConfig:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$ViewConfig;->mItemSizeScale:F

    mul-float v12, v13, v14

    .line 108
    .local v12, "w":F
    move v2, v12

    .line 109
    .local v2, "h":F
    sub-float v13, v1, v2

    const/high16 v14, 0x40000000    # 2.0f

    div-float v8, v13, v14

    .line 110
    .local v8, "sy":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumHeaderPaddingTop()F

    move-result v5

    .line 111
    .local v5, "offsetY":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewNewAlbumHeaderPaddingLeft()F

    move-result v4

    .line 112
    .local v4, "offsetX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v9, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginLeft:F

    .line 113
    .local v9, "titleOffsetX":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumSeparatorRatio()F

    move-result v6

    .line 114
    .local v6, "splitRatio":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v13}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewSplitAlbumSeparatorRatioDividerHeight()F

    move-result v7

    .line 115
    .local v7, "splitRatioDividerHeight":F
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    sub-float v14, v8, v5

    const/4 v15, 0x0

    invoke-virtual {v13, v4, v14, v15}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setPos(FFF)V

    .line 116
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v13, v12, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setSize(FF)V

    .line 118
    const/4 v10, 0x0

    .line 119
    .local v10, "titleTextMarginLeftForMultiWindow":I
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v13

    if-eqz v13, :cond_1

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget-object v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mPosCtrlCom:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;

    iget-boolean v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$PositionControllerCom;->mPortraitMode:Z

    if-nez v13, :cond_1

    .line 120
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget-object v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v14, 0x7f0d038a

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 122
    :cond_1
    const/4 v11, 0x0

    .line 123
    .local v11, "titleTextMarginRightForRtl":I
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v13

    invoke-virtual {v13}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    .line 125
    .local v3, "locale":Ljava/lang/String;
    const-string v13, "ar"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "fa"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string/jumbo v13, "ur"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_2

    const-string v13, "iw"

    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_3

    .line 127
    :cond_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget-object v13, v13, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mResource:Landroid/content/res/Resources;

    const v14, 0x7f0d038b

    invoke-virtual {v13, v14}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v11

    .line 129
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    int-to-float v14, v10

    add-float/2addr v14, v9

    int-to-float v15, v11

    sub-float/2addr v14, v15

    const/high16 v15, 0x40000000    # 2.0f

    div-float v15, v1, v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleTextMarginTop:F

    move/from16 v16, v0

    sub-float v15, v15, v16

    sub-float/2addr v15, v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget v0, v0, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleH:F

    move/from16 v16, v0

    const/high16 v17, 0x40000000    # 2.0f

    div-float v16, v16, v17

    sub-float v15, v15, v16

    sub-float/2addr v15, v5

    const/16 v16, 0x0

    invoke-virtual/range {v13 .. v16}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 130
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v14, v14, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleW:F

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mPosCtrl:Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;

    iget v15, v15, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerPhoto;->mTitleH:F

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlObject;->setSize(FF)V

    .line 132
    neg-float v13, v1

    const/high16 v14, 0x40000000    # 2.0f

    div-float/2addr v13, v14

    const/high16 v14, 0x3fc00000    # 1.5f

    add-float v8, v13, v14

    .line 133
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    const/4 v14, 0x0

    invoke-virtual {v13, v4, v8, v14}, Lcom/sec/android/gallery3d/glcore/GlObject;->setPos(FFF)V

    .line 135
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mSeparatorObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mBgRect:Landroid/graphics/RectF;

    iget v14, v14, Landroid/graphics/RectF;->right:F

    mul-float/2addr v14, v6

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    const/16 v16, 0x4

    invoke-virtual/range {v15 .. v16}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->convY(I)F

    move-result v15

    mul-float/2addr v15, v7

    invoke-virtual {v13, v14, v15}, Lcom/sec/android/gallery3d/glcore/GlObject;->setSize(FF)V

    goto/16 :goto_0
.end method

.method public updateBitmapFromAdapter(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "mediaItem"    # Lcom/sec/android/gallery3d/data/MediaItem;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 147
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "updateBitmapFromAdapter : bitmap = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    :cond_0
    const/4 v0, 0x0

    .line 157
    .local v0, "newBitmap":Landroid/graphics/Bitmap;
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    invoke-virtual {v1, p1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;->setHeaderItem(Lcom/sec/android/gallery3d/data/MediaItem;Landroid/graphics/Bitmap;)V

    .line 158
    return-void

    .line 154
    .end local v0    # "newBitmap":Landroid/graphics/Bitmap;
    :cond_1
    invoke-static {p2}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    .restart local v0    # "newBitmap":Landroid/graphics/Bitmap;
    goto :goto_0
.end method

.method public updateHeaderImage()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 163
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    if-nez v1, :cond_1

    .line 164
    sget-object v1, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->TAG:Ljava/lang/String;

    const-string v2, "mComposeView is null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    if-eqz v1, :cond_2

    .line 169
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 170
    .local v0, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mHeaderObject:Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v1, v0}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 173
    .end local v0    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_2
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mComposeView:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    check-cast v1, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;

    const/4 v2, -0x2

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v3}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/sec/samsung/gallery/view/adapter/ComposePhotoViewAlbumSetAdapter;->getViewExt(ILcom/sec/android/gallery3d/glcore/GlView;ILjava/lang/Object;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v0

    .line 175
    .restart local v0    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlNewAlbumHeader;->mTitleObject:Lcom/sec/android/gallery3d/glcore/GlObject;

    invoke-virtual {v1, v0}, Lcom/sec/android/gallery3d/glcore/GlObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    goto :goto_0
.end method
