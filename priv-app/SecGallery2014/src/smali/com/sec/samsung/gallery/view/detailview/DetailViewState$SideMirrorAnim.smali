.class Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;
.super Lcom/sec/android/gallery3d/anim/FloatAnimation;
.source "DetailViewState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/detailview/DetailViewState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SideMirrorAnim"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;FFI)V
    .locals 0
    .param p2, "from"    # F
    .param p3, "to"    # F
    .param p4, "duration"    # I

    .prologue
    .line 5452
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    .line 5453
    invoke-direct {p0, p2, p3, p4}, Lcom/sec/android/gallery3d/anim/FloatAnimation;-><init>(FFI)V

    .line 5454
    return-void
.end method


# virtual methods
.method protected onCalculate(F)V
    .locals 2
    .param p1, "progress"    # F

    .prologue
    .line 5458
    invoke-super {p0, p1}, Lcom/sec/android/gallery3d/anim/FloatAnimation;->onCalculate(F)V

    .line 5459
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->get()F

    move-result v1

    # setter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mSideMirrorX:F
    invoke-static {v0, v1}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$2002(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;F)F

    .line 5460
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/detailview/DetailViewState$SideMirrorAnim;->this$0:Lcom/sec/samsung/gallery/view/detailview/DetailViewState;

    # getter for: Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->mRootPane:Lcom/sec/android/gallery3d/ui/GLView;
    invoke-static {v0}, Lcom/sec/samsung/gallery/view/detailview/DetailViewState;->access$17700(Lcom/sec/samsung/gallery/view/detailview/DetailViewState;)Lcom/sec/android/gallery3d/ui/GLView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/ui/GLView;->requestLayout()V

    .line 5461
    return-void
.end method
