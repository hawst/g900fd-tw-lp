.class Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;
.super Lcom/sec/samsung/gallery/glview/GlScroller;
.source "GlGallerySearchView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/GlGallerySearchView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemScroller"
.end annotation


# instance fields
.field private isTagCloudListVisible:Z

.field private mFullVisibleCnt:I

.field private mGlAbsListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

.field private mTagListAbsHeight:F

.field private mTagListHeight:F

.field private mTitleOffsetX:F

.field private mTitleOffsetY:F

.field private mglTitleHeight:F

.field private mglTitleWidth:F

.field private mglUnderlineHeight:F

.field private mglUnderlineWidth:F

.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;


# direct methods
.method public constructor <init>(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Lcom/sec/samsung/gallery/glview/GlAbsListView;)V
    .locals 1
    .param p2, "glAbsListView"    # Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .prologue
    const/4 v0, 0x0

    .line 892
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    invoke-direct {p0}, Lcom/sec/samsung/gallery/glview/GlScroller;-><init>()V

    .line 883
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineWidth:F

    .line 885
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    .line 886
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListAbsHeight:F

    .line 893
    iput-object p2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mGlAbsListView:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    .line 894
    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;)F
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;

    .prologue
    .line 879
    iget v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListAbsHeight:F

    return v0
.end method


# virtual methods
.method public applyThumbPosition()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1237
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    if-ge v0, v2, :cond_0

    .line 1238
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v2, v2

    if-lt v0, v2, :cond_1

    .line 1251
    :cond_0
    return-void

    .line 1241
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v1, v2, v0

    .line 1242
    .local v1, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v1, :cond_0

    .line 1244
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    cmpl-float v2, v2, v5

    if-eqz v2, :cond_2

    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    cmpl-float v2, v2, v5

    if-nez v2, :cond_3

    .line 1237
    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1247
    :cond_3
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDx:F

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDy:F

    iget v4, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDz:F

    invoke-virtual {v1, v2, v3, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 1248
    iget v2, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    iget v3, v1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    invoke-virtual {v1, v2, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 1249
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mPitchFact:F
    invoke-static {v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$300(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPitch(F)V

    goto :goto_1
.end method

.method public onFocusChange(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 1265
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    if-eqz v0, :cond_0

    .line 1266
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnFocusChangedListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    const/4 v2, 0x1

    invoke-interface {v0, v1, p1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnFocusChangedListener;->onFocusChange(Lcom/sec/samsung/gallery/glview/GlAbsListView;II)V

    .line 1268
    :cond_0
    return-void
.end method

.method public onMove(F)V
    .locals 1
    .param p1, "delta"    # F

    .prologue
    .line 1260
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iput p1, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mSx:F

    .line 1261
    return-void
.end method

.method public onScroll()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1255
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->CMD_APPLY_POS:I
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$1200()I

    move-result v1

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setCommand(IIII)V

    .line 1256
    return-void
.end method

.method public onScrollStep(I)V
    .locals 3
    .param p1, "step"    # I

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 1273
    iget-object v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v0, v0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mOnScrollListener:Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v1, v1, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    sget v2, Lcom/sec/samsung/gallery/glview/GlAbsListView;->SCROLL_STATE_FLING:I

    invoke-interface {v0, v1, v2}, Lcom/sec/samsung/gallery/glview/GlAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/sec/samsung/gallery/glview/GlAbsListView;I)V

    .line 1275
    :cond_0
    return-void
.end method

.method protected procScrollStep()V
    .locals 7

    .prologue
    .line 1159
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-gtz v4, :cond_1

    .line 1205
    :cond_0
    :goto_0
    return-void

    .line 1161
    :cond_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v1, v4, v5

    .line 1162
    .local v1, "nScrl":I
    sget-object v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->LOCK:Ljava/lang/Object;

    monitor-enter v5

    .line 1163
    :try_start_0
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    .line 1164
    const/4 v4, -0x1

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    .line 1166
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    if-ge v0, v4, :cond_2

    .line 1167
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v4, v4

    if-lt v0, v4, :cond_3

    .line 1200
    :cond_2
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1202
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->onScrollStep(I)V

    .line 1203
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->onScroll()V

    goto :goto_0

    .line 1170
    :cond_3
    :try_start_1
    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v4, v0

    .line 1171
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_4

    .line 1166
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1173
    :cond_4
    const/4 v2, 0x0

    .line 1174
    .local v2, "needUpdate":Z
    :goto_3
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v1, :cond_5

    .line 1175
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    add-int/2addr v4, v6

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1176
    const/4 v2, 0x1

    goto :goto_3

    .line 1178
    :cond_5
    :goto_4
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    add-int/2addr v6, v1

    if-lt v4, v6, :cond_6

    .line 1179
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    sub-int/2addr v4, v6

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1180
    const/4 v2, 0x1

    goto :goto_4

    .line 1182
    :cond_6
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v4, :cond_b

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    if-ge v4, v6, :cond_b

    .line 1183
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    iget v6, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ge v4, v6, :cond_7

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    .line 1184
    :cond_7
    iget v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    iget v6, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-le v4, v6, :cond_8

    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iput v4, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    .line 1185
    :cond_8
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v4, v6

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 1186
    const/4 v4, 0x1

    sget v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v4, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 1188
    if-eqz v2, :cond_a

    .line 1189
    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getVisibility()Z

    move-result v4

    if-nez v4, :cond_9

    .line 1190
    const/4 v4, 0x1

    sget v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v3, v4, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 1192
    :cond_9
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1194
    :cond_a
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_2

    .line 1200
    .end local v0    # "i":I
    .end local v2    # "needUpdate":Z
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 1196
    .restart local v0    # "i":I
    .restart local v2    # "needUpdate":Z
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_b
    const/4 v4, 0x0

    :try_start_2
    sget v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v4, v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 1197
    iget v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v6, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v6, v6, -0x1

    and-int/2addr v4, v6

    iput v4, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public reset()V
    .locals 11

    .prologue
    const/high16 v1, 0x41200000    # 10.0f

    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v9, 0x40000000    # 2.0f

    .line 898
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-nez v7, :cond_0

    .line 970
    :goto_0
    return-void

    .line 900
    :cond_0
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->isTagCloudListVisible()Z

    move-result v7

    iput-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->isTagCloudListVisible:Z

    .line 901
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->isTagCloudListVisible:Z

    if-eqz v7, :cond_4

    const/4 v7, 0x0

    :goto_1
    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    .line 902
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    sub-float/2addr v8, v1

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->setHoverScrollFlexibleHeightMargin(F)V

    .line 903
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getFilterHeight(Z)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListAbsHeight:F

    .line 904
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    if-gtz v7, :cond_1

    .line 905
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V
    invoke-static {v7}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$900(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    .line 907
    :cond_1
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    .line 908
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d0265

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    .line 909
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d0266

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleWidth:F

    .line 910
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d0267

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineHeight:F

    .line 911
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-boolean v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    if-eqz v7, :cond_2

    const/high16 v1, 0x41a00000    # 20.0f

    .line 912
    .local v1, "itemOffSetY":F
    :cond_2
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d026d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float v0, v7, v8

    .line 914
    .local v0, "itemGap":F
    const/4 v3, 0x0

    .line 916
    .local v3, "resultsPaddingLeft":F
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v7, :cond_6

    .line 917
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d029c

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float v2, v7, v8

    .line 919
    .local v2, "leftMargin":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d029d

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float v4, v7, v8

    .line 921
    .local v4, "rightMargin":F
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->isTagCloudListVisible:Z

    if-eqz v7, :cond_5

    .line 922
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d0273

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    add-float v3, v7, v2

    .line 924
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d029b

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    sub-float/2addr v7, v2

    sub-float v6, v7, v4

    .line 930
    .local v6, "width":F
    :goto_2
    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineWidth:F

    .line 933
    .end local v2    # "leftMargin":F
    .end local v4    # "rightMargin":F
    :goto_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v7}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getActionBarHeight()F

    move-result v5

    .line 935
    .local v5, "topMargin":F
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-boolean v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWideMode:Z

    if-eqz v7, :cond_7

    .line 936
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    add-int/lit8 v7, v7, -0x1

    int-to-float v7, v7

    mul-float/2addr v7, v0

    sub-float/2addr v6, v7

    .line 937
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    int-to-float v7, v7

    div-float v7, v6, v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    .line 950
    :goto_4
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    add-float/2addr v7, v0

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapW:F

    .line 951
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    add-float/2addr v7, v0

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    .line 952
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    neg-float v7, v7

    div-float/2addr v7, v9

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    add-float/2addr v7, v3

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartX:F

    .line 953
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    div-float/2addr v7, v9

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    div-float/2addr v8, v9

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    add-float/2addr v8, v5

    add-float/2addr v8, v1

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartY:F

    .line 954
    sget v7, Lcom/sec/samsung/gallery/glview/GlAbsListView;->DEF_DISTANCE:F

    neg-float v7, v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    .line 956
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListH:F

    .line 957
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    sub-float/2addr v7, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    sub-float/2addr v7, v8

    sub-float/2addr v7, v1

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    .line 958
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mHeightSpace:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mFullVisibleCnt:I

    .line 959
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mFullVisibleCnt:I

    add-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVVisibleCnt:I

    .line 960
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVVisibleCnt:I

    mul-int/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    .line 961
    add-float v7, v1, v5

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVStart:F

    .line 962
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVStart:F

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVEnd:F

    .line 963
    iput v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetX:F

    .line 964
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartY:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v8, v9

    add-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    .line 965
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isMultiWindow()Z

    move-result v7

    if-eqz v7, :cond_3

    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseNewSearchUI:Z

    if-eqz v7, :cond_3

    .line 966
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getGlWidthMultiWindowOffset()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    .line 967
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartY:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v9, 0x7f0d027c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v9, v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v9}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v9

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartY:F

    .line 969
    :cond_3
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    add-float/2addr v8, v5

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    add-float/2addr v8, v9

    add-float/2addr v8, v1

    invoke-virtual {v7, p0, v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->resetScrollBar(Lcom/sec/samsung/gallery/glview/GlScroller;F)V

    goto/16 :goto_0

    .line 901
    .end local v0    # "itemGap":F
    .end local v1    # "itemOffSetY":F
    .end local v3    # "resultsPaddingLeft":F
    .end local v5    # "topMargin":F
    .end local v6    # "width":F
    :cond_4
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    check-cast v7, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/sec/samsung/gallery/view/adapter/GallerySearchItemAdapter;->getFilterHeight(Z)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    goto/16 :goto_1

    .line 927
    .restart local v0    # "itemGap":F
    .restart local v1    # "itemOffSetY":F
    .restart local v2    # "leftMargin":F
    .restart local v3    # "resultsPaddingLeft":F
    .restart local v4    # "rightMargin":F
    :cond_5
    move v3, v2

    .line 928
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    sub-float/2addr v7, v2

    sub-float v6, v7, v4

    .restart local v6    # "width":F
    goto/16 :goto_2

    .line 932
    .end local v2    # "leftMargin":F
    .end local v4    # "rightMargin":F
    .end local v6    # "width":F
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v6, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    .restart local v6    # "width":F
    goto/16 :goto_3

    .line 939
    .restart local v5    # "topMargin":F
    :cond_7
    div-float v7, v6, v10

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    .line 940
    const/high16 v7, 0x40400000    # 3.0f

    mul-float/2addr v7, v0

    sub-float/2addr v6, v7

    .line 941
    div-float v7, v6, v10

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    .line 942
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v7, :cond_8

    .line 943
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResource:Landroid/content/res/Resources;

    const v8, 0x7f0d029f

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    int-to-float v7, v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    invoke-virtual {v8}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getWin2WorldRatio()F

    move-result v8

    mul-float/2addr v7, v8

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    goto/16 :goto_4

    .line 946
    :cond_8
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    goto/16 :goto_4
.end method

.method public resetSet(IIZ)V
    .locals 11
    .param p1, "startIndex"    # I
    .param p2, "totalCount"    # I
    .param p3, "resetScroll"    # Z

    .prologue
    .line 1006
    if-nez p2, :cond_0

    .line 1007
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1008
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v8, :cond_0

    .line 1009
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 1011
    :cond_0
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    if-nez v8, :cond_1

    .line 1012
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # invokes: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->getHorizontalCount()V
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$900(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)V

    .line 1013
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    .line 1015
    :cond_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    add-int/2addr v8, p2

    add-int/lit8 v8, v8, -0x1

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemRowCount:I

    .line 1016
    const/4 v2, 0x0

    .line 1017
    .local v2, "maxRange":F
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemRowCount:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    cmpg-float v8, v8, v9

    if-gez v8, :cond_4

    .line 1018
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemRowCount:I

    int-to-float v9, v9

    mul-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    sub-float v2, v8, v9

    .line 1019
    iput v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollRange:F

    .line 1026
    :goto_0
    const/4 v5, 0x0

    .line 1028
    .local v5, "setScroll":Z
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v8, p1, v8

    const/4 v9, 0x1

    if-le v8, v9, :cond_5

    .line 1029
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v8, p1, v8

    add-int/lit8 v4, v8, 0x1

    .line 1033
    .local v4, "rowIndex":I
    :goto_1
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemRowCount:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVVisibleCnt:I

    if-gt v8, v9, :cond_6

    if-eqz p3, :cond_6

    .line 1034
    const/4 v4, 0x0

    .line 1040
    :cond_2
    :goto_2
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v8

    int-to-float v9, v4

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    mul-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    .line 1041
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v7, v4, v8

    .line 1045
    .local v7, "startRowIndex":I
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v8, v8

    if-gtz v8, :cond_7

    .line 1095
    :cond_3
    :goto_3
    return-void

    .line 1021
    .end local v4    # "rowIndex":I
    .end local v5    # "setScroll":Z
    .end local v7    # "startRowIndex":I
    :cond_4
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollRange:F

    goto :goto_0

    .line 1031
    .restart local v5    # "setScroll":Z
    :cond_5
    const/4 v4, 0x0

    .restart local v4    # "rowIndex":I
    goto :goto_1

    .line 1035
    :cond_6
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemRowCount:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mFullVisibleCnt:I

    add-int/2addr v9, v4

    if-gt v8, v9, :cond_2

    .line 1037
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float v8, v2, v8

    float-to-int v4, v8

    .line 1038
    const/4 v5, 0x1

    goto :goto_2

    .line 1047
    .restart local v7    # "startRowIndex":I
    :cond_7
    iput p2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    .line 1048
    const/4 v6, 0x0

    .line 1049
    .local v6, "startOffset":I
    if-eqz p3, :cond_a

    .line 1050
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v8, v7, v8

    add-int/lit8 v8, v8, -0x1

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    mul-int v6, v8, v9

    .line 1054
    :goto_4
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    add-int/2addr v8, v6

    add-int/lit8 v0, v8, -0x1

    .line 1055
    .local v0, "endOffset":I
    sget-object v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->LOCK:Ljava/lang/Object;

    monitor-enter v9

    .line 1056
    :try_start_0
    iput v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    .line 1057
    iput v0, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    .line 1058
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    if-gez v8, :cond_8

    .line 1059
    const/4 v8, 0x0

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    .line 1060
    :cond_8
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    if-lt v8, v10, :cond_9

    .line 1061
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    add-int/lit8 v8, v8, -0x1

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    .line 1062
    :cond_9
    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1063
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->updateScrollBar()V

    .line 1064
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemHVisibleCnt:I

    div-int v8, v7, v8

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    .line 1065
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    int-to-float v8, v8

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    mul-float/2addr v8, v9

    iput v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    .line 1066
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_5
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v8, v8

    if-ge v1, v8, :cond_f

    .line 1067
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v3, v8, v1

    .line 1068
    .local v3, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-nez v3, :cond_b

    .line 1066
    :goto_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 1052
    .end local v0    # "endOffset":I
    .end local v1    # "i":I
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_a
    move v6, v7

    goto :goto_4

    .line 1062
    .restart local v0    # "endOffset":I
    :catchall_0
    move-exception v8

    :try_start_1
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v8

    .line 1070
    .restart local v1    # "i":I
    .restart local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_b
    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    if-lt v1, v8, :cond_c

    .line 1072
    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v9, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    sget v10, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v9, v10

    xor-int/lit8 v9, v9, -0x1

    and-int/2addr v8, v9

    iput v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 1073
    const/4 v8, 0x0

    sget v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_6

    .line 1076
    :cond_c
    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v9, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_USED:I

    or-int/2addr v8, v9

    iput v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 1077
    add-int v8, v6, v1

    iput v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    .line 1078
    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v8, :cond_d

    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleLast:I

    if-le v8, v9, :cond_e

    .line 1079
    :cond_d
    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v9, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    xor-int/lit8 v9, v9, -0x1

    and-int/2addr v8, v9

    iput v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 1080
    const/4 v8, 0x0

    sget v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    goto :goto_6

    .line 1083
    :cond_e
    iget v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    sget v9, Lcom/sec/samsung/gallery/glview/GlBaseObject;->GL_OBJ_ACTIVE:I

    or-int/2addr v8, v9

    iput v8, v3, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mAttr:I

    .line 1084
    const/4 v8, 0x1

    sget v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_POS:I

    invoke-virtual {v3, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 1085
    iget-boolean v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mInitVisible:Z

    sget v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->VISIBLE_BY_INIT:I

    invoke-virtual {v3, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(ZI)V

    .line 1086
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1087
    invoke-virtual {p0, v3}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    goto :goto_6

    .line 1090
    .end local v3    # "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    :cond_f
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mVisibleFirst:I

    iput v9, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFocused:I

    .line 1091
    if-eqz v5, :cond_3

    .line 1092
    invoke-virtual {p0, v2}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->setScroll(F)V

    .line 1093
    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mFlingAnim:Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;
    invoke-static {v8}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$400(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/sec/samsung/gallery/glview/GlBoundaryAnim;->setInitMovement(F)V

    goto/16 :goto_3
.end method

.method public setScroll(F)V
    .locals 13
    .param p1, "scroll"    # F

    .prologue
    const/16 v12, 0x4eac

    const/high16 v11, 0x40000000    # 2.0f

    .line 1101
    const/4 v3, 0x0

    .line 1103
    .local v3, "isMovingTop":Z
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    cmpl-float v7, v7, p1

    if-nez v7, :cond_0

    .line 1147
    :goto_0
    return-void

    .line 1105
    :cond_0
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v0, v7

    .line 1106
    .local v0, "cScrl":I
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    add-float/2addr v7, p1

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    .line 1108
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    if-nez v7, :cond_6

    const/4 v6, 0x0

    .line 1109
    .local v6, "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :goto_1
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1110
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    cmpg-float v7, p1, v7

    if-gez v7, :cond_1

    .line 1111
    const/4 v3, 0x1

    .line 1112
    :cond_1
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v1, v7

    .line 1113
    .local v1, "currentScrolStepBottom":I
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollVisibleH:F

    add-float/2addr v7, p1

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    div-float/2addr v7, v8

    float-to-int v5, v7

    .line 1114
    .local v5, "previousScrollStepBottom":I
    if-eq v1, v5, :cond_2

    .line 1115
    if-eqz v3, :cond_2

    .line 1116
    invoke-virtual {v6, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1119
    .end local v1    # "currentScrolStepBottom":I
    .end local v5    # "previousScrollStepBottom":I
    :cond_2
    iput p1, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    .line 1120
    iget-boolean v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollBarEnabled:Z

    if-eqz v7, :cond_3

    .line 1121
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->showScrollBar()V

    .line 1122
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->updateScrollBar()V

    .line 1124
    :cond_3
    const-string v7, "mScroll modifying (setScroll)"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mScroll="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1125
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetX:F

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    div-float/2addr v9, v11

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleWidth:F

    div-float/2addr v9, v11

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    add-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 1126
    sget-boolean v7, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v7, :cond_4

    .line 1127
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetX:F

    iget-object v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v9, v9, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    div-float/2addr v9, v11

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineWidth:F

    div-float/2addr v9, v11

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    add-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    invoke-virtual {v7, v8, v9, v10}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 1129
    :cond_4
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollStep:I

    if-eq v7, v0, :cond_7

    .line 1130
    if-eqz v6, :cond_5

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/glcore/GlRootView;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-static {v7}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isCaneExtraHapticEnabled(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 1131
    if-nez v3, :cond_5

    .line 1132
    invoke-virtual {v6, v12}, Lcom/sec/android/gallery3d/glcore/GlRootView;->performHapticFeedback(I)Z

    .line 1134
    :cond_5
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->procScrollStep()V

    goto/16 :goto_0

    .line 1108
    .end local v6    # "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :cond_6
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScrollBar:Lcom/sec/samsung/gallery/glview/GlScrollBar;

    iget-object v6, v7, Lcom/sec/samsung/gallery/glview/GlScrollBar;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    goto/16 :goto_1

    .line 1137
    .restart local v6    # "rootView":Lcom/sec/android/gallery3d/glcore/GlRootView;
    :cond_7
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemVisibleCnt:I

    if-ge v2, v7, :cond_8

    .line 1138
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    array-length v7, v7

    if-lt v2, v7, :cond_9

    .line 1146
    :cond_8
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->onScroll()V

    goto/16 :goto_0

    .line 1141
    :cond_9
    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->glObjSet:[Lcom/sec/samsung/gallery/glview/GlBaseObject;

    aget-object v4, v7, v2

    .line 1142
    .local v4, "obj":Lcom/sec/samsung/gallery/glview/GlBaseObject;
    if-eqz v4, :cond_a

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v7, :cond_a

    iget v7, v4, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    if-ge v7, v8, :cond_a

    .line 1143
    invoke-virtual {p0, v4}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V

    .line 1137
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setThumbPosition(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 11
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    .line 1209
    if-nez p1, :cond_1

    .line 1210
    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$000()Ljava/lang/String;

    move-result-object v6

    const-string v7, "setThumbPosition: The obj is null."

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1234
    :cond_0
    :goto_0
    return-void

    .line 1216
    :cond_1
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    rem-int v0, v6, v7

    .line 1217
    .local v0, "col":I
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget-object v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v7, v7, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mColCnt:I

    div-int v1, v6, v7

    .line 1218
    .local v1, "row":I
    iget v3, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemW:F

    .line 1219
    .local v3, "thumbWidth":F
    iget v2, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemH:F

    .line 1220
    .local v2, "thumbHeight":F
    int-to-float v6, v0

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapW:F

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartX:F

    add-float v4, v6, v7

    .line 1221
    .local v4, "x":F
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartY:F

    int-to-float v7, v1

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemGapH:F

    mul-float/2addr v7, v8

    sub-float v5, v6, v7

    .line 1223
    .local v5, "y":F
    iput v4, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDx:F

    .line 1224
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    add-float/2addr v6, v5

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTagListHeight:F

    sub-float/2addr v6, v7

    iput v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDy:F

    .line 1225
    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    iput v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDz:F

    .line 1226
    iput v3, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDw:F

    .line 1227
    iput v2, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mDh:F

    .line 1229
    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-nez v6, :cond_0

    .line 1230
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetX:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    div-float/2addr v8, v10

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleWidth:F

    div-float/2addr v8, v10

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    .line 1231
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v6, :cond_0

    .line 1232
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetX:F

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mWidthSpace:F

    div-float/2addr v8, v10

    sub-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineWidth:F

    div-float/2addr v8, v10

    add-float/2addr v7, v8

    iget v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mScroll:F

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mTitleOffsetY:F

    add-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    iget v9, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemStartZ:F

    invoke-virtual {v6, v7, v8, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setPos(FFF)V

    goto :goto_0
.end method

.method public update(Lcom/sec/samsung/gallery/glview/GlBaseObject;)V
    .locals 11
    .param p1, "obj"    # Lcom/sec/samsung/gallery/glview/GlBaseObject;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 976
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$1000(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailWidthPixel(I)I

    move-result v1

    .line 977
    .local v1, "canvasWidth":I
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mDimensionUtil:Lcom/sec/samsung/gallery/util/DimensionUtil;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    # getter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mViewMode:I
    invoke-static {v6}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$1000(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;)I

    move-result v6

    invoke-virtual {v5, v6}, Lcom/sec/samsung/gallery/util/DimensionUtil;->getThumbnailViewThumbnailHeightPixel(I)I

    move-result v0

    .line 979
    .local v0, "canvasHeight":I
    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getWidth()I

    move-result v5

    if-ne v5, v1, :cond_0

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getCanvas()Lcom/sec/android/gallery3d/glcore/GlCanvas;

    move-result-object v5

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlCanvas;->getHeight()I

    move-result v5

    if-eq v5, v0, :cond_1

    .line 980
    :cond_0
    new-instance v5, Lcom/sec/android/gallery3d/glcore/GlCanvas;

    iget-object v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mGlRoot:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-direct {v5, v6, v1, v0}, Lcom/sec/android/gallery3d/glcore/GlCanvas;-><init>(Lcom/sec/android/gallery3d/glcore/GlRootView;II)V

    invoke-virtual {p1, v5}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setCanvas(Lcom/sec/android/gallery3d/glcore/GlCanvas;)V

    .line 983
    :cond_1
    iget v5, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-nez v5, :cond_2

    .line 984
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v5, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 985
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleWidth:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglTitleHeight:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 986
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-virtual {v5, v8, v6, v10}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v2

    .line 987
    .local v2, "glTitleView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    move-object v5, v2

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlImageView;

    invoke-virtual {v5, v8}, Lcom/sec/android/gallery3d/glcore/GlImageView;->getChild(I)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/glcore/GlTextView;

    invoke-virtual {v5}, Lcom/sec/android/gallery3d/glcore/GlTextView;->getText()Ljava/lang/String;

    move-result-object v5

    # setter for: Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mResultText:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->access$1102(Lcom/sec/samsung/gallery/glview/GlGallerySearchView;Ljava/lang/String;)Ljava/lang/String;

    .line 988
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v5, v2}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 990
    sget-boolean v5, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseGallerySearchUISeperatedMode:Z

    if-eqz v5, :cond_2

    .line 991
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v5, v9}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setVisibility(Z)V

    .line 992
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineWidth:F

    iget v7, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mglUnderlineHeight:F

    invoke-virtual {v5, v6, v7}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setSize(FF)V

    .line 993
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v6, v6, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v6}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-virtual {v5, v8, v6, v10}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v3

    .line 994
    .local v3, "glUnderlineView":Lcom/sec/android/gallery3d/glcore/GlView;
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mTitleUnderlineGlObject:Lcom/sec/samsung/gallery/glview/GlBaseObject;

    invoke-virtual {v5, v3}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 998
    .end local v2    # "glTitleView":Lcom/sec/android/gallery3d/glcore/GlView;
    .end local v3    # "glUnderlineView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_2
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    if-eqz v5, :cond_3

    iget v5, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    if-ltz v5, :cond_3

    iget v5, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    iget v6, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->mItemCount:I

    if-ge v5, v6, :cond_3

    .line 999
    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mAdapter:Lcom/sec/samsung/gallery/glview/GlBaseAdapter;

    iget v6, p1, Lcom/sec/samsung/gallery/glview/GlBaseObject;->mIndex:I

    invoke-virtual {p1}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v7

    iget-object v8, p0, Lcom/sec/samsung/gallery/glview/GlGallerySearchView$ItemScroller;->this$0:Lcom/sec/samsung/gallery/glview/GlGallerySearchView;

    iget-object v8, v8, Lcom/sec/samsung/gallery/glview/GlGallerySearchView;->mThis:Lcom/sec/samsung/gallery/glview/GlAbsListView;

    invoke-virtual {v5, v6, v7, v8}, Lcom/sec/samsung/gallery/glview/GlBaseAdapter;->getView(ILcom/sec/android/gallery3d/glcore/GlView;Lcom/sec/android/gallery3d/glcore/GlLayer;)Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v4

    .line 1000
    .local v4, "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    invoke-virtual {p1, v4}, Lcom/sec/samsung/gallery/glview/GlBaseObject;->setView(Lcom/sec/android/gallery3d/glcore/GlView;)V

    .line 1002
    .end local v4    # "glView":Lcom/sec/android/gallery3d/glcore/GlView;
    :cond_3
    return-void
.end method
