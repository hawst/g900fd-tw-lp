.class public Lcom/sec/samsung/gallery/mapfragment/MapViewState;
.super Lcom/sec/android/gallery3d/app/ActivityState;
.source "MapViewState.java"

# interfaces
.implements Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;
.implements Lcom/sec/samsung/gallery/mapfragment/MapTouchWrapper$MapTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;,
        Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;,
        Lcom/sec/samsung/gallery/mapfragment/MapViewState$Options;,
        Lcom/sec/samsung/gallery/mapfragment/MapViewState$HoverScrollHandler;
    }
.end annotation


# static fields
.field private static final MAP_FRAGMENT_EVENT_TAG:Ljava/lang/String; = "eventmap"

.field private static final MAP_FRAGMENT_TAG:Ljava/lang/String; = "map"

.field private static final MSG_FINISH_MAP_VIEW:I

.field private static final TAG:Ljava/lang/String;

.field private static ZOOM_RATIO:F


# instance fields
.field private final HOVERSCROLL_DELAY:I

.field private final HOVERSCROLL_MOVE:I

.field private final THUMBNAIL_FADEOUT_ANIMATION_NO_DURATION:I

.field bounds:[Lcom/google/android/gms/maps/model/LatLngBounds;

.field private clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

.field inflater:Landroid/view/LayoutInflater;

.field inputPoints:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/clustering/InputPoint;",
            ">;"
        }
    .end annotation
.end field

.field private mAlbumName:Ljava/lang/String;

.field private mAlbumPosition:I

.field private mCurrentItemPath:Ljava/lang/String;

.field private mCurrentSetPath:Ljava/lang/String;

.field private mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

.field private mFirstDraw:Z

.field private mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

.field private mHandler:Landroid/os/Handler;

.field private mHoverScrollHandler:Lcom/sec/samsung/gallery/mapfragment/MapViewState$HoverScrollHandler;

.field private mItemLatLng:[D

.field mLinlaHeaderProgress:Landroid/widget/LinearLayout;

.field private mLoadingDialog:Landroid/app/ProgressDialog;

.field mMainRL:Landroid/widget/RelativeLayout;

.field mMap:Lcom/google/android/gms/maps/GoogleMap;

.field mMapFragment:Lcom/google/android/gms/maps/MapFragment;

.field mMapThumbnailListView:Lcom/sec/samsung/gallery/hlistview/widget/HListView;

.field private mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

.field mMapViewLayout:Landroid/view/View;

.field private mMapViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

.field mMarker:Lcom/google/android/gms/maps/model/Marker;

.field mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

.field mMarkerView:Landroid/view/View;

.field mMarkerbackImg:Landroid/widget/ImageView;

.field mMarkers:[Lcom/google/android/gms/maps/model/Marker;

.field private mMediaChangeObserver:Landroid/database/DataSetObserver;

.field mMediaSet:Lcom/sec/android/gallery3d/data/MediaSet;

.field mScreenPosition:Landroid/graphics/Point;

.field private mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

.field mThumbnailAddress_marker_img:Landroid/widget/ImageView;

.field mThumbnailAddress_txt:Landroid/widget/TextView;

.field mTouchedMarker:Lcom/google/android/gms/maps/model/Marker;

.field private mZoolLevel:I

.field view:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    const-class v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    .line 81
    const/high16 v0, 0x41500000    # 13.0f

    sput v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->ZOOM_RATIO:F

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/gallery3d/app/ActivityState;-><init>()V

    .line 78
    iput v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->HOVERSCROLL_MOVE:I

    .line 79
    const/16 v0, 0xa

    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->HOVERSCROLL_DELAY:I

    .line 87
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mFirstDraw:Z

    .line 138
    iput v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->THUMBNAIL_FADEOUT_ANIMATION_NO_DURATION:I

    .line 143
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;

    const-string v1, "MAP_VIEW"

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v0, p0, v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Ljava/lang/String;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    .line 634
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$4;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMediaChangeObserver:Landroid/database/DataSetObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Landroid/content/res/Configuration;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # Landroid/content/res/Configuration;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->onConfigChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setUpMapIfNeeded()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;)Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I

    return v0
.end method

.method static synthetic access$1302(Lcom/sec/samsung/gallery/mapfragment/MapViewState;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # I

    .prologue
    .line 73
    iput p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I

    return p1
.end method

.method static synthetic access$1400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/StateManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)[D
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/samsung/gallery/view/ActionBarManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setCameraPositon()V

    return-void
.end method

.method static synthetic access$2300(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Landroid/os/Message;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # Landroid/os/Message;

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->handleMessage(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$602(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$700(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/mapfragment/MapViewState;

    .prologue
    .line 73
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    return-object v0
.end method

.method public static createBitmap(Lcom/sec/android/gallery3d/data/MediaItem;)Landroid/graphics/Bitmap;
    .locals 4
    .param p0, "image"    # Lcom/sec/android/gallery3d/data/MediaItem;

    .prologue
    .line 626
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 627
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 628
    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "fail to get image of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 629
    const/4 v1, 0x0

    .line 631
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    invoke-static {v0, v1}, Lcom/sec/android/gallery3d/gadget/WidgetUtils;->createWidgetBitmap(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method private getOptionMenuId()I
    .locals 1

    .prologue
    .line 386
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isSCAMEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    const v0, 0x7f12000d

    .line 391
    :goto_0
    return v0

    .line 388
    :cond_0
    invoke-static {}, Lcom/sec/android/gallery3d/util/GalleryFeature;->isTablet()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 389
    const v0, 0x7f120010

    goto :goto_0

    .line 391
    :cond_1
    const v0, 0x7f12000f

    goto :goto_0
.end method

.method private handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 366
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 371
    :goto_0
    return-void

    .line 368
    :pswitch_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mHoverScrollHandler:Lcom/sec/samsung/gallery/mapfragment/MapViewState$HoverScrollHandler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$HoverScrollHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 366
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private initializeDataAdapter(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 227
    if-eqz p1, :cond_1

    const-string v0, "KEY_MEDIA_SET_PATH"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    .line 228
    if-eqz p1, :cond_2

    const-string v0, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentItemPath:Ljava/lang/String;

    .line 229
    if-eqz p1, :cond_3

    const-string v0, "KEY_MEDIA_SET_POSITION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I

    .line 230
    if-eqz p1, :cond_4

    const-string v0, "KEY_MEDIA_ITEM_LOCATION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getDoubleArray(Ljava/lang/String;)[D

    move-result-object v0

    :goto_3
    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D

    .line 231
    if-eqz p1, :cond_5

    const-string v0, "KEY_MAP_VIEW_ZOOM_LEVEL"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_4
    iput v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mZoolLevel:I

    .line 233
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    if-nez v0, :cond_0

    .line 234
    new-instance v0, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    .line 236
    :cond_0
    return-void

    :cond_1
    move-object v0, v1

    .line 227
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 228
    goto :goto_1

    .line 229
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move-object v0, v1

    .line 230
    goto :goto_3

    :cond_5
    move v0, v2

    .line 231
    goto :goto_4
.end method

.method private onConfigChanged(Landroid/content/res/Configuration;)V
    .locals 1
    .param p1, "config"    # Landroid/content/res/Configuration;

    .prologue
    .line 360
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->invalidateOptionsMenu()V

    .line 363
    :cond_0
    return-void
.end method

.method private setCameraPositon()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 698
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D

    if-nez v3, :cond_1

    .line 713
    :cond_0
    :goto_0
    return-void

    .line 701
    :cond_1
    iget-boolean v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mFirstDraw:Z

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v3}, Lcom/sec/android/gallery3d/util/GalleryUtils;->isEventViewMode(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 704
    :cond_2
    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D

    aget-wide v4, v3, v8

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mItemLatLng:[D

    const/4 v6, 0x1

    aget-wide v6, v3, v6

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 705
    .local v2, "itemLatLng":Lcom/google/android/gms/maps/model/LatLng;
    iget v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mZoolLevel:I

    const/4 v4, -0x1

    if-eq v3, v4, :cond_3

    .line 706
    iget v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mZoolLevel:I

    int-to-float v3, v3

    sput v3, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->ZOOM_RATIO:F

    .line 708
    :cond_3
    new-instance v3, Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    invoke-direct {v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;-><init>()V

    invoke-virtual {v3, v2}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->target(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v3

    sget v4, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->ZOOM_RATIO:F

    invoke-virtual {v3, v4}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->zoom(F)Lcom/google/android/gms/maps/model/CameraPosition$Builder;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/maps/model/CameraPosition$Builder;->build()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v0

    .line 710
    .local v0, "cameraPosition":Lcom/google/android/gms/maps/model/CameraPosition;
    invoke-static {v0}, Lcom/google/android/gms/maps/CameraUpdateFactory;->newCameraPosition(Lcom/google/android/gms/maps/model/CameraPosition;)Lcom/google/android/gms/maps/CameraUpdate;

    move-result-object v1

    .line 711
    .local v1, "cameraUpdate":Lcom/google/android/gms/maps/CameraUpdate;
    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/GoogleMap;->moveCamera(Lcom/google/android/gms/maps/CameraUpdate;)V

    .line 712
    iput-boolean v8, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mFirstDraw:Z

    goto :goto_0
.end method

.method private setUpMap()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 689
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/GoogleMap;->setOnCameraChangeListener(Lcom/google/android/gms/maps/GoogleMap$OnCameraChangeListener;)V

    .line 690
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/UiSettings;->setZoomControlsEnabled(Z)V

    .line 691
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/UiSettings;->setRotateGesturesEnabled(Z)V

    .line 692
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->getUiSettings()Lcom/google/android/gms/maps/UiSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/UiSettings;->setCompassEnabled(Z)V

    .line 693
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setCameraPositon()V

    .line 694
    return-void
.end method

.method private setUpMapIfNeeded()V
    .locals 2

    .prologue
    .line 396
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v1, :cond_0

    .line 398
    :try_start_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/MapFragment;->getMap()Lcom/google/android/gms/maps/GoogleMap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 404
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-eqz v1, :cond_0

    .line 405
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setUpMap()V

    .line 408
    :cond_0
    return-void

    .line 399
    :catch_0
    move-exception v0

    .line 400
    .local v0, "e":Ljava/lang/Exception;
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getPath(ILjava/util/ArrayList;)Ljava/lang/String;
    .locals 1
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 569
    .local p2, "Infos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;>;"
    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$OverlayInfo;->getPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 349
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/StateManager;->getTopState()Lcom/sec/android/gallery3d/app/ActivityState;

    move-result-object v0

    .line 350
    .local v0, "topState":Lcom/sec/android/gallery3d/app/ActivityState;
    if-ne p0, v0, :cond_0

    .line 351
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/sec/android/gallery3d/app/StateManager;->finishState(Lcom/sec/android/gallery3d/app/ActivityState;)V

    .line 352
    :cond_0
    return-void
.end method

.method public onCameraChange(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 3
    .param p1, "cameraPosition"    # Lcom/google/android/gms/maps/model/CameraPosition;

    .prologue
    .line 664
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Zoom Level : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/gms/maps/model/CameraPosition;->zoom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 665
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;
    .param p2, "storedState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x1020002

    .line 164
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    const-string v3, "MapViewState onCreate Start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f00bf

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/gallery3d/glcore/GlRootView;

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    .line 167
    new-instance v2, Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v2, v3, p0}, Lcom/sec/samsung/gallery/view/ActionBarManager;-><init>(Landroid/content/Context;Lcom/sec/android/gallery3d/app/ActivityState;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    .line 168
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    .line 169
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDrawer()Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    .line 170
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v2, v3}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->setStatusProxy(Lcom/sec/android/gallery3d/app/StateManager;)V

    .line 172
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const v3, 0x7f0f0175

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMainRL:Landroid/widget/RelativeLayout;

    .line 174
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 176
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f030087

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewLayout:Landroid/view/View;

    .line 178
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->initializeDataAdapter(Landroid/os/Bundle;)V

    .line 181
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 182
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "map"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/MapFragment;

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    .line 189
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    if-nez v2, :cond_0

    .line 191
    new-instance v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState$2;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$2;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    .line 199
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 201
    .local v0, "fragmentTransaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 202
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    const-string v3, "map"

    invoke-virtual {v0, v4, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 206
    :goto_1
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 209
    .end local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_0
    new-instance v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState$3;

    invoke-direct {v2, p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$3;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;)V

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mHandler:Landroid/os/Handler;

    .line 223
    sget-object v2, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    const-string v3, "MapViewState onCreate End"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 224
    return-void

    .line 185
    :cond_1
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "eventmap"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/maps/MapFragment;

    iput-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    goto :goto_0

    .line 204
    .restart local v0    # "fragmentTransaction":Landroid/app/FragmentTransaction;
    :cond_2
    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    const-string v3, "eventmap"

    invoke-virtual {v0, v4, v2, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    goto :goto_1
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 330
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onCreateOptionsMenu(Landroid/view/Menu;)V

    .line 331
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_1

    .line 326
    :cond_0
    :goto_0
    return-void

    .line 323
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    if-eqz v0, :cond_0

    .line 324
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    goto :goto_0
.end method

.method public onMapTouch(Landroid/view/MotionEvent;)V
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 669
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_0

    .line 685
    :goto_0
    :pswitch_0
    return-void

    .line 672
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 674
    :pswitch_1
    const-string v0, "TEST"

    const-string v1, "onMapTouch ACTION_DOWN"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 677
    :pswitch_2
    const-string v0, "TEST"

    const-string v1, "onMapTouch ACTION_MOVE"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 672
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 340
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mStatusProxy:Lcom/sec/android/gallery3d/app/StateManager;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/StateManager;->isUpButtonVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 341
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->onBackPressed()V

    .line 345
    :goto_0
    return-void

    .line 344
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onOptionsItemSelected(Landroid/view/MenuItem;)V

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 292
    sget-object v0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    const-string v1, "MapViewState onPause Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPause()V

    .line 295
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->removeSourceContentListener()V

    .line 297
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMediaChangeObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v0, :cond_1

    .line 315
    :goto_0
    return-void

    .line 303
    :cond_1
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v0, v2}, Lcom/sec/android/gallery3d/glcore/GlRootView;->setVisibility(I)V

    .line 305
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    if-eqz v0, :cond_2

    .line 306
    invoke-static {v2}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->hideLocationThumbnail(I)V

    .line 307
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;->clear()V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->clusterkraf:Lcom/sec/samsung/gallery/mapfragment/clustering/Clusterkraf;

    .line 311
    :cond_2
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->hide(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 312
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    check-cast v0, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-static {v0}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v0

    const-string v1, "MAP_VIEW"

    invoke-virtual {v0, v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->removeMediator(Ljava/lang/String;)Lorg/puremvc/java/interfaces/IMediator;

    .line 313
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/GoogleMap;->stopAnimation()V

    goto :goto_0
.end method

.method public onPrepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 335
    iget-object v0, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v0, p1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 336
    return-void
.end method

.method public onResume()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 240
    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    const-string v2, "MapViewState onResume Start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mGlRootView:Lcom/sec/android/gallery3d/glcore/GlRootView;

    invoke-virtual {v1, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlRootView;->attachLayer(Lcom/sec/android/gallery3d/glcore/GlLayer;Lcom/sec/android/gallery3d/glcore/GlLayer$onLayerCallback;)V

    .line 244
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    if-nez v1, :cond_3

    .line 245
    invoke-direct {p0}, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->setUpMapIfNeeded()V

    .line 250
    :goto_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapFragment:Lcom/google/android/gms/maps/MapFragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->show(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 252
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->addSourceContentListener()V

    .line 253
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewDataAdapter:Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMediaChangeObserver:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewDataAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 254
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-static {v1}, Lcom/sec/samsung/gallery/core/GalleryFacade;->getInstance(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)Lcom/sec/samsung/gallery/core/GalleryFacade;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMapViewMediator:Lorg/puremvc/java/patterns/mediator/Mediator;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/core/GalleryFacade;->registerMediator(Lorg/puremvc/java/interfaces/IMediator;)V

    .line 257
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    new-instance v2, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;

    iget-object v3, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    iget-object v4, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentSetPath:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentItemPath:Ljava/lang/String;

    iget v6, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumPosition:I

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/samsung/gallery/view/mapview/MapActionBarForNormal;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setAction(Lcom/sec/samsung/gallery/view/AbstractActionBarView;)V

    .line 259
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentItemPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 260
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mCurrentItemPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getMediaSet(Ljava/lang/String;)Lcom/sec/android/gallery3d/data/MediaSet;

    move-result-object v0

    .line 261
    .local v0, "set":Lcom/sec/android/gallery3d/data/MediaSet;
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Lcom/sec/android/gallery3d/data/MediaSet;->getName()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;

    .line 265
    .end local v0    # "set":Lcom/sec/android/gallery3d/data/MediaSet;
    :cond_0
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 266
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mAlbumName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(Ljava/lang/String;)V

    .line 270
    :goto_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mDrawer:Lcom/sec/samsung/gallery/drawer/NavigationDrawer;

    invoke-virtual {v1, v8}, Lcom/sec/samsung/gallery/drawer/NavigationDrawer;->updateDrawerMode(Z)V

    .line 271
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/ActionBarManager;->onResume()V

    .line 273
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    if-eqz v1, :cond_1

    .line 274
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->cancel(Z)Z

    .line 275
    iput-object v7, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    .line 277
    :cond_1
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->isDestroyed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 278
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_2

    .line 279
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mLoadingDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 282
    :cond_2
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    invoke-direct {v1, p0, v7}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;-><init>(Lcom/sec/samsung/gallery/mapfragment/MapViewState;Lcom/sec/samsung/gallery/mapfragment/MapViewState$1;)V

    iput-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    .line 283
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMarkerDrawTask:Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;

    new-array v2, v8, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewState$MarkerDrawTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 285
    new-instance v1, Lcom/sec/samsung/gallery/mapfragment/MapViewBeam;

    iget-object v2, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActivity:Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-direct {v1, v2}, Lcom/sec/samsung/gallery/mapfragment/MapViewBeam;-><init>(Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;)V

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/mapfragment/MapViewBeam;->setBeamListener()V

    .line 287
    sget-object v1, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->TAG:Ljava/lang/String;

    const-string v2, "MapViewState onResume End"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    return-void

    .line 247
    :cond_3
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mMap:Lcom/google/android/gms/maps/GoogleMap;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/GoogleMap;->clear()V

    goto/16 :goto_0

    .line 268
    :cond_4
    iget-object v1, p0, Lcom/sec/samsung/gallery/mapfragment/MapViewState;->mActionBarManager:Lcom/sec/samsung/gallery/view/ActionBarManager;

    const v2, 0x7f0e0074

    invoke-virtual {v1, v2}, Lcom/sec/samsung/gallery/view/ActionBarManager;->setTitle(I)V

    goto :goto_1
.end method

.method public update(Ljava/util/Observable;Ljava/lang/Object;)V
    .locals 0
    .param p1, "observable"    # Ljava/util/Observable;
    .param p2, "data"    # Ljava/lang/Object;

    .prologue
    .line 357
    return-void
.end method
