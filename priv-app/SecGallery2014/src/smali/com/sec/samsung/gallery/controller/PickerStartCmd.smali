.class public Lcom/sec/samsung/gallery/controller/PickerStartCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PickerStartCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field private static final DEBUG:Z = false

.field private static final KEY_CONTACT_CALLER_ID:Ljava/lang/String; = "caller_id_pick"

.field private static final KEY_ONLY_3GP:Ljava/lang/String; = "only3gp"

.field private static final KEY_ONLY_JPG:Ljava/lang/String; = "onlyJpg"

.field public static final KEY_ONLY_MAGIC:Ljava/lang/String; = "onlyMagic"

.field private static final KEY_SENDER_VIDEOCALL:Ljava/lang/String; = "senderIsVideoCall"

.field private static final KEY_SKIP_GOLF:Ljava/lang/String; = "skip_golf"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private contentType:Ljava/lang/String;

.field private mActivity:Landroid/app/Activity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method private getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 201
    invoke-virtual {p2}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v1

    .line 202
    .local v1, "type":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 210
    .end local v1    # "type":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 205
    .restart local v1    # "type":Ljava/lang/String;
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 207
    .local v2, "uri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 208
    :catch_0
    move-exception v0

    .line 209
    .local v0, "t":Ljava/lang/Throwable;
    sget-object v3, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->TAG:Ljava/lang/String;

    const-string v4, "Failed to get intent content type!"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    const-string v1, ""

    goto :goto_0
.end method

.method private getFilterType(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .locals 1
    .param p1, "contentType"    # Ljava/lang/String;

    .prologue
    .line 215
    const-string/jumbo v0, "video"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .line 220
    :goto_0
    return-object v0

    .line 217
    :cond_0
    const-string v0, "*/*"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 218
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE_AND_VIDEO:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    goto :goto_0

    .line 220
    :cond_1
    sget-object v0, Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;->IMAGE:Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    goto :goto_0
.end method

.method private getFilterTypeToString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->contentType:Ljava/lang/String;

    const-string/jumbo v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string/jumbo v0, "video"

    .line 227
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "image"

    goto :goto_0
.end method

.method private getFilteredSetPath(Landroid/content/Intent;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/String;ZI)Ljava/lang/String;
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .param p3, "filterMimeType"    # Ljava/lang/String;
    .param p4, "onlyLocal"    # Z
    .param p5, "singleAlbumBucketId"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 231
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v6, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v6}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    .line 232
    .local v1, "dataProxy":Lcom/sec/android/gallery3d/data/DataManager;
    const/4 v5, 0x3

    .line 233
    .local v5, "typeBit":I
    if-eqz p4, :cond_1

    .line 234
    const/4 v5, 0x5

    .line 259
    :cond_0
    :goto_0
    const/4 v0, 0x0

    .line 260
    .local v0, "basePath":Ljava/lang/String;
    if-eqz p5, :cond_8

    .line 261
    invoke-static {p5, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getSingleAlbumSetPath(II)Ljava/lang/String;

    move-result-object v0

    .line 266
    :goto_1
    if-eqz p3, :cond_9

    .line 267
    invoke-static {p2}, Lcom/sec/android/gallery3d/app/FilterUtils;->toFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)I

    move-result v6

    invoke-static {v0, v6, p3}, Lcom/sec/android/gallery3d/app/FilterUtils;->switchFilterPath(Ljava/lang/String;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 271
    .local v4, "newPath":Ljava/lang/String;
    :goto_2
    return-object v4

    .line 236
    .end local v0    # "basePath":Ljava/lang/String;
    .end local v4    # "newPath":Ljava/lang/String;
    :cond_1
    iget-object v6, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    invoke-static {v6, p1}, Lcom/sec/android/gallery3d/util/GalleryUtils;->determineTypeBits(Landroid/content/Context;Landroid/content/Intent;)I

    move-result v5

    .line 237
    const-string v6, "android.intent.action.GET_CONTENT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.MULTIPLE_PICK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.PICK"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 240
    :cond_2
    if-ne v5, v2, :cond_7

    .line 241
    const-string v6, "mode-crop"

    invoke-virtual {p1, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 242
    .local v3, "modeWallpaper":Z
    const-string v6, "crop"

    invoke-virtual {p1, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 243
    .local v2, "modeContact":Z
    :goto_3
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseCloud:Z

    if-eqz v6, :cond_5

    .line 244
    const/16 v5, 0xd

    .line 245
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSetAsSNSImage:Z

    if-nez v6, :cond_0

    if-nez v3, :cond_3

    if-eqz v2, :cond_0

    .line 246
    :cond_3
    const/16 v5, 0x11

    goto :goto_0

    .end local v2    # "modeContact":Z
    :cond_4
    move v2, v7

    .line 242
    goto :goto_3

    .line 249
    .restart local v2    # "modeContact":Z
    :cond_5
    sget-boolean v6, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseSetAsSNSImage:Z

    if-nez v6, :cond_0

    if-nez v3, :cond_6

    if-eqz v2, :cond_0

    .line 250
    :cond_6
    const/16 v5, 0x11

    goto :goto_0

    .line 253
    .end local v2    # "modeContact":Z
    .end local v3    # "modeWallpaper":Z
    :cond_7
    const/4 v6, 0x2

    if-ne v5, v6, :cond_0

    .line 254
    const/16 v5, 0x14

    goto :goto_0

    .line 263
    .restart local v0    # "basePath":Ljava/lang/String;
    :cond_8
    invoke-virtual {v1, v5}, Lcom/sec/android/gallery3d/data/DataManager;->getTopSetPath(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 269
    :cond_9
    move-object v4, v0

    .restart local v4    # "newPath":Ljava/lang/String;
    goto :goto_2
.end method

.method private setMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V
    .locals 2
    .param p1, "filterType"    # Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    .prologue
    .line 275
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v0

    .line 276
    .local v0, "statusProxy":Lcom/sec/android/gallery3d/app/StateManager;
    if-eqz v0, :cond_0

    .line 277
    invoke-virtual {v0, p1}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 278
    :cond_0
    return-void
.end method

.method private startAblumPickMode(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "newPath"    # Ljava/lang/String;
    .param p2, "isAlbumPick"    # Z

    .prologue
    const/4 v3, 0x1

    .line 150
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 151
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v1, "KEY_PICK_MEDIA_TYPE"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->getFilterTypeToString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    if-eqz p2, :cond_0

    .line 155
    const-string v1, "album-pick"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 157
    :cond_0
    const-string v1, "only-album-pick"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 159
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 161
    return-void
.end method

.method private startPickMode(Ljava/lang/String;)V
    .locals 8
    .param p1, "newPath"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 164
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 165
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v4, "KEY_PICK_MEDIA_TYPE"

    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->getFilterTypeToString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    iget-object v4, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v4, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v4}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    .line 169
    .local v2, "stateManager":Lcom/sec/android/gallery3d/app/StateManager;
    if-nez v2, :cond_0

    .line 198
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {v2, v6}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentTabTagType(Z)Lcom/sec/samsung/gallery/core/TabTagType;

    move-result-object v3

    .line 175
    .local v3, "tabTagType":Lcom/sec/samsung/gallery/core/TabTagType;
    sget-boolean v4, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseDefaultAlbumView:Z

    if-eqz v4, :cond_2

    .line 176
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v4, :cond_1

    .line 177
    invoke-static {p1, v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 178
    .local v1, "clusterPath":Ljava/lang/String;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v2, v4, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 180
    const-class v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v2, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 182
    .end local v1    # "clusterPath":Ljava/lang/String;
    :cond_1
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v2, v4, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 184
    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v2, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 187
    :cond_2
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    if-ne v3, v4, :cond_3

    .line 188
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_ALBUM:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v2, v4, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 190
    const-class v4, Lcom/sec/samsung/gallery/view/albumview/AlbumViewState;

    invoke-virtual {v2, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0

    .line 192
    :cond_3
    invoke-static {p1, v7}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    .line 193
    .restart local v1    # "clusterPath":Ljava/lang/String;
    const-string v4, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    sget-object v4, Lcom/sec/samsung/gallery/core/TabTagType;->TAB_TAG_TIMELINE:Lcom/sec/samsung/gallery/core/TabTagType;

    invoke-virtual {v2, v4, v6}, Lcom/sec/android/gallery3d/app/StateManager;->setCurrentTabTagType(Lcom/sec/samsung/gallery/core/TabTagType;Z)V

    .line 195
    const-class v4, Lcom/sec/samsung/gallery/view/timeview/TimeViewState;

    invoke-virtual {v2, v4, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private startSingleAlbumPickMode(Ljava/lang/String;)V
    .locals 3
    .param p1, "newPath"    # Ljava/lang/String;

    .prologue
    .line 142
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 143
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "KEY_MEDIA_SET_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    const-string v1, "KEY_MEDIA_ITEM_PATH"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v1, "KEY_MEDIA_SET_POSITION"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v1

    const-class v2, Lcom/sec/samsung/gallery/view/photoview/PhotoViewState;

    invoke-virtual {v1, v2, v0}, Lcom/sec/android/gallery3d/app/StateManager;->startState(Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 147
    return-void
.end method


# virtual methods
.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 21
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    .line 51
    invoke-interface/range {p1 .. p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/Object;

    move-object/from16 v16, v2

    check-cast v16, [Ljava/lang/Object;

    .line 52
    .local v16, "params":[Ljava/lang/Object;
    const/4 v2, 0x0

    aget-object v2, v16, v2

    check-cast v2, Landroid/app/Activity;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    .line 53
    const/4 v2, 0x1

    aget-object v3, v16, v2

    check-cast v3, Landroid/content/Intent;

    .line 55
    .local v3, "intent":Landroid/content/Intent;
    const-string v2, "album-pick"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v9

    .line 56
    .local v9, "isAlbumPick":Z
    const-string v2, "onlyMagic"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 57
    .local v10, "isMagicPick":Z
    const-string v2, "android.intent.action.PERSON_PICK"

    invoke-virtual {v3}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    .line 58
    .local v11, "isPersonPick":Z
    const-string v2, "senderIsVideoCall"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    .line 59
    .local v19, "videoCall":Z
    const-string/jumbo v2, "single-album"

    invoke-virtual {v3, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 60
    .local v17, "singleAlbum":Ljava/lang/String;
    const/4 v5, 0x0

    .line 62
    .local v5, "filterMimeType":Ljava/lang/String;
    if-eqz v19, :cond_0

    .line 63
    const-string v2, "onlyJpg"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v14

    .line 64
    .local v14, "onlyJpg":Z
    const-string v2, "only3gp"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v13

    .line 66
    .local v13, "only3gp":Z
    if-eqz v14, :cond_9

    .line 67
    const-string v5, "image/jpeg"

    .line 73
    .end local v13    # "only3gp":Z
    .end local v14    # "onlyJpg":Z
    :cond_0
    :goto_0
    const-string v2, "image/png"

    invoke-virtual {v3}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    .line 74
    .local v15, "onlyPng":Z
    if-eqz v15, :cond_1

    .line 75
    const-string v5, "image/png"

    .line 78
    :cond_1
    const/16 v18, 0x0

    .line 79
    .local v18, "skipGolf":Z
    if-eqz v18, :cond_2

    .line 80
    const-string/jumbo v5, "skip:image/golf"

    .line 83
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->getContentType(Landroid/app/Activity;Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->contentType:Ljava/lang/String;

    .line 84
    if-eqz v10, :cond_3

    .line 85
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->contentType:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v20, "#"

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v20, 0x830

    move/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 88
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->contentType:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->getFilterType(Ljava/lang/String;)Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;

    move-result-object v4

    .line 89
    .local v4, "filterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->setMediaFilterType(Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;)V

    .line 90
    const-string v2, "caller_id_pick"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    .line 92
    .local v6, "isContactPickerMode":Z
    const/4 v7, 0x0

    .line 93
    .local v7, "singleAlbumBucketId":I
    invoke-static/range {v17 .. v17}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 94
    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v20, 0x2f

    move/from16 v0, v20

    if-ne v2, v0, :cond_4

    .line 95
    const/4 v2, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v20

    add-int/lit8 v20, v20, -0x1

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v17

    .line 97
    :cond_4
    invoke-static/range {v17 .. v17}, Lcom/sec/android/gallery3d/util/GalleryUtils;->getBucketId(Ljava/lang/String;)I

    move-result v7

    :cond_5
    move-object/from16 v2, p0

    .line 100
    invoke-direct/range {v2 .. v7}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->getFilteredSetPath(Landroid/content/Intent;Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;Ljava/lang/String;ZI)Ljava/lang/String;

    move-result-object v12

    .line 109
    .local v12, "newPath":Ljava/lang/String;
    const/4 v8, 0x0

    .line 110
    .local v8, "clusterType":I
    if-eqz v6, :cond_a

    .line 111
    const/16 v8, 0x4000

    .line 120
    :cond_6
    :goto_1
    const/16 v2, 0x4000

    if-eq v8, v2, :cond_7

    const v2, 0x8000

    if-eq v8, v2, :cond_7

    const/high16 v2, 0x10000

    if-ne v8, v2, :cond_8

    .line 123
    :cond_7
    invoke-static {v12, v8}, Lcom/sec/android/gallery3d/app/FilterUtils;->newClusterPath(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v12

    .line 131
    :cond_8
    if-eqz v7, :cond_c

    .line 132
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/gallery3d/app/GalleryActivity;

    const/16 v20, 0x1

    move/from16 v0, v20

    iput-boolean v0, v2, Lcom/sec/android/gallery3d/app/GalleryActivity;->mIsSingleAlbumForPick:Z

    .line 133
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->startSingleAlbumPickMode(Ljava/lang/String;)V

    .line 139
    :goto_2
    return-void

    .line 68
    .end local v4    # "filterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .end local v6    # "isContactPickerMode":Z
    .end local v7    # "singleAlbumBucketId":I
    .end local v8    # "clusterType":I
    .end local v12    # "newPath":Ljava/lang/String;
    .end local v15    # "onlyPng":Z
    .end local v18    # "skipGolf":Z
    .restart local v13    # "only3gp":Z
    .restart local v14    # "onlyJpg":Z
    :cond_9
    if-eqz v13, :cond_0

    .line 69
    const-string/jumbo v5, "video/3gp"

    goto/16 :goto_0

    .line 112
    .end local v13    # "only3gp":Z
    .end local v14    # "onlyJpg":Z
    .restart local v4    # "filterType":Lcom/sec/samsung/gallery/core/MediaType$MediaFilterType;
    .restart local v6    # "isContactPickerMode":Z
    .restart local v7    # "singleAlbumBucketId":I
    .restart local v8    # "clusterType":I
    .restart local v12    # "newPath":Ljava/lang/String;
    .restart local v15    # "onlyPng":Z
    .restart local v18    # "skipGolf":Z
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->mActivity:Landroid/app/Activity;

    check-cast v2, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getStateManager()Lcom/sec/android/gallery3d/app/StateManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/gallery3d/app/StateManager;->getCurrentLaunchMode()Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-result-object v2

    sget-object v20, Lcom/sec/samsung/gallery/core/LaunchModeType;->ACTION_PERSON_PICK:Lcom/sec/samsung/gallery/core/LaunchModeType;

    move-object/from16 v0, v20

    if-ne v2, v0, :cond_6

    .line 113
    const-string v2, "include-recommend"

    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v3, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 114
    const/high16 v8, 0x10000

    goto :goto_1

    .line 116
    :cond_b
    const v8, 0x8000

    goto :goto_1

    .line 134
    :cond_c
    if-nez v9, :cond_d

    if-nez v11, :cond_d

    if-nez v10, :cond_d

    if-eqz v8, :cond_e

    .line 135
    :cond_d
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v9}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->startAblumPickMode(Ljava/lang/String;Z)V

    goto :goto_2

    .line 137
    :cond_e
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/sec/samsung/gallery/controller/PickerStartCmd;->startPickMode(Ljava/lang/String;)V

    goto :goto_2
.end method
