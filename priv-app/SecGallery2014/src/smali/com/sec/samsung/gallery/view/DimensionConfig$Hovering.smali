.class public Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;
.super Ljava/lang/Object;
.source "DimensionConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/DimensionConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Hovering"
.end annotation


# static fields
.field private static sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;


# instance fields
.field public hovering_actionbar_button_height:I

.field public hovering_actionbar_button_label_height:I

.field public hovering_actionbar_button_label_width:I

.field public hovering_actionbar_button_margin_left:I

.field public hovering_actionbar_button_margin_right:I

.field public hovering_actionbar_button_width:I

.field public hovering_actionbar_divider_height:I

.field public hovering_actionbar_divider_width:I

.field public hovering_actionbar_height:I

.field public hovering_actionbar_min_height:I

.field public hovering_actionbar_min_width:I

.field public hovering_actionbar_shadow_size:I

.field public hovering_background_res_padding_bottom:I

.field public hovering_background_res_padding_inside:I

.field public hovering_background_res_padding_left:I

.field public hovering_background_res_padding_right:I

.field public hovering_background_res_padding_top:I

.field public hovering_image_gap:I

.field public hovering_image_height:I

.field public hovering_image_offset_from_original_thumbnail:I

.field public hovering_image_set_height:I

.field public hovering_image_set_offset_from_original_thumbnail:I

.field public hovering_image_set_width:I

.field public hovering_image_width:I


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 527
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 528
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 529
    .local v0, "r":Landroid/content/res/Resources;
    const v1, 0x7f0d039d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_width:I

    .line 530
    const v1, 0x7f0d039e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_height:I

    .line 531
    const v1, 0x7f0d039f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_offset_from_original_thumbnail:I

    .line 533
    const v1, 0x7f0d03a0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_width:I

    .line 534
    const v1, 0x7f0d03a1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_height:I

    .line 535
    const v1, 0x7f0d03a2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_set_offset_from_original_thumbnail:I

    .line 537
    const v1, 0x7f0d03a3

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_image_gap:I

    .line 538
    const v1, 0x7f0d03a4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_top:I

    .line 539
    const v1, 0x7f0d03a5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_left:I

    .line 540
    const v1, 0x7f0d03a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_right:I

    .line 541
    const v1, 0x7f0d03a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_bottom:I

    .line 542
    const v1, 0x7f0d03a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_background_res_padding_inside:I

    .line 543
    const v1, 0x7f0d03a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_divider_height:I

    .line 544
    const v1, 0x7f0d03aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_divider_width:I

    .line 545
    const v1, 0x7f0d03ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_width:I

    .line 546
    const v1, 0x7f0d03ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_height:I

    .line 547
    const v1, 0x7f0d03ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_margin_left:I

    .line 548
    const v1, 0x7f0d03ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_margin_right:I

    .line 549
    const v1, 0x7f0d03af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_height:I

    .line 550
    const v1, 0x7f0d03b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_shadow_size:I

    .line 551
    const v1, 0x7f0d03b1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_min_height:I

    .line 552
    const v1, 0x7f0d03b2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_min_width:I

    .line 553
    const v1, 0x7f0d03b4

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_label_height:I

    .line 554
    const v1, 0x7f0d03b5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->hovering_actionbar_button_label_width:I

    .line 555
    return-void
.end method

.method public static declared-synchronized get(Landroid/content/Context;)Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 521
    const-class v1, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    if-nez v0, :cond_0

    .line 522
    new-instance v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    invoke-direct {v0, p0}, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;

    .line 524
    :cond_0
    sget-object v0, Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;->sInstance:Lcom/sec/samsung/gallery/view/DimensionConfig$Hovering;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 521
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
