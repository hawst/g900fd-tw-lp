.class public final enum Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;
.super Ljava/lang/Enum;
.source "ShowBargeInNotificationCmd.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BargiInNotiCmdType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

.field public static final enum HIDE:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

.field public static final enum SHOW:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 32
    new-instance v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    const-string v1, "SHOW"

    invoke-direct {v0, v1, v2}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->SHOW:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    new-instance v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    const-string v1, "HIDE"

    invoke-direct {v0, v1, v3}, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->HIDE:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    sget-object v1, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->SHOW:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->HIDE:Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    aput-object v1, v0, v3

    sput-object v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    return-object v0
.end method

.method public static values()[Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->$VALUES:[Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    invoke-virtual {v0}, [Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/sec/samsung/gallery/controller/ShowBargeInNotificationCmd$BargiInNotiCmdType;

    return-object v0
.end method
