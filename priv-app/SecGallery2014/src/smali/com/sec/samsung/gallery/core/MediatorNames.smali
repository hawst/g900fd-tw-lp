.class public Lcom/sec/samsung/gallery/core/MediatorNames;
.super Ljava/lang/Object;
.source "MediatorNames.java"


# static fields
.field public static final AIR_BROWSE_VIEW:Ljava/lang/String; = "AIR_BROWSE_VIEW"

.field public static final ALBUM_VIEW:Ljava/lang/String; = "ALBUM_VIEW"

.field public static final ALBUM_VIEW_EXIT_SELECTION:Ljava/lang/String; = "ALBUM_VIEW_EXIT_SELECTION"

.field public static final ALBUM_VIEW_MEDIA_EJECT:Ljava/lang/String; = "ALBUM_VIEW_MEDIA_EJECT"

.field public static final ALL_VIEW:Ljava/lang/String; = "ALL_VIEW"

.field public static final ALL_VIEW_MEDIA_REMOVED:Ljava/lang/String; = "ALL_VIEW_MEDIA_REMOVED"

.field public static final ASSIGN_FACE_TAG:Ljava/lang/String; = "ASSIGN_FACE_TAG"

.field public static final EVENT_DRAWER:Ljava/lang/String; = "EVENT_DRAWER"

.field public static final EVENT_NAVIGATION_DRAWER:Ljava/lang/String; = "EVENT_NAVIGATION_DRAWER"

.field public static final EVENT_VIEW:Ljava/lang/String; = "EVENT_VIEW"

.field public static final EVENT_VIEW_EXIT_SELECTION:Ljava/lang/String; = "EVENT_VIEW_EXIT_SELECTION"

.field public static final EVENT_VIEW_MEDIA_EJECT:Ljava/lang/String; = "EVENT_VIEW_MEDIA_EJECT"

.field public static final GALLERY_SEARCH_VIEW:Ljava/lang/String; = "GALLERY_SEARCH_VIEW"

.field public static final HELP_VIEW:Ljava/lang/String; = "HELP_VIEW"

.field public static final HIDDEN_ALBUM_VIEW:Ljava/lang/String; = "HIDDEN_ALBUM_VIEW"

.field public static final HIDDEN_ITEM_VIEW:Ljava/lang/String; = "HIDDEN_ITEM_VIEW"

.field public static final IMAGE_EDIT_VIEW:Ljava/lang/String; = "IMAGE_EDIT_VIEW"

.field public static final MAP_VIEW:Ljava/lang/String; = "MAP_VIEW"

.field public static final NEW_ALBUM_VIEW:Ljava/lang/String; = "NEW_ALBUM_VIEW"

.field public static final NEW_ALBUM_VIEW_EXIT:Ljava/lang/String; = "NEW_ALBUM_VIEW_EXIT"

.field public static final NO_ITEM_VIEW:Ljava/lang/String; = "NO_ITEM_VIEW"

.field public static final PHOTO_PAGE:Ljava/lang/String; = "PHOTO_PAGE"

.field public static final PHOTO_VIEW:Ljava/lang/String; = "PHOTO_VIEW"

.field public static final PHOTO_VIEW_EXIT_SELECTION:Ljava/lang/String; = "PHOTO_VIEW_EXIT_SELECTION"

.field public static final PHOTO_VIEW_MEDIA_EJECT:Ljava/lang/String; = "PHOTO_VIEW_MEDIA_EJECT"

.field public static final SEARCH_VIEW:Ljava/lang/String; = "SEARCH_VIEW"

.field public static final SLIDESHOW_SETTINGS:Ljava/lang/String; = "SLIDESHOW_SETTINGS"

.field public static final SLIDE_SHOW_SETTING_VIEW:Ljava/lang/String; = "SLIDE_SHOW_SETTING_VIEW"

.field public static final TIMELINE_VIEW:Ljava/lang/String; = "TIMELINE_VIEW"

.field public static final TIMELINE_VIEW_MEDIA_EJECT:Ljava/lang/String; = "TIMELINE_VIEW_MEDIA_EJECT"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
