.class Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;
.super Ljava/lang/Object;
.source "SearchItemAdapter.java"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UpdateContent"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;


# direct methods
.method private constructor <init>(Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;)V
    .locals 0

    .prologue
    .line 353
    iput-object p1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;
    .param p2, "x1"    # Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$1;

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;-><init>(Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 353
    invoke-virtual {p0}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->call()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public call()Ljava/lang/Void;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 356
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    invoke-interface {v0}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->resetContentWindow()V

    .line 358
    iget-object v0, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    iget-object v0, v0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->mModelListener:Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;

    iget-object v1, p0, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter$UpdateContent;->this$0:Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;

    invoke-virtual {v1}, Lcom/sec/samsung/gallery/view/adapter/SearchItemAdapter;->getCount()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/sec/samsung/gallery/view/adapter/AbstractGlBaseAdapter$ModelListener;->onSizeChanged(I)V

    .line 360
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method
