.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1187
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 5
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v4, 0x1

    .line 1199
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1200
    .local v0, "compObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget v2, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    if-ne v2, v4, :cond_1

    const/4 v1, 0x0

    .line 1202
    .local v1, "typeIndex":I
    :goto_0
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 1203
    iget-object v2, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v2, v2, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v2, v2, v1

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-interface {v2, v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;->onGenericMotionCancel(Lcom/sec/android/gallery3d/glcore/GlLayer;)V

    .line 1205
    :cond_0
    return v4

    .line 1200
    .end local v1    # "typeIndex":I
    :cond_1
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlObject;II)Z
    .locals 6
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v5, 0x1

    .line 1189
    move-object v0, p1

    check-cast v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    .line 1190
    .local v0, "compObj":Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;
    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mObjective:I

    if-ne v3, v5, :cond_1

    const/4 v2, 0x0

    .line 1191
    .local v2, "typeIndex":I
    :goto_0
    if-nez v2, :cond_2

    iget v1, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1193
    .local v1, "index":I
    :goto_1
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v3, v3, v2

    if-eqz v3, :cond_0

    .line 1194
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnGenericMotionListener:[Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;

    aget-object v3, v3, v2

    iget-object v4, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$8;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v4, v4, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-interface {v3, v4, v1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnGenericMotionListener;->onGenericMotionEnter(Lcom/sec/android/gallery3d/glcore/GlLayer;I)V

    .line 1196
    :cond_0
    return v5

    .line 1190
    .end local v1    # "index":I
    .end local v2    # "typeIndex":I
    :cond_1
    const/4 v2, 0x2

    goto :goto_0

    .line 1191
    .restart local v2    # "typeIndex":I
    :cond_2
    iget v3, v0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    shr-int/lit8 v1, v3, 0x10

    goto :goto_1
.end method
