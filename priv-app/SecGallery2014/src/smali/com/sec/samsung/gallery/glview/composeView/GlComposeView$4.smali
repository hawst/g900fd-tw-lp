.class Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;
.super Ljava/lang/Object;
.source "GlComposeView.java"

# interfaces
.implements Lcom/sec/android/gallery3d/glcore/GlObject$GlClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V
    .locals 0

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Lcom/sec/android/gallery3d/glcore/GlObject;)Z
    .locals 8
    .param p1, "object"    # Lcom/sec/android/gallery3d/glcore/GlObject;

    .prologue
    const/4 v7, 0x0

    const/4 v4, 0x1

    .line 1030
    instance-of v3, p1, Lcom/sec/samsung/gallery/glview/composeView/PositionControllerBase$ThumbObject;

    if-eqz v3, :cond_2

    move-object v3, p1

    .line 1031
    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .line 1037
    .local v1, "index":I
    :goto_0
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickEnabled:Z

    if-nez v3, :cond_3

    .line 1038
    :cond_0
    const-string v3, "GlComposeView"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClick is canceled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickable:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", enabled = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-boolean v6, v6, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mClickEnabled:Z

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1070
    :cond_1
    :goto_1
    return v4

    .line 1033
    .end local v1    # "index":I
    :cond_2
    iget-object v3, p1, Lcom/sec/android/gallery3d/glcore/GlObject;->mParent:Lcom/sec/android/gallery3d/glcore/GlObject;

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    iget v1, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;->mIndex:I

    .restart local v1    # "index":I
    goto :goto_0

    .line 1041
    :cond_3
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->isIdle()Z

    move-result v3

    if-nez v3, :cond_5

    .line 1042
    const-string v5, "GlComposeView"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onClick is canceled Enlarge = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-nez v3, :cond_4

    const/4 v3, -0x1

    :goto_2
    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    move v3, v4

    goto :goto_2

    .line 1045
    :cond_5
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    if-eqz v3, :cond_1

    .line 1048
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3, p1}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->onClickThumbnail(Lcom/sec/android/gallery3d/glcore/GlObject;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1051
    shr-int/lit8 v0, v1, 0x10

    .line 1052
    .local v0, "albumIndex":I
    const v3, 0xffff

    and-int v2, v1, v3

    .line 1054
    .local v2, "photoIndex":I
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v3

    if-eqz v3, :cond_6

    .line 1055
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mQuickScrollHelpView:Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$800(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/view/common/QuickScrollHelpView;->removeView()V

    .line 1057
    :cond_6
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->playSoundOnClickThumb()V

    .line 1058
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # getter for: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mSelectionModeProxy:Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$700(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v3

    invoke-virtual {v3, v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->setExpansionMode(Z)V

    .line 1059
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    # invokes: Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->clearFocus()V
    invoke-static {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->access$900(Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;)V

    .line 1060
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    invoke-virtual {v3}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->IsCheckMode()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 1061
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-interface {v3, v5, v6, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    .line 1062
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mHandler:Lcom/sec/android/gallery3d/glcore/GlHandler;

    const/4 v5, 0x4

    invoke-virtual {v3, v5, v1, v7, v7}, Lcom/sec/android/gallery3d/glcore/GlHandler;->sendMessage(IIII)V

    goto/16 :goto_1

    .line 1066
    :cond_7
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mEnlargeAnim:Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;

    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v6, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mAdapter:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;

    move-object v3, p1

    check-cast v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;

    invoke-virtual {v5, v6, v3, v4}, Lcom/sec/samsung/gallery/glview/composeView/GlEnlargeAnimation;->prepareAnimation(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseAdapter;Lcom/sec/samsung/gallery/glview/composeView/GlComposeObject;Z)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1069
    :cond_8
    iget-object v3, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v3, v3, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mOnItemClickListener:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;

    iget-object v5, p0, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView$4;->this$0:Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;

    iget-object v5, v5, Lcom/sec/samsung/gallery/glview/composeView/GlComposeView;->mThis:Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;

    invoke-virtual {p1}, Lcom/sec/android/gallery3d/glcore/GlObject;->getView()Lcom/sec/android/gallery3d/glcore/GlView;

    move-result-object v6

    invoke-interface {v3, v5, v6, v0, v2}, Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView$OnItemClickListener;->onItemClick(Lcom/sec/samsung/gallery/glview/composeView/GlComposeBaseView;Lcom/sec/android/gallery3d/glcore/GlView;II)Z

    goto/16 :goto_1
.end method
