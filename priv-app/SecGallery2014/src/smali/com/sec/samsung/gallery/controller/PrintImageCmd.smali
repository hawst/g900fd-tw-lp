.class public Lcom/sec/samsung/gallery/controller/PrintImageCmd;
.super Lorg/puremvc/java/patterns/command/SimpleCommand;
.source "PrintImageCmd.java"

# interfaces
.implements Lorg/puremvc/java/interfaces/ICommand;


# static fields
.field public static final INTENT_ACTION_PRINT:Ljava/lang/String; = "com.sec.android.intent.action.PRINT"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mMediaList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/sec/android/gallery3d/data/MediaObject;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lorg/puremvc/java/patterns/command/SimpleCommand;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/samsung/gallery/controller/PrintImageCmd;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/sec/samsung/gallery/controller/PrintImageCmd;

    .prologue
    .line 45
    iget-object v0, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method private doPrintByPrintHelper()V
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 89
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 90
    .local v10, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    if-nez v10, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    .line 95
    .local v8, "filePath":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    check-cast v1, Lcom/sec/android/gallery3d/app/GalleryActivity;

    invoke-virtual {v1}, Lcom/sec/android/gallery3d/app/GalleryActivity;->getDataManager()Lcom/sec/android/gallery3d/data/DataManager;

    move-result-object v1

    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getPath()Lcom/sec/android/gallery3d/data/Path;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/sec/android/gallery3d/data/DataManager;->getContentUri(Lcom/sec/android/gallery3d/data/Path;)Landroid/net/Uri;

    move-result-object v9

    .line 97
    .local v9, "fileUri":Landroid/net/Uri;
    if-eqz v9, :cond_0

    .line 100
    if-eqz v8, :cond_3

    .line 101
    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    .line 102
    .local v11, "localUri":Landroid/net/Uri;
    invoke-virtual {v11}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    .line 107
    .end local v11    # "localUri":Landroid/net/Uri;
    :goto_1
    new-instance v12, Landroid/support/v4/print/PrintHelper;

    iget-object v1, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    invoke-direct {v12, v1}, Landroid/support/v4/print/PrintHelper;-><init>(Landroid/content/Context;)V

    .line 109
    .local v12, "printer":Landroid/support/v4/print/PrintHelper;
    const/4 v13, 0x0

    .line 110
    .local v13, "scaledBitmap":Landroid/graphics/Bitmap;
    const/4 v1, 0x1

    :try_start_0
    invoke-virtual {v10, v1}, Lcom/sec/android/gallery3d/data/MediaItem;->requestImage(I)Lcom/sec/android/gallery3d/util/ThreadPool$Job;

    move-result-object v1

    sget-object v2, Lcom/sec/android/gallery3d/util/ThreadPool;->JOB_CONTEXT_STUB:Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;

    invoke-interface {v1, v2}, Lcom/sec/android/gallery3d/util/ThreadPool$Job;->run(Lcom/sec/android/gallery3d/util/ThreadPool$JobContext;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 112
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 113
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 114
    .local v5, "mtx":Landroid/graphics/Matrix;
    invoke-virtual {v10}, Lcom/sec/android/gallery3d/data/MediaItem;->getRotation()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->postRotate(F)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v13

    .line 123
    .end local v5    # "mtx":Landroid/graphics/Matrix;
    :cond_2
    :goto_2
    const/4 v1, 0x1

    :try_start_2
    invoke-virtual {v12, v1}, Landroid/support/v4/print/PrintHelper;->setScaleMode(I)V

    .line 124
    invoke-virtual {v12, v8, v13}, Landroid/support/v4/print/PrintHelper;->printBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 125
    .end local v0    # "bitmap":Landroid/graphics/Bitmap;
    :catch_0
    move-exception v7

    .line 126
    .local v7, "e":Ljava/lang/Exception;
    sget-object v1, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->TAG:Ljava/lang/String;

    const-string v2, "Error printing an image"

    invoke-static {v1, v2, v7}, Landroid/util/secutil/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 104
    .end local v7    # "e":Ljava/lang/Exception;
    .end local v12    # "printer":Landroid/support/v4/print/PrintHelper;
    .end local v13    # "scaledBitmap":Landroid/graphics/Bitmap;
    :cond_3
    invoke-virtual {v9}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    goto :goto_1

    .line 118
    .restart local v0    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "mtx":Landroid/graphics/Matrix;
    .restart local v12    # "printer":Landroid/support/v4/print/PrintHelper;
    .restart local v13    # "scaledBitmap":Landroid/graphics/Bitmap;
    :catch_1
    move-exception v1

    goto :goto_2
.end method


# virtual methods
.method public doPrint()V
    .locals 4

    .prologue
    .line 131
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    const-string v3, "print"

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/print/PrintManager;

    .line 132
    .local v1, "printManager":Landroid/print/PrintManager;
    iget-object v2, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e002b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    .local v0, "jobName":Ljava/lang/String;
    new-instance v2, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;

    invoke-direct {v2, p0, v0}, Lcom/sec/samsung/gallery/controller/PrintImageCmd$1;-><init>(Lcom/sec/samsung/gallery/controller/PrintImageCmd;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Landroid/print/PrintManager;->print(Ljava/lang/String;Landroid/print/PrintDocumentAdapter;Landroid/print/PrintAttributes;)Landroid/print/PrintJob;

    .line 227
    return-void
.end method

.method public execute(Lorg/puremvc/java/interfaces/INotification;)V
    .locals 12
    .param p1, "noti"    # Lorg/puremvc/java/interfaces/INotification;

    .prologue
    const/4 v11, 0x0

    .line 52
    invoke-interface {p1}, Lorg/puremvc/java/interfaces/INotification;->getBody()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Ljava/lang/Object;

    move-object v6, v8

    check-cast v6, [Ljava/lang/Object;

    .line 53
    .local v6, "params":[Ljava/lang/Object;
    aget-object v8, v6, v11

    check-cast v8, Landroid/app/Activity;

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    .line 55
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    check-cast v8, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;

    invoke-virtual {v8}, Lcom/sec/android/gallery3d/app/AbstractGalleryActivity;->getSelectionManager()Lcom/sec/android/gallery3d/ui/SelectionManager;

    move-result-object v7

    .line 56
    .local v7, "selectionModeProxy":Lcom/sec/android/gallery3d/ui/SelectionManager;
    invoke-virtual {v7}, Lcom/sec/android/gallery3d/ui/SelectionManager;->getMediaList()Ljava/util/LinkedList;

    move-result-object v8

    iput-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;

    .line 58
    sget-boolean v8, Lcom/sec/android/gallery3d/util/GalleryFeature;->mUseMobilePrint:Z

    if-eqz v8, :cond_3

    .line 59
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 60
    .local v1, "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mMediaList:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/sec/android/gallery3d/data/MediaObject;

    .local v5, "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    move-object v4, v5

    .line 61
    check-cast v4, Lcom/sec/android/gallery3d/data/MediaItem;

    .line 63
    .local v4, "item":Lcom/sec/android/gallery3d/data/MediaItem;
    instance-of v8, v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    if-eqz v8, :cond_1

    .line 64
    check-cast v4, Lcom/sec/android/gallery3d/data/LocalFaceImage;

    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/LocalFaceImage;->getLocalImage()Lcom/sec/android/gallery3d/data/LocalImage;

    move-result-object v4

    .line 67
    .restart local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    :cond_1
    if-eqz v4, :cond_0

    .line 68
    invoke-virtual {v4}, Lcom/sec/android/gallery3d/data/MediaItem;->getFilePath()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 72
    .end local v4    # "item":Lcom/sec/android/gallery3d/data/MediaItem;
    .end local v5    # "mediaObject":Lcom/sec/android/gallery3d/data/MediaObject;
    :cond_2
    new-instance v3, Landroid/content/Intent;

    const-string v8, "com.sec.android.intent.action.PRINT"

    invoke-direct {v3, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 73
    .local v3, "intent":Landroid/content/Intent;
    const-string v8, "android.intent.extra.STREAM"

    invoke-virtual {v3, v8, v1}, Landroid/content/Intent;->putParcelableArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    .line 74
    const-string v8, "android.intent.extra.SUBJECT"

    iget-object v9, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0e002b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 75
    const-string v8, "android.intent.extra.DATA_REMOVED"

    invoke-virtual {v3, v8, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 77
    :try_start_0
    iget-object v8, p0, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->mActivity:Landroid/app/Activity;

    invoke-virtual {v8, v3}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 86
    .end local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 78
    .restart local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .restart local v2    # "i$":Ljava/util/Iterator;
    .restart local v3    # "intent":Landroid/content/Intent;
    :catch_0
    move-exception v0

    .line 79
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v8, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->TAG:Ljava/lang/String;

    const-string v9, "Activity Not Found"

    invoke-static {v8, v9}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    goto :goto_1

    .line 84
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "fileList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/os/Parcelable;>;"
    .end local v2    # "i$":Ljava/util/Iterator;
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_3
    invoke-direct {p0}, Lcom/sec/samsung/gallery/controller/PrintImageCmd;->doPrintByPrintHelper()V

    goto :goto_1
.end method
