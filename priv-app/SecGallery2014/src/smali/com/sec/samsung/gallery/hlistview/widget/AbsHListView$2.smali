.class Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;
.super Ljava/lang/Object;
.source "AbsHListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->clearScrollingCache()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;


# direct methods
.method constructor <init>(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;)V
    .locals 0

    .prologue
    .line 4420
    iput-object p1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4424
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-boolean v0, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingStarted:Z

    if-eqz v0, :cond_1

    .line 4425
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iget-object v1, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    iput-boolean v2, v1, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingActive:Z

    iput-boolean v2, v0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->mCachingStarted:Z

    .line 4426
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawnWithCacheEnabled(Z)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2000(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Z)V

    .line 4427
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->getPersistentDrawingCache()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 4428
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    # invokes: Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->setChildrenDrawingCacheEnabled(Z)V
    invoke-static {v0, v2}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->access$2100(Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;Z)V

    .line 4430
    :cond_0
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->isAlwaysDrawnWithCacheEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 4431
    iget-object v0, p0, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView$2;->this$0:Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;

    invoke-virtual {v0}, Lcom/sec/samsung/gallery/hlistview/widget/AbsHListView;->invalidate()V

    .line 4434
    :cond_1
    return-void
.end method
